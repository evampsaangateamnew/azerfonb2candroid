androidx_ssl_pining_gradle_updates -> 03-January-2020 -> staging -> moved to android x with gradle updates and new ssl pining keys with ui optimizations

action-bar-events-on-screen-launch-fixed -> 10-January-2020 -> staging -> app was crashing becasue of NPE on action bar events on the dummy fragments #FIXED_1.0.2

v_110_10_arrowsfor_packages -> 30-11-2020 -> staging -> added arrows for supplementary offers with other UI updates as per client requirements
