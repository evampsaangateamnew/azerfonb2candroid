# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
-keepclassmembers class fqcn.of.javascript.interface.for.webview {
   public *;
}

# Uncomment this to preserve the line number information for
# debugging stack traces.
-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
-renamesourcefileattribute SourceFile

#-optimizations   code/simplification/arithmetic,!code/simplification/cast,!field/*,! class/merging/*,!method/inlining/*
#-optimizationpasses 5
#-allowaccessmodification

-ignorewarnings

-keep class com.crashlytics.** { *; }
-keep class com.google.firebase.*.* { *; }

-keep class android.widget.** { *; }
-keep class android.support.v4.app.** { *; }
-keep class android.support.v4.content.** { *; }
-keep interface android.support.v4.app.** { *; }
-keep interface android.support.v4.content.** { *; }
-keep class android.support.v7.widget.** { *; }
-keep class * extends android.content.ContentProvider

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.support.v7.widget.AppCompatTextView
-keep public class * extends android.support.v7.widget.AppCompatButton
-keep public class * extends com.google.firebase.iid.FirebaseInstanceIdServic
-keep public class * extends com.google.firebase.messaging.FirebaseMessagingService
-keep public class * extends android.os.Parcelable

-keep class com.google.** { *; }
-keep class com.firebase.** { *; }
-keep class org.apache.** { *; }
-keepnames class com.fasterxml.jackson.** { *; }
-keepnames class javax.servlet.** { *; }
-keepnames class org.ietf.jgss.** { *; }
-dontwarn org.apache.**
-dontwarn org.w3c.dom.**

-dontwarn com.mixpanel.**

-keep class com.facebook.** { *; }
-keepattributes Signature

-dontwarn org.jetbrains.annotations.**
-keep class kotlin.Metadata { *; }

-keepclassmembers class * {
    @com.squareup.moshi.FromJson <methods>;
    @com.squareup.moshi.ToJson <methods>;
}

-keep class android.support.** { *; }
-keep interface android.support.** { *; }

## For Pretty Time  Library
-keep class org.ocpsoft.prettytime.** {*;}
-dontwarn org.ocpsoft.prettytime.**




### For Model Classes
-keep class com.azarphone.api.pojo.helperpojo.** { *; }
-keep class com.azarphone.api.pojo.request.** { *; }
-keep class com.azarphone.api.pojo.response.** { *; }



# Retrofit
-keep class com.google.gson.** { *; }
-keep public class com.google.gson.** {public private protected *;}
-keep class com.google.inject.** { *; }
-keep class org.apache.http.** { *; }
-keep class org.apache.james.mime4j.** { *; }
-keep class javax.inject.** { *; }
-keep class javax.xml.stream.** { *; }
-keep class retrofit.** { *; }
-keep class com.google.appengine.** { *; }
-keepattributes *Annotation*
-keepattributes Signature
-dontwarn com.squareup.okhttp.*
-dontwarn rx.**
-dontwarn javax.xml.stream.**
-dontwarn com.google.appengine.**
-dontwarn java.nio.file.**
-dontwarn org.codehaus.**

-dontwarn retrofit2.**
-dontwarn org.codehaus.mojo.**
-keep class retrofit2.** { *; }
-keepattributes Exceptions
-keepattributes RuntimeVisibleAnnotations
-keepattributes RuntimeInvisibleAnnotations
-keepattributes RuntimeVisibleParameterAnnotations
-keepattributes RuntimeInvisibleParameterAnnotations

-keepattributes EnclosingMethod
-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}
-keepclasseswithmembers interface * {
    @retrofit2.* <methods>;
}
# Platform calls Class.forName on types which do not exist on Android to determine platform.
-dontnote retrofit2.Platform
# Platform used when running on RoboVM on iOS. Will not be used at runtime.
-dontnote retrofit2.Platform$IOS$MainThreadExecutor
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8
# Retain generic type information for use by reflection by converters and adapters.
-keepattributes Signature
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions

#juni12891226 -> can be problematic, keep check on it
-dontwarn  io.canner.stepsview.**

-keep public class io.canner.stepsview.** {
  public protected *;
}

-dontwarn net.cachapa.expandablelayout.**

-keep public class net.cachapa.expandablelayout.** {
  public protected *;
}

-dontwarn com.facebook.**

-keep public class com.facebook.** {
  public protected *;
}




