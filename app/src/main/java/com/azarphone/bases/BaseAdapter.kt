package com.azarphone.bases

import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.azarphone.ui.adapters.MyViewHolder

abstract class BaseAdapter: androidx.recyclerview.widget.RecyclerView.Adapter<MyViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater=LayoutInflater.from(parent.context)
        val binding:ViewDataBinding=DataBindingUtil.inflate(layoutInflater,viewType,parent,false)
        val viewHolder=MyViewHolder(binding)
        viewHolder.bind(this)
        return viewHolder
    }

    override fun getItemViewType(position: Int): Int {
        return getLayoutIdForPosition(position)
    }

    override fun onBindViewHolder(holder: MyViewHolder, p1: Int) {
        val obj=getObjForPosition(p1)
        holder.bind(obj)
    }




    protected abstract fun getObjForPosition(position: Int): Any

    protected abstract fun getLayoutIdForPosition(position: Int): Int
}