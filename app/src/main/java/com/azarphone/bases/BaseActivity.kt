package com.azarphone.bases

import android.annotation.SuppressLint
import android.app.Dialog
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.azarphone.IView
import com.azarphone.LocalManager
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.response.BaseResponse
import com.azarphone.ui.activities.mainactivity.MainViewModel
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.isNetworkAvailable
import com.azarphone.util.logE
import com.azarphone.util.logOut
import com.azarphone.validators.hasValue
import com.azarphone.widgets.popups.showMessageDialog
import com.bumptech.glide.Glide
import io.github.inflationx.viewpump.ViewPumpContextWrapper


abstract class BaseActivity<T : ViewDataBinding, Factory : ViewModelProvider.NewInstanceFactory, V : BaseViewModel> : AppCompatActivity(), IView {

    private val fromClass = "BaseActivity"

    protected lateinit var viewDataBinding: T
    private var active = false

    protected lateinit var mViewModel: V
    /**
     * setup content layout
     * @return layout id
     */
    @LayoutRes
    protected abstract fun getLayoutId(): Int

    protected abstract fun getViewModelClass(): Class<V>


    protected abstract fun getFactory(): Factory

    /**
     * Life Cycle Events Start
     **/
    protected fun startScreen() {}

    abstract protected fun init()
    protected fun resumeScreen() {}
    protected fun pauseScreen() {}
    protected fun stopScreen() {}
    protected fun destroyScreen() {}


    /**
     *  Select Language Dialoge Implemeneted
     **/
    fun showLanguageDialoge() {
        val languagedialoge = Dialog(this)
        languagedialoge.requestWindowFeature(Window.FEATURE_NO_TITLE)
        languagedialoge.setContentView(R.layout.language_dialoge_layout)
        if (languagedialoge.window != null) {
            languagedialoge.window!!.setBackgroundDrawable(null)
        }
        languagedialoge.setCancelable(true)
        languagedialoge.setCanceledOnTouchOutside(true)
        val radioGroup = languagedialoge.findViewById<RadioGroup>(R.id.radioGroup)

        setCurrentLanguage(radioGroup)

        setRadioGroupListener(radioGroup)

        val button = languagedialoge.findViewById<Button>(R.id.applyBtn)
        button.isSelected = true
        button.setOnClickListener {
            if (languagedialoge != null) {
                if (languagedialoge.isShowing) {
                    languagedialoge.dismiss()
                }
            }
            val i = Intent(this, this.javaClass)
            startActivity(i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
        }

        languagedialoge.show()


    }

    fun setRadioGroupListener(radioGroup: RadioGroup) {

        radioGroup.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(p0: RadioGroup?, checkedId: Int) {
                setLangauge(checkedId)
            }
        })

    }

    fun setLangauge(checkedID: Int) {

        when (checkedID) {
            R.id.en -> ProjectApplication.mLocaleManager.setNewLocale(this, ConstantsUtility.LanguageKeywords.EN)
            R.id.ru -> ProjectApplication.mLocaleManager.setNewLocale(this, ConstantsUtility.LanguageKeywords.RU)
            R.id.az -> ProjectApplication.mLocaleManager.setNewLocale(this, ConstantsUtility.LanguageKeywords.AZ)
        }

    }


    fun setCurrentLanguage(radioGroup: RadioGroup) {
        val language = ProjectApplication.mLocaleManager.getLanguage()

        when (language) {
            ConstantsUtility.LanguageKeywords.EN -> radioGroup.findViewById<RadioButton>(R.id.en).isChecked = true
            ConstantsUtility.LanguageKeywords.AZ -> radioGroup.findViewById<RadioButton>(R.id.az).isChecked = true
            ConstantsUtility.LanguageKeywords.RU -> radioGroup.findViewById<RadioButton>(R.id.ru).isChecked = true
        }

    }

    /* Life Cycle Events End*/
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewDataBinding = DataBindingUtil.setContentView(this, getLayoutId())
        mViewModel = ViewModelProviders.of(this, getFactory())[getViewModelClass()]
        mViewModel.onCreated(this)

        setLanguageTitle()
        initializeDialoge()
        init()

    }

    fun setLanguageTitle() {
        findViewById<TextView>(R.id.languageTV)?.let {
            val language = ProjectApplication.mLocaleManager.getLanguage()
            when (language) {
                ConstantsUtility.LanguageKeywords.EN -> it.text = ConstantsUtility.LanguageKeywordsForUI.EN
                ConstantsUtility.LanguageKeywords.AZ -> it.text = ConstantsUtility.LanguageKeywordsForUI.AZ
                ConstantsUtility.LanguageKeywords.RU -> it.text = ConstantsUtility.LanguageKeywordsForUI.RU
            }
            it.setOnClickListener {
                showLanguageDialoge()
            }
        }
    }


    public override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        startScreen()
    }

    override fun onStart() {
        super.onStart()
        active = true
    }


    override fun onPostResume() {
        super.onPostResume()
    }

    override fun onResume() {
        super.onResume()
        System.gc()
        System.runFinalization()
        resumeScreen()
    }

    override fun onPause() {
        super.onPause()
        hideLoading()
        pauseScreen()
    }

    override fun onStop() {
        active = false
        super.onStop()
        stopScreen()
    }

    fun isActive(): Boolean {
        return active
    }

    override fun onDestroy() {
        super.onDestroy()
        if (this::dialog.isInitialized) {
            dialog.dismiss()
        }
        System.gc()
        System.runFinalization()
        destroyScreen()
    }


    /**
     * Method to Dispatch Touch Event if clicked anywhere else other then EditText
     * @param event
     **/
    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        val v = currentFocus
        if (v is EditText) {
            val scoops = IntArray(2)
            v.getLocationOnScreen(scoops)
            val x = event.rawX + v.left - scoops[0]
            val y = event.rawY + v.top - scoops[1]

            if (event.action == MotionEvent.ACTION_UP && (x < v.left || x >= v.right || y < v.top || y > v
                            .bottom)) {
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(v
                        .windowToken, 0)
            }
        }
        return super.dispatchTouchEvent(event)
    }


    /*
      Injecting View Pump for Calligraphy Support
     */
    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(ProjectApplication.mLocaleManager.setLocale(newBase)))
    }


    /*
     * Loader For all classes
     */
    lateinit var loaderBuilder: AlertDialog.Builder

    lateinit var dialog: Dialog

    @SuppressLint("InflateParams")
    fun initializeDialoge() {
        loaderBuilder = AlertDialog.Builder(this)
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val dialogView = inflater.inflate(R.layout.loader_layout, null)
        loaderBuilder.setView(dialogView)
        dialog = loaderBuilder.create()
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val loaderImage = dialogView.findViewById(R.id.loaderImage) as ImageView

        Glide
                .with(this@BaseActivity)
                .asGif()
                .load(R.drawable.azerloader)
                .into(loaderImage)
    }


    /**
     * Method to Hide Loading
     **/
    override fun hideLoading() {
        if (this::dialog.isInitialized && dialog.isShowing) {
            dialog.hide()
        }
    }

    /**
     * Method to Show Loading
     **/
    override fun showLoading() {
        if (this::dialog.isInitialized) {
            dialog.show()
        }
    }


    /**
     * Method to Load Throwable Error
     **/
    override fun loadError(e: Throwable) {
        if (!isNetworkAvailable(applicationContext)) {
            loadError(resources.getString(R.string.message_no_internet))
        } else {
            loadError(resources.getString(R.string.server_stopped_responding_please_try_again))
        }
    }


    /**
     * Method to Load Message
     **/
    override fun loadError(msg: String) {
        showMessageDialog(this, this, resources.getString(R.string.popup_error_title), msg)
    }


    /*
     * Method to Load Error
     */

    override fun <T> isErrorErrorDialoge(response: BaseResponse<T>, isForceLogout: Boolean): Boolean {
        if (isForceLogout) {
            //then logout
            return true
        } else {
            return isErrorErrorDialoge(response)
        }

    }


    override fun <T> isErrorErrorDialoge(response: BaseResponse<T>): Boolean {
        //check the response code
        if (!isNetworkAvailable(this)) {
            showMessageDialog(applicationContext, this,
                    this.resources.getString(R.string.popup_note_title),
                    this.resources.getString(R.string.message_no_internet))
            return true
        }
        when (response.resultCode.toString()) {
            ConstantsUtility.ServerResponseCodes.SUCCESS -> {

                if (response.data != null) {
                    if (!hasValue(response.data.toString())) {
                        /**the response has no data object
                         * in this case we have to handle the ui only*/
                        showMessageDialog(applicationContext, this,
                                this.resources.getString(R.string.popup_error_title),
                                this.resources.getString(R.string.server_stopped_responding_please_try_again))
                        return true
                    }
                } else {
                    /**the response has no data object
                     * in this case we have to handle the ui only*/
                    if (hasValue(response.resultDesc.toString())) {
                        showMessageDialog(applicationContext, this,
                                this.resources.getString(R.string.popup_error_title),
                                response.resultDesc.toString())
                    } else {
                        showMessageDialog(applicationContext, this,
                                this.resources.getString(R.string.popup_error_title),
                                this.resources.getString(R.string.server_stopped_responding_please_try_again))
                    }

                    return true
                }

            }
            ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT -> {
                logOut(applicationContext, this@BaseActivity, false, fromClass, "isErrorErrorDialoge1123")
                return true
            }

            else -> {
                logE("responseFail1289", "failed", "BaseActivity", "else")
                if (hasValue(response.resultDesc)) {
                    showMessageDialog(applicationContext, this,
                            this.resources.getString(R.string.popup_error_title),
                            response.resultDesc)
                    return true
                } else {
                    showMessageDialog(applicationContext, this,
                            this.resources.getString(R.string.popup_error_title),
                            this.resources.getString(R.string.server_stopped_responding_please_try_again))
                    return true
                }
            }
        }
        return false
    }

}