package com.azarphone.bases

import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.azarphone.IView
import com.azarphone.R
import com.azarphone.api.pojo.response.BaseResponse

/**
 * @author Ehtisham haq
 * Base Fragment Classes for all Base Related Operations
 **/
abstract class BaseFragment<F : ViewDataBinding, Factory : ViewModelProvider.NewInstanceFactory, V : BaseViewModel> : Fragment(), IView {

    lateinit var mFragmentViewDataBinding: F

    lateinit var mViewModel: V


    private val mShouldSave = true
    private var mIsInLeft: Boolean = false
    private var mIsOutLeft: Boolean = false
    protected var mIsCurrentScreen: Boolean = false
    private var mIsPush: Boolean = false

    private var mInitialized = true
    private var mViewCreated = false
    private var mViewDestroyed = false


    @LayoutRes
    protected abstract fun getLayoutId(): Int

    protected abstract fun getViewModelClass(): Class<V>


    protected abstract fun getFactory(): Factory


    protected abstract fun isBindedToActivityLifeCycle(): Boolean

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mFragmentViewDataBinding = DataBindingUtil.inflate<F>(inflater, getLayoutId(), container, false)
        return mFragmentViewDataBinding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialzeViewModel()
        onVisible(view, savedInstanceState)
        mViewCreated = true
        mViewDestroyed = false
    }

    fun initialzeViewModel() {

        if (isBindedToActivityLifeCycle()) {
            mViewModel = ViewModelProviders.of(activity as androidx.fragment.app.FragmentActivity, getFactory())[getViewModelClass()]
        } else {
            mViewModel = ViewModelProviders.of(this@BaseFragment, getFactory())[getViewModelClass()]
        }


    }

    override fun onDestroyView() {
        mViewDestroyed = true
        mViewCreated = false
        onInVisible()
        mFragmentViewDataBinding.unbind()
        super.onDestroyView()
    }

    override fun showLoading() {
    }

    override fun hideLoading() {

    }

    override fun loadError(e: Throwable) {
    }


    override fun loadError(msg: String) {
    }


    override fun <T> isErrorErrorDialoge(response: BaseResponse<T>, isForceLogout: Boolean): Boolean {
        return true
    }

    override fun <T> isErrorErrorDialoge(response: BaseResponse<T>): Boolean {
            return true
    }

    fun isInitialized(): Boolean {
        return mInitialized
    }

    fun initialized() {
        mInitialized = false
    }

    fun isViewCreated(): Boolean {
        return mViewCreated
    }

    fun setInLeft(inLeft: Boolean) {
        mIsInLeft = inLeft
    }

    fun setOutLeft(outLeft: Boolean) {
        mIsOutLeft = outLeft
    }

    fun setCurrentScreen(currentScreen: Boolean) {
        mIsCurrentScreen = currentScreen
    }

    fun setPush(push: Boolean) {
        mIsPush = push
    }

    fun isShouldSave(): Boolean {
        return mShouldSave
    }

    protected abstract fun onVisible(view: View, savedInstanceState: Bundle?)

    protected fun handleAfterVisible() {}

    protected abstract fun onInVisible()


    /*
     * Methods to get Animations From Anim Folder
     */

    protected fun getLeftInAnimId(): Int {
        return R.anim.slide_in_left
    }

    protected fun getRightInAnimId(): Int {
        return R.anim.slide_in_right
    }

    protected fun getLeftOutAnimId(): Int {
        return R.anim.slide_out_left
    }

    protected fun getRightOutAnimId(): Int {
        return R.anim.slide_out_right
    }


}