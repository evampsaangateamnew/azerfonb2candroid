package com.azarphone.bases

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import android.os.Bundle
import com.azarphone.IView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.lang.ref.WeakReference

open class BaseViewModel:ViewModel(), LifecycleObserver{

    @Volatile
    protected var mViewWeakReference: WeakReference<IView>?=null

    protected var compositeDisposables: CompositeDisposable?=null


    protected fun view(): IView? {
        val viewWeakReference = this.mViewWeakReference ?: return null
        return viewWeakReference.get()

    }


    fun onCreated(view: IView) {
        mViewWeakReference = WeakReference(view)
        if (compositeDisposables == null)
            compositeDisposables = CompositeDisposable()
        view.getLifecycle().addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun resume() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun pause() {
            compositeDisposables?.let {
                it.clear()
            }
    }


    fun onRestoreInstanceState(savedInstanceState: Bundle) {

    }

    fun onSaveInstanceState(outState: Bundle) {

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    internal fun cleanup() {
        val viewWeakReference = this.mViewWeakReference
        if (viewWeakReference != null) {
            val view = viewWeakReference.get()
            view?.lifecycle?.removeObserver(this)
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposables?.let {
            it.dispose()
        }
    }

    fun addDisposable(disposable: Disposable) {
        compositeDisposables?.let {
            it.add(disposable)
        }
    }



}