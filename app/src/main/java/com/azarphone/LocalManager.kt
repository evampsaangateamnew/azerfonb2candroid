package com.azarphone

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import android.content.res.Resources
import android.preference.PreferenceManager
import android.os.Build.VERSION_CODES.JELLY_BEAN_MR1
import android.os.Build.VERSION_CODES.N
import com.azarphone.util.isAtLeastVersion
import java.util.*
import android.icu.util.ULocale.getLanguage
import com.azarphone.util.ConstantsUtility


class LocalManager(var context: Context) {


    private lateinit var prefs: SharedPreferences

    companion object {
        private val LANGUAGE_KEY = "language_key"


        fun getLocale(res: Resources): Locale {
            val config = res.getConfiguration()
            return if (isAtLeastVersion(N)) config.getLocales().get(0) else config.locale
        }

    }

    init {
        prefs = PreferenceManager.getDefaultSharedPreferences(context)
    }

    fun setLocale(c: Context): Context {
        return updateResources(c, getLanguage())
    }

    fun setNewLocale(c: Context, language: String): Context {
        persistLanguage(language)
        return updateResources(c, language)
    }

    fun getLanguage(): String {
        var languageKeyWord = ConstantsUtility.LanguageKeywords.AZ
        if (prefs.getString(LANGUAGE_KEY, ConstantsUtility.LanguageKeywords.AZ) != null) {
            languageKeyWord = prefs.getString(LANGUAGE_KEY, ConstantsUtility.LanguageKeywords.AZ)!!
        }
        return languageKeyWord
    }


    fun getCurrentLanguageNumber(): String {
        var languageConstant = ""
        val language = ProjectApplication.mLocaleManager.getLanguage()

        if (language.equals(ConstantsUtility.LanguageKeywords.EN, ignoreCase = true)) {
            languageConstant = ConstantsUtility.LanguageKeywordConstants.EN
        } else if (language.equals(ConstantsUtility.LanguageKeywords.AZ, ignoreCase = true)) {
            languageConstant = ConstantsUtility.LanguageKeywordConstants.AZ
        } else if (language.equals(ConstantsUtility.LanguageKeywords.RU, ignoreCase = true)) {
            languageConstant = ConstantsUtility.LanguageKeywordConstants.RU
        } else {
            languageConstant = ConstantsUtility.LanguageKeywordConstants.AZ
        }

        return languageConstant
    }

    private fun persistLanguage(language: String) {
        prefs.edit().putString(LANGUAGE_KEY, language).commit()
    }

    private fun updateResources(context: Context, language: String): Context {
        var context = context
        val locale = Locale(language)
        Locale.setDefault(locale)

        val res = context.resources
        val config = Configuration(res.configuration)
        if (isAtLeastVersion(JELLY_BEAN_MR1)) {
            config.setLocale(locale)
            context = context.createConfigurationContext(config)
        } else {
            config.locale = locale
            res.updateConfiguration(config, res.displayMetrics)
        }
        return context
    }


}
