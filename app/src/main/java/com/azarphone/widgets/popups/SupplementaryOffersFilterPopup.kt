package com.azarphone.widgets.popups

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.*
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.response.supplementaryoffers.helper.OfferFilter
import com.azarphone.api.pojo.response.supplementaryoffers.helper.OfferFiltersMain
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.ui.adapters.recyclerviews.SupplementaryOffersFilterAdapter
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.RootValues
import com.azarphone.util.logE
import kotlinx.android.synthetic.main.dialog_filter_fragment.view.*
import java.util.ArrayList

/**
 * @author Junaid Hassan on 30, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class SupplementaryOffersFilterPopup : DialogFragment() {

    private var isFromRoaming: Boolean = false
    private val fromClass = "SupplementaryOffersFilterPopup"
    private var offerFiltersMain: OfferFiltersMain? = null
    private var externalView: View? = null
    private var appAdapter: SupplementaryOffersFilterAdapter? = null

    companion object {
        fun newInstance(offerFiltersMain: OfferFiltersMain, fromRoaming: Boolean): SupplementaryOffersFilterPopup {
            val frag = SupplementaryOffersFilterPopup()
            val args = Bundle()
            args.putParcelable(ConstantsUtility.PackagesConstants.OFFERS_FILTER, offerFiltersMain)
            args.putBoolean(ConstantsUtility.PackagesConstants.IS_FROM_ROAMING, fromRoaming)
            frag.arguments = args
            return frag
        }
    }//companion object ends

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        offerFiltersMain = arguments!!.getParcelable(ConstantsUtility.PackagesConstants.OFFERS_FILTER)
        isFromRoaming = arguments!!.getBoolean(ConstantsUtility.PackagesConstants.IS_FROM_ROAMING)
    }//onCreate ends

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_filter_fragment, container, true)
        externalView = view

        initUI()
        initUIListeners()
        initFiltersAdapter()
        return view
    }

    private fun getLocalizedResetTitle(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "Reset"
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Silmək"
            else -> "Сбросить"
        }
    }

    private fun getLocalizedApplyTitle(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "Apply"
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Tətbiq et"
            else -> "Применять"
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT)
        return dialog
    }

    private fun initFiltersAdapter() {
        if (offerFiltersMain != null) {
            logE("sizeoffilter", "size:::".plus(offerFiltersMain!!.app!!.size), fromClass, "showFilterAlert")
            appAdapter = SupplementaryOffersFilterAdapter(context!!, offerFiltersMain!!.app!! as ArrayList<OfferFilter?>)
            val mLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(context, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
            externalView!!.filterRecycler.layoutManager = mLayoutManager
            externalView!!.filterRecycler.adapter = appAdapter
        }
    }//initFiltersAdapter ends

    private fun initUIListeners() {
        externalView!!.resetTitle.setOnClickListener {
            UserDataManager.getSupplementaryFilterKeysList().clear()
            if (appAdapter != null) {
                appAdapter!!.notifyDataSetChanged()
            }
        }//externalView!!.resetTitle.setOnClickListener ends

        externalView!!.filterApplyButton.setOnClickListener {
            RootValues.getSupplementaryOffersFilterEvents().onFilterApply()
        }//externalView!!.filterApplyButton.setOnClickListener ends
    }//initUIListeners ends

    private fun initUI() {
        externalView!!.filterApplyButton.text = getLocalizedApplyTitle()
        externalView!!.resetTitle.text=getLocalizedResetTitle()
    }
}