package com.azarphone.widgets.popups

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.azarphone.R
import com.azarphone.api.pojo.response.predefinedata.CardType
import com.azarphone.ui.adapters.recyclerviews.TopUpCardTypeDialogAdapter
import com.azarphone.util.*

/**
 * @author Junaid Hassan on 11, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@SuppressLint("InflateParams")
inline fun showMessageDialog(context: Context, activity: Activity, title: String, message: String) {
    try {
        if (activity.isFinishing) return

        val dialogBuilder = AlertDialog.Builder(activity)
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val dialogView = inflater.inflate(R.layout.azer_popup_layout_new, null)
        dialogBuilder.setView(dialogView)

        val alertDialog = dialogBuilder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        if (alertDialog != null) {
            if (alertDialog.isShowing) {
                return
            }
        }
        alertDialog.show()

        val popupTitle = dialogView.findViewById(R.id.popupTitle) as TextView
        popupTitle.typeface = getALSNormalFont()
        val okButton = dialogView.findViewById(R.id.okButton) as Button

        okButton.text = getOkButtonLabel()

        popupTitle.text = message
        okButton.setOnClickListener {
            alertDialog.dismiss()
        }
    } catch (e: Exception) {
        logE("AzerPopup", e.toString(), "AzerFonPopups", "showMessageDialog")
    }//catch ends
}//show message dialog ends

fun showMessageDialogWithOkCallBack(context: Context, activity: Activity, title: String, message: String, onClickListener: OnClickListener) {
    try {
        if (activity.isFinishing) return

        val dialogBuilder = AlertDialog.Builder(activity)
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val dialogView = inflater.inflate(R.layout.azer_popup_layout_new, null)
        dialogBuilder.setView(dialogView)

        val alertDialog = dialogBuilder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        if (alertDialog != null) {
            if (alertDialog.isShowing) {
                return
            }
        }
        alertDialog.show()

        val popupTitle = dialogView.findViewById(R.id.popupTitle) as TextView
        popupTitle.typeface = getALSNormalFont()
        val okButton = dialogView.findViewById(R.id.okButton) as Button

        okButton.text = getOkButtonLabel()

        popupTitle.text = message
        okButton.setOnClickListener {
            onClickListener.onClick()
            alertDialog.dismiss()
        }
    } catch (e: Exception) {
        logE("AzerPopup", e.toString(), "AzerFonPopups", "showMessageDialog")
    }//catch ends
}//show message dialog ends


fun showConfirmationDialog(context: Context, activity: Activity, title: String, message: String, onClickListener: OnClickListener) {
    try {
        if (activity.isFinishing) return

        val dialogBuilder = AlertDialog.Builder(activity)
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val dialogView = inflater.inflate(R.layout.azer_ok_cancel_popup_layout_new, null)
        dialogBuilder.setView(dialogView)

        val alertDialog = dialogBuilder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        if (alertDialog != null) {
            if (alertDialog.isShowing) {
                return
            }
        }
        alertDialog.show()

        val popupTitle = dialogView.findViewById(R.id.popupTitle) as TextView
        popupTitle.typeface = getALSNormalFont()
        val okButton = dialogView.findViewById(R.id.okButton) as Button
        val cancelButton = dialogView.findViewById(R.id.cancelButton) as Button

        okButton.text = getOkButtonLabel()
        cancelButton.text = getCancelButtonLabel()

        popupTitle.text = message
        okButton.setOnClickListener {
            onClickListener.onClick()
            alertDialog.dismiss()
        }

        cancelButton.setOnClickListener {
            alertDialog.dismiss()
        }
    } catch (e: Exception) {
        logE("AzerPopup", e.toString(), "AzerFonPopups", "showConfirmationDialog")
    }//catch ends
}//show message dialog ends

fun showCardTypeSelectionDialog(context: Context, activity: Activity, title: String, cardTypes: List<CardType?>?, onCardTypeClicked: OnCardTypeClicked) {
    try {
        var selectedCardKey: Long = 0

        if (activity.isFinishing) return

        val dialogBuilder = AlertDialog.Builder(activity)
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val dialogView = inflater.inflate(R.layout.card_selection_popup_layout, null)
        dialogBuilder.setView(dialogView)

        val alertDialog = dialogBuilder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        if (alertDialog != null) {
            if (alertDialog.isShowing) {
                return
            }
        }
        alertDialog.show()

        val popupTitle = dialogView.findViewById(R.id.popupTitle) as TextView
        popupTitle.typeface = getALSNormalFont()
        val okButton = dialogView.findViewById(R.id.okButton) as Button
        val cancelButton = dialogView.findViewById(R.id.cancelButton) as Button
        val rvCardSelection = dialogView.findViewById(R.id.rvCardSelection) as RecyclerView

        okButton.text = getOkButtonLabel()
        cancelButton.text = getCancelButtonLabel()
        val topUpCardTypeDialogAdapter = TopUpCardTypeDialogAdapter(context, cardTypes as ArrayList<CardType?>, object : OnCardTypeClicked {
            override fun onCardClicked(cardKey: Long) {
                selectedCardKey = cardKey
            }
        })
        rvCardSelection.layoutManager = LinearLayoutManager(context)
        rvCardSelection.adapter = topUpCardTypeDialogAdapter

        popupTitle.text = title
        okButton.setOnClickListener {
            onCardTypeClicked.onCardClicked(selectedCardKey)
            alertDialog.dismiss()
        }

        cancelButton.setOnClickListener {
            alertDialog.dismiss()
        }

    } catch (e: Exception) {
        logE("AzerPopup", e.toString(), "AzerFonPopups", "showCardTypeSelectionDialog")
    }//catch ends
}//show message dialog ends