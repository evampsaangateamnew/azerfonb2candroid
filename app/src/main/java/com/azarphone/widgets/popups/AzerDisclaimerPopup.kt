package com.azarphone.widgets.popups

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.core.content.ContextCompat
import androidx.appcompat.widget.AppCompatCheckBox
import android.view.LayoutInflater
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.util.*

/**
 * @author Junaid Hassan on 29, June, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@SuppressLint("InflateParams")
inline fun showMessageDisclaimerDialog(context: Context, activity: Activity, title: String, message: String, screenKey: String) {

    try {
        if (activity.isFinishing) return

        val dialogBuilder = android.app.AlertDialog.Builder(activity)
        val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val dialogView = inflater.inflate(R.layout.azer_disclaimer_popup, null)
        val linearLayout = dialogView.findViewById(R.id.root_layout) as LinearLayout
        dialogBuilder.setView(dialogView)

        linearLayout.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorWhite))

        val alertDialog = dialogBuilder.create()
        alertDialog.show()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val okButton = dialogView.findViewById(R.id.btn_okay) as Button
        okButton.isSelected = true
        okButton.typeface = getALSBoldFont()
        val tvTitle = dialogView.findViewById(R.id.tv_dialog_title) as TextView
        tvTitle.text = message
        tvTitle.typeface = getALSNormalFont()

        val appCompatCheckBox = dialogView.findViewById(R.id.checkMarkIcon) as AppCompatCheckBox

        okButton.text = getOkButtonLabel()

        okButton.setOnClickListener {
            if (appCompatCheckBox.isChecked) {

                if (screenKey.equals("o", ignoreCase = true)) {
                    setDisclaimerCheckToLocalPreferences(false, "o")
                } else {
                    setDisclaimerCheckToLocalPreferences(false, "u")
                }
            }
            alertDialog.dismiss()
        }
    } catch (e: Exception) {
        logE("disclaimerDialog", "error:::".plus(e.toString()), "BakcellPopUpDialog", "ShowDisclaimerDialog")
    }

}