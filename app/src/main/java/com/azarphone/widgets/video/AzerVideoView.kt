package com.azarphone.widgets.video

import android.content.Context
import android.util.AttributeSet
import android.widget.VideoView

/**
 * @author Junaid Hassan on 20, June, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class AzerVideoView: VideoView{
    constructor(context: Context?) : super(context)

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle)

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        //		setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
        //Log.i("@@@@", "onMeasure");
        var width = getDefaultSize(this.width, widthMeasureSpec)
        var height = getDefaultSize(this.height, heightMeasureSpec)
        if (this.width > 0 && this.height > 0) {
            if (this.width * height > width * this.height) {
                //Log.i("@@@", "image too tall, correcting");
                height = width * this.height / this.width
            } else if (this.width * height < width * this.height) {
                //Log.i("@@@", "image too wide, correcting");
                width = height * this.width / this.height
            } else {
                //Log.i("@@@", "aspect ratio is correct: " +
                //width+"/"+height+"="+
                //mVideoWidth+"/"+mVideoHeight);
            }
        }
        //Log.i("@@@@@@@@@@", "setting size: " + width + 'x' + height);
        setMeasuredDimension(width, height)
    }
}