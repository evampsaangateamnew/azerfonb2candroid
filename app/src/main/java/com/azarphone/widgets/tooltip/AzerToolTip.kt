package com.azarphone.widgets.tooltip

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Rect
import android.graphics.drawable.BitmapDrawable
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.PopupWindow
import android.widget.RelativeLayout
import android.widget.TextView
import com.azarphone.R
import com.azarphone.util.logE

/**
 * @author Junaid Hassan on 14, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
@SuppressLint("InflateParams")
inline fun initiateErrorToolTip(context: Context, anchorView: View, errorDetail: String) {
    try {
        val pw: PopupWindow
        //We need to get the instance of the LayoutInflater, use the context of this activity
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        //Inflate the view from a predefined XML layout
        val layout = inflater.inflate(R.layout.tool_tip, null)
        // create a PopupWindow
        pw = PopupWindow(layout, RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT, true)

        pw.isOutsideTouchable = true
        pw.isFocusable = true
        pw.setBackgroundDrawable(BitmapDrawable())

        val errorToolTipText = layout.findViewById(R.id.errorToolTipText) as TextView
        errorToolTipText.text = errorDetail

        //get the location array
        val locationOfView = IntArray(2)
        //get the location of the anchor view on the screen
        anchorView.getLocationOnScreen(locationOfView)
        //get the rectangle
        val location = Rect()
        location.left = locationOfView[0] //using to show the tooltip on the left side
        location.top = locationOfView[1]
        location.right = location.left + anchorView.width
        location.bottom = location.top  //using to show the tooltip below the username field

        // display the popup
        pw.showAtLocation(
                anchorView,
                Gravity.BOTTOM and Gravity.START,
                location.left + context!!.resources.getDimensionPixelSize(R.dimen._50sdp),
                location.bottom + context!!.resources.getDimensionPixelSize(R.dimen._25sdp)
        )
    } catch (e: Exception) {
        logE("tooltip", e.toString(), "LoginFragment", "popup methods")
    }
}//initiate popup window ends here
