package com.azarphone.widgets.tariffs

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.azarphone.R

/**
 * @author Junaid Hassan on 15, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class TariffsDetailsPriceAttributesViewItem: FrameLayout {
    private var main: LinearLayout? = null
    private var detailPriceAttributeViewItemLayout: LinearLayout? = null
    private var detailPriceAttributeViewItemLabel: TextView? = null
    private var detailPriceAttributeItemValue: TextView? = null

    @SuppressLint("InflateParams")
    constructor(context: Context) : super(context) {
        main = LayoutInflater.from(context).inflate(R.layout.layout_detail_price_attributes_view_item, null) as LinearLayout

        detailPriceAttributeViewItemLayout = main!!.findViewById(R.id.detailPriceAttributeViewItemLayout) as LinearLayout
        detailPriceAttributeViewItemLabel = main!!.findViewById(R.id.detailPriceAttributeViewItemLabel) as TextView
        detailPriceAttributeItemValue = main!!.findViewById(R.id.detailPriceAttributeItemValue) as TextView

        this.addView(main)
    }//constructor ends

    fun getDetailPriceAttributeViewItemLayout(): LinearLayout {
        return detailPriceAttributeViewItemLayout!!
    }

    fun getDetailPriceAttributeViewItemLabel(): TextView {
        return detailPriceAttributeViewItemLabel!!
    }

    fun getDetailPriceAttributeItemValue(): TextView {
        return detailPriceAttributeItemValue!!
    }

}