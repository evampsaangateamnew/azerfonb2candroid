package com.azarphone.widgets.tariffs

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.azarphone.R

/**
 * @author Junaid Hassan on 15, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class TariffsDetailsPriceViewItem : FrameLayout {
    private var main: LinearLayout? = null
    private var detailsPriceViewItemLayout: LinearLayout? = null
    private var detailsContentLayout: LinearLayout? = null
    private var detailsHeaderLabel: TextView? = null
    private var detailsHeaderValue: TextView? = null
    private var detailsPriceIcon: ImageView? = null
    private var viewBelowPriceDetailsContentLayoutLayout: View?=null

    @SuppressLint("InflateParams")
    constructor(context: Context) : super(context) {
        main = LayoutInflater.from(context).inflate(R.layout.layout_tariffs_details_prices_view_item, null) as LinearLayout

        detailsPriceViewItemLayout = main!!.findViewById(R.id.detailsPriceViewItemLayout) as LinearLayout
        detailsContentLayout = main!!.findViewById(R.id.detailsContentLayout) as LinearLayout
        detailsHeaderLabel = main!!.findViewById(R.id.detailsHeaderLabel) as TextView
        detailsHeaderValue = main!!.findViewById(R.id.detailsHeaderValue) as TextView
        detailsPriceIcon = main!!.findViewById(R.id.detailsPriceIcon) as ImageView
        viewBelowPriceDetailsContentLayoutLayout = main!!.findViewById(R.id.viewBelowPriceDetailsContentLayoutLayout) as View

        this.addView(main)
    }//constructor ends

    fun getViewBelowPriceDetailsContentLayoutLayout(): View {
        return viewBelowPriceDetailsContentLayoutLayout!!
    }

    fun getDetailsPriceViewItemLayout(): LinearLayout {
        return detailsPriceViewItemLayout!!
    }

    fun getDetailsHeaderLabel(): TextView {
        return detailsHeaderLabel!!
    }

    fun getDetailsHeaderValue(): TextView {
        return detailsHeaderValue!!
    }

    fun getDetailsPriceIcon(): ImageView {
        return detailsPriceIcon!!
    }

    fun getDetailsContentLayout():LinearLayout{
        return detailsContentLayout!!
    }

}