package com.azarphone.widgets.tariffs

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.azarphone.R

/**
 * @author Junaid Hassan on 13, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class TariffsSMSPayGViewItem: FrameLayout {
    private var main: LinearLayout? = null
    private var smspaygViewItemLayout: LinearLayout? = null
    private var smspaygViewItemLabel: TextView? = null
    private var smspaygViewItemValue: TextView? = null

    @SuppressLint("InflateParams")
    constructor(context: Context) : super(context) {
        main = LayoutInflater.from(context).inflate(R.layout.layout_tariffs_sms_payg_view_item, null) as LinearLayout

        smspaygViewItemLayout = main!!.findViewById(R.id.smspaygViewItemLayout) as LinearLayout
        smspaygViewItemLabel = main!!.findViewById(R.id.smspaygViewItemLabel) as TextView
        smspaygViewItemValue = main!!.findViewById(R.id.smspaygViewItemValue) as TextView

        this.addView(main)
    }//constructor ends

    fun getSmspaygViewItemLayout(): LinearLayout {
        return smspaygViewItemLayout!!
    }

    fun getSmspaygViewItemLabel(): TextView {
        return smspaygViewItemLabel!!
    }

    fun getSmspaygViewItemValue(): TextView {
        return smspaygViewItemValue!!
    }

}
