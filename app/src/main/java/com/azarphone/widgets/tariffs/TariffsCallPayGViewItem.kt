package com.azarphone.widgets.tariffs

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.azarphone.R

/**
 * @author Junaid Hassan on 13, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class TariffsCallPayGViewItem : FrameLayout {
    private var main: LinearLayout? = null
    private var callpaygViewItemLayout: LinearLayout? = null
    private var callpaygViewItemLabel: TextView? = null
    private var callpaygViewItemValueLeft: TextView? = null
    private var callpaygViewItemValueRight: TextView? = null
    private var colValueLeft: LinearLayout? = null

    @SuppressLint("InflateParams")
    constructor(context: Context) : super(context) {
        main = LayoutInflater.from(context).inflate(R.layout.layout_tariffs_call_payg_view_item, null) as LinearLayout

        callpaygViewItemLayout = main!!.findViewById(R.id.callpaygViewItemLayout) as LinearLayout
        callpaygViewItemLabel = main!!.findViewById(R.id.callpaygViewItemLabel) as TextView
        callpaygViewItemValueRight = main!!.findViewById(R.id.callpaygViewItemValueRight) as TextView
        callpaygViewItemValueLeft = main!!.findViewById(R.id.callpaygViewItemValueLeft) as TextView
        colValueLeft = main!!.findViewById(R.id.colValueLeft) as LinearLayout

        this.addView(main)
    }//constructor ends

    fun getColValueLeft(): LinearLayout {
        return colValueLeft!!
    }

    fun getCallpaygViewItemLayout(): LinearLayout {
        return callpaygViewItemLayout!!
    }

    fun getCallpaygViewItemLabel(): TextView {
        return callpaygViewItemLabel!!
    }

    fun getCallpaygViewItemValueLeft(): TextView {
        return callpaygViewItemValueLeft!!
    }

    fun getCallpaygViewItemValueRight(): TextView {
        return callpaygViewItemValueRight!!
    }

}