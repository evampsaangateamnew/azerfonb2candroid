package com.azarphone.widgets.tariffs

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import com.azarphone.R

/**
 * @author Junaid Hassan on 24, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class TariffsInternetPayGViewItem: FrameLayout {
    private var main: LinearLayout? = null
    private var internetViewItemLayout: LinearLayout? = null
    private var internetViewItemLabel: TextView? = null
    private var internetViewItemValue: TextView? = null

    @SuppressLint("InflateParams")
    constructor(context: Context) : super(context) {
        main = LayoutInflater.from(context).inflate(R.layout.layout_tariffs_internet_payg_view_item, null) as LinearLayout

        internetViewItemLayout = main!!.findViewById(R.id.internetViewItemLayout) as LinearLayout
        internetViewItemLabel = main!!.findViewById(R.id.internetViewItemLabel) as TextView
        internetViewItemValue = main!!.findViewById(R.id.internetViewItemValue) as TextView

        this.addView(main)
    }//constructor ends

    fun getInternetViewItemLayout(): LinearLayout {
        return internetViewItemLayout!!
    }

    fun getInternetViewItemLabel(): TextView {
        return internetViewItemLabel!!
    }

    fun getInternetViewItemValue(): TextView {
        return internetViewItemValue!!
    }

}