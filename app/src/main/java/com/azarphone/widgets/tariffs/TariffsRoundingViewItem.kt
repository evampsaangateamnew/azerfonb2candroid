package com.azarphone.widgets.tariffs

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.azarphone.R

/**
 * @author Junaid Hassan on 15, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class TariffsRoundingViewItem: FrameLayout {
    private var main: LinearLayout? = null
    private var roundingViewItemLayout: LinearLayout? = null
    private var roundingViewItemLabel: TextView? = null
    private var roundingViewItemValue: TextView? = null

    @SuppressLint("InflateParams")
    constructor(context: Context) : super(context) {
        main = LayoutInflater.from(context).inflate(R.layout.layout_tariffs_rounding_view_item, null) as LinearLayout

        roundingViewItemLayout = main!!.findViewById(R.id.roundingViewItemLayout) as LinearLayout
        roundingViewItemLabel = main!!.findViewById(R.id.roundingViewItemLabel) as TextView
        roundingViewItemValue = main!!.findViewById(R.id.roundingViewItemValue) as TextView

        this.addView(main)
    }//constructor ends

    fun getRoundingViewItemLayout(): LinearLayout {
        return roundingViewItemLayout!!
    }

    fun getRoundingViewItemLabel(): TextView {
        return roundingViewItemLabel!!
    }

    fun getRoundingViewItemValue(): TextView {
        return roundingViewItemValue!!
    }

}