package com.azarphone.widgets.tariffs

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.azarphone.R

/**
 * @author Junaid Hassan on 17, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class TariffsRoamingViewItem: FrameLayout {
    private var main: LinearLayout? = null
    private var detailsRoamingViewItemLayout: LinearLayout? = null
    private var roamingOperatorsLayout: LinearLayout? = null
    private var detailsRoamingCountryLabel: TextView? = null
    private var detailsRoamingCountryFlag: ImageView? = null

    @SuppressLint("InflateParams")
    constructor(context: Context) : super(context) {
        main = LayoutInflater.from(context).inflate(R.layout.layout_tariffs_roaming_view_item, null) as LinearLayout

        detailsRoamingViewItemLayout = main!!.findViewById(R.id.detailsRoamingViewItemLayout) as LinearLayout
        roamingOperatorsLayout = main!!.findViewById(R.id.roamingOperatorsLayout) as LinearLayout
        detailsRoamingCountryLabel = main!!.findViewById(R.id.detailsRoamingCountryLabel) as TextView
        detailsRoamingCountryFlag = main!!.findViewById(R.id.detailsRoamingCountryFlag) as ImageView

        this.addView(main)
    }//constructor ends

    fun getDetailsRoamingCountryLabel(): TextView {
        return detailsRoamingCountryLabel!!
    }

    fun getDetailsRoamingCountryFlag(): ImageView {
        return detailsRoamingCountryFlag!!
    }

    fun getroamingOperatorsLayout(): LinearLayout {
        return roamingOperatorsLayout!!
    }

}