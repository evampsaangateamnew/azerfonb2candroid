package com.azarphone.widgets.tariffs

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.azarphone.R

/**
 * @author Junaid Hassan on 13, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class TariffsSMSViewItem : FrameLayout {
    private var main: LinearLayout? = null
    private var smsViewItemLayout: LinearLayout? = null
    private var smsViewItemLabel: TextView? = null
    private var smsViewItemValue: TextView? = null

    @SuppressLint("InflateParams")
    constructor(context: Context) : super(context) {
        main = LayoutInflater.from(context).inflate(R.layout.layout_tariffs_sms_view_item, null) as LinearLayout

        smsViewItemLayout = main!!.findViewById(R.id.smsViewItemLayout) as LinearLayout
        smsViewItemLabel = main!!.findViewById(R.id.smsViewItemLabel) as TextView
        smsViewItemValue = main!!.findViewById(R.id.smsViewItemValue) as TextView

        this.addView(main)
    }//constructor ends

    fun getsmsViewItemLayout(): LinearLayout {
        return smsViewItemLayout!!
    }

    fun getSMSViewItemLabel(): TextView {
        return smsViewItemLabel!!
    }

    fun getSMSViewItemValue(): TextView {
        return smsViewItemValue!!
    }

}