package com.azarphone.widgets.tariffs

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.azarphone.R

/**
 * @author Junaid Hassan on 13, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class TariffsCallViewItem : FrameLayout {
    private var main: LinearLayout? = null
    private var callViewItemLayout: LinearLayout? = null
    private var callViewItemLabel: TextView? = null
    private var callViewItemValueRight: TextView? = null
    private var callViewItemValueLeft: TextView? = null
    private var colValueLeft: LinearLayout? = null

    @SuppressLint("InflateParams")
    constructor(context: Context) : super(context) {
        main = LayoutInflater.from(context).inflate(R.layout.layout_tariffs_call_view_item, null) as LinearLayout

        callViewItemLayout = main!!.findViewById(R.id.callViewItemLayout) as LinearLayout
        callViewItemLabel = main!!.findViewById(R.id.callViewItemLabel) as TextView
        callViewItemValueRight = main!!.findViewById(R.id.callViewItemValueRight) as TextView
        callViewItemValueLeft = main!!.findViewById(R.id.callViewItemValueLeft) as TextView
        colValueLeft = main!!.findViewById(R.id.colValueLeft) as LinearLayout
        this.addView(main)
    }//constructor ends

    fun getColValueLeft(): LinearLayout {
        return colValueLeft!!
    }

    fun getCallViewItemLayout(): LinearLayout {
        return callViewItemLayout!!
    }

    fun getCallViewItemLabel(): TextView {
        return callViewItemLabel!!
    }

    fun getCallViewItemValueLeft(): TextView {
        return callViewItemValueLeft!!
    }

    fun getCallViewItemValueRight(): TextView {
        return callViewItemValueRight!!
    }

}