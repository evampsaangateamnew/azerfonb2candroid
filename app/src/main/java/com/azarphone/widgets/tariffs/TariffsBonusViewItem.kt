package com.azarphone.widgets.tariffs

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.azarphone.R

/**
 * @author Junaid Hassan on 12, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class TariffsBonusViewItem : FrameLayout {
    private var main: LinearLayout? = null
    private var bonusViewItemLayout: LinearLayout? = null
    private var bonusViewItemIcon: ImageView? = null
    private var bonusViewItemLabel: TextView? = null
    private var bonusViewItemValue: TextView? = null
    private var bottomView: View? = null

    @SuppressLint("InflateParams")
    constructor(context: Context) : super(context) {
        main = LayoutInflater.from(context).inflate(R.layout.layout_bonus_tariff_view_item, null) as LinearLayout

        bonusViewItemLayout = main!!.findViewById(R.id.bonusViewItemLayout) as LinearLayout
        bonusViewItemIcon = main!!.findViewById(R.id.bonusViewItemIcon) as ImageView
        bonusViewItemLabel = main!!.findViewById(R.id.bonusViewItemLabel) as TextView
        bonusViewItemValue = main!!.findViewById(R.id.bonusViewItemValue) as TextView
        bottomView = main!!.findViewById(R.id.bottomView) as View

        this.addView(main)
    }//constructor ends

    fun getBonusBottomView(): View {
        return bottomView!!
    }

    fun getBonusViewItemLabel(): TextView {
        return bonusViewItemLabel!!
    }

    fun getBonusViewItemIcon(): ImageView {
        return bonusViewItemIcon!!
    }

    fun getBonusViewItemValue(): TextView {
        return bonusViewItemValue!!
    }

}