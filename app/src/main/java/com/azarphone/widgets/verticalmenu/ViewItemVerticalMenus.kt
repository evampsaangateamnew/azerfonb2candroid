package com.azarphone.widgets.verticalmenu

import android.content.Context
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.azarphone.R
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.RootValues
import com.azarphone.validators.hasValue

/**
 * @author Junaid Hassan on 18, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class ViewItemVerticalMenus : FrameLayout {
    private var verticalMenuItemLayout: LinearLayout? = null
    private var main: LinearLayout? = null
    private var verticalMenuIcon: ImageView? = null
    private var verticalMenuTitle: TextView? = null
    private var menuKey: TextView? = null

    constructor(context: Context) : super(context) {
        main = LayoutInflater.from(context).inflate(R.layout.item_vertical_menu, null) as LinearLayout

        menuKey = main!!.findViewById(R.id.menuKey) as TextView
        verticalMenuItemLayout = main!!.findViewById(R.id.verticalMenuItemLayout) as LinearLayout
        verticalMenuIcon = main!!.findViewById(R.id.verticalMenuIcon) as ImageView
        verticalMenuTitle = main!!.findViewById(R.id.verticalMenuTitle) as TextView

        //click listener for the vertical menu layout holder
        verticalMenuItemLayout!!.setOnClickListener {
            var key = menuKey!!.text.toString()
            if (!hasValue(key)) {
                //aka default value
                key = ""
            }

            if (key.equals(ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_NOTIFICATIONS, ignoreCase = true)) {
                key = ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_NOTIFICATIONS
            }

            if (key.equals(ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_STORE_LOCATOR, ignoreCase = true)) {
                key = ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_STORE_LOCATOR
            }

            if (key.equals(ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_FAQ, ignoreCase = true)) {
                key = ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_FAQ
            }

            if (key.equals(ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_TUTORIAL_AND_FAQ, ignoreCase = true)) {
                key = ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_TUTORIAL_AND_FAQ
            }

            if (key.equals(ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_CONTACT_US, ignoreCase = true)) {
                key = ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_CONTACT_US
            }

            if (key.equals(ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_TERMS_AND_CONDITIONS, ignoreCase = true)) {
                key = ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_TERMS_AND_CONDITIONS
            }

            if (key.equals(ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_LIVE_CHAT, ignoreCase = true)) {
                key = ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_LIVE_CHAT
            }

            if (key.equals(ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_SETTINGS, ignoreCase = true)) {
                key = ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_SETTINGS
            }

            if (key.equals(ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_LOG_OUT, ignoreCase = true)) {
                key = ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_LOG_OUT
            }

            if (key.equals(ConstantsUtility.VerticalMenuIdentifiers.DEFAULT_LOG_OUT_KEY, ignoreCase = true)) {
                key = ConstantsUtility.VerticalMenuIdentifiers.DEFAULT_LOG_OUT_KEY
            }
            //call the listener for loading corresponding fragment screen
            RootValues.getVerticalMenuClickListener().onVerticalMenuClicked(key)
        }//verticalMenuItemLayout!!.setOnClickListener ends

        this.addView(main)
    }//constructor ends

    fun getVerticalMenuItemLayout(): LinearLayout {
        return verticalMenuItemLayout!!
    }

    fun getVerticalMenuKey(): TextView {
        return menuKey!!
    }

    fun verticalMenuIcon(): ImageView {
        return verticalMenuIcon!!
    }

    fun getVerticalMenuItemTitle(): TextView {
        return verticalMenuTitle!!
    }
}