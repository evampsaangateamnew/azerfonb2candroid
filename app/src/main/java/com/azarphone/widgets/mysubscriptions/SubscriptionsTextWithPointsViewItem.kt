package com.azarphone.widgets.mysubscriptions

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.azarphone.R

/**
 * @author Junaid Hassan on 29, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class SubscriptionsTextWithPointsViewItem: FrameLayout {
    private var main: LinearLayout? = null
    private var textWithPointsViewItemLayout: LinearLayout? = null
    private var textWithPointsbullet: ImageView? = null
    private var textWithPointsViewItemValue: TextView? = null

    @SuppressLint("InflateParams")
    constructor(context: Context) : super(context) {
        main = LayoutInflater.from(context).inflate(R.layout.layout_subscriptions_text_points_view_item, null) as LinearLayout

        textWithPointsViewItemLayout = main!!.findViewById(R.id.textWithPointsViewItemLayout) as LinearLayout
        textWithPointsbullet = main!!.findViewById(R.id.textWithPointsbullet) as ImageView
        textWithPointsViewItemValue = main!!.findViewById(R.id.textWithPointsViewItemValue) as TextView

        this.addView(main)
    }//constructor ends

    fun getTextWithPointsViewItemLayout(): LinearLayout {
        return textWithPointsViewItemLayout!!
    }

    fun getTextWithPointsViewItemValue(): TextView {
        return textWithPointsViewItemValue!!
    }

}