package com.azarphone.widgets.mysubscriptions

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import com.azarphone.R

/**
 * @author Junaid Hassan on 29, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class SubscriptionsRoundingViewItem: FrameLayout {
    private var main: LinearLayout? = null
    private var roundingViewItemLayout: LinearLayout? = null
    private var roundingViewItemLabel: TextView? = null
    private var roundingViewItemValue: TextView? = null

    @SuppressLint("InflateParams")
    constructor(context: Context) : super(context) {
        main = LayoutInflater.from(context).inflate(R.layout.layout_subscriptions_rounding_view_item, null) as LinearLayout

        roundingViewItemLayout = main!!.findViewById(R.id.roundingViewItemLayout) as LinearLayout
        roundingViewItemLabel = main!!.findViewById(R.id.roundingViewItemLabel) as TextView
        roundingViewItemValue = main!!.findViewById(R.id.roundingViewItemValue) as TextView

        this.addView(main)
    }//constructor ends

    fun getRoundingViewItemLayout(): LinearLayout {
        return roundingViewItemLayout!!
    }

    fun getRoundingViewItemLabel(): TextView {
        return roundingViewItemLabel!!
    }

    fun getRoundingViewItemValue(): TextView {
        return roundingViewItemValue!!
    }

}