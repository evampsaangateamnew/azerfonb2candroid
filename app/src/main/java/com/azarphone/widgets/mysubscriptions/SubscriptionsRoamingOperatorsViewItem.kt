package com.azarphone.widgets.mysubscriptions

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import com.azarphone.R

/**
 * @author Junaid Hassan on 29, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class SubscriptionsRoamingOperatorsViewItem: FrameLayout {
    private var main: LinearLayout? = null
    private var roamingOperatorViewItemLayout: LinearLayout? = null
    private var roamingOperatorViewItemValue: TextView? = null

    @SuppressLint("InflateParams")
    constructor(context: Context) : super(context) {
        main = LayoutInflater.from(context).inflate(R.layout.layout_subscriptions_roaming_operators_view_item, null) as LinearLayout

        roamingOperatorViewItemLayout = main!!.findViewById(R.id.roamingOperatorViewItemLayout) as LinearLayout
        roamingOperatorViewItemValue = main!!.findViewById(R.id.roamingOperatorViewItemValue) as TextView

        this.addView(main)
    }//constructor ends

    fun getRoamingOperatorViewItemValue(): TextView {
        return roamingOperatorViewItemValue!!
    }

}