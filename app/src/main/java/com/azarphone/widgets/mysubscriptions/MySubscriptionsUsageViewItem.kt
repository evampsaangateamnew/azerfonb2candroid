package com.azarphone.widgets.mysubscriptions

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.azarphone.R
import com.azarphone.util.getALSBoldFont

/**
 * @author Junaid Hassan on 29, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class MySubscriptionsUsageViewItem : FrameLayout {
    private var main: LinearLayout? = null
    private var usageViewItemLayout: LinearLayout? = null

    private var usageProgressLayout: LinearLayout? = null
    private var titleImageIcon: ImageView? = null
    private var titleLeftAboveUsageProgress: TextView? = null
    private var titleRightAboveUsageProgress: TextView? = null
    private var viewUnderUsageProgress: View? = null
    private var usageProgressBar: ProgressBar? = null
    private var titleLeftBelowUsageProgress: TextView? = null
    private var titleRightBelowUsageProgress: TextView? = null

//    private var viewBetweenUsageRenewalProgressLayout: View? = null

    private var renewalProgressLayout: LinearLayout? = null
    private var titleLeftAboveRenewalProgress: TextView? = null
    private var titleRightAboveRenewalProgress: TextView? = null
    private var viewUnderRenewalProgress: View? = null
    private var renewalProgressBar: ProgressBar? = null
    private var titleLeftBelowRenewalProgress: TextView? = null
    private var titleRightBelowRenewalProgress: TextView? = null
    private var viewBelowBottom: View? = null

    @SuppressLint("InflateParams")
    constructor(context: Context) : super(context) {
        main = LayoutInflater.from(context).inflate(R.layout.layout_subscriptions_usage_view_item, null) as LinearLayout

        usageViewItemLayout = main!!.findViewById(R.id.usageViewItemLayout) as LinearLayout
        titleImageIcon = main!!.findViewById(R.id.titleImageIcon) as ImageView
        usageProgressLayout = main!!.findViewById(R.id.usageProgressLayout) as LinearLayout
        titleLeftAboveUsageProgress = main!!.findViewById(R.id.titleLeftAboveUsageProgress) as TextView
        titleRightAboveUsageProgress = main!!.findViewById(R.id.titleRightAboveUsageProgress) as TextView
        viewUnderUsageProgress = main!!.findViewById(R.id.viewUnderUsageProgress) as View
        usageProgressBar = main!!.findViewById(R.id.usageProgressBar) as ProgressBar
        titleLeftBelowUsageProgress = main!!.findViewById(R.id.titleLeftBelowUsageProgress) as TextView
        titleRightBelowUsageProgress = main!!.findViewById(R.id.titleRightBelowUsageProgress) as TextView
        viewBelowBottom = main!!.findViewById(R.id.viewBelowBottom) as View
//        viewBetweenUsageRenewalProgressLayout = main!!.findViewById(R.id.viewBetweenUsageRenewalProgressLayout) as View

        renewalProgressLayout = main!!.findViewById(R.id.renewalProgressLayout) as LinearLayout
        titleLeftAboveRenewalProgress = main!!.findViewById(R.id.titleLeftAboveRenewalProgress) as TextView
        titleRightAboveRenewalProgress = main!!.findViewById(R.id.titleRightAboveRenewalProgress) as TextView
        viewUnderRenewalProgress = main!!.findViewById(R.id.viewUnderRenewalProgress) as View
        renewalProgressBar = main!!.findViewById(R.id.renewalProgressBar) as ProgressBar
        titleLeftBelowRenewalProgress = main!!.findViewById(R.id.titleLeftBelowRenewalProgress) as TextView
        titleRightBelowRenewalProgress = main!!.findViewById(R.id.titleRightBelowRenewalProgress) as TextView

        titleLeftAboveRenewalProgress!!.typeface= getALSBoldFont()

        this.addView(main)
    }//constructor ends

    fun getViewBelowBottom(): View {
        return viewBelowBottom!!
    }

    fun getTitleRightBelowRenewalProgress(): TextView {
        return titleRightBelowRenewalProgress!!
    }

    fun getTitleLeftBelowRenewalProgress(): TextView {
        return titleLeftBelowRenewalProgress!!
    }

    fun getRenewalProgressBar(): ProgressBar {
        return renewalProgressBar!!
    }

    fun getTitleRightAboveRenewalProgress(): TextView {
        return titleRightAboveRenewalProgress!!
    }

    fun getTitleLeftAboveRenewalProgress(): TextView {
        return titleLeftAboveRenewalProgress!!
    }

    fun getRenewalProgressLayout(): LinearLayout {
        return renewalProgressLayout!!
    }

//    fun getViewBetweenUsageRenewalProgressLayout(): View {
//        return viewBetweenUsageRenewalProgressLayout!!
//    }

    fun getUsageProgressLayout(): LinearLayout {
        return usageProgressLayout!!
    }

    fun getTitleLeftAboveUsageProgress(): TextView {
        return titleLeftAboveUsageProgress!!
    }

    fun getTitleRightAboveUsageProgress(): TextView {
        return titleRightAboveUsageProgress!!
    }

    fun getUsageProgressBar(): ProgressBar {
        return usageProgressBar!!
    }

    fun getTitleLeftBelowUsageProgress(): TextView {
        return titleLeftBelowUsageProgress!!
    }

    fun getUsageViewItemLayout(): LinearLayout {
        return usageViewItemLayout!!
    }

    fun getTitleImageIcon(): ImageView {
        return titleImageIcon!!
    }

    fun getTitleRightBelowUsageProgress(): TextView {
        return titleRightBelowUsageProgress!!
    }

}