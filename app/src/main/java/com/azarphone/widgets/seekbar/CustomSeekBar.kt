package com.azarphone.widgets.seekbar

import android.content.Context
import android.os.Build
import androidx.appcompat.view.ContextThemeWrapper
import androidx.appcompat.widget.AppCompatSeekBar
import android.text.TextUtils
import android.view.Gravity
import android.widget.LinearLayout
import android.widget.SeekBar
import android.widget.TextView
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.response.exchangeservicesvaluesresponse.Data
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.getALSBoldFont
import com.azarphone.util.getALSNormalFont


class CustomSeekBar(internal var mContext: Context, internal var maxCount: Int, internal var textColor: Int, val data: Data?, val voiceToData: Boolean) {
    internal lateinit var mSeekLinAbove: LinearLayout
    internal lateinit var mSeekLinBelow: LinearLayout
    internal lateinit var mSeekBar: SeekBar
    var dataValue = ""
    var voiceValue = ""

    fun addSeekBar(parent: LinearLayout) {

        if (parent is LinearLayout) {

            parent.orientation = LinearLayout.VERTICAL
            val newContext = ContextThemeWrapper(mContext, R.style.MySeekBar)
            mSeekBar = AppCompatSeekBar(newContext)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                mSeekBar.tickMark = mContext.getDrawable(R.drawable.fill_10)
            }
            mSeekBar.thumb = mContext.getDrawable(R.drawable.fill_7)
            //    mSeekBar.getProgressDrawable().setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);
            mSeekBar.progress = 1
            dataValue = data?.datavalues?.get(0)?.data.toString()
            voiceValue = data?.voicevalues?.get(0)?.voice.toString()

            mSeekBar.max = maxCount - 1

            // Add LinearLayout for labels below SeekBar
            mSeekLinBelow = LinearLayout(mContext)
            mSeekLinBelow.orientation = LinearLayout.HORIZONTAL
            mSeekLinBelow.setPadding(10, 0, 10, 0)
            val params = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(35, 10, 0, 0)
            mSeekLinBelow.layoutParams = params

            // Add LinearLayout for labels Above SeekBar
            mSeekLinAbove = LinearLayout(mContext)
            mSeekLinAbove.orientation = LinearLayout.HORIZONTAL
            mSeekLinAbove.setPadding(10, 0, 10, 0)
            val params2 = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            )
            params2.setMargins(35, 0, 0, 10)
            mSeekLinAbove.layoutParams = params2

            addLabelsBelowSeekBar()
            addLabelsAboveSeekBar()

            parent.addView(mSeekLinAbove)
            parent.addView(mSeekBar)
            parent.addView(mSeekLinBelow)

        } else {

        }

        mSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                // aBovetextView.id==progress

                if (progress == 0) {
                    mSeekBar.progress = 1

                } else {

                    setTextSize(progress)
                    dataValue = data?.datavalues?.get(progress - 1)?.data.toString()
                    voiceValue = data?.voicevalues?.get(progress - 1)?.voice.toString()
                }

                //Log.d("ID",mSeekLinBelow.getChildAt(progress) as TextView)
                //Log.d("Progress", progress.toString())

            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}

            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })


    }

    private fun setTextSize(i: Int) {

        for (count in 1 until maxCount) {
            val tv_Below = mSeekLinBelow.getChildAt(count) as TextView
            val tv_Above = mSeekLinAbove.getChildAt(count) as TextView
            if (i == count) {
                tv_Below.textSize = 15f
                tv_Above.textSize = 15f
            } else {
                tv_Below.textSize = 10f
                tv_Above.textSize = 10f
            }
        }
    }

    private fun getLocalizedYouGetTitle(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "You get"
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Al"
            else -> "Получи"
        }
    }

    private fun addLabelsBelowSeekBar() {
        for (count in 0 until maxCount) {
            val textView = TextView(mContext)
            if (count == 0) {
                textView.typeface = getALSNormalFont()
                textView.text = getLocalizedYouGetTitle()
            } else {
                textView.typeface = getALSBoldFont()
                if (voiceToData) {
                    textView.text = (data?.voicevalues?.get(count - 1)?.data + "\n" + data?.voicevalues?.get(count - 1)?.dataUnit)
                } else {
                    textView.text = data?.datavalues?.get(count - 1)?.voice + "\n" + data?.datavalues?.get(count - 1)?.voiceUnit
                }
            }
            textView.setTextColor(textColor)
            if (count == 1) {
                textView.textSize = 15f
            } else {
                textView.textSize = 10f
            }
            textView.id = count + 1
            // textView.gravity = Gravity.START
            mSeekLinBelow.addView(textView)
            if (count == 0) {
                val layoutParams = textView.layoutParams
                layoutParams.width = mContext.resources.getDimensionPixelSize(R.dimen._42sdp)
                layoutParams.height = LinearLayout.LayoutParams.MATCH_PARENT
                /*  layoutParams.width =150*/
                textView.ellipsize = TextUtils.TruncateAt.MARQUEE
                textView.isSelected = true
                textView.marqueeRepeatLimit = -1
                textView.setSingleLine(true)
                textView.layoutParams = layoutParams

                textView.gravity = Gravity.CENTER_VERTICAL
            } else {
                textView.layoutParams = if (count == maxCount - 1) getLayoutParams(0.0f) else getLayoutParams(1.0f)
                textView.gravity = Gravity.START
            }
        }
    }

    private fun getLocalizedForTitle(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "For"
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Ver"
            else -> "За"
        }
    }

    private fun addLabelsAboveSeekBar() {
        for (count in 0 until maxCount) {
            val textView = TextView(mContext)

            if (count == 0) {
                textView.typeface = getALSNormalFont()
                textView.text = getLocalizedForTitle()
            } else {
                textView.typeface = getALSBoldFont()
                if (voiceToData) {
                    textView.text = (data?.voicevalues?.get(count - 1)?.voice + "\n" + data?.voicevalues?.get(count - 1)?.voiceUnit)
                } else {
                    textView.text = data?.datavalues?.get(count - 1)?.data + "\n" + data?.datavalues?.get(count - 1)?.dataUnit
                }
            }
            textView.setTextColor(textColor)
            if (count == 1) {
                textView.textSize = 15f
            } else {
                textView.textSize = 10f
            }
            //textView.gravity = Gravity.START

            mSeekLinAbove.addView(textView)
            /* textView.layoutParams = if (count == maxCount - 1) getLayoutParams(0.0f) else getLayoutParams(1.0f)*/
            if (count == 0) {
                val layoutParams = textView.layoutParams
                layoutParams.width = mContext.resources.getDimensionPixelSize(R.dimen._43sdp)
                layoutParams.height = LinearLayout.LayoutParams.MATCH_PARENT
                /*  layoutParams.width =150*/
                textView.ellipsize = TextUtils.TruncateAt.MARQUEE
                textView.isSelected = true
                textView.marqueeRepeatLimit = -1
                textView.setSingleLine(true)
                textView.layoutParams = layoutParams

                textView.gravity = Gravity.CENTER_VERTICAL
            } else {
                textView.layoutParams = if (count == maxCount - 1) getLayoutParams(0.0f) else getLayoutParams(1.0f)

                textView.gravity = Gravity.START
            }
        }
    }

    internal fun getLayoutParams(weight: Float): LinearLayout.LayoutParams {
        return LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, weight)
    }


}