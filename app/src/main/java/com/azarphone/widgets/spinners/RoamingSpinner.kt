package com.azarphone.widgets.spinners

import android.content.Context
import android.util.AttributeSet
import android.widget.Spinner

/**
 * @author Junaid Hassan on 27, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
open class RoamingSpinner: Spinner{

    constructor(context: Context?) : super(context)

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle)

    public override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
    }
}