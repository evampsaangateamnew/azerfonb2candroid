package com.azarphone.widgets.coreservices

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.widget.SwitchCompat
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import com.azarphone.R
import com.azarphone.eventhandler.CoreServiceProcessEvents
import com.azarphone.eventhandler.CoreServicesCallDivertDisableEvents
import com.azarphone.ui.activities.vasservices.CoreServicesNumberForwardActivity
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import kotlinx.android.synthetic.main.activity_data_with_out_pack.*

/**
 * @author Junaid Hassan on 02, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class CoreServicesViewItem : FrameLayout {
    private val fromClass = "CoreServicesViewItem"
    private var main: LinearLayout? = null
    private var coreServiceNameLabel: TextView? = null
    private var forwardToNumberLabel: TextView? = null
    private var coreServicePriceLabel: TextView? = null
    private var forwardNumberSwitch: SwitchCompat? = null
    private var coreServiceDescriptionLabel: TextView? = null
    private var coreServiceDescriptionSwitch: SwitchCompat? = null
    private var coreServiceDescriptionLabelColumn: LinearLayout? = null
    private var titleLeftAboveProgress: TextView? = null
    private var titleRightAboveProgress: TextView? = null
    private var coreServiceProgressBar: ProgressBar? = null
    private var titleRightBelowProgress: TextView? = null
    private var viewBelowContent: View? = null
    private var coreServiceProgressRow: LinearLayout? = null
    private var coreServiceTitleRow: LinearLayout? = null
    private var coreServiceViewItemLayout: LinearLayout? = null
    private var offeringIdLabel: TextView? = null
    private var statusLabel: TextView? = null
    private var mContext: Context? = null
    private var emptySpaceBelowDescriptionRow: View? = null
    private var mCoreServiceProcessEvents: CoreServiceProcessEvents? = null
    private var mCoreServicesCallDivertDisableEvents: CoreServicesCallDivertDisableEvents? = null
    private var validityLabel: TextView? = null

    @SuppressLint("InflateParams")
    constructor(context: Context, coreServiceProcessEvents: CoreServiceProcessEvents, coreServicesCallDivertDisableEvents: CoreServicesCallDivertDisableEvents) : super(context) {
        mContext = context
        mCoreServiceProcessEvents = coreServiceProcessEvents
        mCoreServicesCallDivertDisableEvents = coreServicesCallDivertDisableEvents
        main = LayoutInflater.from(context).inflate(R.layout.layout_core_service_view_item, null) as LinearLayout

        emptySpaceBelowDescriptionRow = main!!.findViewById(R.id.emptySpaceBelowDescriptionRow) as View
        statusLabel = main!!.findViewById(R.id.statusLabel) as TextView
        validityLabel= main!!.findViewById(R.id.validityLabel) as TextView
        validityLabel!!.typeface= getALSNormalFont()
        offeringIdLabel = main!!.findViewById(R.id.offeringIdLabel) as TextView
        coreServiceNameLabel = main!!.findViewById(R.id.coreServiceNameLabel) as TextView
        coreServiceNameLabel!!.typeface= getALSBoldFont()
        forwardToNumberLabel = main!!.findViewById(R.id.forwardToNumberLabel) as TextView
        forwardToNumberLabel!!.typeface= getALSBoldFont()
        coreServicePriceLabel = main!!.findViewById(R.id.coreServicePriceLabel) as TextView
        coreServicePriceLabel!!.typeface= getALSBoldFont()
        forwardNumberSwitch = main!!.findViewById(R.id.forwardNumberSwitch) as SwitchCompat
        coreServiceDescriptionLabel = main!!.findViewById(R.id.coreServiceDescriptionLabel) as TextView
        coreServiceDescriptionLabel!!.typeface= getALSNormalFont()
        coreServiceDescriptionSwitch = main!!.findViewById(R.id.coreServiceDescriptionSwitch) as SwitchCompat
        coreServiceDescriptionLabelColumn = main!!.findViewById(R.id.coreServiceDescriptionLabelColumn) as LinearLayout
        titleLeftAboveProgress = main!!.findViewById(R.id.titleLeftAboveProgress) as TextView
        titleLeftAboveProgress!!.typeface= getALSBoldFont()
        titleRightAboveProgress = main!!.findViewById(R.id.titleRightAboveProgress) as TextView
        titleRightAboveProgress!!.typeface= getALSNormalFont()
        titleRightBelowProgress = main!!.findViewById(R.id.titleRightBelowProgress) as TextView
        titleRightBelowProgress!!.typeface= getALSNormalFont()
        viewBelowContent = main!!.findViewById(R.id.viewBelowContent) as View
        coreServiceProgressRow = main!!.findViewById(R.id.coreServiceProgressRow) as LinearLayout
        coreServiceProgressBar = main!!.findViewById(R.id.coreServiceProgressBar) as ProgressBar
        coreServiceTitleRow = main!!.findViewById(R.id.coreServiceTitleRow) as LinearLayout
        coreServiceViewItemLayout = main!!.findViewById(R.id.coreServiceViewItemLayout) as LinearLayout

        this.addView(main)

        //ui events
        initUIEvents()
    }//constructor ends

    private fun initUIEvents() {
        coreServiceViewItemLayout!!.setOnClickListener {
            //get the offering id from the offering id label
            var offeringId = ""
            if (hasValue(offeringIdLabel!!.text.toString())) {
                offeringId = offeringIdLabel!!.text.toString()
            }

            /**Check if offering id is call divert
             * then check the status as inactive,
             * if the status is inactive; direct the user to the new screen
             * to add the forward number*/
            if (offeringId == ConstantsUtility.CoreServicesConstants.CALL_FORWARD_OFFERING_ID) {
                //get the status from the status label
                var status = statusLabel!!.text.toString()
                if (hasValue(statusLabel!!.text.toString())) {
                    status = statusLabel!!.text.toString()
                }

                if (status.equals(ConstantsUtility.CoreServicesConstants.STATUS_INACTIVE, ignoreCase = true)) {
                    val numberForwardActivityIntent = Intent(context!!, CoreServicesNumberForwardActivity::class.java)
                    numberForwardActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    numberForwardActivityIntent.putExtra(ConstantsUtility.CoreServicesConstants.KEY_NUMBER_FORWARD_DATA, offeringId)
                    context!!.startActivity(numberForwardActivityIntent)
                }
            }
        }//coreServiceViewItemLayout!!.setOnClickListener ends
        forwardNumberSwitch?.setOnTouchListener { v: View?, event: MotionEvent -> event.actionMasked == MotionEvent.ACTION_MOVE }
        forwardNumberSwitch!!.setOnClickListener {
            //get the offering id from the offering id label
            var offeringId = ""
            if (hasValue(offeringIdLabel!!.text.toString())) {
                offeringId = offeringIdLabel!!.text.toString()
            }

            if (forwardNumberSwitch!!.isChecked) {
                /**Check if offering id is call divert
                 * direct the user to the new screen
                 * to add the forward number*/
                if (offeringId == ConstantsUtility.CoreServicesConstants.CALL_FORWARD_OFFERING_ID) {
                    val numberForwardActivityIntent = Intent(context!!, CoreServicesNumberForwardActivity::class.java)
                    numberForwardActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    numberForwardActivityIntent.putExtra(ConstantsUtility.CoreServicesConstants.KEY_NUMBER_FORWARD_DATA, offeringId)
                    context!!.startActivity(numberForwardActivityIntent)
                }
            } else {
                mCoreServicesCallDivertDisableEvents!!.onCallDivertDisabled(offeringId, "", "3")
            }
        }//forwardNumberSwitch!!.setOnClickListener ends
        coreServiceDescriptionSwitch?.setOnTouchListener { v: View?, event: MotionEvent -> event.actionMasked == MotionEvent.ACTION_MOVE }
        coreServiceDescriptionSwitch!!.setOnClickListener {
            //get the offering id from the offering id label
            var offeringId = ""
            if (hasValue(offeringIdLabel!!.text.toString())) {
                offeringId = offeringIdLabel!!.text.toString()
            }

            if (coreServiceDescriptionSwitch!!.isChecked) {
                /**process the core service with action type=1*/

                mCoreServiceProcessEvents!!.onCoreServiceProcessed(getCoreServicesActivateProcessConfirmationPopup(), "1", offeringId)
            } else {
                /**process the core service with action type=3*/
                mCoreServiceProcessEvents!!.onCoreServiceProcessed(getCoreServicesDeactivateProcessConfirmationPopup(), "3", offeringId)
            }
        }//coreServiceDescriptionSwitch!!.setOnClickListener ends
    }//initUIEvents ends

    fun getValidityLabel():TextView{
        return validityLabel!!
    }

    fun getEmptySpaceBelowDescriptionRow(): View {
        return emptySpaceBelowDescriptionRow!!
    }//getEmptySpaceBelowDescriptionRow ends

    fun getStatusLabel(): TextView {
        return statusLabel!!
    }//getStatusLabel ends

    fun getOfferingIdLabel(): TextView {
        return offeringIdLabel!!
    }//getOfferingIdLabel ends

    fun getCoreServiceViewItemLayout(): LinearLayout {
        return coreServiceViewItemLayout!!
    }//getCoreServiceViewItemLayout ends

    fun getCoreServiceTitleRow(): LinearLayout {
        return coreServiceTitleRow!!
    }//getCoreServiceTitleRow ends

    fun getCoreServiceProgressBar(): ProgressBar {
        return coreServiceProgressBar!!
    }//getCoreServiceProgressBar ends

    fun getCoreServiceProgressRow(): LinearLayout {
        return coreServiceProgressRow!!
    }//getCoreServiceProgressRow ends

    fun getViewBelowContent(): View {
        return viewBelowContent!!
    }//getViewBelowContent ends

    fun getTitleRightBelowProgress(): TextView {
        return titleRightBelowProgress!!
    }//getTitleRightBelowProgress ends

    fun getTitleRightAboveProgress(): TextView {
        return titleRightAboveProgress!!
    }//getTitleRightAboveProgress ends

    fun getTitleLeftAboveProgress(): TextView {
        return titleLeftAboveProgress!!
    }//getTitleLeftAboveProgress ends

    fun getCoreServiceDescriptionLabelColumn(): LinearLayout {
        return coreServiceDescriptionLabelColumn!!
    }//getCoreServiceDescriptionLabelColumn ends

    fun getCoreServiceDescriptionSwitch(): SwitchCompat {
        return coreServiceDescriptionSwitch!!
    }//getCoreServiceDescriptionSwitch ends

    fun getCoreServiceDescriptionLabel(): TextView {
        return coreServiceDescriptionLabel!!
    }//getCoreServiceDescriptionLabel ends

    fun getForwardNumberSwitch(): SwitchCompat {
        return forwardNumberSwitch!!
    }//getForwardNumberSwitch ends

    fun getCoreServicePriceLabel(): TextView {
        return coreServicePriceLabel!!
    }//getCoreServicePriceLabel ends

    fun getCoreServiceNameLabel(): TextView {
        return coreServiceNameLabel!!
    }//getCoreServiceNameLabel ends

    fun getForwardToNumberLabel(): TextView {
        return forwardToNumberLabel!!
    }//getForwardToNumberLabel ends
}//class ends