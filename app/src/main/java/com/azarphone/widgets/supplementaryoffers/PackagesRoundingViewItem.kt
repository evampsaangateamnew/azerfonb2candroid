package com.azarphone.widgets.supplementaryoffers

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import com.azarphone.R
import com.azarphone.util.getALSBoldFont
import com.azarphone.util.getALSNormalFont

/**
 * @author Junaid Hassan on 23, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */

class PackagesRoundingViewItem : FrameLayout {
    private var main: LinearLayout? = null
    private var roundingViewItemLayout: LinearLayout? = null
    private var roundingViewItemLabel: TextView? = null
    private var roundingViewItemValue: TextView? = null

    @SuppressLint("InflateParams")
    constructor(context: Context) : super(context) {
        main = LayoutInflater.from(context).inflate(R.layout.layout_packages_rounding_view_item, null) as LinearLayout

        roundingViewItemLayout = main!!.findViewById(R.id.roundingViewItemLayout) as LinearLayout
        roundingViewItemLabel = main!!.findViewById(R.id.roundingViewItemLabel) as TextView
        roundingViewItemLabel!!.typeface = getALSNormalFont()
        roundingViewItemValue = main!!.findViewById(R.id.roundingViewItemValue) as TextView
        roundingViewItemValue!!.typeface = getALSBoldFont()

        this.addView(main)
    }//constructor ends

    fun getRoundingViewItemLayout(): LinearLayout {
        return roundingViewItemLayout!!
    }

    fun getRoundingViewItemLabel(): TextView {
        return roundingViewItemLabel!!
    }

    fun getRoundingViewItemValue(): TextView {
        return roundingViewItemValue!!
    }

}