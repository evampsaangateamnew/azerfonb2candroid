package com.azarphone.widgets.supplementaryoffers

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.azarphone.R

/**
 * @author Junaid Hassan on 23, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class PackagesBonusViewItem : FrameLayout {
    private var main: LinearLayout? = null
    private var bonusViewItemLayout: LinearLayout? = null
    private var bonusViewItemIcon: ImageView? = null
    private var bonusViewItemLabel: TextView? = null
    private var bonusViewItemValue: TextView? = null
    private var viewBelowBonusViewItemLayout: View? = null

    @SuppressLint("InflateParams")
    constructor(context: Context) : super(context) {
        main = LayoutInflater.from(context).inflate(R.layout.layout_packages_bonus_view_tem, null) as LinearLayout

        bonusViewItemLayout = main!!.findViewById(R.id.bonusViewItemLayout) as LinearLayout
        bonusViewItemIcon = main!!.findViewById(R.id.bonusViewItemIcon) as ImageView
        bonusViewItemLabel = main!!.findViewById(R.id.bonusViewItemLabel) as TextView
        bonusViewItemValue = main!!.findViewById(R.id.bonusViewItemValue) as TextView
        viewBelowBonusViewItemLayout = main!!.findViewById(R.id.viewBelowBonusViewItemLayout) as View

        this.addView(main)
    }//constructor ends

    fun getViewBelowBonusViewItemLayout(): View {
        return viewBelowBonusViewItemLayout!!
    }

    fun getBonusViewItemLayout(): LinearLayout {
        return bonusViewItemLayout!!
    }

    fun getBonusViewItemLabel(): TextView {
        return bonusViewItemLabel!!
    }

    fun getBonusViewItemIcon(): ImageView {
        return bonusViewItemIcon!!
    }

    fun getBonusViewItemValue(): TextView {
        return bonusViewItemValue!!
    }

}