package com.azarphone.widgets.supplementaryoffers

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.azarphone.R
import com.azarphone.util.getALSNormalFont

/**
 * @author Junaid Hassan on 23, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */

class PackagesTextWithPointsViewItem : FrameLayout {
    private var main: LinearLayout? = null
    private var textWithPointsViewItemLayout: LinearLayout? = null
    private var textWithPointsbullet: ImageView? = null
    private var textWithPointsViewItemValue: TextView? = null

    @SuppressLint("InflateParams")
    constructor(context: Context) : super(context) {
        main = LayoutInflater.from(context).inflate(R.layout.layout_packages_text_points_view_item, null) as LinearLayout

        textWithPointsViewItemLayout = main!!.findViewById(R.id.textWithPointsViewItemLayout) as LinearLayout
        textWithPointsbullet = main!!.findViewById(R.id.textWithPointsbullet) as ImageView
        textWithPointsViewItemValue = main!!.findViewById(R.id.textWithPointsViewItemValue) as TextView
        textWithPointsViewItemValue!!.typeface = getALSNormalFont()

        this.addView(main)
    }//constructor ends

    fun getTextWithPointsViewItemValue(): TextView {
        return textWithPointsViewItemValue!!
    }

    fun getTextWithPointsViewItemLayout(): LinearLayout {
        return textWithPointsViewItemLayout!!
    }

}