package com.azarphone.widgets.calendar

import android.app.DatePickerDialog
import androidx.core.content.ContextCompat
import android.widget.DatePicker
import android.widget.TextView
import com.azarphone.R
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.changeDateFormatForTextView
import com.azarphone.util.logE
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * @author Junaid Hassan on 04, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class AzerCalendar : DatePickerDialog.OnDateSetListener {
    private var mTextView: TextView? = null
    private var onDate: DatePickerDialog.OnDateSetListener? = null


    constructor(textViewNormal: TextView, onDate: DatePickerDialog.OnDateSetListener) {
        mTextView = textViewNormal
        this.onDate = onDate
    }

    override fun onDateSet(view: DatePicker, year: Int, month: Int, dayOfMonth: Int) {
        val c = Calendar.getInstance()
        c.set(year, month, dayOfMonth)

        val sdf = SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd)
        val formattedDate = sdf.format(c.time)
        try {
            mTextView!!.text = changeDateFormatForTextView(formattedDate)
            mTextView!!.setTextColor(ContextCompat.getColor(view.context, R.color.text_date_color))
            onDate!!.onDateSet(view, year, month, dayOfMonth)
        } catch (e: ParseException) {
            logE("setDate", "error:::".plus(e.toString()), "BakcellCalendar", "onDateSet")
        }

    }
}