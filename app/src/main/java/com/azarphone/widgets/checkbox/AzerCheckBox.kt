package com.azarphone.widgets.checkbox

import android.content.Context
import android.util.AttributeSet
import android.widget.CompoundButton
import androidx.appcompat.widget.AppCompatCheckBox

/**
 * @author Junaid Hassan on 30, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class AzerCheckBox : AppCompatCheckBox {
    private var mListener: CompoundButton.OnCheckedChangeListener? = null

    constructor(context: Context?) : super(context!!)

    constructor(context: Context?, attrs: AttributeSet?) : super(context!!, attrs)

    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(context!!, attrs, defStyle)

    override fun setOnCheckedChangeListener(listener: CompoundButton.OnCheckedChangeListener?) {
        mListener = listener
        super.setOnCheckedChangeListener(listener)
    }//setOnCheckedChangeListener

    fun setChecked(checked: Boolean, alsoNotify: Boolean) {
        if (!alsoNotify) {
            super.setOnCheckedChangeListener(null)
            super.setChecked(checked)
            super.setOnCheckedChangeListener(mListener)
            return
        }
        super.setChecked(checked)
    }//setChecked

    fun toggle(alsoNotify: Boolean) {
        if (!alsoNotify) {
            super.setOnCheckedChangeListener(null)
            super.toggle()
            super.setOnCheckedChangeListener(mListener)
        }
        super.toggle()
    }//toggle
}//class ends