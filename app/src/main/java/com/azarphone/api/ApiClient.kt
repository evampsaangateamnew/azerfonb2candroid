package com.azarphone.api

import android.content.Context
import com.azarphone.BuildConfig
import com.azarphone.ProjectApplication
import com.azarphone.util.KeyValues
import com.azarphone.util.RootValues
import com.azarphone.util.getCustomerDataFromLocalPreferences
import com.azarphone.util.getDeviceID
import com.azarphone.validators.hasValue
import com.es.tec.Dependencies

/*
 There are two ways we can do this
 */

class ApiClient : Dependencies() {


    lateinit var tecContext: Context
    private lateinit var serverAPI: NetworkApi

    /**
     * static variable
     * Timeout and stuff
     */

    companion object {
        val newApiClientInstance = ApiClient()
    }

    /*
     * Primary Consturctor initialziation block
     */
    fun setInit(context: Context) {
        serverAPI = provideRestApi(NetworkApi::class.java)
        this.tecContext = context
    }

    /**
     *    Returns server api instance
     */

    fun getServerAPI(): NetworkApi {
        return serverAPI
    }


    /**
     * Returns server api instance with api specfic timeout
     */

    fun getServerAPI(timeout: Int): NetworkApi {
        serverAPI = provideRestApi(NetworkApi::class.java, timeout)
        return serverAPI
    }

    /*
    Base URL Initialization
    You can take base url from your gradle as well
    */

    override fun setBaseUrl(): String {
        return BuildConfig.SERVER_URL
    }

    /**
     *   Header Initialization
     */
    override fun setHeaders(): HashMap<String, String> {

        //get the logged in user token & msisdn
        var token = ""
        if (getCustomerDataFromLocalPreferences().token != null) {
            if (hasValue(getCustomerDataFromLocalPreferences().token)) {
                token = getCustomerDataFromLocalPreferences().token!!
            }
        }

        var msisdn = ""
        if (getCustomerDataFromLocalPreferences().msisdn != null) {
            if (hasValue(getCustomerDataFromLocalPreferences().msisdn)) {
                msisdn = getCustomerDataFromLocalPreferences().msisdn!!
            }
        }

        var subscriberType = ""
        if (getCustomerDataFromLocalPreferences().subscriberType != null) {
            if (hasValue(getCustomerDataFromLocalPreferences().subscriberType)) {
                subscriberType = getCustomerDataFromLocalPreferences().subscriberType!!
            }
        }

        var tariffType = ""
        if (getCustomerDataFromLocalPreferences().brandName != null) {
            if (hasValue(getCustomerDataFromLocalPreferences().brandName)) {
                tariffType = getCustomerDataFromLocalPreferences().brandName!!
            }
        }

        val headerParams = HashMap<String, String>()
        headerParams["deviceID"] = getDeviceID(tecContext)
        headerParams["Content-Type"] = "application/json"
        headerParams["UserAgent"] = "android"
        headerParams["msisdn"] = msisdn
        headerParams["tariffType"] = tariffType
        headerParams["subscriberType"] = subscriberType
        headerParams["token"] = token
        headerParams["lang"] = ProjectApplication.mLocaleManager.getCurrentLanguageNumber()
        return headerParams
    }

    override fun setTimeOut(): Int = 30

  /*  override fun setSslCertificate(): String {
        return super.setSslCertificate()
    }*/

    override fun setSslCertificate(): ArrayList<String> {
/*
        var publicKeysList: ArrayList<String> = ArrayList<String>()
        publicKeysList.add("fr9aQi0of/5z/Lm4lDp7VNpB1DYr18slEq6KtqdTsJs=")

      //  publicKeysList.add("3pgyCOGaPyUDKoGpvbtglQPkrR14DEP+J7mQnJN6qrU=")

        return publicKeysList*/

        return KeyValues.getInstance().keysPublicServerFromNdk
    }

    override fun enableSSL(): Boolean {
        return BuildConfig.SSL_ENABLED
    }

    override fun setContext(): Context {
        return tecContext
    }


}

