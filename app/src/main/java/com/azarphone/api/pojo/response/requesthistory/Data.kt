package com.azarphone.api.pojo.response.requesthistory

data class Data(
	val loan: List<LoanRequestItem?>? = null
)
