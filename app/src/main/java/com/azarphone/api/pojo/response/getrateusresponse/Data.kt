package com.azarphone.api.pojo.response.getrateusresponse

data class Data(
	val pic_tnc: String? = null,
	val rateus_ios: String? = null,
	val rateus_android: String? = null
)
