package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

/**
 * @author Junaid Hassan on 27, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class GetHomePageRequest(
        @Json(name = "customerType")
        val customerType: String? = null,
        @Json(name = "brandId")
        val brandId: String? = null,
        @Json(name = "offeringId")
        val offeringId: String? = null,
        @Json(name = "subscriberType")
        val subscriberType: String? = null
)
