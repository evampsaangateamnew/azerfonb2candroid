package com.azarphone.api.pojo.response.tariffsresponse

import android.os.Parcel
import android.os.Parcelable

data class ItemsItem(
        val packagePrice: PackagePrice? = null,
        val header: Header? = null,
        val details: Details? = null,
        val subscribable: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readParcelable(PackagePrice::class.java.classLoader),
            parcel.readParcelable(Header::class.java.classLoader),
            parcel.readParcelable(Details::class.java.classLoader),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(packagePrice, flags)
        parcel.writeParcelable(header, flags)
        parcel.writeParcelable(details, flags)
        parcel.writeString(subscribable)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ItemsItem> {
        override fun createFromParcel(parcel: Parcel): ItemsItem {
            return ItemsItem(parcel)
        }

        override fun newArray(size: Int): Array<ItemsItem?> {
            return arrayOfNulls(size)
        }
    }

}