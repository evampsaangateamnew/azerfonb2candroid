package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

data class RequestMoneyRequest(
        @Json(name = "friendMsisdn")
        val friendMsisdn: String? = null,
        @Json(name = "amount")
        val amount: String? = null

)