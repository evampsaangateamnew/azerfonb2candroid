package com.azarphone.api.pojo.response.freesmsstatusresponse

import com.azarphone.api.pojo.response.BaseResponse

data class FreeSmsStatusResponse(
	override var data: Data,
	override var callStatus: String,
	override var resultCode: String,
	override var resultDesc: String
): BaseResponse<Data>()
