package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

/**
 * @author Junaid Hassan on 19, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class AppResumeRequest(
        @Json(name = "customerId")
        val customerId: String? = null,
        @Json(name = "entityId")
        val entityId: String? = null
)