package com.azarphone.api.pojo.response.mysubscriptions.response

import android.os.Parcel
import android.os.Parcelable
import com.azarphone.api.pojo.response.mysubscriptions.helper.Subscriptions

data class Internet(
	val offers: List<Subscriptions?>? = null
): Parcelable {
    constructor(parcel: Parcel) : this(parcel.createTypedArrayList(Subscriptions)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(offers)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Internet> {
        override fun createFromParcel(parcel: Parcel): Internet {
            return Internet(parcel)
        }

        override fun newArray(size: Int): Array<Internet?> {
            return arrayOfNulls(size)
        }
    }
}
