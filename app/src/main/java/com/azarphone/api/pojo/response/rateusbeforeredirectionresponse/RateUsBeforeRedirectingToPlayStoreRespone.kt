package com.azarphone.api.pojo.response.rateusbeforeredirectionresponse

import com.azarphone.api.pojo.response.BaseResponse

data class RateUsBeforeRedirectingToPlayStoreRespone(
		override var data: Data,
		override var callStatus: String,
		override var resultCode: String,
		override var resultDesc: String
):BaseResponse<Data>()
