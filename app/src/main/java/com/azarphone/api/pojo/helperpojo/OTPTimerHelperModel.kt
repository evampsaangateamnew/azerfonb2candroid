package com.azarphone.api.pojo.helperpojo

/**
 * @author Junaid Hassan on 14, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class OTPTimerHelperModel(
        val flag: Boolean? = null,
        val timerText: String? = null,
        val showHideFlag: Boolean? = null
)