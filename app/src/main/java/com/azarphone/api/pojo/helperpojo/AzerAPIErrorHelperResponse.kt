package com.azarphone.api.pojo.helperpojo

/**
 * @author Junaid Hassan on 11, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class AzerAPIErrorHelperResponse(
        val errorCode: Int? = null,
        val errorDetail: String? = null
)