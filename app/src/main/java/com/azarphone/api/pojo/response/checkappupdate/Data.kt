package com.azarphone.api.pojo.response.checkappupdate

data class Data(
	val appStore: Any? = null,
	val playStore: String? = null,
	val versionConfig: Any? = null,
	val message: String? = null,
	val timeStamps: List<TimeStampsItem?>? = null
)
