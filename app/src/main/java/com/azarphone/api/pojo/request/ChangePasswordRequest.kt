package com.azarphone.api.pojo.request

data class ChangePasswordRequest(
	val confirmNewPassword: String? = null,
	val oldPassword: String? = null,
	val newPassword: String? = null,
	val msisdn: String? = null
)
