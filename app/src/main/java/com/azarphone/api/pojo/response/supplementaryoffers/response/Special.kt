package com.azarphone.api.pojo.response.supplementaryoffers.response

import android.os.Parcel
import android.os.Parcelable
import com.azarphone.api.pojo.response.supplementaryoffers.helper.OfferFiltersMain
import com.azarphone.api.pojo.response.supplementaryoffers.helper.SupplementaryOffer

/**
 * @author Junaid Hassan on 25, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class Special(
        val offers: List<SupplementaryOffer?>? = null,
        val filters: OfferFiltersMain? = null
): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.createTypedArrayList(SupplementaryOffer),
            parcel.readParcelable(OfferFiltersMain::class.java.classLoader)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(offers)
        parcel.writeParcelable(filters, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Special> {
        override fun createFromParcel(parcel: Parcel): Special {
            return Special(parcel)
        }

        override fun newArray(size: Int): Array<Special?> {
            return arrayOfNulls(size)
        }
    }
}