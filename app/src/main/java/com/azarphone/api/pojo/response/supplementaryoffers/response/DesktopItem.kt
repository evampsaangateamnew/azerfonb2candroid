package com.azarphone.api.pojo.response.supplementaryoffers.response

data class DesktopItem(
	val value: String? = null,
	val key: String? = null
)
