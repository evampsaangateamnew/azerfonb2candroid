package com.azarphone.api.pojo.response.getrateusresponse

data class GetRateUsResponse(
	val data: Data? = null,
	val callStatus: String? = null,
	val resultCode: String? = null,
	val resultDesc: String? = null
)
