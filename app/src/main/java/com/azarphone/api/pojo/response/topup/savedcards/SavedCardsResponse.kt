package com.azarphone.api.pojo.response.topup.savedcards

import android.os.Parcel
import android.os.Parcelable

data class SavedCardsResponse(
        var data: SavedCardsData?,
        var callStatus: String?,
        var resultCode: String?,
        var resultDesc: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readParcelable(SavedCardsData::class.java.classLoader),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(data, flags)
        parcel.writeString(callStatus)
        parcel.writeString(resultCode)
        parcel.writeString(resultDesc)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SavedCardsResponse> {
        override fun createFromParcel(parcel: Parcel): SavedCardsResponse {
            return SavedCardsResponse(parcel)
        }

        override fun newArray(size: Int): Array<SavedCardsResponse?> {
            return arrayOfNulls(size)
        }
    }
}