package com.azarphone.api.pojo.response.predefinedata

data class TermsAndCondition(
	val prepaid: List<PrepaidItem?>? = null
)
