package com.azarphone.api.pojo.response.predefinedata

data class NarTv(
	val narTvRedirectionLinkWebRussian: Any? = null,
	val narTvRedirectionLinkAndroid: String? = null,
	val narTvRedirectionLinkWebEnglish: String? = null,
	val narTvRedirectionLinkIos: String? = null,
	val narTvRedirectionLinkWebAzeri: Any? = null
)
