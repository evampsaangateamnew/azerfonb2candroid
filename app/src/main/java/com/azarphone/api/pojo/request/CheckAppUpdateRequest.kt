package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

/**
 * Created by Junaid Hassan on 19, February, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
 data class CheckAppUpdateRequest(
        @Json(name = "appversion")
        val appversion: String? = null
)