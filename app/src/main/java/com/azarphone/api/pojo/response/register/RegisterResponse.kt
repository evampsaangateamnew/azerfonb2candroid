package com.azarphone.api.pojo.response.register

import com.squareup.moshi.Json


data class RegisterResponse(

	@Json(name="token")
	val token: String? = null
)