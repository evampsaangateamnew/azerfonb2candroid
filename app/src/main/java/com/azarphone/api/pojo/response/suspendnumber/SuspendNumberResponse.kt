package com.azarphone.api.pojo.response.suspendnumber

import com.azarphone.api.pojo.response.BaseResponse

data class SuspendNumberResponse(
	override var data: Data,
	override var callStatus: String,
	override var resultCode: String,
	override var resultDesc: String
):BaseResponse<Data>()
