package com.azarphone.api.pojo.response.mysubscriptions.response

import android.os.Parcel
import android.os.Parcelable
import com.azarphone.api.pojo.response.mysubscriptions.helper.Subscriptions

data class SmsInclusiveOffers(
	val offers: List<Subscriptions?>? = null
	/*val inclusiveUsageList: List<Any?>? = null*/
): Parcelable {
	constructor(parcel: Parcel) : this(parcel.createTypedArrayList(Subscriptions)) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeTypedList(offers)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<SmsInclusiveOffers> {
		override fun createFromParcel(parcel: Parcel): SmsInclusiveOffers {
			return SmsInclusiveOffers(parcel)
		}

		override fun newArray(size: Int): Array<SmsInclusiveOffers?> {
			return arrayOfNulls(size)
		}
	}
}
