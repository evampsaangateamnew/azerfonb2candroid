package com.azarphone.api.pojo.response.gethomepageresponse

data class FreeResources(
	val freeResourcesRoaming: List<FreeResourcesRoamingItem?>? = null,
	val freeResources: List<FreeResourcesItem?>? = null
)
