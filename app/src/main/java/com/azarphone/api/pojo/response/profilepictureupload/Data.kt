package com.azarphone.api.pojo.response.profilepictureupload

data class Data(
	val imageURL: String? = null
)
