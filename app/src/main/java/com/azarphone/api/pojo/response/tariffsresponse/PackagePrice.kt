package com.azarphone.api.pojo.response.tariffsresponse

import android.os.Parcel
import android.os.Parcelable

data class PackagePrice(
        val call: Call? = null,
        val packagePriceLabel: String? = null,
        val sms: Sms? = null,
        val internet: Internet? = null,
        val callPayg: CallPayg? = null,
        val internetPayg: InternetPayg? = null,
        val smsPayg: SmsPayg? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readParcelable(Call::class.java.classLoader),
            parcel.readString(),
            parcel.readParcelable(Sms::class.java.classLoader),
            parcel.readParcelable(Internet::class.java.classLoader),
            parcel.readParcelable(CallPayg::class.java.classLoader),
            parcel.readParcelable(InternetPayg::class.java.classLoader),
            parcel.readParcelable(SmsPayg::class.java.classLoader))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(call, flags)
        parcel.writeString(packagePriceLabel)
        parcel.writeParcelable(sms, flags)
        parcel.writeParcelable(internet, flags)
        parcel.writeParcelable(callPayg, flags)
        parcel.writeParcelable(internetPayg, flags)
        parcel.writeParcelable(smsPayg, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PackagePrice> {
        override fun createFromParcel(parcel: Parcel): PackagePrice {
            return PackagePrice(parcel)
        }

        override fun newArray(size: Int): Array<PackagePrice?> {
            return arrayOfNulls(size)
        }
    }
}
