package com.azarphone.api.pojo.response.notification

import com.azarphone.api.pojo.response.BaseResponse

data class NotificationResponse(override var data: Data, override var callStatus: String, override var resultCode: String, override var resultDesc: String) : BaseResponse<Data>()
