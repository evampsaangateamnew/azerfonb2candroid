package com.azarphone.api.pojo.response.mysubscriptions.successresponse

data class Data(
	val mySubscriptionsData: MySubscriptionsData? = null,
	val message: String? = null
)
