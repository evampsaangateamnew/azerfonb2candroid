package com.azarphone.api.pojo.response.forgotpassword

data class Fnf(
	val maxFNFCount: String? = null,
	val fnFAllowed: String? = null
)
