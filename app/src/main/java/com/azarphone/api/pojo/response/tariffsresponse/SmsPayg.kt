package com.azarphone.api.pojo.response.tariffsresponse

import android.os.Parcel
import android.os.Parcelable

data class SmsPayg(
	val iconName: String? = null,
	val attributes: List<AttributesItem?>? = null,
	val title: String? = null,
	val titleValue: String? = null
): Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readString(),
			parcel.createTypedArrayList(AttributesItem),
			parcel.readString(),
			parcel.readString()) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(iconName)
		parcel.writeTypedList(attributes)
		parcel.writeString(title)
		parcel.writeString(titleValue)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<SmsPayg> {
		override fun createFromParcel(parcel: Parcel): SmsPayg {
			return SmsPayg(parcel)
		}

		override fun newArray(size: Int): Array<SmsPayg?> {
			return arrayOfNulls(size)
		}
	}
}
