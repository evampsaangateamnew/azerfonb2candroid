package com.azarphone.api.pojo.response.supplementaryoffers.response

import android.os.Parcel
import android.os.Parcelable
import com.azarphone.api.pojo.response.supplementaryoffers.helper.RoamingsOffer

data class Data(
        val call: Call? = null,
        val hybrid: Hybrid? = null,
        val roaming: RoamingsOffer? = null,
        val smsInclusiveOffers: SmsInclusiveOffers? = null,
        val sms: Sms? = null,
        val campaign: Campaign? = null,
        val tm: Tm? = null,
        val voiceInclusiveOffers: VoiceInclusiveOffers? = null,
        val internetInclusiveOffers: InternetInclusiveOffers? = null,
        val internet: Internet? = null,
        val special: Special? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readParcelable(Call::class.java.classLoader),
            parcel.readParcelable(Hybrid::class.java.classLoader),
            parcel.readParcelable(RoamingsOffer::class.java.classLoader),
            parcel.readParcelable(SmsInclusiveOffers::class.java.classLoader),
            parcel.readParcelable(Sms::class.java.classLoader),
            parcel.readParcelable(Campaign::class.java.classLoader),
            parcel.readParcelable(Tm::class.java.classLoader),
            parcel.readParcelable(VoiceInclusiveOffers::class.java.classLoader),
            parcel.readParcelable(InternetInclusiveOffers::class.java.classLoader),
            parcel.readParcelable(Internet::class.java.classLoader),
            parcel.readParcelable(Special::class.java.classLoader))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(call, flags)
        parcel.writeParcelable(hybrid, flags)
        parcel.writeParcelable(roaming, flags)
        parcel.writeParcelable(smsInclusiveOffers, flags)
        parcel.writeParcelable(sms, flags)
        parcel.writeParcelable(campaign, flags)
        parcel.writeParcelable(tm, flags)
        parcel.writeParcelable(voiceInclusiveOffers, flags)
        parcel.writeParcelable(internetInclusiveOffers, flags)
        parcel.writeParcelable(internet, flags)
        parcel.writeParcelable(special, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Data> {
        override fun createFromParcel(parcel: Parcel): Data {
            return Data(parcel)
        }

        override fun newArray(size: Int): Array<Data?> {
            return arrayOfNulls(size)
        }
    }
}
