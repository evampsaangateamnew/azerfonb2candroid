package com.azarphone.api.pojo.response.tariffsresponse

data class TariffsResponse(
        val data: Data? = null,
        val callStatus: String? = null,
        val resultCode: String? = null,
        val resultDesc: String? = null
)
