package com.azarphone.api.pojo.response.predefinedata

data class Dov(
	val footer: String? = null,
	val header: String? = null
)
