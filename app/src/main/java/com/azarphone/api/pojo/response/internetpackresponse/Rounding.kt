package com.azarphone.api.pojo.response.internetpackresponse

data class Rounding(
    val descriptionRounding: String,
    val destinationRoundingIcon: String,
    val destinationRoundingLabel: String,
    val destinationRoundingValue: String,
    val fragmentIcon: String,
    val internationalRoundingLabel: String,
    val internationalRoundingValue: String,
    val internetRoundingLabel: String,
    val internetRoundingValue: String,
    val offnetRoundingLabel: String,
    val offnetRoundingValue: String,
    val onnetRoundingLabel: String,
    val onnetRoundingValue: String
)