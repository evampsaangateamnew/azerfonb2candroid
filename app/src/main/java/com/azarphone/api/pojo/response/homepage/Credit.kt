package com.azarphone.api.pojo.response.homepage

data class Credit(
	val creditTitleLabel: Any? = null,
	val creditTitleValue: String? = null,
	val creditDate: String? = null,
	val creditInitialDate: String? = null,
	val creditLimit: String? = null,
	val creditCurrency: String? = null,
	val creditDateLabel: String? = null
)
