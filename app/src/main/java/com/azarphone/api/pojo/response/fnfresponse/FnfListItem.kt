package com.azarphone.api.pojo.response.fnfresponse

data class FnfListItem(
	val createdDate: String? = null,
	val msisdn: String? = null
)
