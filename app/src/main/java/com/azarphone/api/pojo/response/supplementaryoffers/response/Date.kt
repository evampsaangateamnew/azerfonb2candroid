package com.azarphone.api.pojo.response.supplementaryoffers.response

import android.os.Parcel
import android.os.Parcelable

data class Date(
	val fromValue: String? = null,
	val toTitle: String? = null,
	val toValue: String? = null,
	val fromTitle: String? = null,
	val description: String? = null
): Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString()) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(fromValue)
		parcel.writeString(toTitle)
		parcel.writeString(toValue)
		parcel.writeString(fromTitle)
		parcel.writeString(description)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<Date> {
		override fun createFromParcel(parcel: Parcel): Date {
			return Date(parcel)
		}

		override fun newArray(size: Int): Array<Date?> {
			return arrayOfNulls(size)
		}
	}
}
