package com.azarphone.api.pojo.response.paymenthistory

import com.azarphone.api.pojo.response.BaseResponse

data class PaymentHistoryResponse(
	override var data: Data,
	override var callStatus: String,
	override var resultCode: String,
	override var resultDesc: String
):BaseResponse<Data>()
