package com.azarphone.api.pojo.response.tariffsresponse

import android.os.Parcel
import android.os.Parcelable

data class AttributesItem(
        val iconName: String? = null,
        val metrics: String? = null,
        val title: String? = null,
        val value: String? = null,
        val valueLeft: String? = null,
        val valueRight: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(iconName)
        parcel.writeString(metrics)
        parcel.writeString(title)
        parcel.writeString(value)
        parcel.writeString(valueLeft)
        parcel.writeString(valueRight)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AttributesItem> {
        override fun createFromParcel(parcel: Parcel): AttributesItem {
            return AttributesItem(parcel)
        }

        override fun newArray(size: Int): Array<AttributesItem?> {
            return arrayOfNulls(size)
        }
    }
}
