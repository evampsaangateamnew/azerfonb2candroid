package com.azarphone.api.pojo.response.topup.initiatepayment

data class InitiatePaymentData(val paymentKey: String)
