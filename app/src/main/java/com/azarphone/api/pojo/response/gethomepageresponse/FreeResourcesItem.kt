package com.azarphone.api.pojo.response.gethomepageresponse

data class FreeResourcesItem(
	val resourceDiscountedText: String? = null,
	val resourceUnitName: String? = null,
	val resourceRemainingUnits: String? = null,
	val resourceInitialUnits: String? = null,
	val resourcesTitleLabel: String? = null,
	val resourceType: String? = null,
	val remainingFormatted: String? = null
)
