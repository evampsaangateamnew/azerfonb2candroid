package com.azarphone.api.pojo.response.topup.savedcards

import android.os.Parcel
import android.os.Parcelable

data class SavedCardsData(val cardDetails: List<SavedCardsItem>?) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.createTypedArrayList(SavedCardsItem)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(cardDetails)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SavedCardsData> {
        override fun createFromParcel(parcel: Parcel): SavedCardsData {
            return SavedCardsData(parcel)
        }

        override fun newArray(size: Int): Array<SavedCardsData?> {
            return arrayOfNulls(size)
        }
    }
}
