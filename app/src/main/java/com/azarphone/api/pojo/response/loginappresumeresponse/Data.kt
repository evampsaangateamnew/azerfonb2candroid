package com.azarphone.api.pojo.response.loginappresumeresponse

import com.azarphone.api.pojo.response.customerdata.CustomerData
import com.azarphone.api.pojo.response.predefinedata.PredefinedData

data class Data(
        var supplementaryOfferingList: List<SupplementaryOfferingListItem?>? = null,
        var customerData: CustomerData,
        var primaryOffering: PrimaryOffering? = null,
        var predefinedData: PredefinedData? = null
)