package com.azarphone.api.pojo.response.inappsurvey

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by Muhammad Ahmed on 8/30/2021.
 */


@Parcelize
data class Data(
    @SerializedName("surveys") val surveys: List<Surveys?>?,
    @SerializedName("userSurveys") val userSurveys : List<UserSurveys>?
)  : Parcelable {
    fun findSurvey(pageName: String): Surveys? {
        surveys?.forEach {
            if (it?.screenName == pageName) {
                return it
            }
        }
        return null
    }
    fun findSurveyById(surveyId: Int): Surveys? {
        surveys?.forEach {
            if (it?.id == surveyId) {
                return it
            }
        }
        return null
    }
}