package com.azarphone.api.pojo.response.supplementaryoffers.helper

import android.os.Parcel
import android.os.Parcelable
import com.azarphone.api.pojo.response.supplementaryoffers.response.AttributeListItem
import com.azarphone.api.pojo.response.supplementaryoffers.response.OfferGroup

/**
 * @author Junaid Hassan on 22, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class SupplementryOffersHeaders(
        val validityTitle: String? = null,
        val validityValue: String? = null,
        val offerName: String? = null,
        val attributeList: List<AttributeListItem?>? = null,
        val preReqOfferId: String? = null,
        /*val usage: Any? = null,*/
        val btnDeactivate: String? = null,
        val validityInformation: String? = null,
        val appOfferFilter: String? = null,
        val stickerLabel: String? = null,
        val type: String? = null,
        val offeringId: String? = null,
        /*val sortOrderMS: Any? = null,*/
        val stickerColorCode: String? = null,
        val offerGroup: OfferGroup? = null,
        val price: String? = null,
        /*   val sortOrder: Any? = null,*/
        val offerLevel: String? = null,
        val isTopUp: String? = null,
        val isFreeResource: String? = null,
        val id: String? = null,
        val btnRenew: String? = null,
        val status: String? = null,
        val isAlreadySusbcribed: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.createTypedArrayList(AttributeListItem),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readParcelable(OfferGroup::class.java.classLoader),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(validityTitle)
        parcel.writeString(validityValue)
        parcel.writeString(offerName)
        parcel.writeTypedList(attributeList)
        parcel.writeString(preReqOfferId)
        parcel.writeString(btnDeactivate)
        parcel.writeString(validityInformation)
        parcel.writeString(appOfferFilter)
        parcel.writeString(stickerLabel)
        parcel.writeString(type)
        parcel.writeString(offeringId)
        parcel.writeString(stickerColorCode)
        parcel.writeParcelable(offerGroup, flags)
        parcel.writeString(price)
        parcel.writeString(offerLevel)
        parcel.writeString(isTopUp)
        parcel.writeString(isFreeResource)
        parcel.writeString(id)
        parcel.writeString(btnRenew)
        parcel.writeString(status)
        parcel.writeString(isAlreadySusbcribed)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SupplementryOffersHeaders> {
        override fun createFromParcel(parcel: Parcel): SupplementryOffersHeaders {
            return SupplementryOffersHeaders(parcel)
        }

        override fun newArray(size: Int): Array<SupplementryOffersHeaders?> {
            return arrayOfNulls(size)
        }
    }
}