package com.azarphone.api.pojo.response.internetpackresponse

data class InternetPacksResponse(
    val data: Data?,
    val execTime: Double,
    val msg: String,
    val resultCode: String
)