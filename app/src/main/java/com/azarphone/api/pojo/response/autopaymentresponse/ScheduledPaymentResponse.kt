package com.azarphone.api.pojo.response.autopaymentresponse

import com.azarphone.api.pojo.response.BaseResponse

data class ScheduledPaymentResponse(
        override var data: ScheduledPaymentData,
        override var callStatus: String,
        override var resultCode: String,
        override var resultDesc: String

) : BaseResponse<ScheduledPaymentData>()
