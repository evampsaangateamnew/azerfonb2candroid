package com.azarphone.api.pojo.response.supplementaryoffers.response

import android.os.Parcel
import android.os.Parcelable

data class Rounding(
        val iconName: String? = null,
        val attributeList: List<AttributeListItem?>? = null,
        val description: String? = null,
        val title: String? = null,
        val value: String? = null
): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.createTypedArrayList(AttributeListItem),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(iconName)
        parcel.writeTypedList(attributeList)
        parcel.writeString(description)
        parcel.writeString(title)
        parcel.writeString(value)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Rounding> {
        override fun createFromParcel(parcel: Parcel): Rounding {
            return Rounding(parcel)
        }

        override fun newArray(size: Int): Array<Rounding?> {
            return arrayOfNulls(size)
        }
    }
}
