package com.azarphone.api.pojo.response.sendsmsresponse

import com.azarphone.api.pojo.response.BaseResponse

data class SendSMSResponse(
	override var data: Data,
	override var  callStatus: String,
	override var  resultCode: String,
	override var  resultDesc: String
): BaseResponse<Data>()
