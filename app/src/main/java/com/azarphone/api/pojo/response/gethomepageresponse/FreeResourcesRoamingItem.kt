package com.azarphone.api.pojo.response.gethomepageresponse

/**
 * @author Junaid Hassan on 09, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class FreeResourcesRoamingItem(
        val resourceDiscountedText: String? = null,
        val resourceUnitName: String? = null,
        val resourceRemainingUnits: String? = null,
        val resourceInitialUnits: String? = null,
        val resourcesTitleLabel: String? = null,
        val resourceType: String? = null,
        val remainingFormatted: String? = null
)