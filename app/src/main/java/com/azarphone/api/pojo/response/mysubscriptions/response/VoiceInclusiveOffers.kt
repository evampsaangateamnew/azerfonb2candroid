package com.azarphone.api.pojo.response.mysubscriptions.response

import android.os.Parcel
import android.os.Parcelable
import com.azarphone.api.pojo.response.mysubscriptions.helper.Subscriptions

data class VoiceInclusiveOffers(
	val offers: List<Subscriptions?>? = null
	/*val inclusiveUsageList: List<Any?>? = null*/
): Parcelable {
	constructor(parcel: Parcel) : this(parcel.createTypedArrayList(Subscriptions)) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeTypedList(offers)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<VoiceInclusiveOffers> {
		override fun createFromParcel(parcel: Parcel): VoiceInclusiveOffers {
			return VoiceInclusiveOffers(parcel)
		}

		override fun newArray(size: Int): Array<VoiceInclusiveOffers?> {
			return arrayOfNulls(size)
		}
	}
}
