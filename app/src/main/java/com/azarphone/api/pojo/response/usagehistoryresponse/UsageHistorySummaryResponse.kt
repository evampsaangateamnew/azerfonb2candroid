package com.azarphone.api.pojo.response.usagehistoryresponse

import com.azarphone.api.pojo.response.BaseResponse

data class UsageHistorySummaryResponse(
	override var data: Data,
	override var callStatus: String,
	override var resultCode: String,
	override var resultDesc: String
):BaseResponse<Data>()
