package com.azarphone.api.pojo.response.payg

data class WithOutPackData(val status: String, val offeringId: String)