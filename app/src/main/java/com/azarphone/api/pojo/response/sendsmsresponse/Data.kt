package com.azarphone.api.pojo.response.sendsmsresponse

data class Data(
	val onNetSMS: String? = null,
	val offNetSMS: String? = null,
	val smsSent: String? = null
)
