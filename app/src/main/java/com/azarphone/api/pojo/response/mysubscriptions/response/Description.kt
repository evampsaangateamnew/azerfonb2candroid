package com.azarphone.api.pojo.response.mysubscriptions.response

data class Description(
        val date: Any? = null,
        val textWithOutTitle: Any? = null,
        val price: List<PriceItem?>? = null,
        val textWithPoints: Any? = null,
        val freeResourceValidity: Any? = null,
        val rounding: Any? = null,
        val titleSubTitleValueAndDesc: Any? = null,
        val time: Any? = null,
        val textWithTitle: Any? = null,
        val roamingDetails: Any? = null
)
