package com.azarphone.api.pojo.response.supplementaryoffers.helper

import android.os.Parcel
import android.os.Parcelable

/**
 * @author Junaid Hassan on 22, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class SupplementaryOffer(
        var header: SupplementryOffersHeaders? = null,
        var details: SupplementryOffersDetailsAndDescription? = null,
        var description: SupplementryOffersDetailsAndDescription? = null
): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readParcelable(SupplementryOffersHeaders::class.java.classLoader),
            parcel.readParcelable(SupplementryOffersDetailsAndDescription::class.java.classLoader),
            parcel.readParcelable(SupplementryOffersDetailsAndDescription::class.java.classLoader)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(header, flags)
        parcel.writeParcelable(details, flags)
        parcel.writeParcelable(description, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SupplementaryOffer> {
        override fun createFromParcel(parcel: Parcel): SupplementaryOffer {
            return SupplementaryOffer(parcel)
        }

        override fun newArray(size: Int): Array<SupplementaryOffer?> {
            return arrayOfNulls(size)
        }
    }
}
