package com.azarphone.api.pojo.response.coreservicesprocessnumber

data class CoreServicesProcessResponse(
	val data: Data? = null,
	val callStatus: String? = null,
	val resultCode: String? = null,
	val resultDesc: String? = null
)
