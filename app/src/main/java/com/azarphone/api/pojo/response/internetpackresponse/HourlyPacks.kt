package com.azarphone.api.pojo.response.internetpackresponse

data class HourlyPacks(
    val filters: Filters?,
    val offers: List<Offer?>
)