package com.azarphone.api.pojo.helperpojo

import com.azarphone.api.pojo.response.customerdata.CustomerData

data class UserAccounts(var usersList: List<CustomerData>? = null)