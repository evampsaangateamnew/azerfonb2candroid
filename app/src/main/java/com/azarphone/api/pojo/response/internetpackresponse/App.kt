package com.azarphone.api.pojo.response.internetpackresponse

data class App(
    val key: String?,
    val value: String?
)