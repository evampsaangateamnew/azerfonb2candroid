package com.azarphone.api.pojo.response.mysubscriptions.response

import android.os.Parcel
import android.os.Parcelable
import com.azarphone.api.pojo.response.mysubscriptions.helper.Subscriptions

data class Roaming(
        val offers: List<Subscriptions?>? = null,
        val countries: List<Countries?>? = null
): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.createTypedArrayList(Subscriptions),
            parcel.createTypedArrayList(Countries)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(offers)
        parcel.writeTypedList(countries)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Roaming> {
        override fun createFromParcel(parcel: Parcel): Roaming {
            return Roaming(parcel)
        }

        override fun newArray(size: Int): Array<Roaming?> {
            return arrayOfNulls(size)
        }
    }
}
