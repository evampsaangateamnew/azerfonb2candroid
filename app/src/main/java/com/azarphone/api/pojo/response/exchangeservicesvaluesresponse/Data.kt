package com.azarphone.api.pojo.response.exchangeservicesvaluesresponse

data class Data(
	val datavalues: List<DatavaluesItem?>? = null,
	val voicevalues: List<VoicevaluesItem?>? = null,
	val count: String? = null,
	val isServiceEnabled: String? = null
)
