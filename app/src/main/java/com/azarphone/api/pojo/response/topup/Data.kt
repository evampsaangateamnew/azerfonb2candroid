package com.azarphone.api.pojo.response.topup

data class Data(
	val newBalance: String? = null,
	val oldBalance: String? = null
)
