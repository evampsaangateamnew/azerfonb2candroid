package com.azarphone.api.pojo.response.tariffsresponse

import android.os.Parcel
import android.os.Parcelable

data class TitleSubTitleValueAndDesc(
	val shortDesc: String? = null,
	val attributesList: List<AttributesListItem?>? = null,
	val title: String? = null
): Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readString(),
			parcel.createTypedArrayList(AttributesListItem),
			parcel.readString()) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(shortDesc)
		parcel.writeTypedList(attributesList)
		parcel.writeString(title)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<TitleSubTitleValueAndDesc> {
		override fun createFromParcel(parcel: Parcel): TitleSubTitleValueAndDesc {
			return TitleSubTitleValueAndDesc(parcel)
		}

		override fun newArray(size: Int): Array<TitleSubTitleValueAndDesc?> {
			return arrayOfNulls(size)
		}
	}
}
