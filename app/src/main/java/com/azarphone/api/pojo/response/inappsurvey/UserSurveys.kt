package com.azarphone.api.pojo.response.inappsurvey

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


/**
 * Created by Muhammad Ahmed on 8/30/2021.
 */

@Parcelize
data class UserSurveys (

    @SerializedName("answer") val answer : String?,
    @SerializedName("offeringId") val offeringId : String?,
    @SerializedName("offeringType") val offeringType : String?,
    @SerializedName("comment") val comment : String?,
    @SerializedName("surveyId") val surveyId : String?,
    @SerializedName("answerId") val answerId : String?,
    @SerializedName("questionId") val questionId : String?
): Parcelable