package com.azarphone.api.pojo.response.menuresponse

data class MenuAZ(
	val menuHorizontal: List<MenuHorizontalItem?>? = null,
	val menuVertical: List<MenuVerticalItem?>? = null
)
