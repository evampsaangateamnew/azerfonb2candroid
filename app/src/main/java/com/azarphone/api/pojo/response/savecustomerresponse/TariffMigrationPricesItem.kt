package com.azarphone.api.pojo.response.savecustomerresponse

data class TariffMigrationPricesItem(
	val value: String? = null,
	val key: String? = null
)
