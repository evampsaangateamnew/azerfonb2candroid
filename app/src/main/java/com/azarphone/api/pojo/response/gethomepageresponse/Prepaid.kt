package com.azarphone.api.pojo.response.gethomepageresponse

data class Prepaid(
	val bounusWallet: BounusWallet? = null,
	val mainWallet: MainWallet? = null,
	val countryWideWallet: CountryWideWallet? = null
)
