package com.azarphone.api.pojo.response.menuresponse

data class ItemsItem(
	val identifier: String? = null,
	val sortOrder: String? = null,
	val title: String? = null
)
