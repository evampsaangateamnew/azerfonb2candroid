package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

/**
 * @author Junaid Hassan on 11, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class LoginRequest(

        @Json(name = "msisdn")
        val msisdn: String? = null,

        @Json(name = "password")
        val password: String? = null
)