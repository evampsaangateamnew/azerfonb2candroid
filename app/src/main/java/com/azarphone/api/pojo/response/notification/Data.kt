package com.azarphone.api.pojo.response.notification

data class Data(
	val notificationsList: List<NotificationsListItem>? = null
)
