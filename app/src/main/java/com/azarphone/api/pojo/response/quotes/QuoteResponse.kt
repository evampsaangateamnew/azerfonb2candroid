package com.azarphone.api.pojo.response.newsfeed.quotes

import com.squareup.moshi.Json


data class QuoteResponse(

	@Json(name="id")
	val id: Int? = null,

	@Json(name="completed")
	val completed: Boolean? = null,

	@Json(name="title")
	val title: String? = null,

	@Json(name="userId")
	val userId: Int? = null
)