package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

/**
 * @author Junaid Hassan on 03, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class TopupRequest(
        @Json(name = "cardPinNumber")
        val cardPinNumber: String? = null,

        @Json(name = "topupnum")
        val topupnum: String? = null,

        @Json(name = "subscriberType")
        val subscriberType: String? = null
)