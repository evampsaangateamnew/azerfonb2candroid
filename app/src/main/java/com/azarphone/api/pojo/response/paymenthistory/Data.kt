package com.azarphone.api.pojo.response.paymenthistory

data class Data(
	val paymentHistory: List<PaymentHistoryItem?>? = null
)
