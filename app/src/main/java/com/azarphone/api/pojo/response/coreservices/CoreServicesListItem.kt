package com.azarphone.api.pojo.response.coreservices

import android.os.Parcel
import android.os.Parcelable

data class CoreServicesListItem(
        val progressTitle: String? = null,
        val description: String? = null,
        val visibleFor: String? = null,
        val offeringId: String? = null,
        val storeId: String? = null,
        val title: String? = null,
        val progressDateLabel: String? = null,
        val validityLabel: String? = null,
        val price: String? = null,
        val renewable: Int? = null,
        val sortOrder: Int? = null,
        val name: String? = null,
        var forwardNumber: String? = null,
        val expireDate: String? = null,
        val id: Int? = null,
        val validity: String? = null,
        val categoryId: Int? = null,
        val effectiveDate: String? = null,
        var status: String? = null,
        val freeFor: String? = null,
        val desc: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(progressTitle)
        parcel.writeString(description)
        parcel.writeString(visibleFor)
        parcel.writeString(offeringId)
        parcel.writeString(storeId)
        parcel.writeString(title)
        parcel.writeString(progressDateLabel)
        parcel.writeString(validityLabel)
        parcel.writeString(price)
        parcel.writeValue(renewable)
        parcel.writeValue(sortOrder)
        parcel.writeString(name)
        parcel.writeString(forwardNumber)
        parcel.writeString(expireDate)
        parcel.writeValue(id)
        parcel.writeString(validity)
        parcel.writeValue(categoryId)
        parcel.writeString(effectiveDate)
        parcel.writeString(status)
        parcel.writeString(freeFor)
        parcel.writeString(desc)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CoreServicesListItem> {
        override fun createFromParcel(parcel: Parcel): CoreServicesListItem {
            return CoreServicesListItem(parcel)
        }

        override fun newArray(size: Int): Array<CoreServicesListItem?> {
            return arrayOfNulls(size)
        }
    }
}
