package com.azarphone.api.pojo.response.tariffsresponse

data class TariffResponseListItem(
	val groupType: String? = null,
	val isSpecial: String? = null,
	val items: List<ItemsItem?>? = null
)
