package com.azarphone.api.pojo.response.internetpackresponse

data class Desktop(
    val key: String?,
    val value: String?
)