package com.azarphone.api.pojo.response.supplementaryoffers.response

import android.os.Parcel
import android.os.Parcelable
import com.azarphone.api.pojo.response.supplementaryoffers.helper.OfferFiltersMain
import com.azarphone.api.pojo.response.supplementaryoffers.helper.SupplementaryOffer

data class Roaming(
        val offers: List<SupplementaryOffer?>? = null,
        val countries: List<Countries?>? = null,
        val filters: OfferFiltersMain? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.createTypedArrayList(SupplementaryOffer),
            parcel.createTypedArrayList(Countries),
            parcel.readParcelable(OfferFiltersMain::class.java.classLoader)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(offers)
        parcel.writeTypedList(countries)
        parcel.writeParcelable(filters, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Roaming> {
        override fun createFromParcel(parcel: Parcel): Roaming {
            return Roaming(parcel)
        }

        override fun newArray(size: Int): Array<Roaming?> {
            return arrayOfNulls(size)
        }
    }
}
