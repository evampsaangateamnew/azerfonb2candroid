package com.azarphone.api.pojo.response.moneytransfer

data class Data(
	val oldBalance: String? = null,
	val newBalance: String? = null
)
