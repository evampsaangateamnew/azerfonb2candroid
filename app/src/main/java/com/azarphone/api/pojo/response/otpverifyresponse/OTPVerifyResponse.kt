package com.azarphone.api.pojo.response.otpverifyresponse

import com.azarphone.api.pojo.response.BaseResponse

data class OTPVerifyResponse(
        override var data: Data,
        override var callStatus: String,
        override var resultCode: String,
        override var resultDesc: String
) : BaseResponse<Data>()
