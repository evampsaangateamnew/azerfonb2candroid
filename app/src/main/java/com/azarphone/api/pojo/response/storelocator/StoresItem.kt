package com.azarphone.api.pojo.response.storelocator

import com.squareup.moshi.Json

data class StoresItem(

	@Json(name="address")
	val address: String? = null,

	@Json(name="contactNumbers")
	val contactNumbers: List<String>? = null,

	@Json(name="city")
	val city: String? = null,

	@Json(name="timing")
	val timing: List<TimingItem>? = null,

	@Json(name="latitude")
	val latitude: String? = null,

	@Json(name="store_name")
	val store_name: String? = null,

	@Json(name="id")
	val id: String? = null,

	@Json(name="type")
	val type: String? = null,

	@Json(name="longitude")
	val longitude: String? = null
)