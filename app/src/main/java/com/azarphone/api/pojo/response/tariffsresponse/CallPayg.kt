package com.azarphone.api.pojo.response.tariffsresponse

import android.os.Parcel
import android.os.Parcelable

data class CallPayg(
	val priceTemplate: String? = null,
	val iconName: String? = null,
	val attributes: List<AttributesItem?>? = null,
	val titleValueLeft: String? = null,
	val title: String? = null,
	val titleValueRight: String? = null
): Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readString(),
			parcel.readString(),
			parcel.createTypedArrayList(AttributesItem),
			parcel.readString(),
			parcel.readString(),
			parcel.readString()) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(priceTemplate)
		parcel.writeString(iconName)
		parcel.writeTypedList(attributes)
		parcel.writeString(titleValueLeft)
		parcel.writeString(title)
		parcel.writeString(titleValueRight)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<CallPayg> {
		override fun createFromParcel(parcel: Parcel): CallPayg {
			return CallPayg(parcel)
		}

		override fun newArray(size: Int): Array<CallPayg?> {
			return arrayOfNulls(size)
		}
	}
}
