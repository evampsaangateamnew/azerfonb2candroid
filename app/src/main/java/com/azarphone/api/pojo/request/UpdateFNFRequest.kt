package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

data class UpdateFNFRequest(
        @Json(name = "actionType")
        val actionType: String? = null,

        @Json(name = "oldMsisdn")
        val oldMsisdn: String? = null,

        @Json(name = "newMsisdn")
        val newMsisdn: String? = null,

        @Json(name = "offeringId")
        val offeringId: String? = null
)