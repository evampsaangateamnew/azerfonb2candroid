package com.azarphone.api.pojo.response.storelocator

import com.azarphone.api.pojo.response.BaseResponse
import com.squareup.moshi.Json

data class StoreLocatorResponse(
		@Json(name="data")
		override var data: Data,

		@Json(name="callStatus")
		override var callStatus: String,

		@Json(name="resultCode")
		override var resultCode: String,

		@Json(name="resultDesc")
		override var resultDesc: String


): BaseResponse<Data>()