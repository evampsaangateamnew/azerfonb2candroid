package com.azarphone.api.pojo.response.coreservices

import android.os.Parcel
import android.os.Parcelable

data class Data(
        var coreServices: List<CoreServicesItem?>? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.createTypedArrayList(CoreServicesItem)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(coreServices)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Data> {
        override fun createFromParcel(parcel: Parcel): Data {
            return Data(parcel)
        }

        override fun newArray(size: Int): Array<Data?> {
            return arrayOfNulls(size)
        }
    }
}
