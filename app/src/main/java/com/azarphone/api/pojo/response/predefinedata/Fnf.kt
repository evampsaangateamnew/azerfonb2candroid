package com.azarphone.api.pojo.response.predefinedata

data class Fnf(
	val maxFNFCount: String? = null,
	val fnFAllowed: String? = null
)
