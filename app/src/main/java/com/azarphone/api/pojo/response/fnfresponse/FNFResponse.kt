package com.azarphone.api.pojo.response.fnfresponse

data class FNFResponse(
        val data: Data? = null,
        val callStatus: String? = null,
        val resultCode: String? = null,
        val resultDesc: String? = null
)
