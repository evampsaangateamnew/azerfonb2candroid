package com.azarphone.api.pojo.response.menuresponse

data class MenuRU(
	val menuHorizontal: List<MenuHorizontalItem?>? = null,
	val menuVertical: List<MenuVerticalItem?>? = null
)
