package com.azarphone.api.pojo.helperpojo.faqhelpers

/**
 * @author Junaid Hassan on 26, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class FAQListHelperModel(
        var question: String? = null,
        var answer: String? = null,
        var sortOrder: String? = null,
        var title: String? = null
)