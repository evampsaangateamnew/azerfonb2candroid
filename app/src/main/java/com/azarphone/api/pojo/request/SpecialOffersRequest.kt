package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

/**
 * @author Junaid Hassan on 25, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class SpecialOffersRequest(
        @Json(name = "offeringName")
        val offeringName: String? = null,

        @Json(name = "offersSpecialIds")
        val specialOfferIds: List<String>? = null,

        @Json(name = "groupIds")
        val groupIds: String? = null
)