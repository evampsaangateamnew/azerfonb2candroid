package com.azarphone.api.pojo.response.tariffsubscriptionresponse

import com.azarphone.api.pojo.response.BaseResponse

data class TariffSubscriptionResponse(
	override var data: Data,
	override var callStatus: String,
	override var resultCode: String,
	override var resultDesc: String
):BaseResponse<Data>()
