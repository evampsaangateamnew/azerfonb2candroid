package com.azarphone.api.pojo.response.menuresponse

data class MenuEN(
	val menuHorizontal: List<MenuHorizontalItem?>? = null,
	val menuVertical: List<MenuVerticalItem?>? = null
)
