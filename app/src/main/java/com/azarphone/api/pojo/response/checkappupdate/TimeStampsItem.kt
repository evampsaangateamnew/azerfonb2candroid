package com.azarphone.api.pojo.response.checkappupdate

data class TimeStampsItem(
	val timeStamp: String? = null,
	val cacheType: String? = null
)
