package com.azarphone.api.pojo.response.savecustomerresponse

data class SelectAmount(
	val klass: List<KlassItem?>? = null,
	val cin: List<CinItem?>? = null
)
