package com.azarphone.api.pojo.response.mysubscriptions.successresponse

data class MySubscriptionsSuccessAfterOfferSubscribeResponse(
	val data: Data? = null,
	val callStatus: String? = null,
	val resultCode: String? = null,
	val resultDesc: String? = null
)
