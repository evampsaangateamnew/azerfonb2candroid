package com.azarphone.api.pojo.response.predefinedata

data class SelectAmount(
	val prepaid: List<PrepaidItem?>? = null
)
