package com.azarphone.api.pojo.response.supplementaryoffers.response

import android.os.Parcel
import android.os.Parcelable

/**
 * @author Junaid Hassan on 29, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class TextWithOutTitle(
        val description: String? = null
): Parcelable {
    constructor(parcel: Parcel) : this(parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(description)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TextWithOutTitle> {
        override fun createFromParcel(parcel: Parcel): TextWithOutTitle {
            return TextWithOutTitle(parcel)
        }

        override fun newArray(size: Int): Array<TextWithOutTitle?> {
            return arrayOfNulls(size)
        }
    }
}