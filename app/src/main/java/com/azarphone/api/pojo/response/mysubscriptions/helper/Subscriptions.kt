package com.azarphone.api.pojo.response.mysubscriptions.helper

import android.os.Parcel
import android.os.Parcelable

/**
 * @author Junaid Hassan on 26, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class Subscriptions(
        val header: HeaderMain? = null,
        val details: SubscriptionsDetailsMain? = null
):Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readParcelable(HeaderMain::class.java.classLoader),
            parcel.readParcelable(SubscriptionsDetailsMain::class.java.classLoader)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(header, flags)
        parcel.writeParcelable(details, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Subscriptions> {
        override fun createFromParcel(parcel: Parcel): Subscriptions {
            return Subscriptions(parcel)
        }

        override fun newArray(size: Int): Array<Subscriptions?> {
            return arrayOfNulls(size)
        }
    }
}