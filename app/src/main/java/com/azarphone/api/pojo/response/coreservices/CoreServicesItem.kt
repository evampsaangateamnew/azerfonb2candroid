package com.azarphone.api.pojo.response.coreservices

import android.os.Parcel
import android.os.Parcelable

data class CoreServicesItem(
        var coreServicesList: List<CoreServicesListItem?>? = null,
        var coreServiceCategory: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.createTypedArrayList(CoreServicesListItem),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(coreServicesList)
        parcel.writeString(coreServiceCategory)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CoreServicesItem> {
        override fun createFromParcel(parcel: Parcel): CoreServicesItem {
            return CoreServicesItem(parcel)
        }

        override fun newArray(size: Int): Array<CoreServicesItem?> {
            return arrayOfNulls(size)
        }
    }
}
