package com.azarphone.api.pojo.request

data class ResendPinRequest(
	val cause: String? = null,
	val msisdn: String? = null
)
