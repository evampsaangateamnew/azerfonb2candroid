package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

data class UsageDetailsRequest(
        @Json(name = "startDate")
        val startDate: String? = null,

        @Json(name = "endDate")
        val endDate: String? = null,

        @Json(name = "accountId")
        val accountId: String? = null,

        @Json(name = "customerId")
        val customerId: String? = null
//        ,
//        @Json(name ="isDownloadPdf")
//        val isDownloadPdf: String? =null,
//        @Json(name ="isSendMail")
//        val isSendMail: String? =null,
//        @Json(name ="mailTo")
//        val mailTo: String? =null

)