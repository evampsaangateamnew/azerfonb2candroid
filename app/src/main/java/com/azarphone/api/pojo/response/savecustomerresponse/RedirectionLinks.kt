package com.azarphone.api.pojo.response.savecustomerresponse

data class RedirectionLinks(
	val onlinePaymentGatewayURLRU: String? = null,
	val onlinePaymentGatewayURLAZ: String? = null,
	val onlinePaymentGatewayURLEN: String? = null
)
