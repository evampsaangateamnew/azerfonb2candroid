package com.azarphone.api.pojo.response.faqresponse

import com.azarphone.api.pojo.response.BaseResponse

data class FAQResponse(
        override var data: Data,
        override var callStatus: String,
        override var resultCode: String,
        override var resultDesc: String
) : BaseResponse<Data>()
