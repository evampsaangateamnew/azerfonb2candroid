package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

data class FnFRequest(
        @Json(name = "offeringId")
        val offeringId: String? = null
)