package com.azarphone.api.pojo.response.makepayment

import com.azarphone.api.pojo.response.BaseResponse

/**
 * @author Umair Mustafa on 26/04/2021
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * umair.mustafa@evampsaanga.com
 */
data class MakePaymentTopUpResponse(
        override var data: Object,
        override var callStatus: String,
        override var resultCode: String,
        override var resultDesc: String) :
        BaseResponse<Object>()