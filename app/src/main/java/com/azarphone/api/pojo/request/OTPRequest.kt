package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

/**
 * @author Junaid Hassan on 12, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class OTPRequest(
        @Json(name = "cause")
        val cause: String? = null,

        @Json(name = "msisdn")
        val msisdn: String? = null
)