package com.azarphone.api.pojo.response.checkappupdate

data class CheckAppUpdateResponse(
	val data: Data? = null,
	val callStatus: String? = null,
	val resultCode: String? = null,
	val resultDesc: String? = null
)
