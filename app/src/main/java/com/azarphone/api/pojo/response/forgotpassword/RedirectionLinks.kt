package com.azarphone.api.pojo.response.forgotpassword

data class RedirectionLinks(
	val onlinePaymentGatewayURLRU: String? = null,
	val onlinePaymentGatewayURLAZ: String? = null,
	val onlinePaymentGatewayURLEN: String? = null
)
