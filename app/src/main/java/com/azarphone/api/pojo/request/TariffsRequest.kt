package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

/**
 * @author Junaid Hassan on 10, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class TariffsRequest(
        @Json(name = "storeId")
        val storeId: String? = null,

        @Json(name = "offeringId")
        val offeringId: String? = null,

        @Json(name = "subscriberType")
        val subscriberType: String? = null,

        @Json(name = "specialTariffIds")
        val specialTariffIds: List<String>? = null,

        @Json(name = "groupIds")
        val groupIds: String? = null
)