package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

data class AddFnfRequest(
        @Json(name = "addMsisdn")
        val addMsisdn: String? = null,

        @Json(name = "offeringId")
        val offeringId: String? = null,

        @Json(name = "isFirstTime")
        val isFirstTime: String? = null

)