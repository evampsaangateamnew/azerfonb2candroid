package com.azarphone.api.pojo.response.usagehistoryresponse

data class Data(
	val summaryList: List<SummaryListItem?>? = null
)
