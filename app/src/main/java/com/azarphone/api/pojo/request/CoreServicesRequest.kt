package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

/**
 * @author Junaid Hassan on 02, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class CoreServicesRequest(
        @Json(name = "accountType")
        val accountType: String? = null,

        @Json(name = "groupType")
        val groupType: String? = null,

        @Json(name = "brand")
        val brand: String? = null,

        @Json(name = "userType")
        val userType: String? = null,

        @Json(name = "isFrom")
        val isFrom: String? = null
)