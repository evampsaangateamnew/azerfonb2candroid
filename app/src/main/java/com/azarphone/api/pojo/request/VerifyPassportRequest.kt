package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

data class VerifyPassportRequest(
		@Json(name = "passportNumber")
		val passportNumber: String? = null,

		@Json(name = "accountId")
		val accountId: String? = null,

		@Json(name = "customerId")
		val customerId: String? = null
)
