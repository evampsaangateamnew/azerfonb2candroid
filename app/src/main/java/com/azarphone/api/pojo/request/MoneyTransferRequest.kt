package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

/**
 * @author Junaid Hassan on 08, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class MoneyTransferRequest(
        @Json(name = "msisdn")
        val transferee: String? = null,

        @Json(name = "amount")
        val amount: String? = null,

        @Json(name = "subscribertype")
        val subscriberType: String? = null

)