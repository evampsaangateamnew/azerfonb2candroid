package com.azarphone.api.pojo.response.tariffsresponse

import android.os.Parcel
import android.os.Parcelable

data class Details(
        val date: Date? = null,
        val textWithOutTitle: TextWithOutTitle? = null,
        val rounding: Rounding? = null,
        val titleSubTitleValueAndDesc: TitleSubTitleValueAndDesc? = null,
        val textWithTitle: TextWithTitle? = null,
        val internationalOnPeak: String? = null,
        val price: List<PriceItem?>? = null,
        val textWithPoints: TextWithPoints? = null,
        val freeResourceValidity: FreeResourceValidity? = null,
        val detailLabel: String? = null,
        val internationalOffPeak: String? = null,
        val time: Time? = null,
        val roamingDetails: RoamingDetails? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readParcelable(Date::class.java.classLoader),
            parcel.readParcelable(TextWithOutTitle::class.java.classLoader),
            parcel.readParcelable(Rounding::class.java.classLoader),
            parcel.readParcelable(TitleSubTitleValueAndDesc::class.java.classLoader),
            parcel.readParcelable(TextWithTitle::class.java.classLoader),
            parcel.readString(),
            parcel.createTypedArrayList(PriceItem),
            parcel.readParcelable(TextWithPoints::class.java.classLoader),
            parcel.readParcelable(FreeResourceValidity::class.java.classLoader),
            parcel.readString(),
            parcel.readString(),
            parcel.readParcelable(Time::class.java.classLoader),
            parcel.readParcelable(RoamingDetails::class.java.classLoader)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(date, flags)
        parcel.writeParcelable(textWithOutTitle, flags)
        parcel.writeParcelable(rounding, flags)
        parcel.writeParcelable(titleSubTitleValueAndDesc, flags)
        parcel.writeParcelable(textWithTitle, flags)
        parcel.writeString(internationalOnPeak)
        parcel.writeTypedList(price)
        parcel.writeParcelable(textWithPoints, flags)
        parcel.writeParcelable(freeResourceValidity, flags)
        parcel.writeString(detailLabel)
        parcel.writeString(internationalOffPeak)
        parcel.writeParcelable(time, flags)
        parcel.writeParcelable(roamingDetails, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Details> {
        override fun createFromParcel(parcel: Parcel): Details {
            return Details(parcel)
        }

        override fun newArray(size: Int): Array<Details?> {
            return arrayOfNulls(size)
        }
    }
}
