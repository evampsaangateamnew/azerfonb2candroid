package com.azarphone.api.pojo.response.nartvsubscriptionsresponse

data class ActivatedPlanStatus(
	val label: String? = null,
	val renewalDate: String? = null,
	val renewalDateLabel: String? = null,
	val startDate: String? = null
)
