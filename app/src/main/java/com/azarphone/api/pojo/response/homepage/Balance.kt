package com.azarphone.api.pojo.response.homepage

data class Balance(
	val postpaid: Postpaid? = null,
	val prepaid: Prepaid? = null
)
