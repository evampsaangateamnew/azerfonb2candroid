package com.azarphone.api.pojo.response.usagedetailsresponse

data class Data(
	val records: List<RecordsItem?>? = null
)
