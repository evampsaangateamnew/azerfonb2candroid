package com.azarphone.api.pojo.response.internetpackresponse

data class Monthly(
    val filters: Filters?,
    val offers: List<Offer?>
)