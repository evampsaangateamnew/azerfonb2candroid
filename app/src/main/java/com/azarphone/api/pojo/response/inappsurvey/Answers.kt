package com.azarphone.api.pojo.response.inappsurvey

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by Muhammad Ahmed on 8/30/2021.
 */


@Parcelize
data class Answers(

    @SerializedName("id") val id: Int,
    @SerializedName("answerTextEN") val answerTextEN: String?,
    @SerializedName("answerTextAZ") val answerTextAZ: String?,
    @SerializedName("answerTextRU") val answerTextRU: String?
): Parcelable