package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

/**
 * @author Junaid Hassan on 18, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class MenuRequest(
        @Json(name = "msisdn")
        val msisdn: String? = null,
        @Json(name = "offeringName")
        val offeringName: String? = null
)