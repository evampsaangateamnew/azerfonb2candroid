package com.azarphone.api.pojo.response.menuresponse

data class Data(
	val MenuEN: MenuEN? = null,
	val MenuAZ: MenuAZ? = null,
	val MenuRU: MenuRU? = null
)
