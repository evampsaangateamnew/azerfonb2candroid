package com.azarphone.api.pojo.response.internetpackresponse

import android.os.Parcel
import android.os.Parcelable
import com.azarphone.api.pojo.response.supplementaryoffers.helper.SupplementaryOffer
import com.azarphone.api.pojo.response.supplementaryoffers.helper.SupplementryOffersDetailsAndDescription
import com.azarphone.api.pojo.response.supplementaryoffers.helper.SupplementryOffersHeaders

data class Offer(
    var header: SupplementryOffersHeaders? = null,
    var details: SupplementryOffersDetailsAndDescription? = null,
    var description: SupplementryOffersDetailsAndDescription? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readParcelable(SupplementryOffersHeaders::class.java.classLoader),
        parcel.readParcelable(SupplementryOffersDetailsAndDescription::class.java.classLoader),
        parcel.readParcelable(SupplementryOffersDetailsAndDescription::class.java.classLoader)
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(header, flags)
        parcel.writeParcelable(details, flags)
        parcel.writeParcelable(description, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SupplementaryOffer> {
        override fun createFromParcel(parcel: Parcel): SupplementaryOffer {
            return SupplementaryOffer(parcel)
        }

        override fun newArray(size: Int): Array<SupplementaryOffer?> {
            return arrayOfNulls(size)
        }
    }
}