package com.azarphone.api.pojo.response.supplementaryoffers.response

import android.os.Parcel
import android.os.Parcelable
import com.azarphone.api.pojo.response.supplementaryoffers.helper.OfferFiltersMain
import com.azarphone.api.pojo.response.supplementaryoffers.helper.SupplementaryOffer

data class Campaign(
        val offers: List<SupplementaryOffer?>? = null,
        val filters: OfferFiltersMain? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.createTypedArrayList(SupplementaryOffer),
            parcel.readParcelable(OfferFiltersMain::class.java.classLoader)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(offers)
        parcel.writeParcelable(filters, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Campaign> {
        override fun createFromParcel(parcel: Parcel): Campaign {
            return Campaign(parcel)
        }

        override fun newArray(size: Int): Array<Campaign?> {
            return arrayOfNulls(size)
        }
    }
}
