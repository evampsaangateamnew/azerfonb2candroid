package com.azarphone.api.pojo.response.supplementaryoffers.response

import android.os.Parcel
import android.os.Parcelable

data class RoamingDetails(
        val descriptionAbove: String? = null,
        val descriptionBelow: String? = null,
        val roamingDetailsCountriesList: List<RoamingDetailsCountriesListItem?>? = null,
        val roamingIcon: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.createTypedArrayList(RoamingDetailsCountriesListItem),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(descriptionAbove)
        parcel.writeString(descriptionBelow)
        parcel.writeString(roamingIcon)
        parcel.writeTypedList(roamingDetailsCountriesList)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RoamingDetails> {
        override fun createFromParcel(parcel: Parcel): RoamingDetails {
            return RoamingDetails(parcel)
        }

        override fun newArray(size: Int): Array<RoamingDetails?> {
            return arrayOfNulls(size)
        }
    }
}
