package com.azarphone.api.pojo.response.supplementaryoffers.response

import android.os.Parcel
import android.os.Parcelable

data class OffersItem(
        val description: Description? = null,
        val header: Header? = null,
        val details: Details? = null
): Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readParcelable(Description::class.java.classLoader),
			parcel.readParcelable(Header::class.java.classLoader),
			parcel.readParcelable(Details::class.java.classLoader)) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeParcelable(description, flags)
		parcel.writeParcelable(header, flags)
		parcel.writeParcelable(details, flags)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<OffersItem> {
		override fun createFromParcel(parcel: Parcel): OffersItem {
			return OffersItem(parcel)
		}

		override fun newArray(size: Int): Array<OffersItem?> {
			return arrayOfNulls(size)
		}
	}
}
