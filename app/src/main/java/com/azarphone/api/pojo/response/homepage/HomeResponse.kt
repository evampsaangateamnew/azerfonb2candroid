package com.azarphone.api.pojo.response.homepage

import com.azarphone.api.pojo.response.BaseResponse

data class HomeResponse(
	override var data: Data,
	override var callStatus: String,
	override var resultCode: String,
	override var resultDesc: String
):BaseResponse<Data>()
