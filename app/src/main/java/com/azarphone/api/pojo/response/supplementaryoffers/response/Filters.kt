package com.azarphone.api.pojo.response.supplementaryoffers.response

import android.os.Parcel
import android.os.Parcelable
import com.azarphone.api.pojo.response.supplementaryoffers.helper.OfferFiltersMain

data class Filters(
        val app: List<OfferFiltersMain?>? = null,
        val desktop: List<OfferFiltersMain?>? = null,
        val tab: List<OfferFiltersMain?>? = null
): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.createTypedArrayList(OfferFiltersMain),
            parcel.createTypedArrayList(OfferFiltersMain),
            parcel.createTypedArrayList(OfferFiltersMain)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(app)
        parcel.writeTypedList(desktop)
        parcel.writeTypedList(tab)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Filters> {
        override fun createFromParcel(parcel: Parcel): Filters {
            return Filters(parcel)
        }

        override fun newArray(size: Int): Array<Filters?> {
            return arrayOfNulls(size)
        }
    }
}
