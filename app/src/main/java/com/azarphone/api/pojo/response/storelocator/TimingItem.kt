package com.azarphone.api.pojo.response.storelocator

import com.squareup.moshi.Json

data class TimingItem(

	@Json(name="timings")
	val timings: String? = null,

	@Json(name="day")
	val day: String? = null
)