package com.azarphone.api.pojo.response.supplementaryoffers.helper

import android.os.Parcel
import android.os.Parcelable
import com.azarphone.api.pojo.response.supplementaryoffers.response.Countries

/**
 * @author Junaid Hassan on 24, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class RoamingsOffer(
        val offers: List<SupplementaryOffer?>? = null,
        val countries: List<Countries?>? = null,
        val filters: OfferFiltersMain? = null
): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.createTypedArrayList(SupplementaryOffer),
            parcel.createTypedArrayList(Countries),
            parcel.readParcelable(OfferFiltersMain::class.java.classLoader)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(offers)
        parcel.writeTypedList(countries)
        parcel.writeParcelable(filters, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RoamingsOffer> {
        override fun createFromParcel(parcel: Parcel): RoamingsOffer {
            return RoamingsOffer(parcel)
        }

        override fun newArray(size: Int): Array<RoamingsOffer?> {
            return arrayOfNulls(size)
        }
    }
}