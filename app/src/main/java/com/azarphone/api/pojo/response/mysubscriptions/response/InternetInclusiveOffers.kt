package com.azarphone.api.pojo.response.mysubscriptions.response

import android.os.Parcel
import android.os.Parcelable
import com.azarphone.api.pojo.response.mysubscriptions.helper.Subscriptions

data class InternetInclusiveOffers(
	val offers: List<Subscriptions?>? = null
	/*val inclusiveUsageList: List<Any?>? = null*/
): Parcelable {
	constructor(parcel: Parcel) : this(parcel.createTypedArrayList(Subscriptions)) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeTypedList(offers)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<InternetInclusiveOffers> {
		override fun createFromParcel(parcel: Parcel): InternetInclusiveOffers {
			return InternetInclusiveOffers(parcel)
		}

		override fun newArray(size: Int): Array<InternetInclusiveOffers?> {
			return arrayOfNulls(size)
		}
	}
}
