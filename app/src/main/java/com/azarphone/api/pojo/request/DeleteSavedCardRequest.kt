package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

/**
 * @author Umair Mustafa on 26/04/2021
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * umair.mustafa@evampsaanga.com
 */
data class DeleteSavedCardRequest(
        @Json(name = "id")
        val id: String? = null,
)
