package com.azarphone.api.pojo.response.mysubscriptions.response

data class Data(
		var call: Call? = null,
		var hybrid: Hybrid? = null,
		var roaming: Roaming? = null,
		var smsInclusiveOffers: SmsInclusiveOffers? = null,
		var sms: Sms? = null,
		var campaign: Campaign? = null,
		var tm: Tm? = null,
		var voiceInclusiveOffers: VoiceInclusiveOffers? = null,
		var internetInclusiveOffers: InternetInclusiveOffers? = null,
		var internet: Internet? = null
)
