package com.azarphone.api.pojo.helperpojo

/**
 * @author Junaid Hassan on 28, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class HomePageFreeResourcesHelperModel(
        var resourcesTitleLabel: String? = null,
        var resourceType: String? = null,
        var resourceInitialUnits: String? = null,
        var resourceRemainingUnits: String? = null,
        var resourceUnitName: String? = null,
        var resourceDiscountedText: String? = null,
        var remainingFormatted: String? = null,
        var sortOrder: String? = null
)
