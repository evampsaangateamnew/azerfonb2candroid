package com.azarphone.api.pojo.response.tariffsresponse

import android.os.Parcel
import android.os.Parcelable

data class Internet(
        val subTitle: String? = null,
        val iconName: String? = null,
        val attributes: List<AttributesItem?>? = null,
        val subTitleValue: String? = null,
        val title: String? = null,
        val titleValue: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.createTypedArrayList(AttributesItem),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(subTitle)
        parcel.writeString(iconName)
        parcel.writeString(subTitleValue)
        parcel.writeString(title)
        parcel.writeString(titleValue)
        parcel.writeTypedList(attributes)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Internet> {
        override fun createFromParcel(parcel: Parcel): Internet {
            return Internet(parcel)
        }

        override fun newArray(size: Int): Array<Internet?> {
            return arrayOfNulls(size)
        }
    }
}
