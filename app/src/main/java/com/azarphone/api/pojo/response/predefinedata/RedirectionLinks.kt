package com.azarphone.api.pojo.response.predefinedata

data class RedirectionLinks(
	val onlinePaymentGatewayURL_RU: String? = null,
	val onlinePaymentGatewayURL_AZ: String? = null,
	val onlinePaymentGatewayURL_EN: String? = null
)
