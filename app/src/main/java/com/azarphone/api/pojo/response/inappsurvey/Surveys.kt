package com.azarphone.api.pojo.response.inappsurvey

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by Muhammad Ahmed on 8/30/2021.
 */

@Parcelize
data class Surveys(

    @SerializedName("id") val id: Int,
    @SerializedName("screenName") val screenName: String?,
    @SerializedName("timeLimit") val timeLimit: Int,
    @SerializedName("visitLimit") val visitLimit: Int,
    @SerializedName("surveyLimit") val surveyLimit: Int,
    @SerializedName("surveyCount") val surveyCount: Int,
    @SerializedName("onTransactionCompleteEn") val onTransactionCompleteEn: String?,
    @SerializedName("onTransactionCompleteAz") val onTransactionCompleteAz: String?,
    @SerializedName("onTransactionCompleteRu") val onTransactionCompleteRu: String?,
    @SerializedName("questions") val questions: List<Questions>?,
    @SerializedName("commentEnable") val commentEnable: String?,
    @SerializedName("commentsTitleEn") val commentsTitleEn: String?,
    @SerializedName("commentsTitleAz") val commentsTitleAz: String?,
    @SerializedName("commentsTitleRu") val commentsTitleRu: String?,
    @SerializedName("titleEn") val titleEn: String?,
    @SerializedName("customerType") val customerType: String?

) : Parcelable
{
    fun findQuestion(questionId: Int): Questions? {
        questions?.forEach {
            if (it.id == questionId) {
                return it
            }
        }
        return null
    }
}