package com.azarphone.api.pojo.response.changebillinglanguagereponse

import com.azarphone.api.pojo.response.BaseResponse

data class ChangeBillingResponse(
	override var data: Data,
	override var callStatus: String,
	override var resultCode: String,
	override var resultDesc: String
): BaseResponse<Data>()
