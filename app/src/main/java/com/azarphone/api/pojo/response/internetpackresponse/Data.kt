package com.azarphone.api.pojo.response.internetpackresponse

data class Data(
    val allBundles: AllPacks?,
    val dailyBundles: Daily?,
    val monthlyBundles: Monthly?,
    val weeklyBundles: Weekly?,
    val hourlyBundles: HourlyPacks?
)