package com.azarphone.api.pojo.response.savecustomerresponse

data class CinItem(
	val amount: String? = null,
	val currency: String? = null
)
