package com.azarphone.api.pojo.response.supplementaryoffers.response

import android.os.Parcel
import android.os.Parcelable

data class Description(
        val date: Date? = null,
		/*val textWithOutTitle: Any? = null,*/
		val price: Price? = null,
        val textWithPoints: TextWithPoints? = null,
        val freeResourceValidity: FreeResourceValidity? = null,
        val rounding: Rounding? = null,
        val titleSubTitleValueAndDesc: TitleSubTitleValueAndDesc? = null,
        val time: Time? = null,
        val textWithTitle: TextWithTitle? = null,
        val roamingDetails: RoamingDetails? = null
): Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readParcelable(Date::class.java.classLoader),
			/*textwithouttitle*/
			parcel.readParcelable(Price::class.java.classLoader),
			parcel.readParcelable(TextWithPoints::class.java.classLoader),
			parcel.readParcelable(FreeResourceValidity::class.java.classLoader),
			parcel.readParcelable(Rounding::class.java.classLoader),
			parcel.readParcelable(TitleSubTitleValueAndDesc::class.java.classLoader),
			parcel.readParcelable(Time::class.java.classLoader),
			parcel.readParcelable(TextWithTitle::class.java.classLoader),
			parcel.readParcelable(RoamingDetails::class.java.classLoader)) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeParcelable(date, flags)
		parcel.writeParcelable(price, flags)
		parcel.writeParcelable(textWithPoints, flags)
		parcel.writeParcelable(freeResourceValidity, flags)
		parcel.writeParcelable(rounding, flags)
		parcel.writeParcelable(titleSubTitleValueAndDesc, flags)
		parcel.writeParcelable(time, flags)
		parcel.writeParcelable(textWithTitle, flags)
		parcel.writeParcelable(roamingDetails, flags)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<Description> {
		override fun createFromParcel(parcel: Parcel): Description {
			return Description(parcel)
		}

		override fun newArray(size: Int): Array<Description?> {
			return arrayOfNulls(size)
		}
	}
}
