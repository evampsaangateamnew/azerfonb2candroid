package com.azarphone.api.pojo.response.notification

data class NotificationsListItem(
	val actionType: String? = null,
	val datetime: String? = null,
	val btnTxt: String? = null,
	val icon: String? = null,
	val actionID: String? = null,
	val message: String? = null
)
