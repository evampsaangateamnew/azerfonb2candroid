package com.azarphone.api.pojo.response.forgotpassword

data class MoneyTransfer(
	val termsAndCondition: TermsAndCondition? = null,
	val selectAmount: SelectAmount? = null
)
