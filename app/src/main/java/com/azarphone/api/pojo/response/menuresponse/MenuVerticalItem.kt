package com.azarphone.api.pojo.response.menuresponse

data class MenuVerticalItem(
	val identifier: String? = null,
	val sortOrder: String? = null,
	val title: String? = null,
	val items: List<Items> ? = null
)
