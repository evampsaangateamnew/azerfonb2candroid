package com.azarphone.api.pojo.response.predefinedata

data class MoneyTransfer(
	val termsAndCondition: TermsAndCondition? = null,
	val selectAmount: SelectAmount? = null
)
