package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

/**
 * @author Junaid Hassan on 13, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class OTPVerifyRequest(
    @Json(name = "cause")
    val cause: String? = null,

    @Json(name = "msisdn")
    val msisdn: String? = null,

    @Json(name = "pin")
    val pin: String? = null
)