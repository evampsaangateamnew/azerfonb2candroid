package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

data class ExchangeServiceRequest(
        @Json(name = "isDataToVoiceConversion")
        val isDataToVoiceConversion: Boolean? = null,

        @Json(name = "supplementaryOfferingIds")
        val supplementaryOfferingIds: String? = null,

        @Json(name = "voiceValue")
        val voiceValue: String? = null,

        @Json(name = "dataValue")
        val dataValue: String? = null,

        @Json(name = "offeringId")
        val offeringId: String? = null
)