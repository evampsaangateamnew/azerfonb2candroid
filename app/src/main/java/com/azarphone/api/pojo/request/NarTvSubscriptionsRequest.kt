package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

data class NarTvSubscriptionsRequest(
        @Json(name = "offeringId")
        val offeringId: String? = null,

        @Json(name = "accountType")
        val accountType: String? = null,

        @Json(name = "groupType")
        val groupType: String? = null,

        @Json(name = "brand")
        val brand: String? = null,

        @Json(name = "userType")
        val userType: String? = null,

        @Json(name = "isFrom")
        val isFrom: String? = null
)