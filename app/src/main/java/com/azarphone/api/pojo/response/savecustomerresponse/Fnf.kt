package com.azarphone.api.pojo.response.savecustomerresponse

data class Fnf(
	val maxFNFCount: String? = null,
	val fnFAllowed: String? = null
)
