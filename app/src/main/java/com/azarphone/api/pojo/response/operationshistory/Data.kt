package com.azarphone.api.pojo.response.operationshistory

data class Data(
	val records: List<RecordsItem?>? = null
)
