package com.azarphone.api.pojo.helperpojo.menus

/**
 * @author Junaid Hassan on 18, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class HorizontalMenuHelperModel(
        var identifier: String? = null,
        var title: String? = null,
        var sortOrder: String? = null
)