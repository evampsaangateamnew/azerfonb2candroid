package com.azarphone.api.pojo.response.storelocator

import com.squareup.moshi.Json

data class Data(

	@Json(name="city")
	val city: List<String>? = null,

	@Json(name="stores")
	val stores: List<StoresItem>? = null,

	@Json(name="type")
	val type: List<String>? = null
)