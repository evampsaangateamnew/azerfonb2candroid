package com.azarphone.api.pojo.response.supplementaryoffers.helper

import android.os.Parcel
import android.os.Parcelable

/**
 * @author Junaid Hassan on 23, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class OfferFiltersMain(
        val app: List<OfferFilter?>? = null,
        val desktop: List<OfferFilter?>? = null,
        val tab: List<OfferFilter?>? = null
): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.createTypedArrayList(OfferFilter),
            parcel.createTypedArrayList(OfferFilter),
            parcel.createTypedArrayList(OfferFilter)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(app)
        parcel.writeTypedList(desktop)
        parcel.writeTypedList(tab)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OfferFiltersMain> {
        override fun createFromParcel(parcel: Parcel): OfferFiltersMain {
            return OfferFiltersMain(parcel)
        }

        override fun newArray(size: Int): Array<OfferFiltersMain?> {
            return arrayOfNulls(size)
        }
    }
}