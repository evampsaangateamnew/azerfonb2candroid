package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

/**
 * @author Junaid Hassan on 03, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class CoreServiceProcessRequest(

        @Json(name = "actionType")
        val actionType: String? = null,

        @Json(name = "offeringId")
        val offeringId: String? = null,

        @Json(name = "groupType")
        val groupType: String? = null,

        @Json(name = "number")
        val number: String? = null,

        @Json(name = "accountType")
        val accountType: String? = null
)