package com.azarphone.api.pojo.response.forgotpassword

data class Topup(
	val moneyTransfer: MoneyTransfer? = null,
	val getLoan: GetLoan? = null
)
