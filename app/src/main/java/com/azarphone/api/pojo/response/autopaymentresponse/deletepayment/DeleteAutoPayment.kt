package com.azarphone.api.pojo.response.autopaymentresponse.deletepayment

import com.squareup.moshi.Json

data class DeleteAutoPayment(
        @Json(name = "id")
        val id: Int,
)
