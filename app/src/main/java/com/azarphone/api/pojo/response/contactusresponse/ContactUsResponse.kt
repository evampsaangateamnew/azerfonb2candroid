package com.azarphone.api.pojo.response.contactusresponse

import com.azarphone.api.pojo.response.BaseResponse

data class ContactUsResponse(
	override var data: Data,
	override var callStatus: String,
	override var resultCode: String,
	override var resultDesc: String
):BaseResponse<Data>()
