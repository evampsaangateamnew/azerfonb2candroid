package com.azarphone.api.pojo.response.updateemailreponse

import com.azarphone.api.pojo.response.BaseResponse

data class UpdateEmailResponse(
        override var  data: Data,
        override var  callStatus: String,
        override var  resultCode: String,
        override var  resultDesc: String
): BaseResponse<Data>()