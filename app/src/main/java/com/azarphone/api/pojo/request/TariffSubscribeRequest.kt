package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

/**
 * @author Junaid Hassan on 20, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class TariffSubscribeRequest(
        @Json(name = "tariffName")
        val tariffName: String? = null,

        @Json(name = "offeringId")
        val offeringId: String? = null,

        @Json(name = "actionType")
        val actionType: String? = null
)