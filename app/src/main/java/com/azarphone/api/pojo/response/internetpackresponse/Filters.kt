package com.azarphone.api.pojo.response.internetpackresponse

data class Filters(
    val app: List<App?>,
    val desktop: List<Desktop?>
)