package com.azarphone.api.pojo.response.otpgetresponse

import com.azarphone.api.pojo.response.BaseResponse

data class OTPResponse(
        override var data: Data,
        override var callStatus: String,
        override var resultCode: String,
        override var resultDesc: String
) : BaseResponse<Data>()
