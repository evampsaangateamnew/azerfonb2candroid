package com.azarphone.api.pojo.helperpojo.menus

/**
 * @author Junaid Hassan on 20, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class HorizontalChildMenusHelperModel(
        var identifier: String,
        var title: String,
        var sortOrder: String
)