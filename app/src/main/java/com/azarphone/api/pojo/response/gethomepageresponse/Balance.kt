package com.azarphone.api.pojo.response.gethomepageresponse

data class Balance(
	val postpaid: Postpaid? = null,
	val prepaid: Prepaid? = null
)
