package com.azarphone.api.pojo.response.updatefnfresponse

import com.azarphone.api.pojo.response.fnfresponse.FnfListItem

data class Data(
	val fnfList: List<FnfListItem?>? = null
)
