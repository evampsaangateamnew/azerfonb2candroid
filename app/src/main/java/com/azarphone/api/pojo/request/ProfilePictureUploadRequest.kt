package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

data class ProfilePictureUploadRequest(

        @Json(name = "image")
        val image: String? = null,

        @Json(name = "ext")
        val ext: String? = null,

        @Json(name = "actionType")
        val actionType: String? = null

)