package com.azarphone.api.pojo.response.tariffsresponse

data class Data(
	val tariffResponseList: List<TariffResponseListItem?>? = null
)
