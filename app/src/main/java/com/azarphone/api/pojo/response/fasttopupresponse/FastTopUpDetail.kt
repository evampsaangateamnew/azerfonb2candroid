package com.azarphone.api.pojo.response.fasttopupresponse

data class FastTopUpDetail(
        val id: String,
        val cardType: String,
        val amount: String,
        val topupNumber: String,
        val paymentKey: String
)
