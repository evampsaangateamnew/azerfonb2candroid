package com.azarphone.api.pojo.response.forgotpassword

data class ForgotPasswordResponse(
	val data: Data? = null,
	val callStatus: String? = null,
	val resultCode: String? = null,
	val resultDesc: String? = null
)
