package com.azarphone.api.pojo.response.gethomepageresponse

data class Installments(
	val installmentTitle: String? = null,
	val installments: List<Any?>? = null,
	val installmentDescription: Any? = null
)
