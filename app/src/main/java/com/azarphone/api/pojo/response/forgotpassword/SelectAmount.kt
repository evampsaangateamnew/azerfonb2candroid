package com.azarphone.api.pojo.response.forgotpassword

data class SelectAmount(
	val klass: List<KlassItem?>? = null,
	val cin: List<CinItem?>? = null
)
