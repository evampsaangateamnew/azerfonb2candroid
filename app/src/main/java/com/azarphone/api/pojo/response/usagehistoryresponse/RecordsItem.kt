package com.azarphone.api.pojo.response.usagehistoryresponse

data class RecordsItem(
	val service_type: String? = null,
	val unit: String? = null,
	val chargeable_amount: String? = null,
	val total_usage: String? = null,
	val count: String? = null
)
