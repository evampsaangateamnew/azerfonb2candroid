package com.azarphone.api.pojo.response.menuresponse

/**
 * @author Muhammad Ahmed on 15, July, 2021
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * muhammad.ahmed@evampsaanga.com
 */
data class Item(
        var title: String? = null,
        var sortOrder: String? = null,
        var identifier: String? = null,
)
