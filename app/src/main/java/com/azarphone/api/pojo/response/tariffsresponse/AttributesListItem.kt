package com.azarphone.api.pojo.response.tariffsresponse

import android.os.Parcel
import android.os.Parcelable

data class AttributesListItem(
	val title: String? = null,
	val value: String? = null
): Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readString(),
			parcel.readString()) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(title)
		parcel.writeString(value)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<AttributesListItem> {
		override fun createFromParcel(parcel: Parcel): AttributesListItem {
			return AttributesListItem(parcel)
		}

		override fun newArray(size: Int): Array<AttributesListItem?> {
			return arrayOfNulls(size)
		}
	}
}
