package com.azarphone.api.pojo.response.updatefnfresponse

import com.azarphone.api.pojo.response.BaseResponse

data class UpdateFNFResponse(
	override var data: Data,
	override var  callStatus: String,
	override var  resultCode: String,
	override var  resultDesc: String
):BaseResponse<Data>()
