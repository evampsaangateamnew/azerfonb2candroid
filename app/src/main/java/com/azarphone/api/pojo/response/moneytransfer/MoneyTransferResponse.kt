package com.azarphone.api.pojo.response.moneytransfer

import com.azarphone.api.pojo.response.BaseResponse

data class MoneyTransferResponse(
	override var data: Data,
	override var callStatus: String,
	override var resultCode: String,
	override var resultDesc: String
):BaseResponse<Data>()
