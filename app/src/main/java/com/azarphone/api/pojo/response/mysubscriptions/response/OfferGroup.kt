package com.azarphone.api.pojo.response.mysubscriptions.response

data class OfferGroup(
	val groupName: String? = null,
	val groupValue: String? = null
)
