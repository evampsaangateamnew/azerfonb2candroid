package com.azarphone.api.pojo.response.savecustomerresponse

data class TermsAndCondition(
	val klass: List<KlassItem?>? = null,
	val cin: List<CinItem?>? = null
)
