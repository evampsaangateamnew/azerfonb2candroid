package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

/**
 * @author Junaid Hassan on 07, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class SupplementaryOfferSubscribeRequest(
        @Json(name = "offeringName")
        val offeringName: String? = null,

        @Json(name = "offeringId")
        val offeringId: String? = null,

        @Json(name = "actionType")
        val actionType: String? = null
)