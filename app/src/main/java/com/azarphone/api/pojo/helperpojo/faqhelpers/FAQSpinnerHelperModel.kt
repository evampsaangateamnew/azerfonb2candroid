package com.azarphone.api.pojo.helperpojo.faqhelpers

/**
 * @author Junaid Hassan on 26, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class FAQSpinnerHelperModel(
        val title: String? = null
)