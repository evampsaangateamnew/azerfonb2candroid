package com.azarphone.api.pojo.response.autopaymentresponse

data class ScheduledPaymentData(var data: List<ScheduledPaymentDataItem?>)
