package com.azarphone.api.pojo.request

data class ForgotPasswordRequest(
	val password: String,
	val temp: String,
	val confirmPassword: String,
	val msisdn: String
)
