package com.azarphone.api.pojo.response.forgotpassword

data class CinItem(
	val amount: String? = null,
	val currency: String? = null
)
