package com.azarphone.api.pojo.response.savecustomerresponse

data class PrimaryOffering(
	val offeringName: String? = null,
	val offeringShortName: String? = null,
	val effectiveTime: String? = null,
	val offeringId: String? = null,
	val networkType: String? = null,
	val offeringCode: Any? = null,
	val expiredTime: String? = null,
	val status: String? = null
)
