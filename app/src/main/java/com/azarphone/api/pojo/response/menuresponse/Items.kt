package com.azarphone.api.pojo.response.menuresponse

/**
 * @author Junaid Hassan on 20, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class Items(
        var title: String? = null,
        var sortOrder: String? = null,
        var identifier: String? = null,
        val item: List<Item> ? = null
)
