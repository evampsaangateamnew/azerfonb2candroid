package com.azarphone.api.pojo.response.operationshistory

import com.azarphone.api.pojo.response.BaseResponse

data class OperationHistoryResponse(
        override var data: Data,
        override var callStatus: String,
        override var resultCode: String,
        override var resultDesc: String
) : BaseResponse<Data>()
