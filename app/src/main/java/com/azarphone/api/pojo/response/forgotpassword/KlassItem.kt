package com.azarphone.api.pojo.response.forgotpassword

data class KlassItem(
	val amount: String? = null,
	val currency: String? = null
)
