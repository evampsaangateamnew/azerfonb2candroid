package com.azarphone.api.pojo.response.predefinedata

data class ExchangeService(
	val dov: Dov? = null,
	val vod: Vod? = null
)
