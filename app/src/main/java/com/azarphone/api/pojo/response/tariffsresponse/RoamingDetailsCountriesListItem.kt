package com.azarphone.api.pojo.response.tariffsresponse

import android.os.Parcel
import android.os.Parcelable

data class RoamingDetailsCountriesListItem(
	val flag: String? = null,
	val operatorList: List<String?>? = null,
	val countryName: String? = null
): Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readString(),
			parcel.createStringArrayList(),
			parcel.readString()) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(flag)
		parcel.writeStringList(operatorList)
		parcel.writeString(countryName)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<RoamingDetailsCountriesListItem> {
		override fun createFromParcel(parcel: Parcel): RoamingDetailsCountriesListItem {
			return RoamingDetailsCountriesListItem(parcel)
		}

		override fun newArray(size: Int): Array<RoamingDetailsCountriesListItem?> {
			return arrayOfNulls(size)
		}
	}
}
