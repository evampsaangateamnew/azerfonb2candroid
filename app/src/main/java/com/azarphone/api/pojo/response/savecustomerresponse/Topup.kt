package com.azarphone.api.pojo.response.savecustomerresponse

data class Topup(
	val moneyTransfer: MoneyTransfer? = null,
	val getLoan: GetLoan? = null
)
