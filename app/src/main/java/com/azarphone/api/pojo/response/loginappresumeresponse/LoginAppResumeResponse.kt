package com.azarphone.api.pojo.response.loginappresumeresponse

import com.azarphone.api.pojo.response.BaseResponse

data class LoginAppResumeResponse(
        override var data: Data,
        override var callStatus: String,
        override var resultCode: String,
        override var resultDesc: String
):BaseResponse<Data>()