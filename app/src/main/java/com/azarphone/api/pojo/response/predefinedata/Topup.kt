package com.azarphone.api.pojo.response.predefinedata

data class Topup(
		val moneyTransfer: MoneyTransfer? = null,
		val getLoan: GetLoan? = null,
		val plasticCard: PlasticCard? = null
)
