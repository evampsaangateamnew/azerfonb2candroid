package com.azarphone.api.pojo.response.predefinedata

data class PrepaidItem(
	val amount: String? = null,
	val currency: String? = null
)
