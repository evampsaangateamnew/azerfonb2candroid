package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

data class UpdateEmailRequest(
        @Json(name = "email")
        val email: String? = null
)