package com.azarphone.api.pojo.response.savecustomerresponse

data class MoneyTransfer(
	val termsAndCondition: TermsAndCondition? = null,
	val selectAmount: SelectAmount? = null
)
