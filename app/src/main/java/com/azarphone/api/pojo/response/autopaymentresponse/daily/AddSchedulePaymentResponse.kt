package com.azarphone.api.pojo.response.autopaymentresponse.daily

import com.azarphone.api.pojo.response.BaseResponse

data class AddSchedulePaymentResponse(
        override var data: Object,
        override var callStatus: String,
        override var resultCode: String,
        override var resultDesc: String) :
        BaseResponse<Object>()