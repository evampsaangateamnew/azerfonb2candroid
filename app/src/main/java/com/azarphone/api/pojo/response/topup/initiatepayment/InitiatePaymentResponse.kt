package com.azarphone.api.pojo.response.topup.initiatepayment

import com.azarphone.api.pojo.response.BaseResponse

data class InitiatePaymentResponse(
        override var data: InitiatePaymentData,
        override var callStatus: String,
        override var resultCode: String,
        override var resultDesc: String
) : BaseResponse<InitiatePaymentData>()
