package com.azarphone.api.pojo.response.gethomepageresponse

data class Mrc(
	val mrcDateLabel: String? = null,
	val mrcType: String? = null,
	val mrcInitialDate: String? = null,
	val mrcStatus: String? = null,
	val mrcTitleLabel: String? = null,
	val mrcTitleValue: String? = null,
	val mrcLimit: String? = null,
	val mrcCurrency: String? = null,
	val mrcDate: String? = null
)
