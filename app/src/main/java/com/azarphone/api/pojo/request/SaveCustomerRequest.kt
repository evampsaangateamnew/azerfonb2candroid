package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

data class SaveCustomerRequest(
        @Json(name = "msisdn")
        val msisdn: String? = null,

        @Json(name = "iP")
        val iP: String? = null,

        @Json(name = "password")
        val password: String? = null,

        @Json(name = "confirm_password")
        val confirm_password: String? = null,

        @Json(name = "temp")
        val temp: String? = null,

        @Json(name = "terms_and_conditions")
        val terms_and_conditions: String? = null
)
