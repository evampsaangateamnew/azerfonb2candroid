package com.azarphone.api.pojo.response.inappsurvey

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by Muhammad Ahmed on 8/30/2021.
 */


@Parcelize
data class InAppSurvey(

    @SerializedName("callStatus") val callStatus: String?,
    @SerializedName("resultCode") val resultCode: String?,
    @SerializedName("resultDesc") val resultDesc: String?,
    @SerializedName("data") val data: Data?
): Parcelable