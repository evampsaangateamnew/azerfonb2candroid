package com.azarphone.api.pojo.response.predefinedata

data class PredefinedData(
	val exchangeService: ExchangeService? = null,
	val redirectionLinks: RedirectionLinks? = null,
	val topup: Topup? = null,
	val callStatus: String? = null,
	val resultCode: String? = null,
	val tnc: Tnc? = null,
	val narTv: NarTv? = null,
	val resultDesc: String? = null,
	val liveChat: String? = null,
	val tariffMigrationPrices: List<TariffMigrationPricesItem?>? = null,
	val fnf: Fnf? = null
	,
	val cevirErrorMessage: String? = null,
	val cevirEligibleTariffs: List<String?>? = null

)
