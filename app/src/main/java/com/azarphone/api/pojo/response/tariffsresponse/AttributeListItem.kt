package com.azarphone.api.pojo.response.tariffsresponse

import android.os.Parcel
import android.os.Parcelable

data class AttributeListItem(
	val unit: String? = null,
	val description: String? = null,
	val title: String? = null,
	val value: String? = null,
	val iconMap: String? = null
): Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString()) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(unit)
		parcel.writeString(description)
		parcel.writeString(title)
		parcel.writeString(value)
		parcel.writeString(iconMap)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<AttributeListItem> {
		override fun createFromParcel(parcel: Parcel): AttributeListItem {
			return AttributeListItem(parcel)
		}

		override fun newArray(size: Int): Array<AttributeListItem?> {
			return arrayOfNulls(size)
		}
	}
}
