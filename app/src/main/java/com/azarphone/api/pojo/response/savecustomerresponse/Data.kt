package com.azarphone.api.pojo.response.savecustomerresponse

import com.azarphone.api.pojo.response.customerdata.CustomerData
import com.azarphone.api.pojo.response.predefinedata.PredefinedData

data class Data(
        val supplementaryOfferingList: List<SupplementaryOfferingListItem?>? = null,
        val promoMessage: String? = null,
        val customerData: CustomerData? = null,
        val primaryOffering: PrimaryOffering? = null,
        val predefinedData: PredefinedData? = null
)
