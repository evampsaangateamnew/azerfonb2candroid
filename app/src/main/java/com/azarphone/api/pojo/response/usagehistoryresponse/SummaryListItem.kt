package com.azarphone.api.pojo.response.usagehistoryresponse

data class SummaryListItem(
	val totalCharge: String? = null,
	val records: List<RecordsItem?>? = null,
	val name: String? = null,
	val totalUsage: String? = null
)
