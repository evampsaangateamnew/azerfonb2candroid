package com.azarphone.api.pojo.response.fcmtoken

data class Data(
	val ringingStatus: String? = null,
	val isEnable: String? = null
)
