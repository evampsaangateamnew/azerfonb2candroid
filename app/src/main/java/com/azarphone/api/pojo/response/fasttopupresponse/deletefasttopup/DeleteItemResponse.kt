package com.azarphone.api.pojo.response.fasttopupresponse.deletefasttopup

import com.azarphone.api.pojo.response.BaseResponse

data class DeleteItemResponse(override var data: Object,
                              override var callStatus: String,
                              override var resultCode: String,
                              override var resultDesc: String) :
        BaseResponse<Object>()