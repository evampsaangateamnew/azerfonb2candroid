package com.azarphone.api.pojo.response.mysubscriptions.response

import android.os.Parcel
import android.os.Parcelable

data class UsageItem(
        val unit: String? = null,
        val iconName: String? = null,
        val remainingTitle: String? = null,
        val renewalTitle: String? = null,
        val totalUsage: String? = null,
        val renewalDate: String? = null,
        val activationDate: String? = null,
        val remainingUsage: String? = null,
        val type: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(unit)
        parcel.writeString(iconName)
        parcel.writeString(remainingTitle)
        parcel.writeString(renewalTitle)
        parcel.writeString(totalUsage)
        parcel.writeString(renewalDate)
        parcel.writeString(activationDate)
        parcel.writeString(remainingUsage)
        parcel.writeString(type)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UsageItem> {
        override fun createFromParcel(parcel: Parcel): UsageItem {
            return UsageItem(parcel)
        }

        override fun newArray(size: Int): Array<UsageItem?> {
            return arrayOfNulls(size)
        }
    }
}
