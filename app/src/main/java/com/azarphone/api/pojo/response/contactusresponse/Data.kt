package com.azarphone.api.pojo.response.contactusresponse

data class Data(
	val website: String? = null,
	val address: String? = null,
	val addressLat: String? = null,
	val googleLink: String? = null,
	val instagram: String? = null,
	val facebookLink: String? = null,
	val addressLong: String? = null,
	val phone: String? = null,
	val linkedinLink: String? = null,
	val twitterLink: String? = null,
	val youtubeLink: String? = null,
	val fax: String? = null,
	val email: String? = null,
	val customerCareNo: String? = null,
	val inviteFriendText: String? = null
)
