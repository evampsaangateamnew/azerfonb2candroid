package com.azarphone.api.pojo.response.homepage

data class Data(
	val notificationUnreadCount: String? = null,
	val balance: Balance? = null,
	val installments: Installments? = null,
	val freeResources: FreeResources? = null,
	val credit: Credit? = null,
	val mrc: Mrc? = null
)
