package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

data class ChangeBillingRequest(
        @Json(name = "language")
        val language: String? = null
)