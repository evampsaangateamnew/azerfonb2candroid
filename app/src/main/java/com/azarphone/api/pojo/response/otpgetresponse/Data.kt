package com.azarphone.api.pojo.response.otpgetresponse

data class Data(
	val pin: String? = null
)
