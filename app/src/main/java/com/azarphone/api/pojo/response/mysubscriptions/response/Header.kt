package com.azarphone.api.pojo.response.mysubscriptions.response

import android.os.Parcel
import android.os.Parcelable

data class Header(
        val validityTitle: String? = null,
        val validityValue: String? = null,
        val hideFor: String? = null,
        val attributeList: List<AttributeListItem?>? = null,
        val usage: List<UsageItem?>? = null,
        val btnDeactivate: String? = null,
        val validityInformation: String? = null,
        val type: String? = null,
        /*val offerGroup: Any? = null,*/
        val price: String? = null,
        val offerLevel: String? = null,
        val isFreeResource: String? = null,
        val id: String? = null,
        val offerName: String? = null,
        val preReqOfferId: String? = null,
        val appOfferFilter: String? = null,
        val stickerLabel: String? = null,
        val visibleFor: String? = null,
        val offeringId: String? = null,
        val sortOrderMS: String? = null,
        val stickerColorCode: String? = null,
        val sortOrder: String? = null,
        val isTopUp: String? = null,
        val btnRenew: String? = null,
        val status: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.createTypedArrayList(AttributeListItem),
            parcel.createTypedArrayList(UsageItem),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(validityTitle)
        parcel.writeString(validityValue)
        parcel.writeString(hideFor)
        parcel.writeTypedList(attributeList)
        parcel.writeTypedList(usage)
        parcel.writeString(btnDeactivate)
        parcel.writeString(validityInformation)
        parcel.writeString(type)
        parcel.writeString(price)
        parcel.writeString(offerLevel)
        parcel.writeString(isFreeResource)
        parcel.writeString(id)
        parcel.writeString(offerName)
        parcel.writeString(preReqOfferId)
        parcel.writeString(appOfferFilter)
        parcel.writeString(stickerLabel)
        parcel.writeString(visibleFor)
        parcel.writeString(offeringId)
        parcel.writeString(sortOrderMS)
        parcel.writeString(stickerColorCode)
        parcel.writeString(sortOrder)
        parcel.writeString(isTopUp)
        parcel.writeString(btnRenew)
        parcel.writeString(status)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Header> {
        override fun createFromParcel(parcel: Parcel): Header {
            return Header(parcel)
        }

        override fun newArray(size: Int): Array<Header?> {
            return arrayOfNulls(size)
        }
    }
}
