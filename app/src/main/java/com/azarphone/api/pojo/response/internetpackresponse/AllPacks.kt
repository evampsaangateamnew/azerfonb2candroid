package com.azarphone.api.pojo.response.internetpackresponse

data class AllPacks(
    val filters: Filters?,
    val offers: List<Offer?>
)