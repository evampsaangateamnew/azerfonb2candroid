package com.azarphone.api.pojo.response.supplementaryoffers.response

import android.os.Parcel
import android.os.Parcelable
import com.azarphone.api.pojo.response.supplementaryoffers.helper.SupplementaryOffer

data class InternetInclusiveOffers(
		val offers: List<SupplementaryOffer?>? = null
		/*val inclusiveUsageList: List<Any?>? = null*/
): Parcelable {
	constructor(parcel: Parcel) : this(parcel.createTypedArrayList(SupplementaryOffer)) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeTypedList(offers)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<InternetInclusiveOffers> {
		override fun createFromParcel(parcel: Parcel): InternetInclusiveOffers {
			return InternetInclusiveOffers(parcel)
		}

		override fun newArray(size: Int): Array<InternetInclusiveOffers?> {
			return arrayOfNulls(size)
		}
	}
}
