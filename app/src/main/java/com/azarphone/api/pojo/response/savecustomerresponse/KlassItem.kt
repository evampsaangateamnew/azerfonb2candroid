package com.azarphone.api.pojo.response.savecustomerresponse

data class KlassItem(
	val amount: String? = null,
	val currency: String? = null
)
