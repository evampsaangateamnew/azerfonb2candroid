package com.azarphone.api.pojo.response.internetpackresponse

data class TextTitle(
    val fragmentIcon: String,
    val textWithTitleDescription: String,
    val textWithTitleLabel: String
)