package com.azarphone.api.pojo.response.menuresponse

import com.azarphone.api.pojo.response.BaseResponse

data class MenuResponse(
	override var data: Data,
	override var callStatus: String,
	override var resultCode: String,
	override var resultDesc: String
):BaseResponse<Data>()
