package com.azarphone.api.pojo.response.gethomepageresponse

data class GetHomePageResponse(
        val data: Data? = null,
        val callStatus: String? = null,
        val resultCode: String? = null,
        val resultDesc: String? = null
)
