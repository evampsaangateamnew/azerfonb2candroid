package com.azarphone.api.pojo.response.mysubscriptions.successresponse

import com.azarphone.api.pojo.response.mysubscriptions.response.*

data class MySubscriptionsData(
		val call: Call? = null,
		val hybrid: Hybrid? = null,
		val roaming: Roaming? = null,
		val smsInclusiveOffers: SmsInclusiveOffers? = null,
		val sms: Sms? = null,
		val campaign: Campaign? = null,
		val tm: Tm? = null,
		val voiceInclusiveOffers: VoiceInclusiveOffers? = null,
		val internetInclusiveOffers: InternetInclusiveOffers? = null,
		val internet: Internet? = null
)
