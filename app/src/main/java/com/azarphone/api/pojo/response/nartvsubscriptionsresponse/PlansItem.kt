package com.azarphone.api.pojo.response.nartvsubscriptionsresponse

data class PlansItem(
	val price: String? = null,
	val description: String? = null,
	val title: String? = null,
	val status: String? = null,
	val activatedPlanStatus: ActivatedPlanStatus? = null,
	var isSelected: Boolean =false
)
