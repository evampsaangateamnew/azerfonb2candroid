package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

data class ExchangeServiceValuesRequest(
        @Json(name = "offeringId")
        val offeringId: String? = null,

        @Json(name = "offeringName")
        val offeringName: String? = null
)