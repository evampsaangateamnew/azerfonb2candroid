package com.azarphone.api.pojo.response.fasttopupresponse

data class FastTopUpData(var fastPaymentDetails: List<FastTopUpDetail>)
