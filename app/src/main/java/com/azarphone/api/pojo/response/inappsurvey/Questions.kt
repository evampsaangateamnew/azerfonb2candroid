package com.azarphone.api.pojo.response.inappsurvey

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by Muhammad Ahmed on 8/30/2021.
 */


@Parcelize
data class Questions(

    @SerializedName("id") val id: Int,
    @SerializedName("questionTextEN") val questionTextEN: String?,
    @SerializedName("questionTextAZ") val questionTextAZ: String?,
    @SerializedName("questionTextRU") val questionTextRU: String?,
    @SerializedName("answerType") val answerType: Int,
    @SerializedName("answers") val answers: List<Answers>?
) : Parcelable