package com.azarphone.api.pojo.response.deletefnfresponse

data class Data(
	val fnfLimit: String? = null,
	val fnfList: List<FnfListItem?>? = null
)
