package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

/**
 * @author Junaid Hassan on 05, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class FCMRequest(
        @Json(name = "fcmKey")
        val fcmKey: String? = null,
        @Json(name = "ringingStatus")
        val ringingStatus: String? = null,
        @Json(name = "isEnable")
        val isEnable: String? = null,
        @Json(name = "cause")
        val cause: String? = null,
        @Json(name = "msisdn")
        val msisdn: String? = null,
        @Json(name = "subscriberType")
        val subscriberType: String? = null,
        @Json(name = "tariffType")
        val tariffType: String? = null


)