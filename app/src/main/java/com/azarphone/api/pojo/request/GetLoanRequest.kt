package com.azarphone.api.pojo.request

/**
 * @author Junaid Hassan on 11, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class GetLoanRequest(
        val friendMsisdn: String? = null,
        val loanAmount: String? = null
)