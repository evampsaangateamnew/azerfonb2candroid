package com.azarphone.api.pojo.response

abstract class BaseResponse<T> {
    abstract var data: T
    abstract var callStatus: String
    abstract var resultCode: String
    abstract var resultDesc: String
}