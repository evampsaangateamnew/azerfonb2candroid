package com.azarphone.api.pojo.response.homepage

data class FreeResources(
	val freeResourcesRoaming: List<Any?>? = null,
	val freeResources: List<Any?>? = null
)
