package com.azarphone.api.pojo.response.internetpackresponse

data class Weekly(
    val filters: Filters?,
    val offers: List<Offer?>
)