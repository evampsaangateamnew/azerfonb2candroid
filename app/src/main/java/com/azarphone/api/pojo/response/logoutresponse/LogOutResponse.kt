package com.azarphone.api.pojo.response.logoutresponse

data class LogOutResponse(
        val callStatus: String? = null,
        val resultCode: String? = null,
        val resultDesc: String? = null
)
