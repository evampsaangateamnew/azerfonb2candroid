package com.azarphone.api.pojo.response.operationshistory

data class RecordsItem(
	val date: String? = null,
	val transactionType: String? = null,
	val amount: String? = null,
	val endingBalance: String? = null,
	val description: String? = null,
	val primaryIdentity: String? = null,
	val clarification: String? = null,
	val currency: String? = null
)
