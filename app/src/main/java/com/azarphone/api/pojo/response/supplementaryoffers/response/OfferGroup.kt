package com.azarphone.api.pojo.response.supplementaryoffers.response

import android.os.Parcel
import android.os.Parcelable

data class OfferGroup(
	val groupName: String? = null,
	val groupValue: String? = null
): Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readString(),
			parcel.readString()) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(groupName)
		parcel.writeString(groupValue)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<OfferGroup> {
		override fun createFromParcel(parcel: Parcel): OfferGroup {
			return OfferGroup(parcel)
		}

		override fun newArray(size: Int): Array<OfferGroup?> {
			return arrayOfNulls(size)
		}
	}
}
