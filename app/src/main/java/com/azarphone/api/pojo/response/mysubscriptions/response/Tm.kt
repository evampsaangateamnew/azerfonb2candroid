package com.azarphone.api.pojo.response.mysubscriptions.response

import android.os.Parcel
import android.os.Parcelable
import com.azarphone.api.pojo.response.mysubscriptions.helper.Subscriptions

data class Tm(
	val offers: List<Subscriptions?>? = null
): Parcelable {
	constructor(parcel: Parcel) : this(parcel.createTypedArrayList(Subscriptions)) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeTypedList(offers)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<Tm> {
		override fun createFromParcel(parcel: Parcel): Tm {
			return Tm(parcel)
		}

		override fun newArray(size: Int): Array<Tm?> {
			return arrayOfNulls(size)
		}
	}
}
