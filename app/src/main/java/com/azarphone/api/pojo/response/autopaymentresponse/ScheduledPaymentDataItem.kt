package com.azarphone.api.pojo.response.autopaymentresponse

data class ScheduledPaymentDataItem(
        val savedCardId: String, val recurrentDay: String,
        val cardType: String, val recurrenceNumber: String,
        val amount: String, val id: Int,
        val msisdn: String, val nextScheduledDate: String,
        val startDate: String, var billingCycle: String)
