package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

/**
 * @author Umair Mustafa on 26/04/2021
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * umair.mustafa@evampsaanga.com
 */
data class MakePaymentRequest (
        @Json(name = "paymentKey")
        val paymentKey: String? = null,
        @Json(name = "amount")
        val amount: String? = null,
        @Json(name = "topupNumber")
        val topupNumber: String? = null,
        )