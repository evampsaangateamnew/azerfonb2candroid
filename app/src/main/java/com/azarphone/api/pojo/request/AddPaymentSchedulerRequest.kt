package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

data class AddPaymentSchedulerRequest(
    @Json(name = "amount")
    val amount: String? = null,

    @Json(name = "billingCycle")
    val billingCycle: String? = null,

    @Json(name = "startDate")
    val startDate: String? = null,

    @Json(name = "recurrenceNumber")
    val recurrenceNumber: String? = null,

    @Json(name = "savedCardId")
    val savedCardId: String? = null,

    @Json(name = "recurrenceFrequency")
    val recurrenceFrequency: String = "1"
)
