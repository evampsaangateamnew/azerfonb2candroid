package com.azarphone.api.pojo.response.loan

data class Data(
	val newBalance: String? = null,
	val newCredit: String? = null,
	val message: String? = null
)
