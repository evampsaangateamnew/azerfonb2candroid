package com.azarphone.api.pojo.response.exchangeservicesvaluesresponse

data class DatavaluesItem(
	val voice: String? = null,
	val data: String? = null,
	val voiceUnit: String? = null,
	val dataUnit: String? = null
)
