package com.azarphone.api.pojo.response.mysubscriptions.helper

import android.os.Parcel
import android.os.Parcelable

/**
 * @author Junaid Hassan on 26, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class SubscriptionsMain(
        val call: SubscriptionCategory? = null,
        val hybrid: SubscriptionCategory? = null,
        val roaming: SubscriptionCategory? = null,
        val smsInclusiveOffers: SubscriptionCategory? = null,
        val sms: SubscriptionCategory? = null,
        val campaign: SubscriptionCategory? = null,
        val tm: SubscriptionCategory? = null,
        val voiceInclusiveOffers: SubscriptionCategory? = null,
        val internetInclusiveOffers: SubscriptionCategory? = null,
        val internet: SubscriptionCategory? = null
):Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readParcelable(SubscriptionCategory::class.java.classLoader),
            parcel.readParcelable(SubscriptionCategory::class.java.classLoader),
            parcel.readParcelable(SubscriptionCategory::class.java.classLoader),
            parcel.readParcelable(SubscriptionCategory::class.java.classLoader),
            parcel.readParcelable(SubscriptionCategory::class.java.classLoader),
            parcel.readParcelable(SubscriptionCategory::class.java.classLoader),
            parcel.readParcelable(SubscriptionCategory::class.java.classLoader),
            parcel.readParcelable(SubscriptionCategory::class.java.classLoader),
            parcel.readParcelable(SubscriptionCategory::class.java.classLoader),
            parcel.readParcelable(SubscriptionCategory::class.java.classLoader)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(call, flags)
        parcel.writeParcelable(hybrid, flags)
        parcel.writeParcelable(roaming, flags)
        parcel.writeParcelable(smsInclusiveOffers, flags)
        parcel.writeParcelable(sms, flags)
        parcel.writeParcelable(campaign, flags)
        parcel.writeParcelable(tm, flags)
        parcel.writeParcelable(voiceInclusiveOffers, flags)
        parcel.writeParcelable(internetInclusiveOffers, flags)
        parcel.writeParcelable(internet, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SubscriptionsMain> {
        override fun createFromParcel(parcel: Parcel): SubscriptionsMain {
            return SubscriptionsMain(parcel)
        }

        override fun newArray(size: Int): Array<SubscriptionsMain?> {
            return arrayOfNulls(size)
        }
    }
}