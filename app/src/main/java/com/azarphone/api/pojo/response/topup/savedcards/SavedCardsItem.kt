package com.azarphone.api.pojo.response.topup.savedcards

import android.os.Parcel
import android.os.Parcelable

data class SavedCardsItem(val cardType: String?, val paymentKey: String?,
                          val cardMaskNumber: String?, val id: String?) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(cardType)
        parcel.writeString(paymentKey)
        parcel.writeString(cardMaskNumber)
        parcel.writeString(id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SavedCardsItem> {
        override fun createFromParcel(parcel: Parcel): SavedCardsItem {
            return SavedCardsItem(parcel)
        }

        override fun newArray(size: Int): Array<SavedCardsItem?> {
            return arrayOfNulls(size)
        }
    }
}
