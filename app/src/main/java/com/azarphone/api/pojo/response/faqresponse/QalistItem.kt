package com.azarphone.api.pojo.response.faqresponse

data class QalistItem(
	val question: String? = null,
	val answer: String? = null,
	val sortOrder: String? = null
)
