package com.azarphone.api.pojo.request



import com.squareup.moshi.Json

data class SendSMSRequest(
    @Json(name = "recieverMsisdn")
    val recieverMsisdn: String? = null,

    @Json(name = "textmsg")
    val textmsg: String? = null,

    @Json(name = "msgLang")
    val msgLang: String? = null
)