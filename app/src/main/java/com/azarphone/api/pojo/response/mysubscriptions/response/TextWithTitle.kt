package com.azarphone.api.pojo.response.mysubscriptions.response

import android.os.Parcel
import android.os.Parcelable

data class TextWithTitle(
	val text: String? = null,
	val title: String? = null
): Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readString(),
			parcel.readString()) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(text)
		parcel.writeString(title)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<TextWithTitle> {
		override fun createFromParcel(parcel: Parcel): TextWithTitle {
			return TextWithTitle(parcel)
		}

		override fun newArray(size: Int): Array<TextWithTitle?> {
			return arrayOfNulls(size)
		}
	}
}
