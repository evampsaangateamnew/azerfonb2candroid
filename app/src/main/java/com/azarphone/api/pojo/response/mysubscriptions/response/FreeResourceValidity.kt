package com.azarphone.api.pojo.response.mysubscriptions.response

import android.os.Parcel
import android.os.Parcelable

data class FreeResourceValidity(
	val subTitle: String? = null,
	val subTitleValue: String? = null,
	val description: String? = null,
	val title: String? = null,
	val titleValue: String? = null
): Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString()) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(subTitle)
		parcel.writeString(subTitleValue)
		parcel.writeString(description)
		parcel.writeString(title)
		parcel.writeString(titleValue)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<FreeResourceValidity> {
		override fun createFromParcel(parcel: Parcel): FreeResourceValidity {
			return FreeResourceValidity(parcel)
		}

		override fun newArray(size: Int): Array<FreeResourceValidity?> {
			return arrayOfNulls(size)
		}
	}
}
