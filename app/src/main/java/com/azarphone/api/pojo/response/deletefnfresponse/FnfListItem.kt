package com.azarphone.api.pojo.response.deletefnfresponse

data class FnfListItem(
	val createdDate: String? = null,
	val msisdn: String? = null
)
