package com.azarphone.api.pojo.response.addfnfresponse

import com.azarphone.api.pojo.response.fnfresponse.FnfListItem

data class Data(
		val fnfLimit: String? = null,
		val fnfList:  List<FnfListItem?>?= null
)
