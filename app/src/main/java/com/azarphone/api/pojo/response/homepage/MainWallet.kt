package com.azarphone.api.pojo.response.homepage

data class MainWallet(
	val balanceTypeName: String? = null,
	val amount: String? = null,
	val expireTime: String? = null,
	val effectiveTime: String? = null,
	val currency: String? = null,
	val lowerLimit: String? = null
)
