package com.azarphone.api.pojo.response.inappsurvey

data class UploadSurvey(
    val comment: String = "",
    val answerId: String = "",
    val questionId: String = "",
    val offeringIdSurvey: String = "",
    val offeringTypeSurvey:String = "",
    val surveyId: String = ""
)
