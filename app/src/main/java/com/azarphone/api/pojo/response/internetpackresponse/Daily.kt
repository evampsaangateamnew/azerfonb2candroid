package com.azarphone.api.pojo.response.internetpackresponse

data class Daily(
    val filters: Filters?,
    val offers: List<Offer?>
)