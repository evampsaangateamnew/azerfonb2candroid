package com.azarphone.api.pojo.response.requesthistory

data class LoanRequestItem(
	val dateTime: String? = null,
	val amount: String? = null,
	val paid: String? = null,
	val loanID: String? = null,
	val remaining: String? = null,
	val status: String? = null
)
