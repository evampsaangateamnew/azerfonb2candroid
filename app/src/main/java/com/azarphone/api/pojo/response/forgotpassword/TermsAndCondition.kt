package com.azarphone.api.pojo.response.forgotpassword

data class TermsAndCondition(
	val klass: List<KlassItem?>? = null,
	val cin: List<CinItem?>? = null
)
