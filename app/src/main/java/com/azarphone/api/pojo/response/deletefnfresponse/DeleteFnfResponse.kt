package com.azarphone.api.pojo.response.deletefnfresponse

import com.azarphone.api.pojo.response.BaseResponse

data class DeleteFnfResponse(
	override var data: Data,
	override var callStatus: String,
	override var resultCode: String,
	override var resultDesc: String
):BaseResponse<Data>()
