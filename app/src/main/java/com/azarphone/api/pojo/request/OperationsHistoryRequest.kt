package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

/**
 * @author Junaid Hassan on 11, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class OperationsHistoryRequest(
        @Json(name = "startDate")
        val startDate: String? = null,

        @Json(name = "endDate")
        val endDate: String? = null,

        @Json(name = "accountId")
        val accountId: String? = null,

        @Json(name = "customerId")
        val customerId: String? = null
)