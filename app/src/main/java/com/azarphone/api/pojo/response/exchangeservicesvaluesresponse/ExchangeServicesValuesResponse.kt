package com.azarphone.api.pojo.response.exchangeservicesvaluesresponse

import com.azarphone.api.pojo.response.BaseResponse

data class ExchangeServicesValuesResponse(
	override var data: Data,
	override var callStatus: String,
	override var resultCode: String,
	override var resultDesc: String
):BaseResponse<Data>()
