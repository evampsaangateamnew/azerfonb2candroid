package com.azarphone.api.pojo.response.faqresponse

data class FaqlistItem(
	val qalist: List<QalistItem?>? = null,
	val title: String? = null
)
