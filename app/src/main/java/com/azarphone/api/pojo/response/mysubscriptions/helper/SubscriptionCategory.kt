package com.azarphone.api.pojo.response.mysubscriptions.helper

import android.os.Parcel
import android.os.Parcelable

/**
 * @author Junaid Hassan on 26, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class SubscriptionCategory(
        private val offers: List<Subscriptions>? = null
):Parcelable {
    constructor(parcel: Parcel) : this(parcel.createTypedArrayList(Subscriptions)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(offers)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SubscriptionCategory> {
        override fun createFromParcel(parcel: Parcel): SubscriptionCategory {
            return SubscriptionCategory(parcel)
        }

        override fun newArray(size: Int): Array<SubscriptionCategory?> {
            return arrayOfNulls(size)
        }
    }
}