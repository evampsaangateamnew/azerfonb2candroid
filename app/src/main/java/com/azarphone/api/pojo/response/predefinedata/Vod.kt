package com.azarphone.api.pojo.response.predefinedata

data class Vod(
	val footer: String? = null,
	val header: String? = null
)
