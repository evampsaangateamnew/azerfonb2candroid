package com.azarphone.api.pojo.response.gethomepageresponse

data class Credit(
	val creditTitleLabel: String? = null,
	val creditTitleValue: String? = null,
	val creditDate: String? = null,
	val creditInitialDate: String? = null,
	val creditLimit: String? = null,
	val creditCurrency: String? = null,
	val creditDateLabel: String? = null,
	val daysDifferenceTotal: String? = null,
	val daysDifferenceCurrent: String? = null
)
