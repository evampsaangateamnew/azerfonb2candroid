package com.azarphone.api.pojo.response.gethomepageresponse

data class Postpaid(
	val template: String? = null,
	val currentCreditLimit: String? = null,
	val outstandingIndividualDebtLabel: String? = null,
	val corporateLabel: String? = null,
	val currentCreditLabel: String? = null,
	val currentCreditCorporateValue: String? = null,
	val availableBalanceCorporateValue: String? = null,
	val currentCreditIndividualValue: String? = null,
	val balanceLabel: String? = null,
	val availableBalanceIndividualValue: String? = null,
	val totalPayments: String? = null,
	val currentCreditLimitLabel: String? = null,
	val balanceIndividualValue: String? = null,
	val availableCreditLabel: String? = null,
	val individualLabel: String? = null,
	val balanceCorporateValue: String? = null,
	val totalPaymentsLabel: String? = null,
	val outstandingIndividualDebt: String? = null
)
