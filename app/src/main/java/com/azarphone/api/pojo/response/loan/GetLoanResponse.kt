package com.azarphone.api.pojo.response.loan

import com.azarphone.api.pojo.response.BaseResponse

data class GetLoanResponse(
	override var data: Data,
	override var callStatus: String,
	override var resultCode: String,
	override var resultDesc: String
):BaseResponse<Data>()
