package com.azarphone.api.pojo.response.nartvmigrationresponse

import com.azarphone.api.pojo.response.BaseResponse

data class NarTvMigrationResponse(
	override var data: Data,
	override var callStatus: String,
	override var resultCode: String,
	override var resultDesc: String
):BaseResponse<Data>()
