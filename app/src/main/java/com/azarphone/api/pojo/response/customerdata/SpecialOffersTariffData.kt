package com.azarphone.api.pojo.response.customerdata

/**
 * @author Junaid Hassan on 25, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class SpecialOffersTariffData(
        val tariffSpecialIds: List<String>? = null,
        val offersSpecialIds: List<String>? = null,
        val specialOffersMenu: String? = null,
        val specialTariffMenu: String? = null
)