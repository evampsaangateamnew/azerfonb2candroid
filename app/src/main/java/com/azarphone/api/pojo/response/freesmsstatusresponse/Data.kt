package com.azarphone.api.pojo.response.freesmsstatusresponse

data class Data(
	val onNetSMS: String? = null,
	val offNetSMS: String? = null
)
