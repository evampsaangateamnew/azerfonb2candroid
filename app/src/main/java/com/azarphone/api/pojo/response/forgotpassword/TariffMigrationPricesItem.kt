package com.azarphone.api.pojo.response.forgotpassword

data class TariffMigrationPricesItem(
	val value: String? = null,
	val key: String? = null
)
