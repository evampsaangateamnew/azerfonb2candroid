package com.azarphone.api.pojo.response.paymenthistory

data class PaymentHistoryItem(
	val dateTime: String? = null,
	val amount: String? = null,
	val loanID: String? = null
)
