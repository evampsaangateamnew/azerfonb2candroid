package com.azarphone.api.pojo.response.otpverifyresponse

data class Data(
	val message: String? = null,
	val content: String
)
