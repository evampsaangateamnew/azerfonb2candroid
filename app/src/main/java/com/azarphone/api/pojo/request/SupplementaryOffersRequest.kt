package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

/**
 * @author Junaid Hassan on 22, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class SupplementaryOffersRequest(
        @Json(name = "offeringName")
        val offeringName: String? = null,

        @Json(name = "brandName")
        val brandName: String? = null,

        @Json(name = "specialOfferIds")
        val specialOfferIds: List<String>?=null
)