package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

data class DeleteFnfRequest(
        @Json(name = "offeringId")
        val offeringId: String? = null,

        @Json(name = "actionType")
        val actionType: String? = null,

        @Json(name = "deleteFnf")
        val deleteFnf: List<String>?=null
)