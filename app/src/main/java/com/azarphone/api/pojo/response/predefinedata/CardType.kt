package com.azarphone.api.pojo.response.predefinedata

data class CardType(
        var key: Long = 0L,
        val value: String? = null
)
