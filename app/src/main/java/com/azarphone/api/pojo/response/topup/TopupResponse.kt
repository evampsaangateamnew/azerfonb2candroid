package com.azarphone.api.pojo.response.topup

import com.azarphone.api.pojo.response.BaseResponse

data class TopupResponse(
        override var data: Data,
        override var callStatus: String,
        override var resultCode: String,
        override var resultDesc: String
) : BaseResponse<Data>()
