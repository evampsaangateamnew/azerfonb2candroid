package com.azarphone.api.pojo.response.profilepictureupload

data class ProfilePictureUpdateResponse(
	val data: Data? = null,
	val callStatus: String? = null,
	val resultCode: String? = null,
	val resultDesc: String? = null
)
