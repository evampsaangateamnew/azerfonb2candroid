package com.azarphone.api.pojo.response.gethomepageresponse

data class Data(
        val status: String? = null,
        val offeringNameDisplay: String? = null,
        val notificationUnreadCount: Any? = null,
        val balance: Balance? = null,
        val installments: Installments? = null,
        val freeResources: FreeResources? = null,
        val credit: Credit? = null,
        val mrc: Mrc? = null
)
