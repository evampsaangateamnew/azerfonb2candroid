package com.azarphone.api.pojo.request


import com.squareup.moshi.Json

data class RegisterRequest(

	@Json(name="password")
	val password: String? = null,

	@Json(name="email")
	val email: String? = null
)