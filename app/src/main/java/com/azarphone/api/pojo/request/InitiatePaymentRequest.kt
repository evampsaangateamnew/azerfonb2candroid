package com.azarphone.api.pojo.request

import com.squareup.moshi.Json

/**
 * @author Umair Mustafa on 27/04/2021
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * umair.mustafa@evampsaanga.com
 */
data class InitiatePaymentRequest(
        @Json(name = "cardType")
        val cardType: String? = null,

        @Json(name = "amount")
        val amount: String? = null,

        @Json(name = "saved")
        val saved: String? = null,

        @Json(name = "topupNumber")
        val topupNumber: String? = null
)
