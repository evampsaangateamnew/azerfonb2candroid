package com.azarphone.api.pojo.response.newsfeed

import com.azarphone.api.pojo.response.newsfeed.Results


data class NewsData(
        val status:String,
        val userTier:String,
        val total:Int,
        val startIndex:Int,
        val pageSize:Int,
        val currentPage:Int,
        val pages:Int,
        val orderBy:String,
        val results:List<Results>
)
