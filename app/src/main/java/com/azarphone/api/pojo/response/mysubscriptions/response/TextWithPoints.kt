package com.azarphone.api.pojo.response.mysubscriptions.response

import android.os.Parcel
import android.os.Parcelable

data class TextWithPoints(
        val title: String? = null,
        val titleIcon: String? = null,
        val pointsList: List<String?>? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.createStringArrayList()) {

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(titleIcon)
        parcel.writeStringList(pointsList)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TextWithPoints> {
        override fun createFromParcel(parcel: Parcel): TextWithPoints {
            return TextWithPoints(parcel)
        }

        override fun newArray(size: Int): Array<TextWithPoints?> {
            return arrayOfNulls(size)
        }
    }
}
