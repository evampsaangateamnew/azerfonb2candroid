package com.azarphone.api.pojo.response.coreservices

import android.os.Parcel
import android.os.Parcelable

data class CoreServicesResponse(
        var data: Data? = null,
        var callStatus: String? = null,
        var resultCode: String? = null,
        var resultDesc: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readParcelable(Data::class.java.classLoader),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(data, flags)
        parcel.writeString(callStatus)
        parcel.writeString(resultCode)
        parcel.writeString(resultDesc)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CoreServicesResponse> {
        override fun createFromParcel(parcel: Parcel): CoreServicesResponse {
            return CoreServicesResponse(parcel)
        }

        override fun newArray(size: Int): Array<CoreServicesResponse?> {
            return arrayOfNulls(size)
        }
    }
}
