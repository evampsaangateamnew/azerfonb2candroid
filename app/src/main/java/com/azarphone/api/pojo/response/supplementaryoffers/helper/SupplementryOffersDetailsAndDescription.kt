package com.azarphone.api.pojo.response.supplementaryoffers.helper

import android.os.Parcel
import android.os.Parcelable
import com.azarphone.api.pojo.response.supplementaryoffers.response.*

/**
 * @author Junaid Hassan on 22, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class SupplementryOffersDetailsAndDescription(
        val date: Date? = null,
        val textWithOutTitle: TextWithOutTitle? = null,
        val price: List<PriceItem?>? = null,
        val textWithPoints: TextWithPoints? = null,
        val rounding: Rounding? = null,
        val freeResourceValidity: FreeResourceValidity? = null,
        val titleSubTitleValueAndDesc: TitleSubTitleValueAndDesc? = null,
        val time: Time? = null,
        val textWithTitle: TextWithTitle? = null,
        val roamingDetails: RoamingDetails? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readParcelable(Date::class.java.classLoader),
            parcel.readParcelable(TextWithOutTitle::class.java.classLoader),
            parcel.createTypedArrayList(PriceItem),
            parcel.readParcelable(TextWithPoints::class.java.classLoader),
            parcel.readParcelable(Rounding::class.java.classLoader),
            parcel.readParcelable(FreeResourceValidity::class.java.classLoader),
            parcel.readParcelable(TitleSubTitleValueAndDesc::class.java.classLoader),
            parcel.readParcelable(Time::class.java.classLoader),
            parcel.readParcelable(TextWithTitle::class.java.classLoader),
            parcel.readParcelable(RoamingDetails::class.java.classLoader)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(date, flags)
        parcel.writeParcelable(textWithOutTitle, flags)
        parcel.writeTypedList(price)
        parcel.writeParcelable(textWithPoints, flags)
        parcel.writeParcelable(rounding, flags)
        parcel.writeParcelable(freeResourceValidity, flags)
        parcel.writeParcelable(titleSubTitleValueAndDesc, flags)
        parcel.writeParcelable(time, flags)
        parcel.writeParcelable(textWithTitle, flags)
        parcel.writeParcelable(roamingDetails, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SupplementryOffersDetailsAndDescription> {
        override fun createFromParcel(parcel: Parcel): SupplementryOffersDetailsAndDescription {
            return SupplementryOffersDetailsAndDescription(parcel)
        }

        override fun newArray(size: Int): Array<SupplementryOffersDetailsAndDescription?> {
            return arrayOfNulls(size)
        }
    }
}