package com.azarphone.api.pojo.response.payg

import com.azarphone.api.pojo.response.mysubscriptions.successresponse.Data

data class WithOutPackResponse (
    val data: WithOutPackData? = null,
    val callStatus: String? = null,
    val resultCode: String? = null,
    val resultDesc: String? = null
)