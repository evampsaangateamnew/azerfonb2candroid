package com.azarphone.api.pojo.response.requestmoneyresponse

import com.azarphone.api.pojo.response.BaseResponse

data class RequestMoneyResponse(
		override var data: Any,
	override var callStatus: String,
	override var resultCode: String,
	override var resultDesc: String
): BaseResponse<Any>()
