package com.azarphone.api.pojo.request

/**
 * @author Junaid Hassan on 01, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class GetRateUsRequest(
        val entityId: String? = null
)
