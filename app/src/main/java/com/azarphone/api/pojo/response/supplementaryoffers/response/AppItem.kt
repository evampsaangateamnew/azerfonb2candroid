package com.azarphone.api.pojo.response.supplementaryoffers.response

import android.os.Parcel
import android.os.Parcelable

data class AppItem(
	val value: String? = null,
	val key: String? = null
): Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readString(),
			parcel.readString()) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(value)
		parcel.writeString(key)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<AppItem> {
		override fun createFromParcel(parcel: Parcel): AppItem {
			return AppItem(parcel)
		}

		override fun newArray(size: Int): Array<AppItem?> {
			return arrayOfNulls(size)
		}
	}
}
