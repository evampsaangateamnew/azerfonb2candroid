package com.azarphone.api.pojo.response.nartvsubscriptionsresponse

import com.azarphone.api.pojo.response.BaseResponse

data class NarTvSubscriptionResponse(
	override var data: Data,
	override var callStatus: String,
	override var resultCode: String,
	override var resultDesc: String
):BaseResponse<Data>()
