package com.azarphone.api.pojo.response.homepage

data class Prepaid(
	val bounusWallet: BounusWallet? = null,
	val mainWallet: MainWallet? = null,
	val countryWideWallet: CountryWideWallet? = null
)
