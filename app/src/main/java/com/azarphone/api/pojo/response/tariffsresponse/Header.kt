package com.azarphone.api.pojo.response.tariffsresponse

import android.os.Parcel
import android.os.Parcelable

data class Header(
        val sortOrder: String? = null,
        val name: String? = null,
        val currency: String? = null,
        val priceValue: String? = null,
        val attributes: List<AttributesItem?>? = null,
        val id: String? = null,
        val offeringId: String? = null,
        val priceLabel: String? = null,
        val tariffDescription: String? = null,
        val bonusLabel: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.createTypedArrayList(AttributesItem),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(sortOrder)
        parcel.writeString(name)
        parcel.writeString(currency)
        parcel.writeString(priceValue)
        parcel.writeTypedList(attributes)
        parcel.writeString(id)
        parcel.writeString(offeringId)
        parcel.writeString(priceLabel)
        parcel.writeString(tariffDescription)
        parcel.writeString(bonusLabel)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Header> {
        override fun createFromParcel(parcel: Parcel): Header {
            return Header(parcel)
        }

        override fun newArray(size: Int): Array<Header?> {
            return arrayOfNulls(size)
        }
    }
}
