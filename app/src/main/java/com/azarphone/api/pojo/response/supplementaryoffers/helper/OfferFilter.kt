package com.azarphone.api.pojo.response.supplementaryoffers.helper

import android.os.Parcel
import android.os.Parcelable

/**
 * @author Junaid Hassan on 23, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
data class OfferFilter(
        val value: String? = null,
        val key: String? = null
): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(value)
        parcel.writeString(key)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OfferFilter> {
        override fun createFromParcel(parcel: Parcel): OfferFilter {
            return OfferFilter(parcel)
        }

        override fun newArray(size: Int): Array<OfferFilter?> {
            return arrayOfNulls(size)
        }
    }
}