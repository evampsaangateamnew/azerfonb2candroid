package com.azarphone.api.pojo.response.usagedetailsresponse

data class RecordsItem(
	val number: String? = null,
	val period: String? = null,
	val unit: Any? = null,
	val startDateTime: String? = null,
	val zone: String? = null,
	val service: String? = null,
	val usage: String? = null,
	val chargedAmount: String? = null,
	val destination: String? = null,
	val endDateTime: String? = null,
	val type: String? = null
)
