package com.azarphone.api.pojo.response.fasttopupresponse

import com.azarphone.api.pojo.response.BaseResponse


data class FastTopUpResponse(override var data: FastTopUpData,
                             override var callStatus: String,
                             override var resultCode: String,
                             override var resultDesc: String
) : BaseResponse<FastTopUpData>()