package com.azarphone.api.pojo.response.fnfresponse

data class Data(
	val fnfLimit: String? = null,
	val fnfList: List<FnfListItem?>? = null
)
