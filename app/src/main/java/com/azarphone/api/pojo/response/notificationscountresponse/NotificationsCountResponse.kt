package com.azarphone.api.pojo.response.notificationscountresponse

data class NotificationsCountResponse(
	val data: Data? = null,
	val callStatus: String? = null,
	val resultCode: String? = null,
	val resultDesc: String? = null
)
