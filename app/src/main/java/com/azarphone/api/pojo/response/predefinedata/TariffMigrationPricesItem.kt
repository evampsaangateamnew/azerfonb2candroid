package com.azarphone.api.pojo.response.predefinedata

data class TariffMigrationPricesItem(
	val value: String? = null,
	val key: String? = null
)
