package com.azarphone.api.pojo.response.internetpackresponse

data class Details(
    val rounding: Rounding,
    val textTitle: TextTitle
)