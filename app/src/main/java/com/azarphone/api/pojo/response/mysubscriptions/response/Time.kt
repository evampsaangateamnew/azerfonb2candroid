package com.azarphone.api.pojo.response.mysubscriptions.response

import android.os.Parcel
import android.os.Parcelable

data class Time(
	val fromValue: String? = null,
	val toTitle: String? = null,
	val toValue: String? = null,
	val fromTitle: String? = null,
	val description: String? = null
): Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString()) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(fromValue)
		parcel.writeString(toTitle)
		parcel.writeString(toValue)
		parcel.writeString(fromTitle)
		parcel.writeString(description)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<Time> {
		override fun createFromParcel(parcel: Parcel): Time {
			return Time(parcel)
		}

		override fun newArray(size: Int): Array<Time?> {
			return arrayOfNulls(size)
		}
	}
}
