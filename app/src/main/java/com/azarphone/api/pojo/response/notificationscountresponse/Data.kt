package com.azarphone.api.pojo.response.notificationscountresponse

data class Data(
	val notificationUnreadCount: String? = null
)
