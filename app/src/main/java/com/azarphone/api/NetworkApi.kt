package com.azarphone.api

import com.azarphone.api.pojo.request.*
import com.azarphone.api.pojo.response.addfnfresponse.AddFnfResponse
import com.azarphone.api.pojo.response.autopaymentresponse.ScheduledPaymentResponse
import com.azarphone.api.pojo.response.autopaymentresponse.daily.AddSchedulePaymentResponse
import com.azarphone.api.pojo.response.autopaymentresponse.deletepayment.DeleteAutoPayment
import com.azarphone.api.pojo.response.changebillinglanguagereponse.ChangeBillingResponse
import com.azarphone.api.pojo.response.changepassword.ChangePasswordResponse
import com.azarphone.api.pojo.response.checkappupdate.CheckAppUpdateResponse
import com.azarphone.api.pojo.response.contactusresponse.ContactUsResponse
import com.azarphone.api.pojo.response.coreservices.CoreServicesResponse
import com.azarphone.api.pojo.response.coreservicesprocessnumber.CoreServicesProcessResponse
import com.azarphone.api.pojo.response.deletefnfresponse.DeleteFnfResponse
import com.azarphone.api.pojo.response.exchangeserviceresponse.ExchangeServiceResponse
import com.azarphone.api.pojo.response.exchangeservicesvaluesresponse.ExchangeServicesValuesResponse
import com.azarphone.api.pojo.response.faqresponse.FAQResponse
import com.azarphone.api.pojo.response.fasttopupresponse.FastTopUpResponse
import com.azarphone.api.pojo.response.fasttopupresponse.deletefasttopup.DeleteItemResponse
import com.azarphone.api.pojo.response.fcmtoken.FCMResponse
import com.azarphone.api.pojo.response.fnfresponse.FNFResponse
import com.azarphone.api.pojo.response.forgotpassword.ForgotPasswordResponse
import com.azarphone.api.pojo.response.freesmsstatusresponse.FreeSmsStatusResponse
import com.azarphone.api.pojo.response.gethomepageresponse.GetHomePageResponse
import com.azarphone.api.pojo.response.getrateusresponse.GetRateUsResponse
import com.azarphone.api.pojo.response.inappsurvey.InAppSurvey
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.internetpackresponse.InternetPacksResponse
import com.azarphone.api.pojo.response.loan.GetLoanResponse
import com.azarphone.api.pojo.response.loginappresumeresponse.LoginAppResumeResponse
import com.azarphone.api.pojo.response.logoutresponse.LogOutResponse
import com.azarphone.api.pojo.response.makepayment.MakePaymentTopUpResponse
import com.azarphone.api.pojo.response.menuresponse.MenuResponse
import com.azarphone.api.pojo.response.moneytransfer.MoneyTransferResponse
import com.azarphone.api.pojo.response.mysubscriptions.response.MySubscriptionResponse
import com.azarphone.api.pojo.response.mysubscriptions.successresponse.MySubscriptionsSuccessAfterOfferSubscribeResponse
import com.azarphone.api.pojo.response.nartvmigrationresponse.NarTvMigrationResponse
import com.azarphone.api.pojo.response.nartvsubscriptionsresponse.NarTvSubscriptionResponse
import com.azarphone.api.pojo.response.notification.NotificationResponse
import com.azarphone.api.pojo.response.notificationscountresponse.NotificationsCountResponse
import com.azarphone.api.pojo.response.operationshistory.OperationHistoryResponse
import com.azarphone.api.pojo.response.otpgetresponse.OTPResponse
import com.azarphone.api.pojo.response.otpverifyresponse.OTPVerifyResponse
import com.azarphone.api.pojo.response.payg.WithOutPackResponse
import com.azarphone.api.pojo.response.paymenthistory.PaymentHistoryResponse
import com.azarphone.api.pojo.response.profilepictureupload.ProfilePictureUpdateResponse
import com.azarphone.api.pojo.response.rateusbeforeredirectionresponse.RateUsBeforeRedirectingToPlayStoreRespone
import com.azarphone.api.pojo.response.requesthistory.RequestHistoryResponse
import com.azarphone.api.pojo.response.requestmoneyresponse.RequestMoneyResponse
import com.azarphone.api.pojo.response.resendpin.ResendPinResponse
import com.azarphone.api.pojo.response.savecustomerresponse.SaveCustomerResponse
import com.azarphone.api.pojo.response.sendsmsresponse.SendSMSResponse
import com.azarphone.api.pojo.response.storelocator.StoreLocatorResponse
import com.azarphone.api.pojo.response.supplementaryoffers.response.SupplementaryResponse
import com.azarphone.api.pojo.response.suspendnumber.SuspendNumberResponse
import com.azarphone.api.pojo.response.tariffsresponse.TariffsResponse
import com.azarphone.api.pojo.response.tariffsubscriptionresponse.TariffSubscriptionResponse
import com.azarphone.api.pojo.response.topup.TopupResponse
import com.azarphone.api.pojo.response.topup.initiatepayment.InitiatePaymentResponse
import com.azarphone.api.pojo.response.topup.savedcards.SavedCardsResponse
import com.azarphone.api.pojo.response.updateemailreponse.UpdateEmailResponse
import com.azarphone.api.pojo.response.updatefnfresponse.UpdateFNFResponse
import com.azarphone.api.pojo.response.usagedetailsresponse.UsageDetailsResponse
import com.azarphone.api.pojo.response.usagehistoryresponse.UsageHistorySummaryResponse
import com.azarphone.api.pojo.response.verifypassportresponse.VerifyPassportResponse
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST


interface NetworkApi {

    @POST(EndPoints.CHECK_APP_UPDATE_URL)
    fun checkAppUpdate(@Body checkAppUpdateRequest: CheckAppUpdateRequest): Observable<CheckAppUpdateResponse>

    @POST(EndPoints.USER_LOGIN_ENDPOINT)
    fun userLogin(@Body loginRequest: LoginRequest): Observable<LoginAppResumeResponse>

    @POST(EndPoints.FCM_TOKEN_END_POINT)
    fun requestFCMTokenSentToServerService(@Body fcmRequest: FCMRequest): Observable<FCMResponse>

    @POST(EndPoints.OTP_REQUEST_ENDPOINT)
    fun requestOTP(@Body otpRequest: OTPRequest): Observable<OTPResponse>

    @POST(EndPoints.OTP_VERIFY_REQUEST_ENDPOINT)
    fun requestVerifyOTP(@Body otpVerifyRequest: OTPVerifyRequest): Observable<OTPVerifyResponse>

    @POST(EndPoints.FORGOT_PASSWORD)
    fun forgotPassword(@Body forgotPasswordRequest: ForgotPasswordRequest): Observable<ForgotPasswordResponse>

    @POST(EndPoints.SIGN_UP_SAVE_CUSTOMER_END_POINT)
    fun requestSaveCustomer(@Body saveCustomerRequest: SaveCustomerRequest): Observable<SaveCustomerResponse>

    @POST(EndPoints.RESEND_PIN)
    fun resendPin(@Body resendPinRequest: ResendPinRequest): Observable<ResendPinResponse>

    @POST(EndPoints.REQUEST_MENUS_ENDPOINT)
    fun requestMenus(@Body menuRequest: MenuRequest): Observable<MenuResponse>

    @POST(EndPoints.REQUEST_APP_RESUME_END_POINT)
    fun requestAppResume(@Body appResumeRequest: AppResumeRequest): Observable<LoginAppResumeResponse>

    @POST(EndPoints.HOMEPAGE_API)
    fun requestGetHomePage(@Body getHomePageRequest: GetHomePageRequest): Observable<GetHomePageResponse>

    @POST(EndPoints.NOTIFICATION_API)
    fun getNotifications(): Observable<NotificationResponse>

    @POST(EndPoints.FAQS_ENDPOINT)
    fun requestFAQs(): Observable<FAQResponse>

    @POST(EndPoints.LOGOUT_END_POINT)
    fun requestLogOut(): Observable<LogOutResponse>

    @POST(EndPoints.CONTACT_US_DETAILS)
    fun requestContactUsDetails(): Observable<ContactUsResponse>

    @POST(EndPoints.NOTIFICATIONS_COUNT_END_POINT)
    fun requestGetNotificationsCount(): Observable<NotificationsCountResponse>

    @POST(EndPoints.GET_RATE_US_END_POINT)
    fun requestGetRateUs(@Body getRateUsRequest: GetRateUsRequest): Observable<GetRateUsResponse>

    @POST(EndPoints.TARIFFS_ENDPOINT)
    fun requestGetTariffs(@Body tariffsRequest: TariffsRequest): Observable<TariffsResponse>

    @POST(EndPoints.TOP_UP_ENDPOINT)
    fun requestTopUp(@Body topupRequest: TopupRequest): Observable<TopupResponse>

    @POST(EndPoints.OPERATIONS_HISTORY_ENDPOINT)
    fun requestOperationsHistory(@Body operationsHistoryRequest: OperationsHistoryRequest): Observable<OperationHistoryResponse>

    @POST(EndPoints.GET_LOAN_ENDPOINT)
    fun requestGetLoan(@Body getLoanRequest: GetLoanRequest): Observable<GetLoanResponse>

    @POST(EndPoints.PAYMENT_HISTORY_ENDPOINT)
    fun requestPaymentHistory(@Body paymentHistoryRequest: PaymentHistoryRequest): Observable<PaymentHistoryResponse>

    @POST(EndPoints.REQUEST_HISTORY_ENDPOINT)
    fun requestHistory(@Body paymentHistoryRequest: PaymentHistoryRequest): Observable<RequestHistoryResponse>

    @POST(EndPoints.GET_STORE_DETAILS)
    fun getStoreDetail(): Observable<StoreLocatorResponse>

    @POST(EndPoints.RATE_US_BEFORE_REDIRECTING_TO_PLAY_STORE_END_POINT)
    fun requestRateUsBeforeRedirectingToPlayStore(@Body getRateUsRequest: GetRateUsRequest): Observable<RateUsBeforeRedirectingToPlayStoreRespone>

    @POST(EndPoints.CHANGE_PASSWORD)
    fun changePassword(@Body changePasswordRequest: ChangePasswordRequest): Observable<ChangePasswordResponse>

    @POST(EndPoints.MONEY_TRANSFER_ENDPOINT)
    fun requestMoneyTransfer(@Body moneyTransferRequest: MoneyTransferRequest): Observable<MoneyTransferResponse>

    @POST(EndPoints.REQUEST_MONEY_ENDPOINT)
    fun requestMoneyRequest(@Body requestMoneyRequest: RequestMoneyRequest): Observable<RequestMoneyResponse>

    @POST(EndPoints.USUAGE_SUMMARY_ENDPOINT)
    fun requestUsageHistorySummary(@Body paymentHistoryRequest: PaymentHistoryRequest): Observable<UsageHistorySummaryResponse>

    @POST(EndPoints.SEND_SMS_ENDPOINT)
    fun sendSMS(@Body sendSMSRequest: SendSMSRequest): Observable<SendSMSResponse>

    @POST(EndPoints.FREE_SMS_STATUS_ENDPOINT)
    fun getFreeSmsStatus(): Observable<FreeSmsStatusResponse>

    @POST(EndPoints.VERIFY_PASSPORT_ENDPOINT)
    fun verifyPassport(@Body verifyPassportRequest: VerifyPassportRequest): Observable<VerifyPassportResponse>

    @POST(EndPoints.USAGE_DETAILS_ENDPOINT)
    fun getUsageDetails(@Body usageDetailsRequest: UsageDetailsRequest): Observable<UsageDetailsResponse>

    @POST(EndPoints.GET_FNF_LIST_ENDPOINT)
    fun getFNFList(@Body fnFRequest: FnFRequest): Observable<FNFResponse>

    @POST(EndPoints.ADD_FNF_ENDPOINT)
    fun addFNF(@Body addFNFRequest: AddFnfRequest): Observable<AddFnfResponse>

    @POST(EndPoints.DELETE_FNF_ENDPOINT)
    fun deleteFNF(@Body deleteFnFRequest: DeleteFnfRequest): Observable<DeleteFnfResponse>

    @POST(EndPoints.UPDATE_FNF_ENDPOINT)
    fun updateFNF(@Body updateFnFRequest: UpdateFNFRequest): Observable<UpdateFNFResponse>

    @POST(EndPoints.SUPPLEMENTARY_OFFERS_END_POINT)
    fun requestSupplementaryOffers(@Body supplementaryOffersRequest: SupplementaryOffersRequest): Observable<SupplementaryResponse>

    @POST(EndPoints.EXCHANGE_SERVICES_VALUES_END_POINT)
    fun getExchangeServicesValues(@Body exchangeServiceValueRequest: ExchangeServiceValuesRequest): Observable<ExchangeServicesValuesResponse>

    @POST(EndPoints.EXCHANGE_SERVICE_END_POINT)
    fun requestExchangeService(@Body exchangeServiceRequest: ExchangeServiceRequest): Observable<ExchangeServiceResponse>

    @POST(EndPoints.SPECIAL_OFFERS_END_POINT)
    fun requestSpecialOffers(@Body specialOffersRequest: SpecialOffersRequest): Observable<SupplementaryResponse>

    @POST(EndPoints.MY_SUBSCRIPTIONS_END_POINT)
    fun requestMySubscriptions(@Body mySubscriptionRequest: MySubscriptionRequest): Observable<MySubscriptionResponse>

    @POST(EndPoints.NAR_TV_SUBSCRIPTION_END_POINT)
    fun getNarTvSubscriptions(@Body narTvRequest: NarTvSubscriptionsRequest): Observable<NarTvSubscriptionResponse>

    @POST(EndPoints.NAR_TV_MIGRATION_END_POINT)
    fun requestNarTvMigration(@Body narTvRequest: NarTvMigrationRequest): Observable<NarTvMigrationResponse>

    @POST(EndPoints.CORE_SERVICES_END_POINT)
    fun requestCoreServices(@Body coreServicesRequest: CoreServicesRequest): Observable<CoreServicesResponse>

    @POST(EndPoints.CORE_SERVICES_PROCESS_END_POINT)
    fun requestProcessCoreService(@Body coreServiceProcessRequest: CoreServiceProcessRequest): Observable<CoreServicesProcessResponse>

    @POST(EndPoints.SUSPEND_NUMBER_END_POINT)
    fun requestSuspendNumber(@Body suspendNumberRequest: SuspendNumberRequest): Observable<SuspendNumberResponse>

    @POST(EndPoints.SUPPLEMENTARY_OFFERS_SUBSCRIBE_END_POINT)
    fun subscribeToSupplementaryOffer(@Body supplementaryOfferSubscribeRequest: SupplementaryOfferSubscribeRequest): Observable<MySubscriptionsSuccessAfterOfferSubscribeResponse>

    @POST(EndPoints.UPDATE_CUSTOMER_EMAIL_END_POINT)
    fun requestUpdateEmail(@Body updateEmailRequest: UpdateEmailRequest): Observable<UpdateEmailResponse>

    @POST(EndPoints.CHANGE_BILLING_LANGUAGE_END_POINT)
    fun requestChangeBillingLanguage(@Body changeBillingRequest: ChangeBillingRequest): Observable<ChangeBillingResponse>

    @POST(EndPoints.PROFILE_PICTURE_UPLOAD_END_POINT)
    fun requestUploadImageToServer(@Body profilePictureUploadRequest: ProfilePictureUploadRequest): Observable<ProfilePictureUpdateResponse>

    @POST(EndPoints.PROFILE_PICTURE_UPLOAD_END_POINT)
    fun requestRemoveProfileImageFromServer(@Body profilePictureUploadRequest: ProfilePictureUploadRequest): Observable<ProfilePictureUpdateResponse>

    @POST(EndPoints.CHANGE_TARIFF_END_POINT)
    fun requestSubscribeToTariff(@Body tariffSubscribeRequest: TariffSubscribeRequest): Observable<TariffSubscriptionResponse>

    @POST(EndPoints.SUPPLEMENTARY_OFFERS_SUBSCRIBE_END_POINT)
    fun changeSupplementaryOfferForCallDivert(@Body supplementaryOfferSubscribeRequest: SupplementaryOfferSubscribeRequest): Observable<MySubscriptionsSuccessAfterOfferSubscribeResponse>


    @POST(EndPoints.GET_FAST_TOPUP_PAYMENTS_END_POINT)
    fun requestGetFastPayments(): Observable<FastTopUpResponse>


    @POST(EndPoints.DELETE_FAST_TOPUP_PAYMENT_END_POINT)
    fun deleteFastTopUpPayment(@Body deleteFastPaymentRequest: DeleteFastPaymentRequest): Observable<DeleteItemResponse>


    @POST(EndPoints.MAKE_PAYMENT_END_POINT)
    fun makePayment(@Body makePaymentRequest: MakePaymentRequest): Observable<MakePaymentTopUpResponse>


    @POST(EndPoints.GET_SCHEDULED_PAYMENTS_END_POINT)
    fun requestGetScheduledPayments(): Observable<ScheduledPaymentResponse>


    @POST(EndPoints.DELETE_PAYMENT_SCHEDULER_END_POINT)
    fun deletePaymentScheduler(@Body deleteAutoPayment: DeleteAutoPayment): Observable<DeleteItemResponse>


    @POST(EndPoints.SAVED_CARDS_ENDPOINT)
    fun requestSavedCards(): Observable<SavedCardsResponse>


    @POST(EndPoints.INITIATE_PAYMENT_ENDPOINT)
    fun requestInitiatePayment(@Body initiatePaymentRequest: InitiatePaymentRequest): Observable<InitiatePaymentResponse>


    @POST(EndPoints.ADD_PAYMENT_SCHEDULER_ENDPOINT)
    fun requestAddSchedulePayment(@Body addPaymentSchedulerRequest: AddPaymentSchedulerRequest): Observable<AddSchedulePaymentResponse>

    @POST(EndPoints.REQUEST_DELETE_SAVED_CARD)
    fun deleteSavedCard(@Body deleteSavedCardRequest: DeleteSavedCardRequest): Observable<DeleteItemResponse>

    @POST(EndPoints.INTERNET_OFFERS_END_POINT)
    fun requestInternetOffers(@Body supplementaryOffersRequest: SupplementaryOffersRequest): Observable<InternetPacksResponse>

    @POST(EndPoints.CURRENT_PAYG_END_POINT)
    fun getCurrentPayGStatus(): Observable<WithOutPackResponse>

    @POST(EndPoints.GET_SURVEY_DATA)
    fun requestGetSurveys(): Observable<InAppSurvey>

    @POST(EndPoints.UPLOAD_SURVEY_DATA)
    fun saveInAppSurvey(@Body uploadSurvey: UploadSurvey): Observable<InAppSurvey>


}

