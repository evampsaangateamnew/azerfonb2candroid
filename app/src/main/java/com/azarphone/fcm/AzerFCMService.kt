package com.azarphone.fcm

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.AudioAttributes
import android.media.RingtoneManager
import androidx.core.app.NotificationCompat
import com.azarphone.R
import com.azarphone.ui.activities.splashactivity.SplashActivity
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.RootValues
import com.azarphone.util.getFCMDataFromLocalPreferences
import com.azarphone.util.logE
import com.azarphone.validators.hasValue
import com.azarphone.validators.isNumeric
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import me.leolin.shortcutbadger.ShortcutBadger
import java.util.*


/**
 * @author Junaid Hassan on 19, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class AzerFCMService : FirebaseMessagingService() {

    private var Description = ""
    private val fromClass = "AzerFCMService"

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        if (remoteMessage?.data != null
                && remoteMessage.data["title"] != null
                && remoteMessage.data["body"] != null
                && hasValue(remoteMessage.data["title"]) && hasValue(remoteMessage.data["body"])) {
//            logE("fcm", "message:::".plus(remoteMessage.notification!!.body), "AzerFCMService", "onMessageReceived")
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                /**apply the notification counter*/
                var notificationCount = -1
                if (remoteMessage.data["badge"] != null &&
                        isNumeric(remoteMessage.data["badge"]) &&
                        remoteMessage.data["badge"]!!.toInt() > 0) {
                    notificationCount = remoteMessage.data["badge"]!!.toInt()
                }
                showOreoBasedNotification(remoteMessage.data["title"]!!, remoteMessage.data["body"]!!, notificationCount)
            } else {
                /**apply the notification counter*/
                if (remoteMessage.data["badge"] != null &&
                        isNumeric(remoteMessage.data["badge"]) &&
                        remoteMessage.data["badge"]!!.toInt() > 0) {
                    ShortcutBadger.applyCount(this@AzerFCMService, remoteMessage.data["badge"]!!.toInt())
                }
                processBelowOreoNotification(remoteMessage.data["title"]!!, remoteMessage.data["body"]!!)
            }
        } else {
            if (remoteMessage.notification != null
                    && remoteMessage.notification!!.title != null
                    && remoteMessage.notification!!.body != null
                    && hasValue(remoteMessage.notification!!.title) && hasValue(remoteMessage.notification!!.body)) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    /**apply the notification counter*/
                    var notificationCount = -1
                    if (remoteMessage.data["badge"] != null &&
                            isNumeric(remoteMessage.data["badge"]) &&
                            remoteMessage.data["badge"]!!.toInt() > 0) {
                        notificationCount = remoteMessage.data["badge"]!!.toInt()
                    }
                    showOreoBasedNotification(remoteMessage.notification!!.title!!, remoteMessage.notification!!.body!!, notificationCount)
                } else {
                    /**apply the notification counter*/
                    if (remoteMessage.data["badge"] != null &&
                            isNumeric(remoteMessage.data["badge"]) &&
                            remoteMessage.data["badge"]!!.toInt() > 0) {
                        ShortcutBadger.applyCount(this@AzerFCMService, remoteMessage.data["badge"]!!.toInt())
                    }
                    processBelowOreoNotification(remoteMessage.notification!!.title!!, remoteMessage.notification!!.body!!)
                }
            }
        }
    }

    private fun processBelowOreoNotification(title: String, message: String) {
        /**Check the settings for the tone, mute and vibration*/
        if (getFCMDataFromLocalPreferences().data != null &&
                hasValue(getFCMDataFromLocalPreferences().data.toString()) &&
                hasValue(getFCMDataFromLocalPreferences().data.isEnable) &&
                hasValue(getFCMDataFromLocalPreferences().data.ringingStatus)) {

            //check for the ringing status
            if (getFCMDataFromLocalPreferences().data.ringingStatus.equals(ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_TONE, ignoreCase = true)) {
                /**show the notification with the sound*/
                showBelowOreoNotificationWithTone(title, message)
            } else if (getFCMDataFromLocalPreferences().data.ringingStatus.equals(ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_MUTE, ignoreCase = true)) {
                /**show the notification without the sound*/
                showBelowOreoNotificationWithMute(title, message)
            } else if (getFCMDataFromLocalPreferences().data.ringingStatus.equals(ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_VIBRATE, ignoreCase = true)) {
                /**show the notification with the vibration*/
                showBelowOreoNotificationWithVibration(title, message)
            }
        }//fcm data not null
    }//processBelowOreoNotification ends

    /*private fun processAboveOreoNotification(title: String, message: String) {
        */
    /**Check the settings for the tone, mute and vibration*//*
        if (getFCMDataFromLocalPreferences().data != null &&
                hasValue(getFCMDataFromLocalPreferences().data.toString()) &&
                hasValue(getFCMDataFromLocalPreferences().data.isEnable) &&
                hasValue(getFCMDataFromLocalPreferences().data.ringingStatus)) {

            //check for the ringing status
            if (getFCMDataFromLocalPreferences().data.ringingStatus.equals(ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_TONE, ignoreCase = true)) {
                */
    /**show the notification with the sound*//*
                showAboveOreoNotificationWithTone(title, message)
            } else if (getFCMDataFromLocalPreferences().data.ringingStatus.equals(ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_MUTE, ignoreCase = true)) {
                */
    /**show the notification without the sound*//*
                showAboveOreoNotificationWithMute(title, message)
            } else if (getFCMDataFromLocalPreferences().data.ringingStatus.equals(ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_VIBRATE, ignoreCase = true)) {
                */
    /**show the notification with the vibration*//*
                showAboveOreoNotificationWithVibration(title, message)
            }
        }//fcm data not null
    }//processAboveOreoNotification ends*/

    /**show notification with vibration only*/
    private fun showAboveOreoNotificationWithVibration(title: String, message: String) {

    }//showAboveOreoNotificationWithTone ends

    private fun showBelowOreoNotificationWithVibration(title: String, message: String) {
        val uniqueId = (Date().time / 1000L % Integer.MAX_VALUE)
        val intent = Intent(this, SplashActivity::class.java)
        intent.putExtra("notifyDataKey", "fromFCMPush")
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        val builder = NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.noticon)
                .setColor(applicationContext.resources.getColor(R.color.colorPrimary))
                .setLargeIcon(BitmapFactory.decodeResource(resources,
                        R.drawable.noticon))
                .setContentTitle(title)
                .setStyle(NotificationCompat.BigTextStyle()
                        .bigText(message))
                .setContentText(message)
                .setAutoCancel(true)
                .setVibrate(longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400))
                .setSound(null)
                .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(uniqueId.toInt(), builder.build())
    }//showBelowOreoNotificationWithVibration ends

    /**show the notification with the mute*/
    private fun showAboveOreoNotificationWithMute(title: String, message: String) {
        logE("channelX", "notification showing with mute", fromClass, "deleteOldNotificationChannel")
        deleteOldNotificationChannel()
        val uniqueId = (Date().time / 1000L % Integer.MAX_VALUE)

        val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel = NotificationChannel(resources.getString(R.string.default_notification_channel_id), resources.getString(R.string.notification_channel_name), importance)
            mChannel.description = Description
            mChannel.enableLights(true)
            mChannel.lightColor = Color.MAGENTA
            mChannel.enableVibration(false)
            mChannel.vibrationPattern = null
            mChannel.setShowBadge(true)
            mChannel.setSound(null, null)
            notificationManager.createNotificationChannel(mChannel)
        }//if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) ends

        val intent = Intent(this, SplashActivity::class.java)
        intent.putExtra("notifyDataKey", "fromFCMPush")
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        val builder = NotificationCompat.Builder(this, resources.getString(R.string.default_notification_channel_id))
                .setSmallIcon(R.drawable.noticon).setColor(applicationContext.resources.getColor(android.R.color.transparent))
                .setLargeIcon(BitmapFactory.decodeResource(resources,
                        R.drawable.noticon))
                .setContentTitle(title)
                .setContentText(message)
                .setStyle(NotificationCompat.BigTextStyle()
                        .bigText(message))
                .setAutoCancel(true)
                .setVibrate(null)
                .setSound(null)
                .setContentIntent(pendingIntent)

        notificationManager.notify(uniqueId.toInt(), builder.build())
    }//showAboveOreoNotificationWithMute ends

    private fun showBelowOreoNotificationWithMute(title: String, message: String) {
        val uniqueId = (Date().time / 1000L % Integer.MAX_VALUE)
        val intent = Intent(this, SplashActivity::class.java)
        intent.putExtra("notifyDataKey", "fromFCMPush")
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        val builder = NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.noticon)
                .setColor(applicationContext.resources.getColor(R.color.colorPrimary))
                .setLargeIcon(BitmapFactory.decodeResource(resources,
                        R.drawable.noticon))
                .setContentTitle(title)
                .setStyle(NotificationCompat.BigTextStyle()
                        .bigText(message))
                .setContentText(message)
                .setAutoCancel(true)
                .setVibrate(null)
                .setSound(null)
                .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(uniqueId.toInt(), builder.build())
    }//showBelowOreoNotificationWithMute ends

    /**show the notification with the tone*/
    private fun showAboveOreoNotificationWithTone(title: String, message: String) {
        logE("channelX", "notification showing with tone", fromClass, "deleteOldNotificationChannel")
        deleteOldNotificationChannel()
        val uniqueId = (Date().time / 1000L % Integer.MAX_VALUE)
        val notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val audioAttributes = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                .build()
        val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel = NotificationChannel(resources.getString(R.string.default_notification_channel_id), resources.getString(R.string.notification_channel_name), importance)
            mChannel.description = Description
            mChannel.enableLights(true)
            mChannel.lightColor = Color.MAGENTA
            mChannel.enableVibration(false)
            mChannel.vibrationPattern = null
            mChannel.setShowBadge(true)
            mChannel.setSound(notificationSound, audioAttributes)
            notificationManager.createNotificationChannel(mChannel)
        }//if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) ends

        val intent = Intent(this, SplashActivity::class.java)
        intent.putExtra("notifyDataKey", "fromFCMPush")
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        //set the notification
        val builder = NotificationCompat.Builder(this, resources.getString(R.string.default_notification_channel_id))
                .setSmallIcon(R.drawable.noticon).setColor(applicationContext.resources.getColor(android.R.color.transparent))
                .setLargeIcon(BitmapFactory.decodeResource(resources,
                        R.drawable.noticon))
                .setContentTitle(title)
                .setContentText(message)
                .setStyle(NotificationCompat.BigTextStyle()
                        .bigText(message))
                .setAutoCancel(true)
                .setVibrate(null)
                .setSound(notificationSound)
                .setContentIntent(pendingIntent)

        notificationManager.notify(uniqueId.toInt(), builder.build())
    }//showAboveOreoNotificationWithVibration ends

    private fun showBelowOreoNotificationWithTone(title: String, message: String) {
        val uniqueId = (Date().time / 1000L % Integer.MAX_VALUE)
        val intent = Intent(this, SplashActivity::class.java)
        intent.putExtra("notifyDataKey", "fromFCMPush")
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        //set the notification
        val notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val builder = NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.noticon)
                .setColor(applicationContext.resources.getColor(R.color.colorPrimary))
                .setLargeIcon(BitmapFactory.decodeResource(resources,
                        R.drawable.noticon))
                .setContentTitle(title)
                .setStyle(NotificationCompat.BigTextStyle()
                        .bigText(message))
                .setContentText(message)
                .setAutoCancel(true)
                .setVibrate(null)
                .setSound(notificationSound)
                .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(uniqueId.toInt(), builder.build())
    }//showBelowOreoNotificationWithTone ends

    private fun deleteOldNotificationChannel() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            logE("channelX", "deleting notification channel", fromClass, "deleteOldNotificationChannel")
            notificationManager.deleteNotificationChannel(resources.getString(R.string.default_notification_channel_id))

            if (notificationManager.getNotificationChannel(resources.getString(R.string.default_notification_channel_id)) != null) {
                logE("channelX", "notification channel not deleted", fromClass, "deleteOldNotificationChannel")
            } else {
                logE("channelX", "notification channel deleted successfully", fromClass, "deleteOldNotificationChannel")
            }
        }//if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) ends
    }//deleteOldNotificationChannel ends

    private fun showBelowOreoNotification(title: String, message: String) {
        val uniqueId = (Date().time / 1000L % Integer.MAX_VALUE)
        val intent = Intent(this, SplashActivity::class.java)
        intent.putExtra("notifyDataKey", "fromFCMPush")
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        //set the notification
        val notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val builder = NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.noticon)
                .setColor(applicationContext.resources.getColor(R.color.colorPrimary))
                .setLargeIcon(BitmapFactory.decodeResource(resources,
                        R.drawable.noticon))
                .setContentTitle(title)
                .setStyle(NotificationCompat.BigTextStyle()
                        .bigText(message))
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(notificationSound)
                .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(uniqueId.toInt(), builder.build())
    }

    private fun showOreoBasedNotification(title: String, message: String, notificationsCount: Int) {
        val uniqueId = (Date().time / 1000L % Integer.MAX_VALUE)

        val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel = NotificationChannel(resources.getString(R.string.default_notification_channel_id), resources.getString(R.string.notification_channel_name), importance)
            mChannel.description = Description
            mChannel.enableLights(true)
            mChannel.lightColor = Color.MAGENTA
            mChannel.enableVibration(true)
            mChannel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
            mChannel.setShowBadge(true)
            notificationManager.createNotificationChannel(mChannel)
        }

        val intent = Intent(this, SplashActivity::class.java)
        intent.putExtra("notifyDataKey", "fromFCMPush")
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        val pendingIntent = PendingIntent.getActivity(this, uniqueId.toInt(), intent, PendingIntent.FLAG_ONE_SHOT)
        //set the notification
        val notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        if (notificationsCount != -1) {
            val builder = NotificationCompat.Builder(this, resources.getString(R.string.default_notification_channel_id))
                    .setSmallIcon(R.drawable.noticon).setColor(applicationContext.resources.getColor(android.R.color.transparent))
                    .setLargeIcon(BitmapFactory.decodeResource(resources,
                            R.drawable.noticon))
                    .setContentTitle(title)
                    .setContentText(message)
                    .setStyle(NotificationCompat.BigTextStyle()
                            .bigText(message))
                    .setAutoCancel(true)
                    .setSound(notificationSound)
                    .setNumber(notificationsCount)
                    .setContentIntent(pendingIntent)

            notificationManager.notify(uniqueId.toInt(), builder.build())
        } else {
            val builder = NotificationCompat.Builder(this, resources.getString(R.string.default_notification_channel_id))
                    .setSmallIcon(R.drawable.noticon).setColor(applicationContext.resources.getColor(android.R.color.transparent))
                    .setLargeIcon(BitmapFactory.decodeResource(resources,
                            R.drawable.noticon))
                    .setContentTitle(title)
                    .setContentText(message)
                    .setStyle(NotificationCompat.BigTextStyle()
                            .bigText(message))
                    .setAutoCancel(true)
                    .setSound(notificationSound)
                    .setContentIntent(pendingIntent)

            notificationManager.notify(uniqueId.toInt(), builder.build())
        }
    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        if (hasValue(p0) &&
                getFCMDataFromLocalPreferences() != null &&
                getFCMDataFromLocalPreferences().data != null &&
                getFCMDataFromLocalPreferences().data.ringingStatus != null && hasValue(getFCMDataFromLocalPreferences().data.ringingStatus) &&
                getFCMDataFromLocalPreferences().data.isEnable != null && hasValue(getFCMDataFromLocalPreferences().data.isEnable)) {
            val notificationsData = getFCMDataFromLocalPreferences().data
            if (hasValue(p0)) {
                if (RootValues.getFCMEvents() != null) {
                    RootValues.getFCMEvents()!!.onFCMTokenRefreshed(p0!!, notificationsData.ringingStatus!!,
                            notificationsData.isEnable!!,
                            ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_API_CAUSE_LOGIN,
                            true)
                }
            }//if(TextCheckerTool.hasValue(p0)) ends
        }// if(BakcellPreferences.getNotificationSettingsData(applicationContext)!=null) ends
    }//onNewToken ends
}//fcm service ends