package com.azarphone

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Configuration
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.azarphone.analytics.Analytics
import com.azarphone.api.ApiClient
import com.azarphone.util.RootValues
import com.es.tec.LocalSharedPrefStorage
import com.google.firebase.crashlytics.FirebaseCrashlytics
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump

open class ProjectApplication() : MultiDexApplication() {


    lateinit var mLocalSharedPrefStorage: LocalSharedPrefStorage


    override fun onCreate() {
        super.onCreate()
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true)
//        throw IndexOutOfBoundsException("Test Crash 1289 Humayun 1226")

//        Stetho.initializeWithDefaults(this)
        RootValues.setAzeriSymbolFont(applicationContext)
        RootValues.setALSBoldFont(applicationContext)
        RootValues.setALSNormalFont(applicationContext)
        mInstance = this
        mLocalSharedPrefStorage = LocalSharedPrefStorage(this)
        ApiClient.newApiClientInstance.setInit(this)

        ViewPump.init(
            ViewPump.builder()
                .addInterceptor(CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/af/alsschlangesans.otf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build())

        // Analytics Inilization
        Analytics.initializeInstance(this)


    }

    fun getLocalPreferneces() = mLocalSharedPrefStorage


    override fun attachBaseContext(base: Context) {

        mLocaleManager = LocalManager(base)
        super.attachBaseContext(mLocaleManager.setLocale(base))
        MultiDex.install(this)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        mLocaleManager.setLocale(this)
    }


    override fun onTerminate() {
        super.onTerminate()
    }

    companion object {
        private lateinit var mInstance: ProjectApplication

        @SuppressLint("StaticFieldLeak")
        lateinit var mLocaleManager: LocalManager

        fun getInstance() = mInstance

    }


}