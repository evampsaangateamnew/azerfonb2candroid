package com.azarphone.eventhandler

/**
 * @author Junaid Hassan on 10, June, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
interface TariffsSearchResultListener{
    fun onSearchResult(isResultFound: Boolean)
}