package com.azarphone.eventhandler

/**
 * @author Junaid Hassan on 18, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
interface VerticalMenuClickEvents {
    fun onVerticalMenuClicked(verticalMenuKey: String)
}