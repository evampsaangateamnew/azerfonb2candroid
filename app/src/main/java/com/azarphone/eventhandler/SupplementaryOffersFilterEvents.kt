package com.azarphone.eventhandler

/**
 * @author Junaid Hassan on 30, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
interface SupplementaryOffersFilterEvents {
    fun onFilterApply()
}