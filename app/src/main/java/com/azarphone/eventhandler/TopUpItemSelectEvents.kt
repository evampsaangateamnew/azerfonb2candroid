package com.azarphone.eventhandler

/**
 * @author Junaid Hassan on 22, June, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
interface TopUpItemSelectEvents {
    fun onItemSelected(position: Int, searchKey: String)
}