package com.azarphone.eventhandler

/**
 * @author Junaid Hassan on 28, June, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
interface NotificationsCloserEvents{
    fun onCloseThisScreen()
}