package com.azarphone.eventhandler

/**
 * @author Junaid Hassan on 24, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
interface OfferFilterListener {
    fun onFilterCall(filterString:String)
}