package com.azarphone.eventhandler

import com.azarphone.api.pojo.response.tariffsresponse.TariffsResponse

/**
 * @author Junaid Hassan on 23, June, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
interface TariffsResponseEvents{
    fun onTariffsResponse(tariffsResponse: TariffsResponse)
    fun onTariffsBackgroundResponse(tariffsResponse: TariffsResponse)
    fun onNoTariffsResponseHideData()
    fun onTariffsResponseShowData()
}