package com.azarphone.eventhandler

import com.azarphone.api.pojo.response.mysubscriptions.successresponse.MySubscriptionsSuccessAfterOfferSubscribeResponse

/**
 * @author Junaid Hassan on 07, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
interface InternetOffersSubscriptionResponseEvents {
    fun onInternetSubscribeSuccessful(t: MySubscriptionsSuccessAfterOfferSubscribeResponse?)
    fun onInternetSubscribeError()
}