package com.azarphone.eventhandler

import com.azarphone.api.pojo.response.internetpackresponse.InternetPacksResponse

/**
 * @author Junaid Hassan on 07, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
interface InternetOffersResponseEvents {
    fun onResponseSuccessful(t: InternetPacksResponse?)
    fun onResponseError()
}