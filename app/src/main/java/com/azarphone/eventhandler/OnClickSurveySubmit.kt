package com.azarphone.eventhandler

import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey

/**
 * @author Muhammad Ahmed on 31, August, 2021
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * muhammad.ahmed@evampsaanga.com
 */
interface OnClickSurveySubmit {
    fun onClickSubmit(
        uploadSurvey: UploadSurvey,
        onTransactionComplete: String,
        updateListOnSuccessfullySurvey: UpdateListOnSuccessfullySurvey?
    )

}