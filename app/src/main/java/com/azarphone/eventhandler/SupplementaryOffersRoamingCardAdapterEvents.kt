package com.azarphone.eventhandler

/**
 * @author Junaid Hassan on 24, June, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
interface SupplementaryOffersRoamingCardAdapterEvents{
    fun onSupplementaryOfferSubscribeButtonClick(titleMessage: String, offeringId: String, offeringName: String, actionType: String)
}