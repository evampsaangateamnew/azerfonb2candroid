package com.azarphone.eventhandler

/**
 * @author Junaid Hassan on 06, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
interface SupplementaryOffersCardAdapterEvents {
    fun onSupplementaryOfferSubscribeButtonClick(titleMessage: String, offeringId: String, offeringName: String, actionType: String)
}