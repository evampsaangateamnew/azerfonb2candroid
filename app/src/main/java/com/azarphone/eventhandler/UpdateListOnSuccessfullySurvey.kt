package com.azarphone.eventhandler

/**
 * @author Muhammad Ahmed on 31, August, 2021
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * muhammad.ahmed@evampsaanga.com
 */
interface UpdateListOnSuccessfullySurvey {
    fun updateListOnSuccessfullySurvey()

}