package com.azarphone.eventhandler

interface OnLanguageChangeHandler{
    fun onLanguageChange()
}