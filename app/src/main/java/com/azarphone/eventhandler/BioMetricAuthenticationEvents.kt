package com.azarphone.eventhandler

/**
 * @author Junaid Hassan on 17, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
interface BioMetricAuthenticationEvents{
    fun onBiometricAuthenticationError(errorCode:Int,errorMessage:String)

    fun onBiometricAuthenticationSuccess()

    fun onBiometricAuthenticationFailed()
}