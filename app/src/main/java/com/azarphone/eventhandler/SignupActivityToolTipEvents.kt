package com.azarphone.eventhandler

/**
 * @author Junaid Hassan on 14, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
interface SignupActivityToolTipEvents {
    fun onInvalidMsisdnNumberTooltip(errorDetail: String)

    fun onInvalidOTPPINTooltip(errorDetail: String)

    fun onInvalidPasswordTooltip(errorDetail: String)

    fun onInvalidConfirmPasswordTooltip(errorDetail: String)
}