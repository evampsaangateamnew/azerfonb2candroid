package com.azarphone.eventhandler

import com.azarphone.api.pojo.response.nartvsubscriptionsresponse.PlansItem

interface NarTvSubscriptionsEventHandler {
    fun onSubscriptionSelected(item: PlansItem)
}