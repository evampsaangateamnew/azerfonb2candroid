package com.azarphone.eventhandler

/**
 * @author Junaid Hassan on 04, July, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
interface RateUsAndroidOfflineEvents{
    fun onNeedToShowRateUsPopUp()
}