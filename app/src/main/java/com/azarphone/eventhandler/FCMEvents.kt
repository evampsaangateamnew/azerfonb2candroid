package com.azarphone.eventhandler

/**
 * @author Junaid Hassan on 07, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
interface FCMEvents {
    fun onFCMTokenRefreshed(token: String, ringingStatus: String, isEnable: String, cause: String, isBackground: Boolean)
}