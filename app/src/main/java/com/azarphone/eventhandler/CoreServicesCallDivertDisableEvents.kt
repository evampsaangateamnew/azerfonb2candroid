package com.azarphone.eventhandler

/**
 * @author Junaid Hassan on 23, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
interface CoreServicesCallDivertDisableEvents {
    fun onCallDivertDisabled(offeringId: String, offeringName: String, actionType: String)
}