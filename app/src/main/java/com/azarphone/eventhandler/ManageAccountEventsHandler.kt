package com.azarphone.eventhandler

import com.azarphone.api.pojo.response.customerdata.CustomerData

interface ManageAccountEventsHandler{
    fun onSwitchNumberClick(customerData: CustomerData)

    fun onDeleteNumberClick(customerData: CustomerData)
}