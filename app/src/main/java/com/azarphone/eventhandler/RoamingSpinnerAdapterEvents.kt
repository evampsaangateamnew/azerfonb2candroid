package com.azarphone.eventhandler

/**
 * @author Junaid Hassan on 27, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
interface RoamingSpinnerAdapterEvents {
    fun onCountrySelectedForRoaming(countrySelected: String, countryFlagSelected: String)
}