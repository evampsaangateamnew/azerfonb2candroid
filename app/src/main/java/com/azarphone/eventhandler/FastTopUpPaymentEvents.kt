package com.azarphone.eventhandler

/**
 * @author Umair Mustafa on 02/04/2021
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * umair.mustafa@evampsaanga.com
 */
interface FastTopUpPaymentEvents {
    fun onPayButtonClicked(msisdnToEdit:String)
}