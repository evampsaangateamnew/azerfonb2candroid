package com.azarphone.eventhandler

import com.azarphone.api.pojo.response.customerdata.CustomerData

interface MainActivityEventsHandler{
    fun onSwitchNumberClick(customerData: CustomerData)
}