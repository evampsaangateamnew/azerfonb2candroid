package com.azarphone.eventhandler

/**
 * @author Junaid Hassan on 25, June, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
interface SupplementarySpecialOffersCardAdapterEvents{
    fun onSupplementaryOfferSubscribeButtonClick(titleMessage: String, offeringId: String, offeringName: String, actionType: String)
}