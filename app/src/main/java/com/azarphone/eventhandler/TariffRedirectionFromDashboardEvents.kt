package com.azarphone.eventhandler

/**
 * @author Junaid Hassan on 15, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
interface TariffRedirectionFromDashboardEvents {
    fun onRedirectToTariffsScreen(offeringId: String)
}