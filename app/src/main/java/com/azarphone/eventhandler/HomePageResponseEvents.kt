package com.azarphone.eventhandler

import com.azarphone.api.pojo.response.gethomepageresponse.GetHomePageResponse

/**
 * @author Junaid Hassan on 23, June, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
interface HomePageResponseEvents{
    fun onHomePageResponse(getHomePageResponse: GetHomePageResponse)

    fun onCancelSwipeRefresh()
}