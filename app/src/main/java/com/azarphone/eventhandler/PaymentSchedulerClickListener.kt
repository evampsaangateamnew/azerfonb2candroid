package com.azarphone.eventhandler

interface PaymentSchedulerClickListener {
    fun paymentSchedulerClickListener(amount: String, recurrence: String, billingCycleDaily: String, savedCardId: String, dateForAPI: String)
}