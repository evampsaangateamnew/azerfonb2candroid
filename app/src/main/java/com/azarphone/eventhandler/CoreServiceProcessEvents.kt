package com.azarphone.eventhandler

/**
 * @author Junaid Hassan on 03, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
interface CoreServiceProcessEvents {
    fun onCoreServiceProcessed(messageTitle: String, actionType: String, offeringId: String)
}