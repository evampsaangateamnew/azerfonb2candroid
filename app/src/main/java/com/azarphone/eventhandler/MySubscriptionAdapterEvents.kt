package com.azarphone.eventhandler

/**
 * @author Junaid Hassan on 20, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
interface MySubscriptionAdapterEvents{
    fun onMySubscriptionsSuccessReponse()
}