package com.azarphone.eventhandler

/**
 * @author Junaid Hassan on 25, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
interface AzerFonActionBarEvents {
    fun onDashboardSetActionBar()
    fun onTariffsSetActionBar()
    fun onServicesMenuSetActionBar()
    fun onTopupMenuSetActionBar()
    fun onMyAccountMenuSetActionBar()
    fun onDummyFragmentSetActionBar()
    fun onInternetFragmentSetActionBar()
}