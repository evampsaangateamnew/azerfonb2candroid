package com.azarphone.eventhandler

/**
 * @author Junaid Hassan on 29, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
interface FnFEditButtonEvents{
    fun onEditButtonClicked(msisdnToEdit:String)
}