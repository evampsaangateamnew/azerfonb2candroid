package com.azarphone.eventhandler

interface LoginEventHandlers{

    fun forgotPassword()
    fun login()
    fun signUp()
    fun fingerPrintReader()


}