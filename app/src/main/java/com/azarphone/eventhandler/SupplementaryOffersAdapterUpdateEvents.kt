package com.azarphone.eventhandler

/**
 * @author Junaid Hassan on 21, June, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
interface SupplementaryOffersAdapterUpdateEvents {
    fun onAdapterNeedToBeNotify()
}