package com.azarphone.eventhandler

import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.ui.dialogs.InAppFeedbackDialog

/**
 * @author Muhammad Ahmed on 31, August, 2021
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * muhammad.ahmed@evampsaanga.com
 */
interface CallingSurveyFromDialog {
    fun onCallingSurveyFromDialog(
        uploadSurvey: UploadSurvey?,
        onTransactionComplete: String?,
        inAppFeedbackDialog: InAppFeedbackDialog?,
        updateListOnSuccessfullySurvey: UpdateListOnSuccessfullySurvey?
    )

}