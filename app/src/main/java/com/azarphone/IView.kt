package com.azarphone

import androidx.lifecycle.LifecycleOwner
import com.azarphone.api.pojo.response.BaseResponse

/**
 * Created by vophamtuananh on 4/8/17.
 */
interface IView : LifecycleOwner {
    fun showLoading()

    fun hideLoading()

    fun loadError(e: Throwable)

    fun loadError(msg: String)

    fun <T>isErrorErrorDialoge(response: BaseResponse<T>):Boolean

    fun <T>isErrorErrorDialoge(response: BaseResponse<T>,isForceLogout:Boolean):Boolean
}
