package com.azarphone.datamanagers

import com.azarphone.api.pojo.response.customerdata.CustomerData
import com.azarphone.api.pojo.response.gethomepageresponse.GetHomePageResponse
import com.azarphone.api.pojo.response.mysubscriptions.response.MySubscriptionResponse
import com.azarphone.api.pojo.response.paymenthistory.PaymentHistoryResponse
import com.azarphone.api.pojo.response.predefinedata.PredefinedData
import com.azarphone.api.pojo.response.requesthistory.RequestHistoryResponse
import com.azarphone.api.pojo.response.supplementaryoffers.helper.SupplementaryOffer
import com.azarphone.api.pojo.response.inappsurvey.InAppSurvey

/**
 * @author Junaid Hassan on 13, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
object UserDataManager {

    private var isPromoMessageDisplayed = false
    private var promoMessage: String? = null
    private var supplementaryFilterKeysList = ArrayList<String>()
    private var termsAndConditionsData: String? = null
    private var saveCustomerDataJson: CustomerData? = null
    private var predefineData: PredefinedData? = null
    private var isAppResumeCalledWithSuccess = false
    private var isGetHomePageCalledWithSuccess = false
    private var getHomePageResponse: GetHomePageResponse? = null
    private var requestHistoryResponse: RequestHistoryResponse? = null
    private var paymentHistoryResponse: PaymentHistoryResponse? = null
    private var isChangeLanguageSuccess = false
    private var mySubscriptionResponse: MySubscriptionResponse? = null
    private val countryRoamingDataList = ArrayList<SupplementaryOffer?>()
    private var actionIdFromServer = ""
    private var callDivertNumber = ""
    private var successOfferingIdForCoreService = ""
    private var playStoreRedirectionURL = ""
    private var isUsageDetailsVerified = false
    private var isUsageDetailsCalled = false
    private var rateUsAndroid = "1"
    private var inAppSurvey: InAppSurvey? = null


    fun getInAppSurvey() = inAppSurvey
    fun setInAppSurvey(inAppSurvey: InAppSurvey) {
        this.inAppSurvey = inAppSurvey
    }

    fun getRateUsAndroid(): String {
        return rateUsAndroid
    }

    fun setRateUsAndroid(rateUsAndroid: String) {
        this.rateUsAndroid = rateUsAndroid
    }

    fun isUsageDetailsCalled(): Boolean {
        return isUsageDetailsCalled
    }

    fun setUsageDetailsCalled(isUsageDetailsCalled: Boolean) {
        this.isUsageDetailsCalled = isUsageDetailsCalled
    }

    fun isUsageDetailsVerified(): Boolean {
        return isUsageDetailsVerified
    }

    fun setUsageDetailsVerified(isUsageDetailsVerified: Boolean) {
        this.isUsageDetailsVerified = isUsageDetailsVerified
    }

    fun setPlayStoreRedirectionURL(playStoreRedirectionURL: String) {
        this.playStoreRedirectionURL = playStoreRedirectionURL
    }

    fun getPlayStoreRedirectionURL(): String {
        return playStoreRedirectionURL
    }

    fun setSuccessOfferingIdForCoreService(successOfferingIdForCoreService: String) {
        this.successOfferingIdForCoreService = successOfferingIdForCoreService
    }

    fun getSuccessOfferingIdForCoreService(): String {
        return successOfferingIdForCoreService
    }

    fun setCallDivertNumber(callDivertNumber: String) {
        this.callDivertNumber = callDivertNumber
    }

    fun getCallDivertNumber(): String {
        return callDivertNumber
    }

    fun setActionIdFromServer(actionIdFromServer: String) {
        this.actionIdFromServer = actionIdFromServer
    }

    fun getActionIdFromServer(): String {
        return actionIdFromServer
    }

    fun setIsPromoMessageDisplayed(isPromoMessageDisplayed: Boolean) {
        this.isPromoMessageDisplayed = isPromoMessageDisplayed
    }

    fun isPromoMessageDisplayed(): Boolean {
        return isPromoMessageDisplayed
    }

    fun getPromoMessage(): String? {
        return promoMessage
    }

    fun setPromoMessage(promoMessage: String?) {
        this.promoMessage = promoMessage
    }

    fun getCountryRoamingDataList(): ArrayList<SupplementaryOffer?> {
        return countryRoamingDataList
    }

    fun setMySubscriptionResponse(mySubscriptionResponse: MySubscriptionResponse?) {
        this.mySubscriptionResponse = mySubscriptionResponse
    }

    fun getMySubscriptionResponse(): MySubscriptionResponse? {
        return mySubscriptionResponse
    }

    fun getSupplementaryFilterKeysList(): ArrayList<String> {
        return supplementaryFilterKeysList
    }

    fun setChangeLanguageSuccess(isChangeLanguageSuccess: Boolean) {
        this.isChangeLanguageSuccess = isChangeLanguageSuccess
    }

    fun isChangeLanguageSuccess(): Boolean {
        return isChangeLanguageSuccess
    }

    fun setPaymentHistoryResponse(paymentHistoryResponse: PaymentHistoryResponse?) {
        this.paymentHistoryResponse = paymentHistoryResponse
    }

    fun getPaymentHistoryResponse(): PaymentHistoryResponse? {
        return paymentHistoryResponse
    }

    fun setRequestHistoryResponse(requestHistoryResponse: RequestHistoryResponse?) {
        this.requestHistoryResponse = requestHistoryResponse
    }

    fun getRequestHistoryResponse(): RequestHistoryResponse? {
        return requestHistoryResponse
    }

    fun setHomePageResponse(getHomePageResponse: GetHomePageResponse?) {
        this.getHomePageResponse = getHomePageResponse
    }

    fun getHomePageResponse(): GetHomePageResponse? {
        return getHomePageResponse
    }

    fun isGetHomePageCalledWithSuccess(): Boolean {
        return isGetHomePageCalledWithSuccess
    }

    fun setIsGetHomePageCalledWithSuccess(isGetHomePageCalledWithSuccess: Boolean) {
        this.isGetHomePageCalledWithSuccess = isGetHomePageCalledWithSuccess
    }

    fun isAppResumeCalledWithSuccess(): Boolean {
        return isAppResumeCalledWithSuccess
    }//isAppResumeCalledWithSuccess ends

    fun setIsAppResumeCalledWithSuccess(isAppResumeCalledWithSuccess: Boolean) {
        this.isAppResumeCalledWithSuccess = isAppResumeCalledWithSuccess
    }//setAppResumeCalledWithSuccess ends

    fun setTermsAndConditionsDataString(termsAndConditionsData: String?) {
        this.termsAndConditionsData = termsAndConditionsData
    }//setTermsAndConditionsDataString ends

    fun getTermsAndConditionsDataString(): String? {
        return termsAndConditionsData
    }//getTermsAndConditionsDataString ends

    fun saveCustomerData(saveCustomerDataJson: CustomerData?) {
        this.saveCustomerDataJson = saveCustomerDataJson
    }//saveCustomerData ends

    fun getCustomerData(): CustomerData? {
        return saveCustomerDataJson
    }//getCustomerData ends

    fun savePredefineData(predefineData: PredefinedData?) {
        this.predefineData = predefineData
    }//savePredefineData ends

    fun getPredefineData(): PredefinedData? {
        return predefineData
    }//getPredefineData ends

    inline fun getCustomerMSISDN(): String {
//        var number = ConstantsUtility.UserConstants.USERNAME_PREFIX
        var number = "+994 "
        getCustomerData()?.let { it ->
            it.msisdn.let {
                number += it
            }
        }
        return number
    }

    fun resetUserDataManager() {
        //refresh users data
        getSupplementaryFilterKeysList().clear()
        setChangeLanguageSuccess(false)
        setPaymentHistoryResponse(null)
        setRequestHistoryResponse(null)
        setTermsAndConditionsDataString(null)
        saveCustomerData(null)
        savePredefineData(null)
        setHomePageResponse(null)
        setIsGetHomePageCalledWithSuccess(false)
        setIsAppResumeCalledWithSuccess(false)
        setMySubscriptionResponse(null)
        setActionIdFromServer("")
        setCallDivertNumber("")
        setSuccessOfferingIdForCoreService("")
        setUsageDetailsVerified(false)
        setUsageDetailsCalled(false)
        setRateUsAndroid("1")
    }//resetUserDataManager ends
}//UserDataManager ends here