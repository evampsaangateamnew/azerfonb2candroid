package com.azarphone.analytics

object EventValues{

    object LoginEvents {
        val login = "login"
        val user_login= "user_login"

    }

    object SignUpEvents {
        val sign_up= "sign_up"
        val registered = "registered"

    }

    object ForgotEvents {
        val forgot= "forgot"
        val recovered = "recovered"

    }

    object TariffEvents {
        val tariff_migration = "tariff_migration"
        val migration= "migration"
        val special_tariff_migration="special_tariff_migration"
        val subscription="subscription"

    }

    object RedirectEvents {
        val share= "share"
        val redirected_to_invite= "redirected_to_invite"

    }
    object TutorialsEvents {
        val tutorial_begin = "tutorial_begin"
        val begin= "begin"
        val tutorial_complete= "tutorial_complete"
        val complete = "complete"
    }

    object ExchangeServiceEvents {
        val exchange_offer= "exchange_offer"
        val voice_to_data = "voice_to_data"
        val data_to_voice= "data_to_voice"
    }

    object MySubscriptionEvents {
        val my_subscription= "my_subscription"
        val renewal = "renewal"
        val deactivation="deactivation"
    }

    object SpecialOfferSubscriptionEvents {
        val special_offer_subscription= "special_offer_subscription"
        val subscription= "subscription"
    }

    object RoamingSubscriptionEvents {
        val roaming_subscription = "roaming_subscription"
        val subscription= "subscription"
    }

    object PackageSubscriptionEvents {
        val package_subscription= "package_subscription"
        val subscription= "subscription"
    }

    object GetCreditEvents {
        val get_credit= "get_credit"
        val me_credit = "me_credit"
        val friends_credit="friends_credit"
    }

    object TopUpEvents {
        val top_up = "top_up"
        val nar_card = "nar_card"
        val plastic_card_redirection ="plastic_card_redirection"
    }

    object SearchEvents {
        val search = "search"
        val tariff_screen = "tariff_screen"
        val packages_screen ="packages_screen"
        val special_offers_screen ="special_offers_screen"
        val my_subscription_screen ="my_subscription_screen"
    }

    object GenericValues{
        val Success = "1"
        val Failure = "0"
    }
}