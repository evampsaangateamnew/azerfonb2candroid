package com.azarphone.analytics

import android.content.Context
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics
import java.util.HashMap
import android.util.StatsLog.logEvent



object Analytics{

    var mFirebaseAnalytics: FirebaseAnalytics? = null

    fun initializeInstance(context: Context) {
        if (context != null) {
            if (mFirebaseAnalytics == null) {
                mFirebaseAnalytics = FirebaseAnalytics.getInstance(context)
            }
        }
    }



    fun logRoamingSubscriptionEvent(status: String) {
        var eventMap = HashMap<String, String?>()
            eventMap.put(EventValues.RoamingSubscriptionEvents.subscription, status)

        logEventToFirebase(EventValues.RoamingSubscriptionEvents.roaming_subscription, eventMap)
    }

    fun logPackageSubscriptionEvent(status: String) {
        var eventMap = HashMap<String, String?>()
        eventMap.put(EventValues.PackageSubscriptionEvents.subscription, status)

        logEventToFirebase(EventValues.PackageSubscriptionEvents.package_subscription, eventMap)
    }

    fun logGetCreditEvent(status: String,myCredit:Boolean) {
        var eventMap = HashMap<String, String?>()

        if(myCredit) {
            eventMap.put(EventValues.GetCreditEvents.me_credit, status)
        }
else {
            eventMap.put(EventValues.GetCreditEvents.friends_credit, status)
        }

        logEventToFirebase(EventValues.GetCreditEvents.get_credit, eventMap)
    }

    fun logExchangeServiceEvent(status: String, dataToVoice: Boolean) {
        var eventMap = HashMap<String, String?>()
        if(dataToVoice){
            eventMap.put(EventValues.ExchangeServiceEvents.data_to_voice, status)
        }
        else {
            eventMap.put(EventValues.ExchangeServiceEvents.voice_to_data, status)
        }


        logEventToFirebase(EventValues.ExchangeServiceEvents.exchange_offer, eventMap)
    }

    private fun logEventToFirebase(_mainEventKey: String, eventProperty: HashMap<String, String?>) {

        var mainEventKey = _mainEventKey.replace("\\s+".toRegex(), "_")
        mainEventKey = mainEventKey.substring(0, Math.min(mainEventKey.length, 39))

        val bundle = Bundle()

        for (param in eventProperty) {
            var eventKey = param.key.replace("\\s+".toRegex(), "_")
            eventKey = eventKey.substring(0, Math.min(eventKey.length, 39))
            bundle.putString(eventKey, param.value)
        }

        mFirebaseAnalytics?.logEvent(mainEventKey, bundle)
    }

    fun logAppEvent( eventName: String,eventType: String,status:String) {
        var eventName = eventName
        var contentType = eventType
       // var eventName = screenEvent
        try {

            eventName = eventName.substring(0, Math.min(eventName.length, 39))
            eventName = eventName.replace(" ".toRegex(), "_")
          /*  if (Tools.hasValue(screenName)) {
                screenName = screenName.toLowerCase()
            } else {
                return
            }*/

            contentType = contentType.substring(0, Math.min(eventType.length, 99))
            eventName = eventName.substring(0, Math.min(eventName.length, 99))

            val bundle = Bundle()
          //  bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, eventName)
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, contentType)
            bundle.putString(FirebaseAnalytics.Param.SUCCESS, status)

//            Log.d("Events",contentType)
//            Log.d("Events",eventName)

            mFirebaseAnalytics?.logEvent(eventName, bundle)

        } catch (e: Exception) {
            //Logger.debugLog(Logger.TAG_CATCH_LOGS, e.message)
        }

    }

    fun logSearchEvent( eventName: String,eventType: String,searchString:String) {
        var eventName = eventName
        var contentType = eventType
        // var eventName = screenEvent
      /*  try {
*/
            eventName = eventName.substring(0, Math.min(eventName.length, 39))
            eventName = eventName.replace(" ".toRegex(), "_")
            /*  if (Tools.hasValue(screenName)) {
                  screenName = screenName.toLowerCase()
              } else {
                  return
              }*/

            contentType = contentType.substring(0, Math.min(eventType.length, 99))
            eventName = eventName.substring(0, Math.min(eventName.length, 99))

            val bundle = Bundle()
            //  bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, eventName)
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, contentType)
            bundle.putString(FirebaseAnalytics.Param.SEARCH_TERM, searchString)

            mFirebaseAnalytics?.logEvent(eventName, bundle)

       /* } catch (e: Exception) {
            //Logger.debugLog(Logger.TAG_CATCH_LOGS, e.message)
        }*/

    }

}