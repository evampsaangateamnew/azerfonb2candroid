package com.azarphone.util

import com.azarphone.ProjectApplication

/**
 * @author Junaid Hassan on 28, June, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
inline fun getOkButtonLabel(): String {
    return when {
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.EN, ignoreCase = true) -> "Ok"
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.AZ, ignoreCase = true) -> "BƏLİ"
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.RU, ignoreCase = true) -> "ДА"
        else -> "Ok"
    }
}

inline fun getCancelButtonLabel(): String {
    return when {
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.EN, ignoreCase = true) -> "Cancel"
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.AZ, ignoreCase = true) -> "Ləğv et"
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.RU, ignoreCase = true) -> "Отменить"
        else -> "Ok"
    }
}

inline fun getSubscribeButtonLabel(): String {
    return when {
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.EN, ignoreCase = true) -> "Subscribe"
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.AZ, ignoreCase = true) -> "Abunə ol"
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.RU, ignoreCase = true) -> "Подпишись"
        else -> "Subscribe"
    }
}

inline fun getRenewButtonLabel(): String {
    return when {
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.EN, ignoreCase = true) -> "Renew"
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.AZ, ignoreCase = true) -> "Yenilə"
        else -> "Обновить"
    }
}

inline fun getSubscribedButtonLabel(): String {
    return when {
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.EN, ignoreCase = true) -> "Subscribed"
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.AZ, ignoreCase = true) -> "Qoşulub"
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.RU, ignoreCase = true) -> "Подключен"
        else -> "Subscribed"
    }
}

inline fun getTopupLabel(): String {
    return when {
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.EN, ignoreCase = true) -> "Top up"
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.AZ, ignoreCase = true) -> "Artır"
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.RU, ignoreCase = true) -> "Пополнить"
        else -> "Top up"
    }
}

inline fun getDeactivateLabel(): String {
    return when {
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.EN, ignoreCase = true) -> "Deactivate"
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.AZ, ignoreCase = true) -> "Deaktiv et"
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.RU, ignoreCase = true) -> "Отключить"
        else -> "Deactivate"
    }
}

inline fun getLocalizedDetailsTabTitle(): String {
    return when {
        ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "Details"
        ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Ətraflı"
        else -> "Подробно"
    }
}