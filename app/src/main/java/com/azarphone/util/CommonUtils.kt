package com.azarphone.util

import android.os.Build



inline fun isAtLeastVersion(version:Int):Boolean{
    return Build.VERSION.SDK_INT >= version
}
