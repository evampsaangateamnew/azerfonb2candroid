package com.azarphone.util

import com.azarphone.ProjectApplication
import java.text.SimpleDateFormat

/**
 * @author Junaid Hassan on 02, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */

inline fun isAppRaterDialogShownToUser(): Boolean {
    var isAlreadyShown = false
    try {

        if (ProjectApplication.getInstance().getLocalPreferneces().getBool(
                        ProjectApplication.getInstance().getLocalPreferneces().PREF_APP_RATER_SHOWN, false)) {
            isAlreadyShown = true
            logE("shownToUser", "alreadyShow", "RateUsUtil", "isAppRaterDialogShownToUser")
        } else {
            isAlreadyShown = false
            logE("shownToUser", "NotShow", "RateUsUtil", "isAppRaterDialogShownToUser")
        }
    } catch (exp: Exception) {
        logE("shownToUser", "error:::".plus(exp.toString()), "RateUsUtil", "isAppRaterDialogShownToUser")
        return isAlreadyShown
    }
    return isAlreadyShown
}

inline fun getMayBeLaterDateTime(): String {
    return ProjectApplication.getInstance().getLocalPreferneces().getString(
            ProjectApplication.getInstance().getLocalPreferneces().MAY_BE_LATER_TIME, "")!!
}

inline fun setMayBeLaterDateTime(mayBeLaterTime:String){
    ProjectApplication.getInstance().getLocalPreferneces().addString(
            ProjectApplication.getInstance().getLocalPreferneces().MAY_BE_LATER_TIME, mayBeLaterTime)
}

inline fun getUserLoginTime(): String {
    return ProjectApplication.getInstance().getLocalPreferneces().getString(
            ProjectApplication.getInstance().getLocalPreferneces().USER_LOGIN_TIME, "")!!
}

inline fun setUserLoginTime(loginTime: String) {
    ProjectApplication.getInstance().getLocalPreferneces().addString(
            ProjectApplication.getInstance().getLocalPreferneces().USER_LOGIN_TIME, loginTime)
}

inline fun getDatesDifferenceForRateUs(firstDate: String, secondDate: String, key: String): Long {
    val format = SimpleDateFormat(ConstantsUtility.DateFormats.ddDASHMMDASHyyWithTime)
    val date1 = format.parse(firstDate)
    val date2 = format.parse(secondDate)

    val diff = date1.time - date2.time
    val seconds = diff / 1000
    val minutes = seconds / 60
    val hours = minutes / 60
    val days = hours / 24

    return if (key.equals("h", ignoreCase = true)) {
        //give hours
        hours
    } else {
        //give days
        days
    }
}