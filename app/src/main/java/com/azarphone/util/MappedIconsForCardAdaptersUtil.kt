package com.azarphone.util

import com.azarphone.R

/**
 * @author Junaid Hassan on 27, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
object IconMapping {
    const val calls = "calls"
    const val InternationalCalls = "InternationalCalls"
    const val RoamingCallsOut = "RoamingCallsOut"
    const val RoamingCallsIn = "RoamingCallsIn"
    const val countrywideSMS = "countrywideSMS"
    const val smsInternational = "smsInternational"
    const val smsRoaming = "smsRoaming"
    const val MMS = "MMS"
    const val internet = "internet"
    const val roamingInternet = "roamingInternet"
    const val internetNight = "internetNight"
    const val whatsapp = "whatsapp"
    const val Facebook = "Facebook"
    const val msTwitter = "msTwitter"
    const val msSnap = "msSnap"
    const val YouTube = "YouTube"
    const val Instagram = "Instagram "
    const val vkonakte = "vkonakte"
    const val telegramUsing = "telegramUsing"
    const val exchange = "exchange"
    const val detailsIcon = "detailsIcon"
    const val flag = "flagIcon"
    const val Socialpack = "Socialpack"
    const val tsDRounding="tsDRounding"
}//IconMapping ends

inline fun getCardAdapterMappedImage(imageKey: String): Int {
    return when {
        imageKey.equals(IconMapping.tsDRounding, ignoreCase = true) -> R.drawable.roundingicon
        imageKey.equals(IconMapping.countrywideSMS, ignoreCase = true) -> R.drawable.smsicon
        imageKey.equals(IconMapping.calls, ignoreCase = true) -> R.drawable.countrywidecall
        imageKey.equals(IconMapping.InternationalCalls, ignoreCase = true) -> R.drawable.international_calls
        imageKey.equals(IconMapping.RoamingCallsOut, ignoreCase = true) -> R.drawable.roaming_calls_outgoing
        imageKey.equals(IconMapping.RoamingCallsIn, ignoreCase = true) -> R.drawable.roaming_calls_incoming
        imageKey.equals(IconMapping.smsInternational, ignoreCase = true) -> R.drawable.smsint
        imageKey.equals(IconMapping.smsRoaming, ignoreCase = true) -> R.drawable.smsroaming
        imageKey.equals(IconMapping.MMS, ignoreCase = true) -> R.drawable.mmsicon
        imageKey.equals(IconMapping.internet, ignoreCase = true) -> R.drawable.countrywideinternet
        imageKey.equals(IconMapping.roamingInternet, ignoreCase = true) -> R.drawable.raoming_internet
        imageKey.equals(IconMapping.internetNight, ignoreCase = true) -> R.drawable.internet_night
        imageKey.equals(IconMapping.whatsapp, ignoreCase = true) -> R.drawable.whatsapp
        imageKey.equals(IconMapping.Facebook, ignoreCase = true) -> R.drawable.fb
        imageKey.equals(IconMapping.msTwitter, ignoreCase = true) -> R.drawable.twitter
        imageKey.equals(IconMapping.msSnap, ignoreCase = true) -> R.drawable.snapchat
        imageKey.equals(IconMapping.YouTube, ignoreCase = true) -> R.drawable.youtube
        imageKey.equals(IconMapping.vkonakte, ignoreCase = true) -> R.drawable.vk
        imageKey.equals(IconMapping.telegramUsing, ignoreCase = true) -> R.drawable.telegram
        imageKey.equals(IconMapping.Instagram, ignoreCase = true) -> R.drawable.instagram
        imageKey.equals(IconMapping.exchange, ignoreCase = true) -> R.drawable.exchangeforcard
        imageKey.equals(IconMapping.detailsIcon, ignoreCase = true) -> R.drawable.notes
        imageKey.equals(IconMapping.flag, ignoreCase = true) -> R.drawable.flag
        imageKey.equals(IconMapping.Socialpack, ignoreCase = true) -> R.drawable.socialpack
        else -> R.drawable.ic_empty_icon
    }//when ends
}//getCardAdapterMappedImage ends