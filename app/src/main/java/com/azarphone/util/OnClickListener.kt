package com.azarphone.util

interface OnClickListener {
    fun onClick()
}