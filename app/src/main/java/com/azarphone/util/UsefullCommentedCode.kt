package com.azarphone.util

/** set the spannable string with image at the end*//*
        offeringNameDisplay=offeringNameDisplay.plus("  ")
        val ss = SpannableString(offeringNameDisplay)
        val d = ContextCompat.getDrawable(context!!, R.drawable.ic_group)
        d!!.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight())
        val span = ImageSpan(d, ImageSpan.ALIGN_BASELINE)
        ss.setSpan(span, offeringNameDisplay.indexOf(offeringNameDisplay.substring(offeringNameDisplay.length - 1)), offeringNameDisplay.length, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
        ss.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {
                logE("clickX", "Clicked the image", fromClass, "OfferingNameImageClicked")
            }
        }, offeringNameDisplay.indexOf(offeringNameDisplay.substring(offeringNameDisplay.length - 1)),offeringNameDisplay.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

        externalView.packageNameTV.movementMethod= LinkMovementMethod.getInstance()*/

/**read the json from the assets*/
//private fun loadJSONFromAsset(): String? {
//    return application.assets.open("menus.json").bufferedReader().use {
//        it.readText()
//    }//return ends
//}//loadJSONFromAsset ends




/**when redirecting from the side menu
 * the bottom navigation should show all icons as unselected and their color should also be unselected gray color
 * except that we are redirecting from side menu
 * suppose user goes to notification screen
 * so there should not be any menu item selected in the botton navigation view
 * juni1289*/
/*
private fun resetBottomMenuIcons() {
    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD) != null) {
        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD)
                .setIcon(R.drawable.dashboardunselectedicon)
                .title = getMenuTextColoredString(resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD).title.toString())
    }

    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS) != null) {
        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS)
                .setIcon(getTariffsUnSelectedIcon())
                .title = getMenuTextColoredString(resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS).title.toString())
    }

    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES) != null) {
        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES)
                .setIcon(getServicesUnSelectedIcon())
                .title = getMenuTextColoredString(resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES).title.toString())
    }

    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP) != null) {
        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP)
                .setIcon(R.drawable.topupunselectedicon)
                .title = getMenuTextColoredString(resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP).title.toString())
    }

    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT) != null) {
        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT)
                .setIcon(R.drawable.accountunselectedicon)
                .title = getMenuTextColoredString(resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT).title.toString())
    }
}//resetBottomMenuIcons ends*/
