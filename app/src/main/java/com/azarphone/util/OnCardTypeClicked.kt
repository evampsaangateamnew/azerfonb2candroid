package com.azarphone.util

interface OnCardTypeClicked {
    fun onCardClicked(cardKey: Long)
}