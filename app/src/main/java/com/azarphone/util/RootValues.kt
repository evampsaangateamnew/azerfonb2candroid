package com.azarphone.util

import android.content.Context
import android.graphics.Typeface
import com.azarphone.api.pojo.response.topup.savedcards.SavedCardsResponse
import com.azarphone.eventhandler.*

/**
 * @author Junaid Hassan on 14, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
object RootValues {
    private var supplementaryOffersAdapterUpdateEvents: SupplementaryOffersAdapterUpdateEvents? = null
    private var mySubscriptionsAdapterUpdateEvents: MySubscriptionsAdapterUpdateEvents? = null
    private var fnFEditButtonEvents: FnFEditButtonEvents? = null
    private var tariffCardsEvents: TariffCardsEvents? = null
    private var supplementaryOffersCardAdapterEvents: SupplementaryOffersCardAdapterEvents? = null
    private var coreServiceAdapterUpdaterEvents: CoreServiceAdapterUpdaterEvents? = null
    private var dataWithOutPackAdapterUpdaterEvents: CoreServiceAdapterUpdaterEvents? = null
    private var signupActivityToolTipEvents: SignupActivityToolTipEvents? = null
    private var verticalMenuClickListener: VerticalMenuClickEvents? = null
    private var azerFonActionBarEvents: AzerFonActionBarEvents? = null
    private var forgotPasswordActivityToolTipEvents: ForgotPasswordActivityToolTipEvents? = null
    private var OFFERS_LIST_ORIENTATION = androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL
    private var offerFilterListenerCall: OfferFilterListener? = null
    private var offerFilterListenerInternet: OfferFilterListener? = null
    private var offerFilterListenerSms: OfferFilterListener? = null
    private var offersSearchResultListener: OffersSearchResultListener? = null
    private var offerFilterListenerCampaign: OfferFilterListener? = null
    private var offerFilterListenerTM: OfferFilterListener? = null
    private var offerFilterListenerHybrid: OfferFilterListener? = null
    private var offerFilterListenerInternetInclusive: OfferFilterListener? = null
    private var offerFilterListenerVoiceInclusive: OfferFilterListener? = null
    private var offerFilterListenerSmsInclusive: OfferFilterListener? = null
    private var supplementaryOffersFilterEvents: SupplementaryOffersFilterEvents? = null
    private var CLASS_NAME_M = ""
    private var CLASS_NAME_SE = ""
    private var fCMEvents: FCMEvents? = null
    private var supplementaryOffersSubscriptionEvents: SupplementaryOffersSubscriptionEvents? = null
    private var internetOffersSubscriptionResponseEvents: InternetOffersSubscriptionResponseEvents? =
        null
    private var internetOffersResponseEvents: InternetOffersResponseEvents? = null
    private var tariffRedirectionFromDashboardEvents: TariffRedirectionFromDashboardEvents? = null
    private var fingerPrintScannerToLoginCallBack: BioMetricAuthenticationEvents? = null
    private var tariffsSubscriptionsEvents: TariffsSubscriptionsEvents? = null
    private var mySubscriptionEvents: MySubscriptionEvents? = null
    private var mySubscriptionAdapterEvents: MySubscriptionAdapterEvents? = null
    private var actionBarSearchEvents: ActionBarSearchEvents? = null
    private var hashMapOfTariffListeners = HashMap<String, TariffsSearchPerformEvents>()
    private var homePageResponseEvents: HomePageResponseEvents? = null
    private var tariffsResponseEvents: TariffsResponseEvents? = null
    private var supplementaryOffersRoamingCardAdapterEvents: SupplementaryOffersRoamingCardAdapterEvents? =
        null
    private var supplementarySpecialOffersCardAdapterEvents: SupplementarySpecialOffersCardAdapterEvents? = null
    private var notificationsCloserEvents: NotificationsCloserEvents? = null
    private var signUpEvents: SignUpEvents? = null
    private var alsBoldFont: Typeface? = null
    private var alsNormalFont: Typeface? = null
    private var manatFont: Typeface? = null
    private var rateUsAndroidOfflineEvents: RateUsAndroidOfflineEvents? = null
    private var savedCardsResponse: SavedCardsResponse? = null
    private var paymentSchedulerClickListener: PaymentSchedulerClickListener? = null
    private var callingSurveyFromDialog: CallingSurveyFromDialog? = null

    fun setCallingSurveyFromDialog(callingSurveyFromDialog: CallingSurveyFromDialog) {
        this.callingSurveyFromDialog = callingSurveyFromDialog
    }

    fun getCallingSurveyFromDialog(): CallingSurveyFromDialog? {
        return callingSurveyFromDialog
    }

    fun setRateUsAndroidOfflineEvents(rateUsAndroidOfflineEvents: RateUsAndroidOfflineEvents) {
        this.rateUsAndroidOfflineEvents = rateUsAndroidOfflineEvents
    }

    fun getRateUsAndroidOfflineEvents(): RateUsAndroidOfflineEvents {
        return rateUsAndroidOfflineEvents!!
    }

    fun setALSNormalFont(context: Context) {
        alsNormalFont = Typeface.createFromAsset(context.assets, "fonts/af/alsschlangesans.otf")
    }

    fun setALSBoldFont(context: Context) {
        alsBoldFont = Typeface.createFromAsset(context.assets, "fonts/af/als_schlange_sans_bold.otf")
    }

    fun setAzeriSymbolFont(context: Context) {
        manatFont = Typeface.createFromAsset(context.assets, "fonts/af/manatfb.otf")
    }

    fun getAzeriSymbolFont(): Typeface {
        return manatFont!!
    }

    fun getALSBoldFont(): Typeface {
        return alsBoldFont!!
    }

    fun getALSNormalFont(): Typeface {
        return alsNormalFont!!
    }

    fun setSignUpEvents(signUpEvents: SignUpEvents) {
        this.signUpEvents = signUpEvents
    }

    fun getSignUpEvents(): SignUpEvents? {
        return signUpEvents
    }

    fun setNotificationsCloserEvents(notificationsCloserEvents: NotificationsCloserEvents) {
        this.notificationsCloserEvents = notificationsCloserEvents
    }

    fun getNotificationsCloserEvents(): NotificationsCloserEvents? {
        return notificationsCloserEvents
    }

    fun setSupplementarySpecialOffersCardAdapterEvents(supplementarySpecialOffersCardAdapterEvents: SupplementarySpecialOffersCardAdapterEvents) {
        this.supplementarySpecialOffersCardAdapterEvents = supplementarySpecialOffersCardAdapterEvents
    }

    fun getSupplementarySpecialOffersCardAdapterEvents(): SupplementarySpecialOffersCardAdapterEvents {
        return supplementarySpecialOffersCardAdapterEvents!!
    }

    fun setSupplementaryOffersRoamingCardAdapterEvents(supplementaryOffersRoamingCardAdapterEvents: SupplementaryOffersRoamingCardAdapterEvents) {
        this.supplementaryOffersRoamingCardAdapterEvents = supplementaryOffersRoamingCardAdapterEvents
    }

    fun getSupplementaryOffersRoamingCardAdapterEvents(): SupplementaryOffersRoamingCardAdapterEvents {
        return supplementaryOffersRoamingCardAdapterEvents!!
    }

    fun setTariffsResponseEvents(tariffsResponseEvents: TariffsResponseEvents) {
        this.tariffsResponseEvents = tariffsResponseEvents
    }

    fun getTariffsResponseEvents(): TariffsResponseEvents {
        return tariffsResponseEvents!!
    }

    fun setHomePageResponseEvents(homePageResponseEvents: HomePageResponseEvents) {
        this.homePageResponseEvents = homePageResponseEvents
    }

    fun getHomePageResponseEvents(): HomePageResponseEvents? {
        return homePageResponseEvents
    }

    fun setSupplementaryOffersAdapterUpdateEvents(supplementaryOffersAdapterUpdateEvents: SupplementaryOffersAdapterUpdateEvents) {
        this.supplementaryOffersAdapterUpdateEvents = supplementaryOffersAdapterUpdateEvents
    }

    fun getSupplementaryOffersAdapterUpdateEvents(): SupplementaryOffersAdapterUpdateEvents? {
        return supplementaryOffersAdapterUpdateEvents
    }

    fun setMySubscriptionsAdapterUpdateEvents(mySubscriptionsAdapterUpdateEvents: MySubscriptionsAdapterUpdateEvents) {
        this.mySubscriptionsAdapterUpdateEvents = mySubscriptionsAdapterUpdateEvents
    }

    fun getMySubscriptionsAdapterUpdateEvents(): MySubscriptionsAdapterUpdateEvents? {
        return mySubscriptionsAdapterUpdateEvents
    }

    fun getHashMapOfTariffListeners(): HashMap<String, TariffsSearchPerformEvents> {
        return hashMapOfTariffListeners
    }

    fun setActionBarSearchEvents(actionBarSearchEvents: ActionBarSearchEvents) {
        this.actionBarSearchEvents = actionBarSearchEvents
    }

    fun getActionBarSearchEvents(): ActionBarSearchEvents? {
        return actionBarSearchEvents
    }

    fun setFnFEditButtonEvents(fnFEditButtonEvents: FnFEditButtonEvents) {
        this.fnFEditButtonEvents = fnFEditButtonEvents
    }

    fun getFnFEditButtonEvents(): FnFEditButtonEvents {
        return fnFEditButtonEvents!!
    }

    fun getTariffCardsEvents(): TariffCardsEvents? {
        return tariffCardsEvents
    }

    fun setTariffCardsEvents(tariffCardsEvents: TariffCardsEvents) {
        this.tariffCardsEvents = tariffCardsEvents
    }

    fun setMySubscriptionAdapterEvents(mySubscriptionAdapterEvents: MySubscriptionAdapterEvents) {
        this.mySubscriptionAdapterEvents = mySubscriptionAdapterEvents
    }

    fun getMySubscriptionAdapterEvents(): MySubscriptionAdapterEvents {
        return mySubscriptionAdapterEvents!!
    }

    fun setMySubscriptionEvents(mySubscriptionEvents: MySubscriptionEvents) {
        this.mySubscriptionEvents = mySubscriptionEvents
    }

    fun getMySubscriptionEvents(): MySubscriptionEvents {
        return mySubscriptionEvents!!
    }

    fun setTariffsSubscriptionsEvents(tariffsSubscriptionsEvents: TariffsSubscriptionsEvents) {
        this.tariffsSubscriptionsEvents = tariffsSubscriptionsEvents
    }

    fun getTariffsSubscriptionsEvents(): TariffsSubscriptionsEvents {
        return tariffsSubscriptionsEvents!!
    }

    fun setFingerPrintScannerToLoginCallBack(fingerPrintScannerToLoginCallBack: BioMetricAuthenticationEvents) {
        this.fingerPrintScannerToLoginCallBack = fingerPrintScannerToLoginCallBack
    }

    fun getFingerPrintScannerToLoginCallBack(): BioMetricAuthenticationEvents {
        return fingerPrintScannerToLoginCallBack!!
    }

    fun setTariffRedirectionFromDashboardEvents(tariffRedirectionFromDashboardEvents: TariffRedirectionFromDashboardEvents) {
        this.tariffRedirectionFromDashboardEvents = tariffRedirectionFromDashboardEvents
    }

    fun getTariffRedirectionFromDashboardEvents(): TariffRedirectionFromDashboardEvents {
        return tariffRedirectionFromDashboardEvents!!
    }

    fun getSupplementaryOffersSubscriptionEvents(): SupplementaryOffersSubscriptionEvents? {
        return supplementaryOffersSubscriptionEvents
    }

    fun setSupplementaryOffersSubscriptionEvents(supplementaryOffersSubscriptionEvents: SupplementaryOffersSubscriptionEvents?) {
        this.supplementaryOffersSubscriptionEvents = supplementaryOffersSubscriptionEvents
    }

    fun getInternetOffersSubscriptionEvents(): SupplementaryOffersSubscriptionEvents? {
        return supplementaryOffersSubscriptionEvents
    }

    fun setInternetOffersSubscriptionResponseEvents(supplementaryOffersSubscriptionEvents: SupplementaryOffersSubscriptionEvents?) {
        this.supplementaryOffersSubscriptionEvents = supplementaryOffersSubscriptionEvents
    }

    fun getInternetOfferSubscriptionResponseEvents(): InternetOffersSubscriptionResponseEvents? {
        return internetOffersSubscriptionResponseEvents
    }

    fun setInternetOffersSubscriptionResponseEvents(internetOffersSubscriptionResponseEvents: InternetOffersSubscriptionResponseEvents?) {
        this.internetOffersSubscriptionResponseEvents = internetOffersSubscriptionResponseEvents
    }

    fun getInternetOffersResponseEvents(): InternetOffersResponseEvents? {
        return internetOffersResponseEvents
    }

    fun setInternetOffersResponseEvents(internetOffersResponseEvents: InternetOffersResponseEvents?) {
        this.internetOffersResponseEvents = internetOffersResponseEvents
    }

    fun getFCMEvents(): FCMEvents? {
        return fCMEvents
    }

    fun setFCMEvents(fCMEvents: FCMEvents?) {
        this.fCMEvents = fCMEvents
    }

    fun getSupplementaryOffersCardAdapterEvents(): SupplementaryOffersCardAdapterEvents {
        return supplementaryOffersCardAdapterEvents!!
    }

    fun setSupplementaryOffersCardAdapterEvents(supplementaryOffersCardAdapterEvents: SupplementaryOffersCardAdapterEvents) {
        this.supplementaryOffersCardAdapterEvents = supplementaryOffersCardAdapterEvents
    }

    fun getInternetOffersCardAdapterEvents(): SupplementaryOffersCardAdapterEvents {
        return supplementaryOffersCardAdapterEvents!!
    }

    fun setInternetOffersCardAdapterEvents(supplementaryOffersCardAdapterEvents: SupplementaryOffersCardAdapterEvents) {
        this.supplementaryOffersCardAdapterEvents = supplementaryOffersCardAdapterEvents
    }

    fun getCLASS_NAME_M(): String {
        return CLASS_NAME_M
    }

    fun setCLASS_NAME_M(CLASS_NAME_M: String) {
        this.CLASS_NAME_M = CLASS_NAME_M
    }

    fun getCLASS_NAME_SE(): String {
        return CLASS_NAME_SE
    }

    fun setCLASS_NAME_SE(CLASS_NAME_SE: String) {
        this.CLASS_NAME_SE = CLASS_NAME_SE
    }

    fun setCoreServiceAdapterUpdaterEvents(coreServiceAdapterUpdaterEvents: CoreServiceAdapterUpdaterEvents) {
        this.coreServiceAdapterUpdaterEvents = coreServiceAdapterUpdaterEvents
    }

    fun getCoreServiceAdapterUpdaterEvents(): CoreServiceAdapterUpdaterEvents {
        return coreServiceAdapterUpdaterEvents!!
    }

    fun dataWithOutPackAdapterUpdaterEvents(coreServiceAdapterUpdaterEvents: CoreServiceAdapterUpdaterEvents) {
        this.dataWithOutPackAdapterUpdaterEvents = coreServiceAdapterUpdaterEvents
    }

    fun getDataWithOutPackAdapterUpdaterEvents(): CoreServiceAdapterUpdaterEvents {
        return dataWithOutPackAdapterUpdaterEvents!!
    }

    fun setSupplementaryOffersFilterEvents(supplementaryOffersFilterEvents: SupplementaryOffersFilterEvents) {
        this.supplementaryOffersFilterEvents = supplementaryOffersFilterEvents
    }

    fun getSupplementaryOffersFilterEvents(): SupplementaryOffersFilterEvents {
        return supplementaryOffersFilterEvents!!
    }

    fun setOfferFilterListenerSmsInclusive(offerFilterListenerSmsInclusive: OfferFilterListener) {
        this.offerFilterListenerSmsInclusive = offerFilterListenerSmsInclusive
    }

    fun getOfferFilterListenerSmsInclusive(): OfferFilterListener {
        return offerFilterListenerSmsInclusive!!
    }

    fun setOfferFilterListenerVoiceInclusive(offerFilterListenerVoiceInclusive: OfferFilterListener) {
        this.offerFilterListenerVoiceInclusive = offerFilterListenerVoiceInclusive
    }

    fun getOfferFilterListenerVoiceInclusive(): OfferFilterListener {
        return offerFilterListenerVoiceInclusive!!
    }

    fun setOfferFilterListenerInternetInclusive(offerFilterListenerInternetInclusive: OfferFilterListener) {
        this.offerFilterListenerInternetInclusive = offerFilterListenerInternetInclusive
    }

    fun getOfferFilterListenerInternetInclusive(): OfferFilterListener? {
        return offerFilterListenerInternetInclusive
    }

    fun setOfferFilterListenerHybrid(offerFilterListenerHybrid: OfferFilterListener) {
        this.offerFilterListenerHybrid = offerFilterListenerHybrid
    }

    fun getOfferFilterListenerHybrid(): OfferFilterListener {
        return offerFilterListenerHybrid!!
    }

    fun setOfferFilterListenerTM(offerFilterListenerTM: OfferFilterListener) {
        this.offerFilterListenerTM = offerFilterListenerTM
    }

    fun getOfferFilterListenerTM(): OfferFilterListener {
        return offerFilterListenerTM!!
    }

    fun setOfferFilterListenerCampaign(offerFilterListenerCampaign: OfferFilterListener) {
        this.offerFilterListenerCampaign = offerFilterListenerCampaign
    }

    fun getOfferFilterListenerCampaign(): OfferFilterListener {
        return offerFilterListenerCampaign!!
    }

    fun setOfferFilterListenerInternet(offerFilterListenerInternet: OfferFilterListener) {
        this.offerFilterListenerInternet = offerFilterListenerInternet
    }

    fun getOfferFilterListenerInternet(): OfferFilterListener {
        return offerFilterListenerInternet!!
    }

    fun setOffersSearchResultListener(offersSearchResultListener: OffersSearchResultListener) {
        this.offersSearchResultListener = offersSearchResultListener
    }

    fun getOffersSearchResultListener(): OffersSearchResultListener {
        return offersSearchResultListener!!
    }

    fun setOfferFilterListenerCall(offerFilterListenerCall: OfferFilterListener) {
        this.offerFilterListenerCall = offerFilterListenerCall
    }

    fun getOfferFilterListenerCall(): OfferFilterListener? {
        return offerFilterListenerCall
    }

    fun setOffersListOrientation(offersListOrientation: Int) {
        OFFERS_LIST_ORIENTATION = offersListOrientation
    }

    fun getOffersListOrientation(): Int {
        return OFFERS_LIST_ORIENTATION
    }

    fun setOfferFilterListenerSms(offerFilterListenerSms: OfferFilterListener) {
        this.offerFilterListenerSms = offerFilterListenerSms
    }

    fun getOfferFilterListenerSms(): OfferFilterListener? {
        return offerFilterListenerSms
    }

    fun setForgotPasswordActivityToolTipEvents(forgotPasswordActivityToolTipEvents: ForgotPasswordActivityToolTipEvents) {
        this.forgotPasswordActivityToolTipEvents = forgotPasswordActivityToolTipEvents
    }

    fun getForgotPasswordActivityToolTipEvents(): ForgotPasswordActivityToolTipEvents {
        return forgotPasswordActivityToolTipEvents!!
    }

    fun setAzerFonActionBarEvents(azerFonActionBarEvents: AzerFonActionBarEvents?) {
        this.azerFonActionBarEvents = azerFonActionBarEvents
    }

    fun getAzerFonActionBarEvents(): AzerFonActionBarEvents? {
        return azerFonActionBarEvents
    }

    fun setSignupActivityToolTipEvents(signupActivityToolTipEvents: SignupActivityToolTipEvents) {
        this.signupActivityToolTipEvents = signupActivityToolTipEvents
    }

    fun getSignupActivityToolTipEvents(): SignupActivityToolTipEvents {
        return signupActivityToolTipEvents!!
    }

    fun setVerticalMenuClickListener(verticalMenuClickListener: VerticalMenuClickEvents) {
        this.verticalMenuClickListener = verticalMenuClickListener
    }

    fun getVerticalMenuClickListener(): VerticalMenuClickEvents {
        return verticalMenuClickListener!!
    }

    fun setSavedCardsResponse(savedCardsResponse: SavedCardsResponse?) {
        this.savedCardsResponse = savedCardsResponse
    }

    fun getSavedCardsResponse(): SavedCardsResponse? {
        return savedCardsResponse
    }

    fun setPymentSchedlerClickListener(paymentSchedulerClickListener: PaymentSchedulerClickListener) {
        this.paymentSchedulerClickListener = paymentSchedulerClickListener
    }

    fun getPymentSchedlerClickListener(): PaymentSchedulerClickListener? {
        return paymentSchedulerClickListener
    }
}