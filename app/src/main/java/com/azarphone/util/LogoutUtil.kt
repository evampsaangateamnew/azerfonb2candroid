package com.azarphone.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.azarphone.ProjectApplication
import com.azarphone.api.pojo.helperpojo.UserAccounts
import com.azarphone.api.pojo.response.customerdata.CustomerData
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.ui.activities.loginactivity.LoginActivity
import com.azarphone.ui.activities.manageaccount.ManageAccountActivity
import com.azarphone.validators.hasValue

/**
 * @author Junaid Hassan on 19, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
inline fun logOut(context: Context, activity: Activity, isLogOutWithAPI: Boolean, fromClass: String, fromMethod: String) {
    if (activity != null && !activity.isFinishing) {
        removeCurrentUser(context, activity)

        logE("logXOutY1289", "LOGGED OUT", fromClass, fromMethod)
    } else {
        return
    }
}//logOut ends

fun removeCurrentUser(context: Context, activity: Activity) {

    var currentUserMsisdn = ""
    if (UserDataManager.getCustomerData() != null && hasValue(UserDataManager.getCustomerData()!!.msisdn)) {
        currentUserMsisdn = UserDataManager.getCustomerData()!!.msisdn!!
    } else {
        if (getCustomerDataFromLocalPreferences().msisdn != null && hasValue(getCustomerDataFromLocalPreferences().msisdn)) {
            currentUserMsisdn = getCustomerDataFromLocalPreferences().msisdn!!
        }
    }//checks for msisdn not to be null or empty

    var list: ArrayList<CustomerData>? = ArrayList()
    if (getUserAccountsDataFromLocalPreferences() != null && getUserAccountsDataFromLocalPreferences().usersList != null) {
        list = getUserAccountsDataFromLocalPreferences().usersList as ArrayList<CustomerData>
    }

    if (list!!.size > 1) {
        logE("listXSize", "size:::".plus(list.size), "LogoutUtil", "subscribe")
        val newList: ArrayList<CustomerData> = ArrayList()
        for (customer in list) {
            if (customer.msisdn == currentUserMsisdn) {
                //skip this data
            } else {
                newList.add(customer)
            }
        }

        /**update the list*/
        ProjectApplication.getInstance().getLocalPreferneces().setResponse(
                ProjectApplication.getInstance().getLocalPreferneces().KEY_USER_ACCOUNTS_DATA,
                UserAccounts(newList))

        /**add the change password flag in preferences*/
        ProjectApplication.getInstance().getLocalPreferneces().addBool(
                ProjectApplication.getInstance().getLocalPreferneces().IS_PASSWORD_CHANGE_MULTI_USER_LOGGED_OUT,
                true)

        /**goto manage accounts activity*/
        val intent = Intent(context, ManageAccountActivity::class.java)
        intent.addFlags(
                Intent.FLAG_ACTIVITY_NEW_TASK
                        or
                        Intent.FLAG_ACTIVITY_CLEAR_TASK
                        or
                        Intent.FLAG_ACTIVITY_CLEAR_TOP)
        activity.startActivity(intent)
        activity.finish()
    } else {
        logE("listXSize", "size:::0".plus(list.size), "LogoutUtil", "subscribe")
        logE("logout", "logging out", "LogoutUtil", "removeCurrentUser")
        //remove the shared preferences
        ProjectApplication.getInstance().getLocalPreferneces().deleteAllPreferences()
//    resetWrongPasswordAttempts(context)

        //reset the user data manager
        UserDataManager.resetUserDataManager()

        //start from the new process
        //show the user the login activity
        LoginActivity.start(context)
        //finish the previous activity
        activity.finish()
    }
}