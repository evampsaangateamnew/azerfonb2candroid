package com.azarphone.util

import com.azarphone.R
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.validators.hasValue


/**
 * @author Junaid Hassan on 03, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */

inline fun getDashboardSelectedIcon(): Int {
    return R.drawable.dashboardselectedicon
}

inline fun getDashboardUnSelectedIcon(): Int {
    return R.drawable.dashboardunselectedicon
}

inline fun getTopUpSelectedIcon(): Int {
    return R.drawable.topupselectedicon
}

inline fun getTopUpUnSelectedIcon(): Int {
    return R.drawable.topupunselectedicon
}

inline fun getInternetSelectedIcon(): Int {
    return R.drawable.interent_selected_icon
}

inline fun getInternetUnSelectedIcon(): Int {
    return R.drawable.internet_un_selected_icon
}

inline fun getMyAccountSelectedIcon(): Int {
    return R.drawable.accountselectedicon
}

inline fun getMyAccountUnSelectedIcon(): Int {
    return R.drawable.accountunselectedicon
}


inline fun getTariffsSelectedIcon(): Int {
    var tariffSelectedIcon = R.drawable.tariffselectedicon

    if (UserDataManager.getCustomerData() != null) {
        if (UserDataManager.getCustomerData()!!.specialOffersTariffData != null) {
            if (UserDataManager.getCustomerData()!!.specialOffersTariffData!!.specialTariffMenu != null &&
                hasValue(UserDataManager.getCustomerData()!!.specialOffersTariffData!!.specialTariffMenu)
            ) {
                if (UserDataManager.getCustomerData()!!.specialOffersTariffData!!.specialTariffMenu!!.equals(
                        "show",
                        ignoreCase = true
                    )
                ) {
                    tariffSelectedIcon = R.drawable.tariffs_special_selected_icon
                }
            }
        }
    }

    return tariffSelectedIcon
}

inline fun getTariffsUnSelectedIcon(): Int {
    var tariffUnSelectedIcon = R.drawable.tariffunselectedicon

    if (UserDataManager.getCustomerData() != null) {
        if (UserDataManager.getCustomerData()!!.specialOffersTariffData != null) {
            if (UserDataManager.getCustomerData()!!.specialOffersTariffData!!.specialTariffMenu != null &&
                    hasValue(UserDataManager.getCustomerData()!!.specialOffersTariffData!!.specialTariffMenu)) {
                if (UserDataManager.getCustomerData()!!.specialOffersTariffData!!.specialTariffMenu!!.equals("show", ignoreCase = true)) {
                    tariffUnSelectedIcon = R.drawable.tariffs_special_unselected_icon
                }
            }
        }
    }

    return tariffUnSelectedIcon
}

inline fun getServicesSelectedIcon(): Int {
    var servicesSelectedIcon = R.drawable.serviceselectedicon

    if (UserDataManager.getCustomerData() != null) {
        if (UserDataManager.getCustomerData()!!.specialOffersTariffData != null) {
            if (UserDataManager.getCustomerData()!!.specialOffersTariffData!!.specialOffersMenu != null &&
                    hasValue(UserDataManager.getCustomerData()!!.specialOffersTariffData!!.specialOffersMenu)) {
                if (UserDataManager.getCustomerData()!!.specialOffersTariffData!!.specialOffersMenu!!.equals("show", ignoreCase = true)) {
                    servicesSelectedIcon = R.drawable.services_special_selected_icon
                }
            }
        }
    }

    return servicesSelectedIcon
}

inline fun getServicesUnSelectedIcon(): Int {
    var servicesUnSelectedIcon = R.drawable.servicesunselectedicon

    if (UserDataManager.getCustomerData() != null) {
        if (UserDataManager.getCustomerData()!!.specialOffersTariffData != null) {
            if (UserDataManager.getCustomerData()!!.specialOffersTariffData!!.specialOffersMenu != null &&
                    hasValue(UserDataManager.getCustomerData()!!.specialOffersTariffData!!.specialOffersMenu)) {
                if (UserDataManager.getCustomerData()!!.specialOffersTariffData!!.specialOffersMenu!!.equals("show", ignoreCase = true)) {
                    servicesUnSelectedIcon = R.drawable.services_special_unselected_icon
                }
            }
        }
    }

    return servicesUnSelectedIcon
}

inline fun isShowSpecialTabInTarrifs(): Boolean {
    var show = false

    if (UserDataManager.getCustomerData() != null) {
        if (UserDataManager.getCustomerData()!!.specialOffersTariffData != null) {
            if (UserDataManager.getCustomerData()!!.specialOffersTariffData!!.specialTariffMenu != null &&
                    hasValue(UserDataManager.getCustomerData()!!.specialOffersTariffData!!.specialTariffMenu)) {
                if (UserDataManager.getCustomerData()!!.specialOffersTariffData!!.specialTariffMenu!!.equals("show", ignoreCase = true)) {
                    show = true
                }
            }
        }
    }

    return show
}

inline fun isShowSpecialMenuInServicesMenu(): Boolean {
    var show = false

    if (UserDataManager.getCustomerData() != null) {
        if (UserDataManager.getCustomerData()!!.specialOffersTariffData != null) {
            if (UserDataManager.getCustomerData()!!.specialOffersTariffData!!.specialOffersMenu != null &&
                    hasValue(UserDataManager.getCustomerData()!!.specialOffersTariffData!!.specialOffersMenu)) {
                if (UserDataManager.getCustomerData()!!.specialOffersTariffData!!.specialOffersMenu!!.equals("show", ignoreCase = true)) {
                    show = true
                }
            }
        }
    }

    return show
}