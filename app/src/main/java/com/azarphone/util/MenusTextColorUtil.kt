package com.azarphone.util

import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan

/**
 * @author Junaid Hassan on 18, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
inline fun getMenuTextColoredString(color: Int, title: String): SpannableString {
    val titleSpannable = SpannableString(title)

    titleSpannable.setSpan(
            ForegroundColorSpan(color),
            0,
            title.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

    return titleSpannable
}