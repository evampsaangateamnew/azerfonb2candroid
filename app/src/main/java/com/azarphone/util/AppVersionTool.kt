package com.azarphone.util

import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager

/**
 * @author Junaid Hassan on 18, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
inline fun getAppVersion(context: Context): String {
    if (context == null) {
        return ""
    }
    var version = ""
    try {
        var pInfo: PackageInfo? = null
        try {
            pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
        } catch (e: PackageManager.NameNotFoundException) {
            logE("appVersion", "error:::".plus(e.message), "AppVersionTool", "getAppVersion1")
        }

        if (pInfo != null) {
            version = pInfo.versionName
        }
    } catch (e: Exception) {
        logE("appVersion", "error:::".plus(e.message), "AppVersionTool", "getAppVersion2")
    }

    return version
}