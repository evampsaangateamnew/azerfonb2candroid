package com.azarphone.util

import com.azarphone.ProjectApplication
import com.azarphone.api.EndPoints
import com.azarphone.api.pojo.helperpojo.UserAccounts
import com.azarphone.api.pojo.response.contactusresponse.ContactUsResponse
import com.azarphone.api.pojo.response.coreservices.CoreServicesResponse
import com.azarphone.api.pojo.response.customerdata.CustomerData
import com.azarphone.api.pojo.response.exchangeservicesvaluesresponse.ExchangeServicesValuesResponse
import com.azarphone.api.pojo.response.faqresponse.FAQResponse
import com.azarphone.api.pojo.response.fcmtoken.FCMResponse
import com.azarphone.api.pojo.response.gethomepageresponse.GetHomePageResponse
import com.azarphone.api.pojo.response.internetpackresponse.InternetPacksResponse
import com.azarphone.api.pojo.response.loginappresumeresponse.LoginAppResumeResponse
import com.azarphone.api.pojo.response.menuresponse.MenuResponse
import com.azarphone.api.pojo.response.mysubscriptions.response.MySubscriptionResponse
import com.azarphone.api.pojo.response.storelocator.StoreLocatorResponse
import com.azarphone.api.pojo.response.supplementaryoffers.response.SupplementaryResponse
import com.azarphone.api.pojo.response.tariffsresponse.TariffsResponse
import com.azarphone.validators.hasValue

/**
 * @author Junaid Hassan on 19, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
inline fun setStoreLocatorDataToLocalPreferences(coreServicesResponse: CoreServicesResponse) {
    val preferenceKey = EndPoints.GET_STORE_DETAILS
            .plus(ProjectApplication.mLocaleManager.getLanguage()
                    .plus(getCurrentlyLoggedInUserMsisdn()))

    ProjectApplication.getInstance().getLocalPreferneces().setResponse(
            preferenceKey,
            coreServicesResponse)

    logE("subSSups", "savedvas:::".plus(getStoreLocatorDataFromLocalPreferences()), "setStoreLocatorDataToLocalPreferences", "setStoreLocatorDataToLocalPreferences")

}

inline fun getStoreLocatorDataFromLocalPreferences(): CoreServicesResponse {
    val preferenceKey = EndPoints.GET_STORE_DETAILS
            .plus(ProjectApplication.mLocaleManager.getLanguage()
                    .plus(getCurrentlyLoggedInUserMsisdn()))

    val response = ProjectApplication.getInstance().getLocalPreferneces().getResponse(
            preferenceKey,
            CoreServicesResponse::class.java) as CoreServicesResponse

    logE("subSSups", "getfvas:::".plus(response), "getStoreLocatorDataFromLocalPreferences", "getStoreLocatorDataFromLocalPreferences")

    return response
}

inline fun setVASServicesDataToLocalPreferences(coreServicesResponse: CoreServicesResponse) {
    val preferenceKey = EndPoints.CORE_SERVICES_END_POINT
            .plus(ProjectApplication.mLocaleManager.getLanguage()
                    .plus(getCurrentlyLoggedInUserMsisdn()))

    ProjectApplication.getInstance().getLocalPreferneces().setResponse(
            preferenceKey,
            coreServicesResponse)

    logE("subSSups", "savedvas:::".plus(getVASServicesDataFromLocalPreferences()), "setVASServicesDataToLocalPreferences", "setVASServicesDataToLocalPreferences")

}

inline fun getVASServicesDataFromLocalPreferences(): CoreServicesResponse {
    val preferenceKey = EndPoints.CORE_SERVICES_END_POINT
            .plus(ProjectApplication.mLocaleManager.getLanguage()
                    .plus(getCurrentlyLoggedInUserMsisdn()))

    val response = ProjectApplication.getInstance().getLocalPreferneces().getResponse(
            preferenceKey,
            CoreServicesResponse::class.java) as CoreServicesResponse

    logE("subSSups", "getfvas:::".plus(response), "getVASServicesDataToLocalPreferences", "getVASServicesDataToLocalPreferences")

    return response
}

inline fun setFAQCachedDataToLocalPreferences(fAQResponse: FAQResponse) {
    val preferenceKey = EndPoints.FAQS_ENDPOINT
            .plus(ProjectApplication.mLocaleManager.getLanguage()
                    .plus(getCurrentlyLoggedInUserMsisdn()))

    ProjectApplication.getInstance().getLocalPreferneces().setResponse(
            preferenceKey,
            fAQResponse)

    logE("subSSups", "savedfaq:::".plus(getFAQCachedDataFromLocalPreferences()), "setFAQCachedDataToLocalPreferences", "setFAQCachedDataToLocalPreferences")

}

inline fun getFAQCachedDataFromLocalPreferences(): FAQResponse {
    val preferenceKey = EndPoints.FAQS_ENDPOINT
            .plus(ProjectApplication.mLocaleManager.getLanguage()
                    .plus(getCurrentlyLoggedInUserMsisdn()))

    val response = ProjectApplication.getInstance().getLocalPreferneces().getResponse(
            preferenceKey,
            FAQResponse::class.java) as FAQResponse

    logE("subSSups", "getfaq:::".plus(response), "getFAQCachedDataToLocalPreferences", "getFAQCachedDataToLocalPreferences")

    return response
}

inline fun setExchangeServiceCachedDataToLocalPreferences(exchangeServiceResponse: ExchangeServicesValuesResponse) {
    val preferenceKey = EndPoints.EXCHANGE_SERVICES_VALUES_END_POINT
            .plus(ProjectApplication.mLocaleManager.getLanguage()
                    .plus(getCurrentlyLoggedInUserMsisdn()))

    ProjectApplication.getInstance().getLocalPreferneces().setResponse(
            preferenceKey,
            exchangeServiceResponse)

    logE("subSSups", "savedExchangeService:::".plus(getFAQCachedDataFromLocalPreferences()), "setExchangeServiceCachedDataToLocalPreferences", "setExchangeServiceCachedDataToLocalPreferences")

}

inline fun getExchangeServiceCachedDataFromLocalPreferences(): ExchangeServicesValuesResponse {
    val preferenceKey = EndPoints.EXCHANGE_SERVICES_VALUES_END_POINT
            .plus(ProjectApplication.mLocaleManager.getLanguage()
                    .plus(getCurrentlyLoggedInUserMsisdn()))

    val response = ProjectApplication.getInstance().getLocalPreferneces().getResponse(
            preferenceKey,
            ExchangeServicesValuesResponse::class.java) as ExchangeServicesValuesResponse

    logE("subSSups", "getExchangeService:::".plus(response), "getExchangeServiceCachedDataToLocalPreferences", "getExchangeServiceCachedDataToLocalPreferences")

    return response
}

inline fun setStoreLocatorCachedDataToLocalPreferences(storeLocatorResponseResponse: StoreLocatorResponse) {
    val preferenceKey = EndPoints.GET_STORE_DETAILS
            .plus(ProjectApplication.mLocaleManager.getLanguage()
                    .plus(getCurrentlyLoggedInUserMsisdn()))

    ProjectApplication.getInstance().getLocalPreferneces().setResponse(
            preferenceKey,
            storeLocatorResponseResponse)

    logE("subSSups", "savedStoreLocator:::".plus(getFAQCachedDataFromLocalPreferences()), "setStoreLocatorCachedDataToLocalPreferences", "setStoreLocatorCachedDataToLocalPreferences")

}

inline fun getStoreLocatorCachedDataFromLocalPreferences(): StoreLocatorResponse {
    val preferenceKey = EndPoints.GET_STORE_DETAILS
            .plus(ProjectApplication.mLocaleManager.getLanguage()
                    .plus(getCurrentlyLoggedInUserMsisdn()))

    val response = ProjectApplication.getInstance().getLocalPreferneces().getResponse(
            preferenceKey,
            StoreLocatorResponse::class.java) as StoreLocatorResponse

    logE("subSSups", "getStoreLocator:::".plus(response), "getStoreLocatorCachedDataToLocalPreferences", "getStoreLocatorCachedDataToLocalPreferences")

    return response
}

inline fun setContactUsDataToLocalPreferences(contactUsResponse: ContactUsResponse) {
    val preferenceKey = EndPoints.CONTACT_US_DETAILS
            .plus(ProjectApplication.mLocaleManager.getLanguage()
                    .plus(getCurrentlyLoggedInUserMsisdn()))

    ProjectApplication.getInstance().getLocalPreferneces().setResponse(
            preferenceKey,
            contactUsResponse)

    logE("subSSups", "savedcon:::".plus(getContactUsFromLocalPreferences()), "setContactUsDataToLocalPreferences", "setContactUsDataToLocalPreferences")

}

inline fun getContactUsFromLocalPreferences(): ContactUsResponse {
    val preferenceKey = EndPoints.CONTACT_US_DETAILS
            .plus(ProjectApplication.mLocaleManager.getLanguage()
                    .plus(getCurrentlyLoggedInUserMsisdn()))

    val response = ProjectApplication.getInstance().getLocalPreferneces().getResponse(
            preferenceKey,
            ContactUsResponse::class.java) as ContactUsResponse

    logE("subSSups", "getSups:::".plus(response), "getContactUsFromLocalPreferences", "getContactUsFromLocalPreferences")

    return response
}

inline fun setSupplementaryOffersDataToLocalPreferences(supplementaryResponse: SupplementaryResponse) {
    val preferenceKey = EndPoints.SUPPLEMENTARY_OFFERS_END_POINT
            .plus(ProjectApplication.mLocaleManager.getLanguage()
                    .plus(getCurrentlyLoggedInUserMsisdn()))

    ProjectApplication.getInstance().getLocalPreferneces().setResponse(
            preferenceKey,
            supplementaryResponse)

    logE("subSSups", "savedsups:::".plus(getSupplementaryOffersFromLocalPreferences()), "setSupplementaryOffersDataToLocalPreferences", "setSupplementaryOffersDataToLocalPreferences")

}

inline fun getSupplementaryOffersFromLocalPreferences(): SupplementaryResponse {
    val preferenceKey = EndPoints.SUPPLEMENTARY_OFFERS_END_POINT
            .plus(ProjectApplication.mLocaleManager.getLanguage()
                    .plus(getCurrentlyLoggedInUserMsisdn()))

    val response = ProjectApplication.getInstance().getLocalPreferneces().getResponse(
        preferenceKey,
        SupplementaryResponse::class.java
    ) as SupplementaryResponse

    logE(
        "subSSups",
        "getSups:::".plus(response),
        "getSupplementaryOffersFromLocalPreferences",
        "getSupplementaryOffersFromLocalPreferences"
    )

    return response
}

inline fun getInternetOffersFromLocalPreferences(): InternetPacksResponse {
    val preferenceKey = EndPoints.INTERNET_OFFERS_END_POINT
        .plus(
            ProjectApplication.mLocaleManager.getLanguage()
                .plus(getCurrentlyLoggedInUserMsisdn())
        )

    val response = ProjectApplication.getInstance().getLocalPreferneces().getResponse(
        preferenceKey,
        InternetPacksResponse::class.java
    ) as InternetPacksResponse

    logE(
        "subSSups",
        "getSups:::".plus(response),
        "getSupplementaryOffersFromLocalPreferences",
        "getSupplementaryOffersFromLocalPreferences"
    )

    return response
}

inline fun setInternetOffersDataToLocalPreferences(internetPacksResponse: InternetPacksResponse) {
    val preferenceKey = EndPoints.INTERNET_OFFERS_END_POINT
        .plus(
            ProjectApplication.mLocaleManager.getLanguage()
                .plus(getCurrentlyLoggedInUserMsisdn())
        )

    ProjectApplication.getInstance().getLocalPreferneces().setResponse(
        preferenceKey,
        internetPacksResponse
    )

    logE(
        "subSSups",
        "savedsups:::".plus(getInternetOffersDataToLocalPreferences()),
        "setSupplementaryOffersDataToLocalPreferences",
        "setSupplementaryOffersDataToLocalPreferences"
    )

}

inline fun getInternetOffersDataToLocalPreferences(): InternetPacksResponse {
    val preferenceKey = EndPoints.INTERNET_OFFERS_END_POINT
        .plus(
            ProjectApplication.mLocaleManager.getLanguage()
                .plus(getCurrentlyLoggedInUserMsisdn())
        )

    val response = ProjectApplication.getInstance().getLocalPreferneces().getResponse(
        preferenceKey,
        InternetPacksResponse::class.java
    ) as InternetPacksResponse

    logE(
        "subSSups",
        "getSups:::".plus(response),
        "getSupplementaryOffersFromLocalPreferences",
        "getSupplementaryOffersFromLocalPreferences"
    )

    return response
}

inline fun setMySubscriptionsDataToLocalPreferences(mySubscriptionResponse: MySubscriptionResponse) {
    val preferenceKey = EndPoints.MY_SUBSCRIPTIONS_END_POINT
        .plus(
            ProjectApplication.mLocaleManager.getLanguage()
                .plus(getCurrentlyLoggedInUserMsisdn())
        )

    ProjectApplication.getInstance().getLocalPreferneces().setResponse(
        preferenceKey,
        mySubscriptionResponse
    )

    logE("subSSaved", "savedSubs:::".plus(getMySubscriptionsFromLocalPreferences()), "setMySubscriptionsDataToLocalPreferences", "setMySubscriptionsDataToLocalPreferences")

}

inline fun getMySubscriptionsFromLocalPreferences(): MySubscriptionResponse {
    val preferenceKey = EndPoints.MY_SUBSCRIPTIONS_END_POINT
            .plus(ProjectApplication.mLocaleManager.getLanguage()
                    .plus(getCurrentlyLoggedInUserMsisdn()))

    val response = ProjectApplication.getInstance().getLocalPreferneces().getResponse(
            preferenceKey,
            MySubscriptionResponse::class.java) as MySubscriptionResponse

    logE("subSSaved", "getSubs:::".plus(response), "getMySubscriptionsFromLocalPreferences", "getMySubscriptionsFromLocalPreferences")

    return response
}

inline fun getCustomerDataFromLocalPreferences(): CustomerData {
    return ProjectApplication.getInstance().getLocalPreferneces().getResponse(
            ProjectApplication.getInstance().getLocalPreferneces().KEY_CUSTOMER_DATA,
            CustomerData::class.java) as CustomerData
}

inline fun setCustomerDataToLocalPreferences(customerData: CustomerData) {
    ProjectApplication.getInstance().getLocalPreferneces().setResponse(
            ProjectApplication.getInstance().getLocalPreferneces().KEY_CUSTOMER_DATA,
            customerData)
}

inline fun setFCMDataToLocalPreferences(fcmResponse: FCMResponse) {
    ProjectApplication.getInstance().getLocalPreferneces().setResponse(
            EndPoints.FCM_TOKEN_END_POINT,
            fcmResponse)
}

inline fun getFCMDataFromLocalPreferences(): FCMResponse {
    return ProjectApplication.getInstance().getLocalPreferneces().getResponse(
            EndPoints.FCM_TOKEN_END_POINT,
            FCMResponse::class.java) as FCMResponse
}

inline fun getUserAccountsDataFromLocalPreferences(): UserAccounts {
    return ProjectApplication.getInstance().getLocalPreferneces().getResponse(
            ProjectApplication.getInstance().getLocalPreferneces().KEY_USER_ACCOUNTS_DATA,
            UserAccounts::class.java) as UserAccounts
}

inline fun getMenusDataFromLocalPreferences(): MenuResponse {
    val preferenceKey = EndPoints.REQUEST_MENUS_ENDPOINT
            .plus(getCurrentlyLoggedInUserMsisdn())

    return ProjectApplication.getInstance().getLocalPreferneces().getResponse(
            preferenceKey,
            MenuResponse::class.java) as MenuResponse
}

inline fun setMenusDataToLocalPreferences(menuResponse: MenuResponse) {
    val preferenceKey = EndPoints.REQUEST_MENUS_ENDPOINT
            .plus(getCurrentlyLoggedInUserMsisdn())

    ProjectApplication.getInstance().getLocalPreferneces().setResponse(
            preferenceKey,
            menuResponse)
}

inline fun getLoginAppResumeDataFromLocalPreferences(): LoginAppResumeResponse {
    return ProjectApplication.getInstance().getLocalPreferneces().getResponse(
            EndPoints.REQUEST_APP_RESUME_END_POINT,
            LoginAppResumeResponse::class.java) as LoginAppResumeResponse
}

inline fun setLoginAppResumeDataToLocalPreferences(loginResponse: LoginAppResumeResponse) {
    ProjectApplication.getInstance().getLocalPreferneces().setResponse(
            EndPoints.REQUEST_APP_RESUME_END_POINT,
            loginResponse)

    logE("dataResumeSaved", "savedData:::".plus(getLoginAppResumeDataFromLocalPreferences()), "setLoginAppResumeDataToLocalPreferences", "setLoginAppResumeDataToLocalPreferences")
}

inline fun getHomePageResponseFromLocalPreferences(where: String): GetHomePageResponse {
    val prefrenceKey = EndPoints.HOMEPAGE_API.plus(ProjectApplication.mLocaleManager.getLanguage().plus(getCurrentlyLoggedInUserMsisdn()))

    logE("homePagekeyX", "GETkey:::".plus(prefrenceKey), where, "getHomePageResponseFromLocalPreferences")
    return ProjectApplication.getInstance().getLocalPreferneces().getResponse(
            prefrenceKey,
            GetHomePageResponse::class.java) as GetHomePageResponse
}

inline fun removeHomePageResponseFromLocalPreferences() {
    val prefrenceKey = EndPoints.HOMEPAGE_API.plus(ProjectApplication.mLocaleManager.getLanguage().plus(getCurrentlyLoggedInUserMsisdn()))
    ProjectApplication.getInstance().getLocalPreferneces().deleteHomePageResponse(prefrenceKey)
}

inline fun setHomePageResponseToLocalPreferences(getHomePageResponse: GetHomePageResponse) {
    val prefrenceKey = EndPoints.HOMEPAGE_API.plus(ProjectApplication.mLocaleManager.getLanguage().plus(getCurrentlyLoggedInUserMsisdn()))

    logE("homePagekeyX", "SETkey:::".plus(prefrenceKey), "setHomePageResponseToLocalPreferences", "setHomePageResponseToLocalPreferences")
    ProjectApplication.getInstance().getLocalPreferneces().setResponse(
            prefrenceKey,
            getHomePageResponse)
}

inline fun setTariffsResponseToLocalPreferences(tariffsResponse: TariffsResponse) {
    val preferenceKey = EndPoints.TARIFFS_ENDPOINT
            .plus(ProjectApplication.mLocaleManager.getLanguage()
                    .plus(getCurrentlyLoggedInUserMsisdn()))

    ProjectApplication.getInstance().getLocalPreferneces().setResponse(
            preferenceKey,
            tariffsResponse)
}

inline fun getTariffsResponseFromLocalPreferences(): TariffsResponse {
    val preferenceKey = EndPoints.TARIFFS_ENDPOINT
            .plus(ProjectApplication.mLocaleManager.getLanguage()
                    .plus(getCurrentlyLoggedInUserMsisdn()))

    return ProjectApplication.getInstance().getLocalPreferneces().getResponse(
            preferenceKey,
            TariffsResponse::class.java) as TariffsResponse
}

inline fun getProfileImageFromLocalPreferences(): String {
    return ProjectApplication.getInstance().getLocalPreferneces().getString(
            ProjectApplication.getInstance().getLocalPreferneces().PIC_PROFILE_PICTURE_PATH, "")!!
}

inline fun setProfileImageToLocalPreferences(picturePathURL: String) {
    ProjectApplication.getInstance().getLocalPreferneces().addString(
            ProjectApplication.getInstance().getLocalPreferneces().PIC_PROFILE_PICTURE_PATH, picturePathURL)
}

inline fun getIsBiometricEnabledFlagFromLocalPreferences(): Boolean {
    return ProjectApplication.getInstance().getLocalPreferneces().getBool(
            ProjectApplication.getInstance().getLocalPreferneces().FINGER_PRINT_SHOW, false)
}

inline fun setIsBiometricEnabledFlagToLocalPreferences(biometricFlag: Boolean) {
    ProjectApplication.getInstance().getLocalPreferneces().addBool(
            ProjectApplication.getInstance().getLocalPreferneces().FINGER_PRINT_SHOW, biometricFlag)
}

inline fun getDisclaimerCheckFromLocalPreferences(screenKey:String): Boolean {
    var preferenceKey=""
    if(screenKey.equals("o",ignoreCase = true)){
        preferenceKey=ProjectApplication.getInstance().getLocalPreferneces().PREF_DISCLAIMER_DIALOG_OPERATIONAL_HISTORY_NEEDTO_SHOW_KEY
    }else{
        preferenceKey=ProjectApplication.getInstance().getLocalPreferneces().PREF_DISCLAIMER_DIALOG_USAGE_HISTORY_DETAILS_NEEDTO_SHOW_KEY
    }

    return ProjectApplication.getInstance().getLocalPreferneces().getBool(
            preferenceKey, true)
}

inline fun setDisclaimerCheckToLocalPreferences(disclaimerCheck: Boolean,screenKey:String) {
    var preferenceKey=""
    if(screenKey.equals("o",ignoreCase = true)){
        preferenceKey=ProjectApplication.getInstance().getLocalPreferneces().PREF_DISCLAIMER_DIALOG_OPERATIONAL_HISTORY_NEEDTO_SHOW_KEY
    }else{
        preferenceKey=ProjectApplication.getInstance().getLocalPreferneces().PREF_DISCLAIMER_DIALOG_USAGE_HISTORY_DETAILS_NEEDTO_SHOW_KEY
    }

    ProjectApplication.getInstance().getLocalPreferneces().addBool(
            preferenceKey, disclaimerCheck)
}

inline fun getCurrentlyLoggedInUserMsisdn(): String {

    var msisdn = ""

    if (getCustomerDataFromLocalPreferences().msisdn != null &&
            hasValue(getCustomerDataFromLocalPreferences().msisdn)) {
        msisdn = getCustomerDataFromLocalPreferences().msisdn!!
    }

    return msisdn
}