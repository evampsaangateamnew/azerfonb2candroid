package com.azarphone.util

import android.util.Log
import com.azarphone.api.pojo.helperpojo.menus.HorizontalChildMenusHelperModel
import com.azarphone.api.pojo.response.menuresponse.MenuHorizontalItem
import com.azarphone.api.pojo.response.menuresponse.Item
import com.azarphone.validators.hasValue
import com.azarphone.validators.isNumeric
import java.util.*

/**
 * @author Junaid Hassan on 20, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */

/**this processor utility is used to process all the child menus
 * for the horizontal menu items
 * for the time, only services, topup and myacoount have the child menus
 * to process each of the child menu items for the above parent horizontal menus
 * we use this util to process child items for menus*/
inline fun processServicesChildMenuItems(position: Int, horizontalMenu: List<MenuHorizontalItem?>) {
    if (horizontalMenu[position]!!.items == null) {
        return//safe passage
    }

    HorizontalMenuItems.servicesMenuModel = ArrayList()
    val menuItems = horizontalMenu[position]!!.items
    for (its in 0 until menuItems!!.size) {
        if (hasValue(menuItems[its].identifier) &&
            hasValue(menuItems[its].sortOrder) &&
            hasValue(menuItems[its].title)
        ) {

            /**check the special offers identifier*/
            if (!isShowSpecialMenuInServicesMenu()) {
                if (menuItems[its].identifier.equals(ConstantsUtility.HorizontalChildMenuIdentifiers.IDENTIFIER_SPECIAL_SERVICES)) {
                    //skip this menu
                } else {
                    HorizontalMenuItems.servicesMenuModel!!.add(
                        HorizontalChildMenusHelperModel(
                            menuItems[its].identifier!!,
                            menuItems[its].title!!,
                            menuItems[its].sortOrder!!
                        )
                    )
                    if (menuItems[its].identifier.equals(ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_TOPUP)) {
                        processTopUpChildMenuItemsInService(menuItems[its].item)
                    }
                }
            } else {
                HorizontalMenuItems.servicesMenuModel!!.add(
                    HorizontalChildMenusHelperModel(
                        menuItems[its].identifier!!,
                        menuItems[its].title!!,
                        menuItems[its].sortOrder!!
                    )
                )
            }
        }
    }//for ends

    //sort the list
    Collections.sort(
        HorizontalMenuItems.servicesMenuModel!!,
        object : Comparator<HorizontalChildMenusHelperModel> {
            override fun compare(
                o1: HorizontalChildMenusHelperModel,
                o2: HorizontalChildMenusHelperModel
            ): Int {
                return o1.sortOrder.toIntOrNull() ?: 0.compareTo(o2.sortOrder.toIntOrNull() ?: 0)
            }
        })
}//processServicesChildMenuItems ends

fun processTopUpChildMenuItemsInService(items: List<Item>?) {
    HorizontalMenuItems.topupMenuModel = ArrayList()
    items?.forEach {
        if (hasValue(it.identifier) && hasValue(it.sortOrder) && hasValue(it.title)) {
            HorizontalMenuItems.topupMenuModel!!.add(
                HorizontalChildMenusHelperModel(
                    it.identifier!!,
                    it.title!!,
                    it.sortOrder!!
                )
            )
        }
    }
    HorizontalMenuItems.topupMenuModel?.sortWith { o1, o2 ->
        o1.sortOrder.toIntOrNull() ?: 0.compareTo(o2.sortOrder.toIntOrNull() ?: 0)
    }
    Log.e("top_", "${HorizontalMenuItems.topupMenuModel?.size} -- ${getTopupMenuModel()?.size}")
}

inline fun processTopupChildMenuItems(position: Int, horizontalMenu: List<MenuHorizontalItem?>) {
    if (horizontalMenu[position]!!.items == null) {
        return//safe passage
    }

    HorizontalMenuItems.topupMenuModel = ArrayList()
    val menuItems = horizontalMenu[position]!!.items
    for (its in 0 until menuItems!!.size) {
        if (hasValue(menuItems[its].identifier) &&
            hasValue(menuItems[its].sortOrder) &&
            hasValue(menuItems[its].title)
        ) {
            HorizontalMenuItems.topupMenuModel!!.add(
                HorizontalChildMenusHelperModel(
                    menuItems[its].identifier!!,
                    menuItems[its].title!!,
                    menuItems[its].sortOrder!!
                )
            )
        }
    }//for ends

    //sort the list
    Collections.sort(
        HorizontalMenuItems.topupMenuModel!!,
        object : Comparator<HorizontalChildMenusHelperModel> {
            override fun compare(
                o1: HorizontalChildMenusHelperModel,
                o2: HorizontalChildMenusHelperModel
            ): Int {
                return o1.sortOrder.toIntOrNull() ?: 0.compareTo(o2.sortOrder.toIntOrNull() ?: 0)
            }
        })
}//processServicesChildMenuItems ends

inline fun processMyAccountChildMenuItems(
    position: Int,
    horizontalMenu: List<MenuHorizontalItem?>
) {
    if (horizontalMenu[position]!!.items == null) {
        return//safe passage
    }

    HorizontalMenuItems.myaccountMenuModel = ArrayList()
    val menuItems = horizontalMenu[position]!!.items
    for (its in 0 until menuItems!!.size) {
        if (hasValue(menuItems[its].identifier) &&
            hasValue(menuItems[its].sortOrder) &&
            hasValue(menuItems[its].title)
        ) {
            HorizontalMenuItems.myaccountMenuModel!!.add(
                HorizontalChildMenusHelperModel(
                    menuItems[its].identifier!!,
                    menuItems[its].title!!,
                    menuItems[its].sortOrder!!
                )
            )
        }
    }//for ends

    //sort the list
    Collections.sort(
        HorizontalMenuItems.myaccountMenuModel!!,
        object : Comparator<HorizontalChildMenusHelperModel> {
            override fun compare(
                o1: HorizontalChildMenusHelperModel,
                o2: HorizontalChildMenusHelperModel
            ): Int {
                return o1.sortOrder.toIntOrNull() ?: 0.compareTo(o2.sortOrder.toIntOrNull() ?: 0)
            }
        })
}//processServicesChildMenuItems ends