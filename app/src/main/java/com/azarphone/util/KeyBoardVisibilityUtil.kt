package com.azarphone.util

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

/**
 * @author Junaid Hassan on 12, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
inline fun showKeyboard(activity: Activity) {
    if (activity == null || activity.isFinishing) return
    val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
}//showKeyboard

inline fun hideKeyboard(activity: Activity) {
    try {
        if (activity == null || activity.isFinishing) return
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm?.hideSoftInputFromWindow(view.windowToken, 0)
    } catch (e: Exception) {
        logE("keyboard", "Error:::".plus(e.toString()), "KeyBoardShowHideTool", "hideKeyboard")
    }

}//hideKeyboard