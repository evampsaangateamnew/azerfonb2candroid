package com.azarphone.util

import android.text.Editable
import android.text.Selection
import android.text.SpannableString
import android.text.TextWatcher
import android.widget.EditText
import android.widget.TextView

/**
 * @author Junaid Hassan on 03, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */

inline fun addPrefixToUserNameField(view: EditText) {
    view.setText(ConstantsUtility.UserConstants.USERNAME_PREFIX)
    Selection.setSelection(SpannableString(view.text), view.text!!.length)
    view.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            if (!s.toString().startsWith(ConstantsUtility.UserConstants.USERNAME_PREFIX)) {
                view.setText(ConstantsUtility.UserConstants.USERNAME_PREFIX)
                Selection.setSelection(SpannableString(view.text), view.text!!.length)
            }else if(s.toString().equals(ConstantsUtility.UserConstants.USERNAME_PREFIX)){
                view.setSelection( ConstantsUtility.UserConstants.USERNAME_PREFIX.length)
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        }
    })
}//addPrefixToUserNameField ends