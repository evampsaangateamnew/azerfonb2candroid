package com.azarphone.util

import android.graphics.Rect
import androidx.recyclerview.widget.RecyclerView
import android.view.View

/**
 * @author Junaid Hassan on 10, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class RecyclerItemDecorator: androidx.recyclerview.widget.RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, view: View, parent: androidx.recyclerview.widget.RecyclerView, state: androidx.recyclerview.widget.RecyclerView.State) {
        outRect.left = (-10 * view.resources.displayMetrics.density).toInt()
        outRect.right = (-10 * view.resources.displayMetrics.density).toInt()
        outRect.top = 0
        outRect.bottom = 0
        //super.getItemOffsets(outRect, view, parent, state);
    }
}