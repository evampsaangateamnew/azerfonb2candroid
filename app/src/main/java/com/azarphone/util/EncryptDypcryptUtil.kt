package com.azarphone.util

import android.app.Activity
import android.util.Base64
import com.azarphone.validators.hasValue
import java.io.UnsupportedEncodingException
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec

/**
 * @author Junaid Hassan on 06, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
inline fun encrypt(input: String?, keySec: String): String {
    try {
        if (input == null) return ""
        val keySecPass = keySec.toByteArray()

        val data = input.toByteArray()

        val key = SecretKeySpec(keySecPass, "AES")

        val cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC")

        // encryption pass
        cipher.init(Cipher.ENCRYPT_MODE, key)

        val cipherText = ByteArray(cipher.getOutputSize(data.size))

        var ctLength = cipher.update(data, 0, data.size, cipherText, 0)
        ctLength += cipher.doFinal(cipherText, ctLength)

        var encrited = ""

        try {
            encrited = String(Base64.encode(cipherText, Base64.DEFAULT))
        } catch (e: Exception) {
            logE("encrypt", "error1:::".plus(e.message), "EncryptDecryptService", "encrypt")
        }
        return encrited
    } catch (e: Exception) {
        logE("encrypt", "error2:::".plus(e.message), "EncryptDecryptService", "encrypt")
    }
    return ""
}

@Throws(Exception::class)
inline fun decrypt(encryptedData: String, keySec: String): String {
    try {
        val keySecPass = keySec.toByteArray()

        val input = Base64.decode(encryptedData.toByteArray(), Base64.DEFAULT)

        val key = SecretKeySpec(keySecPass, "AES")

        val cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC")

        cipher.init(Cipher.DECRYPT_MODE, key)

        val plainText = ByteArray(cipher.getOutputSize(input.size))
        var ptLength = cipher.update(input, 0, input.size, plainText, 0)
        ptLength += cipher.doFinal(plainText, ptLength)

        var simplText = String(plainText)

        if (hasValue(simplText)) {
            simplText = simplText.trim { it <= ' ' }
        }

        return simplText
    } catch (e: Exception) {
        logE("decrypt", "error:::".plus(e.message), "EncryptDecryptService", "decrypt")
    }

    return ""
}

fun getBase64EncodedMessage(simpleText: String,activity: Activity): String {
    var textToEncrpt = ""
    var data = ByteArray(0)
    try {
        data = simpleText.toByteArray(charset("UTF-8"))
    } catch (e: UnsupportedEncodingException) {
        logE("base64Error","error:::".plus(e.toString()),"TextEncryptionTool","getBase64EncodedMessage")
    }

    textToEncrpt = Base64.encodeToString(data, Base64.DEFAULT)

    return textToEncrpt
}