package com.azarphone.util

import com.azarphone.ProjectApplication
import com.azarphone.api.pojo.response.menuresponse.MenuHorizontalItem
import com.azarphone.api.pojo.response.menuresponse.MenuResponse
import com.azarphone.api.pojo.response.menuresponse.MenuVerticalItem

/**
 * @author Junaid Hassan on 09, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */

inline fun getVerticalMenuLocaleBased(response: MenuResponse): List<MenuVerticalItem?>? {
    var menuVerticalList: List<MenuVerticalItem?>? = null

    //get the current language keyword from the preferences
    val currentLanguage = ProjectApplication.mLocaleManager.getLanguage()
    if (currentLanguage.equals(ConstantsUtility.LanguageKeywords.AZ, true)) {
        menuVerticalList = response.data.MenuAZ?.menuVertical

    } else if (currentLanguage.equals(ConstantsUtility.LanguageKeywords.EN, ignoreCase = true)) {
        menuVerticalList = response.data.MenuEN?.menuVertical

    } else if (currentLanguage.equals(ConstantsUtility.LanguageKeywords.RU, ignoreCase = true)) {
        menuVerticalList = response.data.MenuRU?.menuVertical
    }

    return menuVerticalList
}

inline fun getHorizontalMenuLocaleBased(response: MenuResponse): List<MenuHorizontalItem?>? {
    var menuHorizontalList: List<MenuHorizontalItem?>? = null

    //get the current language keyword from the preferences
    val currentLanguage = ProjectApplication.mLocaleManager.getLanguage()
    if (currentLanguage.equals(ConstantsUtility.LanguageKeywords.AZ, true)) {
        if (response.data.MenuAZ != null && response.data.MenuAZ!!.menuHorizontal != null) {
            menuHorizontalList = response.data.MenuAZ!!.menuHorizontal
        }
    } else if (currentLanguage.equals(ConstantsUtility.LanguageKeywords.EN, ignoreCase = true)) {
        if (response.data.MenuEN != null && response.data.MenuEN!!.menuHorizontal != null) {
            menuHorizontalList = response.data.MenuEN!!.menuHorizontal
        }
    } else if (currentLanguage.equals(ConstantsUtility.LanguageKeywords.RU, ignoreCase = true)) {
        if (response.data.MenuRU != null && response.data.MenuRU!!.menuHorizontal != null) {
            menuHorizontalList = response.data.MenuRU!!.menuHorizontal
        }
    }

    return menuHorizontalList
}