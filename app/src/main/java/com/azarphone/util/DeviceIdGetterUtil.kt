package com.azarphone.util

import android.annotation.SuppressLint
import android.content.Context
import android.provider.Settings

/**
 * @author Junaid Hassan on 11, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@SuppressLint("HardwareIds")
inline fun getDeviceID(context: Context): String {
    logE("devIdX", "deviceId:::".plus(Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)), "DeviceIdGetterTool", "getDeviceID")
    return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
}