package com.azarphone.util

import com.azarphone.ProjectApplication
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * @author Junaid Hassan on 02, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
inline fun getMiliSecondsFromDateForVasServices(myDate: String): Long {
    var miliSec: Long = 0
    try {
        val sdf = SimpleDateFormat(ConstantsUtility.DateFormats.ddDASHMMDASHyyWithTime)
        var date: Date? = null
        try {
            date = sdf.parse(myDate)
        } catch (e: ParseException) {
            logE("getMiliSeconds123", "error:::".plus(e.toString()), "MySubscriptionsUtil", "getMiliSecondsFromDateForMySubscription")
            return 0
        }

        miliSec = date!!.time
    } catch (e: Exception) {
        logE("getMiliSeconds324", "error:::".plus(e.toString()), "MySubscriptionsUtil", "getMiliSecondsFromDateForMySubscription")
    }

    return miliSec
}

inline fun getOneDayMinusForVasServices(startMili: Long): Long {
    var _startMili = startMili
    if (_startMili > 0) {
        _startMili = (_startMili - 8.64e+7).toLong()
    }
    return _startMili
}

inline fun getCurrentMiliSecondsForVasServices(): Long {
    return System.currentTimeMillis()
}

inline fun getDaysDifferenceBetweenMilisecondsForVasServices(startDate: Long, endDate: Long): Int {

    val duration = endDate - startDate

    val diffInDays = TimeUnit.MILLISECONDS.toDays(duration)

    return diffInDays.toInt()
}

inline fun getCoreServiceCallDivertSectionTitle(): String {
    var forwardToTitle = "Forwarded to"
    if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN) {
        //english
        forwardToTitle = "Forwarded to:"
    }

    if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ) {
        //azri
        forwardToTitle = "Yönləndirilib:"
    }

    if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.RU) {
        //russian
        forwardToTitle = "Перенаправлено на:"
    }

    return forwardToTitle
}//getCoreServiceCallDivertSectionTitle ends

inline fun getLocalizedFreeTextForVasServices(): String {
    var freeLabel = ""
    when {
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.EN, ignoreCase = true) -> freeLabel = ConstantsUtility.CoreServicesConstants.FREE_EN
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.AZ, ignoreCase = true) -> freeLabel = ConstantsUtility.CoreServicesConstants.FREE_AZ
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.RU, ignoreCase = true) -> freeLabel = ConstantsUtility.CoreServicesConstants.FREE_RU
    }

    return freeLabel
}

inline fun getLocalizedDaysTextForVasServices(days: Int): String {
    var daysLabel = ""
    if (ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.EN, ignoreCase = true)) {
        if (days > 1) {
            daysLabel = "days"
        } else {
            daysLabel = "day"
        }
    } else if (ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.AZ, ignoreCase = true)) {
        if (days > 1) {
            daysLabel = "gün"
        } else {
            daysLabel = "gün"
        }
    } else {
        if (days > 1) {
            daysLabel = "дн."
        } else {
            daysLabel = "дн."
        }
    }

    return daysLabel
}

inline fun getForwardNumberNotSetLabel(): String {
    var notSetLabel = ""
    when {
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.EN, ignoreCase = true) -> notSetLabel = ConstantsUtility.CoreServicesConstants.EN_FORWARD_NUMBER_NOT_SET_LABEL
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.AZ, ignoreCase = true) -> notSetLabel = ConstantsUtility.CoreServicesConstants.AZ_FORWARD_NUMBER_NOT_SET_LABEL
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.RU, ignoreCase = true) -> notSetLabel = ConstantsUtility.CoreServicesConstants.RU_FORWARD_NUMBER_NOT_SET_LABEL
    }

    return notSetLabel
}//getForwardNumberNotSetLabel ends

inline fun getCoreServicesActivateProcessConfirmationPopup(): String {
    var message="Please confirm to continue."

    if(ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.EN, ignoreCase = true)){
        message="Please confirm to continue."
    }else if(ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.AZ, ignoreCase = true)){
        message="Davam etmək üçün təsdiq et."
    }else{
        message="Пожалуйста, подтверди, чтобы продолжить."
    }

    return message
}

inline fun getCoreServicesDeactivateProcessConfirmationPopup(): String {
    var message="Please confirm to continue."

    if(ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.EN, ignoreCase = true)){
        message="Please confirm to continue."
    }else if(ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.AZ, ignoreCase = true)){
        message="Davam etmək üçün təsdiq et."
    }else{
        message="Пожалуйста, подтверди, чтобы продолжить."
    }

    return message
}

inline fun getLocalizedCropButtonTitle(): String {
    return when {
        ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "Crop"
        ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Kəsmək"
        else -> "Обрезать"
    }
}