package com.azarphone.util

import android.content.Context
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.response.mysubscriptions.helper.Subscriptions
import com.azarphone.api.pojo.response.mysubscriptions.response.MySubscriptionResponse
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.validators.hasValue

/**
 * @author Junaid Hassan on 23, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
inline fun checkIfSupplementaryOfferPresentInMySubscriptions(context: Context, pageTitle: String, offeringId: String): Boolean {

    var isPresent = false
    if (UserDataManager.getMySubscriptionResponse() != null && UserDataManager.getMySubscriptionResponse()!!.data != null) {
        val mySubscriptionResponse = UserDataManager.getMySubscriptionResponse()
       /* if (context.resources.getString(R.string.supplementary_offers_internet_tab_title).equals(pageTitle, ignoreCase = true)) {
            logE("Check1892", "case:::Internet", "PackagesUtil", "checkIfSupplementaryOfferPresentInMySubscriptions")
            if (mySubscriptionResponse!!.data!!.internet != null && mySubscriptionResponse.data!!.internet!!.offers != null) {
                isPresent = checkIfSupplementaryOfferAreOnSameLevelOfMySubscriptions(mySubscriptionResponse.data!!.internet!!.offers as ArrayList<Subscriptions?>, offeringId)
            } else {
                isPresent = false
            }
        } else if (context.resources.getString(R.string.supplementary_offers_call_tab_title).equals(pageTitle, ignoreCase = true)) {
            logE("Check1892", "case:::Call", "PackagesUtil", "checkIfSupplementaryOfferPresentInMySubscriptions")
            if (mySubscriptionResponse!!.data!!.call != null && mySubscriptionResponse.data!!.call!!.offers != null) {
                isPresent = checkIfSupplementaryOfferAreOnSameLevelOfMySubscriptions(mySubscriptionResponse.data!!.call!!.offers as ArrayList<Subscriptions?>, offeringId)
            } else {
                isPresent = false
            }
        } else if (context.resources.getString(R.string.supplementary_offers_sms_tab_title).equals(pageTitle, ignoreCase = true)) {
            logE("Check1892", "case:::SMS", "PackagesUtil", "checkIfSupplementaryOfferPresentInMySubscriptions")
            if (mySubscriptionResponse!!.data!!.sms != null && mySubscriptionResponse.data!!.sms!!.offers != null) {
                isPresent = checkIfSupplementaryOfferAreOnSameLevelOfMySubscriptions(mySubscriptionResponse.data!!.sms!!.offers as ArrayList<Subscriptions?>, offeringId)
            } else {
                isPresent = false
            }
        } else if (context.resources.getString(R.string.supplementary_offers_campaign_tab_title).equals(pageTitle, ignoreCase = true)) {
            logE("Check1892", "case:::Campaign", "PackagesUtil", "checkIfSupplementaryOfferPresentInMySubscriptions")
            if (mySubscriptionResponse!!.data!!.campaign != null && mySubscriptionResponse.data!!.campaign!!.offers != null) {
                isPresent = checkIfSupplementaryOfferAreOnSameLevelOfMySubscriptions(mySubscriptionResponse.data!!.campaign!!.offers as ArrayList<Subscriptions?>, offeringId)
            } else {
                isPresent = false
            }
        } else if (context.resources.getString(R.string.supplementary_offers_hybrid_tab_title).equals(pageTitle, ignoreCase = true)) {
            logE("Check1892", "case:::Hybrid", "PackagesUtil", "checkIfSupplementaryOfferPresentInMySubscriptions")
            if (mySubscriptionResponse!!.data!!.hybrid != null && mySubscriptionResponse.data!!.hybrid!!.offers != null) {
                isPresent = checkIfSupplementaryOfferAreOnSameLevelOfMySubscriptions(mySubscriptionResponse.data!!.hybrid!!.offers as ArrayList<Subscriptions?>, offeringId)
            } else {
                isPresent = false
            }
        } else if (context.resources.getString(R.string.supplementary_offers_tm_tab_title).equals(pageTitle, ignoreCase = true)) {
            logE("Check1892", "case:::TM", "PackagesUtil", "checkIfSupplementaryOfferPresentInMySubscriptions")
            if (mySubscriptionResponse!!.data!!.tm != null && mySubscriptionResponse.data!!.tm!!.offers != null) {
                isPresent = checkIfSupplementaryOfferAreOnSameLevelOfMySubscriptions(mySubscriptionResponse.data!!.tm!!.offers as ArrayList<Subscriptions?>, offeringId)
            } else {
                isPresent = false
            }
        } else if (context.resources.getString(R.string.supplementary_offers_internetInclusiveOffers_tab_title).equals(pageTitle, ignoreCase = true)) {
            logE("Check1892", "case:::InternetInclusiveOffers", "PackagesUtil", "checkIfSupplementaryOfferPresentInMySubscriptions")
            if (mySubscriptionResponse!!.data!!.internetInclusiveOffers != null && mySubscriptionResponse.data!!.internetInclusiveOffers!!.offers != null) {
                isPresent = checkIfSupplementaryOfferAreOnSameLevelOfMySubscriptions(mySubscriptionResponse.data!!.internetInclusiveOffers!!.offers as ArrayList<Subscriptions?>, offeringId)
            } else {
                isPresent = false
            }
        } else if (context.resources.getString(R.string.supplementary_offers_voiceInclusiveOffers_tab_title).equals(pageTitle, ignoreCase = true)) {
            logE("Check1892", "case:::Internet", "VoiceInclusiveOffers", "checkIfSupplementaryOfferPresentInMySubscriptions")
            if (mySubscriptionResponse!!.data!!.voiceInclusiveOffers != null && mySubscriptionResponse.data!!.voiceInclusiveOffers!!.offers != null) {
                isPresent = checkIfSupplementaryOfferAreOnSameLevelOfMySubscriptions(mySubscriptionResponse.data!!.voiceInclusiveOffers!!.offers as ArrayList<Subscriptions?>, offeringId)
            } else {
                isPresent = false
            }
        } else if (context.resources.getString(R.string.supplementary_offers_smsInclusiveOffers_tab_title).equals(pageTitle, ignoreCase = true)) {
            logE("Check1892", "case:::Internet", "SMSInclusiveOffers", "checkIfSupplementaryOfferPresentInMySubscriptions")
            if (mySubscriptionResponse!!.data!!.smsInclusiveOffers != null && mySubscriptionResponse.data!!.smsInclusiveOffers!!.offers != null) {
                isPresent = checkIfSupplementaryOfferAreOnSameLevelOfMySubscriptions(mySubscriptionResponse.data!!.smsInclusiveOffers!!.offers as ArrayList<Subscriptions?>, offeringId)
            } else {
                isPresent = false
            }
        } else if (context.resources.getString(R.string.supplementary_offers_roaming_tab_title).equals(pageTitle, ignoreCase = true)) {
            logE("Check1892", "case:::Roaming", "PackagesUtil", "checkIfSupplementaryOfferPresentInMySubscriptions")
            if (mySubscriptionResponse!!.data!!.roaming != null && mySubscriptionResponse.data!!.roaming!!.offers != null) {
                isPresent = checkIfSupplementaryOfferAreOnSameLevelOfMySubscriptions(mySubscriptionResponse.data!!.roaming!!.offers as ArrayList<Subscriptions?>, offeringId)
            } else {
                isPresent = false
            }
        } else if (ConstantsUtility.PackagesConstants.TM_AND_SPECIALS.equals(pageTitle, ignoreCase = true)) {
            logE("Check1892", "case:::TM&Specials", "PackagesUtil", "checkIfSupplementaryOfferPresentInMySubscriptions")
            isPresent = checkIfSupplementaryOfferAreOnSameLevelOfMySubscriptionsForAll(mySubscriptionResponse!!, offeringId)
        } else if (context.resources.getString(R.string.my_subscriptions_all_inclusive_tab_title).equals(pageTitle, ignoreCase = true)) {
            logE("Check1892", "case:::all inclusive", "PackagesUtil", "checkIfSupplementaryOfferPresentInMySubscriptions")
            isPresent = checkIfSupplementaryOfferAreOnSameLevelOfMySubscriptionsForAll(mySubscriptionResponse!!, offeringId)
        } else {*/
            logE("Check1892", "case:::else", "PackagesUtil", "checkIfSupplementaryOfferPresentInMySubscriptions")
            isPresent = checkIfSupplementaryOfferAreOnSameLevelOfMySubscriptionsForAll(mySubscriptionResponse!!, offeringId)
//        }
    }

    return isPresent
}//checkIfSupplementaryOfferPresentInMySubscriptions ends

inline fun getSupplementaryOfferRenewConfirmationMessage(): String {
    var confirmationMessage = "Please Confirm.en"

    if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN) {
        //english
        confirmationMessage = "Your current package will be replaced with new one. Please confirm to continue."
    }

    if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ) {
        //azri
        confirmationMessage = "Mövcud paketin yenisi ilə əvəz olunacaq. Davam etmək üçün təsdiq et."
    }

    if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.RU) {
        //russian
        confirmationMessage = "Твой текущий пакет будет заменен новым. Пожалуйста, подтверди, чтобы продолжить."
    }

    return confirmationMessage
}

inline fun getSupplementaryOfferSubscribeConfirmationMessage(): String {
    var confirmationMessage = "Please Confirm.en"

    if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN) {
        //english
        confirmationMessage = "Activation is chargeable. Please confirm to continue."
    }

    if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ) {
        //azri
        confirmationMessage = "Aktivləşdirmə ödənişlidir. Davam etmək üçün təsdiq et."
    }

    if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.RU) {
        //russian
        confirmationMessage = "Активация платная. Пожалуйста, подтверди, чтобы продолжить."
    }

    return confirmationMessage
}

fun checkIfSupplementaryOfferAreOnSameLevelOfMySubscriptionsForAll(mySubscriptionResponse: MySubscriptionResponse, offeringId: String): Boolean {
    var isOnSameLevel = false

    if (mySubscriptionResponse.data!!.internet != null && mySubscriptionResponse.data!!.internet!!.offers != null) {
        //iterate over the whole list of offers
        for (its in mySubscriptionResponse.data!!.internet!!.offers!!.indices) {
            if (mySubscriptionResponse.data!!.internet!!.offers!![its]!!.header != null &&
                    mySubscriptionResponse.data!!.internet!!.offers!![its]!!.header!!.offeringId != null) {
                if (mySubscriptionResponse.data!!.internet!!.offers!![its]!!.header!!.offeringId == offeringId) {
                    isOnSameLevel = true
                    break
                }
            }
        }
    }

    if (mySubscriptionResponse.data!!.sms != null && mySubscriptionResponse.data!!.sms!!.offers != null) {
        //iterate over the whole list of offers
        for (its in mySubscriptionResponse.data!!.sms!!.offers!!.indices) {
            if (mySubscriptionResponse.data!!.sms!!.offers!![its]!!.header != null &&
                    mySubscriptionResponse.data!!.sms!!.offers!![its]!!.header!!.offeringId != null) {
                if (mySubscriptionResponse.data!!.sms!!.offers!![its]!!.header!!.offeringId == offeringId) {
                    isOnSameLevel = true
                    break
                }
            }
        }
    }

    if (mySubscriptionResponse.data!!.call != null && mySubscriptionResponse.data!!.call!!.offers != null) {
        //iterate over the whole list of offers
        for (its in mySubscriptionResponse.data!!.call!!.offers!!.indices) {
            if (mySubscriptionResponse.data!!.call!!.offers!![its]!!.header != null &&
                    mySubscriptionResponse.data!!.call!!.offers!![its]!!.header!!.offeringId != null) {
                if (mySubscriptionResponse.data!!.call!!.offers!![its]!!.header!!.offeringId == offeringId) {
                    isOnSameLevel = true
                    break
                }
            }
        }
    }

    if (mySubscriptionResponse.data!!.campaign != null && mySubscriptionResponse.data!!.campaign!!.offers != null) {
        //iterate over the whole list of offers
        for (its in mySubscriptionResponse.data!!.campaign!!.offers!!.indices) {
            if (mySubscriptionResponse.data!!.campaign!!.offers!![its]!!.header != null &&
                    mySubscriptionResponse.data!!.campaign!!.offers!![its]!!.header!!.offeringId != null) {
                if (mySubscriptionResponse.data!!.campaign!!.offers!![its]!!.header!!.offeringId == offeringId) {
                    isOnSameLevel = true
                    break
                }
            }
        }
    }

    if (mySubscriptionResponse.data!!.hybrid != null && mySubscriptionResponse.data!!.hybrid!!.offers != null) {
        //iterate over the whole list of offers
        for (its in mySubscriptionResponse.data!!.hybrid!!.offers!!.indices) {
            if (mySubscriptionResponse.data!!.hybrid!!.offers!![its]!!.header != null &&
                    mySubscriptionResponse.data!!.hybrid!!.offers!![its]!!.header!!.offeringId != null) {
                if (mySubscriptionResponse.data!!.hybrid!!.offers!![its]!!.header!!.offeringId == offeringId) {
                    isOnSameLevel = true
                    break
                }
            }
        }
    }

    if (mySubscriptionResponse.data!!.tm != null && mySubscriptionResponse.data!!.tm!!.offers != null) {
        //iterate over the whole list of offers
        for (its in mySubscriptionResponse.data!!.tm!!.offers!!.indices) {
            if (mySubscriptionResponse.data!!.tm!!.offers!![its]!!.header != null &&
                    mySubscriptionResponse.data!!.tm!!.offers!![its]!!.header!!.offeringId != null) {
                if (mySubscriptionResponse.data!!.tm!!.offers!![its]!!.header!!.offeringId == offeringId) {
                    isOnSameLevel = true
                    break
                }
            }
        }
    }

    if (mySubscriptionResponse.data!!.smsInclusiveOffers != null && mySubscriptionResponse.data!!.smsInclusiveOffers!!.offers != null) {
        //iterate over the whole list of offers
        for (its in mySubscriptionResponse.data!!.smsInclusiveOffers!!.offers!!.indices) {
            if (mySubscriptionResponse.data!!.smsInclusiveOffers!!.offers!![its]!!.header != null &&
                    mySubscriptionResponse.data!!.smsInclusiveOffers!!.offers!![its]!!.header!!.offeringId != null) {
                if (mySubscriptionResponse.data!!.smsInclusiveOffers!!.offers!![its]!!.header!!.offeringId == offeringId) {
                    isOnSameLevel = true
                    break
                }
            }
        }
    }

    if (mySubscriptionResponse.data!!.internetInclusiveOffers != null &&
            mySubscriptionResponse.data!!.internetInclusiveOffers!!.offers != null) {
        //iterate over the whole list of offers
        for (its in mySubscriptionResponse.data!!.internetInclusiveOffers!!.offers!!.indices) {
            if (mySubscriptionResponse.data!!.internetInclusiveOffers!!.offers!![its]!!.header != null &&
                    mySubscriptionResponse.data!!.internetInclusiveOffers!!.offers!![its]!!.header!!.offeringId != null) {
                if (mySubscriptionResponse.data!!.internetInclusiveOffers!!.offers!![its]!!.header!!.offeringId == offeringId) {
                    isOnSameLevel = true
                    break
                }
            }
        }
    }

    if (mySubscriptionResponse.data!!.voiceInclusiveOffers != null &&
            mySubscriptionResponse.data!!.voiceInclusiveOffers!!.offers != null) {
        //iterate over the whole list of offers
        for (its in mySubscriptionResponse.data!!.voiceInclusiveOffers!!.offers!!.indices) {
            if (mySubscriptionResponse.data!!.voiceInclusiveOffers!!.offers!![its]!!.header != null &&
                    mySubscriptionResponse.data!!.voiceInclusiveOffers!!.offers!![its]!!.header!!.offeringId != null) {
                if (mySubscriptionResponse.data!!.voiceInclusiveOffers!!.offers!![its]!!.header!!.offeringId == offeringId) {
                    isOnSameLevel = true
                    break
                }
            }
        }
    }

    if (mySubscriptionResponse.data!!.roaming != null &&
            mySubscriptionResponse.data!!.roaming!!.offers != null) {
        //iterate over the whole list of offers
        for (its in mySubscriptionResponse.data!!.roaming!!.offers!!.indices) {
            if (mySubscriptionResponse.data!!.roaming!!.offers!![its]!!.header != null &&
                    mySubscriptionResponse.data!!.roaming!!.offers!![its]!!.header!!.offeringId != null) {
                if (mySubscriptionResponse.data!!.roaming!!.offers!![its]!!.header!!.offeringId == offeringId) {
                    isOnSameLevel = true
                    break
                }
            }
        }
    }

    return isOnSameLevel
}//checkIfSupplementaryOfferAreOnSameLevelOfMySubscriptionsForAll ends

fun checkIfSupplementaryOfferAreOnSameLevelOfMySubscriptions(offersList: ArrayList<Subscriptions?>, offerindId: String): Boolean {
    var isOnSameLevel = false

    if (offersList.isNotEmpty()) {
        for (its in offersList.indices) {
            if (offersList[its]!!.header != null && offersList[its]!!.header!!.offeringId != null && hasValue(offersList[its]!!.header!!.offeringId)) {
                if (offersList[its]!!.header!!.offeringId == offerindId) {
                    isOnSameLevel = true
                    break
                }
            }
        }//for ends
    }//if (offersList.isNotEmpty()) ends

    return isOnSameLevel
}//checkIfSupplementaryOfferAreOnSameLevelOfMySubscriptions ends