package com.azarphone.util

import android.hardware.fingerprint.FingerprintManager
import android.os.Build
import android.os.CancellationSignal
import androidx.annotation.RequiresApi

/**
 * @author Junaid Hassan on 17, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
@RequiresApi(Build.VERSION_CODES.M)
class BiometricHandler : FingerprintManager.AuthenticationCallback() {

    private var cancellationSignal: CancellationSignal? = null

    fun starAuthentication(manager: FingerprintManager) {
        cancellationSignal = CancellationSignal()
        manager.authenticate(null, cancellationSignal, 0, this, null)
    }//starAuthentication ends

    fun stopAuthentication() {
        if (cancellationSignal != null) {
            cancellationSignal!!.cancel()
            cancellationSignal!!.setOnCancelListener {
                logE("cancellationSignal", "cancellation signal cancelled success", "BiometricHandler", "stopAuthentication")
            }
        }
        logE("cancellationSignal", "stopAuthentication:::called", "BiometricHandler", "stopAuthentication")
    }//stopAuthentication ends

    override fun onAuthenticationError(errorCode: Int, errString: CharSequence?) {
        super.onAuthenticationError(errorCode, errString)
        logE("cancellationSignal", "onAuthenticationError:::".plus(errString.toString()), "BiometricHandler", "onAuthenticationError")
        RootValues.getFingerPrintScannerToLoginCallBack().onBiometricAuthenticationError(errorCode, errString.toString())
    }//onAuthenticationError ends

    override fun onAuthenticationSucceeded(result: FingerprintManager.AuthenticationResult?) {
        super.onAuthenticationSucceeded(result)
        logE("cancellationSignal", "onAuthenticationSucceeded:::", "BiometricHandler", "onAuthenticationSucceeded")
        RootValues.getFingerPrintScannerToLoginCallBack().onBiometricAuthenticationSuccess()
    }//onAuthenticationSucceeded ends

    override fun onAuthenticationHelp(helpCode: Int, helpString: CharSequence?) {
        super.onAuthenticationHelp(helpCode, helpString)
        logE("cancellationSignal", "onAuthenticationHelp:::".plus(helpString), "BiometricHandler", "onAuthenticationHelp")
    }

    override fun onAuthenticationFailed() {
        super.onAuthenticationFailed()
        logE("cancellationSignal", "onAuthenticationFailed:::", "BiometricHandler", "onAuthenticationFailed")
        RootValues.getFingerPrintScannerToLoginCallBack().onBiometricAuthenticationFailed()
    }
}