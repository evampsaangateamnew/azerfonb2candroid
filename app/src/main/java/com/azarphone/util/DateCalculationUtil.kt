package com.azarphone.util

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * @author Junaid Hassan on 28, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */

inline fun getCreditMRCFormattedDate(oldFormattedDate: String): String {
    var spf = SimpleDateFormat(ConstantsUtility.DateFormats.ddDASHMMDASHyyWithTime)
    val newDate = spf.parse(oldFormattedDate)
    spf = SimpleDateFormat(ConstantsUtility.DateFormats.ddSLASHMMSLASHyy)
    return spf.format(newDate)
}

inline fun getCurrentDateTime(): String {
    val dateTime = SimpleDateFormat(ConstantsUtility.DateFormats.ddDASHMMDASHyyWithTime)
    return dateTime.format(Date())
}

inline fun getDatesDifference(firstDate: String, secondDate: String, key: String): Long {
    val format = SimpleDateFormat(ConstantsUtility.DateFormats.ddDASHMMDASHyyWithTime)
    val date1 = format.parse(firstDate)
    val date2 = format.parse(secondDate)

    val diff = date1.time - date2.time
    val seconds = diff / 1000
    val minutes = seconds / 60
    val hours = minutes / 60
    val days = hours / 24

    return if (key.equals("h", ignoreCase = true)) {
        //give hours
        hours
    } else {
        //give days
        days + 1
    }
}

inline fun getCurrentDateTimeForRateUs(): String {
    val dateTime = SimpleDateFormat(ConstantsUtility.DateFormats.ddDASHMMDASHyyWithTime)
    return dateTime.format(Date()).toString()
}

inline fun getLastDateMinusDays(dateString: String, days: Int): String {
    val date = SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd, Locale.ENGLISH).parse(dateString)
    val calendar = Calendar.getInstance()
    calendar.time = date
    calendar.add(Calendar.DAY_OF_YEAR, -days)
    val newDate = calendar.time
    calendar.time = newDate
//        String monthOfYear = new SimpleDateFormat(Constants.DateAndMonth.MONTH_FORMAT_NUM).format(calendar.getTime());//MMM for short Name(Jun)
//        String dayOfMonth = new SimpleDateFormat(Constants.DateAndMonth.DATE_OF_MONTH).format(calendar.getTime());//Date Number 01
//        String year = new SimpleDateFormat(Constants.DateAndMonth.YEAR).format(calendar.getTime());//year
    return SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd).format(calendar.time)
}

inline fun getCurrentDate(): String {
    val date = SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd)

    return date.format(Date())
}

fun changeDateFormatForTextView(dateString: String): String {
    // parse the String "29-07-2013" to a java.util.Date object
    val date = SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd).parse(dateString)
    // format the java.util.Date object to the desired format

    return SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd).format(date)
}

@Throws(ParseException::class)
inline fun changeDateFormatForMethods(dateString: String): String {

    // parse the String "29-07-2013" to a java.util.Date object //"dd MMM yyyy"
    val date = SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd, Locale.US).parse(dateString)
    // format the java.util.Date object to the desired format

    return SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd, Locale.US).format(date)
}

inline fun getLastMonthFirstDate(): String {
    var lastDate = ""
    val calendar = Calendar.getInstance()
    calendar.add(Calendar.MONTH, -1)
    calendar.add(Calendar.DAY_OF_MONTH, 1)
    val year = calendar.get(Calendar.YEAR)
    val month = calendar.get(Calendar.MONTH)
    val day = 1
    calendar.set(year, month, day)

    lastDate = SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd).format(calendar.time)

    return lastDate
}

inline fun getLastMonthLastDate(): String {
    var lastDate = ""
    val calendar = Calendar.getInstance()
    calendar.add(Calendar.MONTH, -1)
    calendar.add(Calendar.DAY_OF_MONTH, 1)
    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH))
    val year = calendar.get(Calendar.YEAR)
    val month = calendar.get(Calendar.MONTH)
    val day = calendar.get(Calendar.DAY_OF_MONTH)
    calendar.set(year, month, day)

    lastDate = SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd).format(calendar.time)

    return lastDate
}

fun changeDateFormat(time: String): String? {
    val inputPattern = ConstantsUtility.DateFormats.yyyyDASHmmDASHdd
    val outputPattern = ConstantsUtility.DateFormats.ddDASHMMDASHyyWithTime
    val inputFormat = SimpleDateFormat(inputPattern)
    val outputFormat = SimpleDateFormat(outputPattern)

    var date: Date? = null
    var str: String? = null

    try {
        date = inputFormat.parse(time)
        str = outputFormat.format(date)
    } catch (e: ParseException) {
        e.printStackTrace()
    }

    return str
}

@Throws(ParseException::class)
inline fun getDateAccordingToClientSide(dateString: String): String {

    // parse the String "29-07-2013" to a java.util.Date object
    val date = SimpleDateFormat(ConstantsUtility.DateFormats.ddDASHMMDASHyyWithTime).parse(dateString)

    // format the java.util.Date object to the desired format
    return SimpleDateFormat(ConstantsUtility.DateFormats.ddSLASHMMSLASHyy).format(date)
}