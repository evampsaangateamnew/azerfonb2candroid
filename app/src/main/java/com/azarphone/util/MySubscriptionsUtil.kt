package com.azarphone.util

import com.azarphone.ProjectApplication
import com.azarphone.validators.isNumeric
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * @author Junaid Hassan on 29, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
inline fun isMySubscriptionsRedValue(value: String): Boolean {
    return when {
        value.equals(ConstantsUtility.TariffConstants.VALUE_FREE_AZ, ignoreCase = true) -> true
        value.equals(ConstantsUtility.TariffConstants.VALUE_FREE_EN, ignoreCase = true) -> true
        value.equals(ConstantsUtility.TariffConstants.VALUE_FREE_RU, ignoreCase = true) -> true
        value.equals(ConstantsUtility.TariffConstants.VALUE_UNLIMITED_EN, ignoreCase = true) -> true
        value.equals(ConstantsUtility.TariffConstants.VALUE_UNLIMITED_AZ, ignoreCase = true) -> true
        value.equals(ConstantsUtility.TariffConstants.VALUE_UNLIMITED_RU, ignoreCase = true) -> true
        else -> false
    }
}//isMySubscriptionsRedValue ends

inline fun getDoubleFromStringForMySubscription(string: String): Double {
    var value = -1.0
    try {
        if (isNumeric(string)) {
            value = java.lang.Double.valueOf(string)
        }
    } catch (e: NumberFormatException) {
        logE("getDouble123", "error:::".plus(e.toString()), "MySubscriptionsUtil", "getDoubleFromStringForMySubscription")
    }

    return value
}//getDoubleFromStringForMySubscription ends

inline fun getMiliSecondsFromDateForMySubscription(myDate: String): Long {
    var miliSec: Long = 0
    try {
        val sdf = SimpleDateFormat(ConstantsUtility.DateFormats.ddDASHMMDASHyyWithTime)
        var date: Date? = null
        try {
            date = sdf.parse(myDate)
        } catch (e: ParseException) {
            logE("getMiliSeconds123", "error:::".plus(e.toString()), "MySubscriptionsUtil", "getMiliSecondsFromDateForMySubscription")
            return 0
        }

        miliSec = date!!.time
    } catch (e: Exception) {
        logE("getMiliSeconds324", "error:::".plus(e.toString()), "MySubscriptionsUtil", "getMiliSecondsFromDateForMySubscription")
    }

    return miliSec
}//getMiliSecondsFromDateForMySubscription ends

inline fun getOneDayMinusForMySubscription(startMili: Long): Long {
    var _startMili = startMili
    if (startMili > 0) {
        _startMili = (startMili - 8.64e+7).toLong()
    }
    return _startMili
}//getOneDayMinusForMySubscription ends

inline fun getOneHourMinusForMySubscription(startMili: Long): Long {
    var startMili = startMili
    if (startMili > 0) {
        startMili = (startMili - 3.6e+6).toLong()
    }
    return startMili
}

inline fun getHoursDifferenceBetweenMilisecondsForMySubscription(startDate: Long, endDate: Long): Int {

    val duration = endDate - startDate

    val diffInDays = TimeUnit.MILLISECONDS.toHours(duration)

    return diffInDays.toInt()
}

inline fun getCurrentMiliSecondsForMySubscription(): Long {
    return System.currentTimeMillis()
}//getCurrentMiliSecondsForMySubscription ends

inline fun getDaysDifferenceBetweenMilisecondsForMySubscription(startDate: Long, endDate: Long): Int {

    val duration = endDate - startDate

    val diffInDays = TimeUnit.MILLISECONDS.toDays(duration)

    return diffInDays.toInt()
}

inline fun getLocalizedDetailsTabName(): String {
    var message="Details"

    if(ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.EN, ignoreCase = true)){
        message="Details"
    }else if(ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.AZ, ignoreCase = true)){
        message="İzahlı"
    }else{
        message="Подробно"
    }

    return message
}