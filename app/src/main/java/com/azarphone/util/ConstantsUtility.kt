package com.azarphone.util

/**
@author Ehtisham haq
ConstantsUtility File to store all App Related Constants
 **/
object ConstantsUtility {

    object UserConstants {
        const val CALL_HOME_PAGE_API_KEY = "key.call.homepage.api"
        const val SCRATCH_CARD_LENGTH = 13
        const val USERNAME_PREFIX = "+994 "
        const val USER_NUMBER_LENGTH = 9
        const val USER_OTP_LENGTH = 4
        const val USER_TYPE_POSTPAID = "postpaid"
        const val USER_SUBSCRIBER_TYPE_PREPAID = "Prepaid"
        const val CUSTOMER_TYPE_FULL = "full"
        const val CORPORATE_CUSTOMER = "Corporate Customer"
        const val CUSTOMER_TYPE_INDIVIDUAL = "Individual Customer"
        const val CUSTOMER_TYPE_DATA_SIM_PREPAID = "Data Sim Prepaid"
        const val CUSTOMER_TYPE_DATA_SIM_POSTPAID = "Data Sim Postpaid"
        const val CUSTOMER_TYPE_DATA_SIM_POSTPAID_CORPORATE = "Data Sim Postpaid Corporate"
        const val CUSTOMER_TYPE_DATA_SIM_POSTPAID_INDIVIDUAL = "Data Sim Postpaid Individual"
    }//UserConstants ends

    object DashboardStatus {
        const val B01 = "b01"
        const val B04 = "b04"
    }//DashboardStatus ends


    object DataWithOutPack {
        const val ACTIVE = "active"
    }//DashboardStatus ends

    object ResourceTypeConstants {
        const val RESOURCE_TYPE_DATA = "DATA"
        const val RESOURCE_TYPE_INTERNET = "INTERNET"
        const val RESOURCE_TYPE_SMS = "SMS"
        const val RESOURCE_TYPE_VOICE = "VOICE"
        const val RESOURCE_TYPE_CALLS = "Calls"
        const val RESOURCE_TYPE_BONUSES = "BONUSES"
        const val RESOURCE_TYPE_ROAMING = "ROAMING"
    }//ResourceTypeConstants ends

    object MRCTypes {
        const val MRC_MONTHLY = "monthly"
        const val MRC_DAILY = "daily"
    }//MRCTypes ends

    object ForgetPasswordScreenSteps {
        const val ENTER_NUMBER_STEP = "forgot.enter.number.step"
        const val ENTER_PIN_STEP = "forgot.enter.pin.step"
        const val UPDATE_PASSWORD_STEP = "forgot.update.password.step"
    }//ForgetPasswordScreenSteps ends

    object SignupScreenSteps {
        const val ENTER_NUMBER_STEP = "signup.enter.number.step"
        const val ENTER_PIN_STEP = "signup.enter.pin.step"
        const val SET_PASSWORD_STEP = "signup.update.password.step"
    }//SignupScreenSteps ends

    object APIResponseErrorCodes {
        /**these are local error codes for android dev only*/
        const val NO_RESPONSE = 123
        const val API_FAILURE = 456
        const val FAILED_WITH_DESCRIPTION = 789
        const val FORCE_LOG_OUT = 777
    }//APIResponseErrorCodes ends

    object AnimationDurations {
        const val REVEAL = 500
    }//AnimationDurations ends

    object ServerResponseCodes {
        /**these are predefined error codes
         * from the server to process with*/
        const val FORCE_LOG_OUT = "7"
        const val SUCCESS = "00"
    }//ServerResponseCodes ends

    object BundleKeys {
        const val BUNDLE_TARIFFS_CARD_ITEMS_DATA = "tariffs.card.items.data"
        const val BUNDLE_FORGOT_PASSWORD_NUMBER = "forgot_password_number_bundle_fragment"
        const val BUNDLE_SIGN_UP_NUMBER = "sign_up_number_bundle_fragment"
        const val BUNDLE_SIGN_UP_OTP_PIN = "sign_up_otp_pin_bundle_fragment"
        const val BUNDLE_FROM_LOGIN_OR_SIGNUP_SCREEN_OR_SWITCH_NUMBER = "bundle.key.from.login.or.signup.or.switch.number"
        const val BUNDLE_FROM_SWITCH_NUMBER = "bundle.key.from.switch.number"
        const val BUNDLE_DONT_CALL_APP_RESUME = "bundle.data.call.appresume"
        const val BUNDLE_CALLED_FROM_SETTINGS = "bundle.data..called.from.settings"
        const val BUNDLE_CALLED_FROM_USER_PROFILE = "bundle.data..called.from.user.profile"
        const val BUNDLE_NOTIFICATIONS_ACTION_ID = "notifications.redirection.action.id.key.data"
    }//BundleKeys ends

    object APICauses {
        const val CAUSE_FORGOT_PASSWORD_ENTER_PIN = "forgotpassword"
        const val CAUSE_SIGN_UP = "signup"
        const val CAUSE_USAGE_HISTORY_DETAILS = "usagehistorydetails"
    }//APICauses ends

    object HorizontalMenuIds {
        const val ID_DASHBOARD = 1
        const val ID_TARIFFS = 2
        const val ID_SERVICES = 3
        const val ID_INTERNET = 4
        const val ID_TOPUP = 6
        const val ID_MYACCOUNT = 5
    }//HorizontalMenuIds ends

    object HorizontalMenuIdentifiers {
        const val IDENTIFIER_DASHBOARD = "dashboard"
        const val IDENTIFIER_TARIFFS = "tariffs"
        const val IDENTIFIER_SERVICES = "services"
        const val IDENTIFIER_TOPUP = "topup"
        const val IDENTIFIER_INTERNET = "Internet"
        const val IDENTIFIER_MYACCOUNT = "myaccount"
    }//HorizontalMenuIds ends

    object HorizontalChildMenuIdentifiers {
        const val IDENTIFIER_TOPUP = "topup_topup"
        const val IDENTIFIER_USAGE_HISTORY = "myaccount_usage_history"
        const val IDENTIFIER_LOAN = "topup_loan"
        const val IDENTIFIER_MONEY_TRANSFER = "topup_money_transfer"
        const val IDENTIFIER_MONEY_REQUEST = "topup_money_request"
        const val IDENTIFIER_OPERATIONS_HISTORY = "myaccount_operations_history"
        const val IDENTIFIER_FREE_SMS = "services_free_sms"
        const val IDENTIFIER_FNF = "services_fnf"
        const val IDENTIFIER_SUPPLEMENTARY_OFFERS = "services_supplementary_offers"
        const val IDENTIFIER_MY_SUBSCRIPTIONS = "myaccount_my_subscriptions"
        const val IDENTIFIER_VAS_SERVICES = "services_vas_services"
        const val IDENTIFIER_EXCHANGE_SERVICES = "services_exchange_service"
        const val IDENTIFIER_SPECIAL_SERVICES = "services_special_offers"
        const val IDENTIFIER_NAR_TV = "services_nar_tv"
    }

    object LanguageKeywords {
        const val EN = "en"
        const val RU = "ru"
        const val AZ = "az"
    }//LanguageKeywords ends

    object LiveChatLanguageConstants {
        const val EN = "0"
        const val AZ = "1"
        const val RU = "2"
    }//LanguageKeywords ends

    object LanguageKeywordConstants {
        const val EN = "3"
        const val RU = "2"
        const val AZ = "4"
    }//LanguageKeywordConstants ends

    object LanguageKeywordsForUI {
        const val EN = "English"
        const val RU = "Русский"
        const val AZ = "Azərbaycan"
    }//LanguageKeywordsForUI ends

    object VerticalMenuIdentifiers {
        const val IDENTIFIER_NOTIFICATIONS = "notifications"
        const val IDENTIFIER_STORE_LOCATOR = "store_locator"
        const val IDENTIFIER_FAQ = "faq"
        const val IDENTIFIER_CONTACT_US = "contact_us"
        const val IDENTIFIER_TERMS_AND_CONDITIONS = "terms_and_conditions"
        const val IDENTIFIER_LIVE_CHAT = "live_chat"
        const val IDENTIFIER_SETTINGS = "settings"
        const val IDENTIFIER_LOG_OUT = "logout"
        const val IDENTIFIER_TUTORIAL_AND_FAQ = "tutorial_and_faqs"
        const val DEFAULT_LOG_OUT_KEY = "logOutDefault"
        const val IDENTIFIER_MANAGE_ACCOUNT = "manage_account"
    }//VerticalMenuIdentifiers ends

    object DateFormats {
        const val LAST_SEVEN_DAY = 7
        const val LAST_14_DAYS = 14
        const val LAST_THIRTY_DAY = 30
        const val LAST_NINETY_DAY = 90
        const val CURRENT_DAY = 0
        const val ddDASHMMDASHyyWithTime = "yyyy-MM-dd HH:mm:ss"
        const val ddSLASHMMSLASHyy = "dd/MM/yy"
        const val yyyyDASHmmDASHdd = "yyyy-MM-dd"
        const val SAMPLE_DATE_FORMAT_DAY_NAME = "EEE dd-MM-yy"
    }//DateFormats

    object FCMNotificationConfigurations {
        const val FCM_NOTIFICATION_API_CAUSE_LOGIN = "login"
        const val FCM_NOTIFICATION_API_CAUSE_SETTINGS = "settings"

        const val FCM_NOTIFICATION_ON = "1"
        const val FCM_NOTIFICATION_OFF = "0"

        const val FCM_NOTIFICATION_TONE = "tone"
        const val FCM_NOTIFICATION_VIBRATE = "vibrate"
        const val FCM_NOTIFICATION_MUTE = "mute"
    }//FCMNotificationConfigurations ends


    object ServicesChildMenusIdentifiers {
        const val SERVICES_SPECIAL_OFFERS = "services_special_offers"
        const val SERVICES_SUPPLEMENTORY_OFFERS = "services_supplementary_offers"
        const val SERVICES_FREE_SMS = "services_free_sms"
        const val SERVICES_FNF = "services_fnf"
        const val SERVICES_VAS_SERVICES = "services_vas_services"
        const val SERVICES_EXCHANGE_SERVICES = "services_exchange_service"
        const val SERVICES_NAR_TV = "services_nar_tv"
        const val SERVICES_TOPUP = "service_topup"
        const val SERVICES_DATA_WITH_OUT_PACK =  "dataUsage_withoutPack"
    }//ServicesChildMenusIdentifiers ends

    object TopupChildMenusIdentifiers {
        const val TOPUP_TOPUP = "topup_topup"
        const val TOPUP_LOAN = "topup_loan"
        const val Fast_TOPUP = "topup_fasttopup"
        const val AUTO_PAYMENT = "topup_autopayment"
        const val TOPUP_MONEY_TRANSFER = "topup_money_transfer"
        const val TOPUP_MONEY_REQUEST = "topup_money_request"

    }//TopupChildMenusIdentifiers ends

    object MyAccountChildMenusIdentifiers {
        const val MYACCOUNT_USAGE_HISTORY = "myaccount_usage_history"
        const val MYACCOUNT_OPERATIONS_HISTORY = "myaccount_operations_history"
        const val MYACCOUNT_MY_SUBSCRIPTION = "myaccount_my_subscriptions"
    }//MyAccountChildMenusIdentifiers ends

    object ManageAccount {
        const val LOGIN_USECASE_TYPE_KEY = "login.usecase.type.key"
        const val LOGIN_USECASE_ADDACCOUNT = "login.add.account"
        const val LOGIN_USECASE_LOGIN = "login.login"
    }//ManageAccount ends

    object RequestCodeConstants {
        const val PICK_IMAGE_FROM_GALLERY = 14
        const val PICK_IMAGE_FROM_CAMERA = 15
    }//RequestCodeConstants ends

    object FreeSMSConstants {
        const val ALPHA_NUMARIC_WITH_SPACIFIC_SPACIAL_CHARS = "abcdefghijklmnopqrstuvwxyz1234567890  ~`!@#$%^&*()_+=-]}[{|'\";:/?.>,<"
        const val SMS_LENGTH_EN = 160
        const val SMS_LENGTH_AZ = 70
    }

    object PermissionConstants {
        const val EXTERNAL_STORAGE_FOR_GALLERY = 203
        const val FOR_CAMERA = 209
        const val FINGER_PRINT_SCANNER_PERMISSION_CODE = 103
    }//PermissionConstants ends

    object TariffConstants {
        const val NOTIFICATION_TO_TARIFF_ACTION_ID = "action.id.tari.f.data.ke.y"
        const val TARIFF_MIGRATION_PRICES_MESSAGE_DEFAULT_KEY = "default"
        const val PRICE_OFFERS_CURRENCY = "azn"
        const val ATTRIBUTE_LIST_UNIT = "manat"
        const val AZERI_SYMBOL = "\u20BC"
        const val VALUE_FREE_EN = "FREE"
        const val VALUE_FREE_RU = "БЕСПЛАТНО"
        const val VALUE_FREE_AZ = "PULSUZ"

        const val VALUE_UNLIMITED_EN = "Unlimited"
        const val VALUE_UNLIMITED_RU = "Безлимитный"
        const val VALUE_UNLIMITED_AZ = "Limitsiz"

        const val OFFERING_ID_BUNDLE_DATA = "key.offering.id.bundle.data.key"
        const val OFFERING_ID_BUNDLE_DATA_FRAGMENT = "key.offering.id.bundle.data.fragment.key"
        const val TARIFFS_PAGE_TITLE_DATA = "tariffs.data.page.title.data"

        const val CAN_NOT_SUBSCRIBE = "0"
        const val CAN_SUBSCRIBE = "1"
        const val ALREADY_SUBSCRIBED = "2"
        const val RENEWABLE = "3"

        const val PRICE_TEMPELATE_SINGLE_VALUE = "Single value"
        const val PRICE_TEMPELATE_DOUBLE_VALUE = "Double values"
        const val PRICE_TEMPELATE_DOUBLE_TITLE = "Double titles"

        const val TARIFF_REDIRECTION_INDEX = "tar.iff.redirection.index"
        const val TARIFF_REDIRECTION_OFFER_ID = "tariff.offer.id.data.key"
    }//TariffConstants ends

    object PackagesConstants {
        const val NOTIFICATIONS_SPECIAL_REDIRECTION_KEY = "notifi.red.special.irect.key"
        const val NOTIFICATIONS_REDIRECTION_KEY = "notifi.red.irect.key"
        const val TAB_TITLE = "tab.title.sup.offers"
        const val FOUNDED_OFFERS_ITEM_POS = "founded.offers.item.position"
        const val TAB_OFFERS = "tab.offers.supp.offers"
        const val INTERNET_TAB_OFFERS = "tab.offers.internet.offers"
        const val MYSUBSCRIPTIONS_DATA = "my.subscriptions.data.for.packages"
        const val SELECTED_ROAMING_COUNTRY_NAME = "selected.country.name.roaming"
        const val SELECTED_ROAMING_COUNTRY_FLAG = "selected.country.flag.roaming"
        const val SELECTED_ROAMING_FILTERS = "selected.country.filters.roaming.filters"
        const val KEYWORD_ROAMING = "key.roamingsOffer.supplementary"
        const val KEYWORD_ROAMING_OFFERS_DATA = "key.roamings.offers.data.supplementary"
        const val AZERI_SYMBOL = "\u20BC"
        const val OFFERS_FILTER = "offers.filters.supplementary"
        const val IS_FROM_ROAMING = "offers.from.roaming.supplementary"
        const val TM_AND_SPECIALS = "tmandspecials"
        const val DASHBOARD_INTENT_DATA = "key.dash.board.intent.data.for.tabs"
    }//SupplementaryOffersConstants ends

    object MySubscriptionsConstants {
        const val DASHBOARD_INTENT_DATA = "key.dash.board.intent.data.for.tabs"
        const val REDIRECTION_INTENT_DATA_MYSUBSCRIPTION = "key.dash.board.intent.data.for.tabs.mysubscription"
        const val IS_UNLIMITED = "free"
        const val AZERI_SYMBOL = "\u20BC"
        const val TAB_TITLE = "tab.title.subscription.my.data.title"
        const val OFFERING_ID = "tab.title.subscription.my.data.offeringid"
        const val TAB_OFFERS = "tab.subscriptions.offers.data.offers"
        const val TAB_POSITION = "tab.position"


    }//MySubscriptionsConstants ends

    object CoreServicesConstants {
        const val CALL_FORWARD_OFFERING_ID = "519637024"/*"1365050059"*/
        const val USAGE_WITHOUT_PACK_OFFERING_ID = "1483592628"
        const val STATUS_ACTIVE = "Active"
        const val STATUS_INACTIVE = "Inactive"
        const val AZERI_SYMBOL = "\u20BC"
        const val EN_FORWARD_NUMBER_NOT_SET_LABEL = "Not set"
        const val AZ_FORWARD_NUMBER_NOT_SET_LABEL = "Təyin olunmayıb"
        const val RU_FORWARD_NUMBER_NOT_SET_LABEL = "Не настроено"
        const val FREE_EN = "Free"
        const val FREE_RU = "Бесплатно"
        const val FREE_AZ = "Pulsuz"
        const val KEY_NUMBER_FORWARD_DATA = "key.number.forward.data"
    }//CoreServicesConstants ends

    object NotificationsConstants {
        const val ACTION_TYPE_SUPPLEMENTARY_OFFERS = "Supplementary"
        const val ACTION_TYPE_TARIFFS_OFFERS = "tariff"
    }

    object AutoPaymentConstants {
        const val BILLING_CYCLE_DAILY = "1"
        const val BILLING_CYCLE_WEEKLY = "2"
        const val BILLING_CYCLE_MONTHLY = "3"

        const val SAT = "Sat"
        const val SUN = "Sun"
        const val MON = "Mon"
        const val TUE = "Tue"
        const val WED = "Wed"
        const val THU = "Thu"
        const val FRI = "Fri"
    }

    object TopUpConstants {
        const val MOBILE_NUMBER = "MOBILE_NUMBER"
        const val AMOUNT = "AMOUNT"
    }

    object LiveChat {
        const val LICENCE_NUMBER = "12862467"
        const val PHONE_NUMBER = "Phone number"
    }

    object DynamicLinkConstants {
        //Tag Constant
        const val DYNAMIC_LINK_TAG = "DYNAMIC_LINK_TAG"

        const val BOTTOM_MENU = "BottomMenu"
        const val SCREEN_NAME = "Screen_Name"
        const val SUB_SCREEN = "SubScreen"
        const val STEP_ONE_SCREEN = "StepOneScreen"
        const val TAB_NAME = "TabName"
        const val OFFERING_ID = "OfferingId"

        //Data constants
        const val SCREEN_NAME_DATA = "dynamic.link.screen.name.data"
        const val SUB_SCREEN_NAME_DATA = "dynamic.link.sub.screen.name.data"
        const val STEP_ONE_SCREEN_NAME_DATA = "dynamic.link.step.one.screen.name.data"
        const val TAB_NAME_DATA = "dynamic_link_tab_name.data"
        const val OFFERING_ID_DATA = "dynamic_link_offering.id.data"
    }

    object InAppSurveyConstants {
        const val COMMENT_ENABLE = "enable"
        const val HOME_PAGE = "dashboard"
        const val TARIFF_PAGE = "tariff"
        const val TARIFF_SUBSCRIBED_PAGE = "tariff_subscribed"
        const val TOP_UP_PAGE = "topup"
        const val BUNDLE_PAGE = "bundle"
        const val BUNDLE_SUBSCRIBED_PAGE = "bundle_subscribed"
        const val LOAN_PAGE = "get_loan"
        const val FNF_PAGE = "fnf"
        const val TRANSFER_MONEY_PAGE = "transfer_money"
    }

}//ConstantsUtility ends

