package com.azarphone.util

import com.azarphone.api.pojo.response.customerdata.CustomerData
import com.azarphone.datamanagers.UserDataManager

inline fun getUpdatedCustomerDataLocaliy(oldCustomerData: CustomerData): CustomerData {
    UserDataManager.getCustomerData()!!.lastName = oldCustomerData.lastName
    UserDataManager.getCustomerData()!!.groupType = oldCustomerData.groupType
    UserDataManager.getCustomerData()!!.lateOnPopup = oldCustomerData.lateOnPopup
    UserDataManager.getCustomerData()!!.gender = oldCustomerData.gender
    UserDataManager.getCustomerData()!!.loyaltySegment = oldCustomerData.loyaltySegment
    UserDataManager.getCustomerData()!!.rateusAndroid = oldCustomerData.rateusAndroid
    UserDataManager.getCustomerData()!!.puk = oldCustomerData.puk
    UserDataManager.getCustomerData()!!.firstPopup = oldCustomerData.firstPopup
    UserDataManager.getCustomerData()!!.language = oldCustomerData.billingLanguage
    UserDataManager.getCustomerData()!!.title = oldCustomerData.popupTitle
    UserDataManager.getCustomerData()!!.expiryDate = oldCustomerData.expiryDate
    UserDataManager.getCustomerData()!!.customerStatus = oldCustomerData.customerStatus
    UserDataManager.getCustomerData()!!.popupContent = oldCustomerData.popupContent
    UserDataManager.getCustomerData()!!.customerType = oldCustomerData.customerType
    UserDataManager.getCustomerData()!!.offeringName = oldCustomerData.offeringName
    UserDataManager.getCustomerData()!!.pin = oldCustomerData.pin
    UserDataManager.getCustomerData()!!.sim = oldCustomerData.sim
    UserDataManager.getCustomerData()!!.imageURL = oldCustomerData.imageURL
    UserDataManager.getCustomerData()!!.customerId = oldCustomerData.customerId
    UserDataManager.getCustomerData()!!.offeringNameDisplay = oldCustomerData.offeringNameDisplay
    UserDataManager.getCustomerData()!!.msisdn = oldCustomerData.msisdn
    UserDataManager.getCustomerData()!!.email = oldCustomerData.email
    UserDataManager.getCustomerData()!!.brandName = oldCustomerData.brandName
    UserDataManager.getCustomerData()!!.rateusIos = oldCustomerData.rateusIos
    UserDataManager.getCustomerData()!!.subscriberType = oldCustomerData.subscriberType
    UserDataManager.getCustomerData()!!.hideNumberTariffIds = oldCustomerData.hideNumberTariffIds
    UserDataManager.getCustomerData()!!.entityId = oldCustomerData.entityId
    UserDataManager.getCustomerData()!!.offeringId = oldCustomerData.offeringId
    UserDataManager.getCustomerData()!!.token = oldCustomerData.token
    UserDataManager.getCustomerData()!!.firstName = oldCustomerData.firstName
    UserDataManager.getCustomerData()!!.accountId = oldCustomerData.accountId
    UserDataManager.getCustomerData()!!.billingLanguage = oldCustomerData.billingLanguage
    UserDataManager.getCustomerData()!!.dob = oldCustomerData.dob
    UserDataManager.getCustomerData()!!.popupTitle = oldCustomerData.popupTitle
    UserDataManager.getCustomerData()!!.brandId = oldCustomerData.brandId
    UserDataManager.getCustomerData()!!.statusDetails = oldCustomerData.statusDetails
    UserDataManager.getCustomerData()!!.middleName = oldCustomerData.middleName
    UserDataManager.getCustomerData()!!.magCustomerId = oldCustomerData.magCustomerId
    UserDataManager.getCustomerData()!!.effectiveDate = oldCustomerData.effectiveDate
    UserDataManager.getCustomerData()!!.status = oldCustomerData.customerStatus
    UserDataManager.getCustomerData()!!.specialOffersTariffData = oldCustomerData.specialOffersTariffData
    UserDataManager.getCustomerData()!!.groupIds = oldCustomerData.groupIds

    return UserDataManager.getCustomerData()!!
}