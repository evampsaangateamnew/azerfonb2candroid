package com.azarphone.util

import android.util.Log
import com.azarphone.BuildConfig

/**
 * Created by Junaid Hassan on 19, February, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */


   inline fun logE(key: String, logValue: String, fromClass: String, fromMethod: String) {
    if(BuildConfig.LOGS_ENABLED){
        Log.e(key, "logValue:::$logValue from Class:::$fromClass from Method:::$fromMethod")
    }

    }
