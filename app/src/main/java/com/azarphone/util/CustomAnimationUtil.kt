package com.azarphone.util

import android.view.View
import android.view.ViewAnimationUtils

/**
 * @author Junaid Hassan on 29, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
inline fun reveal(revealView: View, sourceView: View) {

    // get the final radius for the clipping circle
    val finalRadius = Math.hypot(revealView.width.toDouble(), revealView.height.toDouble()).toFloat()

    // create the animator for this view (the start radius is zero)
    val anim = ViewAnimationUtils.createCircularReveal(revealView,
            sourceView.right, sourceView.top, 0f, finalRadius)
    // make the view visible and start the animation
    revealView.visibility = View.VISIBLE
    anim.duration = ConstantsUtility.AnimationDurations.REVEAL.toLong()
    anim.start()
}