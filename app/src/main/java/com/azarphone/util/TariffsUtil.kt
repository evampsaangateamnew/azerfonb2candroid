package com.azarphone.util

import android.content.Context
import android.graphics.drawable.Drawable
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.validators.hasValue
import java.io.IOException
import java.io.InputStream

/**
 * @author Junaid Hassan on 12, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
inline fun isRedValue(value: String): Boolean {
    /*return when {
        value.equals(ConstantsUtility.TariffConstants.VALUE_FREE_AZ, ignoreCase = true) -> true
        value.equals(ConstantsUtility.TariffConstants.VALUE_FREE_EN, ignoreCase = true) -> true
        value.equals(ConstantsUtility.TariffConstants.VALUE_FREE_RU, ignoreCase = true) -> true
        value.equals(ConstantsUtility.TariffConstants.VALUE_UNLIMITED_EN, ignoreCase = true) -> true
        value.equals(ConstantsUtility.TariffConstants.VALUE_UNLIMITED_AZ, ignoreCase = true) -> true
        value.equals(ConstantsUtility.TariffConstants.VALUE_UNLIMITED_RU, ignoreCase = true) -> true
        else -> false
    }*/

    return false
}

inline fun getTariffMigrationMessageWithPricing(offeringId: String): String {
    var messageTitle = ""

    if (UserDataManager.getPredefineData() != null && !UserDataManager.getPredefineData()!!.tariffMigrationPrices.isNullOrEmpty()) {
        for (its in UserDataManager.getPredefineData()!!.tariffMigrationPrices!!.indices) {
            if (hasValue(UserDataManager.getPredefineData()!!.tariffMigrationPrices!![its]!!.key)) {
                if (offeringId == UserDataManager.getPredefineData()!!.tariffMigrationPrices!![its]!!.key) {
                    if (hasValue(UserDataManager.getPredefineData()!!.tariffMigrationPrices!![its]!!.value)) {
                        messageTitle = UserDataManager.getPredefineData()!!.tariffMigrationPrices!![its]!!.value!!
                    }
                    break
                }
            }
        }
    }

    /**check if the message title is found
     * if not found then pick the default message*/
    if (!hasValue(messageTitle)) {
        if (UserDataManager.getPredefineData() != null && !UserDataManager.getPredefineData()!!.tariffMigrationPrices.isNullOrEmpty()) {
            for (its in UserDataManager.getPredefineData()!!.tariffMigrationPrices!!.indices) {
                if (hasValue(UserDataManager.getPredefineData()!!.tariffMigrationPrices!![its]!!.key)) {
                    if (UserDataManager.getPredefineData()!!.tariffMigrationPrices!![its]!!.key
                                    .equals(ConstantsUtility.TariffConstants.TARIFF_MIGRATION_PRICES_MESSAGE_DEFAULT_KEY, ignoreCase = true)) {
                        if (hasValue(UserDataManager.getPredefineData()!!.tariffMigrationPrices!![its]!!.value)) {
                            messageTitle = UserDataManager.getPredefineData()!!.tariffMigrationPrices!![its]!!.value!!
                        }
                        break
                    }
                }
            }
        }
    }

    return if (hasValue(messageTitle)) {
        messageTitle
    } else {
        "Please Confirm.en"
    }
}//getTariffMigrationMessageWithPricing ends

inline fun getImageFromAssests(context: Context, imagePath: String): Drawable? {
    if (context == null) return null
    // load image
    var d: Drawable? = null
    if (context != null && hasValue(imagePath)) {
        try {
            if (context.assets == null) return null
            // get input stream
            val ims = context.assets.open("country_flags/$imagePath.png")
            // load image as Drawable
            d = Drawable.createFromStream(ims, null)
            // set image to ImageView
        } catch (ex: IOException) {

            var ims: InputStream? = null
            try {
                ims = context.assets.open("country_flags/" + "dummy_flag" + ".png")
            } catch (e: IOException) {
                /*Logger.debugLog(Logger.TAG_CATCH_LOGS, e.message)*/
            }

            // load image as Drawable
            d = Drawable.createFromStream(ims, null)

        }

    } else {
        var ims: InputStream? = null
        try {
            ims = context.assets.open("country_flags/" + "dummy_flag" + ".png")
        } catch (e: IOException) {
            /*Logger.debugLog(Logger.TAG_CATCH_LOGS, e.message)*/
        }

        // load image as Drawable
        d = Drawable.createFromStream(ims, null)

    }

    return d
}