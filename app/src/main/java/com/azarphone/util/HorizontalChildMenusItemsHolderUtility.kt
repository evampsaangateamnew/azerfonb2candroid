package com.azarphone.util

import android.util.Log
import com.azarphone.api.pojo.helperpojo.menus.HorizontalChildMenusHelperModel

/**
 * @author Junaid Hassan on 20, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */

/**this util is used to hold all the horizontal
 * menu child items arraylists
 * so that we can get the child menus/items for each horizontal menu item
 * the getter methods will be used to get each of the child items array list
 * for a specific horizontal menu*/
object HorizontalMenuItems {
    var servicesMenuModel: ArrayList<HorizontalChildMenusHelperModel>? = null
    var topupMenuModel: ArrayList<HorizontalChildMenusHelperModel>? = null
    var myaccountMenuModel: ArrayList<HorizontalChildMenusHelperModel>? = null
}//HorizontalMenuItems ends

inline fun getServicesMenuModel(): ArrayList<HorizontalChildMenusHelperModel>? {
    return HorizontalMenuItems.servicesMenuModel
}//getServicesMenuModel ends

inline fun getTopupMenuModel(): ArrayList<HorizontalChildMenusHelperModel>? {
    Log.e("top_","${HorizontalMenuItems.topupMenuModel?.size} -- inner")
    return HorizontalMenuItems.topupMenuModel
}//getTopupMenuModel ends

inline fun getMyAccountMenuModel(): ArrayList<HorizontalChildMenusHelperModel>? {
    return HorizontalMenuItems.myaccountMenuModel
}//getMyAccountMenuModel ends