package com.azarphone.util

class KeyValues private constructor() {
    companion object {

        private var instance: KeyValues? = null

        fun getInstance(): KeyValues {
            if (instance == null) {
                instance = KeyValues()
            }
            return instance as KeyValues
        }

    }

    // NDK Key Values
    var keyValueFromNdk: String = ""

    // NDK Public Keys From Server
    var keysPublicServerFromNdk: ArrayList<String> = ArrayList()

}