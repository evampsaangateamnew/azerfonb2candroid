package com.azarphone.util

import androidx.databinding.BindingAdapter
import android.widget.ImageView
import com.azarphone.R

@BindingAdapter("bind:identifier")
fun loadImage(view: ImageView, identifierValue: String?) {

    when (identifierValue) {

        ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_SPECIAL_OFFERS -> {
            view.setImageResource(R.drawable.ic_special_offers)
        }

        ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_VAS_SERVICES -> {
//            view.setImageResource(R.drawable.vas_services_icon)
        }

        ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_SUPPLEMENTORY_OFFERS -> {

            view.setImageResource(R.drawable.ic_special_offers)
        }

        ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_EXCHANGE_SERVICES -> {
//            view.setImageResource(R.drawable.exchange_service_icon)
        }

        ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_FNF -> {

            view.setImageResource(R.drawable.ic_fnf)
        }
        ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_NAR_TV -> {

            view.setImageResource(R.drawable.ic_nar_tv)
        }

        ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_FREE_SMS -> {
            view.setImageResource(R.drawable.ic_free_sms)
        }

        ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_TOPUP -> {
            view.setImageResource(R.drawable.ic_top_up)
        }

        ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_LOAN -> {
            view.setImageResource(R.drawable.ic_loan)
        }

        ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_MONEY_TRANSFER -> {
            view.setImageResource(R.drawable.ic_money_transfer)
        }



        ConstantsUtility.MyAccountChildMenusIdentifiers.MYACCOUNT_MY_SUBSCRIPTION -> {
            view.setImageResource(R.drawable.ic_my_subscriptions)
        }

        ConstantsUtility.MyAccountChildMenusIdentifiers.MYACCOUNT_OPERATIONS_HISTORY -> {
            view.setImageResource(R.drawable.ic_operations_history)
        }

        ConstantsUtility.MyAccountChildMenusIdentifiers.MYACCOUNT_USAGE_HISTORY -> {
            view.setImageResource(R.drawable.ic_usage_history)
        }

    }

}
