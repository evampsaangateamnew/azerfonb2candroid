package com.azarphone.util

import android.content.Context
import android.net.ConnectivityManager

/**
 * @author Junaid Hassan on 11, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
inline fun isNetworkAvailable(context: Context): Boolean {
    try {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        // test for connection
        return (cm.activeNetworkInfo != null && cm.activeNetworkInfo!!.isAvailable
                && cm.activeNetworkInfo!!.isConnected)
    } catch (ex: Exception) {
        logE("InternetConnectivityCheckerTool", ex.toString(), "InternetConnectivityCheckerTool", "isNetworkAvailable")
    }//catch ends

    return false
}//isnetworkavailable ends