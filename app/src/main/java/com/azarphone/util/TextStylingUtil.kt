@file:Suppress("DEPRECATION")

package com.azarphone.util

import android.content.Context
import android.graphics.Paint
import android.graphics.Typeface
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.TextPaint
import android.text.TextUtils
import android.text.style.AbsoluteSizeSpan
import android.text.style.MetricAffectingSpan
import android.text.style.SuperscriptSpan
import android.text.style.TypefaceSpan
import android.util.TypedValue
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.google.android.material.textfield.TextInputLayout


/**
 * @author Junaid Hassan on 03, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */

inline fun setHintColor(view: TextInputLayout, color: Int, context: Context) {
    view.defaultHintTextColor = context.resources.getColorStateList(color)
}

inline fun disableEditText(editText: EditText, color: Int, context: Context) {
    editText.isEnabled = false
    editText.isClickable = false
    editText.setTextColor(context.resources.getColor(color))
}

inline fun enableEditText(editText: EditText, color: Int, context: Context) {
    editText.isEnabled = true
    editText.isClickable = true
    editText.setTextColor(context.resources.getColor(color))
}

inline fun setMenuFont(context: Context, textView: TextView) {
    textView.typeface = getALSBoldFont()
    //set the text color
    textView.setTextColor(context.resources.getColor(R.color.purplish_brown))
    textView.textSize = context.resources.getDimension(R.dimen._8ssp)
}

inline fun setSelectedTabFont(context: Context, textView: TextView) {
    textView.typeface = getALSBoldFont()
    //set the text color
    textView.setTextColor(context.resources.getColor(R.color.colorTabSelectedText))
    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.resources.getDimension(R.dimen._12ssp))
}

inline fun setUnSelectedTabFont(context: Context, textView: TextView) {
    textView.typeface = getALSNormalFont()
    //set the text color
    textView.setTextColor(context.resources.getColor(R.color.colorTabTextNormal))
    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.resources.getDimension(R.dimen._12ssp))
}

inline fun setSelectedAutoPaymentTabFont(context: Context, textView: TextView) {
    textView.typeface = getALSBoldFont()
    //set the text color
    textView.setTextColor(context.resources.getColor(R.color.colorTabSelectedText))
    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.resources.getDimension(R.dimen._12ssp))

    textView.ellipsize = TextUtils.TruncateAt.MARQUEE
    textView.isSelected = true
    textView.marqueeRepeatLimit = -1
    textView.isSingleLine = true
    textView.layoutParams = LinearLayout.LayoutParams(
        LinearLayout.LayoutParams.MATCH_PARENT,
        LinearLayout.LayoutParams.WRAP_CONTENT
    )

}

inline fun setUnSelectedAutoPaymentTabFont(context: Context, textView: TextView) {
    textView.typeface = getALSNormalFont()
    //set the text color
    textView.setTextColor(context.resources.getColor(R.color.colorTabTextNormal))
    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.resources.getDimension(R.dimen._12ssp))

    textView.ellipsize = TextUtils.TruncateAt.MARQUEE
    textView.isSelected = true
    textView.marqueeRepeatLimit = -1
    textView.isSingleLine = true
    textView.layoutParams = LinearLayout.LayoutParams(
        LinearLayout.LayoutParams.MATCH_PARENT,
        LinearLayout.LayoutParams.WRAP_CONTENT
    )

}

inline fun getALSNormalFont(): Typeface {
    return RootValues.getALSNormalFont()
}

inline fun getAzeriSymbolFont(): Typeface {
    return RootValues.getAzeriSymbolFont()
}

inline fun getALSBoldFont(): Typeface {
    return RootValues.getALSBoldFont()
}

inline fun getManatConcatedString(context: Context, oldValue: String, manat: String, manatTopRatio: Float, dimenTextSize: Int): SpannableStringBuilder {
    var newValue = ""
    newValue = oldValue
    val cs = SpannableStringBuilder(newValue.plus(manat))
    cs.setSpan(CustomCharacterSpan(-manatTopRatio
            /**margin from top*/), cs.indexOf(manat), cs.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
    cs.setSpan(SuperscriptSpan(), cs.indexOf(manat), cs.length, 0)
    cs.setSpan(AbsoluteSizeSpan(context.resources.getDimensionPixelSize(dimenTextSize)), cs.indexOf(manat), cs.length, 0)

    cs.setSpan(CustomTypefaceSpan("", getAzeriSymbolFont()), cs.indexOf(manat), cs.length, Spanned.SPAN_EXCLUSIVE_INCLUSIVE)

    return cs
}

class CustomTypefaceSpan(family: String, typeface: Typeface) : TypefaceSpan(family) {
    private var newType: Typeface? = typeface

    override fun updateMeasureState(paint: TextPaint) {
        super.updateMeasureState(paint)
        applyCustomTypeFace(paint, newType!!)
    }

    override fun updateDrawState(ds: TextPaint) {
        super.updateDrawState(ds)
        applyCustomTypeFace(ds, newType!!)
    }

    private fun applyCustomTypeFace(paint: Paint, tf: Typeface) {
        val oldStyle: Int
        val old = paint.typeface
        oldStyle = old?.style ?: 0

        val fake = oldStyle and tf.style.inv()
        if (fake and Typeface.BOLD != 0) {
            paint.isFakeBoldText = true
        }

        if (fake and Typeface.ITALIC != 0) {
            paint.textSkewX = -0.25f
        }

        paint.typeface = tf
    }
}

class CustomCharacterSpan(private var proportion: Float) : MetricAffectingSpan() {

    override fun updateDrawState(textPaint: TextPaint) {
        textPaint.baselineShift += (textPaint.ascent() * proportion).toInt()
    }

    override fun updateMeasureState(textPaint: TextPaint) {
        textPaint.baselineShift += (textPaint.ascent() * proportion).toInt()
    }
}

inline fun checkIfTextContainOtherThenEnglishCharacters(context: Context?, text: String?): Boolean {
    var stringText = text
    var isOtherLangText = false
    if (context == null) return isOtherLangText
    if (stringText != null) {
        stringText = stringText.toLowerCase()
        for (i in 0 until stringText.length) {
            if (!ConstantsUtility.FreeSMSConstants.ALPHA_NUMARIC_WITH_SPACIFIC_SPACIAL_CHARS.contains(stringText[i] + "")) {
                isOtherLangText = true
                break
            }
        }
    }
    return isOtherLangText
}

inline fun getNoDataFoundOnSearchMessage(): String {
    return when {
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.EN, ignoreCase = true) -> "Nothing found, please try other keyword."
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.AZ, ignoreCase = true) -> "Məlumat tapılmadı. Zəhmət olmasa axtarış üçün başqa açar söz daxil et."
        else -> "Ничего не найдено. Пожалуйста введи другое слово для поиска."
    }
}

inline fun getNoDataFoundSimpleMessage(): String {
    return when {
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.EN, ignoreCase = true) -> "No data"
        ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.AZ, ignoreCase = true) -> "Məlumat yoxdur"
        else -> "Нет информации"
    }
}