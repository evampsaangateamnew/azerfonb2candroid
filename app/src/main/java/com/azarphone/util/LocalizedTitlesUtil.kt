package com.azarphone.util

import com.azarphone.ProjectApplication

inline fun getHardcodedNoResultDescFromServer(): String {
    return when {
        ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "Operation completed successfully."
        ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Əməliyyat uğurla başa çatdı."
        else -> "Операция успешно завершена."
    }
}