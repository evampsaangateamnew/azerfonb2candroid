package com.azarphone.ui.dialogs

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Html
import android.text.Spanned
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.CheckBox
import androidx.databinding.DataBindingUtil
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.response.inappsurvey.Questions
import com.azarphone.api.pojo.response.inappsurvey.Surveys
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.databinding.DialogSurveyBinding
import com.azarphone.eventhandler.OnClickSurveySubmit
import com.azarphone.eventhandler.UpdateListOnSuccessfullySurvey
import com.azarphone.util.ConstantsUtility

class InAppFeedbackDialog(context: Context, private val onClickSurveySubmit: OnClickSurveySubmit) :
    Dialog(context) {


    private lateinit var binding: DialogSurveyBinding
    private val selectedLanguage = ProjectApplication.mLocaleManager.getLanguage()
    private var question: Questions? = null
    private var answerId: Int = -1
    private var surveyId: Int = -1
    private var tariffId: String = ""
    private var offeringType = ""
    private var isCommentRequired = true
    private var onTransactionComplete = ""
    private var updateListOnSuccessfullySurvey: UpdateListOnSuccessfullySurvey? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setCancelable(false)

        binding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.dialog_survey,
            null,
            false
        )

        binding.apply {
            star1.setOnClickListener { select(it as CheckBox) }
            star2.setOnClickListener { select(it as CheckBox) }
            star3.setOnClickListener { select(it as CheckBox) }
            star4.setOnClickListener { select(it as CheckBox) }
            star5.setOnClickListener { select(it as CheckBox) }
            closeButton.setOnClickListener { dismiss() }
            submitFeedback.setOnClickListener { submitFeedback() }
        }
        setContentView(binding.root)
    }

    fun setUpdateListOnSuccessfullySurveyListener(updateListOnSuccessfullySurvey: UpdateListOnSuccessfullySurvey) {
        this.updateListOnSuccessfullySurvey = updateListOnSuccessfullySurvey
    }

    private fun submitFeedback() {
        val comment = binding.feedbackText.text.toString()
        val uploadSurvey = UploadSurvey(
            comment,
            answerId.toString(),
            question?.id?.toString() ?: "-1",
            tariffId, offeringType, surveyId.toString()
        )
        onClickSurveySubmit.onClickSubmit(uploadSurvey, onTransactionComplete,updateListOnSuccessfullySurvey)

    }

    fun showNormalDialog(survey: Surveys) {
        show()
        onTransactionComplete = getOnTransactionComplete(survey) ?: ""
        this.surveyId = survey.id
        this.question = survey.questions?.get(0)
        binding.apply {
            questionText = getQuestion()
            topUpGroup.visibility = View.GONE
            noteText.gravity = Gravity.CENTER
            if (survey.commentEnable == ConstantsUtility.InAppSurveyConstants.COMMENT_ENABLE) {
                feedbackText.hint = getHint(survey)
            } else {
                isCommentRequired = false
            }
        }

    }

    fun showMessageWithBalanceDialog(survey: Surveys, balance: String, message: String) {
        show()
        onTransactionComplete = getOnTransactionComplete(survey) ?: ""
        this.surveyId = survey.id
        this.question = survey.questions?.get(0)
        binding.apply {
            balanceValue = context.getString(R.string.getBalance, balance)
            transactionValue = Html.fromHtml(message)
            topUpGroup.visibility = View.VISIBLE
            questionText = getQuestion()
            if (survey.commentEnable == ConstantsUtility.InAppSurveyConstants.COMMENT_ENABLE) {
                feedbackText.hint = getHint(survey)
            } else {
                isCommentRequired = false
            }
        }
    }

    fun showSpannableMessageDialog(survey: Surveys, balance: String, message: Spanned) {
        show()
        onTransactionComplete = getOnTransactionComplete(survey) ?: ""
        this.surveyId = survey.id
        this.question = survey.questions?.get(0)
        binding.apply {
            balanceValue = context.getString(R.string.getBalance, balance)
            transactionValue = message
            topUpGroup.visibility = View.VISIBLE
            questionText = getQuestion()
            if (survey.commentEnable == ConstantsUtility.InAppSurveyConstants.COMMENT_ENABLE) {
                feedbackText.hint = getHint(survey)
            } else {
                isCommentRequired = false
            }
        }
    }

    fun showTariffDialog(survey: Surveys, offeringType: String, tariffId: String, rate: Int) {
        show()
        onTransactionComplete = getOnTransactionComplete(survey) ?: ""
        this.surveyId = survey.id
        this.tariffId = tariffId
        this.offeringType = offeringType
        this.question = survey.questions?.get(0)
        binding.apply {
            topUpGroup.visibility = View.GONE
            questionText = getQuestion()
            if (survey.commentEnable == ConstantsUtility.InAppSurveyConstants.COMMENT_ENABLE) {
                feedbackText.hint = getHint(survey)
            } else {
                isCommentRequired = false
            }
            setRating(rate)
        }

    }

    fun showTariffTitleWithMessageDialog(
        survey: Surveys,
        title: String,
        message: String,
        tariffId: String,
        offeringType: String
    ) {
        show()
        onTransactionComplete = getOnTransactionComplete(survey) ?: ""
        this.surveyId = survey.id
        this.question = survey.questions?.get(0)
        this.tariffId = tariffId
        this.offeringType = offeringType
        binding.apply {
            noteText.text = title
            transactionValue = Html.fromHtml(message)
            topUpGroup.visibility = View.VISIBLE
            questionText = getQuestion()
            balance.visibility = View.GONE
            balanceSign.text = ""
            if (survey.commentEnable == ConstantsUtility.InAppSurveyConstants.COMMENT_ENABLE) {
                feedbackText.hint = getHint(survey)
            } else {
                isCommentRequired = false
            }
        }

    }

    fun showTitleWithMessageDialog(survey: Surveys, title: String, message: String) {
        show()
        onTransactionComplete = getOnTransactionComplete(survey) ?: ""
        this.surveyId = survey.id
        this.question = survey.questions?.get(0)
        binding.apply {

            transactionValue = Html.fromHtml(message)
            topUpGroup.visibility = View.VISIBLE
            questionText = getQuestion()
            balance.visibility = View.GONE
            balanceSign.text = ""
            if (survey.commentEnable == ConstantsUtility.InAppSurveyConstants.COMMENT_ENABLE) {
                feedbackText.hint = getHint(survey)
            } else {
                isCommentRequired = false
            }
        }
    }

    private fun getOnTransactionComplete(survey: Surveys): String? {

        return when {
            selectedLanguage.equals(
                ConstantsUtility.LanguageKeywords.EN,
                ignoreCase = true
            ) -> {
                survey.onTransactionCompleteEn
            }
            selectedLanguage.equals(
                ConstantsUtility.LanguageKeywords.AZ,
                ignoreCase = true
            ) -> {
                survey.onTransactionCompleteAz
            }
            selectedLanguage.equals(
                ConstantsUtility.LanguageKeywords.RU,
                ignoreCase = true
            ) -> {
                survey.onTransactionCompleteRu
            }
            else -> {
                survey.onTransactionCompleteEn
            }
        }
    }

    private fun getQuestion(): String? {

        return when {
            selectedLanguage.equals(
                ConstantsUtility.LanguageKeywords.EN,
                ignoreCase = true
            ) -> {
                question?.questionTextEN
            }
            selectedLanguage.equals(
                ConstantsUtility.LanguageKeywords.AZ,
                ignoreCase = true
            ) -> {
                question?.questionTextAZ
            }
            selectedLanguage.equals(
                ConstantsUtility.LanguageKeywords.RU,
                ignoreCase = true
            ) -> {
                question?.questionTextRU
            }
            else -> {
                question?.questionTextEN
            }
        }
    }

    private fun getHint(survey: Surveys): String? {
        return when {
            selectedLanguage.equals(
                ConstantsUtility.LanguageKeywords.EN,
                ignoreCase = true
            ) -> {
                survey.commentsTitleEn
            }
            selectedLanguage.equals(
                ConstantsUtility.LanguageKeywords.AZ,
                ignoreCase = true
            ) -> {
                survey.commentsTitleAz
            }
            selectedLanguage.equals(
                ConstantsUtility.LanguageKeywords.RU,
                ignoreCase = true
            ) -> {
                survey.commentsTitleRu
            }
            else -> {
                survey.commentsTitleEn
            }
        }
    }

    private fun select(button: CheckBox) {
        if (isCommentRequired) {
            binding.feedbackText.visibility = View.VISIBLE
        } else {
            binding.feedbackText.visibility = View.GONE
        }
        binding.answerFeedbackType.visibility = View.VISIBLE
        binding.submitFeedback.visibility = View.VISIBLE

        when (button.isChecked) {
            true -> {
                rateSelectStar(button)
            }
            else -> {
                rateUnSelectStar(button)
            }
        }
    }

    private fun rateUnSelectStar(button: View) {
        when (button.id) {
            R.id.star1 -> {
                binding.star1.isChecked = true
                binding.star2.isChecked = false
                binding.star3.isChecked = false
                binding.star4.isChecked = false
                binding.star5.isChecked = false
                binding.feedback = getAnswer(0)
            }
            R.id.star2 -> {
                binding.star1.isChecked = true
                binding.star2.isChecked = true
                binding.star3.isChecked = false
                binding.star4.isChecked = false
                binding.star5.isChecked = false
                binding.feedback = getAnswer(1)
            }
            R.id.star3 -> {
                binding.star1.isChecked = true
                binding.star2.isChecked = true
                binding.star3.isChecked = true
                binding.star4.isChecked = false
                binding.star5.isChecked = false
                binding.feedback = getAnswer(2)
            }
            R.id.star4 -> {
                binding.star1.isChecked = true
                binding.star2.isChecked = true
                binding.star3.isChecked = true
                binding.star4.isChecked = true
                binding.star5.isChecked = false
                binding.feedback = getAnswer(3)
            }
            R.id.star5 -> {
                binding.star1.isChecked = true
                binding.star2.isChecked = true
                binding.star3.isChecked = true
                binding.star4.isChecked = true
                binding.star5.isChecked = true
                binding.feedback = getAnswer(4)
            }

        }
    }

    private fun setRating(rate: Int) {
        when (rate) {
            1 -> rateSelectStar(binding.star1)
            2 -> rateSelectStar(binding.star2)
            3 -> rateSelectStar(binding.star3)
            4 -> rateSelectStar(binding.star4)
            5 -> rateSelectStar(binding.star5)
        }
    }

    private fun rateSelectStar(button: View) {
        when (button.id) {
            R.id.star1 -> {
                binding.star1.isChecked = true
                binding.star2.isChecked = false
                binding.star3.isChecked = false
                binding.star4.isChecked = false
                binding.star5.isChecked = false
                binding.feedback = getAnswer(0)
            }
            R.id.star2 -> {
                binding.star1.isChecked = true
                binding.star2.isChecked = true
                binding.star3.isChecked = false
                binding.star4.isChecked = false
                binding.star5.isChecked = false
                binding.feedback = getAnswer(1)
            }
            R.id.star3 -> {
                binding.star1.isChecked = true
                binding.star2.isChecked = true
                binding.star3.isChecked = true
                binding.star4.isChecked = false
                binding.star5.isChecked = false
                binding.feedback = getAnswer(2)
            }
            R.id.star4 -> {
                binding.star1.isChecked = true
                binding.star2.isChecked = true
                binding.star3.isChecked = true
                binding.star4.isChecked = true
                binding.star5.isChecked = false
                binding.feedback = getAnswer(3)
            }
            R.id.star5 -> {
                binding.star1.isChecked = true
                binding.star2.isChecked = true
                binding.star3.isChecked = true
                binding.star4.isChecked = true
                binding.star5.isChecked = true
                binding.feedback = getAnswer(4)
            }

        }
    }

    private fun getAnswer(index: Int): String? {
        answerId = question?.answers?.get(index)?.id ?: -1
        return when {
            selectedLanguage.equals(
                ConstantsUtility.LanguageKeywords.EN,
                ignoreCase = true
            ) -> {
                question?.answers?.get(index)?.answerTextEN
            }
            selectedLanguage.equals(
                ConstantsUtility.LanguageKeywords.AZ,
                ignoreCase = true
            ) -> {
                question?.answers?.get(index)?.answerTextAZ
            }
            selectedLanguage.equals(
                ConstantsUtility.LanguageKeywords.RU,
                ignoreCase = true
            ) -> {
                question?.answers?.get(index)?.answerTextRU
            }
            else -> {
                question?.answers?.get(index)?.answerTextEN
            }
        }
    }
}