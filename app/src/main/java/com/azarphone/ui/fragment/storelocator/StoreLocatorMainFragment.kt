package com.azarphone.ui.fragment.storelocator

import android.os.Bundle
import android.view.View
import com.azarphone.R
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.LayoutStoreLocatorMainFragmentBinding
import com.azarphone.ui.activities.mainactivity.MainFactory
import com.azarphone.ui.activities.mainactivity.MainInjection
import com.azarphone.ui.activities.mainactivity.MainViewModel
import com.azarphone.ui.adapters.pagers.StoreLocatorPagerAdapter
import android.view.ViewGroup


/**
 * @author Junaid Hassan on 26, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
/*
class StoreLocatorMainFragment : BaseFragment<LayoutStoreLocatorMainFragmentBinding, MainFactory, MainViewModel>() {

    private val fromClass = "StoreLocatorMainFragment"

    private lateinit var mPagerAdapter: StoreLocatorPagerAdapter

    override fun getLayoutId(): Int {
        return R.layout.layout_store_locator_main_fragment
    }

    override fun getViewModelClass(): Class<MainViewModel> {
        return MainViewModel::class.java
    }

    override fun getFactory(): MainFactory {
        return MainInjection.provideMainFactory()
    }

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        if (activity != null && !activity!!.isFinishing) {
            mViewModel.getStoreLocator()

            mPagerAdapter = StoreLocatorPagerAdapter(context!!, childFragmentManager)
            mFragmentViewDataBinding.viewpager.apply {
                adapter = mPagerAdapter
            }
            setupTabView()
        }
    }

    fun setupTabView() {
        mFragmentViewDataBinding.slidingTabs.setupWithViewPager(mFragmentViewDataBinding.viewpager)
//        for (i in 0 until mFragmentViewDataBinding.slidingTabs.getTabCount()) {
//            mFragmentViewDataBinding.slidingTabs.getTabAt(i)!!.setCustomView(R.layout.customer_map_tab)
////            val tab_name = tabLayout.getTabAt(i).getCustomView().findViewById(R.id.txt_tab_name) as TextView
////            tab_name.text = "" + tabNames[i]
//        }

        for (i in 0 until mFragmentViewDataBinding.slidingTabs.tabCount) {
            val tab = (mFragmentViewDataBinding.slidingTabs.getChildAt(0) as ViewGroup).getChildAt(i)
            val p = tab.layoutParams as ViewGroup.MarginLayoutParams
            p.setMargins(16, 0, 16, 0)
            tab.requestLayout()
        }

    }


    override fun onInVisible() {
    }

}*/
