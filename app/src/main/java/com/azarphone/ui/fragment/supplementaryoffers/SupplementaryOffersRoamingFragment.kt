package com.azarphone.ui.fragment.supplementaryoffers

import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.ArrayAdapter
import android.widget.TextView
import com.azarphone.R
import com.azarphone.api.pojo.response.supplementaryoffers.helper.RoamingsOffer
import com.azarphone.api.pojo.response.supplementaryoffers.response.Countries
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.LayoutPackagesRoamingFragmentBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.RoamingSpinnerAdapterEvents
import com.azarphone.ui.activities.roamingoffers.RoamingCountryActivity
import com.azarphone.ui.activities.supplementaryoffers.SupplementaryOffersFactory
import com.azarphone.ui.activities.supplementaryoffers.SupplementaryOffersInjection
import com.azarphone.ui.activities.supplementaryoffers.SupplementaryOffersViewModel
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.hideKeyboard
import com.azarphone.util.logE
import com.azarphone.validators.hasValue
import com.azarphone.widgets.popups.showMessageDialog
import kotlinx.android.synthetic.main.layout_packages_roaming_fragment.*
import kotlinx.android.synthetic.main.layout_packages_roaming_fragment.view.*

/**
 * @author Junaid Hassan on 24, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class SupplementaryOffersRoamingFragment : BaseFragment<LayoutPackagesRoamingFragmentBinding, SupplementaryOffersFactory, SupplementaryOffersViewModel>(), RoamingSpinnerAdapterEvents {

    private lateinit var externalView: View
    private val fromClass = "SupplementaryOffersRoamingFragment"
    private var roaming: RoamingsOffer? = null

    companion object {
        fun getInstance(roaming: RoamingsOffer): SupplementaryOffersRoamingFragment {
            val fragment = SupplementaryOffersRoamingFragment()
            val bundle = Bundle()
            bundle.putParcelable(ConstantsUtility.PackagesConstants.KEYWORD_ROAMING, roaming)
            fragment.arguments = bundle
            return fragment
        }//getInstance ends
    }//companion object ends

    override fun getLayoutId(): Int {
        return R.layout.layout_packages_roaming_fragment
    }//getLayoutId ends

    override fun getViewModelClass(): Class<SupplementaryOffersViewModel> {
        return SupplementaryOffersViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): SupplementaryOffersFactory {
        return SupplementaryOffersInjection.provideSupplementaryOffersFactory()
    }//getFactory ends

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }//isBindedToActivityLifeCycle ends

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        if (activity != null && !activity!!.isFinishing) {
            externalView = view
            initFragmentData()
            initUI()
            initUIEvents()
        }
    }//onVisible ends

    override fun onCountrySelectedForRoaming(countrySelected: String, countryFlagSelected: String) {
        logE("selectedCountry", "country:::".plus(countrySelected).plus(" flag:::").plus(countryFlagSelected), fromClass, "selectingCountryFromDropDown")
    }//onCountrySelectedForRoaming ends

    private fun initUIEvents() {
        externalView.nextButton.setOnClickListener {
            if (roaming != null) {
                if (roaming!!.offers != null && !roaming!!.offers!!.isEmpty()) {
                    /**get the flag code from the country name*/
                    if (hasValue(externalView.roamingDropDownMessageTile.text.toString())) {
                        val selectedCountryFlag = getSelectedCountryFlagCode(externalView.roamingDropDownMessageTile.text.toString())
                        if (hasValue(selectedCountryFlag)) {
                            hideKeyboard(activity!!)
                            gotoRoamingActivity(externalView.roamingDropDownMessageTile.text.toString(), selectedCountryFlag)
                        }
                    } else {
                        showMessageDialog(context!!, activity!!, context!!.resources.getString(R.string.popup_error_title),
                                context!!.resources.getString(R.string.supplementary_offers_roaming_no_offers_found_lbl))
                    }
                }else {
                    showMessageDialog(context!!, activity!!, context!!.resources.getString(R.string.popup_error_title),
                            context!!.resources.getString(R.string.supplementary_offers_roaming_no_offers_found_lbl))
                }
            } else {
                showMessageDialog(context!!, activity!!, context!!.resources.getString(R.string.popup_error_title),
                        context!!.resources.getString(R.string.supplementary_offers_roaming_no_offers_found_lbl))
            }
        }//externalView.nextButton.setOnClickListener ends

        roamingDropDownMessageTile.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (roaming != null) {
                        if (roaming!!.offers != null && !roaming!!.offers!!.isEmpty()) {
                            /**get the flag code from the country name*/
                            if (hasValue(externalView.roamingDropDownMessageTile.text.toString())) {
                                val selectedCountryFlag = getSelectedCountryFlagCode(externalView.roamingDropDownMessageTile.text.toString())
                                if (hasValue(selectedCountryFlag)) {
                                    hideKeyboard(activity!!)
                                    gotoRoamingActivity(externalView.roamingDropDownMessageTile.text.toString(), selectedCountryFlag)
                                }
                            } else {
                                showMessageDialog(context!!, activity!!, context!!.resources.getString(R.string.popup_error_title),
                                        context!!.resources.getString(R.string.supplementary_offers_roaming_no_offers_found_lbl))
                                return false
                            }
                        }else {
                            showMessageDialog(context!!, activity!!, context!!.resources.getString(R.string.popup_error_title),
                                    context!!.resources.getString(R.string.supplementary_offers_roaming_no_offers_found_lbl))
                            return false
                        }
                    }else {
                        showMessageDialog(context!!, activity!!, context!!.resources.getString(R.string.popup_error_title),
                                context!!.resources.getString(R.string.supplementary_offers_roaming_no_offers_found_lbl))
                        return false
                    }
                    return true
                }
                return false
            }
        })
    }//initUIEvents ends

    private fun getSelectedCountryFlagCode(countryName: String): String {
        var flagCode = ""

        for (its in roaming!!.countries!!.indices) {
            if (roaming!!.countries!![its]!!.name == countryName) {
                if (hasValue(roaming!!.countries!![its]!!.flag)) {
                    flagCode = roaming!!.countries!![its]!!.flag!!
                }
            }
        }

        return flagCode
    }//getSelectedCountryFlagCode ends

    private fun gotoRoamingActivity(countryName: String, countryFlag: String) {

        UserDataManager.getCountryRoamingDataList().clear()
        for (its in roaming!!.offers!!.indices) {
            if (roaming!!.offers!![its] != null &&
                    roaming!!.offers!![its]!!.details != null &&
                    roaming!!.offers!![its]!!.details!!.roamingDetails != null &&
                    !roaming!!.offers!![its]!!.details!!.roamingDetails!!.roamingDetailsCountriesList.isNullOrEmpty()) {

                for (innerIts in roaming!!.offers!![its]!!.details!!.roamingDetails!!.roamingDetailsCountriesList!!.indices) {
                    var countryToMatch = ""
                    if (roaming!!.offers!![its]!!.details!!.roamingDetails!!.roamingDetailsCountriesList!![innerIts]!!.countryName != null) {
                        countryToMatch = roaming!!.offers!![its]!!.details!!.roamingDetails!!.roamingDetailsCountriesList!![innerIts]!!.countryName!!
                    }

                    countryToMatch = countryToMatch
                            .replace("\\r".toRegex(), "").trim()
                            .replace("\\n".toRegex(), "").trim()
                            .replace("\\r\\n".toRegex(), "").trim()
                            .replace("\r".toRegex(), "").trim()
                            .replace("\n".toRegex(), "").trim()
                            .replace("\r\n".toRegex(), "").trim()
                    logE("countryX12", "countryToMatch:::".plus(countryToMatch),
                            fromClass, "gotoRoamingActivity")
                    if (countryName.equals(countryToMatch, ignoreCase = true)) {
                        /* logE("countryX12", "added:::".plus(roaming!!.offers!![its]!!.header!!.offerName),
                                 fromClass, "gotoRoamingActivity")*/
                        UserDataManager.getCountryRoamingDataList().add(roaming!!.offers!![its]!!)
                    } else {
                        /*logE("countryX12", "not added:::".plus(roaming!!.offers!![its]!!.header!!.offerName),
                                fromClass, "gotoRoamingActivity")*/
                    }
                }
            } else {
                logE("countryX12", "nullll:::", fromClass, "gotoRoamingActivity")
            }
        }

        if (UserDataManager.getCountryRoamingDataList().isNullOrEmpty()) {
            showMessageDialog(context!!, activity!!, context!!.resources.getString(R.string.popup_error_title),
                    context!!.resources.getString(R.string.supplementary_offers_roaming_no_offers_found_lbl))
            return
        }

        val bundle = Bundle()
        bundle.putString(ConstantsUtility.PackagesConstants.SELECTED_ROAMING_COUNTRY_NAME, countryName)
        bundle.putString(ConstantsUtility.PackagesConstants.SELECTED_ROAMING_COUNTRY_FLAG, countryFlag)
        if (roaming!!.filters != null) {
            bundle.putParcelable(ConstantsUtility.PackagesConstants.SELECTED_ROAMING_FILTERS, roaming!!.filters)
        }

        RoamingCountryActivity.start(context!!, bundle)
    }//gotoRoamingActivity ends

    private fun initUI() {
        if (roaming != null) {
            if (roaming!!.countries != null && !roaming!!.countries!!.isEmpty()) {
                populateSpinner(roaming!!.countries!!)
                externalView.dropDownButton.visibility = View.VISIBLE
                externalView.noDataFoundLayout.visibility = View.GONE
                externalView.nextButton.visibility = View.VISIBLE
            } else {
                externalView.dropDownButton.visibility = View.GONE
                externalView.noDataFoundLayout.visibility = View.VISIBLE
                externalView.nextButton.visibility = View.GONE
            }
        } else {
            externalView.nextButton.visibility = View.GONE
            externalView.dropDownButton.visibility = View.GONE
            externalView.noDataFoundLayout.visibility = View.VISIBLE
            logE("raomV", "value:::NULL", fromClass, "initUI")
        }

        nextButton.isSelected = true
        disposeArgumentsData()
    }//initUI ends

    private fun populateSpinner(countries: List<Countries?>) {
//        externalView.spinnerRoaming.adapter = RoamingCountriesSpinnerAdapter(context!!, countries, this)

        val listOfCountries = ArrayList<String>()
        for (its in countries.indices) {
            if (countries[its] != null && hasValue(countries[its]!!.name)) {
                listOfCountries.add(countries[its]!!.name!!)
            }
        }
        externalView.roamingDropDownMessageTile.setAdapter(ArrayAdapter<String>(context!!, android.R.layout.simple_dropdown_item_1line, listOfCountries))
    }//populateSpinner ends

    private fun disposeArgumentsData() {
        if (arguments != null && arguments!!.containsKey(ConstantsUtility.PackagesConstants.KEYWORD_ROAMING)) {
            arguments!!.clear()
        }
    }//disposeArgumentsData ends

    private fun initFragmentData() {
        if (arguments != null && arguments!!.containsKey(ConstantsUtility.PackagesConstants.KEYWORD_ROAMING)) {
            roaming = arguments!!.getParcelable(ConstantsUtility.PackagesConstants.KEYWORD_ROAMING)
        }
    }//initFragmentData ends

    override fun onInVisible() {

    }//onInVisible ends

}//class ends
