package com.azarphone.ui.fragment.loan

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.response.inappsurvey.InAppSurvey
import com.azarphone.api.pojo.response.inappsurvey.Surveys
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.loan.GetLoanResponse
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.LayoutLoanFragmentBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.OnClickSurveySubmit
import com.azarphone.eventhandler.UpdateListOnSuccessfullySurvey
import com.azarphone.ui.activities.loan.LoanFactory
import com.azarphone.ui.activities.loan.LoanInjection
import com.azarphone.ui.activities.loan.LoanViewModel
import com.azarphone.ui.adapters.spinners.CreditAmountSpinnerAdapter
import com.azarphone.ui.dialogs.InAppFeedbackDialog
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.validators.isNumeric
import com.azarphone.validators.isValidMsisdn
import com.azarphone.widgets.popups.showMessageDialog
import com.es.tec.LocalSharedPrefStorage
import kotlinx.android.synthetic.main.layout_loan_fragment.view.*

/**
 * @author Junaid Hassan on 05, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */

class LoanFragment : BaseFragment<LayoutLoanFragmentBinding, LoanFactory, LoanViewModel>() {

    private val logKey = "loanFragmentZ78"
    private var myNumber = ""
    private val fromClass = "LoanFragment"
    private lateinit var externalView: View
    private var isMe = true
    private var amountVal = ""

    override fun getLayoutId(): Int {
        return R.layout.layout_loan_fragment
    }

    override fun getViewModelClass(): Class<LoanViewModel> {
        return LoanViewModel::class.java
    }

    override fun getFactory(): LoanFactory {
        return LoanInjection.provideLoanFactory()
    }

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        externalView = view
        subscribe()
        initUI()
        initUIEvents()
    }

    private fun subscribe() {
        val getLoanResponseLiveData = object : Observer<GetLoanResponse> {
            override fun onChanged(response: GetLoanResponse?) {
                if (activity != null && !activity!!.isFinishing) {
                    if (response != null) {
                        logE(
                            logKey,
                            "response:::".plus(response.toString()),
                            fromClass,
                            "subscribe"
                        )
                        if (response.resultDesc != null && hasValue(response.resultDesc)) {
                            inAppFeedback(
                                requireContext(),
                                resources.getString(R.string.popup_note_title),
                                response.resultDesc
                            )
                            UserDataManager.setIsGetHomePageCalledWithSuccess(false)
                            removeHomePageResponseFromLocalPreferences()
                        } else {
                            showMessageDialog(
                                context!!, activity!!,
                                context!!.resources.getString(R.string.popup_note_title),
                                "success but no response msg from server"
                            )
                        }
                    } else {
                        showMessageDialog(
                            context!!, activity!!,
                            context!!.resources.getString(R.string.popup_error_title),
                            resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    }
                }
            }
        }

        mViewModel.getLoanResponseLiveData.observe(activity!!, getLoanResponseLiveData)

        val inAppSurveyResponseErrorResponseData =
            Observer<AzerAPIErrorHelperResponse> { response ->
                if (!isNetworkAvailable(requireContext())) {
                    showMessageDialog(
                        requireContext(), requireActivity(),
                        resources.getString(R.string.popup_note_title),
                        resources.getString(R.string.message_no_internet)
                    )
                } else {
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                        showMessageDialog(
                            requireContext(), requireActivity(),
                            resources.getString(R.string.popup_error_title),
                            response.errorDetail.toString()
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                        //force logout the user here
                        logOut(
                            requireContext(),
                            requireActivity(),
                            false,
                            fromClass,
                            "initSupplementaryOffersCardAdapterEventsa34324"
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                        //handle the ui in this case
                        //hide or show the no data found layout in this case
                        showMessageDialog(
                            requireContext(), requireActivity(),
                            resources.getString(R.string.popup_error_title),
                            resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                        showMessageDialog(
                            requireContext(), requireActivity(),
                            resources.getString(R.string.popup_error_title),
                            resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    } else {
                        showMessageDialog(
                            requireContext(), requireActivity(),
                            resources.getString(R.string.popup_error_title),
                            resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    }
                }
            }
        mViewModel.inAppSurveyResponseErrorResponseData.observe(
            requireActivity(),
            inAppSurveyResponseErrorResponseData
        )

    }

    private fun inAppFeedback(context: Context, title: String, message: String) {

        var inAppFeedbackDialog: InAppFeedbackDialog? = null
        var currentVisit: Int =
            LocalSharedPrefStorage(context).getInt(LocalSharedPrefStorage.PREF_LOAN_PAGE)
        currentVisit += 1
        val inAppSurvey: InAppSurvey? = UserDataManager.getInAppSurvey()
        if ((inAppSurvey?.data) != null) {
            val surveys: Surveys? =
                inAppSurvey.data.findSurvey(ConstantsUtility.InAppSurveyConstants.LOAN_PAGE)
            if (surveys != null) {
                if (surveys.surveyLimit > surveys.surveyCount) {
                    if (surveys.visitLimit <= currentVisit && surveys.visitLimit > -1) {
                        inAppFeedbackDialog =
                            InAppFeedbackDialog(context, object : OnClickSurveySubmit {
                                override fun onClickSubmit(
                                    uploadSurvey: UploadSurvey,
                                    onTransactionComplete: String,updateListOnSuccessfullySurvey: UpdateListOnSuccessfullySurvey?
                                ) {
                                    mViewModel.uploadInAppSurvey(
                                        context,
                                        uploadSurvey,
                                        onTransactionComplete
                                    )
                                    if (inAppFeedbackDialog?.isShowing == true) {
                                        inAppFeedbackDialog?.dismiss()
                                    }
                                }

                            })
                        if (surveys.questions != null) {
                            currentVisit = 0
                            inAppFeedbackDialog.showTitleWithMessageDialog(
                                surveys,
                                title,
                                message
                            )
                        } else {
                            showMessageDialog(
                                context, requireActivity(),
                                title,
                                message
                            )
                        }
                    } else {
                        showMessageDialog(
                            context, requireActivity(),
                            title,
                            message
                        )
                    }
                } else {
                    showMessageDialog(
                        context, requireActivity(),
                        title,
                        message
                    )
                }

            } else {
                showMessageDialog(
                    context, requireActivity(),
                    title,
                    message
                )
            }
        } else {
            showMessageDialog(
                context, requireActivity(),
                resources.getString(R.string.popup_note_title),
                message
            )
        }
        LocalSharedPrefStorage(context).addInt(
            LocalSharedPrefStorage.PREF_LOAN_PAGE,
            currentVisit
        )
    }

    private fun initUI() {
        setHintColor(externalView.msisdnTL, R.color.colorFloatingHints, context!!)
        addPrefixToUserNameField(externalView.msisdnET)
        disableEditText(externalView.msisdnET, R.color.colorFloatingHintsDisabled, context!!)
        if (UserDataManager.getCustomerData() != null) {
            if (hasValue(UserDataManager.getCustomerData()!!.msisdn)) {
                externalView.msisdnET.setText(
                    externalView.msisdnET.text.toString()
                        .plus(UserDataManager.getCustomerData()!!.msisdn!!)
                )
            }
        }
        externalView.receiverSection.visibility = View.GONE
        /*if (getHomePageResponseFromLocalPreferences("loanFragment").data != null
            && getHomePageResponseFromLocalPreferences("loanFragment").data!!.credit != null
            && hasValue(getHomePageResponseFromLocalPreferences("loanFragment").data!!.credit!!.creditTitleValue)
        ) {
            amountVal =
                getHomePageResponseFromLocalPreferences("loanFragment").data!!.credit!!.creditTitleValue!!

            externalView.amountValue.text = getManatConcatedString(
                context!!, amountVal,
                ConstantsUtility.PackagesConstants.AZERI_SYMBOL, 0.20f, R.dimen._10ssp
            )
        }*/

        //Implemented spinner for loan amount
        UserDataManager.getPredefineData()?.topup?.getLoan?.selectAmount?.prepaid?.let {
            externalView.amountSpinner.adapter = CreditAmountSpinnerAdapter(context!!, it)

        }

        externalView.spinnerHolder?.setOnClickListener {
            externalView.amountSpinner.performClick()
        }

    }//initUI ends

    private fun initUIEvents() {
        externalView.radioGroupLoan.setOnCheckedChangeListener { group, checkedId ->
            val checkedRadioButton = group!!.findViewById(checkedId) as RadioButton
            val isChecked = checkedRadioButton.isChecked
            if (checkedId == R.id.radioMe) {
                if (isChecked) {
                    isMe = true
                    disableEditText(
                        externalView.msisdnET,
                        R.color.colorFloatingHintsDisabled,
                        context!!
                    )
                    if (UserDataManager.getCustomerData() != null) {
                        if (hasValue(UserDataManager.getCustomerData()!!.msisdn)) {
                            externalView.msisdnET.setText(
                                externalView.msisdnET.text.toString()
                                    .plus(UserDataManager.getCustomerData()!!.msisdn!!)
                            )
                        }
                    }
                    externalView.receiverSection.visibility = View.GONE
                }
            } else {
                if (isChecked) {
                    isMe = false
                    enableEditText(
                        externalView.msisdnET,
                        R.color.colorFloatingHintsEnabled,
                        context!!
                    )
                    externalView.msisdnET.setText("")
                    externalView.receiverSection.visibility = View.VISIBLE
                }
            }
        }//radioGroupTopup.setOnCheckedChangeListener  ends

        externalView.getLoanButton.setOnClickListener {
            externalView?.amountSpinner?.selectedItem.let { prePaidItem ->
                if (prePaidItem is String && !TextUtils.isEmpty(prePaidItem)) {
                    amountVal = prePaidItem
                }
            }
            if (isMe) {

                var dialogMessage = ""

                when {
                    ProjectApplication.mLocaleManager.getLanguage()
                        .equals(ConstantsUtility.LanguageKeywords.AZ, ignoreCase = true) -> {
                        dialogMessage =
                            if(!isNumeric(amountVal))
                            {
                                "Siz $amountVal " + requireContext().getString(
                                    R.string.credit_confirmation_amount_new
                                ) + " 0.05 " + requireContext().getString(
                                    R.string.credit_confirmation_fees_new
                                )
                            }
                        else {
                                "Siz $amountVal " + requireContext().getString(
                                    R.string.credit_confirmation_amount
                                ) + " 0.05 " + requireContext().getString(
                                    R.string.credit_confirmation_fees
                                )
                            }
                    }
                    ProjectApplication.mLocaleManager.getLanguage()
                        .equals(ConstantsUtility.LanguageKeywords.RU, ignoreCase = true) -> {
                        dialogMessage = if (!isNumeric(amountVal)) {
                            requireContext().getString(R.string.credit_confirmation_amount_new) + " $amountVal " + requireContext().getString(
                                R.string.credit_confirmation_fees_new
                            ) + " 0.05 AZN."

                        } else {
                            requireContext().getString(R.string.credit_confirmation_amount) + " $amountVal " + requireContext().getString(
                                R.string.credit_confirmation_fees
                            ) + " 0.05 AZN"
                        }
                    }
                    else -> {
                        dialogMessage = if (!isNumeric(amountVal)) {
                                requireContext().getString(R.string.credit_confirmation_amount_new) +  " $amountVal "  + requireContext().getString(
                                    R.string.credit_confirmation_fees_new
                                ) + " 0.05 AZN."
                            }
                        else {
                            requireContext().getString(R.string.credit_confirmation_amount) + " $amountVal " + requireContext().getString(
                                R.string.credit_confirmation_fees
                            ) + " 0.05 AZN"
                        }
                    }
                }

                showRequestGetLoanConfirmationPopup(dialogMessage, "")
            } else {
                //get loan for friend
                myNumber = externalView.msisdnET.text.toString()
                myNumber = myNumber.replace(ConstantsUtility.UserConstants.USERNAME_PREFIX, "")
                if (isValidMsisdn(myNumber)) {
                    showRequestGetLoanConfirmationPopup(getLoanConfirmationMessageLabel(), myNumber)
                } else {
                    showMessageDialog(
                        context!!,
                        activity!!,
                        context!!.resources.getString(R.string.popup_error_title),
                        resources.getString(R.string.error_msg_invalid_number)
                    )
                }
            }
        }//externalView.getLoanButton.setOnClickListener ends
    }//initUIEvents ends

    private fun getLoanConfirmationMessageLabel(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "Please confirm to continue."
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Davam etmək üçün təsdiq et."
            else -> "Пожалуйста, подтверди, чтобы продолжить."
        }
    }

    @SuppressLint("InflateParams", "SetTextI18n")
    private fun showRequestGetLoanConfirmationPopup(message: String, msisdn: String) {
        if (activity != null && !activity!!.isFinishing) {

            val dialogBuilder = AlertDialog.Builder(activity!!)
            val inflater =
                activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val dialogView = inflater.inflate(R.layout.credit_confirmation_popup, null)
            dialogBuilder.setView(dialogView)

            val alertDialog = dialogBuilder.create()
            alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            alertDialog.show()

            val llPopupBalance = dialogView.findViewById(R.id.llPopupBalance) as LinearLayout
            val popupMessage = dialogView.findViewById(R.id.popupMessage) as TextView
            val popupTitle = dialogView.findViewById(R.id.popupTitle) as TextView
            val popupBalance = dialogView.findViewById(R.id.popupBalance) as TextView
            popupMessage.typeface = getALSNormalFont()
            if (isMe) {
                llPopupBalance.visibility = View.VISIBLE
                popupTitle.text = requireContext().getString(R.string.credit_confirmation_title)
                popupBalance.text = " : " + getManatConcatedString(
                    context!!,
                    getHomePageResponseFromLocalPreferences("loan").data?.balance?.prepaid?.mainWallet?.amount
                        ?: amountVal,
                    ConstantsUtility.PackagesConstants.AZERI_SYMBOL,
                    0.20f,
                    R.dimen._10ssp
                )
            } else {
                llPopupBalance.visibility = View.INVISIBLE
            }
            popupMessage.text = message
            val popupOkayButton = dialogView.findViewById(R.id.yesButton) as Button
            popupOkayButton.typeface = getALSBoldFont()
            val popupCancelButton = dialogView.findViewById(R.id.noButton) as Button
            popupCancelButton.typeface = getALSBoldFont()

            if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN) {
                //english
                popupOkayButton.text = getString(R.string.popup_text_update_ok)
                popupCancelButton.text = "No"
            }

            if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ) {
                //azri
                popupOkayButton.text = getString(R.string.popup_text_update_ok)
                popupCancelButton.text = "Xeyr"
            }

            if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.RU) {
                //russian
                popupOkayButton.text = getString(R.string.popup_text_update_ok)
                popupCancelButton.text = "Нет"
            }

            popupOkayButton.setOnClickListener {
                alertDialog.dismiss()
                mViewModel.requestGetLoan(msisdn, isMe, amountVal)
            }

            popupCancelButton.setOnClickListener {
                alertDialog.dismiss()
            }
        }
    }

    override fun onInVisible() {

    }

}