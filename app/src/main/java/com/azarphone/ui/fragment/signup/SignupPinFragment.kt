package com.azarphone.ui.fragment.signup

import android.app.Activity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.azarphone.R
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.SignUpPinFragmentBinding
import com.azarphone.eventhandler.SignUpEvents
import com.azarphone.eventhandler.SignupActivityToolTipEvents
import com.azarphone.ui.activities.signupactivity.SignUpActivity
import com.azarphone.ui.activities.signupactivity.SignUpFactory
import com.azarphone.ui.activities.signupactivity.SignUpInjectionUtils
import com.azarphone.ui.activities.signupactivity.SignUpViewModel
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.RootValues
import com.azarphone.util.getALSBoldFont
import com.azarphone.util.logE
import com.azarphone.widgets.tooltip.initiateErrorToolTip
import io.canner.stepsview.StepsView
import kotlinx.android.synthetic.main.sign_up_pin_fragment.view.*

@Suppress("DEPRECATION")
class SignupPinFragment : BaseFragment<SignUpPinFragmentBinding, SignUpFactory, SignUpViewModel>() {

    private var msisdnFromPreviousScreen = ""
    private val fromClass = "SignupPinFragment"
    private lateinit var externalView: View
    private var signupActivityToolTipEvents: SignupActivityToolTipEvents? = null

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        initOnBackEvents()
    }

    private fun initOnBackEvents(){
        val signUpEvents=object: SignUpEvents {
            override fun onGoBack() {
                externalView.enterPinET.setText("")
            }
        }

        RootValues.setSignUpEvents(signUpEvents)
    }

    override fun getLayoutId(): Int {
        return R.layout.sign_up_pin_fragment
    }

    override fun getViewModelClass(): Class<SignUpViewModel> {
        return SignUpViewModel::class.java
    }

    override fun getFactory(): SignUpFactory {
        return SignUpInjectionUtils.provideSignUpFactory()
    }

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        externalView = view
        setupInputFields()
        registerListener()
        mViewModel.isTermsAndCondtionsAccepted.value = true
        mFragmentViewDataBinding.apply {
            viewmodel = mViewModel
        }//mFragmentViewDataBinding.apply ends

        //get the msisdnFromPreviousScreen from the previous screen
        if (arguments != null && arguments!!.containsKey(ConstantsUtility.BundleKeys.BUNDLE_SIGN_UP_NUMBER)) {
            msisdnFromPreviousScreen = arguments!!.getString(ConstantsUtility.BundleKeys.BUNDLE_SIGN_UP_NUMBER)!!
        }

        //set the stepper steps
        mViewModel.setSignupPasswordStep(ConstantsUtility.SignupScreenSteps.ENTER_PIN_STEP)

        view.resendTV.setOnClickListener {
            //resend the otp
            mViewModel.resendPin(msisdnFromPreviousScreen)
        }//view.resendTV.setOnClickListener ends

        activity?.let {
            val stepView = it.findViewById<StepsView>(R.id.stepsView).setCompletedPosition(1).drawView()
        }//activity?.let  ends

        //show the timer
        mViewModel.set25SecondsTimer(context!!)

        view.enterPinET.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if (event!!.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    (activity as SignUpActivity).processOnSignup()
                }//if ends
                return false
            }//onKey ends
        })

        externalView.enterPinET.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    (activity as SignUpActivity).processOnSignup()
                }//if ends
                return false
            }//onEditorAction ends
        })
    }

    private fun setupInputFields() {
        externalView.enterPinTIL.defaultHintTextColor = context!!.resources.getColorStateList(R.color.soft_pink)
        externalView.enterPinET.typeface = getALSBoldFont()
    }//setupInputFields ends

    private fun registerListener() {
        signupActivityToolTipEvents = object : SignupActivityToolTipEvents {
            override fun onInvalidPasswordTooltip(errorDetail: String) {
                //do nothing
            }

            override fun onInvalidConfirmPasswordTooltip(errorDetail: String) {
                //do nothing
            }

            override fun onInvalidOTPPINTooltip(errorDetail: String) {
                logE("toolerX", "detail:::".plus(errorDetail), fromClass, "onInvalidOTPPINTooltip")
                initiateErrorToolTip(context!!, externalView.enterPinET, errorDetail)
            }

            override fun onInvalidMsisdnNumberTooltip(errorDetail: String) {
                //do nothing
            }
        }

        RootValues.setSignupActivityToolTipEvents(signupActivityToolTipEvents!!)
    }

    override fun onInVisible() {
    }//onInVisible ends

}//onInVisible ends