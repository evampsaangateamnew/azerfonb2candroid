package com.azarphone.ui.fragment.loan

import android.app.DatePickerDialog
import androidx.lifecycle.Observer
import android.os.Build
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import android.widget.AdapterView
import android.widget.TextView
import com.azarphone.R
import com.azarphone.api.pojo.response.paymenthistory.PaymentHistoryResponse
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.LayoutPaymentFragmentBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.ui.activities.loan.LoanFactory
import com.azarphone.ui.activities.loan.LoanInjection
import com.azarphone.ui.activities.loan.LoanViewModel
import com.azarphone.ui.adapters.recyclerviews.PaymentHistoryAdapter
import com.azarphone.ui.adapters.spinners.LoanSpinnerAdapter
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.widgets.calendar.AzerCalendar
import kotlinx.android.synthetic.main.layout_payment_fragment.view.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


/**
 * @author Junaid Hassan on 05, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class PaymentFragment : BaseFragment<LayoutPaymentFragmentBinding, LoanFactory, LoanViewModel>() {

    private lateinit var externalView: View
    private var date = ""
    private var startDate = ""
    private var endDate = ""
    private var dateOne = ""
    private var dateTwo = ""
    private var flagDate = false
    private var logKey = "PaymentFragmentX1289"
    private val fromClass = "PaymentFragment"
    private var isLoadOnce = false
    private var paymentHistoryAdapter: PaymentHistoryAdapter? = null
    private var paymentHistoryResponse: PaymentHistoryResponse? = null

    override fun getLayoutId(): Int {
        return R.layout.layout_payment_fragment
    }

    override fun getViewModelClass(): Class<LoanViewModel> {
        return LoanViewModel::class.java
    }

    override fun getFactory(): LoanFactory {
        return LoanInjection.provideLoanFactory()
    }

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        externalView = view
        subscribe()
        initUI()
        initUIEvents()
    }

    private fun subscribe() {
        val paymentHistoryResponseLiveData = object : Observer<PaymentHistoryResponse> {
            override fun onChanged(response: PaymentHistoryResponse?) {
                if (activity != null && !activity!!.isFinishing) {
                    if (response != null) {
                        if (response.data.paymentHistory != null && !response.data.paymentHistory!!.isEmpty()) {
                            UserDataManager.setPaymentHistoryResponse(response)
                            logE(logKey, "response:::".plus(response), fromClass, "subscribe")
                            paymentHistoryAdapter = PaymentHistoryAdapter(context!!, response.data.paymentHistory!!)
                            externalView.paymentHistoryList.adapter = paymentHistoryAdapter
                            externalView.paymentHistoryList.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
                            showContents()
                        } else {
                            UserDataManager.setPaymentHistoryResponse(null)
                            logE(logKey, "response.data.paymentHistory != null111:::", fromClass, "subscribe")
                            hideContents()
                        }
                    } else {
                        UserDataManager.setPaymentHistoryResponse(null)
                        logE(logKey, "response != null222:::", fromClass, "subscribe")
                        hideContents()
                    }
                }
            }
        }

        mViewModel.paymentHistoryResponseLiveData.observe(activity!!, paymentHistoryResponseLiveData)
    }

    private fun hideContents() {
        externalView.paymentHistoryList.visibility = View.GONE
        externalView.noDataFoundLayout.visibility = View.VISIBLE
    }//hideContents ends

    private fun showContents() {
        externalView.noDataFoundLayout.visibility = View.GONE
        externalView.paymentHistoryList.visibility = View.VISIBLE
    }//showContents ends


    private fun initUI() {
        val periodArray = resources.getStringArray(R.array.period_filter)
        externalView.spinnerPeriod.adapter = LoanSpinnerAdapter(context!!, periodArray)
        //set the default dates for the spinner period date views
        setDefaultDates()
        externalView.loanIdTitle.isSelected = true
        externalView.amountTitle.isSelected = true
    }//initUI ends

    private fun initUIEvents() {
        externalView.spinnerHolder.setOnClickListener {
            externalView.spinnerPeriod.performClick()
        }

        externalView.dateOneView.setOnClickListener {
            showCalendar(externalView.dateOneView, true, externalView)//false for max date
        }//dateOne.setOnClickListener  ends

        externalView.dateTwoView.setOnClickListener {
            showCalendar(externalView.dateTwoView, false, externalView)//true for min date
        }

        externalView.spinnerPeriod.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                logE(logKey, "itemPos:::".plus(position.toString()), fromClass, "onItemSelected")
                if (position == 0) {
                    //current day
                    try {
                        if (flagDate) {
                            externalView.calendarLayout.visibility = View.GONE
                            updateDates(getCurrentDate(), getCurrentDate())
                        } else {
                            flagDate = true
                        }
                    } catch (exp: Exception) {
                        logE(logKey, "error1:::".plus(exp.toString()), fromClass, "onItemSelected")
                    }
                } else if (position == 1) {
                    //last seven days
                    try {
                        externalView.calendarLayout.visibility = View.GONE
                        date = ""
                        date = getLastDateMinusDays(getCurrentDate(), ConstantsUtility.DateFormats.LAST_SEVEN_DAY)
                        updateDates(date, getCurrentDate())
                    } catch (exp: Exception) {
                        logE(logKey, "error2:::".plus(exp.toString()), fromClass, "onItemSelected")
                    }
                } else if (position == 2) {
                    //last 30 days
                    try {
                        externalView.calendarLayout.visibility = View.GONE
                        date = ""
                        date = getLastDateMinusDays(getCurrentDate(), ConstantsUtility.DateFormats.LAST_THIRTY_DAY)
                        updateDates(date, getCurrentDate())
                    } catch (exp: Exception) {
                        logE(logKey, "error3:::".plus(exp.toString()), fromClass, "onItemSelected")
                    }
                } else if (position == 3) {
                    //previous month
                    try {
                        externalView.calendarLayout.visibility = View.GONE
                        date = ""
                        var maxDate = ""
                        date = getLastMonthFirstDate()
                        maxDate = getLastMonthLastDate()
                        updateDates(date, maxDate)
                    } catch (exp: Exception) {
                        logE(logKey, "error4:::".plus(exp.toString()), fromClass, "onItemSelected")
                    }
                } else if (position == 4) {
                    //custom dates
                    try {
                        setDefaultDates()
                        externalView.dateOneView.setTextColor(resources.getColor(R.color.colorTextGray))
                        externalView.dateTwoView.setTextColor(resources.getColor(R.color.colorTextGray))
                        externalView.calendarLayout.visibility = View.VISIBLE
                    } catch (exp: Exception) {
                        logE(logKey, "error5:::".plus(exp.toString()), fromClass, "onItemSelected")
                    }
                } else {
                    externalView.calendarLayout.visibility = View.GONE
                }
            }
        }
    }//initUIEvents ends

    @Throws(ParseException::class)
    fun showCalendar(textViewNormal: TextView, isStartDateSelected: Boolean, view: View) {

        try {
            dateOne = changeDateFormatForMethods(view.dateOneView.text.toString())
            dateTwo = changeDateFormatForMethods(view.dateTwoView.text.toString())

            val minDate = SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd, Locale.ENGLISH).parse(dateOne)
            val maxDate = SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd, Locale.ENGLISH).parse(dateTwo)

            val azerCalendar: AzerCalendar

            val calendar = Calendar.getInstance()
            if (isStartDateSelected) {
                calendar.time = minDate
            } else {
                calendar.time = maxDate
            }
            val year = calendar.get(Calendar.YEAR)
            val month = calendar.get(Calendar.MONTH)
            val day = calendar.get(Calendar.DAY_OF_MONTH)

            azerCalendar = AzerCalendar(textViewNormal, ondate)


            val datePickerDialog = DatePickerDialog(context!!, azerCalendar, year, month, day)
            //calculate min and max dates (for older versions use System Current TimeMillis

            if (isStartDateSelected) {

                val calndPre = Calendar.getInstance()
                calndPre.add(Calendar.MONTH, -3)
                calndPre.set(Calendar.DATE, 1)
                datePickerDialog.datePicker.minDate = calndPre.timeInMillis

                val dateMax = SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd, Locale.ENGLISH).parse(dateTwo)
                calendar.time = dateMax
                datePickerDialog.datePicker.maxDate = calendar.timeInMillis

            } else {

                val dateMin = SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd, Locale.ENGLISH).parse(dateOne)
                calendar.time = dateMin
                datePickerDialog.datePicker.minDate = calendar.timeInMillis
                datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
            }

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                datePickerDialog.setTitle("")//Prevent Date picker from creating extra Title.!
            }

            datePickerDialog.show()
        } catch (e: ParseException) {
            logE(logKey, "error:::".plus(e.toString()), fromClass, "showCalendar")
        }

    }//showCalendar ends

    private var ondate: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        try {
            dateOne = changeDateFormatForMethods(externalView.dateOneView.text.toString())
        } catch (e: ParseException) {
            logE(logKey, "error1:::".plus(e.toString()), fromClass, "ondate")
        }

        try {
            dateTwo = changeDateFormatForMethods(externalView.dateTwoView.text.toString())
        } catch (e: ParseException) {
            logE(logKey, "error2:::".plus(e.toString()), fromClass, "ondate")
        }

        updateDates(dateOne, dateTwo)
    }//ondate ends

    private fun updateDates(newStartDate: String, newEndDate: String) {
        startDate = newStartDate
        endDate = newEndDate

        var formattedStartedDate = ""
        if (hasValue(changeDateFormat(startDate))) {
            formattedStartedDate = changeDateFormat(startDate)!!
        }

        var formattedEndDate = ""
        if (hasValue(changeDateFormat(endDate))) {
            formattedEndDate = changeDateFormat(endDate)!!
        }
        mViewModel.requestPaymentHistory(formattedStartedDate, formattedEndDate)
    }//updateDates ends

    private fun setDefaultDates() {
        externalView.dateOneView.text = changeDateFormatForTextView(getCurrentDate())
        externalView.dateTwoView.text = changeDateFormatForTextView(getCurrentDate())
    }//setDefaultDates ends

    override fun onInVisible() {

    }

}