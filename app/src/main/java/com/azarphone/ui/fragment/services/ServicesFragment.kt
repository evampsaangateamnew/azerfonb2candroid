package com.azarphone.ui.fragment.services

import android.os.Bundle
import android.view.View
import com.azarphone.R
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.ServicesLayoutBinding
import com.azarphone.ui.activities.mainactivity.MainFactory
import com.azarphone.ui.activities.mainactivity.MainInjection
import com.azarphone.ui.activities.mainactivity.MainViewModel
import com.azarphone.ui.adapters.recyclerviews.ServiceMenuRecyclerAdapter
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.RootValues
import com.azarphone.util.getServicesMenuModel
import com.azarphone.util.logE
import com.azarphone.validators.hasValue
import kotlinx.android.synthetic.main.services_layout.*

class ServicesFragment : BaseFragment<ServicesLayoutBinding, MainFactory, MainViewModel>() {

    //Class Name variable
    private val fromClass = "ServicesFragment"

    //Bundle variables
    private var matchedSubScreen = ""
    private var stepOneScreen = ""
    private var tabToRedirect = ""
    private var offeringIdToRedirect = ""

    override fun getLayoutId(): Int {
        return R.layout.services_layout
    }

    override fun getViewModelClass(): Class<MainViewModel> {
        return MainViewModel::class.java
    }

    override fun getFactory(): MainFactory {
        return MainInjection.provideMainFactory()
    }

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }

    override fun onResume() {
        super.onResume()
        if (RootValues.getAzerFonActionBarEvents() != null) {
            RootValues.getAzerFonActionBarEvents()!!.onServicesMenuSetActionBar()
        }
    }

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        if (activity != null && !activity!!.isFinishing) {

            if (!getServicesMenuModel().isNullOrEmpty()) {
                //Getting data from arguments
                getArgumentsData()
                //Logging value of identifier
                getServicesMenuModel()?.get(1)?.let {
                    logE(
                        "index",
                        it.identifier.toString(),
                        fromClass,
                        "onVisible"
                    )
                }
                //Setting up recycler view
                innerMenuRcycler?.apply {

                    //Setting up layout manager
                    layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
                        context,
                        androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                        false
                    )
                    //Setting up adapter
                    adapter = ServiceMenuRecyclerAdapter(
                        context!!,
                        getServicesMenuModel()!!,
                        stepOneScreen,
                        matchedSubScreen,
                        tabToRedirect,
                        offeringIdToRedirect
                    )
                }

            } else {
                noDataFoundLayout.visibility = View.VISIBLE
                innerMenuRcycler.visibility = View.GONE
            }
        }
    }

    private fun getArgumentsData() {
        //Getting values from argument bundle
        arguments?.let {

            if (it.containsKey(ConstantsUtility.DynamicLinkConstants.SUB_SCREEN_NAME_DATA)
                && hasValue(it.getString(ConstantsUtility.DynamicLinkConstants.SUB_SCREEN_NAME_DATA))
            ) {
                val subScreen =
                    it.getString(ConstantsUtility.DynamicLinkConstants.SUB_SCREEN_NAME_DATA)
                setMatchedSubScreenValue(subScreen!!)
            }

            if (it.containsKey(ConstantsUtility.DynamicLinkConstants.STEP_ONE_SCREEN_NAME_DATA)
                && hasValue(it.getString(ConstantsUtility.DynamicLinkConstants.STEP_ONE_SCREEN_NAME_DATA))
            ) {
                stepOneScreen =
                    it.getString(ConstantsUtility.DynamicLinkConstants.STEP_ONE_SCREEN_NAME_DATA)!!
            }

            if (it.containsKey(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA)
                && hasValue(it.getString(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA))
            ) {
                tabToRedirect = it.getString(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA)!!
            }

            if (it.containsKey(ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA)
                && hasValue(it.getString(ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA))
            ) {
                offeringIdToRedirect =
                    it.getString(ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA)!!
            }
        }

        //Logging final received values from argument bundle
        logE(
            ConstantsUtility.DynamicLinkConstants.DYNAMIC_LINK_TAG,
            "matchedSubScreen = $matchedSubScreen tabToRedirect = $tabToRedirect" +
                    "offeringIdToRedirect = $offeringIdToRedirect",
            fromClass,
            "getArgumentsData"
        )
    }//getArgumentsData ends

    private fun setMatchedSubScreenValue(subScreen: String) {
        logE(
            ConstantsUtility.DynamicLinkConstants.DYNAMIC_LINK_TAG,
            "subScreen = $subScreen",
            fromClass,
            "setMatchedSubScreenValue"
        )
        when {
            subScreen.equals(
                ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_FREE_SMS,
                ignoreCase = true
            ) -> {
                matchedSubScreen =
                    ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_FREE_SMS
            }
            subScreen.equals(
                ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_FNF,
                ignoreCase = true
            ) -> {
                matchedSubScreen =
                    ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_FNF
            }
            subScreen.equals(
                ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_SUPPLEMENTORY_OFFERS,
                ignoreCase = true
            ) -> {
                matchedSubScreen =
                    ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_SUPPLEMENTORY_OFFERS
            }
            subScreen.equals(
                ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_VAS_SERVICES,
                ignoreCase = true
            ) -> {
                matchedSubScreen =
                    ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_VAS_SERVICES
            }
            subScreen.equals(
                ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_EXCHANGE_SERVICES,
                ignoreCase = true
            ) -> {
                matchedSubScreen =
                    ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_EXCHANGE_SERVICES
            }
            subScreen.equals(
                ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_NAR_TV,
                ignoreCase = true
            ) -> {
                matchedSubScreen =
                    ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_NAR_TV
            }
            subScreen.equals(
                ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_TOPUP,
                ignoreCase = true
            ) -> {
                matchedSubScreen =
                    ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_TOPUP
            }
        }
    }//setMatchedSubScreenValue ends

    override fun onInVisible() {
    }

}