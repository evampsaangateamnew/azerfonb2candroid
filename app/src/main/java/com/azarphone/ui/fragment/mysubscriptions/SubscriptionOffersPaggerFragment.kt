package com.azarphone.ui.fragment.mysubscriptions

import android.os.Bundle
import android.os.Handler
import android.os.Parcelable
import android.view.View
import com.azarphone.R
import com.azarphone.api.pojo.response.mysubscriptions.helper.Subscriptions
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.LayoutSubscriptionOffersPaggerFragmentBinding
import com.azarphone.eventhandler.MySubscriptionAdapterEvents
import com.azarphone.eventhandler.MySubscriptionsAdapterUpdateEvents
import com.azarphone.ui.activities.mysubscriptions.MySubscriptionsFactory
import com.azarphone.ui.activities.mysubscriptions.MySubscriptionsInjection
import com.azarphone.ui.activities.mysubscriptions.MySubscriptionsViewModel
import com.azarphone.ui.activities.supplementaryoffers.SupplementaryOffersActivity
import com.azarphone.ui.adapters.recyclerviews.MySubscriptionsCardAdapter
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.RootValues
import com.azarphone.util.logE
import com.yarolegovich.discretescrollview.Orientation
import com.yarolegovich.discretescrollview.transform.ScaleTransformer
import kotlinx.android.synthetic.main.layout_subscription_offers_pagger_fragment.view.*
import kotlinx.android.synthetic.main.no_data_found_mysubscriptions_white_no_bg.view.*

/**
 * @author Junaid Hassan on 26, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class SubscriptionOffersPaggerFragment : BaseFragment<LayoutSubscriptionOffersPaggerFragmentBinding, MySubscriptionsFactory, MySubscriptionsViewModel>() {

    private val fromClass = "SubscriptionOffersPaggerFragment"
    private lateinit var externalView: View
    private var pageTitle = ""
    private var offeringIDPosition = 0
    private var selectedTabPosition = 0
    private var offers: List<Subscriptions?>? = null
    private var mySubscriptionsCardAdapter: MySubscriptionsCardAdapter? = null

    companion object {
        fun getInstance(
            selectedTabPosition: Int,
            offeringID: Int,
            tabTitle: String,
            offers: List<Subscriptions?>
        ): SubscriptionOffersPaggerFragment {
            val fragment = SubscriptionOffersPaggerFragment()
            val bundle = Bundle()
            bundle.putString(ConstantsUtility.MySubscriptionsConstants.TAB_TITLE, tabTitle)
            bundle.putInt(ConstantsUtility.MySubscriptionsConstants.OFFERING_ID, offeringID)
            bundle.putInt(
                ConstantsUtility.MySubscriptionsConstants.TAB_POSITION,
                selectedTabPosition
            )
            bundle.putParcelableArrayList(
                ConstantsUtility.MySubscriptionsConstants.TAB_OFFERS,
                offers as ArrayList<out Parcelable>
            )
            fragment.arguments = bundle
            return fragment
        }//getInstance ends
    }//companion object ends

    override fun getLayoutId(): Int {
        return R.layout.layout_subscription_offers_pagger_fragment
    }//getLayoutId ends

    override fun getViewModelClass(): Class<MySubscriptionsViewModel> {
        return MySubscriptionsViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): MySubscriptionsFactory {
        return MySubscriptionsInjection.provideMySubscriptionsFactory()
    }//getFactory ends

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }//isBindedToActivityLifeCycle ends

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        if (activity != null && !activity!!.isFinishing) {
            externalView = view
            initFragmentData()
            initAdapterNotifyDataSetEvents()
            initUI()
            initUIEvents()
        }
    }//onVisible ends

    private fun initAdapterNotifyDataSetEvents() {
        val mySubscriptionsAdapterUpdateEvents = object : MySubscriptionsAdapterUpdateEvents {
            override fun onAdapterNeedToBeNotify() {
                logE("AdapterXu", "called111", fromClass, "initAdapterNotifyDataSetEvents")
                if (mySubscriptionsCardAdapter != null) {
                    mySubscriptionsCardAdapter!!.notifyDataSetChanged()
                }
            }
        }

        RootValues.setMySubscriptionsAdapterUpdateEvents(mySubscriptionsAdapterUpdateEvents)

        val mySubscriptionAdapterEvents = object : MySubscriptionAdapterEvents {
            override fun onMySubscriptionsSuccessReponse() {
                logE("AdapterXu", "called222", fromClass, "initAdapterNotifyDataSetEvents")
                if (mySubscriptionsCardAdapter != null) {
                    mySubscriptionsCardAdapter!!.notifyDataSetChanged()
                }
            }
        }

        RootValues.setMySubscriptionAdapterEvents(mySubscriptionAdapterEvents)
    }

    private fun initUIEvents() {
        externalView.noDataFoundReviewButton.setOnClickListener {
            context?.let { it1 ->
                SupplementaryOffersActivity.start(
                    it1,
                    selectedTabPosition
                )
            }
        }
    }//initUIEvents ends

    private fun initUI() {
        mViewModel.showLoader()
        Handler().postDelayed({
            if (offers != null && !offers!!.isEmpty()) {
                externalView.mDiscreteView.visibility = View.VISIBLE
                externalView.noDataFoundLayout.visibility = View.GONE
                externalView.mDiscreteView.recycledViewPool.setMaxRecycledViews(0, 0)
                externalView.mDiscreteView.setOrientation(Orientation.HORIZONTAL)
                externalView.mDiscreteView.setItemTransformer(ScaleTransformer.Builder().build())
                mySubscriptionsCardAdapter =
                    context?.let { MySubscriptionsCardAdapter(it, offers, false, "", "", false,mViewModel) }
                externalView.mDiscreteView.adapter = mySubscriptionsCardAdapter

                if (offeringIDPosition != -1) {
                    externalView.mDiscreteView.scrollToPosition(offeringIDPosition)
                }

            } else {
                externalView.mDiscreteView.visibility = View.GONE
                externalView.noDataFoundLayout.visibility = View.VISIBLE

            }
        },1000L)

        Handler().postDelayed({mViewModel.hideLoader()},1500L)

        disposeArgumentsData()
    }//initUI ends

    private fun disposeArgumentsData() {
        if (arguments != null && arguments!!.containsKey(ConstantsUtility.MySubscriptionsConstants.TAB_TITLE)) {
            arguments!!.clear()
        }

        if (arguments != null && arguments!!.containsKey(ConstantsUtility.MySubscriptionsConstants.TAB_OFFERS)) {
            arguments!!.clear()
        }
    }//disposeArgumentsData ends

    private fun initFragmentData() {
        if (arguments != null && arguments!!.containsKey(ConstantsUtility.MySubscriptionsConstants.TAB_TITLE)) {
            pageTitle = arguments!!.getString(ConstantsUtility.MySubscriptionsConstants.TAB_TITLE)!!
        }

        if (arguments != null && arguments!!.containsKey(ConstantsUtility.MySubscriptionsConstants.TAB_OFFERS)) {
            offers =
                arguments!!.getParcelableArrayList(ConstantsUtility.MySubscriptionsConstants.TAB_OFFERS)
        }

        if (arguments != null && arguments!!.containsKey(ConstantsUtility.MySubscriptionsConstants.OFFERING_ID)) {
            offeringIDPosition =
                arguments!!.getInt(ConstantsUtility.MySubscriptionsConstants.OFFERING_ID, 0)
        }

        if (arguments != null && arguments!!.containsKey(ConstantsUtility.MySubscriptionsConstants.TAB_POSITION)) {
            selectedTabPosition =
                arguments!!.getInt(ConstantsUtility.MySubscriptionsConstants.TAB_POSITION, 0)
        }

        logE(
            ConstantsUtility.DynamicLinkConstants.DYNAMIC_LINK_TAG,
            "offeringIDPosition = $offeringIDPosition, selectedTabPosition = $selectedTabPosition",
            fromClass,
            "initFragmentData"
        )
    }//initFragmentData ends

    override fun onInVisible() {

    }//onInVisible ends

}//class ends