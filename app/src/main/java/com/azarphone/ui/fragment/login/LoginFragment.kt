package com.azarphone.ui.fragment.login

import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.hardware.fingerprint.FingerprintManager
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.Selection
import android.text.TextWatcher
import android.text.method.PasswordTransformationMethod
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.UserAccounts
import com.azarphone.api.pojo.response.customerdata.CustomerData
import com.azarphone.api.pojo.response.loginappresumeresponse.LoginAppResumeResponse
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.LoginFragmentBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.BioMetricAuthenticationEvents
import com.azarphone.eventhandler.LoginEventHandlers
import com.azarphone.ui.activities.forgotpasswordactivity.ForgotPasswordActivity
import com.azarphone.ui.activities.loginactivity.LoginActivity
import com.azarphone.ui.activities.loginactivity.LoginFactory
import com.azarphone.ui.activities.loginactivity.LoginInjectionUtils
import com.azarphone.ui.activities.loginactivity.LoginViewModel
import com.azarphone.ui.activities.mainactivity.MainActivity
import com.azarphone.ui.activities.signupactivity.SignUpActivity
import com.azarphone.util.*
import com.azarphone.util.ConstantsUtility.ManageAccount.LOGIN_USECASE_ADDACCOUNT
import com.azarphone.validators.hasValue
import com.azarphone.validators.isValidMsisdn
import com.azarphone.widgets.popups.showMessageDialog
import com.azarphone.widgets.tooltip.initiateErrorToolTip
import kotlinx.android.synthetic.main.login_fragment.view.*


@Suppress("DEPRECATION")
@TargetApi(Build.VERSION_CODES.M)
class LoginFragment : BaseFragment<LoginFragmentBinding, LoginFactory, LoginViewModel>(), LoginEventHandlers {

    private var biometricHandler: BiometricHandler? = null
    private val fromClass = LoginFragment::class.java.name
    private lateinit var externalView: View
    private var fingerPrintAlertDialog: AlertDialog? = null
    private var fingerprintManager: FingerprintManager? = null
    private var bioMetricAuthenticationEvents: BioMetricAuthenticationEvents? = null
    private var cancelBiometric = false

    override fun onResume() {
        super.onResume()
        stopAuthentication()
        registerBiometricEventsListener()
        hideKeyboard(activity!!)
        if (mViewModel.useCase_Type.equals(LOGIN_USECASE_ADDACCOUNT, ignoreCase = true)) {
            //do not show the biometric login
            externalView.fingerPrintIV.visibility = View.INVISIBLE
        } else {
            if (Build.VERSION.SDK_INT >= 23) {
                try {
                    /**
                     * check if user has enabled the biometric sign in then get the finger print manager
                     * otherwise don't do anything
                     * */
                    if (getIsBiometricEnabledFlagFromLocalPreferences()) {
                        // Initializing Fingerprint Manager
                        fingerprintManager = activity!!.getSystemService(Context.FINGERPRINT_SERVICE) as? FingerprintManager
                        if (fingerprintManager != null && fingerprintManager!!.isHardwareDetected) {
                            if (fingerprintManager!!.hasEnrolledFingerprints()) {
                                if (getLoginAppResumeDataFromLocalPreferences() != null &&
                                        getLoginAppResumeDataFromLocalPreferences().data != null &&
                                        hasValue(getLoginAppResumeDataFromLocalPreferences().data.toString()) &&
                                        getLoginAppResumeDataFromLocalPreferences().data.customerData != null &&
                                        getLoginAppResumeDataFromLocalPreferences().data.customerData.msisdn != null &&
                                        hasValue(getCustomerDataFromLocalPreferences().msisdn)) {
                                    /**user is logged in
                                     * show him the popup for the biometric authentication*/
                                    //init the finger print management handler
                                    biometricHandler = BiometricHandler()
                                    requestFingerPrint()
                                }
                            } else {
                                showMessageDialog(context!!, activity!!,
                                        resources.getString(R.string.popup_error_title),
                                        resources.getString(R.string.biometric_no_finger_prints_enrolled_error))
                            }
                        }
                    }
                } catch (exp: Exception) {
                    logE("SastiDevice", "Since you have an sasti device, crash occurs2", fromClass, "onresume")
                }
            }
        }
    }//onResume ends

    override fun onPause() {
        if (Build.VERSION.SDK_INT >= 23) {
            stopAuthentication()
        }
        super.onPause()
    }//onPause ends

    private fun requestFingerPrint() {
        if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            //Fingerprint authentication permission not enabled
            requestFingerPrintScannerPermission()
        } else {
            if (Build.VERSION.SDK_INT >= 23) {
                showFingerPrintScannerDialog()
            }
        }
    }//requestFingerPrint ends

    private fun requestFingerPrintScannerPermission() {
        ActivityCompat.requestPermissions(activity!!,
                arrayOf(Manifest.permission.USE_FINGERPRINT),
                ConstantsUtility.PermissionConstants.FINGER_PRINT_SCANNER_PERMISSION_CODE)
    }//requestFingerPrintScannerPermission ends

    private fun registerBiometricEventsListener() {
        bioMetricAuthenticationEvents = object : BioMetricAuthenticationEvents {
            override fun onBiometricAuthenticationError(errorCode: Int, errorMessage: String) {
                logE("biometricError", "ErrorCode:::".plus(errorCode).plus(" ErrorMessage:::").plus(errorMessage), fromClass, "onBiometricAuthenticationError")
                if (!cancelBiometric) {
                    if (errorCode == FingerprintManager.FINGERPRINT_ERROR_CANCELED) {
                        //Fingerprint operation canceled.
                        context?.let {
                            Toast.makeText(
                                it,
                                resources.getString(R.string.biometric_invalid_credentials_error),
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                        stopAuthentication()
                        showFingerPrintScannerDialog()
                    } else {
                        //general error
                        context?.let {
                            Toast.makeText(
                                it,
                                resources.getString(R.string.biometric_general_error_label),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    cancelBiometric = false
                }
            }//onBiometricAuthenticationError ends

            override fun onBiometricAuthenticationSuccess() {
                //stop the authentication
                stopAuthentication()
                //start the dashboard activity
                MainActivity.start(context!!, "")
                activity!!.finish()
            }//onBiometricAuthenticationSuccess ends

            override fun onBiometricAuthenticationFailed() {
                //stop the authentication
                stopAuthentication()
                //show the general error
//                Toast.makeText(applicationContext, resources.getString(R.string.biometric_general_error_label), Toast.LENGTH_SHORT).show()
            }//onBiometricAuthenticationFailed ends
        }//bioMetricAuthenticationEvents=object:BioMetricAuthenticationEvents ends

        RootValues.setFingerPrintScannerToLoginCallBack(bioMetricAuthenticationEvents!!)
    }//registerBiometricEventsListener ends

    private fun stopAuthentication() {
        //stop the authentication
        if (biometricHandler != null) {
            biometricHandler!!.stopAuthentication()
        }

        //kill the dialog
        if (fingerPrintAlertDialog != null) {
            if (fingerPrintAlertDialog!!.isShowing) {
                fingerPrintAlertDialog!!.dismiss()
                fingerPrintAlertDialog = null
            }
        }
    }//stopAuthentication ends

    @RequiresApi(Build.VERSION_CODES.M)
    @SuppressLint("InflateParams")
    private fun showFingerPrintScannerDialog() {
        if (activity != null && !activity!!.isFinishing) {
            val dialogBuilder = AlertDialog.Builder(activity!!)
            val inflater = activity!!.layoutInflater
            val dialogView = inflater.inflate(R.layout.dialog_finger_print_scanner, null)
            dialogBuilder.setView(dialogView)

            fingerPrintAlertDialog = dialogBuilder.create()
            fingerPrintAlertDialog!!.setCancelable(false)
            fingerPrintAlertDialog!!.setCanceledOnTouchOutside(false)
            fingerPrintAlertDialog!!.show()
            fingerPrintAlertDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            val popupTitle = dialogView.findViewById(R.id.popupTitle) as TextView
            val cancelButton = dialogView.findViewById(R.id.cancelButton) as Button

            cancelButton.isSelected = true
            popupTitle.typeface = getALSNormalFont()
            cancelButton.typeface = getALSBoldFont()

            if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN) {
                //english
                cancelButton.text = "Cancel"
                popupTitle.text = "Use your biometric settings to sign in to application."
            }

            if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ) {
                //azri
                cancelButton.text = "Ləğv et"
                popupTitle.text = "Proqrama daxil olmaq üçün biometrik məlumatlarını istifadə et."
            }

            if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.RU) {
                //russian
                cancelButton.text = "Отменить"
                popupTitle.text = "Используй свои биометрические данные для входа в приложение."
            }

            cancelButton.setOnClickListener {
                fingerPrintAlertDialog!!.dismiss()
                stopAuthentication()
                cancelBiometric = true
            }

            //start finger print authentication
            fingerprintManager = activity!!.getSystemService(Context.FINGERPRINT_SERVICE) as? FingerprintManager
            if (fingerprintManager != null) {
                biometricHandler!!.starAuthentication(fingerprintManager!!)
            }
        }
    }//showFingerPrintScannerDialog ends

    override fun getViewModelClass(): Class<LoginViewModel> {
        return LoginViewModel::class.java
    }

    override fun getFactory(): LoginFactory = LoginInjectionUtils.provideLoginFactory()

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }

    override fun getLayoutId(): Int {
        return R.layout.login_fragment
    }

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        externalView = view
        setupInputFields()
        mFragmentViewDataBinding.apply {
            handler = this@LoginFragment
            viewModel = mViewModel
        }
        subscribe()
        addPrefixToUserNameField(view)

        initFingerPrintScanner()

        view.passwordET.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if (event!!.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    login()
                }//if ends
                return false
            }//onKey ends
        })//view.passwordET.setOnKeyListener ends

        view.passwordET.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    login()
                }//if ends
                return false
            }//onEditorAction ends
        })//view.passwordET.setOnEditorActionListener ends

    }//onVisible ends

    private fun initFingerPrintScanner() {
        if (Build.VERSION.SDK_INT >= 23) {
            // Initializing Fingerprint Manager
            val fingerprintManager = activity!!.getSystemService(Context.FINGERPRINT_SERVICE) as? FingerprintManager
            if (fingerprintManager != null && fingerprintManager.isHardwareDetected) {
                logE("biometric", "hardware detected:::", fromClass, "initFingerPrintScanner")
                externalView.fingerPrintIV.visibility = View.VISIBLE
            } else {
                logE("biometric", "hardware not detected:::", fromClass, "initFingerPrintScanner")
                externalView.fingerPrintIV.visibility = View.INVISIBLE
            }
        } else {
            logE("biometric", "build version not matched:::", fromClass, "initFingerPrintScanner")
            externalView.fingerPrintIV.visibility = View.INVISIBLE
        }

        externalView.fingerPrintIV.setOnClickListener {

        }//externalView.fingerPrintIV.setOnClickListener ends
    }//initFingerPrintScanner ends

    private fun setupInputFields() {
        externalView.passwordTIL.defaultHintTextColor = context!!.resources.getColorStateList(R.color.soft_pink)
        externalView.passwordET.typeface = getALSBoldFont()

        externalView.passwordET.transformationMethod = PasswordTransformationMethod.getInstance()

        externalView.mobileNumberTIL.defaultHintTextColor = context!!.resources.getColorStateList(R.color.soft_pink)
        externalView.userNameET.typeface = getALSBoldFont()
    }//setupInputFields ends

    /*
     *Event Listeners
     */
    override fun forgotPassword() {
        ForgotPasswordActivity.start(activity as LoginActivity, mViewModel.useCase_Type)
    }

    override fun login() {
        if (mViewModel.useCase_Type.equals(LOGIN_USECASE_ADDACCOUNT)) {
            var list: ArrayList<CustomerData>? = ArrayList()
            if (getUserAccountsDataFromLocalPreferences() != null && getUserAccountsDataFromLocalPreferences().usersList != null) {
                list = getUserAccountsDataFromLocalPreferences().usersList as ArrayList<CustomerData>
            }
            var msisdn = externalView.userNameET.text.toString()
            //replace the prefix
            msisdn = msisdn.replace(ConstantsUtility.UserConstants.USERNAME_PREFIX, "")

            var isMsisdnAlreadyExists = false
            if (list != null) {
                for (number in list.iterator()) {
                    if (number.msisdn == msisdn) {
                        isMsisdnAlreadyExists = true
                        break
                    }
                }
            }

            if (isMsisdnAlreadyExists) {
                showMessageDialog(activity!!.baseContext, requireActivity(),
                        resources.getString(R.string.popup_error_title),
                        resources.getString(R.string.number_already_exists))
            } else {
                proceedOnLogin()
            }

        } else {
            proceedOnLogin()
        }

    }

    override fun signUp() {
        SignUpActivity.startSignUpActivity(context!!, mViewModel.useCase_Type)
    }

    override fun fingerPrintReader() {
//        Crashlytics.getInstance().crash() // Force a crash
        hideKeyboard(activity!!)
        if (getIsBiometricEnabledFlagFromLocalPreferences()) {
            if (Build.VERSION.SDK_INT >= 23) {
                try {
                    /**
                     * check if user has enabled the biometric sign in then get the finger print manager
                     * otherwise don't do anything
                     * */
                    if (getIsBiometricEnabledFlagFromLocalPreferences()) {
                        // Initializing Fingerprint Manager
                        fingerprintManager = activity!!.getSystemService(Context.FINGERPRINT_SERVICE) as? FingerprintManager
                        if (fingerprintManager != null &&
                                fingerprintManager!!.isHardwareDetected) {
                            if (fingerprintManager!!.hasEnrolledFingerprints()) {
                                if (getLoginAppResumeDataFromLocalPreferences() != null &&
                                        getLoginAppResumeDataFromLocalPreferences().data != null &&
                                        hasValue(getLoginAppResumeDataFromLocalPreferences().data.toString()) &&
                                        getLoginAppResumeDataFromLocalPreferences().data.customerData.msisdn != null &&
                                        hasValue(getCustomerDataFromLocalPreferences().msisdn)) {
                                    /**user is logged in
                                     * show him the popup for the biometric authentication*/
                                    //init the finger print management handler
                                    biometricHandler = BiometricHandler()
                                    requestFingerPrint()
                                }
                            } else {
                                showMessageDialog(context!!, activity!!,
                                        resources.getString(R.string.popup_error_title),
                                        resources.getString(R.string.biometric_no_finger_prints_enrolled_error))
                            }
                        }
                    }
                } catch (exp: Exception) {
                    logE("SastiDevice", "Since you have an sasti device, crash occurs2", fromClass, "onresume")
                }
            }
        } else {
            showMessageDialog(context!!, activity!!, "", getLocalizedTitleForAddingBiotmetric())
        }
    }

    private fun getLocalizedTitleForAddingBiotmetric(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "Set up your biometric settings to use."
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Biometrik məlumatları istifadə etmək üçün onları tətbiqə əlavə ət."
            else -> "Настрой свои биометрические данные для использования."
        }
    }

    private fun subscribe() {
        val loginResponseData = object : Observer<LoginAppResumeResponse> {
            override fun onChanged(customer: LoginAppResumeResponse?) {
                if (activity != null && !activity!!.isFinishing) {
                    //handle the response
                    if (customer != null) {
                        if (customer.data != null) {
                            if (customer.data.customerData != null) {
                                setLoginAppResumeDataToLocalPreferences(customer)

                                var list: ArrayList<CustomerData>? = ArrayList()
                                if (mViewModel.useCase_Type.equals(LOGIN_USECASE_ADDACCOUNT, ignoreCase = true)) {

                                    //var list: ArrayList<CustomerData>? = ArrayList()
                                    if (getUserAccountsDataFromLocalPreferences() != null && getUserAccountsDataFromLocalPreferences().usersList != null) {
                                        list = getUserAccountsDataFromLocalPreferences().usersList as ArrayList<CustomerData>
                                    }

                                    logE("H2O", "called", fromClass, "subscribe")
                                    ProjectApplication.getInstance().getLocalPreferneces().deleteAllPreferences()
                                    UserDataManager.resetUserDataManager()
                                }

                                list?.add(0, customer.data.customerData)

                                if (list != null) {
                                    for (i in 0 until list.size)
                                        logE("UserListMSISDN", list.get(i).msisdn!!, fromClass, "UserAccountAdded")
                                }

                                ProjectApplication.getInstance().getLocalPreferneces().setResponse(
                                        ProjectApplication.getInstance().getLocalPreferneces().KEY_USER_ACCOUNTS_DATA,
                                        UserAccounts(list)
                                )

                                //save the data in the session
                                UserDataManager.saveCustomerData(customer.data.customerData)
                                UserDataManager.savePredefineData(customer.data.predefinedData)

                                //save the response in shared preferences
                                setCustomerDataToLocalPreferences(customer.data.customerData)

                                //set the terms and conditions
                                if (customer.data.predefinedData != null) {
                                    if (customer.data.predefinedData != null) {
                                        if (hasValue(customer.data.predefinedData!!.tnc!!.content)) {
                                            UserDataManager.setTermsAndConditionsDataString(customer.data.predefinedData!!.tnc!!.content)
                                        }
                                    }
                                }

                                //save the time of the login
                                logE("timeX", "time1:::".plus(getUserLoginTime()), fromClass, "currentDateTime")
                                setUserLoginTime(getCurrentDateTimeForRateUs())
                                logE("timeX", "time2:::".plus(getUserLoginTime()), fromClass, "currentDateTime")
                                //no go to the next activity
                                MainActivity.start(activity!!, ConstantsUtility.BundleKeys.BUNDLE_DONT_CALL_APP_RESUME)

                            }
                        }
                    }
                }
            }
        }
        mViewModel.loginResponseData.observe(activity!!, loginResponseData)
    }//subscribe ends

    override fun onInVisible() {
    }//onInVisible ends

    private fun proceedOnLogin() {
        hideKeyboard(activity!!)
        logE("proceedOnLogin", "called->proceedOnLogin", fromClass, "proceedOnLogin")
        var msisdn = externalView.userNameET.text.toString()
        //replace the prefix
        msisdn = msisdn.replace(ConstantsUtility.UserConstants.USERNAME_PREFIX, "")
        val password = externalView.passwordET.text.toString()

        //validations
        if (!isValidMsisdn(msisdn)) {
            //number is incorrect
            initiateErrorToolTip(context!!, externalView.userNameET, context!!.resources.getString(R.string.invalid_number))
            return
        }//if (!isValidMsisdn(msisdn)) ends

        if (!hasValue(password)) {
            //password not entered
            initiateErrorToolTip(context!!, externalView.passwordET, context!!.resources.getString(R.string.login_invalid_password_tooltip))
            return
        }//if (!hasValue(password)) ends

        if (password.length in 6..15) {

        } else {
            //password length not correct
            initiateErrorToolTip(context!!, externalView.passwordET, context!!.resources.getString(R.string.login_invalid_password_tooltip))
            return
        }

        //call the login webservice
        mViewModel.processUserLogin(msisdn, password)
    }//proceedOnLogin ends

    private fun addPrefixToUserNameField(view: View) {
        view.userNameET.setText(ConstantsUtility.UserConstants.USERNAME_PREFIX)
        Selection.setSelection(view.userNameET.text, view.userNameET.text!!.length)
        view.userNameET.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (!s.toString().startsWith(ConstantsUtility.UserConstants.USERNAME_PREFIX)) {
                    view.userNameET.setText(ConstantsUtility.UserConstants.USERNAME_PREFIX)
                    Selection.setSelection(view.userNameET.text, view.userNameET.text!!.length)

                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
    }//addPrefixToUserNameField ends

}//class ends