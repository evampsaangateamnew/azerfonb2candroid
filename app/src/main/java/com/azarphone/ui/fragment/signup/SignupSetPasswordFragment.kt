package com.azarphone.ui.fragment.signup

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.*
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.SignUpSetPasswordFragmentBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.SignupActivityToolTipEvents
import com.azarphone.ui.activities.signupactivity.SignUpActivity
import com.azarphone.ui.activities.signupactivity.SignUpFactory
import com.azarphone.ui.activities.signupactivity.SignUpInjectionUtils
import com.azarphone.ui.activities.signupactivity.SignUpViewModel
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.RootValues
import com.azarphone.util.getALSBoldFont
import com.azarphone.validators.hasValue
import com.azarphone.widgets.popups.showMessageDialog
import com.azarphone.widgets.tooltip.initiateErrorToolTip
import io.canner.stepsview.StepsView
import kotlinx.android.synthetic.main.sign_up_set_password_fragment.view.*


/**
 * @author Junaid Hassan on 12, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class SignupSetPasswordFragment : BaseFragment<SignUpSetPasswordFragmentBinding, SignUpFactory, SignUpViewModel>() {

    private val fromClass = "SignupSetPasswordFragment"
    private var signupActivityToolTipEvents: SignupActivityToolTipEvents? = null
    private lateinit var externalView: View

    override fun getLayoutId(): Int {
        return R.layout.sign_up_set_password_fragment
    }

    override fun getViewModelClass(): Class<SignUpViewModel> {
        return SignUpViewModel::class.java
    }

    override fun getFactory(): SignUpFactory {
        return SignUpInjectionUtils.provideSignUpFactory()
    }

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }

    override fun onVisible(view: View, savedInstanceState: Bundle?) {

        externalView = view

        mViewModel.isTermsAndCondtionsAccepted.value = false
        setupInputFields()

        mFragmentViewDataBinding.apply {
            viewmodel = mViewModel
        }

        registerListener()

        mViewModel.setSignupPasswordStep(ConstantsUtility.SignupScreenSteps.SET_PASSWORD_STEP)
        mViewModel.setTermsConditionsCheck(false)
        //ui events
        view.termsConditionsLabel.setOnClickListener {
            var terms = ""
            if (hasValue(UserDataManager.getTermsAndConditionsDataString())) {
                terms = "<html><body><style type='text/css'>@font-face {font-family: 'ALSSchlangesans';src: url('file:///android_asset/fonts/af/alsschlangesans.otf')} body {font-family: 'ALSSchlangesans';}</style>" + UserDataManager.getTermsAndConditionsDataString() + "</body></html>";
            }
            showTermsAndConditionsPopup(terms)
        }//view.termsConditionsLabel.setOnClickListener ends

        activity?.let {
            val stepView = it.findViewById<StepsView>(R.id.stepsView).setCompletedPosition(2).drawView()
        }//activity?.let  ends

        mViewModel.set25SecondsTimerOff()

        /*view.reenterPasswordET.setOnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
//                mViewModel.requestSaveCustomer()

            }
            false
        }

        view.reenterPasswordET.setOnEditorActionListener { _, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
//                mViewModel.requestSaveCustomer()

            }
            false
        }*/

        view.reenterPasswordET.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if (event!!.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (mViewModel.isTermsAndCondtionsAccepted.value == false) {
                        showMessageDialog(context!!, activity!!, "", getLocalizedErrorLabel())
                    } else {
                        (activity as SignUpActivity).processOnSignup()
                    }

                }//if ends
                return false
            }//onKey ends
        })//view.passwordET.setOnKeyListener ends

        externalView.reenterPasswordET.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (mViewModel.isTermsAndCondtionsAccepted.value == false) {
                        showMessageDialog(context!!, activity!!, "", getLocalizedErrorLabel())
                    } else {
                        (activity as SignUpActivity).processOnSignup()
                    }
                }//if ends
                return false
            }//onEditorAction ends
        })

        externalView.acceptTermsCheckBox.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                mViewModel.isTermsAndCondtionsAccepted.value = isChecked
            }

        })
    }

    private fun getLocalizedErrorLabel(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "Please accept Terms&Conditions"
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Zəhmət olmasa Şərtlər və qaydalar qəbul et"
            else -> "Пожалуйста, прими Правила и условия"
        }
    }

    private fun setupInputFields() {
        externalView.enterPasswordTIL.defaultHintTextColor = context!!.resources.getColorStateList(R.color.soft_pink)
        externalView.enterPasswordET.typeface = getALSBoldFont()
        externalView.enterPasswordET.transformationMethod = PasswordTransformationMethod.getInstance()

        externalView.reenterPasswordTIL.defaultHintTextColor = context!!.resources.getColorStateList(R.color.soft_pink)
        externalView.reenterPasswordET.typeface = getALSBoldFont()
        externalView.reenterPasswordET.transformationMethod = PasswordTransformationMethod.getInstance()
    }//setupInputFields ends

    private fun registerListener() {
        signupActivityToolTipEvents = object : SignupActivityToolTipEvents {
            override fun onInvalidPasswordTooltip(errorDetail: String) {
                initiateErrorToolTip(context!!, externalView.enterPasswordET, errorDetail)
            }

            override fun onInvalidConfirmPasswordTooltip(errorDetail: String) {
                initiateErrorToolTip(context!!, externalView.reenterPasswordET, errorDetail)
            }

            override fun onInvalidOTPPINTooltip(errorDetail: String) {
                //do nothing
            }

            override fun onInvalidMsisdnNumberTooltip(errorDetail: String) {
                //do nothing
            }
        }

        RootValues.setSignupActivityToolTipEvents(signupActivityToolTipEvents!!)
    }

    override fun onInVisible() {

    }

    @SuppressLint("InflateParams", "SetJavaScriptEnabled")
    fun showTermsAndConditionsPopup(termsAndConditionsHtml: String) {

        try {
            if (context == null || (context as Activity).isFinishing) return

            val dialogBuilder = android.app.AlertDialog.Builder(context)
            val inflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val dialogView = inflater.inflate(R.layout.terms_and_conditions_pop_up, null)
            dialogBuilder.setView(dialogView)

            val alertDialog = dialogBuilder.create()
            alertDialog.show()
            alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            val webView = dialogView.findViewById(R.id.termsConditionsWebView) as WebView
            val corneredCloserButton = dialogView.findViewById(R.id.corneredCloserButton) as ImageView
            val closeButton = dialogView.findViewById(R.id.closeButton) as Button

            if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN) {
                //english
                closeButton.text = "I accept"
            }

            if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ) {
                //azri
                closeButton.text = "Qəbul edirəm"
            }

            if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.RU) {
                //russian
                closeButton.text = "Я принимаю"
            }

            webView.setBackgroundColor(context!!.resources.getColor(R.color.colorTermsConditionsPopupBg))
            webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null)

            webView.webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView, url: String) {
                    view.setBackgroundColor(context!!.resources.getColor(R.color.colorTermsConditionsPopupBg))
                    view.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null)
                }
            }

            webView.settings.javaScriptEnabled = true
            webView.loadDataWithBaseURL("", termsAndConditionsHtml, "text/html", "UTF-8", "")

            corneredCloserButton.setOnClickListener { alertDialog.dismiss() }
            closeButton.setOnClickListener { alertDialog.dismiss() }
        } catch (e: Exception) {
            e.stackTrace
        }
    }//showTermsAndConditionsPopup ends

}