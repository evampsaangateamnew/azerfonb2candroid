package com.azarphone.ui.fragment.signup

import android.os.Bundle
import android.text.Editable
import android.text.Selection
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.azarphone.R
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.SignUpEnterMobileNumberBinding
import com.azarphone.eventhandler.SignupActivityToolTipEvents
import com.azarphone.ui.activities.signupactivity.SignUpActivity
import com.azarphone.ui.activities.signupactivity.SignUpFactory
import com.azarphone.ui.activities.signupactivity.SignUpInjectionUtils
import com.azarphone.ui.activities.signupactivity.SignUpViewModel
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.RootValues
import com.azarphone.util.getALSBoldFont
import com.azarphone.util.logE
import com.azarphone.widgets.tooltip.initiateErrorToolTip
import io.canner.stepsview.StepsView
import kotlinx.android.synthetic.main.sign_up_enter_mobile_number.view.*

@Suppress("DEPRECATION")
class SignupMobileNumberFragment : BaseFragment<SignUpEnterMobileNumberBinding, SignUpFactory, SignUpViewModel>() {

    private var signupActivityToolTipEvents: SignupActivityToolTipEvents? = null

    private lateinit var externalView: View

    override fun getViewModelClass(): Class<SignUpViewModel> {
        return SignUpViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): SignUpFactory {
        return SignUpInjectionUtils.provideSignUpFactory()
    }//getFactory ends

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }//isBindedToActivityLifeCycle ends

    override fun getLayoutId(): Int {
        return R.layout.sign_up_enter_mobile_number
    }//getLayoutId ends

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        externalView = view
        setupInputFields()
        registerListener()
        mViewModel.isTermsAndCondtionsAccepted.value = true
        mFragmentViewDataBinding.apply {
            viewmodel = mViewModel
        }

        addPrefixToUserNameField(view)
        mViewModel.setSignupPasswordStep(ConstantsUtility.SignupScreenSteps.ENTER_NUMBER_STEP)

        activity?.let {
            val stepView = it.findViewById<StepsView>(R.id.stepsView).setCompletedPosition(0).drawView()
        }//activity?.let  ends

        mViewModel.set25SecondsTimerOff()

        view.userNameET.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if (event!!.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    (activity as SignUpActivity).processOnSignup()
                }//if ends
                return false
            }//onKey ends
        })//view.passwordET.setOnKeyListener ends

        externalView.userNameET.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    (activity as SignUpActivity).processOnSignup()
                }//if ends
                return false
            }//onEditorAction ends
        })
    }//onVisible ends

    private fun setupInputFields() {
        externalView.mobileNumberTIL.defaultHintTextColor = context!!.resources.getColorStateList(R.color.soft_pink)
        externalView.userNameET.typeface = getALSBoldFont()
    }//setupInputFields ends

    private fun registerListener() {
        signupActivityToolTipEvents = object : SignupActivityToolTipEvents {
            override fun onInvalidPasswordTooltip(errorDetail: String) {
                //do nothing
            }

            override fun onInvalidConfirmPasswordTooltip(errorDetail: String) {
                //do nothing
            }

            override fun onInvalidOTPPINTooltip(errorDetail: String) {
                //do nothing here
            }

            override fun onInvalidMsisdnNumberTooltip(errorDetail: String) {
                logE("toolerX", "detail:::".plus(errorDetail), "SignupMobileNumberFragment", "onInvalidMsisdnNumberTooltip")
                initiateErrorToolTip(context!!, externalView.userNameET, errorDetail)
            }
        }

        RootValues.setSignupActivityToolTipEvents(signupActivityToolTipEvents!!)
    }

    override fun onInVisible() {
    }//onInVisible ends

    private fun addPrefixToUserNameField(view: View) {
        view.userNameET.setText(ConstantsUtility.UserConstants.USERNAME_PREFIX)
        Selection.setSelection(view.userNameET.text, view.userNameET.text!!.length)
        view.userNameET.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (!s.toString().startsWith(ConstantsUtility.UserConstants.USERNAME_PREFIX)) {
                    view.userNameET.setText(ConstantsUtility.UserConstants.USERNAME_PREFIX)
                    Selection.setSelection(view.userNameET.text, view.userNameET.text!!.length)

                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
    }//addPrefixToUserNameField ends

}//class ends
