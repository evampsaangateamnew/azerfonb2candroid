package com.azarphone.ui.fragment.myaccount

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.azarphone.R
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.LayoutMyaccountBinding
import com.azarphone.ui.activities.mainactivity.MainFactory
import com.azarphone.ui.activities.mainactivity.MainInjection
import com.azarphone.ui.activities.mainactivity.MainViewModel
import com.azarphone.ui.adapters.recyclerviews.MyAccountMenuRecyclerAdapter
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.RootValues
import com.azarphone.util.getMyAccountMenuModel
import com.azarphone.util.logE
import com.azarphone.validators.hasValue
import kotlinx.android.synthetic.main.layout_myaccount.*

class MyAccountFragment : BaseFragment<LayoutMyaccountBinding, MainFactory, MainViewModel>() {

    //Class Name variable
    private val fromClass = MyAccountFragment::class.simpleName.toString()

    //Bundle variables
    private var matchedSubScreen = ""
    private var tabToRedirect = ""
    private var offeringIdToRedirect = ""

    override fun getLayoutId(): Int {
        return R.layout.layout_myaccount
    }//getLayoutId ends

    override fun getViewModelClass(): Class<MainViewModel> {
        return MainViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): MainFactory {
        return MainInjection.provideMainFactory()
    }//getFactory ends

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }//isBindedToActivityLifeCycle ends

    override fun onResume() {
        super.onResume()
        if (RootValues.getAzerFonActionBarEvents() != null) {
            RootValues.getAzerFonActionBarEvents()!!.onMyAccountMenuSetActionBar()
        }
    }//onResume ends

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        if (activity != null && !activity!!.isFinishing) {

            if (!getMyAccountMenuModel().isNullOrEmpty()) {
                //Getting data from arguments
                getArgumentsData()
                //Setting up recyclerview
                innerMenuRcycler.apply {
                    layoutManager =
                        LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                    adapter = MyAccountMenuRecyclerAdapter(
                        context!!, getMyAccountMenuModel()!!,
                        matchedSubScreen,
                        tabToRedirect,
                        offeringIdToRedirect
                    )
                }

            } else {
                noDataFoundLayout.visibility = View.VISIBLE
                innerMenuRcycler.visibility = View.GONE
            }
        }
    }//onVisible ends

    private fun getArgumentsData() {
        //Getting values from argument bundle
        arguments?.let {

            if (it.containsKey(ConstantsUtility.DynamicLinkConstants.SUB_SCREEN_NAME_DATA)
                && hasValue(it.getString(ConstantsUtility.DynamicLinkConstants.SUB_SCREEN_NAME_DATA))
            ) {
                val subScreen =
                    it.getString(ConstantsUtility.DynamicLinkConstants.SUB_SCREEN_NAME_DATA)
                setMatchedSubScreenValue(subScreen!!)
            }

            if (it.containsKey(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA)
                && hasValue(it.getString(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA))
            ) {
                tabToRedirect = it.getString(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA)!!
            }

            if (it.containsKey(ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA)
                && hasValue(it.getString(ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA))
            ) {
                offeringIdToRedirect =
                    it.getString(ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA)!!
            }
        }

        //Logging final received values from argument bundle
        logE(
            ConstantsUtility.DynamicLinkConstants.DYNAMIC_LINK_TAG,
            "matchedSubScreen = $matchedSubScreen tabToRedirect = $tabToRedirect" +
                    "offeringIdToRedirect = $offeringIdToRedirect",
            fromClass,
            "getArgumentsData"
        )
    }//getArgumentsData ends

    private fun setMatchedSubScreenValue(subScreen: String) {
        when {
            subScreen.equals(
                ConstantsUtility.MyAccountChildMenusIdentifiers.MYACCOUNT_USAGE_HISTORY,
                ignoreCase = true
            ) -> {
                matchedSubScreen =
                    ConstantsUtility.MyAccountChildMenusIdentifiers.MYACCOUNT_USAGE_HISTORY
            }
            subScreen.equals(
                ConstantsUtility.MyAccountChildMenusIdentifiers.MYACCOUNT_OPERATIONS_HISTORY,
                ignoreCase = true
            ) -> {
                matchedSubScreen =
                    ConstantsUtility.MyAccountChildMenusIdentifiers.MYACCOUNT_OPERATIONS_HISTORY
            }
            subScreen.equals(
                ConstantsUtility.MyAccountChildMenusIdentifiers.MYACCOUNT_MY_SUBSCRIPTION,
                ignoreCase = true
            ) -> {
                matchedSubScreen =
                    ConstantsUtility.MyAccountChildMenusIdentifiers.MYACCOUNT_MY_SUBSCRIPTION
            }
        }
    }//setMatchedSubScreenValue ends

    override fun onInVisible() {
    }//onInVisible ends


}