package com.azarphone.ui.fragment.storelocator

import android.Manifest
import androidx.lifecycle.Observer
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.core.content.ContextCompat
import android.view.View
import com.azarphone.R
import com.azarphone.api.pojo.response.storelocator.StoreLocatorResponse
import com.azarphone.api.pojo.response.storelocator.StoresItem
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.LayoutMapFragmentBinding
import com.azarphone.ui.activities.mainactivity.MainFactory
import com.azarphone.ui.activities.mainactivity.MainInjection
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import android.widget.ArrayAdapter
import android.location.Location
import android.util.Log
import android.widget.AdapterView
import com.azarphone.ProjectApplication
import com.azarphone.ui.activities.storelocator.StoreLocatorViewModel
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.logE
import com.azarphone.validators.isNumeric
import com.google.android.gms.maps.model.LatLngBounds
import java.util.*

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.*


class MapFragment : BaseFragment<LayoutMapFragmentBinding, MainFactory, StoreLocatorViewModel>(),
    OnMapReadyCallback {


    lateinit var mapView: MapView
    lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    var isMapReady = false
    private var isFound = false
    val LOCATION_PERMISSION_REQUEST_CODE = 1001


    override fun getLayoutId(): Int {
        return R.layout.layout_map_fragment
    }

    override fun getViewModelClass(): Class<StoreLocatorViewModel> {
        return StoreLocatorViewModel::class.java
    }

    override fun getFactory(): MainFactory {
        return MainInjection.provideMainFactory()
    }

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        if (activity != null && !activity!!.isFinishing) {
            // Gets the MapView from the XML layout and creates it
            mapView = view.findViewById(R.id.map)
            mapView.onCreate(savedInstanceState)
            mapView.getMapAsync(this)
            subscribe()
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(context!!)
        }
    }


    override fun onResume() {
        mapView.onResume()
        super.onResume()
    }


    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }


    override fun onInVisible() {
    }


    fun subscribe() {
        val storeObserver = object : Observer<StoreLocatorResponse> {
            override fun onChanged(t: StoreLocatorResponse?) {
                if (activity != null && !activity!!.isFinishing) {
                    t?.let {
                        if (ContextCompat.checkSelfPermission(
                                context!!,
                                Manifest.permission.ACCESS_FINE_LOCATION
                            )
                            == PackageManager.PERMISSION_GRANTED
                        ) {
                            try {
                                if (map != null) {
                                    map.setMyLocationEnabled(true)
                                    map.uiSettings.isMyLocationButtonEnabled = true
                                }
                            } catch (e: Exception) {

                            }

                            // mViewModel.getStoreLocator()
                        } else {
                            try {
                                if (map != null) {
                                    map.setMyLocationEnabled(false)
                                }
                            } catch (e: Exception) {

                            }

                        }
                        setMarkers(it)
                        setSpinners(it)
                        setNearestPoints(it.data.stores as ArrayList<StoresItem>)

                    }
                }
            }
        }

        mViewModel.storeLocatorResponse.observe(this, storeObserver)
    }


    fun setNearestPoints(storeLocators: ArrayList<StoresItem>) {
        if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED
        ) {
            fusedLocationClient.lastLocation.addOnSuccessListener {
                if (it != null) {
                    val latLng = LatLng(it.latitude, it.longitude)
                    val list = sortListbyDistance(storeLocators, latLng)

                    showNearestStoresUI()
                    populateNearestStoreToContainer(list, 3, latLng)
                }
            }
        }
    }

    private fun showNearestStoresUI() {
        mFragmentViewDataBinding.nearestStoreTV.visibility = View.VISIBLE
        mFragmentViewDataBinding.nearestStoreValuesTV.visibility = View.VISIBLE
        mFragmentViewDataBinding.nearestStoreValues2TV.visibility = View.VISIBLE
        mFragmentViewDataBinding.nearestStoreValues3TV.visibility = View.VISIBLE
    }


    private fun populateNearestStoreToContainer(
        nearestStores: ArrayList<StoresItem>,
        i: Int,
        myPostion: LatLng
    ) {

        mFragmentViewDataBinding.nearestStoreValuesTV.isSelected = true
        mFragmentViewDataBinding.nearestStoreValuesTV.text =
            nearestStores[0].store_name + " " + distance(
                nearestStores[0].latitude!!.toDouble(),
                nearestStores[0]!!.longitude!!.toDouble(),
                myPostion.latitude,
                myPostion.longitude
            ).toInt() + "Km"
        mFragmentViewDataBinding.nearestStoreValuesTV.setOnClickListener {

            val latlng = LatLng(
                nearestStores[0].latitude!!.toDouble(),
                nearestStores[0].longitude!!.toDouble()
            )
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, 15f))

        }

        mFragmentViewDataBinding.nearestStoreValues2TV.isSelected = true
        mFragmentViewDataBinding.nearestStoreValues2TV.text =
            nearestStores[1].store_name + " " + distance(
                nearestStores[1].latitude!!.toDouble(),
                nearestStores[1]!!.longitude!!.toDouble(),
                myPostion.latitude,
                myPostion.longitude
            ).toInt() + "Km"
        mFragmentViewDataBinding.nearestStoreValues2TV.setOnClickListener {

            val latlng = LatLng(
                nearestStores[1].latitude!!.toDouble(),
                nearestStores[1].longitude!!.toDouble()
            )
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, 15f))
        }
        mFragmentViewDataBinding.nearestStoreValues3TV.isSelected = true
        mFragmentViewDataBinding.nearestStoreValues3TV.text =
            nearestStores[2].store_name + " " + distance(
                nearestStores[2].latitude!!.toDouble(),
                nearestStores[2]!!.longitude!!.toDouble(),
                myPostion.latitude,
                myPostion.longitude
            ).toInt() + "Km"
        mFragmentViewDataBinding.nearestStoreValues3TV.setOnClickListener {

            val latlng = LatLng(
                nearestStores[2].latitude!!.toDouble(),
                nearestStores[2].longitude!!.toDouble()
            )
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, 15f))
        }

    }//populateNearestStoreToContainer ends


    private fun distance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
        val theta = lon1 - lon2
        var dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + (Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta)))
        dist = Math.acos(dist)
        dist = rad2deg(dist)
        dist = dist * 60.0 * 1.1515
        return dist
    }

    private fun deg2rad(deg: Double): Double {
        return deg * Math.PI / 180.0
    }

    private fun rad2deg(rad: Double): Double {
        return rad * 180.0 / Math.PI
    }


    fun setSpinners(mStoreListResponse: StoreLocatorResponse) {

        var allString = getLocalizedAllTitle()

        val language = ProjectApplication.mLocaleManager.getLanguage()
        when (language) {
            ConstantsUtility.LanguageKeywords.EN -> allString = "All"
            ConstantsUtility.LanguageKeywords.AZ -> allString = "Hamısı"
            ConstantsUtility.LanguageKeywords.RU -> allString = "Все"
        }

        var mCityList = mStoreListResponse.data.city as ArrayList<String>
        mCityList.add(0, allString)
        var mTypeList = mStoreListResponse.data.type as ArrayList<String>
        mTypeList.add(0, allString)

        val citiesAdapter = ArrayAdapter<String>(context!!, R.layout.spinner_item, mCityList)
        citiesAdapter.setDropDownViewResource(R.layout.spinner_dropdown)
        mFragmentViewDataBinding.citySpinner.setAdapter(citiesAdapter)
        mFragmentViewDataBinding.citySpinner.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                //showOnlyCity(position)
                var city =
                    mFragmentViewDataBinding.citySpinner.getItemAtPosition(position).toString()
                if (position == 0) {
                    city = getLocalizedAllTitle()
                }

                val category = mFragmentViewDataBinding.catogerySpinner.selectedItem.toString()
                showByCityCategoryFilter(city, category)
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }
        })

        val typeAdapter = ArrayAdapter<String>(context!!, R.layout.spinner_item, mTypeList)
        typeAdapter.setDropDownViewResource(R.layout.spinner_dropdown)
        mFragmentViewDataBinding.catogerySpinner.setAdapter(typeAdapter)

        mFragmentViewDataBinding.catogerySpinner.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                // showOnlyCategory(position)
                val city = mFragmentViewDataBinding.citySpinner.selectedItem.toString()
                var category =
                    mFragmentViewDataBinding.catogerySpinner.getItemAtPosition(position).toString()
                if (position == 0) {
                    category = getLocalizedAllTitle()
                }
                showByCityCategoryFilter(city, category)
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }
        })

    }

    private fun getLocalizedAllTitle(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "All"
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Hamısı"
            else -> "Все"
        }
    }

    /* fun showOnlyCategory(position: Int) {
         if (position == 0) {
             setMarkers(mViewModel.storeLocatorResponse.value!!)
             return
         }
         mViewModel.storeLocatorResponse.value!!.data.stores?.let {
             val catogery = mViewModel.storeLocatorResponse.value!!.data.type!![position]

             val list = ArrayList<StoresItem>()

             for (storeItem in it) {
                 if (storeItem.type.equals(catogery)) {
                     list.add(storeItem)
                 }
             }

             setMarkers(list)
         }
     }


     fun showOnlyCity(position: Int) {


         if (position == 0) {
             setMarkers(mViewModel.storeLocatorResponse.value!!)
             return
         }

         mViewModel.storeLocatorResponse.value!!.data.stores?.let {
             val city = mViewModel.storeLocatorResponse.value!!.data.city!![position]


             val list = ArrayList<StoresItem>()

             for (storeItem in it) {

                 if (storeItem.city.equals(city)) {
                     list.add(storeItem)
                 }

             }

             setMarkers(list)
         }

     }*/

    fun showByCityCategoryFilter(city: String, category: String) {

        //Log.d("Spinner", "City11111111:" + city + " Category:" + category)
        isFound = false
        // val position=2
        /*  mFragmentViewDataBinding.catogerySpinner.getItemAtPosition(position).toString()
          mFragmentViewDataBinding.citySpinner.getItemAtPosition(position).toString()*/

        /*   if (position == 0) {
               setMarkers(mViewModel.storeLocatorResponse.value!!)
               return
           }*/

        mViewModel.storeLocatorResponse.value!!.data.stores?.let {
            /* val city = mViewModel.storeLocatorResponse.value!!.data.city!![position]
             val catogery = mViewModel.storeLocatorResponse.value!!.data.type!![position]*/

            val list = ArrayList<StoresItem>()

            for (storeItem in it) {

                if (city.equals(getLocalizedAllTitle(), ignoreCase = true) && category.equals(
                        getLocalizedAllTitle(),
                        ignoreCase = true
                    )
                ) {
                    list.add(storeItem)
                    //Log.d("Spinner", "City:ALL Category:All")
                    isFound = true
                } else if (category.equals(getLocalizedAllTitle(), ignoreCase = true)) {
                    if (storeItem.city.equals(city)) {
                        list.add(storeItem)
                        isFound = true
                        //Log.d("Spinner", "City:" + city + "Category:All")
                    }
                } else if (city.equals(getLocalizedAllTitle(), ignoreCase = true)) {
                    if (storeItem.type.equals(category)) {
                        list.add(storeItem)
                        isFound = true
                        //Log.d("Spinner", "City:All" + "Category:" + category)
                    }
                } else {
                    if (storeItem.city.equals(city) && storeItem.type.equals(category)) {
                        list.add(storeItem)
                        isFound = true
                        //Log.d("Spinner", "City:" + city + " Category:" + category)
                    }
                }


            }

            if (!isFound) {
                list.clear()
            }

            if (list.size > 0) {
                setMarkers(list)
            } else {
                if (!isMapReady) {
                    return
                }
                map.clear()
            }
        }

    }

    private fun sortListbyDistance(
        storeLocators: ArrayList<StoresItem>,
        location: LatLng
    ): ArrayList<StoresItem> {
        var arrayList = ArrayList<StoresItem>()
        try {
            arrayList = storeLocators
            arrayList.sortWith(Comparator { o1, o2 ->
                if (calculateDistanceBetweenTwoLocation(
                        o1.latitude!!.toDouble(),
                        o1.longitude!!.toDouble(),
                        location.latitude,
                        location.longitude
                    ) > calculateDistanceBetweenTwoLocation(
                        o2.latitude!!.toDouble(),
                        o2.longitude!!.toDouble(), location.latitude, location.longitude
                    )
                ) {
                    1
                } else {
                    -1
                }
            })
        } catch (exp: Exception) {
        }
        return arrayList
    }


    private fun calculateDistanceBetweenTwoLocation(
        firstLatitude: Double,
        firstLongitude: Double,
        secondLatitude: Double,
        secondLongitude: Double
    ): Float {
        val results = FloatArray(1)
        Location.distanceBetween(
            firstLatitude,
            firstLongitude,
            secondLatitude,
            secondLongitude,
            results
        )
        return results[0]
    }//calculateDistanceBetweenTwoLocation ends


    fun setMarkers(mapApiResponse: StoreLocatorResponse) {
        if (!isMapReady) {
            return
        }
        map.clear()
        val builder = LatLngBounds.Builder();
        val latLongBounds = LatLngBounds.Builder()
        var markerCount = 0
        mapApiResponse.data.stores?.let {
            for (storeitem in it) {

                storeitem.latitude?.let { lat ->

                    storeitem.longitude?.let { log ->
                        val latLng =
                            LatLng(
                                lat.toDouble(),
                                log.toDouble()
                            );
                        val markerOptions = MarkerOptions()
                        markerOptions.position(latLng)
                        markerOptions.title(storeitem.store_name)
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.store_icon))
                        builder.include(latLng)
                        map.addMarker(markerOptions)
                        val location =
                            LatLng(
                                lat.toDouble(),
                                log.toDouble()
                            )
                        latLongBounds.include(location)
                        markerCount += 1
                    }
                }
            }
        }

        val camZoom: CameraUpdate

        try {
            if (mapApiResponse != null && mapApiResponse.data != null && mapApiResponse.data.stores != null) {

                if (markerCount > 1) {
                    // try {
                    val bounds = latLongBounds.build()
                    val width = resources.displayMetrics.widthPixels
                    val height = resources.displayMetrics.heightPixels
                    val padding =
                        (width * 0.30).toInt() // offset from edges of the map 10% of screen
                    //            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
                    camZoom = CameraUpdateFactory.newLatLngBounds(bounds, 30)
                } else {
                    val lat = getFloatFromString(mapApiResponse!!.data!!.stores!![0].latitude!!)
                    val lng = getFloatFromString(mapApiResponse!!.data!!.stores!![0].longitude!!)
                    val latLng = LatLng(lat.toDouble(), lng.toDouble())

                    camZoom = CameraUpdateFactory.newLatLngZoom(latLng, 15f)
                }

                map.animateCamera(camZoom)

            }
        } catch (exp: Exception) {
            val padding = 0
            val bounds = builder.build()
            val cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding)
            map.animateCamera(cameraUpdate)
        }

    }


    private fun setMarkers(mList: ArrayList<StoresItem>) {
        if (!isMapReady) {
            return
        }
        map.clear()
        val latLongBounds = LatLngBounds.Builder()

        val builder = LatLngBounds.Builder();
        var markerCount = 0
        for (storeitem in mList) {
            storeitem.latitude?.let { lat ->

                storeitem.longitude?.let { log ->
                    Log.e(
                        "location_",
                        "latitude = ${storeitem.latitude} ,  long = ${storeitem.longitude} city- = ${storeitem.city}"
                    )
                    val latLng = LatLng(lat.toDouble(), log.toDouble());
                    val markerOptions = MarkerOptions()
                    markerOptions.position(latLng)
                    markerOptions.title(storeitem.store_name)
                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.store_icon))
                    builder.include(latLng)
                    map.addMarker(markerOptions)
                    val location = LatLng(lat.toDouble(), log.toDouble())
                    latLongBounds.include(location)
                    markerCount += 1
                }

            }

        }

        val camZoom: CameraUpdate

        try {
            if (markerCount > 1) {
                // try {
                val bounds = latLongBounds.build()
                val width = resources.displayMetrics.widthPixels
                val height = resources.displayMetrics.heightPixels
                val padding = (width * 0.30).toInt() // offset from edges of the map 10% of screen
                //            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
                camZoom = CameraUpdateFactory.newLatLngBounds(bounds, 30)
            } else {
                val lat = getFloatFromString(mList[0].latitude!!)
                val lng = getFloatFromString(mList[0].longitude!!)
                val latLng = LatLng(lat.toDouble(), lng.toDouble())

                camZoom = CameraUpdateFactory.newLatLngZoom(latLng, 15f)
            }

            map.animateCamera(camZoom)

        } catch (exp: Exception) {
            val padding = 0
            val bounds = builder.build()
            val cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding)
            map.animateCamera(cameraUpdate)
        }

    }

    private fun getFloatFromString(string: String): Float {
        var value = -1f
        try {
            if (isNumeric(string)) {
                value = java.lang.Float.valueOf(string)!!
            }
        } catch (e: NumberFormatException) {
            logE("float", "error:::".plus(e.message), "FloatFromStringTool", "getFloatFromString")
        }

        return value
    }


    private fun setGoogleMapZoomOut(googleMap: GoogleMap?) {
        if (googleMap == null) return
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(1.0f))
    }//setGoogleMapZoomOut ends

    override fun onMapReady(googleMap: GoogleMap?) {
        isMapReady = true
        googleMap?.let {
            map = googleMap
            map.getUiSettings().setMyLocationButtonEnabled(false)
            if (ContextCompat.checkSelfPermission(
                    context!!,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
                == PackageManager.PERMISSION_GRANTED
            ) {
                map.setMyLocationEnabled(true)
                // mViewModel.getStoreLocator()
            } else {
                map.setMyLocationEnabled(false)

            }
        }
    }

    /*
     No need for check we already got the permission
     */

    /*@SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
       *//* if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            }
        }*//*
        when (requestCode) {

            LOCATION_PERMISSION_REQUEST_CODE -> {

                if (requestCode == LOCATION_PERMISSION_REQUEST_CODE && grantResults != null && grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (ContextCompat.checkSelfPermission(context!!,
                                    Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        map.setMyLocationEnabled(true)
                        mViewModel.getStoreLocator()
                    }
                } else {
                    mViewModel.getStoreLocator()
                }
            }
        }
    }*/

}