package com.azarphone.ui.fragment.usagehistory

import android.app.DatePickerDialog
import androidx.lifecycle.Observer
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.TextView
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.response.otpverifyresponse.OTPVerifyResponse
import com.azarphone.api.pojo.response.usagedetailsresponse.RecordsItem
import com.azarphone.api.pojo.response.usagedetailsresponse.UsageDetailsResponse
import com.azarphone.api.pojo.response.verifypassportresponse.VerifyPassportResponse
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.LayoutUsageHistoryDetailsBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.ui.activities.mainactivity.MainFactory
import com.azarphone.ui.activities.mainactivity.MainInjection
import com.azarphone.ui.activities.usagehistory.UsageHistoryViewModel
import com.azarphone.ui.adapters.expandablelists.UsageHistoryDetailsAdapter
import com.azarphone.ui.adapters.spinners.UsageHistorySpinnerAdapter
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.widgets.calendar.AzerCalendar
import com.azarphone.widgets.popups.showMessageDialog
import com.azarphone.widgets.popups.showMessageDisclaimerDialog
import kotlinx.android.synthetic.main.layout_usage_history_details.view.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


/**
 * @author Junaid Hassan on 04, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class UsageHistoryDetailsFragment : BaseFragment<LayoutUsageHistoryDetailsBinding, MainFactory, UsageHistoryViewModel>() {

    private var serviceType: String = ""
    private lateinit var externalView: View
    private val fromClass = "UsageHistoryDetailsFragment"
    private var date = ""
    private var startDate = ""
    private var endDate = ""
    private var dateOne = ""
    private var dateTwo = ""
    private var flagDate = false
    private var logKey = "usageHistoryDetails"
    private var verificationStep = 1
    private var accountID: String? = ""
    private var customerID: String? = ""
    private var usageHistoryAdapter: UsageHistoryDetailsAdapter? = null
    private var usageDetailsResponse: UsageDetailsResponse? = null
    private var filterList = ArrayList<RecordsItem?>()


    override fun getLayoutId(): Int {
        return R.layout.layout_usage_history_details
    }//getLayoutId ends

    override fun getViewModelClass(): Class<UsageHistoryViewModel> {
        return UsageHistoryViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): MainFactory {
        return MainInjection.provideMainFactory()
    }//getFactory ends

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }//isBindedToActivityLifeCycle ends

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        if (activity != null && !activity!!.isFinishing) {
            externalView = view
            initUI()
            initUIEvents()
            onCreateViewProcess()
            subscribeToVerifyOTPAPIResponse()
            subscribeToVerifyPassportAPIResponse()
            subscribeToUsageDetailsAPIResponse()

            if (UserDataManager.isUsageDetailsVerified()) {
                logE("DetXU", "true", fromClass, "showing abc")
                externalView.categoriesSpinnerHolder.visibility = View.VISIBLE
                externalView.periodSpinnerHolder.visibility = View.VISIBLE
                externalView.passportLayoutHolder.visibility = View.GONE
                externalView.pinLayout.visibility = View.GONE
                /**call the usage details history*/
                /*date = getLastDateMinusDays(getCurrentDate(), ConstantsUtility.DateFormats.CURRENT_DAY)
                updateDates(date, getCurrentDate())*/
            } else {
                logE("DetXU", "false", fromClass, "showing abc")
            }
        }
    }//onVisible ends

    private fun onCreateViewProcess() {
        try {
            //set the default dates for the spinner period date views
            setDefaultDates(externalView)
        } catch (e: Exception) {
            logE(logKey, "error:::".plus(e.toString()), fromClass, "onCreateViewProcess")
        }
    }//onCreateViewProcess ends

    private fun subscribeToVerifyOTPAPIResponse() {
        val signupOTPVerifyResponseLiveData = object : Observer<OTPVerifyResponse> {
            override fun onChanged(t: OTPVerifyResponse?) {
                if (activity != null && !activity!!.isFinishing) {
                    if (t != null) {
                        if (t.data != null) {
                            verificationStep = 3
                            logE("DetXU", "setting true", fromClass, "showing 3 Step")
                            UserDataManager.setUsageDetailsVerified(true)
                            externalView.usageDetailsList.visibility = View.VISIBLE
                            externalView.pinLayout.visibility = View.GONE
                            externalView.passportLayoutHolder.visibility = View.GONE
                            externalView.submitButton.visibility = View.GONE
                            externalView.categoriesSpinnerHolder.visibility = View.VISIBLE
                            externalView.periodSpinnerHolder.visibility = View.VISIBLE
                            date = getLastDateMinusDays(getCurrentDate(), ConstantsUtility.DateFormats.CURRENT_DAY)
                            updateDates(date, getCurrentDate())
                        }
                    }
                }
            }
        }

        mViewModel.verifyOTPResponse.observe(this@UsageHistoryDetailsFragment, signupOTPVerifyResponseLiveData)
    }


    private fun subscribeToVerifyPassportAPIResponse() {
        val verifyPassportResponseLiveData = object : Observer<VerifyPassportResponse> {
            override fun onChanged(t: VerifyPassportResponse?) {
                if (activity != null && !activity!!.isFinishing) {
                    if (t != null) {
                        if (t.data != null) {
                            verificationStep = 2
                            externalView.passportLayoutHolder.visibility = View.GONE
                            externalView.pinLayout.visibility = View.VISIBLE
                            externalView.detailsOTPHeading.text = resources.getString(R.string.usage_history_details_otp_heading)
                        }
                    }
                }
            }
        }

        mViewModel.verifyPassportResponse.observe(this@UsageHistoryDetailsFragment, verifyPassportResponseLiveData)
    }

    private fun subscribeToUsageDetailsAPIResponse() {
        val usageDetailsResponseObserver = object : Observer<UsageDetailsResponse> {
            override fun onChanged(response: UsageDetailsResponse?) {
                if (activity != null && !activity!!.isFinishing) {
                    if (response != null) {
                        if (response != null) {
                            verificationStep = 3
                            logE(logKey, "response:::".plus(response.toString()), fromClass, "requestUsageHistorySummary")
                            if (response.data.records != null && !response.data.records!!.isEmpty()) {
                                showDisclaimerDialog()
                                usageDetailsResponse = response
                                showContent(response.data.records!!)
                            } else {
                                hideContent()
                            }
                        } else {
                            logE(logKey, "response:::NOOOOOO", fromClass, "requestUsageHistorySummary")
                            hideContent()
                        }
                    }
                }
            }
        }

        mViewModel.usageHistoryDetailsResponse.observe(this@UsageHistoryDetailsFragment, usageDetailsResponseObserver)
    }

    private fun showDisclaimerDialog() {
        if (getDisclaimerCheckFromLocalPreferences("u"))
            showMessageDisclaimerDialog(context!!, activity!!, activity!!.resources.getString(R.string.lbl_disclaimer), activity!!.resources.getString(R.string.message_disclaimer_message), "u")
    }

    private fun hideContent() {
        externalView.no_data_layout.visibility = View.VISIBLE
        externalView.usageDetails_Layout.visibility = View.GONE
    }

    private fun showContent(records: List<RecordsItem?>) {
        usageHistoryAdapter = UsageHistoryDetailsAdapter(activity!!.baseContext, records)
        externalView.usageDetailsList.setAdapter(usageHistoryAdapter)

        externalView.usageDetails_Layout.visibility = View.VISIBLE
        externalView.no_data_layout.visibility = View.GONE

        filterUsageDetails(serviceType)
    }

    private fun setDefaultDates(view: View) {
        view.dateOneView.text = changeDateFormatForTextView(getCurrentDate())
        view.dateTwoView.text = changeDateFormatForTextView(getCurrentDate())
    }//setDefaultDates ends

    private fun initUI() {
        setHintColor(externalView.passportTL, R.color.colorFloatingHints, context!!)
        setHintColor(externalView.otpTL, R.color.colorFloatingHints, context!!)
        externalView.spinnerCategoryHeading.isSelected = true

        val periodArray = resources.getStringArray(R.array.period_filter)
        externalView.spinnerPeriod.adapter = UsageHistorySpinnerAdapter(context!!, periodArray)

        val categoriesArray = resources.getStringArray(R.array.usage_history_service_type_filter)
        externalView.spinnerCategory.adapter = UsageHistorySpinnerAdapter(context!!, categoriesArray)

        externalView.charged.isSelected = true
        externalView.usage.isSelected = true
        externalView.service.isSelected = true
    }//initUI ends

    private fun initUIEvents() {
        externalView.categoriesSpinnerHolder.setOnClickListener {
            externalView.spinnerCategory.performClick()
        }

        externalView.periodSpinnerHolder.setOnClickListener {
            externalView.spinnerPeriod.performClick()
        }

        externalView.dateOneView.setOnClickListener {
            showCalendar(externalView.dateOneView, true, externalView)//false for max date
        }//dateOne.setOnClickListener  ends

        externalView.dateTwoView.setOnClickListener {
            showCalendar(externalView.dateTwoView, false, externalView)//true for min date
        }

        externalView.submitButton.setOnClickListener {
            processOnSubmit()
        }

        externalView.resend_pin.setOnClickListener {
            val msisdn = UserDataManager.getCustomerData()?.msisdn!!
            if (hasValue(msisdn)) {
                mViewModel.resendPin(msisdn)
            }
        }

        externalView.otpET.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if (event!!.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    processOnSubmit()
                }//if ends
                return false
            }//onKey ends
        })//view.otpET.setOnKeyListener ends

        externalView.passportET.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    processOnSubmit()
                }//if ends
                return false
            }//onEditorAction ends
        })

        externalView.spinnerCategory.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val flterArray = resources.getStringArray(R.array.usage_history_service_type_filter)
                serviceType = flterArray[p2]
                filterUsageDetails(serviceType)
            }
        }

        externalView.spinnerPeriod.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                //do nothing
            }//onNothingSelected ends

            override fun onItemSelected(parent: AdapterView<*>?, viewItem: View?, position: Int, id: Long) {
                logE(logKey, "itemPos:::".plus(position.toString()), fromClass, "onItemSelected")
                if (position == 0) {
                    //current day
                    try {
                        if (flagDate) {
                            externalView.calendarLayout.visibility = View.GONE
                            updateDates(getCurrentDate(), getCurrentDate())
                        } else {
                            flagDate = true
                        }
                    } catch (exp: Exception) {
                        logE(logKey, "error1:::".plus(exp.toString()), fromClass, "onItemSelected")
                    }
                } else if (position == 1) {
                    //last seven days
                    try {
                        externalView.calendarLayout.visibility = View.GONE
                        date = ""
                        date = getLastDateMinusDays(getCurrentDate(), ConstantsUtility.DateFormats.LAST_SEVEN_DAY)
                        updateDates(date, getCurrentDate())
                    } catch (exp: Exception) {
                        logE(logKey, "error2:::".plus(exp.toString()), fromClass, "onItemSelected")
                    }
                } else if (position == 2) {
                    //last 30 days
                    try {
                        externalView.calendarLayout.visibility = View.GONE
                        date = ""
                        date = getLastDateMinusDays(getCurrentDate(), ConstantsUtility.DateFormats.LAST_THIRTY_DAY)
                        updateDates(date, getCurrentDate())
                    } catch (exp: Exception) {
                        logE(logKey, "error3:::".plus(exp.toString()), fromClass, "onItemSelected")
                    }
                } else if (position == 3) {
                    //previous month
                    try {
                        externalView.calendarLayout.visibility = View.GONE
                        date = ""
                        var maxDate = ""
                        date = getLastMonthFirstDate()
                        maxDate = getLastMonthLastDate()
                        updateDates(date, maxDate)
                    } catch (exp: Exception) {
                        logE(logKey, "error4:::".plus(exp.toString()), fromClass, "onItemSelected")
                    }
                } else if (position == 4) {
                    //custom dates
                    try {
                        setDefaultDates(externalView)
                        externalView.dateOneView.setTextColor(resources.getColor(R.color.colorTextGray))
                        externalView.dateTwoView.setTextColor(resources.getColor(R.color.colorTextGray))
                        externalView.calendarLayout.visibility = View.VISIBLE
                    } catch (exp: Exception) {
                        logE(logKey, "error5:::".plus(exp.toString()), fromClass, "onItemSelected")
                    }
                } else {
                    externalView.calendarLayout.visibility = View.GONE
                }
            }//onItemSelected ends
        }
    }//initUIEvents ends

    private fun getLocalizedAllTitle(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "All"
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Hamısı"
            else -> "Все"
        }
    }

    private fun getLocalizedOtherTitle(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "Others"
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Digərləri"
            else -> "Другие"
        }
    }

    private fun getLocalizedInternetTitle(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "data"
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "data"
            else -> "data"
        }
    }

    private fun getLocalizedSMSTitle(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "SMS"
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "SMS"
            else -> "SMS"
        }
    }

    private fun getLocalizedCallTitle(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "voice"
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "voice"
            else -> "voice"
        }
    }

    private fun filterUsageDetails(item: String) {
        if (!hasValue(item)) return
        when (item.toLowerCase()) {
            getLocalizedAllTitle().toLowerCase() -> {
                if (usageHistoryAdapter != null
                        && usageDetailsResponse != null
                        && usageDetailsResponse!!.data != null
                        && !usageDetailsResponse!!.data.records.isNullOrEmpty()) {
                    showItems()
                    usageHistoryAdapter!!.updateList(usageDetailsResponse!!.data.records!! as ArrayList<RecordsItem?>)
                }
            }
            getLocalizedOtherTitle().toLowerCase() -> {
                if (usageDetailsResponse == null) return
                filterList.clear()

                if (usageDetailsResponse != null &&
                        usageDetailsResponse!!.data != null &&
                        !usageDetailsResponse!!.data.records.isNullOrEmpty()) {
                    showItems()
                    for (i in 0 until usageDetailsResponse!!.data.records!!.size) {
                        if (hasValue(usageDetailsResponse!!.data.records!![i]!!.type)) {
                            when {
                                "data".equals(usageDetailsResponse!!.data.records!![i]!!.type!!.trim(), ignoreCase = true) -> {
                                }
                                "sms".equals(usageDetailsResponse!!.data.records!![i]!!.type!!.trim(), ignoreCase = true) -> {
                                }
                                "voice".equals(usageDetailsResponse!!.data.records!![i]!!.type!!.trim(), ignoreCase = true) -> {
                                }
                                else -> filterList.add(usageDetailsResponse!!.data.records!![i])
                            }
                        }
                    }

                    if (filterList.isNullOrEmpty()) {
                        hideContent()
                    } else {
                        if (usageHistoryAdapter != null) {
                            showItems()
                            usageHistoryAdapter!!.updateList(filterList)
                        }
                    }

                }
            }
            else -> {
                filterList.clear()

                if (usageDetailsResponse != null &&
                        usageDetailsResponse!!.data != null &&
                        !usageDetailsResponse!!.data.records.isNullOrEmpty()) {
                    showItems()
                    for (i in 0 until usageDetailsResponse!!.data.records!!.size) {
                        if (hasValue(usageDetailsResponse!!.data.records!![i]!!.type)) {
                            val keyToSearch = when {
                                item.equals("Internet", ignoreCase = true) -> "data"
                                item.equals("İnternet", ignoreCase = true) -> "data"
                                item.equals("Интернет", ignoreCase = true) -> "data"
                                item.equals("Call", ignoreCase = true) -> "voice"
                                item.equals("Zəng", ignoreCase = true) -> "voice"
                                item.equals("Звонок", ignoreCase = true) -> "voice"
                                item.equals("SMS", ignoreCase = true) -> "sms"
                                else -> item
                            }
                            if (keyToSearch.equals(usageDetailsResponse!!.data.records!![i]!!.type!!.trim(), ignoreCase = true)) {
                                filterList.add(usageDetailsResponse!!.data.records!![i])
                            }
                        }
                    }

                    if (filterList.isNullOrEmpty()) {
                        hideContent()
                    } else {
                        if (usageHistoryAdapter != null) {
                            showItems()
                            usageHistoryAdapter!!.updateList(filterList)
                        }
                    }
                }
            }
        }
    }

    private fun showItems() {
        externalView.no_data_layout.visibility = View.GONE
        externalView.usageDetails_Layout.visibility = View.VISIBLE
    }

    private fun getInvalidPassportErrorLabel(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "Please, enter Passport ID"
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Zəhmət olmasa, şəxsiyyət vəsiqəsinin seriya nömrəsini qeyd et."
            else -> "Пожалуйста, отметь серийный номер удостоверения личности."
        }
    }

    private fun processOnSubmit() {
        if (verificationStep == 1) {
            if (externalView.passportET.text.toString().length != 7) {
                showMessageDialog(context!!, activity!!,
                        "", getInvalidPassportErrorLabel())
                return
            }
            accountID = UserDataManager.getCustomerData()?.accountId
            customerID = UserDataManager.getCustomerData()?.customerId
            mViewModel.requestVerifyPassportID(externalView.passportET.text.toString(), accountID, customerID)
        } else if (verificationStep == 2) {
            if (externalView.otpET.text.toString().length != 4) {
                showMessageDialog(context!!, activity!!,
                        "", context!!.resources.getString(R.string.invalid_otp_label))
                return
            }
            val msisdn = UserDataManager.getCustomerData()?.msisdn!!
            if (hasValue(msisdn)) {
                mViewModel.requestVerifyOTP(externalView.otpET.text.toString(), msisdn)
            }
        } else if (verificationStep == 3) {
            date = getLastDateMinusDays(getCurrentDate(), ConstantsUtility.DateFormats.CURRENT_DAY)
            updateDates(date, getCurrentDate())
        }
    }

    @Throws(ParseException::class)
    fun showCalendar(textViewNormal: TextView, isStartDateSelected: Boolean, view: View) {

        try {
            dateOne = changeDateFormatForMethods(view.dateOneView.text.toString())
            dateTwo = changeDateFormatForMethods(view.dateTwoView.text.toString())

            val minDate = SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd, Locale.ENGLISH).parse(dateOne)
            val maxDate = SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd, Locale.ENGLISH).parse(dateTwo)

            val azerCalendar: AzerCalendar

            val calendar = Calendar.getInstance()
            if (isStartDateSelected) {
                calendar.time = minDate
            } else {
                calendar.time = maxDate
            }
            val year = calendar.get(Calendar.YEAR)
            val month = calendar.get(Calendar.MONTH)
            val day = calendar.get(Calendar.DAY_OF_MONTH)

            azerCalendar = AzerCalendar(textViewNormal, ondate)


            val datePickerDialog = DatePickerDialog(context!!, azerCalendar, year, month, day)
            //calculate min and max dates (for older versions use System Current TimeMillis

            if (isStartDateSelected) {

                val calndPre = Calendar.getInstance()
                calndPre.add(Calendar.MONTH, -3)
                calndPre.set(Calendar.DATE, 1)
                datePickerDialog.datePicker.minDate = calndPre.timeInMillis

                val dateMax = SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd, Locale.ENGLISH).parse(dateTwo)
                calendar.time = dateMax
                datePickerDialog.datePicker.maxDate = calendar.timeInMillis

            } else {

                val dateMin = SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd, Locale.ENGLISH).parse(dateOne)
                calendar.time = dateMin
                datePickerDialog.datePicker.minDate = calendar.timeInMillis
                datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
            }

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                datePickerDialog.setTitle("")//Prevent Date picker from creating extra Title.!
            }

            datePickerDialog.show()
        } catch (e: ParseException) {
            logE(logKey, "error:::".plus(e.toString()), fromClass, "showCalendar")
        }
    }//showCalendar ends

    private var ondate: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        try {
            dateOne = changeDateFormatForMethods(externalView.dateOneView.text.toString())
        } catch (e: ParseException) {
            logE(logKey, "error1:::".plus(e.toString()), fromClass, "ondate")
        }

        try {
            dateTwo = changeDateFormatForMethods(externalView.dateTwoView.text.toString())
        } catch (e: ParseException) {
            logE(logKey, "error2:::".plus(e.toString()), fromClass, "ondate")
        }

        updateDates(dateOne, dateTwo)
    }//ondate ends

    /*   private fun updateDates(newStartDate: String, newEndDate: String) {
           startDate = newStartDate
           endDate = newEndDate

           //get the msisdn
           if (UserDataManager.getCustomerData() != null) {
               if (hasValue(UserDataManager.getCustomerData()!!.msisdn)) {

               } else {
                   Toast.makeText(context!!, context!!.resources.getString(R.string.server_stopped_responding_please_try_again), Toast.LENGTH_SHORT).show()
               }
           } else {
               Toast.makeText(context!!, context!!.resources.getString(R.string.server_stopped_responding_please_try_again), Toast.LENGTH_SHORT).show()
           }
       }//updateDates ends*/

    private fun updateDates(newStartDate: String, newEndDate: String) {
        startDate = newStartDate
        endDate = newEndDate

        var formattedStartedDate = ""
        if (hasValue(changeDateFormat(startDate))) {
            formattedStartedDate = changeDateFormat(startDate)!!
        }

        var formattedEndDate = ""
        if (hasValue(changeDateFormat(endDate))) {
            formattedEndDate = changeDateFormat(endDate)!!
        }

        var accountIdId = ""
        if (UserDataManager.getCustomerData() != null && hasValue(UserDataManager.getCustomerData()!!.accountId)) {
            accountIdId = UserDataManager.getCustomerData()!!.accountId!!
        }

        var customerId = ""
        if (UserDataManager.getCustomerData() != null && hasValue(UserDataManager.getCustomerData()!!.customerId)) {
            customerId = UserDataManager.getCustomerData()!!.customerId!!
        }

        mViewModel.requestUsageHistoryDetails(formattedStartedDate, formattedEndDate, accountIdId, customerId)
    }//updateDates ends

    override fun onInVisible() {

    }//onInVisible ends

}//class ends