package com.azarphone.ui.fragment.newautopayment.monthly

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MonthlyAutoPaymentFactory(private val monthlyAutoPaymentRepository: MonthlyAutoPaymentRepository) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MonthlyAutoPaymentViewModel::class.java)) {
            return MonthlyAutoPaymentViewModel(monthlyAutoPaymentRepository) as T
        }
        throw IllegalStateException("ViewModel Cannot be Converted")
    }//create ends

}