package com.azarphone.ui.fragment.dummy

import android.app.Activity
import android.os.Bundle
import android.view.View
import com.azarphone.R
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.DummyFragmentBinding
import com.azarphone.ui.activities.mainactivity.MainFactory
import com.azarphone.ui.activities.mainactivity.MainInjection
import com.azarphone.ui.activities.mainactivity.MainViewModel
import com.azarphone.util.RootValues

/**
 * @author Junaid Hassan on 30, September, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class DummyFragment : BaseFragment<DummyFragmentBinding, MainFactory, MainViewModel>() {

    private val fromClass = "DummyFragment"

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        if (RootValues.getAzerFonActionBarEvents() != null) {
            RootValues.getAzerFonActionBarEvents()!!.onDummyFragmentSetActionBar()
        }
    }//onAttach ends

    override fun getLayoutId(): Int {
        return R.layout.dummy_fragment
    }//getLayoutId ends

    override fun getViewModelClass(): Class<MainViewModel> {
        return MainViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): MainFactory {
        return MainInjection.provideMainFactory()
    }//getFactory ends

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }//isBindedToActivityLifeCycle ends

    override fun onVisible(view: View, savedInstanceState: Bundle?) {

    }//onVisible ends

    override fun onInVisible() {

    }//onInVisible ends

}//class ends