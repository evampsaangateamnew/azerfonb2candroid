package com.azarphone.ui.fragment.topup

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.azarphone.R
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.LayoutTopupBinding
import com.azarphone.ui.activities.mainactivity.MainFactory
import com.azarphone.ui.activities.mainactivity.MainInjection
import com.azarphone.ui.activities.mainactivity.MainViewModel
import com.azarphone.ui.adapters.recyclerviews.TopupMenuRecyclerAdapter
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.RootValues
import com.azarphone.util.getTopupMenuModel
import com.azarphone.util.logE
import com.azarphone.validators.hasValue
import kotlinx.android.synthetic.main.layout_topup.*

class TopupFragment : BaseFragment<LayoutTopupBinding, MainFactory, MainViewModel>() {

    //Class Name variable
    private val fromClass = TopupFragment::class.simpleName.toString()

    //Bundle variables
    private var matchedSubScreen = ""
    private var tabToRedirect = ""
    private var offeringIdToRedirect = ""

    override fun getLayoutId(): Int {
        return R.layout.layout_topup
    }

    override fun getViewModelClass(): Class<MainViewModel> {
        return MainViewModel::class.java
    }

    override fun getFactory(): MainFactory {
        return MainInjection.provideMainFactory()
    }

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }

    override fun onResume() {
        super.onResume()
        if (RootValues.getAzerFonActionBarEvents() != null) {
            RootValues.getAzerFonActionBarEvents()!!.onTopupMenuSetActionBar()
        }
    }

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        if (activity != null && !activity!!.isFinishing) {

            if (!getTopupMenuModel().isNullOrEmpty()) {
                //Getting data from arguments
                getArgumentsData()
                //Setting up recyclerview
                innerMenuRcycler?.apply {
                    layoutManager = LinearLayoutManager(
                        context,
                        androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                        false
                    )
                    adapter = TopupMenuRecyclerAdapter(
                        context!!, getTopupMenuModel()!!,
                        matchedSubScreen,
                        tabToRedirect,
                        offeringIdToRedirect
                    )
                }
            } else {
                noDataFoundLayout.visibility = View.VISIBLE
                innerMenuRcycler.visibility = View.GONE
            }
        }
    }

    private fun getArgumentsData() {
        //Getting values from argument bundle
        arguments?.let {

            if (it.containsKey(ConstantsUtility.DynamicLinkConstants.SUB_SCREEN_NAME_DATA)
                && hasValue(it.getString(ConstantsUtility.DynamicLinkConstants.SUB_SCREEN_NAME_DATA))
            ) {
                val subScreen =
                    it.getString(ConstantsUtility.DynamicLinkConstants.SUB_SCREEN_NAME_DATA)
                setMatchedSubScreenValue(subScreen!!)
            }

            if (it.containsKey(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA)
                && hasValue(it.getString(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA))
            ) {
                tabToRedirect = it.getString(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA)!!
            }

            if (it.containsKey(ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA)
                && hasValue(it.getString(ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA))
            ) {
                offeringIdToRedirect =
                    it.getString(ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA)!!
            }
        }

        //Logging final received values from argument bundle
        logE(
            ConstantsUtility.DynamicLinkConstants.DYNAMIC_LINK_TAG,
            "matchedSubScreen = $matchedSubScreen tabToRedirect = $tabToRedirect" +
                    "offeringIdToRedirect = $offeringIdToRedirect",
            fromClass,
            "getArgumentsData"
        )
    }//getArgumentsData ends

    private fun setMatchedSubScreenValue(subScreen: String) {
        when {
            subScreen.equals(
                ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_TOPUP,
                ignoreCase = true
            ) -> {
                matchedSubScreen =
                    ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_TOPUP
            }
            subScreen.equals(
                ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_LOAN,
                ignoreCase = true
            ) -> {
                matchedSubScreen =
                    ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_LOAN
            }
            subScreen.equals(
                ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_MONEY_TRANSFER,
                ignoreCase = true
            ) -> {
                matchedSubScreen =
                    ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_MONEY_TRANSFER
            }
            subScreen.equals(
                ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_MONEY_REQUEST,
                ignoreCase = true
            ) -> {
                matchedSubScreen =
                    ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_MONEY_REQUEST
            }
            subScreen.equals(
                ConstantsUtility.TopupChildMenusIdentifiers.Fast_TOPUP,
                ignoreCase = true
            ) -> {
                matchedSubScreen =
                    ConstantsUtility.TopupChildMenusIdentifiers.Fast_TOPUP
            }
            subScreen.equals(
                ConstantsUtility.TopupChildMenusIdentifiers.AUTO_PAYMENT,
                ignoreCase = true
            ) -> {
                matchedSubScreen =
                    ConstantsUtility.TopupChildMenusIdentifiers.AUTO_PAYMENT
            }
        }
    }//setMatchedSubScreenValue ends

    override fun onInVisible() {
    }//onInVisible ends
}