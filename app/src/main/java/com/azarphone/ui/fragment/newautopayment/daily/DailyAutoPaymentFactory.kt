package com.azarphone.ui.fragment.newautopayment.daily

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class DailyAutoPaymentFactory(private val dailyAutoPaymentRepository: DailyAutoPaymentRepository) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DailyAutoPaymentViewModel::class.java)) {
            return DailyAutoPaymentViewModel(dailyAutoPaymentRepository) as T
        }
        throw IllegalStateException("ViewModel Cannot be Converted")
    }//create ends

}