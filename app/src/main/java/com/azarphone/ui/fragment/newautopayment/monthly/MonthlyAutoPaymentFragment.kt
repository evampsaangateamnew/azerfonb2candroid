package com.azarphone.ui.fragment.newautopayment.monthly

import android.app.DatePickerDialog
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.View
import android.widget.DatePicker
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.azarphone.R
import com.azarphone.api.pojo.response.topup.savedcards.SavedCardsItem
import com.azarphone.api.pojo.response.topup.savedcards.SavedCardsResponse
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.FragmentMonthlyAutoPaymentBinding
import com.azarphone.eventhandler.TopUpItemSelectEvents
import com.azarphone.ui.adapters.recyclerviews.AutoPaymentCardsAdapter
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.DecimalDigitsInputFilter
import com.azarphone.util.RootValues
import com.azarphone.validators.hasValue
import com.azarphone.validators.isNumeric
import com.azarphone.widgets.calendar.AzerCalendar
import com.azarphone.widgets.popups.showMessageDialog
import kotlinx.android.synthetic.main.fragment_daily_auto_payment.*
import kotlinx.android.synthetic.main.fragment_monthly_auto_payment.*
import kotlinx.android.synthetic.main.fragment_monthly_auto_payment.amountET
import kotlinx.android.synthetic.main.fragment_monthly_auto_payment.btnSchedule
import kotlinx.android.synthetic.main.fragment_monthly_auto_payment.checkbox_monthly_renewal
import kotlinx.android.synthetic.main.fragment_monthly_auto_payment.et_recurence
import kotlinx.android.synthetic.main.fragment_monthly_auto_payment.ll_calendar
import kotlinx.android.synthetic.main.fragment_monthly_auto_payment.noDataFoundLayout
import kotlinx.android.synthetic.main.fragment_monthly_auto_payment.rvCardSelection
import kotlinx.android.synthetic.main.fragment_monthly_auto_payment.start_date
import kotlinx.android.synthetic.main.fragment_weekly_auto_payment.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class MonthlyAutoPaymentFragment : BaseFragment<FragmentMonthlyAutoPaymentBinding, MonthlyAutoPaymentFactory, MonthlyAutoPaymentViewModel>() {
    private var isPostPaidBrandKlass: Boolean = false
    private var savedCardId = ""
    private var savedCards: List<SavedCardsItem>? = null
    private var year: Int = 0
    private var month: Int = 0
    private var day: Int = 0
    private var dateForAPI = ""
    private var savedCardsFromArg: SavedCardsResponse? = null
    private var isCheckBoxEnabled = false

    companion object {
        fun getInstance(savedCardsResponse: SavedCardsResponse?): MonthlyAutoPaymentFragment {
            val fragment = MonthlyAutoPaymentFragment()
            val bundle = Bundle()
            bundle.putParcelable("AutoPaymentMonthlyData", savedCardsResponse)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_monthly_auto_payment
    }

    override fun getViewModelClass(): Class<MonthlyAutoPaymentViewModel> {
        return MonthlyAutoPaymentViewModel::class.java
    }

    override fun getFactory(): MonthlyAutoPaymentFactory {
        return MonthlyAutoPaymentInjection.provideWeeklyAutoPaymentFactory()
    }

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        initData()
        initUIEvents()
    }

    override fun onInVisible() {}


    private fun initData() {
        if (arguments != null && arguments!!.containsKey("AutoPaymentMonthlyData")) {
            savedCardsFromArg = arguments!!.getParcelable("AutoPaymentMonthlyData")
        }//bundle
        savedCardResponse(savedCardsFromArg)
    }

    private fun initGUI(savedCardsItems: ArrayList<SavedCardsItem?>?) {
        updateDateTextView(getCurrentDate()!!)
        if (savedCardsItems != null && savedCardsItems.size > 0) {
            noDataFoundLayout.visibility = View.GONE
            rvCardSelection.visibility = View.VISIBLE
            setUpCardsRecyclerView(savedCardsItems)
        } else {
            noDataFoundLayout.visibility = View.VISIBLE
            rvCardSelection.visibility = View.INVISIBLE
        }

        /*if (UserDataManager.getCustomerData() != null) {
            var customerData: CustomerData? = UserDataManager.getCustomerData()
            if (customerData?.customerType.equals(ConstantsUtility.UserConstants.USER_TYPE_POSTPAID , ignoreCase = true)) {
                isPostPaidBrandKlass = true
                tvAmountHeading.visibility = View.GONE
                amountTL.visibility = View.GONE
                checkbox_monthly_renewal.visibility = View.VISIBLE
            } else {
                isPostPaidBrandKlass = false
                tvAmountHeading.visibility = View.VISIBLE
                amountTL.visibility = View.VISIBLE
                checkbox_monthly_renewal.visibility = View.GONE
            }
        }*/
    } // initGUI ends

    private fun initUIEvents() {
        ll_calendar.setOnClickListener {
            if (day > 0 && month > 0 && year > 0) {
                showCalendar(start_date, year = year, month = month, day = day)
            }
        }

        et_recurence.addTextChangedListener(object : TextWatcher {
            var prevValue: String = ""
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                prevValue = charSequence.toString()
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if ((prevValue == null || prevValue.isEmpty()) && charSequence.toString().equals("0", ignoreCase = true)) {
                    et_recurence.setText("")
                }
            }

            override fun afterTextChanged(editable: Editable) {}
        })

        btnSchedule.setOnClickListener {
            val amount: String = amountET.text.toString()
            val recurrence: String = et_recurence.text.toString()
            try {
                if (!hasValue(recurrence) || !isNumeric(recurrence) || recurrence.toDouble() <= 0) {
                    showMessageDialog(requireContext(), requireActivity(),
                            getString(R.string.popup_error_title), requireContext().getString(R.string.auto_payment_invalid_recurrence_alert))
                    return@setOnClickListener
                }
            } catch (exception: Exception) {
            }

            if (savedCards == null || savedCards!!.isEmpty()) {
                showMessageDialog(
                    requireContext(),
                    requireActivity(),
                    getString(R.string.popup_error_title),
                    requireContext().getString(R.string.auto_payment_no_saved_cards_alert)
                )
                return@setOnClickListener
            }

            /* if (isPostPaidBrandKlass) {

                 if (isCheckBoxEnabled && savedCards != null && savedCards!!.isNotEmpty() && isNumeric(recurrence) && hasValue(recurrence) && savedCardId.isNotEmpty()) {
                     if (RootValues?.getPymentSchedlerClickListener() != null) {
                         RootValues.getPymentSchedlerClickListener().paymentSchedulerClickListener("", recurrence, ConstantsUtility.AutoPaymentConstants.BILLING_CYCLE_DAILY, savedCardId, dateForAPI)
                     }
                 } else {
                     showMessageDialog(requireContext(), requireActivity(), requireContext().getString(R.string.popup_error_title), requireContext().getString(R.string.check_selection))
                 }

             } else*/ if (savedCards != null && savedCards!!.isNotEmpty() && isValidAmount(amount) && isNumeric(recurrence) && hasValue(recurrence) && savedCardId.isNotEmpty()) {
            if (RootValues.getPymentSchedlerClickListener() != null) {
                RootValues.getPymentSchedlerClickListener()!!.paymentSchedulerClickListener(
                    amount,
                    recurrence,
                    ConstantsUtility.AutoPaymentConstants.BILLING_CYCLE_MONTHLY,
                    savedCardId,
                    dateForAPI
                )
            }
        }
        }

        checkbox_monthly_renewal.setOnCheckedChangeListener { _, b ->
            isCheckBoxEnabled = b
        }

        amountET.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(text: Editable) {
                val value = text.toString()
                val valueSplits = value.split("\\.".toRegex()).toTypedArray()
                if (value.length <= 2 && valueSplits.isNotEmpty() && hasValue(valueSplits[0]) && valueSplits[0] == "0") {
                    amountET.setText("")
                } else if (value.startsWith(".")) {
                    amountET.setText("")
                }
                amountET.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(100, 2))
            }
        })
    } // initUIEvents ends

    private fun setUpCardsRecyclerView(savedCardsItems: ArrayList<SavedCardsItem?>) {
        val autoPaymentCardsAdapter = AutoPaymentCardsAdapter(savedCardsItems, requireContext(), object : TopUpItemSelectEvents {
            override fun onItemSelected(position: Int, searchKey: String) {
                if (savedCardsItems.isNotEmpty() && savedCardsItems[position] != null) {
                    savedCardId = savedCardsItems[position]?.id.toString()
                }
            }
        })

        if (savedCardsItems.isNotEmpty() && savedCardsItems[0] != null) {
            savedCardId = savedCardsItems[0]?.id.toString()
        }
        rvCardSelection.layoutManager = LinearLayoutManager(requireContext())
        rvCardSelection.adapter = autoPaymentCardsAdapter
    }

    //update start date text ui
    private fun updateDateTextView(dateNew: String) {
        if (activity == null || requireActivity().isFinishing) return

        start_date.text = dateNew
        start_date.setTextColor(ContextCompat.getColor(requireContext(), R.color.brown_grey))
    } // updateDateTextView ends

    //create Listener to Date Picker
    private var onDateSetListener = DatePickerDialog.OnDateSetListener { _: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int ->
        //create calendar instance and set date information which are picked by Date picker
        val c = Calendar.getInstance()
        c.set(year, monthOfYear, dayOfMonth)

        //Months are indexed from 0 not 1 , so we add 1
        var finalmonthOfYear = monthOfYear + 1
        if (finalmonthOfYear.toString().length == 1) {
            dateForAPI = "$year-0$finalmonthOfYear-$dayOfMonth"
        } else {
            dateForAPI = "$year-$finalmonthOfYear-$dayOfMonth"
        }

        day = dayOfMonth
        month = finalmonthOfYear
        this.year = year

        //format date into require output format
        val sdf = SimpleDateFormat(ConstantsUtility.DateFormats.SAMPLE_DATE_FORMAT_DAY_NAME, Locale.US)
        val formattedDate = sdf.format(c.time)

        //update textView with new selected date
        updateDateTextView(settingUpDateWithLang(formattedDate))
    } // onDateSetListener ends

    fun showCalendar(textViewNormal: TextView, year: Int, month: Int, day: Int) {
        try {
            //return if activity is null or isFinishing true
            if (activity == null || requireActivity().isFinishing) return

            val calendar = Calendar.getInstance()

            val azerCalendar: AzerCalendar = AzerCalendar(textViewNormal, onDateSetListener)


            val datePickerDialog = DatePickerDialog(requireContext(), azerCalendar, year, month, day)
            //calculate min and max dates (for older versions use System Current TimeMillis

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                datePickerDialog.setTitle("")//Prevent Date picker from creating extra Title.!
            }

            //restrict user to only select from current day onwards
            datePickerDialog.datePicker.minDate = calendar.timeInMillis
            datePickerDialog.show()
        } catch (e: ParseException) {
//            logE(logKey, "error:::".plus(e.toString()), fromClass, "showCalendar")
        }

    }//showCalendar ends

    private fun getCurrentDate(): String? {
        val calendar = Calendar.getInstance()
        val sdf =
            SimpleDateFormat(ConstantsUtility.DateFormats.SAMPLE_DATE_FORMAT_DAY_NAME, Locale.US)
        val formattedDate = sdf.format(calendar.time)
        year = calendar[Calendar.YEAR]
        month = calendar[Calendar.MONTH]
        day = calendar[Calendar.DAY_OF_MONTH]
        val monthForAPI = month + 1
        if (monthForAPI.toString().length == 1) dateForAPI =
            "$year-0$monthForAPI-$day" else dateForAPI = "$year-$monthForAPI-$day"
        return settingUpDateWithLang(formattedDate)
    } // getCurrentDate ends

    private fun settingUpDateWithLang(formattedDate: String): String {
        var datePieces = formattedDate.split(" ")

        var finalDayName = ""

        if (datePieces.isNotEmpty()) {

            val dayName = datePieces[0]

            if (dayName.isNotEmpty()) {
                finalDayName = when (dayName) {
                    ConstantsUtility.AutoPaymentConstants.SAT -> requireContext().getString(R.string.add_auto_payment_day_sat)
                    ConstantsUtility.AutoPaymentConstants.SUN -> requireContext().getString(R.string.add_auto_payment_day_sun)
                    ConstantsUtility.AutoPaymentConstants.MON -> requireContext().getString(R.string.add_auto_payment_day_mon)
                    ConstantsUtility.AutoPaymentConstants.TUE -> requireContext().getString(R.string.add_auto_payment_day_tue)
                    ConstantsUtility.AutoPaymentConstants.WED -> requireContext().getString(R.string.add_auto_payment_day_wed)
                    ConstantsUtility.AutoPaymentConstants.THU -> requireContext().getString(R.string.add_auto_payment_day_thu)
                    ConstantsUtility.AutoPaymentConstants.FRI -> requireContext().getString(R.string.add_auto_payment_day_fri)
                    else -> dayName
                }
            }
            finalDayName = finalDayName.plus(" " + datePieces[1])
        }
        return finalDayName
    }

    private fun savedCardResponse(savedCardsResponse: SavedCardsResponse?) {
//        if (savedCardsResponse != null && savedCardsResponse.data != null && savedCardsResponse?.data?.cardDetails != null && savedCardsResponse?.data?.cardDetails!!.isNotEmpty()) {
        savedCards = savedCardsResponse?.data?.cardDetails!!
        initGUI(savedCardsResponse.data?.cardDetails as ArrayList<SavedCardsItem?>?)
//        }
    }


    private fun isValidAmount(amount: String): Boolean {
        var isValid = true
        if (!hasValue(amount) || !isNumeric(amount) || amount.toDouble() <= 0) {
            showMessageDialog(requireContext(), requireActivity(),
                    getString(R.string.popup_error_title),
                    getString(R.string.error_msg_invalid_amount))
            isValid = false
        }
        return isValid
    }


}