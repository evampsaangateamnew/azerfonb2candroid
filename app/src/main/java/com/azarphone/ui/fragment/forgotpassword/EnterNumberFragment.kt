package com.azarphone.ui.fragment.forgotpassword

import android.os.Bundle
import android.text.Editable
import android.text.Selection
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.azarphone.R
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.EnterMobileNumberLayoutBinding
import com.azarphone.eventhandler.ForgotPasswordActivityToolTipEvents
import com.azarphone.ui.activities.forgotpasswordactivity.ForgotPasswordActivity
import com.azarphone.ui.activities.forgotpasswordactivity.ForgotPasswordFactory
import com.azarphone.ui.activities.forgotpasswordactivity.ForgotPasswordInjection
import com.azarphone.ui.activities.forgotpasswordactivity.ForgotPasswordViewModel
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.RootValues
import com.azarphone.util.getALSBoldFont
import com.azarphone.util.logE
import com.azarphone.widgets.tooltip.initiateErrorToolTip
import io.canner.stepsview.StepsView
import kotlinx.android.synthetic.main.enter_mobile_number_layout.view.*

@Suppress("DEPRECATION")
class EnterNumberFragment : BaseFragment<EnterMobileNumberLayoutBinding, ForgotPasswordFactory, ForgotPasswordViewModel>() {

    private lateinit var externalView: View

    private var forgotPasswordActivityToolTipEvents: ForgotPasswordActivityToolTipEvents? = null

    override fun getViewModelClass(): Class<ForgotPasswordViewModel> {
        return ForgotPasswordViewModel::class.java
    }

    override fun getFactory(): ForgotPasswordFactory {
        return ForgotPasswordInjection.provideForgotPasswordFactory()
    }

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }

    override fun getLayoutId(): Int {
        return R.layout.enter_mobile_number_layout
    }

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        externalView = view
        setupInputFields()
        registerListener()
        mFragmentViewDataBinding.apply {
            viewmodel = mViewModel
        }

        addPrefixToUserNameField(view)


        activity?.let {
            val stepView = it.findViewById<StepsView>(R.id.stepsView).setCompletedPosition(0).drawView()
        }
        mViewModel.setBackPressEnable(true)
        mViewModel.showBackBtn.set(false)
        mViewModel.setForgotPasswordStep(ConstantsUtility.ForgetPasswordScreenSteps.ENTER_NUMBER_STEP)

        view.userNameET.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if (event!!.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    (activity as ForgotPasswordActivity).processForgotPassword()
                }//if ends
                return false
            }//onKey ends
        })

        externalView.userNameET.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    (activity as ForgotPasswordActivity).processForgotPassword()
                }//if ends
                return false
            }//onEditorAction ends
        })
    }



    private fun registerListener() {
        forgotPasswordActivityToolTipEvents = object : ForgotPasswordActivityToolTipEvents {
            override fun onInvalidPasswordTooltip(errorDetail: String) {
                //do nothing
            }

            override fun onInvalidConfirmPasswordTooltip(errorDetail: String) {
                //do nothing
            }

            override fun onInvalidOTPPINTooltip(errorDetail: String) {
                //do nothing here
            }

            override fun onInvalidMsisdnNumberTooltip(errorDetail: String) {
                logE("toolerX", "detail:::".plus(errorDetail), "SignupMobileNumberFragment", "onInvalidMsisdnNumberTooltip")
                context?.let { initiateErrorToolTip(it, externalView.userNameET, errorDetail) }
            }
        }

        RootValues.setForgotPasswordActivityToolTipEvents(forgotPasswordActivityToolTipEvents!!)
    }

    private fun setupInputFields() {
        externalView.mobileNumberTIL.defaultHintTextColor = context!!.resources.getColorStateList(R.color.soft_pink)
        externalView.userNameET.typeface = getALSBoldFont()
    }//setupInputFields ends

    override fun onInVisible() {
    }

    private fun addPrefixToUserNameField(view: View) {
        view.userNameET.setText(ConstantsUtility.UserConstants.USERNAME_PREFIX)
        Selection.setSelection(view.userNameET.text, view.userNameET.text!!.length)
        view.userNameET.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (!s.toString().startsWith(ConstantsUtility.UserConstants.USERNAME_PREFIX)) {
                    view.userNameET.setText(ConstantsUtility.UserConstants.USERNAME_PREFIX)
                    Selection.setSelection(view.userNameET.text, view.userNameET.text!!.length)

                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
    }
}