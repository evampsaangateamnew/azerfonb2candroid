package com.azarphone.ui.fragment.supplementaryoffers

import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.azarphone.R
import com.azarphone.api.pojo.response.supplementaryoffers.helper.SupplementaryOffer
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.LayoutSupplementaryOffersPaggerFragmentBinding
import com.azarphone.eventhandler.OfferFilterListener
import com.azarphone.eventhandler.OffersSearchResultListener
import com.azarphone.eventhandler.SupplementaryOffersAdapterUpdateEvents
import com.azarphone.eventhandler.SupplementaryOffersSubscriptionEvents
import com.azarphone.ui.activities.supplementaryoffers.SupplementaryOffersFactory
import com.azarphone.ui.activities.supplementaryoffers.SupplementaryOffersInjection
import com.azarphone.ui.activities.supplementaryoffers.SupplementaryOffersViewModel
import com.azarphone.ui.adapters.recyclerviews.SupplementaryOffersCardAdapter
import com.azarphone.util.*
import com.azarphone.util.ConstantsUtility.PackagesConstants.FOUNDED_OFFERS_ITEM_POS
import com.azarphone.util.ConstantsUtility.PackagesConstants.TAB_OFFERS
import com.azarphone.util.ConstantsUtility.PackagesConstants.TAB_TITLE
import com.yarolegovich.discretescrollview.Orientation
import com.yarolegovich.discretescrollview.transform.ScaleTransformer
import kotlinx.android.synthetic.main.layout_supplementary_offers_pagger_fragment.view.*
import kotlinx.android.synthetic.main.layout_tariff_card_fragment.*
import kotlinx.android.synthetic.main.no_data_found_white_no_bg.view.*
import java.util.*


/**
 * @author Junaid Hassan on 22, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class SupplementaryOffersPaggerFragment : BaseFragment<LayoutSupplementaryOffersPaggerFragmentBinding, SupplementaryOffersFactory, SupplementaryOffersViewModel>(), OffersSearchResultListener {

    private val fromClass = "SupplementaryOffersPaggerFragment"
    private lateinit var externalView: View
    private var pageTitle = ""
    private var foundOfferItemPos = -1
    private var supplementaryOffersList: ArrayList<SupplementaryOffer?>? = null
    private var supplementaryOffersCardAdapter: SupplementaryOffersCardAdapter? = null

    companion object {
        fun getInstance(foundOfferItemPos: Int, tabTitle: String, supplementaryOffers: List<SupplementaryOffer?>): SupplementaryOffersPaggerFragment {
            val fragment = SupplementaryOffersPaggerFragment()
            val bundle = Bundle()
            bundle.putString(TAB_TITLE, tabTitle)
            bundle.putInt(FOUNDED_OFFERS_ITEM_POS, foundOfferItemPos)
            bundle.putParcelableArrayList(TAB_OFFERS, supplementaryOffers as ArrayList<out Parcelable>)
            fragment.arguments = bundle
            return fragment
        }//getInstance ends
    }//companion object ends

    override fun getLayoutId(): Int {
        return R.layout.layout_supplementary_offers_pagger_fragment
    }//getLayoutId ends

    override fun getViewModelClass(): Class<SupplementaryOffersViewModel> {
        return SupplementaryOffersViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): SupplementaryOffersFactory {
        return SupplementaryOffersInjection.provideSupplementaryOffersFactory()
    }//getFactory ends

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }//isBindedToActivityLifeCycle ends

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        if (activity != null && !activity!!.isFinishing) {
            externalView = view
            initFragmentData()
            initAdapterUpdateEvents()
            initOfferFilterListener()
            initAdapterNotifyDataSetEvents()
            initUI()
            initUIEvents()
        }
    }//onVisible ends

    private fun initAdapterUpdateEvents() {
        val supplementaryOffersAdapterUpdateEvents = object : SupplementaryOffersAdapterUpdateEvents {
            override fun onAdapterNeedToBeNotify() {
                if (supplementaryOffersCardAdapter != null) {
                    logE("AdapterXU", "updated", fromClass, "initAdapterUpdateEvents")
                    supplementaryOffersCardAdapter!!.notifyDataSetChanged()
                }
            }
        }

        RootValues.setSupplementaryOffersAdapterUpdateEvents(supplementaryOffersAdapterUpdateEvents)
    }

    private fun initAdapterNotifyDataSetEvents() {
        val supplementaryOffersSubscriptionEvents = object : SupplementaryOffersSubscriptionEvents {
            override fun onSupplementaryOffersSubscriptionSuccess() {
                logE("AdapterXu", "called", fromClass, "initAdapterNotifyDataSetEvents")
                if (supplementaryOffersCardAdapter != null) {
                    supplementaryOffersCardAdapter!!.notifyDataSetChanged()
                }
            }
        }

        RootValues.setSupplementaryOffersSubscriptionEvents(supplementaryOffersSubscriptionEvents)
    }

    override fun onSearchResult(isResultFound: Boolean) {
        activity?.runOnUiThread {
            foundSearchResult(isResultFound)
        }
    }//onSearchResult ends

    private fun foundSearchResult(isResultFound: Boolean) {
        if (isResultFound) {
            logE("foundResults", "found:::", fromClass, "foundSearchResult")
            externalView.mainView.visibility = VISIBLE
            externalView.noDataFoundLayout.visibility = GONE
        } else {
            logE("foundResults", "not found:::", fromClass, "foundSearchResult")
            externalView.mainView.visibility = GONE
            externalView.noDataFoundLayout.visibility = VISIBLE
            externalView.noDataFoundTextLabel.text = getNoDataFoundOnSearchMessage()
            externalView.noDataFoundTextLabel.isSelected = true
            externalView.img_arrowleft.visibility = GONE
            externalView.img_arrowright.visibility = GONE
            externalView.img_arrowup.visibility = GONE
            externalView.img_arrowdown.visibility = GONE

        }

    }//foundSearchResult ends

    private fun initOfferFilterListener() {
        val offerFilterListener = object : OfferFilterListener {
            override fun onFilterCall(filterString: String) {
                if (activity != null && !activity!!.isFinishing) {
                    logE("offerFilterString", "string:::".plus(filterString), fromClass, "initOfferFilterListener")
                    if (supplementaryOffersCardAdapter == null) {
                        logE("offerFilterString", "adapter is null:::", fromClass, "initOfferFilterListener")
                        return
                    } else {
                        /**perform filtering in the adapter*/
                        supplementaryOffersCardAdapter!!.filter.filter(filterString)
                    }
                }
            }
        }

        if (getString(R.string.supplementary_offers_call_tab_title).equals(pageTitle, ignoreCase = true)) {
            RootValues.setOfferFilterListenerCall(offerFilterListener)
        } else if (getString(R.string.supplementary_offers_internet_tab_title).equals(pageTitle, ignoreCase = true)) {
            RootValues.setOfferFilterListenerInternet(offerFilterListener)
        } else if (getString(R.string.supplementary_offers_sms_tab_title).equals(pageTitle, ignoreCase = true)) {
            RootValues.setOfferFilterListenerSms(offerFilterListener)
        } else if (getString(R.string.supplementary_offers_campaign_tab_title).equals(pageTitle, ignoreCase = true)) {
            RootValues.setOfferFilterListenerCampaign(offerFilterListener)
        } else if (getString(R.string.supplementary_offers_tm_tab_title).equals(pageTitle, ignoreCase = true)) {
            RootValues.setOfferFilterListenerTM(offerFilterListener)
        } else if (getString(R.string.supplementary_offers_hybrid_tab_title).equals(pageTitle, ignoreCase = true)) {
            RootValues.setOfferFilterListenerHybrid(offerFilterListener)
        } else if (getString(R.string.supplementary_offers_voiceInclusiveOffers_tab_title).equals(pageTitle, ignoreCase = true)) {
            RootValues.setOfferFilterListenerVoiceInclusive(offerFilterListener)
        } else if (getString(R.string.supplementary_offers_internetInclusiveOffers_tab_title).equals(pageTitle, ignoreCase = true)) {
            RootValues.setOfferFilterListenerInternetInclusive(offerFilterListener)
        } else if (getString(R.string.supplementary_offers_smsInclusiveOffers_tab_title).equals(pageTitle, ignoreCase = true)) {
            RootValues.setOfferFilterListenerSmsInclusive(offerFilterListener)
        }
    }

    private fun initFragmentData() {
        if (arguments != null && arguments!!.containsKey(TAB_TITLE)) {
            pageTitle = arguments!!.getString(TAB_TITLE)!!
        }
        if (arguments != null && arguments!!.containsKey(FOUNDED_OFFERS_ITEM_POS)) {
            foundOfferItemPos = arguments!!.getInt(FOUNDED_OFFERS_ITEM_POS)

        }
        if (arguments != null && arguments!!.containsKey(TAB_OFFERS)) {
            supplementaryOffersList = arguments!!.getParcelableArrayList(TAB_OFFERS)
        }

        logE(
            ConstantsUtility.DynamicLinkConstants.DYNAMIC_LINK_TAG,
            "foundOfferItemPos = $foundOfferItemPos",
            fromClass,
            "initFragmentData"
        )
    }//initFragmentData ends

    private fun initUI() {
        if (supplementaryOffersList != null && !supplementaryOffersList!!.isEmpty()) {
            if (RootValues.getOffersListOrientation() == androidx.recyclerview.widget.LinearLayoutManager.VERTICAL) {
                externalView.img_arrowleft.visibility = GONE
                externalView.img_arrowright.visibility = GONE
                externalView.img_arrowup.visibility = VISIBLE
                externalView.img_arrowdown.visibility = VISIBLE
                externalView.mRecylerview.visibility = VISIBLE
                externalView.mDiscreteView.visibility = GONE
                externalView.mRecylerview.recycledViewPool.setMaxRecycledViews(0, 0)
                externalView.noDataFoundLayout.visibility = GONE
                externalView.mRecylerview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
                supplementaryOffersCardAdapter = SupplementaryOffersCardAdapter(
                        context!!,
                        supplementaryOffersList,
                        false,
                        "",
                        "",
                        false,
                        this,
                        pageTitle,mViewModel)

                externalView.mRecylerview.adapter = supplementaryOffersCardAdapter

                if (foundOfferItemPos != -1) {
                    externalView.mRecylerview.scrollToPosition(foundOfferItemPos)
                }
                setRecycleViewArrowListener(externalView.mRecylerview.layoutManager as LinearLayoutManager)

            } else {
                externalView.img_arrowleft.visibility = VISIBLE
                externalView.img_arrowright.visibility = VISIBLE

                externalView.img_arrowup.visibility = GONE
                externalView.img_arrowdown.visibility = GONE

                externalView.mRecylerview.visibility = GONE
                externalView.mDiscreteView.visibility = VISIBLE
                externalView.noDataFoundLayout.visibility = GONE
                externalView.mDiscreteView.recycledViewPool.setMaxRecycledViews(0, 0)
                externalView.mDiscreteView.setOrientation(Orientation.HORIZONTAL)
                externalView.mDiscreteView.setItemTransformer(ScaleTransformer.Builder().build())
                supplementaryOffersCardAdapter = SupplementaryOffersCardAdapter(
                        context!!,
                        supplementaryOffersList,
                        false,
                        "",
                        "",
                        false,
                        this,
                        pageTitle,mViewModel)
                externalView.mDiscreteView.adapter = supplementaryOffersCardAdapter

                if (foundOfferItemPos != -1) {
                    externalView.mDiscreteView.scrollToPosition(foundOfferItemPos)
                }

                externalView.mDiscreteView.addOnItemChangedListener { viewHolder, i -> updateArrowDescreteView(i) }


            }
        } else {
            externalView.img_arrowleft.visibility = GONE
            externalView.img_arrowright.visibility = GONE

            externalView.img_arrowup.visibility = GONE
            externalView.img_arrowdown.visibility = GONE


            externalView.mainView.visibility = GONE
            externalView.noDataFoundLayout.visibility = VISIBLE
            externalView.noDataFoundTextLabel.text = getNoDataFoundSimpleMessage()
            externalView.noDataFoundTextLabel.isSelected = true
        }

        disposeArgumentsData()
    }//initUI ends

    private fun setRecycleViewArrowListener(linearLayoutManager: LinearLayoutManager) {

        val recyclerViewOnScrollListener: RecyclerView.OnScrollListener = object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount: Int = linearLayoutManager.getChildCount()
                val totalItemCount: Int = linearLayoutManager.getItemCount()
                val firstVisibleItemPosition: Int = linearLayoutManager.findFirstVisibleItemPosition()
                val lastItem = firstVisibleItemPosition + visibleItemCount
                var position = 0;
                if (!recyclerView.canScrollVertically(1)) {
                    position=2
                }
                if (!recyclerView.canScrollVertically(-1)) {
                    position=1
                }
                updateArrowRecyclerView(firstVisibleItemPosition, lastItem, visibleItemCount,position)

            }
        }
        externalView.mRecylerview.addOnScrollListener(recyclerViewOnScrollListener)
    }

    private fun updateArrowRecyclerView(firstItemShowing: Int, lastItemShowing: Int, visibleItemCount: Int, position: Int) {
        if (visibleItemCount < 2) {
            if (externalView.img_arrowdown?.visibility == VISIBLE)
                externalView.img_arrowdown?.visibility = GONE
            if (externalView.img_arrowup?.visibility == VISIBLE)
                externalView.img_arrowup?.visibility = GONE
        }  else {
                if (firstItemShowing.equals(0)) {
                    if (externalView.img_arrowup.visibility == VISIBLE)
                        externalView.img_arrowup.visibility = GONE
                } else {
                    if (externalView.img_arrowup.visibility == GONE)
                        externalView.img_arrowup.visibility = VISIBLE

                }
                if (supplementaryOffersList?.size?.equals(lastItemShowing)!! || externalView.mRecylerview.adapter?.itemCount!!.equals(lastItemShowing)) {
                    if (externalView.img_arrowdown.visibility == VISIBLE)
                        externalView.img_arrowdown.visibility = GONE

                } else {
                    if (externalView.img_arrowdown.visibility == GONE)
                        externalView.img_arrowdown.visibility = VISIBLE

                }
        }

        if(position==1 && externalView.mRecylerview?.adapter?.itemCount!! > 1 )
        {
            if (externalView.img_arrowdown.visibility == GONE)
                externalView.img_arrowdown.visibility = VISIBLE
            if (externalView.img_arrowup.visibility == VISIBLE)
                externalView.img_arrowup.visibility = GONE
        } else
            if(position==2 && externalView.mRecylerview?.adapter?.itemCount!! > 1 )
            {
                if (externalView.img_arrowdown.visibility == VISIBLE)
                    externalView.img_arrowdown.visibility = GONE
                if (externalView.img_arrowup.visibility == GONE)
                    externalView.img_arrowup.visibility = VISIBLE

            }

    } //updateArrowRecyclerView ends

    private fun updateArrowDescreteView(position: Int) {
        if ((supplementaryOffersList?.size!!) <= 1 || (externalView?.mDiscreteView.adapter?.itemCount!!) <= 1) {
            externalView.img_arrowleft.visibility = GONE
            externalView.img_arrowright.visibility = GONE
        } else
            if (position == 0) {
                externalView.img_arrowleft.visibility = GONE
                externalView.img_arrowright.visibility = VISIBLE
            } else if (position == (supplementaryOffersList?.size?.minus(1)) || position == (externalView?.mDiscreteView.adapter?.itemCount?.minus(1))) {
                externalView.img_arrowleft.visibility = VISIBLE
                externalView.img_arrowright.visibility = GONE
            } else {
                externalView.img_arrowleft.visibility = VISIBLE
                externalView.img_arrowright.visibility = VISIBLE
            }
    }  //updateArrowDescreteView

    private fun disposeArgumentsData() {
        if (arguments != null && arguments!!.containsKey(TAB_TITLE)) {
            arguments!!.clear()
        }
        if (arguments != null && arguments!!.containsKey(FOUNDED_OFFERS_ITEM_POS)) {
            arguments!!.clear()
        }
        if (arguments != null && arguments!!.containsKey(TAB_OFFERS)) {
            arguments!!.clear()
        }
    }//disposeArgumentsData ends

    private fun initUIEvents() {
        externalView.img_arrowleft.setOnClickListener { leftArrowClickListener() }
        externalView.img_arrowright.setOnClickListener { rightArrowClickListener() }
    }//initUIEvents ends

    private fun rightArrowClickListener() {
        var index = mDiscreteView?.currentItem?.plus(1)
        if (index != null) {
            if (index >= 0 && index < supplementaryOffersList?.size!! && index < externalView.mDiscreteView?.adapter?.itemCount!!)
                externalView.mDiscreteView?.smoothScrollToPosition(index)
        }
    }

    private fun leftArrowClickListener() {
        var index = mDiscreteView?.currentItem?.minus(1)
        if (index != null) {
            if (index >= 0 && index < supplementaryOffersList?.size!! && index < externalView.mDiscreteView?.adapter?.itemCount!!)
                externalView.mDiscreteView?.smoothScrollToPosition(index)
        }


    }

    override fun onInVisible() {
        /*externalView.mRecylerview.setOnTouchListener { view, motionEvent ->
            // Disallow the touch request for parent scroll on touch of child view
            view.parent.requestDisallowInterceptTouchEvent(false)
            false
        }*/
    }//onInVisible ends
}//class ends