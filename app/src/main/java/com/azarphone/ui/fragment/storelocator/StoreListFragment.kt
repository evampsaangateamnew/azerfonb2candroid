package com.azarphone.ui.fragment.storelocator

import androidx.lifecycle.Observer
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.response.storelocator.StoreLocatorResponse
import com.azarphone.api.pojo.response.storelocator.StoresItem
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.LayoutStoreListBinding
import com.azarphone.ui.activities.mainactivity.MainFactory
import com.azarphone.ui.activities.mainactivity.MainInjection
import com.azarphone.ui.activities.storelocator.StoreLocatorViewModel
import com.azarphone.ui.adapters.expandablelists.StoreListAdapter
import com.azarphone.util.ConstantsUtility
import java.util.*

class StoreListFragment : BaseFragment<LayoutStoreListBinding, MainFactory, StoreLocatorViewModel>() {

    lateinit var mStoreList: StoreListAdapter
    private var isFound = false
    override fun getLayoutId(): Int {
        return R.layout.layout_store_list
    }

    override fun getViewModelClass(): Class<StoreLocatorViewModel> {
        return StoreLocatorViewModel::class.java
    }

    override fun getFactory(): MainFactory {
        return MainInjection.provideMainFactory()
    }

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        if (activity != null && !activity!!.isFinishing) {
            subscribe()
        }
    }

    override fun onInVisible() {
    }

    fun subscribe() {
        val storeObserver = object : Observer<StoreLocatorResponse> {
            override fun onChanged(t: StoreLocatorResponse?) {
                if (activity != null && !activity!!.isFinishing) {
                    t?.let {
                        mStoreList = StoreListAdapter(context!!, t.data.stores!!)
                        mFragmentViewDataBinding.storelist.setAdapter(mStoreList)
                        setSpinners(it)
                    }
                }
            }
        }

        mViewModel.storeLocatorResponse.observe(this, storeObserver)
    }

    private fun getLocalizedAllTitle(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "All"
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Hamısı"
            else -> "Все"
        }
    }
    fun setSpinners(mStoreListResponse: StoreLocatorResponse) {

        var mCityList = mStoreListResponse.data.city as ArrayList<String>
        //   mCityList.add(0, getLocalizedAllTitle())
        var mTypeList = mStoreListResponse.data.type as ArrayList<String>
        // mTypeList.add(0, getLocalizedAllTitle())

        val citiesAdapter = ArrayAdapter<String>(context!!, R.layout.spinner_item, mCityList)
        citiesAdapter.setDropDownViewResource(R.layout.spinner_dropdown)
        mFragmentViewDataBinding.citySpinner.setAdapter(citiesAdapter)
        mFragmentViewDataBinding.citySpinner.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
              //  showOnlyCity(position)
                var city=mFragmentViewDataBinding.citySpinner.getItemAtPosition(position).toString()
                if(position==0){
                    city=getLocalizedAllTitle()
                }

                val  category=mFragmentViewDataBinding.catogerySpinner.selectedItem.toString()
                showByCityCategoryFilter(city,category)
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }
        })

        val typeAdapter = ArrayAdapter<String>(context!!, R.layout.spinner_item, mTypeList)
        typeAdapter.setDropDownViewResource(R.layout.spinner_dropdown)
        mFragmentViewDataBinding.catogerySpinner.setAdapter(typeAdapter)

        mFragmentViewDataBinding.catogerySpinner.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                // showOnlyCategory(position)
                val city=mFragmentViewDataBinding.citySpinner.selectedItem.toString()
                var  category=mFragmentViewDataBinding.catogerySpinner.getItemAtPosition(position).toString()
                if(position==0){
                    category=getLocalizedAllTitle()
                }
                showByCityCategoryFilter(city,category)
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }
        })

    }

    fun showOnlyCategory(position: Int) {
        if (position == 0) {
            mStoreList = StoreListAdapter(context!!, mViewModel.storeLocatorResponse.value!!.data.stores!!)
            mFragmentViewDataBinding.storelist.setAdapter(mStoreList)
            return
        }

        mViewModel.storeLocatorResponse.value!!.data.stores?.let {
            val catogery = mViewModel.storeLocatorResponse.value!!.data.type!![position]

            val list = ArrayList<StoresItem>()

            for (storeItem in it) {
                if (storeItem.type.equals(catogery)) {
                    list.add(storeItem)
                }
            }

            mStoreList = StoreListAdapter(context!!, list)
            mFragmentViewDataBinding.storelist.setAdapter(mStoreList)

        }
    }


    fun showOnlyCity(position: Int) {
        if (position == 0) {
            mStoreList = StoreListAdapter(context!!, mViewModel.storeLocatorResponse.value!!.data.stores!!)
            mFragmentViewDataBinding.storelist.setAdapter(mStoreList)
            return
        }

        mViewModel.storeLocatorResponse.value!!.data.stores?.let {
            val city = mViewModel.storeLocatorResponse.value!!.data.city!![position]

            val list = ArrayList<StoresItem>()

            for (storeItem in it) {
                if (storeItem.city.equals(city)) {
                    list.add(storeItem)
                }
            }

            mStoreList = StoreListAdapter(context!!, list)
            mFragmentViewDataBinding.storelist.setAdapter(mStoreList)

        }

    }

    fun showByCityCategoryFilter(city: String, category: String) {

        isFound = false
        // val position=2
        /*  mFragmentViewDataBinding.catogerySpinner.getItemAtPosition(position).toString()
          mFragmentViewDataBinding.citySpinner.getItemAtPosition(position).toString()*/

        /*   if (position == 0) {
               setMarkers(mViewModel.storeLocatorResponse.value!!)
               return
           }*/

        mViewModel.storeLocatorResponse.value!!.data.stores?.let {
            /* val city = mViewModel.storeLocatorResponse.value!!.data.city!![position]
             val catogery = mViewModel.storeLocatorResponse.value!!.data.type!![position]*/

            val list = ArrayList<StoresItem>()

            for (storeItem in it) {

                if(city.equals(getLocalizedAllTitle()) && category.equals(getLocalizedAllTitle())){
                    list.add(storeItem)
                    isFound = true
                }
                else if(category.equals(getLocalizedAllTitle())){
                    if (storeItem.city.equals(city) ){
                        list.add(storeItem)
                        isFound = true
                    }
                }
                else if(city.equals(getLocalizedAllTitle())){
                    if (storeItem.type.equals(category) ){
                        list.add(storeItem)
                        isFound = true
                    }
                }

                else {
                    if (storeItem.city.equals(city) && storeItem.type.equals(category)) {
                        list.add(storeItem)
                        isFound = true
                    }
                }


            }

            mStoreList = StoreListAdapter(context!!, list)
            mFragmentViewDataBinding.storelist.setAdapter(mStoreList)

            if (!isFound) {
                list.clear()
            }

            if(list.size>0) {
                hideNoData()
            }
            else{
                showNoData()
            }
        }

    }

    private fun showNoData() {
        mFragmentViewDataBinding.noDataFoundLayout.visibility=View.VISIBLE
        mFragmentViewDataBinding.storelist.visibility=View.GONE

    }

    private fun hideNoData() {
        mFragmentViewDataBinding.noDataFoundLayout.visibility=View.GONE
        mFragmentViewDataBinding.storelist.visibility=View.VISIBLE

    }



}
