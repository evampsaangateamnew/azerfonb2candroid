package com.azarphone.ui.fragment.newautopayment.monthly

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.AddPaymentSchedulerRequest
import com.azarphone.api.pojo.response.autopaymentresponse.daily.AddSchedulePaymentResponse
import com.azarphone.api.pojo.response.topup.savedcards.SavedCardsResponse
import io.reactivex.Observable

class MonthlyAutoPaymentRepository {


    fun requestSavedCards(): Observable<SavedCardsResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestSavedCards()
    }


    fun requestPaymentScheduler(addPaymentSchedulerRequest: AddPaymentSchedulerRequest): Observable<AddSchedulePaymentResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestAddSchedulePayment(addPaymentSchedulerRequest)
    }
}