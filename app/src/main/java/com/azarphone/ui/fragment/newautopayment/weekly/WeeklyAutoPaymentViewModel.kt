package com.azarphone.ui.fragment.newautopayment.weekly

import androidx.lifecycle.MutableLiveData
import com.azarphone.analytics.Analytics
import com.azarphone.analytics.EventValues
import com.azarphone.api.pojo.request.AddPaymentSchedulerRequest
import com.azarphone.api.pojo.response.autopaymentresponse.daily.AddSchedulePaymentResponse
import com.azarphone.api.pojo.response.topup.savedcards.SavedCardsResponse
import com.azarphone.bases.BaseViewModel
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

class WeeklyAutoPaymentViewModel(val weeklyAutoPaymentRepository: WeeklyAutoPaymentRepository) : BaseViewModel() {
    private lateinit var disposable: Disposable
    val savedCardsResponseLiveData = MutableLiveData<SavedCardsResponse>()
    val addPaymentSchedulerResponseLiveData = MutableLiveData<AddSchedulePaymentResponse>()

    fun requestSavedCards() {
        view()?.showLoading()

        var mObserver = weeklyAutoPaymentRepository.requestSavedCards()
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            /*  view()?.isErrorErrorDialoge(result)?.let {
                                  if (it != true) {*/
                            savedCardsResponseLiveData.postValue(result)
                            Analytics.logAppEvent(EventValues.TopUpEvents.top_up, EventValues.TopUpEvents.nar_card, EventValues.GenericValues.Success)
                            /*       }
                               }*/
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                            Analytics.logAppEvent(EventValues.TopUpEvents.top_up, EventValues.TopUpEvents.nar_card, EventValues.GenericValues.Failure)
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestSavedCards ends

    fun requestPaymentScheduler(addPaymentSchedulerRequest: AddPaymentSchedulerRequest) {
        view()?.showLoading()

        var mObserver = weeklyAutoPaymentRepository.requestPaymentScheduler(addPaymentSchedulerRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    addPaymentSchedulerResponseLiveData.postValue(result)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                            Analytics.logAppEvent(EventValues.TopUpEvents.top_up, EventValues.TopUpEvents.nar_card, EventValues.GenericValues.Failure)
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestPaymentScheduler ends
}