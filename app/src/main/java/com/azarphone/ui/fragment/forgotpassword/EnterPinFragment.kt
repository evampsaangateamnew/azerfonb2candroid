package com.azarphone.ui.fragment.forgotpassword

import androidx.lifecycle.Observer
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.azarphone.R
import com.azarphone.api.pojo.response.otpgetresponse.OTPResponse
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.EnterPinLayoutBinding
import com.azarphone.eventhandler.ForgotPasswordActivityToolTipEvents
import com.azarphone.ui.activities.forgotpasswordactivity.ForgotPasswordActivity
import com.azarphone.ui.activities.forgotpasswordactivity.ForgotPasswordFactory
import com.azarphone.ui.activities.forgotpasswordactivity.ForgotPasswordInjection
import com.azarphone.ui.activities.forgotpasswordactivity.ForgotPasswordViewModel
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.RootValues
import com.azarphone.util.getALSBoldFont
import com.azarphone.util.logE
import com.azarphone.validators.hasValue
import com.azarphone.validators.isValidOTP
import com.azarphone.widgets.popups.showMessageDialog
import com.azarphone.widgets.tooltip.initiateErrorToolTip
import io.canner.stepsview.StepsView
import kotlinx.android.synthetic.main.enter_pin_layout.view.*

@Suppress("DEPRECATION")
class EnterPinFragment : BaseFragment<EnterPinLayoutBinding, ForgotPasswordFactory, ForgotPasswordViewModel>() {

    private var msisdnFromPreviousScreen = ""
    private val fromClass = "EnterPinFragment"
    private var forgotPasswordActivityToolTipEvents: ForgotPasswordActivityToolTipEvents? = null
    private lateinit var externalView: View

    override fun getViewModelClass(): Class<ForgotPasswordViewModel> {
        return ForgotPasswordViewModel::class.java
    }

    override fun getFactory(): ForgotPasswordFactory {
        return ForgotPasswordInjection.provideForgotPasswordFactory()
    }

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }

    override fun getLayoutId(): Int {
        return R.layout.enter_pin_layout
    }

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        externalView = view
        setupInputFields()
        registerListener()

        externalView.resendTV.isSelected = true
        externalView.pinNotRecievedTV.isSelected = true
        externalView.otpbackButton.isSelected = true
        mFragmentViewDataBinding.apply {
            viewmodel = mViewModel
        }

        //set the forgot password screen step name
        mViewModel.setForgotPasswordStep(ConstantsUtility.ForgetPasswordScreenSteps.ENTER_PIN_STEP)

        //get the msisdnFromPreviousScreen from the previous screen
        if (arguments != null && arguments!!.containsKey(ConstantsUtility.BundleKeys.BUNDLE_FORGOT_PASSWORD_NUMBER)) {
            msisdnFromPreviousScreen = arguments!!.getString(ConstantsUtility.BundleKeys.BUNDLE_FORGOT_PASSWORD_NUMBER)!!
        }

        //subscribe to the request api responses live data observers
        subscribe()

        mViewModel.set25SecondsTimer()


        view.verifyBtn.setOnClickListener {
            processOnNext()
        }

        view.resendTV.setOnClickListener {
            mViewModel.resendPin()
        }

        view.otpbackButton.setOnClickListener {
            activity?.let {
                it.onBackPressed()
            }
        }
        activity?.let {
            val stepView = it.findViewById<StepsView>(R.id.stepsView).setCompletedPosition(1).drawView()
        }

        view.enterPinET.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if (event!!.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    (activity as ForgotPasswordActivity).processForgotPassword()
                }//if ends
                return false
            }//onKey ends
        })

        externalView.enterPinET.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    (activity as ForgotPasswordActivity).processForgotPassword()
                }//if ends
                return false
            }//onEditorAction ends
        })
    }

    private fun registerListener() {
        forgotPasswordActivityToolTipEvents = object : ForgotPasswordActivityToolTipEvents {
            override fun onInvalidPasswordTooltip(errorDetail: String) {
                //do nothing
            }

            override fun onInvalidConfirmPasswordTooltip(errorDetail: String) {
                //do nothing
            }

            override fun onInvalidOTPPINTooltip(errorDetail: String) {
                logE("toolerX", "detail:::".plus(errorDetail), fromClass, "onInvalidOTPPINTooltip")
                initiateErrorToolTip(context!!, externalView.enterPinET, errorDetail)
            }

            override fun onInvalidMsisdnNumberTooltip(errorDetail: String) {
                //do nothing
            }
        }

        RootValues.setForgotPasswordActivityToolTipEvents(forgotPasswordActivityToolTipEvents!!)
    }

    private fun setupInputFields() {
        externalView.enterPinTIL.defaultHintTextColor = context!!.resources.getColorStateList(R.color.soft_pink)
        externalView.enterPinET.typeface = getALSBoldFont()
    }//setupInputFields ends

    private fun processOnNext() {
        val otp = externalView.enterPinET.text.toString()
        if (isValidOTP(otp)) {
            //call verify otp here
            /*val action = EnterPinFragmentDirections.actionEnterPinToUpdatePassword()
        findNavController().navigate(action)*/
        } else {
            showMessageDialog(context!!, activity!!,
                    context!!.resources.getString(R.string.popup_error_title),
                    context!!.resources.getString(R.string.error_msg_invalid_otp))
        }
    }

    override fun onInVisible() {

    }

    private fun subscribe() {
        val forgotPasswordOTPLiveData = object : Observer<OTPResponse> {
            override fun onChanged(t: OTPResponse?) {
                //for temperoray we are showing the otp here
                if (t != null && hasValue(t.data.toString()) && hasValue(t.data.pin!!)) {
                    logE("forgotpasswordotp", "otp:::".plus(t.data.pin), fromClass, "forgotPasswordOTPLiveData->onChanged")
                }
            }
        }

        val countDownTimerSecValue = object : Observer<Int> {
            override fun onChanged(t: Int?) {
                if (activity != null && !activity!!.isFinishing) {
                    t?.let {
                        val text = context!!.resources.getString(R.string.signup_password_timer_text, "" + it)
                        mFragmentViewDataBinding.otpTimerText.text = text
                    }
                }
            }
        }

        mViewModel.timerField.observe(this, countDownTimerSecValue)
        mViewModel.forgotPasswordOTPLiveData.observe(this, forgotPasswordOTPLiveData)

    }//subscribeToRequestOTPObservers ends

}//class ends