package com.azarphone.ui.fragment.internet

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.response.inappsurvey.InAppSurvey
import com.azarphone.api.pojo.response.inappsurvey.Surveys
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.internetpackresponse.InternetPacksResponse
import com.azarphone.api.pojo.response.internetpackresponse.Offer
import com.azarphone.api.pojo.response.mysubscriptions.successresponse.MySubscriptionsSuccessAfterOfferSubscribeResponse
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.InternetFragmentLayoutBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.*
import com.azarphone.ui.activities.mainactivity.MainFactory
import com.azarphone.ui.activities.mainactivity.MainInjection
import com.azarphone.ui.activities.mainactivity.MainViewModel
import com.azarphone.ui.adapters.pagers.InternetPagerAdapter
import com.azarphone.ui.dialogs.InAppFeedbackDialog
import com.azarphone.ui.fragment.internetoffers.InternetOffersPagerFragment
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.widgets.popups.showMessageDialog
import com.es.tec.LocalSharedPrefStorage
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.internet_fragment_layout.*
import java.util.*

class InternetFragment : BaseFragment<InternetFragmentLayoutBinding, MainFactory, MainViewModel>() {

    private val fromClass: String = "InternetFragment"
    private val offeringId: String = ""
    private var internetPacksResponse: InternetPacksResponse? = null
    private var redirectionIndex = 0
    private lateinit var internetPagerAdapter: InternetPagerAdapter
    private val tabNames = ArrayList<String>()
//    private val DIALOG_FILTER = "supplementary.filter"

    override fun getLayoutId(): Int {
        return R.layout.internet_fragment_layout
    }

    override fun getViewModelClass(): Class<MainViewModel> {
        return MainViewModel::class.java
    }

    override fun getFactory(): MainFactory {
        return MainInjection.provideMainFactory()
    }

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }

    override fun onResume() {
        super.onResume()
        if (RootValues.getAzerFonActionBarEvents() != null) {
            RootValues.getAzerFonActionBarEvents()!!.onInternetFragmentSetActionBar()
        }
    }

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        initUIEvents()
        initSupplementaryOffersCardAdapterEvents()
        initInternetOffersResponseEvents()
        initInternetSubscriptionResponseEvents()
        requestInternetOffers()
//        initFilterApplyEvents()
    }

    private fun initUIEvents() {
        mFragmentViewDataBinding.apply {
            tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabReselected(p0: TabLayout.Tab?) {

                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {
                    for (i in 0 until tabLayout.tabCount) {
                        val tabView = tabLayout.getTabAt(i)
                        if (tabView != null && tabView.customView != null) {
                            val tabTextView = tabView.customView as TextView
                            setUnSelectedAutoPaymentTabFont(context!!, tabTextView)
                        }
                    }
//                if (tab != null && tab.customView != null) {
//                    val tabTextView = tab.customView as TextView
//                    setUnSelectedAutoPaymentTabFont(context!!, tabTextView)
//                }
                }//onTabUnselected ends

                override fun onTabSelected(tab: TabLayout.Tab?) {
                    internetViewpager.setCurrentItem(tab!!.position, false)
                    if (tab != null && tab.customView != null) {
                        val tabTextView = tab.customView as TextView
                        setSelectedAutoPaymentTabFont(context!!, tabTextView)
                    }
                }//onTabSelected ends
            })//tabLayout.addOnTabSelectedListener ends
        }
    }//initUIEvents ends

    private fun setUpTabs(internetPacksResponse: InternetPacksResponse?) {

        mFragmentViewDataBinding.apply {
            tabLayout.removeAllTabs()

            internetPacksResponse?.data?.allBundles?.let {
                tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.internet_packs_all)))
                tabNames.add(getString(R.string.internet_packs_all))
            }


            internetPacksResponse?.data?.monthlyBundles?.let {
                tabLayout.addTab(
                    tabLayout.newTab().setText(getString(R.string.internet_packs_monthly))
                )
                tabNames.add(getString(R.string.internet_packs_monthly))
            }

            internetPacksResponse?.data?.weeklyBundles?.let {
                tabLayout.addTab(
                    tabLayout.newTab().setText(getString(R.string.add_auto_payment_weekly))
                )
                tabNames.add(getString(R.string.add_auto_payment_weekly))
            }

            internetPacksResponse?.data?.dailyBundles?.let {
                tabLayout.addTab(
                    tabLayout.newTab().setText(getString(R.string.internet_packs_daily))
                )
                tabNames.add(getString(R.string.internet_packs_daily))
            }

            internetPacksResponse?.data?.hourlyBundles?.let {
                tabLayout.addTab(
                    tabLayout.newTab().setText(getString(R.string.internet_packs_hourly))
                )
                tabNames.add(getString(R.string.internet_packs_hourly))
            }


            for (i in 0 until tabLayout.tabCount) {
                val tab = (tabLayout.getChildAt(0) as ViewGroup).getChildAt(i)
                val p = tab.layoutParams as ViewGroup.MarginLayoutParams
                val margin = (15 * resources.displayMetrics.density)
                if (i < tabLayout.tabCount - 1) {
                    p.setMargins(0, 0, margin.toInt(), 0)
                }
                tab.requestLayout()

                //set the custom view
                val tabView = tabLayout.getTabAt(i)
                if (tabView != null) {
                    val tabTextView = TextView(context)
                    tabView.customView = tabTextView
                    //set the text
                    tabTextView.text = tabView.text
                    tabTextView.ellipsize = TextUtils.TruncateAt.MARQUEE
                    tabTextView.isSelected = true
                    tabTextView.marqueeRepeatLimit = -1
                    tabTextView.isSingleLine = true
                    tabTextView.layoutParams = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                    tabTextView.gravity = Gravity.CENTER
                    //set the currently selected tab font
                    if (i == 0) {
                        setSelectedAutoPaymentTabFont(context!!, tabTextView)
                    } else {
                        setUnSelectedAutoPaymentTabFont(context!!, tabTextView)
                    }
                }
            }
        }
//        gotoTabByIndex(redirectionIndex)
    }

    private fun gotoTabByIndex(index: Int) {
        if (tabLayout != null && tabLayout.tabCount > 0) {
            Handler().postDelayed({
                tabLayout.getTabAt(index)!!.select()
                tabLayout.isSmoothScrollingEnabled = true
                tabLayout.setScrollPosition(index, 0f, true)
            }, 50L)
        }
    }//gotoTabByIndex ends

    override fun onInVisible() {}

    private fun hideContent() {
        mFragmentViewDataBinding.apply {
            noDataFoundLayout.visibility = View.VISIBLE
            tabLayout.visibility = View.GONE
            internetViewpager.visibility = View.GONE
        }
    }//hideContent ends

    private fun showContent() {
        mFragmentViewDataBinding.apply {
            noDataFoundLayout.visibility = View.GONE
            tabLayout.visibility = View.VISIBLE
            internetViewpager.visibility = View.VISIBLE
        }
    }//hideContent ends

    /** Setting Up tabs and Fragments
     *  for view pager according to
     *  data received from server*/
    private fun initViewPager(internetPacksResponse: InternetPacksResponse?) {
        val pagerFragmentsList = ArrayList<Fragment>()
        var foundTabPosFromDynamicLink = -1
        if (tabLayout != null && tabLayout.tabCount > 0) {
            for (its in 0..tabLayout.tabCount) {
                if (getString(R.string.internet_packs_all).equals(
                        getCurrentSelectedTabName(its),
                        ignoreCase = true
                    )
//                    && !internetPacksResponse?.data?.allBundles?.offers.isNullOrEmpty()
                ) {
                    logE("pacClick", "daily populated", fromClass, "initViewPager")

                    internetPacksResponse?.data?.allBundles?.offers?.let {

                        val foundOfferItemFromDynamic =
                            findOfferIdFromDynamicLinks(
                                offeringId,
                                internetPacksResponse.data.allBundles.offers as ArrayList<Offer?>
                            )

                        if (foundOfferItemFromDynamic != -1) {
                            foundTabPosFromDynamicLink = 0
                        }


                        pagerFragmentsList.add(
                            InternetOffersPagerFragment.getInstance(
                                foundTabPosFromDynamicLink,
                                getCurrentSelectedTabName(its),
                                it
                            )
                        )
                    } ?: run {
                        pagerFragmentsList.add(
                            InternetOffersPagerFragment.getInstance(
                                0,
                                getCurrentSelectedTabName(its),
                                ArrayList<Offer?>()
                            )
                        )
                    }
                } else if (getString(R.string.internet_packs_daily).equals(
                        getCurrentSelectedTabName(its),
                        ignoreCase = true
                    )
//                    && !internetPacksResponse?.data?.dailyBundles?.offers.isNullOrEmpty()
                ) {
                    logE("pacClick", "daily populated", fromClass, "initViewPager")

                    internetPacksResponse?.data?.dailyBundles?.offers?.let {

                        val foundOfferItemFromDynamic =
                            findOfferIdFromDynamicLinks(
                                offeringId,
                                internetPacksResponse.data.dailyBundles.offers as ArrayList<Offer?>
                            )

                        if (foundOfferItemFromDynamic != -1) {
                            foundTabPosFromDynamicLink = 1
                        }

                        pagerFragmentsList.add(
                            InternetOffersPagerFragment.getInstance(
                                foundTabPosFromDynamicLink,
                                getCurrentSelectedTabName(its),
                                it
                            )
                        )
                    } ?: run {
                        pagerFragmentsList.add(
                            InternetOffersPagerFragment.getInstance(
                                0,
                                getCurrentSelectedTabName(its),
                                ArrayList<Offer?>()
                            )
                        )
                    }
                } else if (getString(R.string.add_auto_payment_weekly).equals(
                        getCurrentSelectedTabName(its),
                        ignoreCase = true
                    )
//                    && !internetPacksResponse?.data?.weeklyBundles?.offers.isNullOrEmpty()
                ) {

                    internetPacksResponse?.data?.weeklyBundles?.offers?.let {

                        val foundOfferItemFromDynamic =
                            findOfferIdFromDynamicLinks(
                                offeringId,
                                internetPacksResponse.data.weeklyBundles.offers as ArrayList<Offer?>
                            )

                        if (foundOfferItemFromDynamic != -1) {
                            foundTabPosFromDynamicLink = 2
                        }

                        pagerFragmentsList.add(
                            InternetOffersPagerFragment.getInstance(
                                foundTabPosFromDynamicLink,
                                getCurrentSelectedTabName(its),
                                it
                            )
                        )
                    } ?: run {
                        pagerFragmentsList.add(
                            InternetOffersPagerFragment.getInstance(
                                0,
                                getCurrentSelectedTabName(its),
                                ArrayList<Offer?>()
                            )
                        )
                    }
                } else if (getString(R.string.internet_packs_monthly).equals(
                        getCurrentSelectedTabName(its),
                        ignoreCase = true
                    )
//                    && !internetPacksResponse?.data?.monthlyBundles?.offers.isNullOrEmpty()
                ) {
                    internetPacksResponse?.data?.monthlyBundles?.offers?.let {

                        val foundOfferItemFromDynamic =
                            findOfferIdFromDynamicLinks(
                                offeringId,
                                internetPacksResponse.data.monthlyBundles.offers as ArrayList<Offer?>
                            )

                        if (foundOfferItemFromDynamic != -1) {
                            foundTabPosFromDynamicLink = 2
                        }


                        pagerFragmentsList.add(
                            InternetOffersPagerFragment.getInstance(
                                foundTabPosFromDynamicLink,
                                getCurrentSelectedTabName(its),
                                it
                            )
                        )
                    } ?: run {
                        pagerFragmentsList.add(
                            InternetOffersPagerFragment.getInstance(
                                0,
                                getCurrentSelectedTabName(its),
                                ArrayList<Offer?>()
                            )
                        )
                    }
                } else if (getString(R.string.internet_packs_hourly).equals(
                        getCurrentSelectedTabName(its),
                        ignoreCase = true
                    )
//                    && !internetPacksResponse?.data?.hourlyBundles?.offers.isNullOrEmpty()
                ) {
                    logE("pacClick", "daily populated", fromClass, "initViewPager")



                    internetPacksResponse?.data?.hourlyBundles?.offers?.let {
                        val foundOfferItemFromDynamic =
                            findOfferIdFromDynamicLinks(
                                offeringId,
                                internetPacksResponse.data.hourlyBundles.offers as ArrayList<Offer?>
                            )

                        if (foundOfferItemFromDynamic != -1) {
                            foundTabPosFromDynamicLink = 3
                        }

                        pagerFragmentsList.add(
                            InternetOffersPagerFragment.getInstance(
                                foundTabPosFromDynamicLink,
                                getCurrentSelectedTabName(its),
                                it
                            )
                        )
                    } ?: run {
                        pagerFragmentsList.add(
                            InternetOffersPagerFragment.getInstance(
                                0,
                                getCurrentSelectedTabName(its),
                                ArrayList<Offer?>()
                            )
                        )
                    }
                }
            }//for ends
        }//if(tabLayout!=null&&tabLayout.tabCount>0) ends

        internetPagerAdapter =
            InternetPagerAdapter(childFragmentManager, pagerFragmentsList)
        if (internetPagerAdapter != null) {
            mFragmentViewDataBinding.apply {
                internetViewpager.adapter = internetPagerAdapter
            }

        }

        if (foundTabPosFromDynamicLink != -1 && hasValue(offeringId)) {
            redirectionIndex = foundTabPosFromDynamicLink
        }

        if (pagerFragmentsList.isNotEmpty()) {
            internetViewpager.currentItem = redirectionIndex
        }
    }

    /**Getting Tab Name from
     * list according to index*/
    private fun getCurrentSelectedTabName(index: Int): String {
        var tabName = ""

        try {
            tabName = tabNames[index]
        } catch (exp: Exception) {

        }

        return tabName
    }//getCurrentSelectedTabName ends

    /** Calling Api call
     * if Network is available
     * otherwise getting Data from Cache*/
    private fun requestInternetOffers() {
        if (context?.let { isNetworkAvailable(it) } == true) {
            Handler().postDelayed({ callInternetOffersAPI() }, 50L)
        } else {
            val cachedSupplementaryOffers = getInternetOffersFromLocalPreferences()
            if (cachedSupplementaryOffers.data != null && hasValue(cachedSupplementaryOffers.data.toString())) {
                internetPacksResponse = cachedSupplementaryOffers
                setInternetOffersDataToLocalPreferences(internetPacksResponse!!)
                setUpTabs(internetPacksResponse!!)
                initViewPager(internetPacksResponse!!)
                showContent()
            } else {
                showMessageDialog(
                    context!!, activity!!,
                    "", resources.getString(R.string.message_no_internet)
                )
            }
        }
//        }
    }//requestSupplementaryOffers ends

    /** Calling Api call to get
     * Internet Packages data from server*/
    private fun callInternetOffersAPI() {
        var offeringName = ""
        var brandName = ""
        if (UserDataManager.getCustomerData() != null) {

            if (hasValue(UserDataManager.getCustomerData()!!.offeringId)) {
                offeringName = UserDataManager.getCustomerData()!!.offeringId!!
            }

            if (hasValue(UserDataManager.getCustomerData()!!.brandName)) {
                brandName = UserDataManager.getCustomerData()!!.brandName!!
            }
        }

        mViewModel.requestInternetOffers(offeringName, brandName)
    }//callSupplementaryOffersAPI ends

    /**Finding Offering Id
     *  sent in Dynamic Link*/
    private fun findOfferIdFromDynamicLinks(
        offeringId: String,
        offersList: ArrayList<Offer?>
    ): Int {
        var foundOfferPosFromNotification = -1
        try {
            if (hasValue(offeringId) && !offersList.isNullOrEmpty()) {
                for (i in offersList.indices) {
                    if (offersList[i] != null &&
                        offersList[i]!!.header != null &&
                        hasValue(offersList[i]!!.header!!.offeringId) &&
                        offersList[i]!!.header!!.offeringId == offeringId
                    ) {
                        foundOfferPosFromNotification = i
                        break
                    }//if ends
                }//for ends
            }//outer if ends
        } catch (e: Exception) {
            logE(
                "Packages",
                "error:::".plus(e.toString()),
                "PackagesActivity",
                "findOfferIdAfterRedirectingFromNotifications"
            )
        }//catch ends
        logE(
            ConstantsUtility.DynamicLinkConstants.DYNAMIC_LINK_TAG,
            "foundOfferPosFromNotification = $foundOfferPosFromNotification",
            fromClass,
            "findOfferIdAfterRedirectingFromNotifications"
        )
        return foundOfferPosFromNotification
    }//findOfferIdAfterRedirectingFromNotifications ends

    /** Implemented interface to send
     * Api call by clicking button in
     * Pager Fragment*/
    private fun initSupplementaryOffersCardAdapterEvents() {
        val supplementaryOffersCardAdapterEvents = object : SupplementaryOffersCardAdapterEvents {
            override fun onSupplementaryOfferSubscribeButtonClick(
                titleMessage: String,
                offeringId: String,
                offeringName: String,
                actionType: String
            ) {
                if (activity != null && !activity!!.isFinishing) {
                    logE(
                        "offerSubscribe",
                        "offername:::".plus(offeringName).plus(" actionType:::$actionType")
                            .plus(" offeringId:::").plus(offeringId),
                        fromClass,
                        "initSupplementaryOffersCardAdapterEvents"
                    )
                    /**call the api to subscribe the offer*/
                    showSupplementaryOfferSubscribePopup(
                        titleMessage,
                        offeringName,
                        offeringId,
                        actionType
                    )
                }
            }
        }

        RootValues.setInternetOffersCardAdapterEvents(supplementaryOffersCardAdapterEvents)
    }//initSupplementaryOffersCardAdapterEvents ends

    /** Getting Internet Offers from server*/
    private fun initInternetOffersResponseEvents() {
        val internetOffersResponseEvents = object : InternetOffersResponseEvents {
            override fun onResponseSuccessful(t: InternetPacksResponse?) {
                if (activity != null && !activity!!.isFinishing) {
                    if (t?.data != null) {
                        internetPacksResponse = null
                        internetPacksResponse = t
                        setUpTabs(internetPacksResponse!!)
                        initViewPager(internetPacksResponse!!)
                        setInternetOffersDataToLocalPreferences(t)
                        showContent()
                    } else {
                        hideContent()
                    }
                }
            }

            override fun onResponseError() {
                hideContent()
            }

        }
        RootValues.setInternetOffersResponseEvents(internetOffersResponseEvents)
    }//initInternetOffersResponseEvents ends

    /** Getting Internet Offer Subscription response from server*/
    private fun initInternetSubscriptionResponseEvents() {
        val internetSubscriptionResponseEvents = object : InternetOffersSubscriptionResponseEvents {
            override fun onInternetSubscribeSuccessful(t: MySubscriptionsSuccessAfterOfferSubscribeResponse?) {
                if (activity != null && !activity!!.isFinishing) {
                    if (t?.data?.mySubscriptionsData != null) {

                        if (t.data.message != null && hasValue(t.data.message)) {

                            inAppFeedback(requireContext(),resources.getString(R.string.popup_note_title),t.resultDesc!!)

                            mViewModel.subscribeToSupplementaryOfferResponseLiveData.postValue(
                                null
                            )
                        }

                        /** update the mysubscriptions response*/
                        if (UserDataManager.getMySubscriptionResponse() != null) {
                            val mySubscriptions = UserDataManager.getMySubscriptionResponse()

                            if (mySubscriptions!!.data != null &&
                                t.data.mySubscriptionsData.internet != null
                            ) {
                                mySubscriptions.data!!.internet =
                                    t.data.mySubscriptionsData.internet
                            }

                            if (mySubscriptions.data != null &&
                                t.data.mySubscriptionsData.sms != null
                            ) {
                                mySubscriptions.data!!.sms = t.data.mySubscriptionsData.sms
                            }

                            if (mySubscriptions.data != null &&
                                t.data.mySubscriptionsData.call != null
                            ) {
                                mySubscriptions.data!!.call = t.data.mySubscriptionsData.call
                            }

                            if (mySubscriptions.data != null &&
                                t.data.mySubscriptionsData.campaign != null
                            ) {
                                mySubscriptions.data!!.campaign =
                                    t.data.mySubscriptionsData.campaign
                            }

                            if (mySubscriptions.data != null &&
                                t.data.mySubscriptionsData.hybrid != null
                            ) {
                                mySubscriptions.data!!.hybrid =
                                    t.data.mySubscriptionsData.hybrid
                            }

                            if (mySubscriptions.data != null &&
                                t.data.mySubscriptionsData.voiceInclusiveOffers != null
                            ) {
                                mySubscriptions.data!!.voiceInclusiveOffers =
                                    t.data.mySubscriptionsData.voiceInclusiveOffers
                            }

                            if (mySubscriptions.data != null &&
                                t.data.mySubscriptionsData.roaming != null
                            ) {
                                mySubscriptions.data!!.roaming =
                                    t.data.mySubscriptionsData.roaming
                            }

                            if (mySubscriptions.data != null &&
                                t.data.mySubscriptionsData.smsInclusiveOffers != null
                            ) {
                                mySubscriptions.data!!.smsInclusiveOffers =
                                    t.data.mySubscriptionsData.smsInclusiveOffers
                            }

                            if (mySubscriptions.data != null &&
                                t.data.mySubscriptionsData.tm != null
                            ) {
                                mySubscriptions.data!!.tm = t.data.mySubscriptionsData.tm
                            }

                            if (mySubscriptions.data != null &&
                                t.data.mySubscriptionsData.internetInclusiveOffers != null
                            ) {
                                mySubscriptions.data!!.internetInclusiveOffers =
                                    t.data.mySubscriptionsData.internetInclusiveOffers
                            }

                            UserDataManager.setMySubscriptionResponse(mySubscriptions)
                        }

                        /**notify the data adapter in each fragment of this activity*/
                        if (RootValues.getInternetOffersSubscriptionEvents() != null) {
                            RootValues.getInternetOffersSubscriptionEvents()!!
                                .onSupplementaryOffersSubscriptionSuccess()
                        } else {
                            logE(
                                "AdapterXu",
                                "listener is null",
                                fromClass,
                                "initSupplementaryOffersCardAdapterEvents"
                            )
                        }
                    }
                }
            }

            override fun onInternetSubscribeError() {

            }

        }
        RootValues.setInternetOffersSubscriptionResponseEvents(internetSubscriptionResponseEvents)
    }//initInternetSubscriptionResponseEvents ends

    private fun inAppFeedback(context: Context, title: String, message: String) {

        var inAppFeedbackDialog : InAppFeedbackDialog?= null
        var currentVisit: Int =
            LocalSharedPrefStorage(context).getInt(LocalSharedPrefStorage.PREF_BUNDLE_PAGE)
        currentVisit += 1
        val inAppSurvey: InAppSurvey? = UserDataManager.getInAppSurvey()
        if ((inAppSurvey?.data) != null) {
            val surveys: Surveys? =
                inAppSurvey.data.findSurvey(ConstantsUtility.InAppSurveyConstants.BUNDLE_PAGE)
            if (surveys != null) {
                inAppFeedbackDialog = InAppFeedbackDialog(context,object : OnClickSurveySubmit
                {
                    override fun onClickSubmit(
                        uploadSurvey: UploadSurvey,
                        onTransactionComplete: String,updateListOnSuccessfullySurvey: UpdateListOnSuccessfullySurvey?
                    ) {
                        mViewModel.uploadInAppSurvey(context,uploadSurvey,onTransactionComplete,updateListOnSuccessfullySurvey)
                        if (inAppFeedbackDialog?.isShowing == true) {
                            inAppFeedbackDialog?.dismiss()
                        }
                    }

                })

                if (surveys.questions != null) {
                    currentVisit = 0
                    inAppFeedbackDialog.showTitleWithMessageDialog(surveys,title,message)

                } else {
                    showMessageDialog(
                        context, requireActivity(),
                        title,
                        message
                    )
                }
            } else {
                showMessageDialog(
                    context, requireActivity(),
                    title,
                    message
                )
            }
        } else {
            showMessageDialog(
                context, requireActivity(),
                resources.getString(R.string.popup_note_title),
                message
            )
        }
        LocalSharedPrefStorage(context).addInt(
            LocalSharedPrefStorage.PREF_BUNDLE_PAGE,
            currentVisit
        )
    }
    /** Showing Confirmation PopUp*/
    @SuppressLint("InflateParams")
    private fun showSupplementaryOfferSubscribePopup(
        title: String,
        offeringName: String,
        offeringId: String,
        actionType: String
    ) {
        if (activity?.isFinishing!!) return

        val dialogBuilder = AlertDialog.Builder(context!!)
        val inflater =
            activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val dialogView = inflater.inflate(R.layout.azer_vas_services_confirmation_popup, null)
        dialogBuilder.setView(dialogView)

        val alertDialog = dialogBuilder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.show()

        val popupTitle = dialogView.findViewById(R.id.popupTitle) as TextView
        popupTitle.typeface = getALSNormalFont()
        popupTitle.text = title
        val popupOkayButton = dialogView.findViewById(R.id.yesButton) as Button
        popupOkayButton.typeface = getALSBoldFont()
        val popupCancelButton = dialogView.findViewById(R.id.noButton) as Button
        popupCancelButton.typeface = getALSBoldFont()

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN) {
            //english
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "No"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ) {
            //azri
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "Xeyr"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.RU) {
            //russian
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "Нет"
        }

        popupOkayButton.setOnClickListener {
            alertDialog.dismiss()
            mViewModel.subscribeToSupplementaryOffer(offeringName, offeringId, actionType)
        }

        popupCancelButton.setOnClickListener {
            alertDialog.dismiss()
        }
    }//showSupplementaryOfferSubscribePopup ends
}