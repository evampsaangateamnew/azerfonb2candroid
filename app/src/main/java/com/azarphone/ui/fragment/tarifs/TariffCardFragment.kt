package com.azarphone.ui.fragment.tarifs

import android.app.Activity
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import com.azarphone.R
import com.azarphone.analytics.Analytics
import com.azarphone.analytics.EventValues
import com.azarphone.api.pojo.response.tariffsresponse.ItemsItem
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.LayoutTariffCardFragmentBinding
import com.azarphone.eventhandler.TariffCardsEvents
import com.azarphone.eventhandler.TariffsSearchPerformEvents
import com.azarphone.eventhandler.TariffsSearchResultListener
import com.azarphone.ui.activities.mainactivity.MainFactory
import com.azarphone.ui.activities.mainactivity.MainInjection
import com.azarphone.ui.activities.mainactivity.MainViewModel
import com.azarphone.ui.adapters.recyclerviews.TariffsCardAdapter
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.yarolegovich.discretescrollview.Orientation
import com.yarolegovich.discretescrollview.transform.ScaleTransformer
import kotlinx.android.synthetic.main.layout_supplementary_offers_pagger_fragment.view.*
import kotlinx.android.synthetic.main.layout_tariff_card_fragment.*
import kotlinx.android.synthetic.main.layout_tariff_card_fragment.view.*
import kotlinx.android.synthetic.main.layout_tariff_card_fragment.view.mDiscreteView
import kotlinx.android.synthetic.main.layout_tariff_card_fragment.view.noDataFoundLayout
import kotlinx.android.synthetic.main.no_data_found_white_no_bg.view.*
import java.util.*

/**
 * @author Junaid Hassan on 10, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class TariffCardFragment : BaseFragment<LayoutTariffCardFragmentBinding, MainFactory, MainViewModel>(), TariffsSearchResultListener {

    private val fromClass = "TariffCardFragment"
    private var tariffCardItems: List<ItemsItem?>? = null
    private lateinit var externalView: View
    private var actionIdFromServer = ""
    private var offeringIdFromDashBoard = ""
    private var tariffsCardAdapter: TariffsCardAdapter? = null
    private var pageTitle = ""

    companion object {
        fun getInstance(tariffCardItems: List<ItemsItem?>, actionIdFromServer: String, offeringIdFromDashBoard: String, pTitle: String): TariffCardFragment {
            val fragment = TariffCardFragment()
            val bundle = Bundle()
            bundle.putString(ConstantsUtility.TariffConstants.TARIFFS_PAGE_TITLE_DATA, pTitle)
            bundle.putString(ConstantsUtility.TariffConstants.TARIFF_REDIRECTION_OFFER_ID, offeringIdFromDashBoard)
            bundle.putString(ConstantsUtility.TariffConstants.TARIFF_REDIRECTION_INDEX, actionIdFromServer)
            bundle.putParcelableArrayList(ConstantsUtility.BundleKeys.BUNDLE_TARIFFS_CARD_ITEMS_DATA, tariffCardItems as ArrayList<out Parcelable>)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onSearchResult(isResultFound: Boolean) {
        if (activity != null && !activity!!.isFinishing) {
            activity!!.runOnUiThread {
                foundSearchResult(isResultFound)
            }
        }
    }//onSearchResult ends

    private fun foundSearchResult(isResultFound: Boolean) {
        if (isResultFound) {
            logE("foundResults", "found:::", fromClass, "foundSearchResult")
            externalView.mDiscreteView.visibility = View.VISIBLE
            externalView.noDataFoundLayout.visibility = View.GONE
        } else {
            logE("foundResults", "not found:::", fromClass, "foundSearchResult")
            externalView.mDiscreteView.visibility = View.GONE
            externalView.noDataFoundLayout.visibility = View.VISIBLE
            externalView.img_arrowleft_tarif.visibility= View.GONE
            externalView.img_arrowright_tarif.visibility= View.GONE
            externalView.noDataFoundTextLabel.text = getNoDataFoundOnSearchMessage()
            externalView.noDataFoundTextLabel.isSelected = true
        }
    }//foundSearchResult ends

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
    }//onAttach ends

    private fun initSearchEvents(pageTitleKey: String) {
        /**save each listener with title to an hashmap of user data manager
         * and register the events listeners for each tab for search filter*/
        val tariffsSearchPerformerEvents = object : TariffsSearchPerformEvents {
            override fun doTariffsSearch(searchFilter: String, pageTitleTestingKey: String) {
                logE("searchToX1", "searchKey:::".plus(searchFilter).plus(" pagetitlekey:::".plus(pageTitleTestingKey)), fromClass, "initSearchEvents")
                if (tariffsCardAdapter != null) {
                    tariffsCardAdapter!!.filter.filter(searchFilter)
                    Analytics.logSearchEvent(EventValues.SearchEvents.search, EventValues.SearchEvents.tariff_screen, searchFilter)
                }
            }
        }

        logE("saveLisX", "savingTariffListenerOf:::".plus(pageTitleKey), fromClass, "initSearchEvents")
        RootValues.getHashMapOfTariffListeners()[pageTitleKey] = tariffsSearchPerformerEvents
    }//initSearchEvents ends

    override fun getLayoutId(): Int {
        return R.layout.layout_tariff_card_fragment
    }//getLayoutId ends

    override fun getViewModelClass(): Class<MainViewModel> {
        return MainViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): MainFactory {
        return MainInjection.provideMainFactory()
    }//getFactory ends

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }//isBindedToActivityLifeCycle ends

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        if (activity != null && !activity!!.isFinishing) {
            externalView = view
            initTariffCardsEvents()
            if (arguments != null && arguments!!.containsKey(ConstantsUtility.BundleKeys.BUNDLE_TARIFFS_CARD_ITEMS_DATA)) {
                tariffCardItems = arguments!!.getParcelableArrayList(ConstantsUtility.BundleKeys.BUNDLE_TARIFFS_CARD_ITEMS_DATA)
            }//bundle

            if (arguments != null && arguments!!.containsKey(ConstantsUtility.TariffConstants.TARIFFS_PAGE_TITLE_DATA)) {
                pageTitle = arguments!!.getString(ConstantsUtility.TariffConstants.TARIFFS_PAGE_TITLE_DATA)!!
            }//bundle

            if (arguments != null && arguments!!.containsKey(ConstantsUtility.TariffConstants.TARIFF_REDIRECTION_INDEX)) {
                actionIdFromServer = arguments!!.getString(ConstantsUtility.TariffConstants.TARIFF_REDIRECTION_INDEX)!!
            }//bundle

            if (arguments != null && arguments!!.containsKey(ConstantsUtility.TariffConstants.TARIFF_REDIRECTION_OFFER_ID)) {
                offeringIdFromDashBoard = arguments!!.getString(ConstantsUtility.TariffConstants.TARIFF_REDIRECTION_OFFER_ID)!!
            }//bundle

            if (!tariffCardItems.isNullOrEmpty()) {
                externalView.mDiscreteView.visibility = View.VISIBLE
                externalView.noDataFoundLayout.visibility = View.GONE
                //set the recycler view
                externalView.mDiscreteView.setOrientation(Orientation.HORIZONTAL)
                externalView.mDiscreteView.setItemTransformer(ScaleTransformer.Builder().build())
                externalView.mDiscreteView.recycledViewPool.setMaxRecycledViews(0, 0)
                /*externalView.mDiscreteView.setOffscreenItems(2)*/
                tariffsCardAdapter = TariffsCardAdapter(context!!, tariffCardItems, this,mViewModel)
                externalView.mDiscreteView.adapter = tariffsCardAdapter
                externalView.mDiscreteView.addOnItemChangedListener { viewHolder, i -> updateArrowDescreteView(i) }

                externalView.img_arrowleft_tarif.setOnClickListener { leftArowClickListener() }
                externalView.img_arrowright_tarif.setOnClickListener { rightArowClickListener() }

            } else {
                externalView.img_arrowleft_tarif.visibility= View.GONE
                externalView.img_arrowright_tarif.visibility= View.GONE
                externalView.mDiscreteView.visibility = View.GONE
                externalView.noDataFoundLayout.visibility = View.VISIBLE
                externalView.noDataFoundTextLabel.text = getNoDataFoundSimpleMessage()
                externalView.noDataFoundTextLabel.isSelected = true
            }

            if (hasValue(offeringIdFromDashBoard)) {
                /**redirect to the specific card item*/
                if (tariffCardItems != null) {
                    for (its in tariffCardItems!!.indices) {
                        if (tariffCardItems!![its]!!.header != null &&
                                tariffCardItems!![its]!!.header!!.offeringId != null &&
                                hasValue(tariffCardItems!![its]!!.header!!.offeringId)) {

                            if (offeringIdFromDashBoard == tariffCardItems!![its]!!.header!!.offeringId) {
                                externalView.mDiscreteView.smoothScrollToPosition(its)
                                break
                            }
                        }//offering id not null ends
                    }//for ends
                }//if(tariffCardItems!=null) ends
            } else {
                if (hasValue(actionIdFromServer)) {
                    logE("acX","actionId:::".plus(actionIdFromServer),fromClass,"showingActionId")
                    for (its in tariffCardItems!!.indices) {
                        if (tariffCardItems!![its]!!.header != null &&
                                tariffCardItems!![its]!!.header!!.offeringId != null &&
                                hasValue(tariffCardItems!![its]!!.header!!.offeringId)) {

                            if (actionIdFromServer == tariffCardItems!![its]!!.header!!.offeringId) {
                                externalView.mDiscreteView.smoothScrollToPosition(its)
                                break
                            }
                        }//offering id not null ends
                    }//for ends
                }else{
                    logE("acX","noActionId:::",fromClass,"showingActionId")
                }
            }//if (hasValue(offeringIdFromDashBoard))  ends

            logE("tPTX", "pageTitle:::".plus(pageTitle), fromClass, "showingPageTitle")
            initSearchEvents(pageTitle)

            if (arguments != null && arguments!!.containsKey(ConstantsUtility.BundleKeys.BUNDLE_TARIFFS_CARD_ITEMS_DATA)) {
                arguments!!.clear()
            }

            if (arguments != null && arguments!!.containsKey(ConstantsUtility.TariffConstants.OFFERING_ID_BUNDLE_DATA_FRAGMENT)) {
                arguments!!.clear()
            }
        }
    }//onVisible ends



    private fun rightArowClickListener() {
        var index = externalView.mDiscreteView.currentItem.plus(1)
        if(index>=0 && index <tariffCardItems?.size!! && index <externalView.mDiscreteView.adapter?.itemCount!!)
            externalView.mDiscreteView.smoothScrollToPosition(index)
    }

    private fun leftArowClickListener() {
        var index = externalView.mDiscreteView.currentItem.minus(1)
        if(index>=0 && index <tariffCardItems?.size!! && index <externalView.mDiscreteView.adapter?.itemCount!!)
            externalView.mDiscreteView.smoothScrollToPosition(index)
    }

    private fun updateArrowDescreteView(position: Int) {
        if ((tariffCardItems?.size!!) <= 1 || (externalView.mDiscreteView.adapter?.itemCount!!) <= 1) {
            externalView.img_arrowleft_tarif.visibility = View.GONE
            externalView.img_arrowright_tarif.visibility = View.GONE
        } else
        if (position == 0) {
            externalView.img_arrowleft_tarif.visibility = View.GONE
            externalView.img_arrowright_tarif.visibility = View.VISIBLE
        } else if (position == (tariffCardItems?.size?.minus(1))) {
            externalView.img_arrowleft_tarif.visibility = View.VISIBLE
            externalView.img_arrowright_tarif.visibility = View.GONE
        } else {
            externalView.img_arrowleft_tarif.visibility = View.VISIBLE
            externalView.img_arrowright_tarif.visibility = View.VISIBLE
        }

    }

    private fun initTariffCardsEvents() {
        val tariffCardsEvents = object : TariffCardsEvents {
            override fun onTariffsDataUpdate() {
                if (tariffsCardAdapter != null) {
                    logE("adapter2jk", "not null", fromClass, "initTariffCardsEvents")
                    tariffsCardAdapter!!.notifyDataSetChanged()
                } else {
                    logE("adapter2jk", "adapter null null null", fromClass, "initTariffCardsEvents")
                }
            }
        }

        RootValues.setTariffCardsEvents(tariffCardsEvents)
    }//initTariffCardsEvents ends

    override fun onInVisible() {

    }//onInVisible ends

}//class ends