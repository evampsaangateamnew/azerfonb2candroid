package com.azarphone.ui.fragment.usagehistory

import android.app.DatePickerDialog
import androidx.lifecycle.Observer
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.TextView
import com.azarphone.R
import com.azarphone.api.pojo.response.usagehistoryresponse.SummaryListItem
import com.azarphone.api.pojo.response.usagehistoryresponse.UsageHistorySummaryResponse
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.LayoutUsageHistorySummaryBinding
import com.azarphone.ui.activities.usagehistory.UsageHistoryFactory
import com.azarphone.ui.activities.usagehistory.UsageHistoryInjection
import com.azarphone.ui.activities.usagehistory.UsageHistoryViewModel
import com.azarphone.ui.adapters.expandablelists.UsageHistorySummaryAdapter
import com.azarphone.ui.adapters.spinners.UsageHistorySpinnerAdapter
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.widgets.calendar.AzerCalendar
import kotlinx.android.synthetic.main.layout_usage_history_summary.view.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * @author Junaid Hassan on 04, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class UsageHistorySummaryFragment : BaseFragment<LayoutUsageHistorySummaryBinding, UsageHistoryFactory, UsageHistoryViewModel>() {

    private var date = ""
    private var startDate = ""
    private var endDate = ""
    private var dateOne = ""
    private var dateTwo = ""
    private var flagDate = false
    private var logKey = "usageHistorySummary"
    private val fromClass = "UsageHistorySummaryFragment"
    private lateinit var externalView: View

    override fun getLayoutId(): Int {
        return R.layout.layout_usage_history_summary
    }//getLayoutId ends

    override fun getViewModelClass(): Class<UsageHistoryViewModel> {
        return UsageHistoryViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): UsageHistoryFactory {
        return UsageHistoryInjection.provideUsageHistoryFactory()
    }//getFactory ends

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }//isBindedToActivityLifeCycle ends

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        if (activity != null && !activity!!.isFinishing) {
            externalView = view
            subscribe()
            initUI()
            initUIEvents()
        }
    }//onVisible ends

    private fun subscribe() {
        val usageHistorySummaryResponseLiveData = object : Observer<UsageHistorySummaryResponse> {
            override fun onChanged(response: UsageHistorySummaryResponse?) {
                if (activity != null && !activity!!.isFinishing) {
                    if (response != null) {
                        logE(logKey, "response:::".plus(response.toString()), fromClass, "requestUsageHistorySummary")
                        if (response.data.summaryList != null && !response.data.summaryList!!.isEmpty()) {
                            showContent(response.data.summaryList!!)

                        } else {
                            hideContent()
                        }
                    } else {
                        logE(logKey, "response:::NOOOOOO", fromClass, "requestUsageHistorySummary")
                        hideContent()
                    }
                }
            }
        }

        mViewModel.usageHistorySummaryResponseLiveData.observe(activity!!, usageHistorySummaryResponseLiveData)
    }

    private fun hideContent() {
        externalView.noDataFoundLayout.visibility = View.VISIBLE
        externalView.usageSummaryList.visibility = View.GONE
    }

    private fun showContent(summaryList: List<SummaryListItem?>) {
        val usageHistoryAdapter = UsageHistorySummaryAdapter(activity!!.baseContext, summaryList)
        externalView.usageSummaryList.setAdapter(usageHistoryAdapter)

        externalView.usageSummaryList.visibility = View.VISIBLE
        externalView.noDataFoundLayout.visibility = View.GONE
    }

    private fun initUI() {
        val periodArray = resources.getStringArray(R.array.period_filter)
        externalView.spinnerPeriod.adapter = UsageHistorySpinnerAdapter(context!!, periodArray)

        try {
            //set the default dates for the spinner period date views
            setDefaultDates()

            //get the date
            date = getLastDateMinusDays(getCurrentDate(), ConstantsUtility.DateFormats.CURRENT_DAY)
            updateDates(date, getCurrentDate())
        } catch (exp: Exception) {
            logE(logKey, "error:::".plus(exp.toString()), fromClass, "initUI")
        }
    }//initUI ends

    private fun updateDates(newStartDate: String, newEndDate: String) {
        startDate = newStartDate
        endDate = newEndDate

        var formattedStartedDate = ""
        if (hasValue(changeDateFormat(startDate))) {
            formattedStartedDate = changeDateFormat(startDate)!!
        }

        var formattedEndDate = ""
        if (hasValue(changeDateFormat(endDate))) {
            formattedEndDate = changeDateFormat(endDate)!!
        }

        mViewModel.requestUsageHistorySummary(formattedStartedDate, formattedEndDate)
    }//updateDates ends

    private fun setDefaultDates() {
        externalView.dateOneView.text = changeDateFormatForTextView(getCurrentDate())
        externalView.dateTwoView.text = changeDateFormatForTextView(getCurrentDate())
    }//setDefaultDates ends

    private fun initUIEvents() {
        externalView.spinnerHolder.setOnClickListener {
            externalView.spinnerPeriod.performClick()
        }

        externalView.dateOneView.setOnClickListener {
            showCalendar(externalView.dateOneView, true, externalView)//false for max date
        }//dateOne.setOnClickListener  ends

        externalView.dateTwoView.setOnClickListener {
            showCalendar(externalView.dateTwoView, false, externalView)//true for min date
        }

        externalView.spinnerPeriod.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                //do nothing
            }//onNothingSelected ends

            override fun onItemSelected(parent: AdapterView<*>?, viewItem: View?, position: Int, id: Long) {
                logE(logKey, "itemPos:::".plus(position.toString()), fromClass, "onItemSelected")
                if (position == 0) {
                    //current day
                    try {
                        if (flagDate) {
                            externalView.calendarLayout.visibility = View.GONE
                            updateDates(getCurrentDate(), getCurrentDate())
                        } else {
                            flagDate = true
                        }
                    } catch (exp: Exception) {
                        logE(logKey, "error1:::".plus(exp.toString()), fromClass, "onItemSelected")
                    }
                } else if (position == 1) {
                    //last seven days
                    try {
                        externalView.calendarLayout.visibility = View.GONE
                        date = ""
                        date = getLastDateMinusDays(getCurrentDate(), ConstantsUtility.DateFormats.LAST_SEVEN_DAY)
                        updateDates(date, getCurrentDate())
                    } catch (exp: Exception) {
                        logE(logKey, "error2:::".plus(exp.toString()), fromClass, "onItemSelected")
                    }
                } else if (position == 2) {
                    //last 30 days
                    try {
                        externalView.calendarLayout.visibility = View.GONE
                        date = ""
                        date = getLastDateMinusDays(getCurrentDate(), ConstantsUtility.DateFormats.LAST_THIRTY_DAY)
                        updateDates(date, getCurrentDate())
                    } catch (exp: Exception) {
                        logE(logKey, "error3:::".plus(exp.toString()), fromClass, "onItemSelected")
                    }
                } else if (position == 3) {
                    //previous month
                    try {
                        externalView.calendarLayout.visibility = View.GONE
                        date = ""
                        var maxDate = ""
                        date = getLastMonthFirstDate()
                        maxDate = getLastMonthLastDate()
                        updateDates(date, maxDate)
                    } catch (exp: Exception) {
                        logE(logKey, "error4:::".plus(exp.toString()), fromClass, "onItemSelected")
                    }
                } else if (position == 4) {
                    //custom dates
                    try {
                        setDefaultDates()
                        externalView.dateOneView.setTextColor(resources.getColor(R.color.colorTextGray))
                        externalView.dateTwoView.setTextColor(resources.getColor(R.color.colorTextGray))
                        externalView.calendarLayout.visibility = View.VISIBLE
                    } catch (exp: Exception) {
                        logE(logKey, "error5:::".plus(exp.toString()), fromClass, "onItemSelected")
                    }
                } else {
                    externalView.calendarLayout.visibility = View.GONE
                }
            }//onItemSelected ends
        }
    }//initUIEvents ends

    @Throws(ParseException::class)
    fun showCalendar(textViewNormal: TextView, isStartDateSelected: Boolean, view: View) {

        try {
            dateOne = changeDateFormatForMethods(view.dateOneView.text.toString())
            dateTwo = changeDateFormatForMethods(view.dateTwoView.text.toString())

            val minDate = SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd, Locale.ENGLISH).parse(dateOne)
            val maxDate = SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd, Locale.ENGLISH).parse(dateTwo)

            val azerCalendar: AzerCalendar

            val calendar = Calendar.getInstance()
            if (isStartDateSelected) {
                calendar.time = minDate
            } else {
                calendar.time = maxDate
            }
            val year = calendar.get(Calendar.YEAR)
            val month = calendar.get(Calendar.MONTH)
            val day = calendar.get(Calendar.DAY_OF_MONTH)

            azerCalendar = AzerCalendar(textViewNormal, ondate)


            val datePickerDialog = DatePickerDialog(context!!, azerCalendar, year, month, day)
            //calculate min and max dates (for older versions use System Current TimeMillis

            if (isStartDateSelected) {

                val calndPre = Calendar.getInstance()
                calndPre.add(Calendar.MONTH, -3)
                calndPre.set(Calendar.DATE, 1)
                datePickerDialog.datePicker.minDate = calndPre.timeInMillis

                val dateMax = SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd, Locale.ENGLISH).parse(dateTwo)
                calendar.time = dateMax
                datePickerDialog.datePicker.maxDate = calendar.timeInMillis

            } else {

                val dateMin = SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd, Locale.ENGLISH).parse(dateOne)
                calendar.time = dateMin
                datePickerDialog.datePicker.minDate = calendar.timeInMillis
                datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
            }

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                datePickerDialog.setTitle("")//Prevent Date picker from creating extra Title.!
            }

            datePickerDialog.show()
        } catch (e: ParseException) {
            logE(logKey, "error:::".plus(e.toString()), fromClass, "showCalendar")
        }

    }//showCalendar ends

    private var ondate: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        try {
            dateOne = changeDateFormatForMethods(externalView!!.dateOneView.text.toString())
        } catch (e: ParseException) {
            logE(logKey, "error1:::".plus(e.toString()), fromClass, "ondate")
        }

        try {
            dateTwo = changeDateFormatForMethods(externalView!!.dateTwoView.text.toString())
        } catch (e: ParseException) {
            logE(logKey, "error2:::".plus(e.toString()), fromClass, "ondate")
        }

        updateDates(dateOne, dateTwo)
    }//ondate ends

    override fun onInVisible() {

    }//onInVisible ends

}