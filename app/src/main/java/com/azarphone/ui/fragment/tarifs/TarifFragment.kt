package com.azarphone.ui.fragment.tarifs

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.response.tariffsresponse.TariffResponseListItem
import com.azarphone.api.pojo.response.tariffsresponse.TariffsResponse
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.LayoutTariffMainScreenBinding
import com.azarphone.eventhandler.ActionBarSearchEvents
import com.azarphone.eventhandler.TariffsResponseEvents
import com.azarphone.eventhandler.TariffsSubscriptionsEvents
import com.azarphone.ui.activities.mainactivity.MainFactory
import com.azarphone.ui.activities.mainactivity.MainInjection
import com.azarphone.ui.activities.mainactivity.MainViewModel
import com.azarphone.ui.adapters.pagers.TariffsPagerAdapter
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.layout_tariff_main_screen.view.*
import java.util.*

@Suppress("DEPRECATION")
class TarifFragment : BaseFragment<LayoutTariffMainScreenBinding, MainFactory, MainViewModel>() {

    private var actionIdFromServer = ""
    private var tariffsPagerAdapter: TariffsPagerAdapter? = null
    private var currentSelectedTabIndex = 0
    private var externalView: View? = null
    private val fromClass = "TarifFragment"
    private val logKey = "tariffMainScreen1289"
    private var offeringIdFromDashBoard = ""
    private var tariffsCardsResponse: TariffsResponse? = null
    private val tabNames = ArrayList<String>()
    private var specialTabName = ""

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        initActionBarSearchEvents()
        initTariffsResponseEvents()
    }//onAttach ends

    private fun initTariffsResponseEvents() {
        val tariffsResponseEvents = object : TariffsResponseEvents {
            override fun onTariffsBackgroundResponse(tariffsResponse: TariffsResponse) {
                //populate the tab layout
                tariffsCardsResponse = null
                tariffsCardsResponse = tariffsResponse
                setupTabLayout(tariffsCardsResponse!!.data!!.tariffResponseList!!)
                setTariffsResponseToLocalPreferences(tariffsResponse)
                setupViewPager(tariffsCardsResponse!!.data!!.tariffResponseList!!)
            }//onTariffsBackgroundResponse ends

            override fun onTariffsResponse(tariffsResponse: TariffsResponse) {
                //populate the tab layout
                tariffsCardsResponse = null
                tariffsCardsResponse = tariffsResponse
                setupTabLayout(tariffsCardsResponse!!.data!!.tariffResponseList!!)
                setupViewPager(tariffsCardsResponse!!.data!!.tariffResponseList!!)
                setTariffsResponseToLocalPreferences(tariffsResponse)
            }//onTariffsResponse ends

            override fun onNoTariffsResponseHideData() {
                hideContent()
            }//onNoTariffsResponseHideData ends

            override fun onTariffsResponseShowData() {
                showContent()
            }//onTariffsResponseShowData ends
        }

        RootValues.setTariffsResponseEvents(tariffsResponseEvents)
    }//initTariffsResponseEvents ends

    override fun onResume() {
        super.onResume()
        //set the action bar for this screen
        if (RootValues.getAzerFonActionBarEvents() != null) {
            RootValues.getAzerFonActionBarEvents()!!.onTariffsSetActionBar()
        }
    }//onResume ends

    private fun initActionBarSearchEvents() {
        val actionBarSearchEvents = object : ActionBarSearchEvents {
            override fun onSearchFilterApply(searchFilter: String) {
                /**do the search according to the page title*/
                if (RootValues.getHashMapOfTariffListeners()[getCurrentSelectedTabName(currentSelectedTabIndex)] != null) {
                    logE("saveLisX", "callingTariffListenerOf:::".plus(getCurrentSelectedTabName(currentSelectedTabIndex)), fromClass, "initSearchEvents")
                    RootValues.getHashMapOfTariffListeners()[getCurrentSelectedTabName(currentSelectedTabIndex)]!!.doTariffsSearch(searchFilter, getCurrentSelectedTabName(currentSelectedTabIndex))
                }
            }//onSearchFilterApply ends
        }

        RootValues.setActionBarSearchEvents(actionBarSearchEvents)
    }//initActionBarSearchEvents ends

    override fun getLayoutId(): Int {
        return R.layout.layout_tariff_main_screen
    }//getLayoutId ends

    override fun getViewModelClass(): Class<MainViewModel> {
        return MainViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): MainFactory {
        return MainInjection.provideMainFactory()
    }//getFactory ends

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }//isBindedToActivityLifeCycle ends

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        if (activity != null && !activity!!.isFinishing) {
            RootValues.getHashMapOfTariffListeners().clear()//clear it on each load
            externalView = view
            initTariffsSubscriptionsEvents()
            initBundleData()
            initUI()
            initUIEvents()

            if (getTariffsResponseFromLocalPreferences().data != null &&
                    getTariffsResponseFromLocalPreferences().data!!.tariffResponseList != null &&
                    getTariffsResponseFromLocalPreferences().data!!.tariffResponseList!!.isNotEmpty()) {
                //populate the tab layout
                setupTabLayout(getTariffsResponseFromLocalPreferences().data!!.tariffResponseList!!)
                setupViewPager(getTariffsResponseFromLocalPreferences().data!!.tariffResponseList!!)
                showContent()
                if (hasValue(offeringIdFromDashBoard)) {

                } else if (hasValue(actionIdFromServer)) {

                } else {
                    /**call the tariffs API in the background*/
                    mViewModel.requestGetTariffsSilently()
                }
            } else {
                //call the tariffs API
                mViewModel.requestGetTariffs()
            }
        }

        disposeBundleData()

        logE("OiTx", "offeringIdFromDashBoard:::".plus(offeringIdFromDashBoard), fromClass, "onVisible")
    }//onVisible ends

    private fun initTariffsSubscriptionsEvents() {
        val tariffsSubscriptionsEvents = object : TariffsSubscriptionsEvents {
            override fun onTariffToBeSubscribe(confirmationTitle: String, tariffName: String, offeringId: String, actionType: String) {
                logE("tariffSubSxY", "tariffName:::".plus(tariffName).plus(" offeringId:::").plus(offeringId).plus(" actionType:::").plus(actionType), fromClass, "initTariffsSubscriptionsEvents")
                showTariffSubscribeRenewConfirmationPopup(confirmationTitle, tariffName, offeringId, actionType)
            }//onTariffToBeSubscribe ends
        }

        RootValues.setTariffsSubscriptionsEvents(tariffsSubscriptionsEvents)
    }//initTariffsSubscriptionsEvents ends

    @SuppressLint("InflateParams")
    private fun showTariffSubscribeRenewConfirmationPopup(title: String, tariffName: String, offeringId: String, actionType: String) {
        if (activity!!.isFinishing) return

        val dialogBuilder = AlertDialog.Builder(activity!!)
        val inflater = activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val dialogView = inflater.inflate(R.layout.azer_vas_services_confirmation_popup, null)
        dialogBuilder.setView(dialogView)

        val alertDialog = dialogBuilder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.show()

        val popupTitle = dialogView.findViewById(R.id.popupTitle) as TextView
        popupTitle.typeface = getALSNormalFont()
        popupTitle.text = title
        val popupOkayButton = dialogView.findViewById(R.id.yesButton) as Button
        popupOkayButton.typeface = getALSBoldFont()
        val popupCancelButton = dialogView.findViewById(R.id.noButton) as Button
        popupCancelButton.typeface = getALSBoldFont()

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN) {
            //english
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "No"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ) {
            //azri
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "Xeyr"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.RU) {
            //russian
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "Нет"
        }

        popupOkayButton.setOnClickListener {
            alertDialog.dismiss()
            mViewModel.requestSubscribeToTariff(tariffName, offeringId, actionType)
        }

        popupCancelButton.setOnClickListener {
            alertDialog.dismiss()
        }
    }//showTariffSubscribeRenewConfirmationPopup ends

    private fun initBundleData() {
        if (arguments != null && arguments!!.containsKey(ConstantsUtility.TariffConstants.NOTIFICATION_TO_TARIFF_ACTION_ID)) {
            actionIdFromServer =
                arguments!!.getString(ConstantsUtility.TariffConstants.NOTIFICATION_TO_TARIFF_ACTION_ID)!!
        }

        if (arguments != null && arguments!!.containsKey(ConstantsUtility.TariffConstants.TARIFF_REDIRECTION_OFFER_ID)) {
            offeringIdFromDashBoard =
                arguments!!.getString(ConstantsUtility.TariffConstants.TARIFF_REDIRECTION_OFFER_ID)!!
        }

        if (arguments != null && arguments!!.containsKey(ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA)) {
            actionIdFromServer =
                arguments!!.getString(ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA)!!
        }
    }//initBundleData ends

    private fun disposeBundleData() {
        if (arguments != null && arguments!!.containsKey(ConstantsUtility.TariffConstants.NOTIFICATION_TO_TARIFF_ACTION_ID)) {
            arguments!!.clear()
        }

        if (arguments != null && arguments!!.containsKey(ConstantsUtility.TariffConstants.TARIFF_REDIRECTION_OFFER_ID)) {
            arguments!!.clear()
        }

        if (arguments != null && arguments!!.containsKey(ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA)) {
            arguments!!.clear()
        }
    }//disposeBundleData ends

    private fun removeTabs() {
        if (externalView?.tabLayout != null) {
            externalView?.tabLayout?.removeAllTabs()
        }
    }

    private fun initUI() {

    }

    private fun initUIEvents() {
        externalView?.tabLayout?.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                if (tab != null && tab.customView != null) {
                    val tabTextView = tab.customView as TextView
                    setUnSelectedTabFont(context!!, tabTextView)

                    if (tabTextView.text.toString().equals(specialTabName, ignoreCase = true)) {
                        tabTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_special_button_white, 0)
                        tabTextView.compoundDrawablePadding = context!!.resources.getDimensionPixelSize(R.dimen._10sdp)
                    }
                }
            }//onTabUnselected ends

            override fun onTabSelected(tab: TabLayout.Tab?) {
                externalView?.tariffsViewpager?.setCurrentItem(tab?.position?:0, false)
                currentSelectedTabIndex = tab?.position?:0
                if (tab?.customView != null) {
                    val tabTextView = tab.customView as TextView
                    setSelectedTabFont(context!!, tabTextView)

                    if (tabTextView.text.toString().equals(specialTabName, ignoreCase = true)) {
                        tabTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_special_button_icon, 0)
                        tabTextView.compoundDrawablePadding = context!!.resources.getDimensionPixelSize(R.dimen._10sdp)
                    }
                }
            }//onTabSelected ends
        })//tabLayout.addOnTabSelectedListener ends
    }

    private fun setupViewPager(tariffResponseList: List<TariffResponseListItem?>) {
        logE(logKey, "tariffResponseListSize:::".plus(tariffResponseList.size), fromClass, "onVisible")
        val fragmentsList = ArrayList<androidx.fragment.app.Fragment>()
        fragmentsList.clear()
        var tabToBeSelectedIndex = 0
        //set the tabs by iterating over the list
        for (iterations in tariffResponseList.indices) {
            if (tariffResponseList[iterations]!!.items != null) {
                //add the fragment in the list
                fragmentsList.add(TariffCardFragment.getInstance(tariffResponseList[iterations]!!.items!!, actionIdFromServer, offeringIdFromDashBoard, getCurrentSelectedTabName(iterations)))
            }
        }//for ends

        for (iterations in tariffResponseList.indices) {
            /**check for the redirection to the tab*/
            if (hasValue(offeringIdFromDashBoard)) {
                if (tariffResponseList[iterations]!!.items != null) {
                    for (its in tariffResponseList[iterations]!!.items!!.indices) {
                        if (tariffResponseList[iterations]!!.items!![its] != null &&
                                tariffResponseList[iterations]!!.items!![its]!!.header != null &&
                                hasValue(tariffResponseList[iterations]!!.items!![its]!!.header!!.offeringId)) {

                            if (offeringIdFromDashBoard == tariffResponseList[iterations]!!.items!![its]!!.header!!.offeringId) {
                                tabToBeSelectedIndex = iterations
                                break
                            }
                        }//offering id not null ends
                    }//for ends
                }//if (tariffResponseList[iterations]!!.items != null && tariffResponseList[iterations]!!.items!!.isNotEmpty())  ends
            } else {
                if (hasValue(actionIdFromServer)) {
                    if (tariffResponseList[iterations]!!.items != null) {
                        for (its in tariffResponseList[iterations]!!.items!!.indices) {
                            if (tariffResponseList[iterations]!!.items!![its] != null &&
                                    tariffResponseList[iterations]!!.items!![its]!!.header != null &&
                                    hasValue(tariffResponseList[iterations]!!.items!![its]!!.header!!.offeringId)) {

                                if (actionIdFromServer == tariffResponseList[iterations]!!.items!![its]!!.header!!.offeringId) {
                                    tabToBeSelectedIndex = iterations
                                    break
                                }
                            }//offering id not null ends
                        }//for ends
                    }//if (tariffResponseList[iterations]!!.items != null && tariffResponseList[iterations]!!.items!!.isNotEmpty())  ends
                }
            }//if (hasValue(offeringIdFromDashBoard))  ends
        }
        tariffsPagerAdapter = TariffsPagerAdapter(childFragmentManager, fragmentsList)
        externalView?.tariffsViewpager?.adapter = tariffsPagerAdapter
        logE("tabToBeSelectedIndex", "index:::".plus(tabToBeSelectedIndex), fromClass, "showingtabToBeSelectedIndex")

        if (!fragmentsList.isNullOrEmpty()) {
            externalView?.tariffsViewpager?.setCurrentItem(tabToBeSelectedIndex, false)
        }

//        gotoTabByIndex(tabToBeSelectedIndex)
    }//setupViewPager ends

    private fun gotoTabByIndex(index: Int) {
        if (externalView?.tabLayout != null && externalView?.tabLayout?.tabCount?:0 > 0) {
            Handler().postDelayed({
                externalView?.tabLayout?.getTabAt(index)?.select()
                externalView?.tabLayout?.isSmoothScrollingEnabled = true
                externalView?.tabLayout?.setScrollPosition(index, 0f, true)
            }, 50L)
        }
    }//gotoTabByIndex ends

    private fun getCurrentSelectedTabName(index: Int): String {
        var tabName = ""

        try {
            tabName = tabNames[index]
        } catch (exp: Exception) {

        }

        return tabName
    }//getCurrentSelectedTabName ends

    private fun setupTabLayout(tariffResponseList: List<TariffResponseListItem?>) {
        removeTabs()
        tabNames.clear()//refresh it everytime when this method called
        //set the tabs by iterating over the list
        for (iterations in tariffResponseList.indices) {
            /**save the tab title to the array list*/
            if (tariffResponseList[iterations]!!.groupType != null && hasValue(tariffResponseList[iterations]!!.groupType)) {
                tabNames.add(tariffResponseList[iterations]!!.groupType!!)
            }

            if (tariffResponseList[iterations]!!.isSpecial.equals("true", ignoreCase = true)) {
                /**this is special tab case,
                 * check if to show it or not*/
                if (isShowSpecialTabInTarrifs()) {
                    if (tariffResponseList[iterations]!!.groupType != null && hasValue(tariffResponseList[iterations]!!.groupType)) {
                        externalView?.tabLayout?.addTab(externalView!!.tabLayout.newTab().setText(tariffResponseList[iterations]!!.groupType))
                    }
                }
            } else {
                if (tariffResponseList[iterations]!!.groupType != null && hasValue(tariffResponseList[iterations]!!.groupType)) {
                    externalView?.tabLayout?.addTab(externalView!!.tabLayout.newTab().setText(tariffResponseList[iterations]!!.groupType))
                }
            }
        }//for ends

        /* if (externalView.tabLayout.tabCount > 0) {
             //set the first tab by default
             externalView.tabLayout.getTabAt(0)!!.select()
         }*/
       externalView?.tabLayout?.let {
           for (i in 0 until it.tabCount) {
               val tab = (it.getChildAt(0) as ViewGroup).getChildAt(i)
               val p = tab.layoutParams as ViewGroup.MarginLayoutParams
               val margin = (15 * resources.displayMetrics.density)
               p.setMargins(0, 0, margin.toInt(), 0)
               tab.requestLayout()

               //set the custom view
               val tabView = it.getTabAt(i)
               if (tabView != null) {
                   val tabTextView = TextView(activity!!)
                   tabView.customView = tabTextView
                   //set the text
                   tabTextView.text = tabView.text

                   if (tariffResponseList[i]!!.isSpecial != null && hasValue(tariffResponseList[i]!!.isSpecial)) {
                       if (tariffResponseList[i]!!.isSpecial.equals("true", ignoreCase = true)) {
                           if (i == 0) {
                               tabTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_special_button_icon, 0)
                               tabTextView.compoundDrawablePadding = context!!.resources.getDimensionPixelSize(R.dimen._10sdp)
                           } else {
                               tabTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_special_button_white, 0)
                               tabTextView.compoundDrawablePadding = context!!.resources.getDimensionPixelSize(R.dimen._10sdp)
                           }
                           specialTabName = tabView.text.toString()
                       }
                   }
                   //set the currently selected tab font
                   if (i == 0) {
                       setSelectedTabFont(context!!, tabTextView)
                   } else {
                       setUnSelectedTabFont(context!!, tabTextView)
                   }
               }
           }
       }


        var tabToBeSelectedIndex = 0

        for (iterations in tariffResponseList.indices) {
            /**check for the redirection to the tab*/
            if (hasValue(offeringIdFromDashBoard)) {
                if (tariffResponseList[iterations]!!.items != null) {
                    for (its in tariffResponseList[iterations]!!.items!!.indices) {
                        if (tariffResponseList[iterations]!!.items!![its] != null &&
                                tariffResponseList[iterations]!!.items!![its]!!.header != null &&
                                hasValue(tariffResponseList[iterations]!!.items!![its]!!.header!!.offeringId)) {

                            if (offeringIdFromDashBoard == tariffResponseList[iterations]!!.items!![its]!!.header!!.offeringId) {
                                tabToBeSelectedIndex = iterations
                                break
                            }
                        }//offering id not null ends
                    }//for ends
                }//if (tariffResponseList[iterations]!!.items != null && tariffResponseList[iterations]!!.items!!.isNotEmpty())  ends
            } else {
                if (hasValue(actionIdFromServer)) {
                    if (tariffResponseList[iterations]!!.items != null) {
                        for (its in tariffResponseList[iterations]!!.items!!.indices) {
                            if (tariffResponseList[iterations]!!.items!![its] != null &&
                                    tariffResponseList[iterations]!!.items!![its]!!.header != null &&
                                    hasValue(tariffResponseList[iterations]!!.items!![its]!!.header!!.offeringId)) {

                                if (actionIdFromServer == tariffResponseList[iterations]!!.items!![its]!!.header!!.offeringId) {
                                    tabToBeSelectedIndex = iterations
                                    break
                                }
                            }//offering id not null ends
                        }//for ends
                    }//if (tariffResponseList[iterations]!!.items != null && tariffResponseList[iterations]!!.items!!.isNotEmpty())  ends
                }
            }//if (hasValue(offeringIdFromDashBoard))  ends
        }

        gotoTabByIndex(tabToBeSelectedIndex)

    }//setupTabLayout ends

    private fun hideContent() {
        externalView?.noDataFoundLayout?.visibility = View.VISIBLE
        externalView?.tabLayout?.visibility = View.GONE
        externalView?.tariffsViewpager?.visibility = View.GONE
    }//hideContent ends

    private fun showContent() {
        externalView?.noDataFoundLayout?.visibility = View.GONE
        externalView?.tabLayout?.visibility = View.VISIBLE
        externalView?.tariffsViewpager?.visibility = View.VISIBLE
    }//hideContent ends

    override fun onInVisible() {

    }//onInVisible ends
}//class ends