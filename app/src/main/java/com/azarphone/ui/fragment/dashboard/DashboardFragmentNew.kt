package com.azarphone.ui.fragment.dashboard

import android.app.Activity
import android.os.Bundle
import android.os.Handler
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.HomePageFreeResourcesHelperModel
import com.azarphone.api.pojo.response.gethomepageresponse.GetHomePageResponse
import com.azarphone.api.pojo.response.inappsurvey.InAppSurvey
import com.azarphone.api.pojo.response.inappsurvey.Surveys
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.LayoutDashboardFragmentNewBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.HomePageResponseEvents
import com.azarphone.eventhandler.OnClickSurveySubmit
import com.azarphone.eventhandler.UpdateListOnSuccessfullySurvey
import com.azarphone.ui.activities.exchangeservice.ExchangeServiceActivity
import com.azarphone.ui.activities.loan.LoanActivity
import com.azarphone.ui.activities.mainactivity.MainFactory
import com.azarphone.ui.activities.mainactivity.MainInjection
import com.azarphone.ui.activities.mainactivity.MainViewModel
import com.azarphone.ui.activities.topup.TopupActivity
import com.azarphone.ui.adapters.recyclerviews.DashboadMiddleRecycler
import com.azarphone.ui.dialogs.InAppFeedbackDialog
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.validators.isNumeric
import com.azarphone.widgets.popups.showMessageDialog
import com.es.tec.LocalSharedPrefStorage
import kotlinx.android.synthetic.main.homepage_credit_row.*
import kotlinx.android.synthetic.main.homepage_mrc_row.*
import kotlinx.android.synthetic.main.homepage_postpaid_values_row.*
import kotlinx.android.synthetic.main.layout_dashboard_fragment_new.*
import kotlinx.android.synthetic.main.layout_dashboard_fragment_new.view.*
import java.text.SimpleDateFormat
import java.util.*


/**
 * @author Junaid Hassan on 27, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class DashboardFragmentNew :
    BaseFragment<LayoutDashboardFragmentNewBinding, MainFactory, MainViewModel>() {

    private var callHomePageAPIData = ""
    private var isDataSimPrepaidPostpaid = false
    private var clickCount = 1
    private val fromClass = "DashboardFragmentNew"
    lateinit var mDashboardCenterContentAdapter: DashboadMiddleRecycler

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        initHomePageResponseEvents()
        if (UserDataManager.getRateUsAndroid() == "0") {
            RootValues.getRateUsAndroidOfflineEvents().onNeedToShowRateUsPopUp()
        }
    }

    private fun initHomePageResponseEvents() {
        val homePageResponseEvents = object : HomePageResponseEvents {
            override fun onCancelSwipeRefresh() {
                dashBoardOneSwiper?.isRefreshing = false
            }

            override fun onHomePageResponse(getHomePageResponse: GetHomePageResponse) {
                dashBoardOneSwiper?.isRefreshing = false
                populateDashboard(getHomePageResponse)
            }
        }

        RootValues.setHomePageResponseEvents(homePageResponseEvents)
    }

    override fun getLayoutId(): Int {
        return R.layout.layout_dashboard_fragment_new
    }

    override fun getViewModelClass(): Class<MainViewModel> {
        return MainViewModel::class.java
    }

    override fun getFactory(): MainFactory {
        return MainInjection.provideMainFactory()
    }

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }

    override fun onResume() {
        super.onResume()
        initBundleData()
        if (RootValues.getAzerFonActionBarEvents() != null) {
            RootValues.getAzerFonActionBarEvents()!!.onDashboardSetActionBar()
        }
    }//onResume ends

    private fun initBundleData() {
        if (arguments != null && arguments!!.containsKey(ConstantsUtility.UserConstants.CALL_HOME_PAGE_API_KEY)) {
            callHomePageAPIData =
                arguments!!.getString(ConstantsUtility.UserConstants.CALL_HOME_PAGE_API_KEY)!!
            logE(
                "callHomePageAPIData",
                "callHomePageAPIData1289:::".plus(callHomePageAPIData),
                fromClass,
                "initBundleData"
            )
            callGetHomePageAPI()
        }
    }//initBundleData ends

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        middleContainer.visibility = View.GONE

//        callGetHomePageAPI()

        balanceTV.setOnClickListener {
            TopupActivity.start(context!!)
        }//balanceTV.setOnClickListener ends

        planeRoamingIcon.setImageResource(getAirPlaneRoamingIcon(false))

        //swipe refresh layout event here
        view.dashBoardOneSwiper.setOnRefreshListener {
            if (!isNetworkAvailable(activity!!)) {
                showMessageDialog(
                    context!!, activity!!,
                    activity!!.resources.getString(R.string.popup_error_title),
                    activity!!.resources.getString(R.string.message_no_internet)
                )
                dashBoardOneSwiper.isRefreshing = false
            } else {
                dashBoardOneSwiper.isRefreshing = true
                planeRoamingIcon.setImageResource(getAirPlaneRoamingIcon(false))
                clickCount = 1
                mViewModel.requestGetHomePage(true)
            }

        }//view.dashBoardOneSwiper.setOnRefreshListener ends

        view.planeRoamingRow.setOnClickListener {
            //            reveal(revealLayout, revealSource)
            clickCount += 1
            if (clickCount % 2 == 0) {
                planeRoamingIcon.setImageResource(getAirPlaneRoamingIcon(true))
                populateMiddleRecycler(
                    getHomePageResponseFromLocalPreferences("adhjkj3454566"),
                    true
                )
            } else {
                planeRoamingIcon.setImageResource(getAirPlaneRoamingIcon(false))
                populateMiddleRecycler(getHomePageResponseFromLocalPreferences("nvnvnv2222"), false)
            }
        }

        /*dashBoardNoData strings*/
        when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> dashBoardNoData.text =
                "No data available."
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> dashBoardNoData.text =
                "Məlumat yoxdur."
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.RU -> dashBoardNoData.text =
                "Данные отсутствуют."
        }

        //ui events
        packageNameTV.setOnTouchListener(OnTouchListener { v, event ->
            val DRAWABLE_LEFT = 0
            val DRAWABLE_TOP = 1
            val DRAWABLE_RIGHT = 2
            val DRAWABLE_BOTTOM = 3

            if (event.action == MotionEvent.ACTION_DOWN) {
                if (event.rawX >= packageNameTV.right - packageNameTV.compoundDrawables[DRAWABLE_RIGHT].bounds.width()) {
                    // your action here
                    logE("ClickX", "clicked", fromClass, "offeringNameClicked")
                    //set the tariff offering id

                    var offeringNameDisplayId = ""
                    if (UserDataManager.getCustomerData() != null && hasValue(UserDataManager.getCustomerData()!!.offeringId)) {
                        offeringNameDisplayId = UserDataManager.getCustomerData()!!.offeringId!!
                    }

                    RootValues.getTariffRedirectionFromDashboardEvents()
                        .onRedirectToTariffsScreen(offeringNameDisplayId)
                    return@OnTouchListener true
                }
            }
            return@OnTouchListener false
        })

        dashboardChangerIcon.setOnClickListener {
            ExchangeServiceActivity.start(context!!)
        }

        creditPlusIcon.setOnClickListener {
            LoanActivity.start(context!!)
        }
        inAppFeedback()
    }//onVisible ends

    private fun inAppFeedback() {

        var inAppFeedbackDialog: InAppFeedbackDialog? = null
        var currentVisit: Int =
            LocalSharedPrefStorage(requireContext()).getInt(LocalSharedPrefStorage.PREF_HOME_PAGE)
        currentVisit += 1
        val inAppSurvey: InAppSurvey? = UserDataManager.getInAppSurvey()
        if ((inAppSurvey?.data) != null) {
            val surveys: Surveys? =
                inAppSurvey.data.findSurvey(ConstantsUtility.InAppSurveyConstants.HOME_PAGE)
            if (surveys != null) {
                if (surveys.surveyLimit > surveys.surveyCount) {
                    if (surveys.visitLimit <= currentVisit && surveys.visitLimit > -1) {
                        inAppFeedbackDialog =
                            InAppFeedbackDialog(requireContext(), object : OnClickSurveySubmit {
                                override fun onClickSubmit(
                                    uploadSurvey: UploadSurvey,
                                    onTransactionComplete: String,
                                    updateListOnSuccessfullySurvey: UpdateListOnSuccessfullySurvey?
                                ) {
                                    mViewModel.uploadInAppSurvey(
                                        requireContext(),
                                        uploadSurvey,
                                        onTransactionComplete
                                    )
                                    if (inAppFeedbackDialog?.isShowing == true) {
                                        inAppFeedbackDialog?.dismiss()
                                    }

                                }
                            })
                        if (surveys.questions != null) {
                            currentVisit = 0
                            inAppFeedbackDialog.showNormalDialog(surveys)
                        }
                    }
                }
            }
        }
        LocalSharedPrefStorage(requireContext()).addInt(
            LocalSharedPrefStorage.PREF_HOME_PAGE,
            currentVisit
        )
    }

    private fun getAirPlaneRoamingIcon(isPink: Boolean): Int {
        var icon = R.drawable.ic_empty_icon
        if (isPink) {
            /**return the pink icons based on locale*/
            if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN) {
                icon = R.drawable.ic_roaming_pink_en
            } else if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ) {
                icon = R.drawable.ic_roaming_pink_az
            } else {
                icon = R.drawable.ic_roaming_pink_ru
            }
        } else {
            /**return the gray icons based on locale*/
            if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN) {
                icon = R.drawable.ic_roaming_gray_en
            } else if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ) {
                icon = R.drawable.ic_roaming_gray_az
            } else {
                icon = R.drawable.ic_roaming_gray_ru
            }
        }

        //return the required icon
        return icon
    }//getAirPlaneRoamingIcon ends

    private fun callGetHomePageAPI() {
        //get the homepage response from the cache
        val homePageCachedResponse = getHomePageResponseFromLocalPreferences("cvaaaa2333")
        logE(
            "homePageCachedResponse",
            "cache:::".plus(homePageCachedResponse.data),
            fromClass,
            "callGetHomePageAPI"
        )
        if (homePageCachedResponse.data != null && hasValue(homePageCachedResponse.data.toString())) {
            logE("homePageCachedResponse", "cache hai", fromClass, "callGetHomePageAPI")
            populateDashboard(homePageCachedResponse)

            if (!hasValue(callHomePageAPIData)) {
                logE(
                    "callHomePageAPIData",
                    "not has value:::".plus(callHomePageAPIData),
                    fromClass,
                    "initBundleData"
                )
                mViewModel.requestGetHomePageSilently(false)
            } else {
                logE(
                    "callHomePageAPIData",
                    "has value:::".plus(callHomePageAPIData),
                    fromClass,
                    "initBundleData"
                )
            }

        } else {
            logE("homePageCachedResponse", "cache nai hai", fromClass, "callGetHomePageAPI")
            if (!UserDataManager.isGetHomePageCalledWithSuccess()) {
                logE(
                    "homePageCachedResponse",
                    "not called with success",
                    fromClass,
                    "callGetHomePageAPI"
                )
                mViewModel.requestGetHomePage(false)
            }
        }

    }//callGetHomePageAPI ends

    private fun refreshAllViews() {
        logE("refreshX", "called", fromClass, "refreshAllViews")
        balanceTitleTv?.text = ""

        balanceTV?.text = getManatConcatedString(
            context!!, "0.00",
            ConstantsUtility.PackagesConstants.AZERI_SYMBOL,
            0.08f, R.dimen._26ssp
        )
        bonusBalanceTV?.text = ""
//        bonusBalanceTVAzeriSymbol.visibility = View.GONE
        creditLimitTitleTv?.text = ""
        creditLimitTV?.text = getManatConcatedString(
            context!!, "0.00",
            ConstantsUtility.PackagesConstants.AZERI_SYMBOL,
            0.25f, R.dimen._10ssp
        )
        totalPaymentTitleTV?.text = ""
        totalPaymentTV?.text = getManatConcatedString(
            context!!, "0.00",
            ConstantsUtility.PackagesConstants.AZERI_SYMBOL,
            0.25f, R.dimen._10ssp
        )
        outstandingTV?.text = getManatConcatedString(
            context!!, "0.00",
            ConstantsUtility.PackagesConstants.AZERI_SYMBOL,
            0.25f, R.dimen._10ssp
        )
        outstandingTitleTV?.text = ""
        packageNameTV?.text = ""
        activeStatusTV?.text = ""
        differenceOfDaysTv?.text = ""
        centerContentRecycler?.visibility = View.GONE

        postpaidValuesRowsContainer?.visibility = View.GONE
        creditRowContainer?.visibility = View.GONE
        mrcRowContainer?.visibility = View.GONE
        centerContentRecycler?.visibility = View.GONE

        dashBoardNoData?.visibility = View.VISIBLE
        differenceOfDaysTv?.visibility = View.GONE
        packageNameTV?.visibility = View.GONE
    }//refreshAllViews ends

    private fun populateDashboard(response: GetHomePageResponse) {

        if (activity != null && !activity!!.isFinishing) {

        } else {
            return //safe passage
        }

        if (packageNameTV == null) {
            return //safe passage
        }
        logE("DashboardData1289", response.toString(), fromClass, "populateDashboard")
        refreshAllViews()
        bonusBalanceTV.visibility = View.VISIBLE
//        bonusBalanceTVAzeriSymbol.visibility = View.VISIBLE
        balanceTitleTv.visibility = View.VISIBLE
        balanceTV.visibility = View.VISIBLE
//        currencyIV.visibility = View.VISIBLE
        dashboardChangerIcon.visibility = View.GONE
        postpaidValuesRowsContainer.visibility = View.VISIBLE
        creditRowContainer.visibility = View.VISIBLE
        mrcRowContainer.visibility = View.VISIBLE
        centerContentRecycler.visibility = View.VISIBLE

        dashBoardNoData.visibility = View.GONE

        //textview marquee
        packageNameTV.isSelected = true
        activeStatusTV.isSelected = true
        creditLimitTV.isSelected = true
        totalPaymentTV.isSelected = true
        outstandingTV.isSelected = true
        bonusBalanceTV.isSelected = true
        balanceTV.isSelected = true

        if (UserDataManager.getCustomerData() == null) {
            return// safe passage
        }

        var subscriberType = ""
        if (hasValue(UserDataManager.getCustomerData()!!.subscriberType)) {
            subscriberType = UserDataManager.getCustomerData()!!.subscriberType!!
        } else {
            showMessageDialog(
                context!!, activity!!,
                activity!!.resources.getString(R.string.popup_error_title),
                activity!!.resources.getString(R.string.server_stopped_responding_please_try_again)
            )
            return//safe passage
        }

        //check the customer type
        if (subscriberType.equals(
                ConstantsUtility.UserConstants.USER_SUBSCRIBER_TYPE_PREPAID,
                ignoreCase = true
            )
        ) {
            /**check the customer type*/
            var customerType = ""
            if (hasValue(UserDataManager.getCustomerData()!!.customerType)) {
                customerType = UserDataManager.getCustomerData()!!.customerType!!
            } else {
                showMessageDialog(
                    context!!, activity!!,
                    activity!!.resources.getString(R.string.popup_error_title),
                    activity!!.resources.getString(R.string.server_stopped_responding_please_try_again)
                )
                refreshAllViews()
                return//safe passage
            }

//old full condition for Cevir
//            if (customerType.equals(ConstantsUtility.UserConstants.CUSTOMER_TYPE_FULL, ignoreCase = true)) {
//                populatePrepaidUserDashBoard(response, true)
//                isDataSimPrepaidPostpaid = false
//            } else

            if (customerType.equals(
                    ConstantsUtility.UserConstants.CUSTOMER_TYPE_DATA_SIM_PREPAID,
                    ignoreCase = true
                ) ||
                customerType.equals(
                    ConstantsUtility.UserConstants.CUSTOMER_TYPE_DATA_SIM_POSTPAID,
                    ignoreCase = true
                )
            ) {
                isDataSimPrepaidPostpaid = true
                populatePrepaidUserDashBoard(response, false)
            } else {
                isDataSimPrepaidPostpaid = false
                populatePrepaidUserDashBoard(response, true)
            }

        } else {
            /**check the customer type*/
            var customerType = ""
            if (hasValue(UserDataManager.getCustomerData()!!.customerType)) {
                customerType = UserDataManager.getCustomerData()!!.customerType!!
            } else {
                showMessageDialog(
                    context!!, activity!!,
                    activity!!.resources.getString(R.string.popup_error_title),
                    activity!!.resources.getString(R.string.server_stopped_responding_please_try_again)
                )
                refreshAllViews()
                return//safe passage
            }

            logE("custType", "type:::".plus(customerType), fromClass, "populateDashboard")
            isDataSimPrepaidPostpaid = customerType.equals(
                ConstantsUtility.UserConstants.CUSTOMER_TYPE_DATA_SIM_POSTPAID_CORPORATE,
                ignoreCase = true
            ) ||
                    customerType.equals(
                        ConstantsUtility.UserConstants.CUSTOMER_TYPE_DATA_SIM_POSTPAID_INDIVIDUAL,
                        ignoreCase = true
                    )

            populatePostpaidUserDashBoard(response)
        }
    }//populateDashboard ends

    private fun populatePostpaidUserDashBoard(postpaidResponse: GetHomePageResponse) {

        if (postpaidResponse.data?.balance?.postpaid?.balanceIndividualValue?.toDouble()!!
            < postpaidResponse.data.balance.prepaid?.mainWallet?.lowerLimit?.toDouble()!!
        ) {
            balanceTV.setTextColor(ContextCompat.getColor(context!!, R.color.dark_pink))
        } else {
            balanceTV.setTextColor(ContextCompat.getColor(context!!, R.color.purplish_brown))
        }

        var balanceLabel = ""
        var balanceIndividualValue = ""
        dashboardChangerIcon.visibility = View.GONE

        /**hide the credit row with 3 values*/
        if (isDataSimPrepaidPostpaid) {
            creditRowContainer.visibility = View.GONE
        } else {
            creditRowContainer.visibility = View.VISIBLE
        }

        var offeringNameDisplay = ""
        if (hasValue(postpaidResponse.data.offeringNameDisplay)) {
            offeringNameDisplay = postpaidResponse.data.offeringNameDisplay!!
            logE("offerXDis", "name:::".plus(offeringNameDisplay), fromClass, "offeringNameDisplay")
        }

        packageNameTV.text = offeringNameDisplay
        packageNameTV.visibility = View.VISIBLE

        var userStatus = ""
        if (hasValue(postpaidResponse.data.status)) {
            userStatus = postpaidResponse.data.status!!
        }

        activeStatusTV.text = userStatus

        if (postpaidResponse.data.balance != null) {
            /**set the postpaid
            values here*/
            if (postpaidResponse.data.balance.postpaid != null) {

                //balanceTypeName
                if (hasValue(postpaidResponse.data.balance.postpaid.balanceLabel)) {
                    balanceLabel = postpaidResponse.data.balance.postpaid.balanceLabel!!
                }

                //prepaidBalanceAmount
                if (hasValue(postpaidResponse.data.balance.postpaid.balanceIndividualValue)) {
                    balanceIndividualValue =
                        postpaidResponse.data.balance.postpaid.balanceIndividualValue
                }

                //set the values to the views
                logE(
                    "balanceTypeName",
                    "name:::".plus(balanceLabel),
                    fromClass,
                    "populatePostpaidUserDashBoard"
                )
                balanceTitleTv.text = balanceLabel
                balanceTV.text = getManatConcatedString(
                    context!!, balanceIndividualValue,
                    ConstantsUtility.PackagesConstants.AZERI_SYMBOL,
                    0.08f, R.dimen._26ssp
                )

                //the mainWalletAmount has not value so hide the manat symbol
                /* if (hasValue(balanceIndividualValue)) {
                     currencyIV.visibility = View.VISIBLE
                 } else {
                     currencyIV.visibility = View.GONE
                 }*/

                //hide the bonus row
                bonusBalanceTV.visibility = View.GONE
//                bonusBalanceTVAzeriSymbol.visibility = View.GONE

                //show the postpaidValuesRows in case of prepaid customer
                postpaidValuesRowsContainer.visibility = View.VISIBLE

                populatePostpaidValuesRow(postpaidResponse)
            }// if (postpaidResponse.data!!.balance!!.postpaid != null) ends
        }//if (postpaidResponse.data!!.balance != null) ends

        middleContainer.visibility = View.VISIBLE
        //populate the free resources/middle row-> recycler view
        if (postpaidResponse.data.freeResources != null) {
            if (postpaidResponse.data.freeResources.freeResources != null) {
                if (postpaidResponse.data.freeResources.freeResources.isNotEmpty()) {
                    populateMiddleRecycler(postpaidResponse, false)
                    revealSource.visibility = View.VISIBLE
                    middleContainer.setBackgroundDrawable(context!!.resources.getDrawable(R.drawable.reveal_bg))
                    revealLayout.visibility = View.VISIBLE
                    dashBoardNoData.visibility = View.GONE
                } else {
                    middleContainer.setBackgroundDrawable(context!!.resources.getDrawable(R.drawable.unreveal_bg))
                    dashBoardNoData.visibility = View.VISIBLE
                    revealSource.visibility = View.GONE
                    revealLayout.visibility = View.GONE
                }
            } else {
                middleContainer.setBackgroundDrawable(context!!.resources.getDrawable(R.drawable.unreveal_bg))
                dashBoardNoData.visibility = View.VISIBLE
                revealSource.visibility = View.GONE
                revealLayout.visibility = View.GONE
            }
        } else {
            middleContainer.setBackgroundDrawable(context!!.resources.getDrawable(R.drawable.unreveal_bg))
            dashBoardNoData.visibility = View.VISIBLE
            revealSource.visibility = View.GONE
            revealLayout.visibility = View.GONE
        }
        //juni12891226 case of no free resources

        /**calculate the credit
         * and populate the credit row here*/
        /*if (postpaidResponse.data.credit != null) {
            var creditTitleLabel = ""
            if (hasValue(postpaidResponse.data.credit.creditTitleLabel)) {
                creditTitleLabel = postpaidResponse.data.credit.creditTitleLabel!!
            }
            var creditTitleValue = ""
            if (hasValue(postpaidResponse.data.credit.creditTitleValue)) {
                creditTitleValue = postpaidResponse.data.credit.creditTitleValue!!
            }
            var creditDateLabel = ""
            if (hasValue(postpaidResponse.data.credit.creditDateLabel)) {
                creditDateLabel = postpaidResponse.data.credit.creditDateLabel!!
            }
            var startDate = ""
            if (hasValue(postpaidResponse.data.credit.creditInitialDate)) {
                startDate = postpaidResponse.data.credit.creditInitialDate!!
            }
            var endDate = ""
            if (hasValue(postpaidResponse.data.credit.creditDate)) {
                endDate = postpaidResponse.data.credit.creditDate!!
            }
            var creditDate = ""
            if (hasValue(endDate)) {
                creditDate = getCreditMRCFormattedDate(endDate)
            }

            populateCreditRow(creditTitleLabel, creditTitleValue, creditDateLabel, startDate, endDate, creditDate)
        } else {
            creditRowContainer.visibility = View.GONE
        }//if(postpaidResponse.data!!.credit!=null) ends*/
        /**don't show the credit container*/
        creditRowContainer.visibility = View.GONE
        /**calculate the mrc here
         * for the prepaid user*/
        if (postpaidResponse.data.mrc != null) {
            var mrcTitleLabel = ""
            if (hasValue(postpaidResponse.data.mrc.mrcTitleLabel)) {
                mrcTitleLabel = postpaidResponse.data.mrc.mrcTitleLabel!!
            } else {
                mrcRowContainer.visibility = View.GONE
            }

            var mrcTitleValue = ""
            if (hasValue(postpaidResponse.data.mrc.mrcTitleValue)) {
                mrcTitleValue = postpaidResponse.data.mrc.mrcTitleValue!!
            } else {
                mrcRowContainer.visibility = View.GONE
            }

            var mrcDateLabel = ""
            if (hasValue(postpaidResponse.data.mrc.mrcDateLabel)) {
                mrcDateLabel = postpaidResponse.data.mrc.mrcDateLabel!!
            }

            var startDate = ""
            if (hasValue(postpaidResponse.data.mrc.mrcInitialDate)) {
                startDate = postpaidResponse.data.mrc.mrcInitialDate!!
            }

            var endDate = ""
            if (hasValue(postpaidResponse.data.mrc.mrcDate)) {
                endDate = postpaidResponse.data.mrc.mrcDate!!
            }

            var mrcDate = ""
            if (hasValue(endDate)) {
                mrcDate = getCreditMRCFormattedDate(endDate)
            }

            var mrcType = ""
            if (hasValue(postpaidResponse.data.mrc.mrcType)) {
                mrcType = postpaidResponse.data.mrc.mrcType!!
            }

            if (mrcType.equals(ConstantsUtility.MRCTypes.MRC_DAILY, ignoreCase = true)) {
                populateMRCDailyRow(
                    mrcTitleLabel,
                    mrcTitleValue,
                    mrcDateLabel,
                    startDate,
                    endDate,
                    mrcDate
                )
            } else {
                populateMRCMonthlyRow(
                    mrcTitleLabel,
                    mrcTitleValue,
                    mrcDateLabel,
                    startDate,
                    endDate,
                    mrcDate
                )
            }

            //juni12891226 daily mrc
        } else {
            mrcRowContainer.visibility = View.GONE
        }//if (postpaidResponse.data.mrc != null)  ends
    }//populatePostpaidUserDashBoard ends

    private fun populatePostpaidValuesRow(postpaidResponse: GetHomePageResponse) {
        var currentCreditLabel = ""
        if (hasValue(postpaidResponse.data!!.balance!!.postpaid!!.currentCreditLabel)) {
            currentCreditLabel = postpaidResponse.data.balance!!.postpaid!!.currentCreditLabel!!
        }

        var currentCreditLimit = ""
        if (hasValue(postpaidResponse.data.balance!!.postpaid!!.currentCreditLimit)) {
            currentCreditLimit = postpaidResponse.data.balance.postpaid!!.currentCreditLimit!!
//            currentCreditLimitAzeri.visibility = View.VISIBLE
        } else {
//            currentCreditLimitAzeri.visibility = View.GONE
        }

        var totalPaymentsLabel = ""
        if (hasValue(postpaidResponse.data.balance.postpaid!!.totalPaymentsLabel)) {
            totalPaymentsLabel = postpaidResponse.data.balance.postpaid.totalPaymentsLabel!!
        }

        var totalPayments = ""
        if (hasValue(postpaidResponse.data.balance.postpaid.totalPayments)) {
            totalPayments = postpaidResponse.data.balance.postpaid.totalPayments!!
//            totalPaymentsAzeri.visibility = View.VISIBLE
        } else {
//            totalPaymentsAzeri.visibility = View.GONE
        }

        var outstandingIndividualDebtLabel = ""
        if (hasValue(postpaidResponse.data.balance.postpaid.outstandingIndividualDebtLabel)) {
            outstandingIndividualDebtLabel =
                postpaidResponse.data.balance.postpaid.outstandingIndividualDebtLabel!!
        }

        var outstandingIndividualDebt = ""
        if (hasValue(postpaidResponse.data.balance.postpaid.outstandingIndividualDebt)) {
            outstandingIndividualDebt =
                postpaidResponse.data.balance.postpaid.outstandingIndividualDebt!!
//            outstandingIndividualDebtAzeri.visibility = View.VISIBLE
        } else {
//            outstandingIndividualDebtAzeri.visibility = View.GONE
        }

        //set the values
        creditLimitTitleTv.text = currentCreditLabel
        creditLimitTitleTv.isSelected = true
        creditLimitTV.text = getManatConcatedString(
            context!!, currentCreditLimit,
            ConstantsUtility.PackagesConstants.AZERI_SYMBOL,
            0.25f, R.dimen._10ssp
        )
        logE(
            "totalPaymentTitleTV",
            "title:::".plus(totalPaymentsLabel),
            fromClass,
            "populatePostpaidValuesRow"
        )
        totalPaymentTitleTV.text = totalPaymentsLabel
        totalPaymentTitleTV.isSelected = true
        totalPaymentTV.text = getManatConcatedString(
            context!!, totalPayments,
            ConstantsUtility.PackagesConstants.AZERI_SYMBOL,
            0.25f, R.dimen._10ssp
        )
        outstandingTitleTV.text = outstandingIndividualDebtLabel
        outstandingTitleTV.isSelected = true
        outstandingTV.text = getManatConcatedString(
            context!!, outstandingIndividualDebt,
            ConstantsUtility.PackagesConstants.AZERI_SYMBOL,
            0.25f, R.dimen._10ssp
        )

    }//populatePostpaidValuesRow ends

    private fun populatePrepaidUserDashBoard(
        prepaidResponse: GetHomePageResponse,
        isFullPrepaid: Boolean
    ) {

        if (packageNameTV == null)
            return //safe passage solved crash


        if (prepaidResponse.data?.balance?.prepaid?.mainWallet?.amount?.toDouble() ?: 0.0 < prepaidResponse.data?.balance?.prepaid?.mainWallet?.lowerLimit?.toDouble() ?: 0.0) {
            balanceTV.setTextColor(ContextCompat.getColor(context!!, R.color.dark_pink))
        } else {
            balanceTV.setTextColor(ContextCompat.getColor(context!!, R.color.purplish_brown))
        }

        //hide the postpaidValuesRows in case of prepaid customer
        postpaidValuesRowsContainer.visibility = View.GONE
        creditRowContainer.visibility = View.VISIBLE
        //check if full prepaid
        /**show or hide the dashboard changer icon*/
        if (isFullPrepaid) {
            dashboardChangerIcon.visibility = View.VISIBLE
        } else {
            dashboardChangerIcon.visibility = View.GONE
        }

        var offeringNameDisplay = ""
        if (hasValue(prepaidResponse.data?.offeringNameDisplay)) {
            offeringNameDisplay = prepaidResponse.data?.offeringNameDisplay!!
            logE(
                "offerXDis",
                "name222:::".plus(offeringNameDisplay),
                fromClass,
                "offeringNameDisplay"
            )
        }

        packageNameTV.text = offeringNameDisplay
        packageNameTV.visibility = View.VISIBLE

        var userStatus = ""
        if (hasValue(prepaidResponse.data?.status)) {
            userStatus = prepaidResponse.data?.status!!
        }

        activeStatusTV.text = userStatus

        var mainWalletBalanceTypeName = ""
        var mainWalletAmount = ""
        var bounusWalletBalanceTypeName = ""
        var bounusWalletBalanceAmount = ""
        if (prepaidResponse.data?.balance != null) {

            if (prepaidResponse.data.balance.prepaid != null) {

                /**set the mainWallet values here*/
                if (prepaidResponse.data.balance.prepaid.mainWallet != null) {

                    //balanceTypeName
                    if (hasValue(prepaidResponse.data.balance.prepaid.mainWallet.balanceTypeName)) {
                        mainWalletBalanceTypeName =
                            prepaidResponse.data.balance.prepaid.mainWallet.balanceTypeName!!
                    }

                    //prepaidBalanceAmount
                    if (hasValue(prepaidResponse.data.balance.prepaid.mainWallet.amount)) {
                        mainWalletAmount = prepaidResponse.data.balance.prepaid.mainWallet.amount!!
                    }
                }//if (prepaidResponse.data!!.balance!!.prepaid!!.mainWallet != null) ends

                /**set the bounusWallet
                values here*/
                if (prepaidResponse.data.balance.prepaid.bounusWallet != null) {
                    if (hasValue(prepaidResponse.data.balance.prepaid.bounusWallet.balanceTypeName)) {
                        bounusWalletBalanceTypeName =
                            prepaidResponse.data.balance.prepaid.bounusWallet.balanceTypeName!!
                    }

                    if (hasValue(prepaidResponse.data.balance.prepaid.bounusWallet.amount)) {
                        bounusWalletBalanceAmount =
                            prepaidResponse.data.balance.prepaid.bounusWallet.amount!!
                    }
                }//if (prepaidResponse.data!!.balance!!.prepaid!!.bounusWallet != null) ends

            }//if(prepaidResponse.data!!.balance!!.prepaid!=null) ends

        }//if (prepaidResponse.data!!.balance != null) ends

        //set the values to the views
        logE(
            "balanceTypeName",
            "name:::".plus(mainWalletBalanceTypeName),
            fromClass,
            "populatePrepaidUserDashBoard"
        )
        balanceTitleTv.text = mainWalletBalanceTypeName
        balanceTV.text = getManatConcatedString(
            context!!, mainWalletAmount,
            ConstantsUtility.PackagesConstants.AZERI_SYMBOL,
            0.08f, R.dimen._26ssp
        )

        if (isNumeric(bounusWalletBalanceAmount) && bounusWalletBalanceAmount.toDouble() > 0) {
            bonusBalanceTV.text = getManatConcatedString(
                context!!, bounusWalletBalanceTypeName.plus(" ").plus(bounusWalletBalanceAmount),
                ConstantsUtility.PackagesConstants.AZERI_SYMBOL,
                0.25f, R.dimen._10ssp
            )
            bonusBalanceTV.visibility = View.VISIBLE
            //the bounusWalletBalanceAmount has not value so hide the manat symbol
            if (hasValue(bounusWalletBalanceAmount)) {
//                bonusBalanceTVAzeriSymbol.visibility = View.VISIBLE
            } else {
//                bonusBalanceTVAzeriSymbol.visibility = View.GONE
            }
        } else {
            bonusBalanceTV.visibility = View.GONE
//            bonusBalanceTVAzeriSymbol.visibility = View.GONE
        }

        middleContainer.visibility = View.VISIBLE
        //populate the free resources/middle row-> recycler view
        if (prepaidResponse.data?.freeResources != null) {
            if (prepaidResponse.data.freeResources.freeResources != null) {
                if (prepaidResponse.data.freeResources.freeResources.isNotEmpty()) {
                    populateMiddleRecycler(prepaidResponse, false)
                    revealSource.visibility = View.VISIBLE
                    middleContainer.setBackgroundDrawable(context!!.resources.getDrawable(R.drawable.reveal_bg))
                    revealLayout.visibility = View.VISIBLE
                    dashBoardNoData.visibility = View.GONE
                } else {
                    middleContainer.setBackgroundDrawable(context!!.resources.getDrawable(R.drawable.unreveal_bg))
                    dashBoardNoData.visibility = View.VISIBLE
                    revealSource.visibility = View.GONE
                    revealLayout.visibility = View.GONE
                }
            } else {
                middleContainer.setBackgroundDrawable(context!!.resources.getDrawable(R.drawable.unreveal_bg))
                dashBoardNoData.visibility = View.VISIBLE
                revealSource.visibility = View.GONE
                revealLayout.visibility = View.GONE
            }
        } else {
            middleContainer.setBackgroundDrawable(context!!.resources.getDrawable(R.drawable.unreveal_bg))
            dashBoardNoData.visibility = View.VISIBLE
            revealSource.visibility = View.GONE
            revealLayout.visibility = View.GONE
        }
        //juni12891226 case of no free resources

        /**calculate the mrc here
         * for the prepaid user*/
        if (prepaidResponse.data?.mrc != null) {
            var mrcTitleLabel = ""
            if (hasValue(prepaidResponse.data.mrc.mrcTitleLabel)) {
                mrcTitleLabel = prepaidResponse.data.mrc.mrcTitleLabel!!
            } else {
                mrcRowContainer.visibility = View.GONE
            }

            var mrcTitleValue = ""
            if (hasValue(prepaidResponse.data.mrc.mrcTitleValue)) {
                mrcTitleValue = prepaidResponse.data.mrc.mrcTitleValue!!
            } else {
                mrcRowContainer.visibility = View.GONE
            }

            var mrcDateLabel = ""
            if (hasValue(prepaidResponse.data.mrc.mrcDateLabel)) {
                mrcDateLabel = prepaidResponse.data.mrc.mrcDateLabel!!
            }

            var startDate = ""
            if (hasValue(prepaidResponse.data.mrc.mrcInitialDate)) {
                startDate = prepaidResponse.data.mrc.mrcInitialDate!!
            }

            var endDate = ""
            if (hasValue(prepaidResponse.data.mrc.mrcDate)) {
                endDate = prepaidResponse.data.mrc.mrcDate!!
            }

            var mrcDate = ""
            if (hasValue(endDate)) {
                mrcDate = getCreditMRCFormattedDate(endDate)
            }

            var mrcType = ""
            if (hasValue(prepaidResponse.data.mrc.mrcType)) {
                mrcType = prepaidResponse.data.mrc.mrcType!!
            }

            if (mrcType.equals(ConstantsUtility.MRCTypes.MRC_DAILY, ignoreCase = true)) {
                populateMRCDailyRow(
                    mrcTitleLabel,
                    mrcTitleValue,
                    mrcDateLabel,
                    startDate,
                    endDate,
                    mrcDate
                )
            } else {
                populateMRCMonthlyRow(
                    mrcTitleLabel,
                    mrcTitleValue,
                    mrcDateLabel,
                    startDate,
                    endDate,
                    mrcDate
                )
            }

            //juni12891226 daily mrc
            logE("mrcPrepaid", "notnull", fromClass, "prepaid dashboard")
        } else {
            logE("mrcPrepaid", "nulll", fromClass, "prepaid dashboard")
            mrcRowContainer.visibility = View.GONE
        }//if (prepaidResponse.data!!.mrc != null) ends
        /**calculate the credit
         * and populate the credit row here*/
        if (prepaidResponse.data?.credit != null) {
            var creditTitleLabel = ""
            if (hasValue(prepaidResponse.data.credit.creditTitleLabel)) {
                creditTitleLabel = prepaidResponse.data.credit.creditTitleLabel!!
            }
            var creditTitleValue = ""
            if (hasValue(prepaidResponse.data.credit.creditTitleValue)) {
                creditTitleValue = prepaidResponse.data.credit.creditTitleValue!!
            }
            var creditDateLabel = ""
            if (hasValue(prepaidResponse.data.credit.creditDateLabel)) {
                creditDateLabel = prepaidResponse.data.credit.creditDateLabel!!
            }
            var startDate = ""
            if (hasValue(prepaidResponse.data.credit.creditInitialDate)) {
                startDate = prepaidResponse.data.credit.creditInitialDate!!
            }
            var endDate = ""
            if (hasValue(prepaidResponse.data.credit.creditDate)) {
                endDate = prepaidResponse.data.credit.creditDate!!
            }
            var creditDate = ""
            if (hasValue(endDate)) {
                creditDate = getCreditMRCFormattedDate(endDate)
            }

            if (hasValue(prepaidResponse.data.credit.daysDifferenceTotal) && hasValue(
                    prepaidResponse.data.credit.daysDifferenceCurrent
                )
            ) {
                Handler().postDelayed({
                    populateCreditRow(
                        creditTitleLabel,
                        creditTitleValue,
                        creditDateLabel,
                        startDate,
                        endDate,
                        creditDate,
                        prepaidResponse.data.credit.daysDifferenceTotal!!,
                        prepaidResponse.data.credit.daysDifferenceCurrent!!
                    )
                }, 100L)
            } else {
                creditRowContainer.visibility = View.GONE
            }
        } else {
            creditRowContainer.visibility = View.GONE
        }//if(prepaidResponse.data!!.credit!=null) ends
    }//populatePrepaidUserDashBoard ends

    private fun populateMRCDailyRow(
        mrcTitleLabel: String,
        mrcTitleValue: String,
        mrcDateLabel: String,
        startDate: String,
        endDate: String,
        mrcDate: String
    ) {

        if (!hasValue(mrcTitleLabel) || !hasValue(mrcTitleValue)) {
            //hide the whole view
            logE("mrcPrepaid", "title not", fromClass, "populateMRCDailyRow")
            mrcRowContainer.visibility = View.GONE
        } else {
            logE("mrcPrepaid", "title yes:::".plus(mrcTitleLabel), fromClass, "populateMRCDailyRow")
        }

        mrcTitleLabelTv.text = mrcTitleLabel
        if (isNumeric(mrcTitleValue)) {
            mrcTitleValueTv.text = getManatConcatedString(
                context!!, mrcTitleValue,
                ConstantsUtility.TariffConstants.AZERI_SYMBOL, 0.25f, R.dimen._10ssp
            )
            mrcTitleValueTv.isSelected = true
        } else {
            mrcTitleValueTv.text = mrcTitleValue
            mrcTitleValueTv.isSelected = true
        }

        mrcDateLabelTv.text = mrcDateLabel
        mrcEndingDateTv.text = mrcDate

        var totalHoursDifference = 0
        if (hasValue(startDate) && hasValue(endDate)) {
            totalHoursDifference = getDatesDifference(endDate, startDate, "h").toInt()
            logE(
                "daysDiff",
                "total hours:::".plus(totalHoursDifference),
                fromClass,
                "populateMRCDailyRow"
            )
        }

        var usedHoursDifference = 0
        if (hasValue(startDate)) {
            usedHoursDifference = getDatesDifference(getCurrentDateTime(), startDate, "h").toInt()
            logE(
                "daysDiff",
                "usedHours:::".plus(usedHoursDifference),
                fromClass,
                "populateMRCDailyRow"
            )
        }

        if (hasValue(endDate) && isExpireToShow(endDate)) {
            differenceOfDaysMRCTv.text =
                "".plus(context!!.resources.getString(R.string.dashboard_expired))
        } else {
            val differenceOfTotalHoursAndUsedHours =
                (totalHoursDifference - usedHoursDifference) - 1
            if (differenceOfTotalHoursAndUsedHours > 0) {
                differenceOfDaysMRCTv.text = "".plus(differenceOfTotalHoursAndUsedHours).plus(" ")
                    .plus(context!!.resources.getString(R.string.my_subscriptions_hour))
            } else {
                differenceOfDaysMRCTv.text =
                    "0 ".plus(context!!.resources.getString(R.string.my_subscriptions_hour))
            }
        }

        //progress
        var percentage = 0
        val v = totalHoursDifference.toDouble()
        val n = usedHoursDifference.toDouble()

        if (n > 0) {
            if (v > 0) {
                percentage = ((n * 100) / v).toInt()
            }
        }
        logE("percentage", "percentage:::".plus(percentage), fromClass, "populateMRCDailyRow")
        if (percentage > 0) {
            mrcProgressBar.progress = percentage
        } else {
            mrcProgressBar.progress = 100
        }

    }//populateMRCDailyRow ends

    private fun getCurrentDateDashboard(): String {
        val date = SimpleDateFormat(ConstantsUtility.DateFormats.ddDASHMMDASHyyWithTime)

        return date.format(Date())
    }

    private fun isExpireToShow(mrcDate: String): Boolean {
        var isExpired = false
        val format = SimpleDateFormat(ConstantsUtility.DateFormats.ddDASHMMDASHyyWithTime)
        val mrc = format.parse(mrcDate)
        val current = format.parse(getCurrentDateDashboard())
        if (mrc <= current) {
            isExpired = true
            logE("eXpire", "isExpired:::YES", "isExpireToShow", "isExpireToShow")
        } else {
            isExpired = false
            logE("eXpire", "isExpired:::NOT", "isExpireToShow", "isExpireToShow")
        }

        return isExpired
    }

    private fun populateMRCMonthlyRow(
        mrcTitleLabel: String,
        mrcTitleValue: String,
        mrcDateLabel: String,
        startDate: String,
        endDate: String,
        mrcDate: String
    ) {

        if (!hasValue(mrcTitleLabel) || !hasValue(mrcTitleValue)) {
            //hide the whole view
            logE("mrcPrepaid", "title not", fromClass, "populateMRCMonthlyRow")
            mrcRowContainer.visibility = View.GONE
        } else {
            logE(
                "mrcPrepaid",
                "title yes:::".plus(mrcTitleLabel),
                fromClass,
                "populateMRCMonthlyRow"
            )
        }

        mrcTitleLabelTv.text = mrcTitleLabel
        if (isNumeric(mrcTitleValue)) {
            mrcTitleValueTv.text = getManatConcatedString(
                context!!, mrcTitleValue,
                ConstantsUtility.TariffConstants.AZERI_SYMBOL, 0.25f, R.dimen._10ssp
            )
            mrcTitleValueTv.isSelected = true
        } else {
            mrcTitleValueTv.text = mrcTitleValue
            mrcTitleValueTv.isSelected = true
        }


        mrcDateLabelTv.text = mrcDateLabel
        mrcEndingDateTv.text = mrcDate

        var totalDaysDifference = 0
        if (hasValue(startDate) && hasValue(endDate)) {
            totalDaysDifference = getDatesDifference(endDate, startDate, "d").toInt()
            logE(
                "daysDiff",
                "total days:::".plus(totalDaysDifference),
                fromClass,
                "populateMRCMonthlyRow"
            )
        }

        var usedDaysDifference = 0
        if (hasValue(startDate)) {
            usedDaysDifference = getDatesDifference(getCurrentDateTime(), startDate, "d").toInt()
            logE(
                "daysDiff",
                "usedDays:::".plus(usedDaysDifference),
                fromClass,
                "populateMRCMonthlyRow"
            )
        }

        if (hasValue(endDate) && isExpireToShow(endDate)) {
            differenceOfDaysMRCTv.text =
                "".plus(context!!.resources.getString(R.string.dashboard_expired))
        } else {
            val differenceOfTotalDaysAndUsedDays = (totalDaysDifference - usedDaysDifference) - 1
            if (differenceOfTotalDaysAndUsedDays > 0) {
                differenceOfDaysMRCTv.text = context!!.resources.getQuantityString(
                    R.plurals.my_subscriptions_remaining_days,
                    differenceOfTotalDaysAndUsedDays,
                    differenceOfTotalDaysAndUsedDays
                )
            } else {
                differenceOfDaysMRCTv.text = context!!.resources.getQuantityString(
                    R.plurals.my_subscriptions_remaining_days,
                    0,
                    0
                )
            }
        }


        //progress
        var percentage = 0
        val v = totalDaysDifference.toDouble()
        val n = (usedDaysDifference - 1).toDouble()

        if (n > 0) {
            if (v > 0) {
                percentage = ((n * 100) / v).toInt()
            }
        }
        logE("percentage", "percentage:::".plus(percentage), fromClass, "populateMRCMonthlyRow")
        /*  if (percentage == 100) {
              mrcProgressBar.progress = 0
          } else {
              mrcProgressBar.progress = percentage
          }*/
        mrcProgressBar.progress = percentage
    }//populateMRCRow ends

    private fun populateCreditRow(
        creditTitleLabel: String,
        creditTitleValue: String,
        creditDateLabel: String,
        startDate: String,
        endDate: String,
        creditDate: String,
        daysDifferenceTotal: String,
        daysDifferenceCurrent: String
    ) {

        creditTitleLabelTv?.text = creditTitleLabel
        creditTitleValueTv?.text = getManatConcatedString(
            context!!, creditTitleValue,
            ConstantsUtility.TariffConstants.AZERI_SYMBOL, 0.25f, R.dimen._12ssp
        )
        creditTitleValueTv?.isSelected = true

        creditDateLabelTv?.text = creditDateLabel
        creditEndingDateTv?.text = creditDate

        /* var totalDaysDifference = 0
         if (hasValue(startDate) && hasValue(endDate)) {
             totalDaysDifference = getDatesDifference(endDate, startDate, "d").toInt()
             logE("daysDiff", "total days:::".plus(totalDaysDifference), fromClass, "populateCreditRow")
         }

         var usedDaysDifference = 0
         if (hasValue(startDate)) {
             usedDaysDifference = getDatesDifference(getCurrentDateTime(), startDate, "d").toInt()
             logE("daysDiff", "usedDays:::".plus(usedDaysDifference), fromClass, "populateCreditRow")
         }*/

        var daysTotal = 0
        var daysCurrent = 0

        if (isNumeric(daysDifferenceTotal)) {
            daysTotal = daysDifferenceTotal.toInt()
        }

        if (isNumeric(daysDifferenceCurrent)) {
            daysCurrent = daysDifferenceCurrent.toInt()
        }

        val differenceOfTotalDaysAndUsedDays = (daysTotal - daysCurrent)
        logE(
            "daysDiff",
            "differenceOfTotalDaysAndUsedDays:::".plus(differenceOfTotalDaysAndUsedDays),
            fromClass,
            "populateCreditRow"
        )
        if (differenceOfTotalDaysAndUsedDays > 0) {
            if (differenceOfTotalDaysAndUsedDays == 1) {
                differenceOfDaysTv?.text = differenceOfTotalDaysAndUsedDays.toString().plus(" ")
                    .plus(getLocalizedDayTitle())
            } else {
                differenceOfDaysTv?.text = differenceOfTotalDaysAndUsedDays.toString().plus(" ")
                    .plus(getLocalizedDaysTitle())
            }
        } else {
            differenceOfDaysTv?.text = "0".plus(" ").plus(getLocalizedDayTitle())
        }

        differenceOfDaysTv?.visibility = View.VISIBLE

        //progress
        var percentage = 0
        val v = daysTotal.toDouble()
        val n = daysCurrent.toDouble()

        if (n > 0) {
            if (v > 0) {
                percentage = ((n * 100) / v).toInt()
            }
        }
        logE("percentage", "percentage:::".plus(percentage), fromClass, "populateCreditRow")
        if (percentage > 0) {
            creditProgressBar?.progress = percentage
        } else {
            creditProgressBar?.progress = 100
        }

    }//populateCreditRow ends

    private fun getLocalizedDayTitle(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "day"
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "gün"
            else -> "дн."
        }
    }

    private fun getLocalizedDaysTitle(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "days"
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "gün"
            else -> "дн."
        }
    }

    private fun populateMiddleRecycler(
        dashboardResponse: GetHomePageResponse?,
        isFreeResourcesRoaming: Boolean
    ) {
        if (dashboardResponse == null) {
            logE("dashboard", "repsonse is null", fromClass, "populateMiddleRecycler")
            middleContainer.visibility = View.VISIBLE
            middleContainer.setBackgroundDrawable(context!!.resources.getDrawable(R.drawable.unreveal_bg))
            dashBoardNoData.visibility = View.VISIBLE
            revealSource.visibility = View.GONE
            revealLayout.visibility = View.GONE
            return//safe passage go back
        }//if (dashboardResponse == null) ends

        if (isFreeResourcesRoaming) {
            logE("dashboard", "isFreeResourcesRoaming", fromClass, "populateMiddleRecycler")
            val data = ArrayList<HomePageFreeResourcesHelperModel>()
            dashboardResponse.data?.freeResources?.freeResourcesRoaming?.let {
                logE(
                    "postpaid_",
                    "data size = ${data.size}",
                    fromClass,
                    "populatePostpaidUserDashBoard"
                )
                data.addAll(
                    mViewModel.getFreeResourcesRoamingItemsList(
                        it,
                        isDataSimPrepaidPostpaid
                    )
                )
            }
            mDashboardCenterContentAdapter = DashboadMiddleRecycler(context!!, data)
            //set the adapter
            centerContentRecycler.apply {
                layoutManager = getVerticalLayoutManager()
                adapter = mDashboardCenterContentAdapter
            }
            //check model if not empty, then show the view
            if (!data.isNullOrEmpty()) {
                logE(
                    "postpaid_",
                    "isFreeResourcesRoaming data size = ${data.size}",
                    fromClass,
                    "populatePostpaidUserDashBoard"
                )
                revealSource.visibility = View.VISIBLE
                middleContainer.setBackgroundDrawable(context!!.resources.getDrawable(R.drawable.reveal_bg))
                revealLayout.visibility = View.VISIBLE
                dashBoardNoData.visibility = View.GONE
            } else {
                middleContainer.setBackgroundDrawable(context!!.resources.getDrawable(R.drawable.unreveal_bg))
                dashBoardNoData.visibility = View.VISIBLE
                revealSource.visibility = View.GONE
                revealLayout.visibility = View.GONE
            }
        } else {
            logE("dashboard", "isFreeResourcesRoaming NOT", fromClass, "populateMiddleRecycler")
            val data = ArrayList<HomePageFreeResourcesHelperModel>()
            dashboardResponse.data?.freeResources?.freeResources?.let {
                logE(
                    "postpaid_",
                    "data size = ${data.size}",
                    fromClass,
                    "populatePostpaidUserDashBoard"
                )
                data.addAll(mViewModel.getFreeResourcesItemsList(it, isDataSimPrepaidPostpaid))
            }
            mDashboardCenterContentAdapter = DashboadMiddleRecycler(
                context!!,
                data
            )
            //set the adapter
            centerContentRecycler.apply {
                layoutManager = getVerticalLayoutManager()
                adapter = mDashboardCenterContentAdapter
            }
            //check model if not empty, then show the view
            if (!data.isNullOrEmpty()) {
                logE(
                    "postpaid_",
                    "isNotFreeResourcesRoaming data size = ${data.size}",
                    fromClass,
                    "populatePostpaidUserDashBoard"
                )

                revealSource.visibility = View.VISIBLE
                middleContainer.setBackgroundDrawable(context!!.resources.getDrawable(R.drawable.reveal_bg))
                revealLayout.visibility = View.VISIBLE
                dashBoardNoData.visibility = View.GONE
            } else {
                middleContainer.setBackgroundDrawable(context!!.resources.getDrawable(R.drawable.unreveal_bg))
                dashBoardNoData.visibility = View.VISIBLE
                revealSource.visibility = View.GONE
                revealLayout.visibility = View.GONE
            }
        }

    }//populateMiddleRecycler ends

    private fun getVerticalLayoutManager(): LinearLayoutManager {
        /*centerContentRecycler.addItemDecoration(
                object : DividerItemDecoration(context, mLayoutManager.orientation) {
                    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                        val position = parent.getChildAdapterPosition(view)
                        // hide the divider for the last child
                        if (position == parent.adapter!!.itemCount - 1) {
                            outRect.setEmpty()
                        } else {
                            super.getItemOffsets(outRect, view, parent, state)
                        }
                    }
                }
        )*/
        return LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
    }//getVerticalLayoutManager ends

    override fun onInVisible() {

    }

}