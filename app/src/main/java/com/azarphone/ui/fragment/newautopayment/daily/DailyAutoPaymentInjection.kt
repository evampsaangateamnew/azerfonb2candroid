package com.azarphone.ui.fragment.newautopayment.daily

object DailyAutoPaymentInjection {

    private fun provideDailyAutoPaymentRepository(): DailyAutoPaymentRepository {
        return DailyAutoPaymentRepository()
    }

    fun provideDailyAutoPaymentFactory(): DailyAutoPaymentFactory {
        return DailyAutoPaymentFactory(provideDailyAutoPaymentRepository())
    }

}