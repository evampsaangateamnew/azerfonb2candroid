package com.azarphone.ui.fragment.newautopayment.weekly

object WeeklyAutoPaymentInjection {

    private fun provideDailyAutoPaymentRepository(): WeeklyAutoPaymentRepository {
        return WeeklyAutoPaymentRepository()
    }

    fun provideWeeklyAutoPaymentFactory(): WeeklyAutoPaymentFactory {
        return WeeklyAutoPaymentFactory(provideDailyAutoPaymentRepository())
    }

}