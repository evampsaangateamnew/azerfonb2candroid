package com.azarphone.ui.fragment.newautopayment.weekly

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class WeeklyAutoPaymentFactory(private val weeklyAutoPaymentRepository: WeeklyAutoPaymentRepository) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(WeeklyAutoPaymentViewModel::class.java)) {
            return WeeklyAutoPaymentViewModel(weeklyAutoPaymentRepository) as T
        }
        throw IllegalStateException("ViewModel Cannot be Converted")
    }//create ends

}