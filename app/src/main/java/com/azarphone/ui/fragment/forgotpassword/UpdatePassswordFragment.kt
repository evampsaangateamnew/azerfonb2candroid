package com.azarphone.ui.fragment.forgotpassword

import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.azarphone.R
import com.azarphone.bases.BaseFragment
import com.azarphone.databinding.ConfirmPasswordLayoutBinding
import com.azarphone.eventhandler.ForgotPasswordActivityToolTipEvents
import com.azarphone.ui.activities.forgotpasswordactivity.ForgotPasswordActivity
import com.azarphone.ui.activities.forgotpasswordactivity.ForgotPasswordFactory
import com.azarphone.ui.activities.forgotpasswordactivity.ForgotPasswordInjection
import com.azarphone.ui.activities.forgotpasswordactivity.ForgotPasswordViewModel
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.RootValues
import com.azarphone.util.getALSBoldFont
import com.azarphone.widgets.tooltip.initiateErrorToolTip
import io.canner.stepsview.StepsView
import kotlinx.android.synthetic.main.confirm_password_layout.view.*

@Suppress("DEPRECATION")
class UpdatePassswordFragment : BaseFragment<ConfirmPasswordLayoutBinding, ForgotPasswordFactory, ForgotPasswordViewModel>() {

    private val fromClass = "UpdatePassswordFragment"
    private lateinit var externalView: View
    private var forgotPasswordActivityToolTipEvents: ForgotPasswordActivityToolTipEvents? = null

    override fun getLayoutId(): Int {
        return R.layout.confirm_password_layout
    }


    override fun getViewModelClass(): Class<ForgotPasswordViewModel> {
        return ForgotPasswordViewModel::class.java
    }

    override fun getFactory(): ForgotPasswordFactory {
        return ForgotPasswordInjection.provideForgotPasswordFactory()
    }

    override fun isBindedToActivityLifeCycle(): Boolean {
        return true
    }

    override fun onVisible(view: View, savedInstanceState: Bundle?) {
        externalView = view
        setupInputFields()
        registerListener()
        mFragmentViewDataBinding.apply {
            viewmodel = mViewModel
        }

        activity?.let {
            val stepView = it.findViewById<StepsView>(R.id.stepsView).setCompletedPosition(2).drawView()
        }

        /*//ui events
        view.termsConditionsLabel.setOnClickListener {
            showTermsAndConditionsPopup(mViewModel.mOTPVerifyResponse.value?.data?.content!!)
        }//view.termsConditionsLabel.setOnClickListener ends*/


        mViewModel.setBackPressEnable(false)
        mViewModel.setForgotPasswordStep(ConstantsUtility.ForgetPasswordScreenSteps.UPDATE_PASSWORD_STEP)

        view.reenterPasswordET.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if (event!!.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    (activity as ForgotPasswordActivity).processForgotPassword()
                }//if ends
                return false
            }//onKey ends
        })

        externalView.reenterPasswordET.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    (activity as ForgotPasswordActivity).processForgotPassword()
                }//if ends
                return false
            }//onEditorAction ends
        })
    }

    private fun registerListener() {
        forgotPasswordActivityToolTipEvents = object : ForgotPasswordActivityToolTipEvents {
            override fun onInvalidPasswordTooltip(errorDetail: String) {
                initiateErrorToolTip(context!!, externalView.enterPasswordET, errorDetail)
            }

            override fun onInvalidConfirmPasswordTooltip(errorDetail: String) {
                initiateErrorToolTip(context!!, externalView.reenterPasswordET, errorDetail)
            }

            override fun onInvalidOTPPINTooltip(errorDetail: String) {
                //do nothing
            }

            override fun onInvalidMsisdnNumberTooltip(errorDetail: String) {
                //do nothing
            }
        }

        RootValues.setForgotPasswordActivityToolTipEvents(forgotPasswordActivityToolTipEvents!!)
    }

    private fun setupInputFields() {
        externalView.enterPasswordTIL.defaultHintTextColor = context!!.resources.getColorStateList(R.color.soft_pink)
        externalView.enterPasswordET.typeface = getALSBoldFont()
        externalView.enterPasswordET.transformationMethod = PasswordTransformationMethod.getInstance()

        externalView.reenterPasswordTIL.defaultHintTextColor = context!!.resources.getColorStateList(R.color.soft_pink)
        externalView.reenterPasswordET.typeface = getALSBoldFont()
        externalView.reenterPasswordET.transformationMethod = PasswordTransformationMethod.getInstance()
    }//setupInputFields ends

    override fun onInVisible() {
    }


}