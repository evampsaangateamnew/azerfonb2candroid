package com.azarphone.ui.fragment.newautopayment.monthly

object MonthlyAutoPaymentInjection {

    private fun provideDailyAutoPaymentRepository(): MonthlyAutoPaymentRepository {
        return MonthlyAutoPaymentRepository()
    }

    fun provideWeeklyAutoPaymentFactory(): MonthlyAutoPaymentFactory {
        return MonthlyAutoPaymentFactory(provideDailyAutoPaymentRepository())
    }

}