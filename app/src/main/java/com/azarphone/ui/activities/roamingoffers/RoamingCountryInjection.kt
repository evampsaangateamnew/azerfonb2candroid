package com.azarphone.ui.activities.roamingoffers

/**
 * @author Junaid Hassan on 24, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
object RoamingCountryInjection {

    fun provideRoamingCountryRepository(): RoamingCountryRepositry {
        return RoamingCountryRepositry()
    }

    fun provideRoamingCountryFactory(): RoamingCountryFactory {
        return RoamingCountryFactory(provideRoamingCountryRepository())
    }

}