package com.azarphone.ui.activities.cardselection

object CardSelectionInjection {

    private fun provideCardSelectionRepository(): CardSelectionRepository {
        return CardSelectionRepository()
    }

    fun provideCardSelectionFactory(): CardSelectionFactory {
        return CardSelectionFactory(provideCardSelectionRepository())
    }
}