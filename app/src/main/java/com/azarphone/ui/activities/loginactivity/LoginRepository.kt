package com.azarphone.ui.activities.loginactivity

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.LoginRequest
import com.azarphone.api.pojo.request.RegisterRequest
import com.azarphone.api.pojo.response.loginappresumeresponse.LoginAppResumeResponse
import com.azarphone.api.pojo.response.register.RegisterResponse
import io.reactivex.Observable

class LoginRepository {

    init {

    }

    fun processUserLogin(loginRequest: LoginRequest): Observable<LoginAppResumeResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().userLogin(loginRequest)
    }

}