package com.azarphone.ui.activities.forgotpasswordactivity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.Navigation.findNavController
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.UserAccounts
import com.azarphone.api.pojo.response.customerdata.CustomerData
import com.azarphone.api.pojo.response.forgotpassword.ForgotPasswordResponse
import com.azarphone.api.pojo.response.otpgetresponse.OTPResponse
import com.azarphone.api.pojo.response.otpverifyresponse.OTPVerifyResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.LayoutActivityForgotPasswordBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.ui.activities.mainactivity.MainActivity
import com.azarphone.util.*
import com.azarphone.util.ConstantsUtility.ManageAccount.LOGIN_USECASE_ADDACCOUNT
import com.azarphone.util.ConstantsUtility.ManageAccount.LOGIN_USECASE_LOGIN
import com.azarphone.util.ConstantsUtility.ManageAccount.LOGIN_USECASE_TYPE_KEY
import com.azarphone.validators.hasValue
import com.azarphone.validators.isValidMsisdn
import com.azarphone.validators.isValidOTP
import com.azarphone.validators.isValidPasswordLength
import com.azarphone.widgets.popups.showMessageDialog
import kotlinx.android.synthetic.main.app_header_layout.*
import kotlinx.android.synthetic.main.layout_activity_forgot_password.*

class ForgotPasswordActivity : BaseActivity<LayoutActivityForgotPasswordBinding, ForgotPasswordFactory, ForgotPasswordViewModel>() {

    private val fromClass = "ForgotPasswordActivity"
    private var userNumber = ""
    private var newPassword = ""
    private var newPasswordConfirm = ""
    private var pinOTP = ""
    private var useCase_Type = LOGIN_USECASE_LOGIN

    override fun getViewModelClass(): Class<ForgotPasswordViewModel> {
        return ForgotPasswordViewModel::class.java
    }

    override fun getFactory(): ForgotPasswordFactory {
        return ForgotPasswordInjection.provideForgotPasswordFactory()
    }

    override fun getLayoutId(): Int {
        return R.layout.layout_activity_forgot_password
    }

    override fun init() {
        subscribeForgotPasswordStepperDataObserver()
        subscribeToForgetPasswordLiveDataObserver()


        if (intent.extras != null) {
            getDataFromIntent(intent.extras!!)
        }

        forgotPasswordButton.setOnClickListener {
            logE("forXC", "clicked", fromClass, "forgotPasswordButtonClick")
            processForgotPassword()
        }//forgotPasswordButton.setOnClickListener ends
        azerActionBarBackIconBacker.visibility = View.VISIBLE
        azerActionBarBackIconBacker.setOnClickListener {
            if (forgotPasswordButton.tag.equals(ConstantsUtility.ForgetPasswordScreenSteps.ENTER_NUMBER_STEP)) {
                finish()
            } else if (forgotPasswordButton.tag.equals(ConstantsUtility.ForgetPasswordScreenSteps.ENTER_PIN_STEP)) {
                if (mViewModel.enableBackPress) {
                    super.onBackPressed()
                }
            }
        }
    }

    fun processForgotPassword() {
        hideKeyboard(this@ForgotPasswordActivity)
        if (forgotPasswordButton.tag.toString()
                        .equals(ConstantsUtility.ForgetPasswordScreenSteps.ENTER_NUMBER_STEP, ignoreCase = true)) {
            //validate the msisdn
            if (isValidMsisdn(userNumber)) {
                if (useCase_Type.equals(LOGIN_USECASE_ADDACCOUNT)) {
                    var list: ArrayList<CustomerData>? = ArrayList()
                    if (getUserAccountsDataFromLocalPreferences() != null && getUserAccountsDataFromLocalPreferences().usersList != null) {
                        list = getUserAccountsDataFromLocalPreferences().usersList as ArrayList<CustomerData>
                    }
                    var msisdn = userNumber.toString()
                    //replace the prefix
                    msisdn = msisdn.replace(ConstantsUtility.UserConstants.USERNAME_PREFIX, "")

                    var isMsisdnAlreadyExists = false
                    if (list != null) {
                        for (number in list.iterator()) {
                            if (number.msisdn == msisdn) {
                                isMsisdnAlreadyExists = true
                                break
                            }
                        }
                    }

                    if (isMsisdnAlreadyExists) {
                        showMessageDialog(this, this@ForgotPasswordActivity,
                                resources.getString(R.string.popup_error_title),
                                resources.getString(R.string.number_already_exists))
                    } else {
                        mViewModel.requestOTP(userNumber)
                    }

                } else {
                    mViewModel.requestOTP(userNumber)
                }

            } else {
                RootValues.getForgotPasswordActivityToolTipEvents().onInvalidMsisdnNumberTooltip(resources.getString(R.string.error_msg_invalid_number))
            }//validate msisdn ends
        } else if (forgotPasswordButton.tag.toString()
                        .equals(ConstantsUtility.ForgetPasswordScreenSteps.ENTER_PIN_STEP, ignoreCase = true)) {
            if (!hasValue(pinOTP)) {
                RootValues.getForgotPasswordActivityToolTipEvents().onInvalidOTPPINTooltip(resources.getString(R.string.invalid_otp_label))
            } else {
                if (isValidOTP(pinOTP)) {
                    mViewModel.verfityPin()
                } else {
                    RootValues.getForgotPasswordActivityToolTipEvents().onInvalidOTPPINTooltip(resources.getString(R.string.invalid_otp_label))
                }
            }
        } else if (forgotPasswordButton.tag.toString()
                        .equals(ConstantsUtility.ForgetPasswordScreenSteps.UPDATE_PASSWORD_STEP, ignoreCase = true)) {
            if (!hasValue(newPassword)) {
                //password field is empty
                RootValues.getForgotPasswordActivityToolTipEvents().onInvalidPasswordTooltip(resources.getString(R.string.passwords_criteria_error))
            } else if (!hasValue(newPasswordConfirm)) {
                RootValues.getForgotPasswordActivityToolTipEvents().onInvalidConfirmPasswordTooltip(resources.getString(R.string.passwords_criteria_error))
            } else if (!isValidPasswordLength(newPassword)) {
                RootValues.getForgotPasswordActivityToolTipEvents().onInvalidPasswordTooltip(resources.getString(R.string.passwords_criteria_error))
            } else if (!isValidPasswordLength(newPasswordConfirm)) {
                RootValues.getForgotPasswordActivityToolTipEvents().onInvalidConfirmPasswordTooltip(resources.getString(R.string.passwords_criteria_error))
            } else {
                //check if password and confirm passwords are matched
                if (newPassword.equals(newPasswordConfirm)) {
                    //passwords matched
                    mViewModel.updatePassword()
                } else {
                    //show error for passwords not matched
                    RootValues.getForgotPasswordActivityToolTipEvents().onInvalidConfirmPasswordTooltip(resources.getString(R.string.passwords_not_matched))
                }
            }
        } else {
            logE("forgotpasswordsteps", "else->do nothing", fromClass, "forgotPasswordButton.setOnClickListener")
        }
    }

    private fun getDataFromIntent(extras: Bundle) {
        if (extras.containsKey(LOGIN_USECASE_TYPE_KEY)) {
            useCase_Type = extras.getString(LOGIN_USECASE_TYPE_KEY)!!
        }
    }

    override fun onBackPressed() {
        if (mViewModel.enableBackPress) {
            super.onBackPressed()
        }
    }

    private fun subscribeToForgetPasswordLiveDataObserver() {
        val forgotPasswordOtpPinLiveDataObserver = Observer<String> {
            if (it != null) {
                pinOTP = it
            }
        }

        val forgotPasswordNewPinLiveDataObserver = Observer<String> {
            if (it != null) {
                newPassword = it
            }
        }

        val forgotPasswordConfirmPinLiveDataObserver = Observer<String> {
            it?.let { confirmPin ->
                newPasswordConfirm = confirmPin
            }
        }

        /*val forgotPasswordIsCheckedLiveDataObserver = Observer<Boolean> {
            it?.let { termAndCondition ->
                if (forgotPasswordButton.tag.toString()
                                .equals(ConstantsUtility.ForgetPasswordScreenSteps.UPDATE_PASSWORD_STEP, ignoreCase = true)){
                    termAndCondIsChecked = termAndCondition
                    if(termAndCondIsChecked){
                        forgotPasswordButton.isEnabled = true
                        forgotPasswordButton.isClickable = true
                    }else{
                        forgotPasswordButton.isEnabled = false
                        forgotPasswordButton.isClickable = false
                    }
                }
            }
        }*/

        val forgotPasswordNumberLiveDataObserver = object : Observer<String> {
            override fun onChanged(t: String?) {
                t?.let {
                    userNumber = t
                    userNumber = userNumber.replace(ConstantsUtility.UserConstants.USERNAME_PREFIX, "")
                }
            }
        }

        val requestOtpResponse = object : Observer<OTPResponse> {
            override fun onChanged(t: OTPResponse?) {
                t?.let {
                    if (it.resultCode.equals("00")) {

                        val mBundle = Bundle()
                        mBundle.putString(ConstantsUtility.BundleKeys.BUNDLE_FORGOT_PASSWORD_NUMBER, userNumber)

                        //goto enter pin fragment
                        findNavController(this@ForgotPasswordActivity, R.id.my_nav_host_fragment)
                                .navigate(R.id.action_enter_number_to_enter_pin, mBundle)
                    }
                }
            }
        }

        val verifyOtpResponse = object : Observer<OTPVerifyResponse> {
            override fun onChanged(p0: OTPVerifyResponse?) {
                p0?.let {
                    findNavController(this@ForgotPasswordActivity, R.id.my_nav_host_fragment).navigate(R.id.action_enter_pin_to_update_password)
                }
            }

        }


        val mForgotPasswordResponse = object : Observer<ForgotPasswordResponse> {
            override fun onChanged(response: ForgotPasswordResponse?) {
                response?.let {

                    //handle the response
                    if (response?.data != null) {
                        if (response.data.customerData != null) {

                            var list: ArrayList<CustomerData>? = ArrayList()
                            if (useCase_Type.equals(LOGIN_USECASE_ADDACCOUNT)) {


                                if (getUserAccountsDataFromLocalPreferences() != null && getUserAccountsDataFromLocalPreferences().usersList != null) {
                                    list = getUserAccountsDataFromLocalPreferences().usersList as ArrayList<CustomerData>
                                }
                            }

                            list?.add(0, response.data.customerData!!)

                            /* if (list != null) {
                                 for (i in 0 until list.size)
                                     logE("UserListMSISDN", list.get(i).msisdn!!, fromClass, "UserAccountAdded")
                             }*/

                            ProjectApplication.getInstance().getLocalPreferneces().setResponse(
                                    ProjectApplication.getInstance().getLocalPreferneces().KEY_USER_ACCOUNTS_DATA,
                                    UserAccounts(list)
                            )

                            //save the data in the session
                            UserDataManager.saveCustomerData(response.data.customerData)
                            UserDataManager.savePredefineData(response.data.predefinedData)
                            //save the response in shared preferences
                            setCustomerDataToLocalPreferences(response.data.customerData)

                            //save the time of the login
                            logE("timeX", "time1:::".plus(getUserLoginTime()), fromClass, "currentDateTime")
                            setUserLoginTime(getCurrentDateTimeForRateUs())
                            logE("timeX", "time2:::".plus(getUserLoginTime()), fromClass, "currentDateTime")

                            //no go to the next activity
                            MainActivity.start(applicationContext, ConstantsUtility.BundleKeys.BUNDLE_DONT_CALL_APP_RESUME)
                        } else {
                            showMessageDialog(applicationContext, this@ForgotPasswordActivity,
                                    resources.getString(R.string.popup_error_title),
                                    resources.getString(R.string.server_stopped_responding_please_try_again))
                        }
                    } else {
                        showMessageDialog(applicationContext, this@ForgotPasswordActivity,
                                resources.getString(R.string.popup_error_title),
                                resources.getString(R.string.server_stopped_responding_please_try_again))
                    }
                }
            }
        }


        mViewModel.requestOTPResponse.observe(this@ForgotPasswordActivity, requestOtpResponse)
        mViewModel.mForgotPasswordResponse.observe(this@ForgotPasswordActivity, mForgotPasswordResponse)
        mViewModel.mOTPVerifyResponse.observe(this@ForgotPasswordActivity, verifyOtpResponse)
        mViewModel.forgotPasswordNumberLiveDataObserver.observe(this@ForgotPasswordActivity, forgotPasswordNumberLiveDataObserver)
        mViewModel.updatePasswordNewPin.observe(this@ForgotPasswordActivity, forgotPasswordNewPinLiveDataObserver)
        mViewModel.updatePasswordConfirmPin.observe(this@ForgotPasswordActivity, forgotPasswordConfirmPinLiveDataObserver)
//        mViewModel.updatePasswordIsChecked.observe(this@ForgotPasswordActivity, forgotPasswordIsCheckedLiveDataObserver)
        mViewModel.forgotpasswordPinET.observe(this@ForgotPasswordActivity, forgotPasswordOtpPinLiveDataObserver)

    }

    private fun subscribeForgotPasswordStepperDataObserver() {
        val forgotPasswordStepperDataObserver = object : Observer<String> {
            override fun onChanged(step: String?) {
                if (step.equals(ConstantsUtility.ForgetPasswordScreenSteps.ENTER_NUMBER_STEP, ignoreCase = true)) {
                    forgotPasswordButton.text = resources.getString(R.string.forgot_password_no_next_button_label)
                    forgotPasswordButton.tag = ConstantsUtility.ForgetPasswordScreenSteps.ENTER_NUMBER_STEP
                } else if (step.equals(ConstantsUtility.ForgetPasswordScreenSteps.ENTER_PIN_STEP, ignoreCase = true)) {
                    forgotPasswordButton.text = resources.getString(R.string.forgot_password_enter_pin_verify_button)
                    forgotPasswordButton.tag = ConstantsUtility.ForgetPasswordScreenSteps.ENTER_PIN_STEP
                } else if (step.equals(ConstantsUtility.ForgetPasswordScreenSteps.UPDATE_PASSWORD_STEP, ignoreCase = true)) {
                    forgotPasswordButton.text = resources.getString(R.string.update_password_update_label)
                    forgotPasswordButton.tag = ConstantsUtility.ForgetPasswordScreenSteps.UPDATE_PASSWORD_STEP
                } else {
                    logE("forgotpasswordsteps", "else->do nothing", fromClass, "subscribeForgotPasswordStepperDataObserver")
                }//else ends
            }//onChanged ends
        }//val forgotPasswordStepperDataObserver = object : Observer<String> ends

        mViewModel.forgotPasswordStepperData.observe(this@ForgotPasswordActivity, forgotPasswordStepperDataObserver)
    }//subscribeForgotPasswordStepperDataObserver ends

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            /**if user is on the last step of the forgot password
             * force him that he cant use back key to go back
             * in other words on the last step of the forgot password the user cant go back*/
            if (!forgotPasswordButton.tag.toString().equals(ConstantsUtility.ForgetPasswordScreenSteps.UPDATE_PASSWORD_STEP, ignoreCase = true)) {
                //do nothing, continue the normal flow
            } else {
                return false
            }//if-else ends
        }
        return super.onKeyDown(keyCode, event)
    }//onKeyDown ends

    companion object {
        fun start(context: Context, useCase_Type: String) {
            val intent = Intent(context, ForgotPasswordActivity::class.java)
            intent.putExtra(LOGIN_USECASE_TYPE_KEY, useCase_Type)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends
    }//object ends
}//class ends