package com.azarphone.ui.activities.supplementaryoffers

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

/**
 * @author Junaid Hassan on 22, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class SupplementaryOffersFactory(val mSupplementaryOffersRepositry: SupplementaryOffersRepositry) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SupplementaryOffersViewModel::class.java)) {
            return SupplementaryOffersViewModel(mSupplementaryOffersRepositry) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}