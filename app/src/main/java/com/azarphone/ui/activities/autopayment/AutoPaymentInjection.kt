package com.azarphone.ui.activities.autopayment

/**
 * @author Umair Mustafa on 02/04/2021
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * umair.mustafa@evampsaanga.com
 */
object AutoPaymentInjection {


    fun provideAutoPaymentRepository(): AutoPaymentRepositry {
        return AutoPaymentRepositry()
    }

    fun provideAutoPaymentFactory(): AutoPaymentFactory {
        return AutoPaymentFactory(provideAutoPaymentRepository())
    }

}