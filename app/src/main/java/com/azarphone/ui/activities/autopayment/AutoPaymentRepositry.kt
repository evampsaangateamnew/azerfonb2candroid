package com.azarphone.ui.activities.autopayment

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.response.autopaymentresponse.ScheduledPaymentResponse
import com.azarphone.api.pojo.response.autopaymentresponse.deletepayment.DeleteAutoPayment
import com.azarphone.api.pojo.response.fasttopupresponse.deletefasttopup.DeleteItemResponse
import io.reactivex.Observable

/**
 * @author Umair Mustafa on 02/04/2021
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * umair.mustafa@evampsaanga.com
 */
class AutoPaymentRepositry {
    init {

    }

    fun requestGetScheduledPayments(): Observable<ScheduledPaymentResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestGetScheduledPayments()
    }

    fun requestDeletePaymentScheduler(deleteAutoPayment: DeleteAutoPayment): Observable<DeleteItemResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().deletePaymentScheduler(deleteAutoPayment)
    }

}