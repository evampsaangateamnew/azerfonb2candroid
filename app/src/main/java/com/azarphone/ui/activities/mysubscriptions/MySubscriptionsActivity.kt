package com.azarphone.ui.activities.mysubscriptions

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.response.inappsurvey.InAppSurvey
import com.azarphone.api.pojo.response.inappsurvey.Surveys
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.mysubscriptions.helper.Subscriptions
import com.azarphone.api.pojo.response.mysubscriptions.response.MySubscriptionResponse
import com.azarphone.api.pojo.response.mysubscriptions.successresponse.MySubscriptionsSuccessAfterOfferSubscribeResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.LayoutMysubscriptionsActivityBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.MySubscriptionEvents
import com.azarphone.eventhandler.OnClickSurveySubmit
import com.azarphone.eventhandler.UpdateListOnSuccessfullySurvey
import com.azarphone.ui.adapters.pagers.SubscriptionsOffersPagerAdapter
import com.azarphone.ui.dialogs.InAppFeedbackDialog
import com.azarphone.ui.fragment.mysubscriptions.SubscriptionOffersPaggerFragment
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.widgets.popups.showMessageDialog
import com.es.tec.LocalSharedPrefStorage
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.layout_core_service_view_item.*
import kotlinx.android.synthetic.main.layout_mysubscriptions_activity.*
import kotlinx.android.synthetic.main.no_data_found_mysubscriptions_white_no_bg.*

/**
 * @author Junaid Hassan on 22, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class MySubscriptionsActivity :
    BaseActivity<LayoutMysubscriptionsActivityBinding, MySubscriptionsFactory, MySubscriptionsViewModel>() {

    private val tabNames = ArrayList<String>()
    private var subscriptionsOffersPagerAdapter: SubscriptionsOffersPagerAdapter? = null
    private var mySubscriptionResponse: MySubscriptionResponse? = null
    private var redirectionIndex = 1
    private var dashboardRedirectionKey = ""
    private var offeringId = ""
    private val fromClass = "MySubscriptionsActivity"

    override fun getLayoutId(): Int {
        return R.layout.layout_mysubscriptions_activity
    }//getLayoutId ends

    override fun getViewModelClass(): Class<MySubscriptionsViewModel> {
        return MySubscriptionsViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): MySubscriptionsFactory {
        return MySubscriptionsInjection.provideMySubscriptionsFactory()
    }//getFactory ends

    override fun init() {
        setupHeaderActionBar()
        initIntentData()
        initUIEvents()
        initMySubscriptionEvents()
        subscribe()
        initUI()
        requestMySubscriptions()
    }//init ends

    private fun showConfirmationPopup(
        _offeringName: String,
        _offeringId: String,
        _actionType: String
    ) {
        if (this@MySubscriptionsActivity.isFinishing) return

        val dialogBuilder = AlertDialog.Builder(this@MySubscriptionsActivity)
        val inflater =
            this@MySubscriptionsActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val dialogView = inflater.inflate(R.layout.azer_vas_services_confirmation_popup, null)
        dialogBuilder.setView(dialogView)

        val alertDialog = dialogBuilder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.show()

        val popupTitle = dialogView.findViewById(R.id.popupTitle) as TextView
        popupTitle.typeface = getALSNormalFont()
        popupTitle.text = getSupplementaryOfferRenewConfirmationMessage()
        val popupOkayButton = dialogView.findViewById(R.id.yesButton) as Button
        popupOkayButton.typeface = getALSBoldFont()
        val popupCancelButton = dialogView.findViewById(R.id.noButton) as Button
        popupCancelButton.typeface = getALSBoldFont()
        popupOkayButton.text = getString(R.string.popup_text_update_ok)

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN) {
            //english
            popupCancelButton.text = "No"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ) {
            //azri
            popupCancelButton.text = "Xeyr"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.RU) {
            //russian
            popupCancelButton.text = "Нет"
        }

        popupOkayButton.setOnClickListener {
            alertDialog.dismiss()
            mViewModel.subscribeToSupplementaryOffer(_offeringName, _offeringId, _actionType)
        }

        popupCancelButton.setOnClickListener {
            alertDialog.dismiss()
        }
    }
    private fun inAppFeedback(context: Context, message: String) {

        var inAppFeedbackDialog: InAppFeedbackDialog? = null
        var currentVisit: Int =
            LocalSharedPrefStorage(this@MySubscriptionsActivity).getInt(LocalSharedPrefStorage.PREF_BUNDLE_PAGE)
        currentVisit += 1
        val inAppSurvey: InAppSurvey? = UserDataManager.getInAppSurvey()
        if ((inAppSurvey?.data) != null) {
            val surveys: Surveys? =
                inAppSurvey.data.findSurvey(ConstantsUtility.InAppSurveyConstants.BUNDLE_PAGE)
            if (surveys != null) {
                inAppFeedbackDialog = InAppFeedbackDialog(context, object : OnClickSurveySubmit {
                    override fun onClickSubmit(
                        uploadSurvey: UploadSurvey,
                        onTransactionComplete: String,
                        updateListOnSuccessfullySurvey: UpdateListOnSuccessfullySurvey?
                    ) {
                        mViewModel.uploadInAppSurvey(
                            this@MySubscriptionsActivity,
                            uploadSurvey,
                            onTransactionComplete,
                            updateListOnSuccessfullySurvey
                        )
                        if (inAppFeedbackDialog?.isShowing == true) {
                            inAppFeedbackDialog?.dismiss()
                        }
                    }

                })
                if (surveys.questions != null) {
                    currentVisit = 0
                    inAppFeedbackDialog.showTitleWithMessageDialog(surveys,resources.getString(R.string.popup_note_title),message)
                } else {
                    showMessageDialog(
                        applicationContext, this@MySubscriptionsActivity,
                        resources.getString(R.string.popup_note_title),
                        message
                    )
                }
            } else {
                showMessageDialog(
                    applicationContext, this@MySubscriptionsActivity,
                    resources.getString(R.string.popup_note_title),
                    message
                )
            }
        } else {
            showMessageDialog(
                applicationContext, this@MySubscriptionsActivity,
                resources.getString(R.string.popup_note_title),
                message
            )
        }
        LocalSharedPrefStorage(this@MySubscriptionsActivity).addInt(
            LocalSharedPrefStorage.PREF_BUNDLE_PAGE,
            currentVisit
        )
    }
    private fun initMySubscriptionEvents() {
        val mySubscriptionEvents = object : MySubscriptionEvents {
            override fun onMySubscriptionOfferProcessed(
                offeringName: String,
                offeringId: String,
                actionType: String
            ) {
                if (!this@MySubscriptionsActivity.isFinishing) {
                    logE(
                        "MySubXEr",
                        "name:::$offeringName".plus(" id:::$offeringId")
                            .plus(" actionType:::$actionType"),
                        fromClass,
                        "initMySubscriptionEvents"
                    )
                    showConfirmationPopup(offeringName, offeringId, actionType)
                }
            }
        }

        RootValues.setMySubscriptionEvents(mySubscriptionEvents)

        /** subscribe to the observers for the api called above*/
        val subscribeToMySubscriptionProcessedResponseLiveData =
            object : Observer<MySubscriptionsSuccessAfterOfferSubscribeResponse> {
                override fun onChanged(t: MySubscriptionsSuccessAfterOfferSubscribeResponse?) {
                    if (!this@MySubscriptionsActivity.isFinishing) {
                        if (t != null && t.data != null && t.data.mySubscriptionsData != null) {

                            if (t.data.message != null && hasValue(t.data.message)) {
                                inAppFeedback(this@MySubscriptionsActivity,t.data.message)
//                                showMessageDialog(
//                                    applicationContext, this@MySubscriptionsActivity,
//                                    resources.getString(R.string.popup_note_title),
//                                    t.data.message
//                                )
                            }

                            /** update the mysubscriptions response*/
                            if (mySubscriptionResponse != null) {

                                if (mySubscriptionResponse!!.data != null &&
                                    t.data.mySubscriptionsData.internet != null
                                ) {
                                    mySubscriptionResponse!!.data!!.internet =
                                        t.data.mySubscriptionsData.internet
                                }

                                if (mySubscriptionResponse!!.data != null &&
                                    t.data.mySubscriptionsData.sms != null
                                ) {
                                    mySubscriptionResponse!!.data!!.sms =
                                        t.data.mySubscriptionsData.sms
                                }

                                if (mySubscriptionResponse!!.data != null &&
                                    t.data.mySubscriptionsData.call != null
                                ) {
                                    mySubscriptionResponse!!.data!!.call =
                                        t.data.mySubscriptionsData.call
                                }

                                if (mySubscriptionResponse!!.data != null &&
                                    t.data.mySubscriptionsData.campaign != null
                                ) {
                                    mySubscriptionResponse!!.data!!.campaign =
                                        t.data.mySubscriptionsData.campaign
                                }

                                if (mySubscriptionResponse!!.data != null &&
                                    t.data.mySubscriptionsData.hybrid != null
                                ) {
                                    mySubscriptionResponse!!.data!!.hybrid =
                                        t.data.mySubscriptionsData.hybrid
                                }

                                if (mySubscriptionResponse!!.data != null &&
                                    t.data.mySubscriptionsData.voiceInclusiveOffers != null
                                ) {
                                    mySubscriptionResponse!!.data!!.voiceInclusiveOffers =
                                        t.data.mySubscriptionsData.voiceInclusiveOffers
                                }

                                if (mySubscriptionResponse!!.data != null &&
                                    t.data.mySubscriptionsData.roaming != null
                                ) {
                                    mySubscriptionResponse!!.data!!.roaming =
                                        t.data.mySubscriptionsData.roaming
                                }

                                if (mySubscriptionResponse!!.data != null &&
                                    t.data.mySubscriptionsData.smsInclusiveOffers != null
                                ) {
                                    mySubscriptionResponse!!.data!!.smsInclusiveOffers =
                                        t.data.mySubscriptionsData.smsInclusiveOffers
                                }

                                if (mySubscriptionResponse!!.data != null &&
                                    t.data.mySubscriptionsData.tm != null
                                ) {
                                    mySubscriptionResponse!!.data!!.tm =
                                        t.data.mySubscriptionsData.tm
                                }

                                if (mySubscriptionResponse!!.data != null &&
                                    t.data.mySubscriptionsData.internetInclusiveOffers != null
                                ) {
                                    mySubscriptionResponse!!.data!!.internetInclusiveOffers =
                                        t.data.mySubscriptionsData.internetInclusiveOffers
                                }
                            }

                            /**notify the data adapter in each fragment of this activity*/
                            RootValues.getMySubscriptionAdapterEvents()
                                .onMySubscriptionsSuccessReponse()
                        }
                    }
                }
            }

        mViewModel.subscribeToMySubscriptionProcessedResponseLiveData.observe(
            this@MySubscriptionsActivity,
            subscribeToMySubscriptionProcessedResponseLiveData
        )

        val subscribeToMySubscriptionProcessedErrorResponseLiveData =
            object : Observer<AzerAPIErrorHelperResponse> {
                override fun onChanged(response: AzerAPIErrorHelperResponse?) {
                    if (!this@MySubscriptionsActivity.isFinishing) {
                        //check internet
                        if (!isNetworkAvailable(this@MySubscriptionsActivity)) {
                            showMessageDialog(
                                applicationContext, this@MySubscriptionsActivity,
                                resources.getString(R.string.popup_note_title),
                                resources.getString(R.string.message_no_internet)
                            )
                        } else {
                            /**check the error code
                             * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                             * else show a general api failure popup*/
                            if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                                showMessageDialog(
                                    applicationContext, this@MySubscriptionsActivity,
                                    this@MySubscriptionsActivity.resources.getString(R.string.popup_error_title),
                                    response.errorDetail.toString()
                                )
                            } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                                //force logout the user here
                                logOut(
                                    applicationContext,
                                    this@MySubscriptionsActivity,
                                    false,
                                    fromClass,
                                    "initSupplementaryOffersCardAdapterEventsa34324"
                                )
                            } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                                //handle the ui in this case
                                //hide or show the no data found layout in this case
                                showMessageDialog(
                                    applicationContext, this@MySubscriptionsActivity,
                                    this@MySubscriptionsActivity.resources.getString(R.string.popup_error_title),
                                    this@MySubscriptionsActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                                showMessageDialog(
                                    applicationContext, this@MySubscriptionsActivity,
                                    this@MySubscriptionsActivity.resources.getString(R.string.popup_error_title),
                                    this@MySubscriptionsActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            } else {
                                showMessageDialog(
                                    applicationContext, this@MySubscriptionsActivity,
                                    this@MySubscriptionsActivity.resources.getString(R.string.popup_error_title),
                                    this@MySubscriptionsActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            }
                        }
                    }// if (activity != null && !this@MySubscriptionsActivity.isFinishing)  ends
                }
            }

        mViewModel.subscribeToMySubscriptionProcessedErrorResponseLiveData.observe(
            this@MySubscriptionsActivity,
            subscribeToMySubscriptionProcessedErrorResponseLiveData
        )
    }//initMySubscriptionEvents ends

    private fun initIntentData() {
        //get the extras data
        if (intent.extras != null) {
            val bundle = intent.extras!!
            if (bundle.containsKey(ConstantsUtility.PackagesConstants.DASHBOARD_INTENT_DATA)) {
                dashboardRedirectionKey =
                    bundle.getString(ConstantsUtility.PackagesConstants.DASHBOARD_INTENT_DATA)!!
            }

            if (bundle.containsKey(ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA)
                && hasValue(bundle.getString(ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA))
            ) {
                offeringId =
                    bundle.getString(ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA)!!
            }

            if (bundle.containsKey(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA)
                && hasValue(bundle.getString(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA))
            ) {
                dashboardRedirectionKey =
                    bundle.getString(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA)!!
            }
        }
        logE(
            ConstantsUtility.DynamicLinkConstants.DYNAMIC_LINK_TAG,
            "actionIdFromServer = $offeringId" +
                    "dashboardRedirectionKey = $dashboardRedirectionKey",
            fromClass, "initIntentData"
        )
    }

    private fun requestMySubscriptions() {
        /* */
        /**get the cached response from the local preferences*//*
        val cachedMySubscriptionResponse = getMySubscriptionsFromLocalPreferences()
        if (cachedMySubscriptionResponse.data != null && hasValue(cachedMySubscriptionResponse.data.toString())) {
//            setupTabLayout(cachedMySubscriptionResponse)
//            initViewPager(cachedMySubscriptionResponse)
            */
        /**call the my subscriptions api in the background to get the updated data*//*
            mViewModel.requestMySubscriptionsInBackground()
            showContent()
        } else {*/
        if (isNetworkAvailable(applicationContext)) {
            Handler().postDelayed({ mViewModel.requestMySubscriptions() }, 50L)
        } else {
            val cachedMySubscriptionResponse = getMySubscriptionsFromLocalPreferences()
            if (cachedMySubscriptionResponse.data != null && hasValue(cachedMySubscriptionResponse.data.toString())) {
                setupTabLayout(cachedMySubscriptionResponse)
                initViewPager(cachedMySubscriptionResponse)
                showContent()
            } else {
                showMessageDialog(
                    applicationContext, this@MySubscriptionsActivity,
                    "", resources.getString(R.string.message_no_internet)
                )
            }
//            }
        }
    }//requestMySubscriptions ends

    private fun initUI() {
    }//initUI ends

    private fun initUIEvents() {
        azerActionBarBackIcon.setOnClickListener {
            onBackPressed()
        }//azerActionBarBackIcon.setOnClickListener  ends

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                if (tab != null && tab.customView != null) {
                    val tabTextView = tab.customView as TextView
                    setUnSelectedTabFont(applicationContext, tabTextView)
                }
            }//onTabUnselected ends

            override fun onTabSelected(tab: TabLayout.Tab?) {
                subscriptionsViewpager.setCurrentItem(tab!!.position, false)
                if (tab.customView != null) {
                    val tabTextView = tab.customView as TextView
                    setSelectedTabFont(applicationContext, tabTextView)
                }
            }//onTabSelected ends
        })//tabLayout.addOnTabSelectedListener ends
    }//initUIEvents ends

    private fun subscribe() {
        val mySubscriptionsBackgroundResponseLiveData = object : Observer<MySubscriptionResponse> {
            override fun onChanged(response: MySubscriptionResponse?) {
                if (!this@MySubscriptionsActivity.isFinishing) {
                    if (response != null && response.data != null) {
                        mySubscriptionResponse = null
                        mySubscriptionResponse = response
                        setupTabLayout(mySubscriptionResponse!!)
                        setMySubscriptionsDataToLocalPreferences(response)
                        if (subscriptionsOffersPagerAdapter != null) {
                            subscriptionsOffersPagerAdapter!!.notifyDataSetChanged()
                        }
                        /* */
                        /**notify the adapter on the pagger fragment*//*
                    if (RootValues.getMySubscriptionsAdapterUpdateEvents() != null) {
                        RootValues.getMySubscriptionsAdapterUpdateEvents()!!.onAdapterNeedToBeNotify()
                    }*/
                    }
                }
            }
        }

        mViewModel.mySubscriptionsBackgroundResponseLiveData.observe(
            this@MySubscriptionsActivity,
            mySubscriptionsBackgroundResponseLiveData
        )

        val mySubscriptionsResponseLiveData = object : Observer<MySubscriptionResponse> {
            override fun onChanged(response: MySubscriptionResponse?) {
                if (!this@MySubscriptionsActivity.isFinishing) {
                    if (response != null && response.data != null) {
                        mySubscriptionResponse = null
                        mySubscriptionResponse = response
                        /**save the response in the local preferences*/
                        setMySubscriptionsDataToLocalPreferences(response)
                        setupTabLayout(mySubscriptionResponse!!)
                        initViewPager(mySubscriptionResponse!!)
                        showContent()
                    } else {
                        hideContent()
                    }
                }
            }
        }

        mViewModel.mySubscriptionsResponseLiveData.observe(
            this@MySubscriptionsActivity,
            mySubscriptionsResponseLiveData
        )

        val mySubscriptionsErrorResponseLiveData = object : Observer<AzerAPIErrorHelperResponse> {
            override fun onChanged(response: AzerAPIErrorHelperResponse?) {
                if (!this@MySubscriptionsActivity.isFinishing) {
                    //check internet
                    hideContent()
                    if (!isNetworkAvailable(this@MySubscriptionsActivity)) {
                        showMessageDialog(
                            applicationContext, this@MySubscriptionsActivity,
                            resources.getString(R.string.popup_note_title),
                            resources.getString(R.string.message_no_internet)
                        )
                    } else {
                        /**check the error code
                         * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                         * else show a general api failure popup*/
                        if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                            showMessageDialog(
                                applicationContext, this@MySubscriptionsActivity,
                                this@MySubscriptionsActivity.resources.getString(R.string.popup_error_title),
                                response.errorDetail.toString()
                            )
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                            //force logout the user here
                            logOut(
                                applicationContext,
                                this@MySubscriptionsActivity,
                                false,
                                fromClass,
                                "specialOffersErrorResponseData2321sf"
                            )
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                            //handle the ui in this case
                            //hide or show the no data found layout in this case
                            showMessageDialog(
                                applicationContext, this@MySubscriptionsActivity,
                                this@MySubscriptionsActivity.resources.getString(R.string.popup_error_title),
                                this@MySubscriptionsActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                            showMessageDialog(
                                applicationContext, this@MySubscriptionsActivity,
                                this@MySubscriptionsActivity.resources.getString(R.string.popup_error_title),
                                this@MySubscriptionsActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                        } else {
                            showMessageDialog(
                                applicationContext, this@MySubscriptionsActivity,
                                this@MySubscriptionsActivity.resources.getString(R.string.popup_error_title),
                                this@MySubscriptionsActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                        }
                    }
                }
            }
        }

        mViewModel.mySubscriptionsErrorResponseLiveData.observe(
            this@MySubscriptionsActivity,
            mySubscriptionsErrorResponseLiveData
        )

        val inAppSurveyResponseErrorResponseData =
            Observer<AzerAPIErrorHelperResponse> { response ->
                if (!isNetworkAvailable(this@MySubscriptionsActivity)) {
                    showMessageDialog(
                        this@MySubscriptionsActivity, this@MySubscriptionsActivity,
                        resources.getString(R.string.popup_note_title),
                        resources.getString(R.string.message_no_internet)
                    )
                } else {
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                        showMessageDialog(
                            this@MySubscriptionsActivity, this@MySubscriptionsActivity,
                            this@MySubscriptionsActivity.resources.getString(R.string.popup_error_title),
                            response.errorDetail.toString()
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                        //force logout the user here
                        logOut(
                            this@MySubscriptionsActivity,
                            this@MySubscriptionsActivity,
                            false,
                            fromClass,
                            "initSupplementaryOffersCardAdapterEventsa34324"
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                        //handle the ui in this case
                        //hide or show the no data found layout in this case
                        showMessageDialog(
                            this@MySubscriptionsActivity, this@MySubscriptionsActivity,
                            this@MySubscriptionsActivity.resources.getString(R.string.popup_error_title),
                            this@MySubscriptionsActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                        showMessageDialog(
                            this@MySubscriptionsActivity, this@MySubscriptionsActivity,
                            this@MySubscriptionsActivity.resources.getString(R.string.popup_error_title),
                            this@MySubscriptionsActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    } else {
                        showMessageDialog(
                            this@MySubscriptionsActivity, this@MySubscriptionsActivity,
                            this@MySubscriptionsActivity.resources.getString(R.string.popup_error_title),
                            this@MySubscriptionsActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    }
                }
            }
        mViewModel.inAppSurveyResponseErrorResponseData.observe(
            this@MySubscriptionsActivity,
            inAppSurveyResponseErrorResponseData
        )
    }//subscribe ends

    private fun setupTabLayout(response: MySubscriptionResponse) {
        if (response.data == null) {
            return //safe passage
        }
        removeTabs()

        /**call tab 0*/
        if (response.data!!.call != null) {
            tabLayout.addTab(
                tabLayout.newTab()
                    .setText(resources.getString(R.string.my_subscriptions_call_tab_title))
            )
            tabNames.add(resources.getString(R.string.my_subscriptions_call_tab_title))
        }

        /**internet tab 1*/
        if (response.data!!.internet != null) {
            tabLayout.addTab(
                tabLayout.newTab()
                    .setText(resources.getString(R.string.my_subscriptions_internet_tab_title))
            )
            tabNames.add(resources.getString(R.string.my_subscriptions_internet_tab_title))
        }

        /**sms tab 2*/
        if (response.data!!.sms != null) {
            tabLayout.addTab(
                tabLayout.newTab()
                    .setText(resources.getString(R.string.my_subscriptions_sms_tab_title))
            )
            tabNames.add(resources.getString(R.string.my_subscriptions_sms_tab_title))
        }

        /**hybrid tab 3*/
        if (response.data!!.hybrid != null) {
            tabLayout.addTab(
                tabLayout.newTab()
                    .setText(resources.getString(R.string.my_subscriptions_hybrid_tab_title))
            )
            tabNames.add(resources.getString(R.string.my_subscriptions_hybrid_tab_title))
        }

        /**roaming tab 4*/
        if (response.data!!.roaming != null) {
            tabLayout.addTab(
                tabLayout.newTab()
                    .setText(resources.getString(R.string.my_subscriptions_roaming_tab_title))
            )
            tabNames.add(resources.getString(R.string.my_subscriptions_roaming_tab_title))
        }

        /* */
        /**campaign tab 5*//*
        if (response.data!!.campaign != null) {
            tabLayout.addTab(tabLayout.newTab().setText(resources.getString(R.string.my_subscriptions_campaign_tab_title)))
        }

        */
        /**tm tab 6*//*
        if (response.data!!.tm != null) {
            tabLayout.addTab(tabLayout.newTab().setText(resources.getString(R.string.my_subscriptions_tm_tab_title)))
        }*/

        logE(
            "dashboardRedirectionKey",
            "dashboardRedirectionKey:::".plus(dashboardRedirectionKey),
            fromClass,
            "setupTabLayout"
        )

        if (hasValue(dashboardRedirectionKey)) {
            if (dashboardRedirectionKey.equals(
                    ConstantsUtility.ResourceTypeConstants.RESOURCE_TYPE_VOICE,
                    ignoreCase = true
                ) &&
                response.data!!.voiceInclusiveOffers != null &&
                response.data!!.voiceInclusiveOffers!!.offers != null &&
                !response.data!!.voiceInclusiveOffers!!.offers!!.isNullOrEmpty()
            ) {
                /**all inclusive tab*/
                tabLayout.addTab(
                    tabLayout.newTab()
                        .setText(resources.getString(R.string.my_subscriptions_all_inclusive_tab_title))
                )
                tabNames.add(resources.getString(R.string.my_subscriptions_all_inclusive_tab_title))
                redirectionIndex = 5
                logE(
                    "redirectionIndexMX",
                    "voiceInclusiveOffers NOT NULLLL:::".plus(response.data!!.voiceInclusiveOffers.toString()),
                    fromClass,
                    "showingIndex"
                )
            } else if (dashboardRedirectionKey.equals(
                    ConstantsUtility.ResourceTypeConstants.RESOURCE_TYPE_SMS,
                    ignoreCase = true
                )
                && response.data!!.smsInclusiveOffers != null &&
                response.data!!.smsInclusiveOffers!!.offers != null &&
                response.data!!.smsInclusiveOffers!!.offers!!.isNotEmpty()
            ) {
                /**all inclusive tab*/
                tabLayout.addTab(
                    tabLayout.newTab()
                        .setText(resources.getString(R.string.my_subscriptions_all_inclusive_tab_title))
                )
                tabNames.add(resources.getString(R.string.my_subscriptions_all_inclusive_tab_title))
                redirectionIndex = 5
                logE(
                    "redirectionIndexMX",
                    "smsInclusiveOffers NOT NULLLL:::".plus(redirectionIndex),
                    fromClass,
                    "showingIndex"
                )
            } else if ((dashboardRedirectionKey.equals(
                    ConstantsUtility.ResourceTypeConstants.RESOURCE_TYPE_DATA,
                    ignoreCase = true
                )
                        || dashboardRedirectionKey.equals(
                    ConstantsUtility.ResourceTypeConstants.RESOURCE_TYPE_DATA,
                    ignoreCase = true
                ))
                && response.data!!.internetInclusiveOffers != null &&
                response.data!!.internetInclusiveOffers!!.offers != null &&
                response.data!!.internetInclusiveOffers!!.offers!!.isNotEmpty()
            ) {
                /**all inclusive tab*/
                tabLayout.addTab(
                    tabLayout.newTab()
                        .setText(resources.getString(R.string.my_subscriptions_all_inclusive_tab_title))
                )
                tabNames.add(resources.getString(R.string.my_subscriptions_all_inclusive_tab_title))
                redirectionIndex = 5
                logE(
                    "redirectionIndexMX",
                    "internetInclusiveOffers NOT NULLLL:::".plus(redirectionIndex),
                    fromClass,
                    "showingIndex"
                )
            } else {
                logE(
                    "dashboardRedirectionKey",
                    "elseee:::".plus(dashboardRedirectionKey),
                    fromClass,
                    "setupTabLayout"
                )

                if (dashboardRedirectionKey.equals(
                        ConstantsUtility.ResourceTypeConstants.RESOURCE_TYPE_DATA,
                        ignoreCase = true
                    )
                    || dashboardRedirectionKey.equals(
                        ConstantsUtility.ResourceTypeConstants.RESOURCE_TYPE_INTERNET,
                        ignoreCase = true
                    )
                ) {
                    redirectionIndex = 1
                } else if (dashboardRedirectionKey.equals(
                        ConstantsUtility.ResourceTypeConstants.RESOURCE_TYPE_SMS,
                        ignoreCase = true
                    )
                ) {
                    logE(
                        "dashboardRedirectionKey",
                        "resourceType:::".plus(dashboardRedirectionKey),
                        fromClass,
                        "setupTabLayout"
                    )
                    redirectionIndex = 2
                } else if (dashboardRedirectionKey.equals(
                        ConstantsUtility.ResourceTypeConstants.RESOURCE_TYPE_CALLS,
                        ignoreCase = true
                    )
                    || dashboardRedirectionKey.equals(
                        ConstantsUtility.ResourceTypeConstants.RESOURCE_TYPE_CALLS,
                        ignoreCase = true
                    )
                ) {
                    logE(
                        "dashboardRedirectionKey",
                        "resourceType:::".plus(dashboardRedirectionKey),
                        fromClass,
                        "setupTabLayout"
                    )
                    redirectionIndex = 0
                } else if (dashboardRedirectionKey.equals(
                        ConstantsUtility.ResourceTypeConstants.RESOURCE_TYPE_ROAMING,
                        ignoreCase = true
                    )
                ) {
                    /**all inclusive tab*/
                    redirectionIndex = 4
                } else if (dashboardRedirectionKey.equals(
                        ConstantsUtility.ResourceTypeConstants.RESOURCE_TYPE_BONUSES,
                        ignoreCase = true
                    )
                ) {
                    /**all inclusive tab*/
                    redirectionIndex = 3
                } else {
                    redirectionIndex = 0
                }
            }
        } else {
            if (hasValue(offeringId)) {
                redirectionIndex = 0
                redirectionIndex = getRedirectionIndexFromOfferingId(offeringId, response)
            }
        }

        //set the tab by index
        tabLayout.getTabAt(redirectionIndex)!!.select()

        logE("redirectionIndexMX", "Index:::".plus(redirectionIndex), fromClass, "showingIndex")

        for (i in 0 until tabLayout.tabCount) {
            val tab = (tabLayout.getChildAt(0) as ViewGroup).getChildAt(i)
            val p = tab.layoutParams as ViewGroup.MarginLayoutParams
            val margin = (15 * resources.displayMetrics.density)
            p.setMargins(0, 0, margin.toInt(), 0)
            tab.requestLayout()

            //set the custom view
            val tabView = tabLayout.getTabAt(i)
            if (tabView != null) {
                val tabTextView = TextView(this@MySubscriptionsActivity)
                tabView.customView = tabTextView
                //set the text
                tabTextView.text = tabView.text
                //set the currently selected tab font
                if (redirectionIndex == i) {
                    setSelectedTabFont(applicationContext, tabTextView)
                } else {
                    setUnSelectedTabFont(applicationContext, tabTextView)
                }
            }
        }

        gotoTabByIndex(redirectionIndex)
    }//setupTabLayout ends

    private fun initViewPager(offers: MySubscriptionResponse) {
        val paggerFragmentsList = ArrayList<androidx.fragment.app.Fragment>()
        var foundTabPosFromOffering = -1

        if (tabLayout != null && tabLayout.tabCount > 0) {
            for (its in 0..tabLayout.tabCount) {
                if (getString(R.string.supplementary_offers_call_tab_title).equals(
                        getCurrentSelectedTabName(its),
                        ignoreCase = true
                    )
                    && offers.data != null
                    && offers.data!!.call != null
                    && offers.data!!.call!!.offers != null
                ) {

                    val foundOfferItemFromOfferingId =
                        findOfferIdAfterRedirecting(
                            offeringId,
                            offers.data!!.call!!.offers as java.util.ArrayList<Subscriptions?>
                        )
                    if (foundOfferItemFromOfferingId != -1) {
                        foundTabPosFromOffering = 0
                    }

                    logE("pacClick", "calls populated", fromClass, "initViewPager")
                    paggerFragmentsList.add(
                        SubscriptionOffersPaggerFragment.getInstance(
                            0,
                            foundOfferItemFromOfferingId,
                            getCurrentSelectedTabName(its),
                            offers.data!!.call!!.offers!!
                        )
                    )
                } else if (getString(R.string.supplementary_offers_internet_tab_title).equals(
                        getCurrentSelectedTabName(its),
                        ignoreCase = true
                    )
                    && offers.data != null
                    && offers.data!!.internet != null
                    && offers.data!!.internet!!.offers != null
                ) {

                    val foundOfferItemFromOfferingId =
                        findOfferIdAfterRedirecting(
                            offeringId,
                            offers.data!!.internet!!.offers as java.util.ArrayList<Subscriptions?>
                        )
                    if (foundOfferItemFromOfferingId != -1) {
                        foundTabPosFromOffering = 1
                    }

                    logE("pacClick", "internet populated", fromClass, "initViewPager")
                    paggerFragmentsList.add(
                        SubscriptionOffersPaggerFragment.getInstance(
                            1,
                            foundOfferItemFromOfferingId,
                            getCurrentSelectedTabName(its),
                            offers.data!!.internet!!.offers!!
                        )
                    )
                } else if (getString(R.string.supplementary_offers_sms_tab_title).equals(
                        getCurrentSelectedTabName(its),
                        ignoreCase = true
                    )
                    && offers.data != null
                    && offers.data!!.sms != null
                    && offers.data!!.sms!!.offers != null
                ) {

                    val foundOfferItemFromOfferingId =
                        findOfferIdAfterRedirecting(
                            offeringId,
                            offers.data!!.sms!!.offers as java.util.ArrayList<Subscriptions?>
                        )
                    if (foundOfferItemFromOfferingId != -1) {
                        foundTabPosFromOffering = 2
                    }

                    logE("pacClick", "sms populated", fromClass, "initViewPager")
                    paggerFragmentsList.add(
                        SubscriptionOffersPaggerFragment.getInstance(
                            2,
                            foundOfferItemFromOfferingId,
                            getCurrentSelectedTabName(its),
                            offers.data!!.sms!!.offers!!
                        )
                    )
                } /*else if (getString(R.string.supplementary_offers_campaign_tab_title).equals(getCurrentSelectedTab(its), ignoreCase = true)
                        && offers.data != null
                        && offers.data!!.campaign != null
                        && offers.data!!.campaign!!.offers != null) {

                    logE("pacClick", "campaign populated", fromClass, "initViewPager")
                    paggerFragmentsList.add(SubscriptionOffersPaggerFragment.getInstance(getCurrentSelectedTab(its)!!, offers.data!!.campaign!!.offers!!))
                }*/  /*else if (getString(R.string.supplementary_offers_tm_tab_title).equals(getCurrentSelectedTab(its), ignoreCase = true)
                        && offers.data != null
                        && offers.data!!.tm != null
                        && offers.data!!.tm!!.offers != null) {

                    logE("pacClick", "tm populated", fromClass, "initViewPager")
                    paggerFragmentsList.add(SubscriptionOffersPaggerFragment.getInstance(getCurrentSelectedTab(its)!!, offers.data!!.tm!!.offers!!))
                } */ else if (getString(R.string.supplementary_offers_hybrid_tab_title).equals(
                        getCurrentSelectedTabName(its),
                        ignoreCase = true
                    )
                    && offers.data != null
                    && offers.data!!.hybrid != null
                    && offers.data!!.hybrid!!.offers != null
                ) {

                    val foundOfferItemFromOfferingId =
                        findOfferIdAfterRedirecting(
                            offeringId,
                            offers.data!!.hybrid!!.offers as java.util.ArrayList<Subscriptions?>
                        )
                    if (foundOfferItemFromOfferingId != -1) {
                        foundTabPosFromOffering = 3
                    }

                    logE("pacClick", "hybrid populated", fromClass, "initViewPager")
                    paggerFragmentsList.add(
                        SubscriptionOffersPaggerFragment.getInstance(
                            3,
                            foundOfferItemFromOfferingId,
                            getCurrentSelectedTabName(its),
                            offers.data!!.hybrid!!.offers!!
                        )
                    )
                } else if (getString(R.string.supplementary_offers_roaming_tab_title).equals(
                        getCurrentSelectedTabName(its),
                        ignoreCase = true
                    )
                    && offers.data != null
                    && offers.data!!.roaming != null
                    && offers.data!!.roaming!!.offers != null
                ) {

                    val foundOfferItemFromOfferingId =
                        findOfferIdAfterRedirecting(
                            offeringId,
                            offers.data!!.roaming!!.offers as java.util.ArrayList<Subscriptions?>
                        )
                    if (foundOfferItemFromOfferingId != -1) {
                        foundTabPosFromOffering = 4
                    }

                    logE("pacClick", "roaming populated", fromClass, "initViewPager")
                    paggerFragmentsList.add(
                        SubscriptionOffersPaggerFragment.getInstance(
                            4,
                            foundOfferItemFromOfferingId,
                            getCurrentSelectedTabName(its),
                            offers.data!!.roaming!!.offers!!
                        )
                    )
                } else if (resources.getString(R.string.my_subscriptions_all_inclusive_tab_title)
                        .equals(getCurrentSelectedTabName(its), ignoreCase = true)
                ) {
                    if (dashboardRedirectionKey.equals(
                            ConstantsUtility.ResourceTypeConstants.RESOURCE_TYPE_DATA,
                            ignoreCase = true
                        )
                    ) {
                        if (offers.data!!.internetInclusiveOffers != null &&
                            offers.data!!.internetInclusiveOffers!!.offers != null &&
                            !offers.data!!.internetInclusiveOffers!!.offers!!.isNullOrEmpty()
                        ) {

                            val foundOfferItemFromOfferingId =
                                findOfferIdAfterRedirecting(
                                    offeringId,
                                    offers.data!!.internetInclusiveOffers!!.offers as java.util.ArrayList<Subscriptions?>
                                )
                            if (foundOfferItemFromOfferingId != -1) {
                                foundTabPosFromOffering = 5
                            }

                            paggerFragmentsList.add(
                                SubscriptionOffersPaggerFragment.getInstance(
                                    5,
                                    foundOfferItemFromOfferingId,
                                    getCurrentSelectedTabName(its),
                                    offers.data!!.internetInclusiveOffers!!.offers!!
                                )
                            )
                        }
                    } else if (dashboardRedirectionKey.equals(
                            ConstantsUtility.ResourceTypeConstants.RESOURCE_TYPE_SMS,
                            ignoreCase = true
                        )
                    ) {
                        if (offers.data!!.smsInclusiveOffers != null &&
                            offers.data!!.smsInclusiveOffers!!.offers != null &&
                            !offers.data!!.smsInclusiveOffers!!.offers!!.isNullOrEmpty()
                        ) {

                            val foundOfferItemFromOfferingId =
                                findOfferIdAfterRedirecting(
                                    offeringId,
                                    offers.data!!.smsInclusiveOffers!!.offers as java.util.ArrayList<Subscriptions?>
                                )
                            if (foundOfferItemFromOfferingId != -1) {
                                foundTabPosFromOffering = 5
                            }

                            paggerFragmentsList.add(
                                SubscriptionOffersPaggerFragment.getInstance(
                                    5,
                                    foundOfferItemFromOfferingId,
                                    getCurrentSelectedTabName(its),
                                    offers.data!!.smsInclusiveOffers!!.offers!!
                                )
                            )
                        }
                    } else {
                        if (offers.data!!.voiceInclusiveOffers != null &&
                            offers.data!!.voiceInclusiveOffers!!.offers != null &&
                            !offers.data!!.voiceInclusiveOffers!!.offers!!.isNullOrEmpty()
                        ) {

                            val foundOfferItemFromOfferingId =
                                findOfferIdAfterRedirecting(
                                    offeringId,
                                    offers.data!!.voiceInclusiveOffers!!.offers as java.util.ArrayList<Subscriptions?>
                                )
                            if (foundOfferItemFromOfferingId != -1) {
                                foundTabPosFromOffering = 5
                            }

                            paggerFragmentsList.add(
                                SubscriptionOffersPaggerFragment.getInstance(
                                    5,
                                    foundOfferItemFromOfferingId,
                                    getCurrentSelectedTabName(its),
                                    offers.data!!.voiceInclusiveOffers!!.offers!!
                                )
                            )
                        }
                    }
                }
            }//for ends
        }//if(tabLayout!=null&&tabLayout.tabCount>0) ends

        subscriptionsOffersPagerAdapter =
            SubscriptionsOffersPagerAdapter(supportFragmentManager, paggerFragmentsList)
        subscriptionsViewpager.adapter = subscriptionsOffersPagerAdapter
        if (foundTabPosFromOffering != -1 && hasValue(offeringId)) {
            redirectionIndex = foundTabPosFromOffering
        }
        subscriptionsViewpager.currentItem = redirectionIndex
        tabLayout.getTabAt(redirectionIndex)!!.select()
        gotoTabByIndex(redirectionIndex)
    }//initViewPager ends

    private fun gotoTabByIndex(index: Int) {
        if (tabLayout != null && tabLayout.tabCount > 0) {
            Handler().postDelayed({
                tabLayout.getTabAt(index)!!.select()
                tabLayout.isSmoothScrollingEnabled = true
                tabLayout.setScrollPosition(index, 0f, true)
            }, 50L)
        }
    }//gotoTabByIndex ends

    private fun getCurrentSelectedTabName(index: Int): String {
        var tabName = ""

        try {
            tabName = tabNames[index]
        } catch (exp: Exception) {

        }

        return tabName
    }//getCurrentSelectedTabName ends

    private fun getRedirectionIndexFromOfferingId(
        offerId: String,
        offers: MySubscriptionResponse?
    ): Int {
        var index = 0
        if (offers?.data?.call != null && !offers.data?.call!!.offers.isNullOrEmpty()) {
            for (its in offers.data?.call!!.offers!!.indices) {
                if (offers.data?.call!!.offers!![its]!!.header != null
                    && hasValue(offers.data?.call!!.offers!![its]!!.header!!.offeringId)
                    && offers.data?.call!!.offers!![its]!!.header!!.offeringId == offerId
                ) {
                    index = 0
                    break
                }
            }
        }

        if (offers?.data?.internet != null && !offers.data?.internet!!.offers.isNullOrEmpty()) {
            for (its in offers.data?.internet!!.offers!!.indices) {
                if (offers.data?.internet!!.offers!![its]!!.header != null
                    && hasValue(offers.data?.internet!!.offers!![its]!!.header!!.offeringId)
                    && offers.data?.internet!!.offers!![its]!!.header!!.offeringId == offerId
                ) {
                    index = 1
                    break
                }
            }
        }

        if (offers?.data?.sms != null && !offers.data?.sms!!.offers.isNullOrEmpty()) {
            for (its in offers.data?.sms!!.offers!!.indices) {
                if (offers.data?.sms!!.offers!![its]!!.header != null
                    && hasValue(offers.data?.sms!!.offers!![its]!!.header!!.offeringId)
                    && offers.data?.sms!!.offers!![its]!!.header!!.offeringId == offerId
                ) {
                    index = 2
                    break
                }
            }
        }

        if (offers?.data?.hybrid != null && !offers.data?.hybrid!!.offers.isNullOrEmpty()) {
            for (its in offers.data?.hybrid!!.offers!!.indices) {
                if (offers.data?.hybrid!!.offers!![its]!!.header != null
                    && hasValue(offers.data?.hybrid!!.offers!![its]!!.header!!.offeringId)
                    && offers.data?.hybrid!!.offers!![its]!!.header!!.offeringId == offerId
                ) {
                    index = 3
                    break
                }
            }
        }

        if (offers?.data?.internetInclusiveOffers != null && !offers.data?.internetInclusiveOffers!!.offers.isNullOrEmpty()) {
            for (its in offers.data?.internetInclusiveOffers!!.offers!!.indices) {
                if (offers.data?.internetInclusiveOffers!!.offers!![its]!!.header != null
                    && hasValue(offers.data?.internetInclusiveOffers!!.offers!![its]!!.header!!.offeringId)
                    && offers.data?.internetInclusiveOffers!!.offers!![its]!!.header!!.offeringId == offerId
                ) {
                    index = 5
                    break
                }
            }
        }

        if (offers?.data?.smsInclusiveOffers != null && !offers.data?.smsInclusiveOffers!!.offers.isNullOrEmpty()) {
            for (its in offers.data?.smsInclusiveOffers!!.offers!!.indices) {
                if (offers.data?.smsInclusiveOffers!!.offers!![its]!!.header != null
                    && hasValue(offers.data?.smsInclusiveOffers!!.offers!![its]!!.header!!.offeringId)
                    && offers.data?.smsInclusiveOffers!!.offers!![its]!!.header!!.offeringId == offerId
                ) {
                    index = 5
                    break
                }
            }
        }

        if (offers?.data?.voiceInclusiveOffers != null && !offers.data?.voiceInclusiveOffers!!.offers.isNullOrEmpty()) {
            for (its in offers.data?.voiceInclusiveOffers!!.offers!!.indices) {
                if (offers.data?.voiceInclusiveOffers!!.offers!![its]!!.header != null
                    && hasValue(offers.data?.voiceInclusiveOffers!!.offers!![its]!!.header!!.offeringId)
                    && offers.data?.voiceInclusiveOffers!!.offers!![its]!!.header!!.offeringId == offerId
                ) {
                    index = 5
                    break
                }
            }
        }
        return index
    }

    private fun removeTabs() {
        if (tabLayout != null) {
            tabLayout.removeAllTabs()
        }
        tabNames.clear()//refresh it everytime when this method called
    }//removeTabs ends

    private fun hideContent() {
        mViewModel.hideLoader()
        noDataFoundLayout.visibility = View.VISIBLE
        tabLayout.visibility = View.GONE
        subscriptionsViewpager.visibility = View.GONE
    }//hideContent ends

    private fun showContent() {
        noDataFoundLayout.visibility = View.GONE
        tabLayout.visibility = View.VISIBLE
        subscriptionsViewpager.visibility = View.VISIBLE
    }//hideContent ends

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.my_subscriptions_title)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, MySubscriptionsActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends

        fun start(context: Context, offeringId: String, tabName: String) {
            val intent = Intent(context, MySubscriptionsActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            val bundle = Bundle()
            bundle.putString(ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA, offeringId)
            bundle.putString(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA, tabName)
            intent.putExtras(bundle)
            context.startActivity(intent)
        }//start ends
    }//object ends

    private fun findOfferIdAfterRedirecting(
        offeringId: String,
        offersList: ArrayList<Subscriptions?>
    ): Int {
        var foundOfferPosFromSubscription = -1
        try {
            if (hasValue(offeringId) && !offersList.isNullOrEmpty()) {
                for (i in offersList.indices) {
                    if (offersList[i] != null &&
                        offersList[i]!!.header != null &&
                        hasValue(offersList[i]!!.header!!.offeringId) &&
                        offersList[i]!!.header!!.offeringId == offeringId
                    ) {
                        foundOfferPosFromSubscription = i
                        break
                    }//if ends
                }//for ends
            }//outer if ends
        } catch (e: Exception) {
            logE(
                "Packages",
                "error:::".plus(e.toString()),
                fromClass,
                "findOfferIdAfterRedirecting"
            )
        }//catch ends

        logE(
            ConstantsUtility.DynamicLinkConstants.DYNAMIC_LINK_TAG,
            "foundOfferPosFromSubscription = $foundOfferPosFromSubscription",
            fromClass,
            "findOfferIdAfterRedirecting"
        )

        return foundOfferPosFromSubscription
    }//findOfferIdAfterRedirectingFromNotifications ends


}//class ends