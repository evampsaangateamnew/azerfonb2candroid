package com.azarphone.ui.activities.cardselection

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class CardSelectionFactory(val cardSelectionFactory: CardSelectionRepository) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CardSelectionViewModel::class.java)) {
            return CardSelectionViewModel(cardSelectionFactory) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }

}