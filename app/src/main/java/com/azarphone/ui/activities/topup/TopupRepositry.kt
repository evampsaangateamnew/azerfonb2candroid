package com.azarphone.ui.activities.topup

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.DeleteSavedCardRequest
import com.azarphone.api.pojo.request.InitiatePaymentRequest
import com.azarphone.api.pojo.request.MakePaymentRequest
import com.azarphone.api.pojo.request.TopupRequest
import com.azarphone.api.pojo.response.fasttopupresponse.deletefasttopup.DeleteItemResponse
import com.azarphone.api.pojo.response.inappsurvey.InAppSurvey
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.makepayment.MakePaymentTopUpResponse
import com.azarphone.api.pojo.response.topup.TopupResponse
import com.azarphone.api.pojo.response.topup.initiatepayment.InitiatePaymentResponse
import com.azarphone.api.pojo.response.topup.savedcards.SavedCardsResponse
import io.reactivex.Observable

/**
 * @author Junaid Hassan on 03, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class TopupRepositry {
    init {

    }

    fun requestTopUp(topupRequest: TopupRequest): Observable<TopupResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestTopUp(topupRequest)
    }

    fun requestSavedCards(): Observable<SavedCardsResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestSavedCards()
    }

    fun requestInitiatePayment(initiatePaymentRequest: InitiatePaymentRequest): Observable<InitiatePaymentResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestInitiatePayment(initiatePaymentRequest)
    }

    fun requestMakePayment(makePaymentRequest: MakePaymentRequest): Observable<MakePaymentTopUpResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().makePayment(makePaymentRequest)
    }

    fun requestDeleteTopUpCard(deleteSavedCardRequest: DeleteSavedCardRequest): Observable<DeleteItemResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().deleteSavedCard(deleteSavedCardRequest)
    }

    fun saveInAppSurvey(uploadSurvey: UploadSurvey): Observable<InAppSurvey> {
        return ApiClient.newApiClientInstance.getServerAPI().saveInAppSurvey(uploadSurvey)
    }

}