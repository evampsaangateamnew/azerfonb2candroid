package com.azarphone.ui.activities.mainactivity

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.analytics.Analytics
import com.azarphone.analytics.EventValues
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.helperpojo.HomePageFreeResourcesHelperModel
import com.azarphone.api.pojo.request.*
import com.azarphone.api.pojo.response.fcmtoken.FCMResponse
import com.azarphone.api.pojo.response.gethomepageresponse.FreeResourcesItem
import com.azarphone.api.pojo.response.gethomepageresponse.FreeResourcesRoamingItem
import com.azarphone.api.pojo.response.gethomepageresponse.GetHomePageResponse
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.internetpackresponse.InternetPacksResponse
import com.azarphone.api.pojo.response.loginappresumeresponse.LoginAppResumeResponse
import com.azarphone.api.pojo.response.logoutresponse.LogOutResponse
import com.azarphone.api.pojo.response.menuresponse.MenuResponse
import com.azarphone.api.pojo.response.mysubscriptions.successresponse.MySubscriptionsSuccessAfterOfferSubscribeResponse
import com.azarphone.api.pojo.response.rateusbeforeredirectionresponse.RateUsBeforeRedirectingToPlayStoreRespone
import com.azarphone.api.pojo.response.tariffsresponse.TariffsResponse
import com.azarphone.api.pojo.response.tariffsubscriptionresponse.TariffSubscriptionResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.UpdateListOnSuccessfullySurvey
import com.azarphone.ui.dialogs.InAppThankYouDialog
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.logE
import com.azarphone.validators.hasValue
import com.azarphone.validators.isNumeric
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable
import java.util.*

/**
 * @author Junaid Hassan on 13, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class MainViewModel(val mMainRepositry: MainRepositry) : BaseViewModel() {

    private val fromClass = "MainViewModel"
    private lateinit var disposable: Disposable
    val menuResponseData = MutableLiveData<MenuResponse>()
    val menuSilentResponseData =
        MutableLiveData<MenuResponse>()//data observer for the request menus called in the background
    val menuErrorResponseData = MutableLiveData<AzerAPIErrorHelperResponse>()

    val appResumeResponseData = MutableLiveData<LoginAppResumeResponse>()
    val appResumeErrorResponseData = MutableLiveData<AzerAPIErrorHelperResponse>()

    val fcmResponseLiveData = MutableLiveData<FCMResponse>()

    val getHomePageResponseData = MutableLiveData<GetHomePageResponse>()
    val getHomePageResponseSilentData = MutableLiveData<GetHomePageResponse>()
    val getHomePageErrorResponseData = MutableLiveData<AzerAPIErrorHelperResponse>()

    val logOutResponseLiveData = MutableLiveData<LogOutResponse>()
    val logOutResponseErrorLiveData = MutableLiveData<AzerAPIErrorHelperResponse>()

    val notificationsUnreadCountResponseLiveData = MutableLiveData<Int>()
    val getRateUsResponseLiveData = MutableLiveData<String>()
    val rateUsBeforePlayStoreRedirectionLiveDataResponse =
        MutableLiveData<RateUsBeforeRedirectingToPlayStoreRespone>()
    val tariffsResponseLiveData = MutableLiveData<TariffsResponse>()
    val tariffsSilentResponseLiveData = MutableLiveData<TariffsResponse>()
    val tariffsResponseErrorLiveData = MutableLiveData<AzerAPIErrorHelperResponse>()
    val internetResponseLiveData = MutableLiveData<InternetPacksResponse>()
    val internetResponseErrorLiveData = MutableLiveData<AzerAPIErrorHelperResponse>()

    val changeTariffResponseLiveData = MutableLiveData<TariffSubscriptionResponse>()
    val inAppSurveyResponseErrorResponseData = MutableLiveData<AzerAPIErrorHelperResponse>()

    val subscribeToSupplementaryOfferResponseLiveData =
        MutableLiveData<MySubscriptionsSuccessAfterOfferSubscribeResponse>()
    val subscribeToSupplementaryOfferErrorResponseLiveData =
        MutableLiveData<AzerAPIErrorHelperResponse>()

    val apisCalledCount = MutableLiveData<Int>()

    val surveyResponseErrorResponse = MutableLiveData<AzerAPIErrorHelperResponse>()
    val surveyResponse = MutableLiveData<AzerAPIErrorHelperResponse>()

    init {
        /*getNotifications()*/
        apisCalledCount.value = 0
    }//init ends

    fun showLoading() {
        view()?.showLoading()
    }

    fun hideLoading() {
        view()?.hideLoading()
    }

    /**all the api caller methods
     * should be here*/

    fun getSurvey() {
        var observer = mMainRepositry.getSurvey()
        observer = observer.onErrorReturn { throwable -> null }

        disposable = observer
            .compose(applyIOSchedulers())
            .subscribe(
                { appSurvey ->
                    Log.e("data_", appSurvey.toString())
                    UserDataManager.setInAppSurvey(appSurvey)
                },
                { error ->
                    surveyResponseErrorResponse.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            error.message
                        )
                    )
                }
            )
    }

    fun uploadInAppSurvey(context: Context, uploadSurvey: UploadSurvey, onTransactionComplete: String,updateListOnSuccessfullySurvey: UpdateListOnSuccessfullySurvey?= null) {
        view()?.showLoading()
        var observable = mMainRepositry.saveInAppSurvey(uploadSurvey)
        observable = observable.onErrorReturn { throwable -> null }
        disposable = observable
            .compose(applyIOSchedulers())
            .subscribe(
                { appSurvey ->
                    view()?.hideLoading()
                    when {
                        appSurvey?.data != null -> {
                            UserDataManager.setInAppSurvey(appSurvey)
                            val thankYouDialog =
                                InAppThankYouDialog(context, onTransactionComplete)
                            thankYouDialog.show()
                        }
                        appSurvey?.resultDesc != null -> {
                            inAppSurveyResponseErrorResponseData.postValue(
                                AzerAPIErrorHelperResponse(
                                    ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                    appSurvey.resultDesc
                                )
                            )
                        }
                        else -> {
                            inAppSurveyResponseErrorResponseData.postValue(
                                AzerAPIErrorHelperResponse(
                                    ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                    context.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            )
                        }
                    }
                    updateListOnSuccessfullySurvey?.updateListOnSuccessfullySurvey()
                },
                { error ->
                    inAppSurveyResponseErrorResponseData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            error.message
                        )
                    )
                }
            )
        compositeDisposables?.add(disposable)
    }

    fun requestGetHomePage(isPullToRefresh: Boolean) {
        var customerType = ""
        var brandId = ""
        var offeringId = ""
        var subscriberType = ""

        if (UserDataManager.getCustomerData() != null) {
            if (hasValue(UserDataManager.getCustomerData()!!.customerType)) {
                customerType = UserDataManager.getCustomerData()!!.customerType!!
            }

            if (hasValue(UserDataManager.getCustomerData()!!.brandId)) {
                brandId = UserDataManager.getCustomerData()!!.brandId!!
            }

            if (hasValue(UserDataManager.getCustomerData()!!.subscriberType)) {
                subscriberType = UserDataManager.getCustomerData()!!.subscriberType!!
            }

            if (hasValue(UserDataManager.getCustomerData()!!.offeringId)) {
                offeringId = UserDataManager.getCustomerData()!!.offeringId!!
            }
        }

        logE(
            "geHOmePage",
            "called::: offeringId:::".plus(offeringId),
            fromClass,
            "requestGetHomePage"
        )
        if (!isPullToRefresh) {
            view()?.showLoading()
        }
        val getHomePageRequest =
            GetHomePageRequest(customerType, brandId, offeringId, subscriberType)

        var mObserver = mMainRepositry.requestGetHomePage(getHomePageRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
            .compose(applyIOSchedulers())
            .subscribe(
                { result ->
                    view()?.hideLoading()
                    parseGetHomePageResponse(result)
                },
                { error ->
                    view()?.hideLoading()
                    getHomePageErrorResponseData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                }
            )

        compositeDisposables?.add(disposable)
    }//requestGetHomePage ends

    fun requestGetHomePageSilently(isPullToRefresh: Boolean) {
        var customerType = ""
        var brandId = ""
        var offeringId = ""
        var subscriberType = ""

        if (UserDataManager.getCustomerData() != null) {
            if (hasValue(UserDataManager.getCustomerData()!!.customerType)) {
                customerType = UserDataManager.getCustomerData()!!.customerType!!
            }

            if (hasValue(UserDataManager.getCustomerData()!!.brandId)) {
                brandId = UserDataManager.getCustomerData()!!.brandId!!
            }

            if (hasValue(UserDataManager.getCustomerData()!!.subscriberType)) {
                subscriberType = UserDataManager.getCustomerData()!!.subscriberType!!
            }

            if (hasValue(UserDataManager.getCustomerData()!!.offeringId)) {
                offeringId = UserDataManager.getCustomerData()!!.offeringId!!
            }
        }

        logE(
            "geHOmePage",
            "called::: offeringId:::".plus(offeringId),
            fromClass,
            "requestGetHomePageSilently"
        )

        val getHomePageRequest =
            GetHomePageRequest(customerType, brandId, offeringId, subscriberType)

        var mObserver = mMainRepositry.requestGetHomePage(getHomePageRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
            .compose(applyIOSchedulers())
            .subscribe(
                { result ->
                    getHomePageResponseSilentData.postValue(result)
                    logE(
                        "homePageSilent",
                        "Response:::".plus(result.toString()),
                        fromClass,
                        "requestGetHomePageSilently"
                    )
                },
                { error ->
                    logE(
                        "homePageSilent",
                        "error:::".plus(error.toString()),
                        fromClass,
                        "requestGetHomePageSilently"
                    )
                }
            )

        compositeDisposables?.add(disposable)
    }//requestGetHomePageSilently ends

    fun requestAppResume(customerId: String, entityId: String) {
        logE(
            "appResumeCalled",
            "called::: customerId:::".plus(customerId).plus(" entityid:::").plus(entityId),
            fromClass,
            "requestAppResume"
        )
        view()?.showLoading()
        val appResumeRequest = AppResumeRequest(customerId, entityId)

        var mObserver = mMainRepositry.requestAppResume(appResumeRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
            .compose(applyIOSchedulers())
            .subscribe(
                { result ->
                    view()?.hideLoading()
                    parseAppResumeResponse(result)
                },
                { error ->
                    view()?.hideLoading()
                    appResumeErrorResponseData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                    UserDataManager.setIsAppResumeCalledWithSuccess(false)
                }
            )

        compositeDisposables?.add(disposable)
    }//requestAppResume ends

    fun requestSubscribeToTariff(tariffName: String, offeringId: String, actionType: String) {
        logE(
            "tariffSubSxY",
            "tariffName:::".plus(tariffName).plus(" offeringId:::").plus(offeringId)
                .plus(" actionType:::").plus(actionType),
            fromClass,
            "requestSubscribeToTariff"
        )
        view()?.showLoading()
        val tariffSubscribeRequest = TariffSubscribeRequest(tariffName, offeringId, actionType)

        var mObserver = mMainRepositry.requestSubscribeToTariff(tariffSubscribeRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
            .compose(applyIOSchedulers())
            .subscribe(
                { result ->
                    view()?.hideLoading()
                    view()?.isErrorErrorDialoge(result)?.let {
                        if (it != true) {
                            changeTariffResponseLiveData.postValue(result)
                            Analytics.logAppEvent(
                                EventValues.TariffEvents.tariff_migration,
                                EventValues.TariffEvents.migration,
                                EventValues.GenericValues.Success
                            )
                        }
                    }
                },
                { error ->
                    view()?.hideLoading()
                    view()?.loadError(error)
                    Analytics.logAppEvent(
                        EventValues.TariffEvents.tariff_migration,
                        EventValues.TariffEvents.migration,
                        EventValues.GenericValues.Failure
                    )
                }
            )

        compositeDisposables?.add(disposable)
    }//requestSubscribeToTariff ends

    fun showLoader() {
//        view()?.showLoading()
    }

    fun hideLoader() {
//        view()?.hideLoading()
    }

    fun requestMenus(msisdn: String, offeringName: String) {
        logE("MenusCalled", "called::: msisdn:::".plus(msisdn), fromClass, "requestMenus")
        view()?.showLoading()
        val menuRequest = MenuRequest(msisdn, offeringName)

        var mObserver = mMainRepositry.requestMenus(menuRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
            .compose(applyIOSchedulers())
            .subscribe(
                { result ->
                    parseMenusResponse(result)
                },
                { error ->
                    view()?.hideLoading()
                    menuErrorResponseData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                }
            )

        compositeDisposables?.add(disposable)
    }//requestMenus ends

    fun requestRateUsBeforeRedirectingToPlayStore() {
        var entityId = ""
        if (UserDataManager.getCustomerData() != null) {
            if (UserDataManager.getCustomerData()!!.entityId != null && hasValue(UserDataManager.getCustomerData()!!.entityId)) {
                entityId = UserDataManager.getCustomerData()!!.entityId!!
            }
        }
        view()?.showLoading()
        val getRateUsRequest = GetRateUsRequest(entityId)

        var mObserver = mMainRepositry.requestRateUsBeforeRedirectingToPlayStore(getRateUsRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
            .compose(applyIOSchedulers())
            .subscribe(
                { result ->
                    view()?.hideLoading()
                    view()?.isErrorErrorDialoge(result)?.let {
                        if (it != true) {
                            rateUsBeforePlayStoreRedirectionLiveDataResponse.postValue(result)
                        }
                    }
                },
                { error ->
                    view()?.hideLoading()
                    view()?.loadError(error)
                }
            )

        compositeDisposables?.add(disposable)
    }//requestRateUsBeforeRedirectingToPlayStore ends

    fun requestGetRateUs(entityId: String) {
        logE("rateUsX", "called::: requestGetRateUs:::", fromClass, "requestGetRateUs")
        val getRateUsRequest = GetRateUsRequest(entityId)

        var mObserver = mMainRepositry.requestGetRateUs(getRateUsRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
            .compose(applyIOSchedulers())
            .subscribe(
                { result ->
                    if (result != null && result.data != null) {
                        logE(
                            "rateUsX",
                            "rate_android:::".plus(result.toString()),
                            fromClass,
                            "subscribeToGetRateResponse"
                        )
                        getRateUsResponseLiveData.postValue(result.data.rateus_android)
                    }
                },
                { error ->
                    //do nothing
                }
            )

        compositeDisposables?.add(disposable)
    }//requestGetRateUs ends

    fun requestGetNotificationsCount() {
        logE(
            "countX",
            "called::: CountForNotificaitons:::",
            fromClass,
            "requestGetNotificationsCount"
        )

        var mObserver = mMainRepositry.requestGetNotificationsCount()
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
            .compose(applyIOSchedulers())
            .subscribe(
                { result ->
                    if (result != null) {
                        if (result.data != null) {
                            if (hasValue(result.data.notificationUnreadCount)) {
                                if (isNumeric(result.data.notificationUnreadCount)) {
                                    notificationsUnreadCountResponseLiveData.postValue(result.data.notificationUnreadCount!!.toInt())
                                }
                            }
                        }
                    }
                },
                { error ->
                    //do nothing
                }
            )

        compositeDisposables?.add(disposable)
    }//requestGetNotificationsCount ends

    fun requestLogOut() {
        view()?.showLoading()

        var mObserver = mMainRepositry.requestLogOut()
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
            .compose(applyIOSchedulers())
            .subscribe(
                { result ->
                    view()?.hideLoading()

                    parseLogOutResponse(result)

                },
                { error ->
                    view()?.hideLoading()
                    logOutResponseErrorLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                }
            )

        compositeDisposables?.add(disposable)
    }//logOut ends

    /**request the app menus in the background call
     * if any new menus is added will be shown in this call
     * keep this call silent in background
     * dont show the loader at any cost
     * also dont show any error message if this call fails to consume
     * simply a silent call in the background
     * juni1289*/
    fun requestMenusInBackground(msisdn: String, offeringName: String) {
        logE(
            "MenusCalled",
            "calledInBackground::: msisdn:::".plus(msisdn),
            fromClass,
            "requestMenusInBackground"
        )
        val menuRequest = MenuRequest(msisdn, offeringName)

        var mObserver = mMainRepositry.requestMenus(menuRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
            .compose(applyIOSchedulers())
            .subscribe(
                { result ->
                    parseMenusResponseSilently(result)
                },
                { error ->
                    //do nothing
                }
            )

        compositeDisposables?.add(disposable)
    }//requestMenus ends

    fun requestGetTariffs() {
        var offeringId = ""
        var subscriberType = ""
        var specialTariffsIds = listOf<String>()
        var groupIds = ""
        if (UserDataManager.getCustomerData() != null) {
            if (hasValue(UserDataManager.getCustomerData()!!.offeringId)) {
                offeringId = UserDataManager.getCustomerData()!!.offeringId!!
            }

            if (hasValue(UserDataManager.getCustomerData()!!.subscriberType)) {
                subscriberType = UserDataManager.getCustomerData()!!.subscriberType!!
            }

            if (hasValue(UserDataManager.getCustomerData()!!.groupIds)) {
                groupIds = UserDataManager.getCustomerData()!!.groupIds!!
            }

            if (UserDataManager.getCustomerData()!!.specialOffersTariffData != null &&
                UserDataManager.getCustomerData()!!.specialOffersTariffData!!.tariffSpecialIds != null
            ) {
                specialTariffsIds =
                    UserDataManager.getCustomerData()!!.specialOffersTariffData!!.tariffSpecialIds!!
            }
        }

        //get the store id
        val storeId = ProjectApplication.mLocaleManager.getCurrentLanguageNumber()

        logE(
            "tariffsX1289Z",
            "called::: storeId:::".plus(storeId).plus(" offeringId").plus(offeringId)
                .plus(" subscriberType").plus(subscriberType),
            fromClass,
            "requestGetTariffs"
        )
        view()?.showLoading()
        val tariffsRequest =
            TariffsRequest(storeId, offeringId, subscriberType, specialTariffsIds, groupIds)

        var mObserver = mMainRepositry.requestGetTariffs(tariffsRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
            .compose(applyIOSchedulers())
            .subscribe(
                { result ->
                    parseGetTariffsResponse(result)
                },
                { error ->
                    view()?.hideLoading()
                    tariffsResponseErrorLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                }
            )

        compositeDisposables?.add(disposable)
    }//requestGetTariffs ends

    fun requestGetTariffsSilently() {
        var offeringId = ""
        var subscriberType = ""
        var groupIds = ""
        var specialTariffsIds = listOf<String>()
        if (UserDataManager.getCustomerData() != null) {
            if (hasValue(UserDataManager.getCustomerData()!!.offeringId)) {
                offeringId = UserDataManager.getCustomerData()!!.offeringId!!
            }

            if (hasValue(UserDataManager.getCustomerData()!!.subscriberType)) {
                subscriberType = UserDataManager.getCustomerData()!!.subscriberType!!
            }

            if (hasValue(UserDataManager.getCustomerData()!!.groupIds)) {
                groupIds = UserDataManager.getCustomerData()!!.groupIds!!
            }

            if (UserDataManager.getCustomerData()!!.specialOffersTariffData != null &&
                UserDataManager.getCustomerData()!!.specialOffersTariffData!!.tariffSpecialIds != null
            ) {
                specialTariffsIds =
                    UserDataManager.getCustomerData()!!.specialOffersTariffData!!.tariffSpecialIds!!
            }
        }

        //get the store id
        val storeId = ProjectApplication.mLocaleManager.getCurrentLanguageNumber()

        logE(
            "tariffsX1289Z", "calledINSilentMode::: storeId:::".plus(storeId).plus(" offeringId")
                .plus(offeringId).plus(" subscriberType").plus(subscriberType),
            fromClass, "requestGetTariffsSilently"
        )
        val tariffsRequest =
            TariffsRequest(storeId, offeringId, subscriberType, specialTariffsIds, groupIds)

        var mObserver = mMainRepositry.requestGetTariffs(tariffsRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
            .compose(applyIOSchedulers())
            .subscribe(
                { result ->
                    tariffsSilentResponseLiveData.postValue(result)
                },
                { error ->
                    //do nothing, be quite!
                }
            )

        compositeDisposables?.add(disposable)
    }

    fun requestFCMTokenSentToServerService(
        token: String,
        ringingStatus: String,
        isEnable: String,
        cause: String,
        msisdn: String,
        isBackgroundRun: Boolean,
        subscriberTypeFCM: String,
        tariffTypeFCM: String
    ) {
        logE("fcm", "token called:::".plus(token), fromClass, "sendFCMToken")
        if (isBackgroundRun) {
            //run in background
            val fcmRequest = FCMRequest(
                token,
                ringingStatus,
                isEnable,
                cause,
                msisdn,
                subscriberTypeFCM,
                tariffTypeFCM
            )

            var mObserver = mMainRepositry.requestFCMTokenSentToServerService(fcmRequest)
            mObserver = mObserver.onErrorReturn({ throwable -> null })

            disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                    { result ->
                        fcmResponseLiveData.postValue(result)
                        logE(
                            "fcm",
                            "tokenSentSuccess:::".plus(result.toString()),
                            fromClass,
                            "requestFCMTokenSentToServerService"
                        )
                    },
                    { error ->
                        //do nothing
                        logE(
                            "fcm",
                            "tokenSentFailure:::".plus(error.toString()),
                            fromClass,
                            "requestFCMTokenSentToServerService"
                        )
                    }
                )

            compositeDisposables?.add(disposable)
        } else {
            //run in foreground
        }
    }//requestFCMTokenSentToServerService ends

    fun getFreeResourcesItemsList(
        resources: List<FreeResourcesItem?>,
        isDataSimPrepaidPostpaid: Boolean
    ): ArrayList<HomePageFreeResourcesHelperModel> {
        val resultItems = ArrayList<HomePageFreeResourcesHelperModel>()

        for (its in 0 until resources.size) {
            var resourcesTitleLabel = ""
            if (hasValue(resources[its]!!.resourcesTitleLabel)) {
                resourcesTitleLabel = resources[its]!!.resourcesTitleLabel!!
            }

            var resourceType = ""
            if (hasValue(resources[its]!!.resourceType)) {
                resourceType = resources[its]!!.resourceType!!
            }

            var resourceInitialUnits = "0"
            if (hasValue(resources[its]!!.resourceInitialUnits)) {
                resourceInitialUnits = resources[its]!!.resourceInitialUnits!!
            }
            var resourceRemainingUnits = "0"
            if (hasValue(resources[its]!!.resourceRemainingUnits)) {
                resourceRemainingUnits = resources[its]!!.resourceRemainingUnits!!
            }

            var resourceUnitName = ""
            if (hasValue(resources[its]!!.resourceUnitName)) {
                resourceUnitName = resources[its]!!.resourceUnitName!!
            }

            var resourceDiscountedText = ""
            if (hasValue(resources[its]!!.resourceDiscountedText)) {
                resourceDiscountedText = resources[its]!!.resourceDiscountedText!!
            }

            var remainingFormatted = ""
            if (hasValue(resources[its]!!.remainingFormatted)) {
                remainingFormatted = resources[its]!!.remainingFormatted!!
            }

            var sortOrder = ""

            if (resourceType.equals("DATA", ignoreCase = true)) {
                sortOrder = "2"
            }

            if (resourcesTitleLabel.equals("SMS", ignoreCase = true)) {
                sortOrder = "3"
            }

            if (resourcesTitleLabel.equals("VOICE", ignoreCase = true)) {
                sortOrder = "1"
            }
            /**in case of datasim prepaid postpaid only show the data item*/
            if (isDataSimPrepaidPostpaid) {
                if (resourceType.equals("DATA", ignoreCase = true)) {
                    resultItems.add(
                        HomePageFreeResourcesHelperModel(
                            resourcesTitleLabel,
                            resourceType,
                            resourceInitialUnits,
                            resourceRemainingUnits,
                            resourceUnitName,
                            resourceDiscountedText,
                            remainingFormatted,
                            sortOrder
                        )
                    )
                }
            } else {
                resultItems.add(
                    HomePageFreeResourcesHelperModel(
                        resourcesTitleLabel,
                        resourceType,
                        resourceInitialUnits,
                        resourceRemainingUnits,
                        resourceUnitName,
                        resourceDiscountedText,
                        remainingFormatted,
                        sortOrder
                    )
                )
            }
        }//for ends

        Collections.sort(resultItems, object : Comparator<HomePageFreeResourcesHelperModel> {
            override fun compare(
                o1: HomePageFreeResourcesHelperModel,
                o2: HomePageFreeResourcesHelperModel
            ): Int {
                return o1.sortOrder?.toIntOrNull() ?: 0.compareTo(o2.sortOrder?.toIntOrNull() ?: 0)
            }
        })

        return resultItems
    }

    fun getFreeResourcesRoamingItemsList(
        resources: List<FreeResourcesRoamingItem?>,
        isDataSimPrepaidPostpaid: Boolean
    ): ArrayList<HomePageFreeResourcesHelperModel> {
        val resultItems = ArrayList<HomePageFreeResourcesHelperModel>()

        for (its in 0 until resources.size) {
            var resourcesTitleLabel = ""
            if (hasValue(resources[its]!!.resourcesTitleLabel)) {
                resourcesTitleLabel = resources[its]!!.resourcesTitleLabel!!
            }

            var resourceType = ""
            if (hasValue(resources[its]!!.resourceType)) {
                resourceType = resources[its]!!.resourceType!!
            }

            var resourceInitialUnits = ""
            if (hasValue(resources[its]!!.resourceInitialUnits)) {
                resourceInitialUnits = resources[its]!!.resourceInitialUnits!!
            }
            var resourceRemainingUnits = ""
            if (hasValue(resources[its]!!.resourceRemainingUnits)) {
                resourceRemainingUnits = resources[its]!!.resourceRemainingUnits!!
            }

            var resourceUnitName = ""
            if (hasValue(resources[its]!!.resourceUnitName)) {
                resourceUnitName = resources[its]!!.resourceUnitName!!
            }

            var resourceDiscountedText = ""
            if (hasValue(resources[its]!!.resourceDiscountedText)) {
                resourceDiscountedText = resources[its]!!.resourceDiscountedText!!
            }

            var remainingFormatted = ""
            if (hasValue(resources[its]!!.remainingFormatted)) {
                remainingFormatted = resources[its]!!.remainingFormatted!!
            }

            var sortOrder = ""

            if (resourceType.equals(
                    ConstantsUtility.ResourceTypeConstants.RESOURCE_TYPE_DATA,
                    ignoreCase = true
                )
            ) {
                sortOrder = "2"
            }

            if (resourcesTitleLabel.equals(
                    ConstantsUtility.ResourceTypeConstants.RESOURCE_TYPE_SMS,
                    ignoreCase = true
                )
            ) {
                sortOrder = "3"
            }

            if (resourcesTitleLabel.equals(
                    ConstantsUtility.ResourceTypeConstants.RESOURCE_TYPE_VOICE,
                    ignoreCase = true
                )
            ) {
                sortOrder = "1"
            }

            if (isDataSimPrepaidPostpaid) {
                if (resourcesTitleLabel.equals("DATA", ignoreCase = true)) {
                    resultItems.add(
                        HomePageFreeResourcesHelperModel(
                            resourcesTitleLabel,
                            resourceType,
                            resourceInitialUnits,
                            resourceRemainingUnits,
                            resourceUnitName,
                            resourceDiscountedText,
                            remainingFormatted,
                            sortOrder
                        )
                    )
                }
            } else {
                resultItems.add(
                    HomePageFreeResourcesHelperModel(
                        resourcesTitleLabel,
                        resourceType,
                        resourceInitialUnits,
                        resourceRemainingUnits,
                        resourceUnitName,
                        resourceDiscountedText,
                        remainingFormatted,
                        sortOrder
                    )
                )
            }
        }//for ends

        Collections.sort(resultItems, object : Comparator<HomePageFreeResourcesHelperModel> {
            override fun compare(
                o1: HomePageFreeResourcesHelperModel,
                o2: HomePageFreeResourcesHelperModel
            ): Int {
                return o1.sortOrder?.toIntOrNull() ?: 0.compareTo(o2.sortOrder?.toIntOrNull() ?: 0)
            }
        })

        return resultItems
    }

    private fun parseGetTariffsResponse(response: TariffsResponse) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        tariffsResponseLiveData.postValue(response)
                    } else {
                        /**the response has no data object
                         * in this case we have to handle the ui only*/
                        tariffsResponseErrorLiveData.postValue(
                            AzerAPIErrorHelperResponse(
                                ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                ""
                            )
                        )
                    }
                } else {
                    /**the response has no data object
                     * in this case we have to handle the ui only*/
                    tariffsResponseErrorLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                }
            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                /**force log out user case
                 * when the server sends the result code as 7*/
                tariffsResponseErrorLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT,
                        ""
                    )
                )
            } else {
                /**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*/
                if (hasValue(response.resultDesc.toString())) {
                    tariffsResponseErrorLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                            response.resultDesc
                        )
                    )
                } else {
                    tariffsResponseErrorLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                            ""
                        )
                    )
                }
            }
        } else {
            /**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*/
            if (hasValue(response.resultDesc.toString())) {
                tariffsResponseErrorLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                        response.resultDesc
                    )
                )
            } else {
                tariffsResponseErrorLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                        ""
                    )
                )
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }//parseGetHomePageResponse ends

    private fun parseAppResumeResponse(response: LoginAppResumeResponse) {
        //check the response code
        if (hasValue(response.resultCode)) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        appResumeResponseData.postValue(response)
                    } else {
                        /**the response has no data object
                         * in this case we have to handle the ui only*/
                        appResumeErrorResponseData.postValue(
                            AzerAPIErrorHelperResponse(
                                ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                ""
                            )
                        )
                    }
                } else {
                    /**the response has no data object
                     * in this case we have to handle the ui only*/
                    appResumeErrorResponseData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                }
            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                /**force log out user case
                 * when the server sends the result code as 7*/
                appResumeErrorResponseData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT,
                        ""
                    )
                )
            } else {
                /**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*/
                if (hasValue(response.resultDesc)) {
                    appResumeErrorResponseData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                            response.resultDesc
                        )
                    )
                } else {
                    appResumeErrorResponseData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                            ""
                        )
                    )
                }
            }
        } else {
            /**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*/
            if (hasValue(response.resultDesc)) {
                appResumeErrorResponseData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                        response.resultDesc
                    )
                )
            } else {
                appResumeErrorResponseData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                        ""
                    )
                )
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }//parseAppResumeResponse ends

    private fun parseLogOutResponse(response: LogOutResponse) {
        if (response.resultCode != null) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                logOutResponseLiveData.postValue(response)
            } else {
                /**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*/
                if (hasValue(response.resultDesc.toString())) {
                    logOutResponseErrorLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                            response.resultDesc
                        )
                    )
                } else {
                    logOutResponseErrorLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                            ""
                        )
                    )
                }
            }
        } else {
            /**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*/
            if (hasValue(response.resultDesc.toString())) {
                logOutResponseErrorLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                        response.resultDesc
                    )
                )
            } else {
                logOutResponseErrorLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                        ""
                    )
                )
            }
        }
    }//parseLogOutResponse ends

    private fun parseGetHomePageResponse(response: GetHomePageResponse) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        getHomePageResponseData.postValue(response)
                    } else {
                        /**the response has no data object
                         * in this case we have to handle the ui only*/
                        getHomePageErrorResponseData.postValue(
                            AzerAPIErrorHelperResponse(
                                ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                ""
                            )
                        )
                    }
                } else {
                    /**the response has no data object
                     * in this case we have to handle the ui only*/
                    getHomePageErrorResponseData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                }
            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                /**force log out user case
                 * when the server sends the result code as 7*/
                getHomePageErrorResponseData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT,
                        ""
                    )
                )
            } else {
                /**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*/
                if (hasValue(response.resultDesc.toString())) {
                    getHomePageErrorResponseData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                            response.resultDesc
                        )
                    )
                } else {
                    getHomePageErrorResponseData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                            ""
                        )
                    )
                }
            }
        } else {
            /**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*/
            if (hasValue(response.resultDesc.toString())) {
                getHomePageErrorResponseData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                        response.resultDesc
                    )
                )
            } else {
                getHomePageErrorResponseData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                        ""
                    )
                )
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }//parseGetHomePageResponse ends

    private fun parseMenusResponse(response: MenuResponse) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        menuResponseData.postValue(response)
                    } else {
                        /**the response has no data object
                         * in this case we have to handle the ui only*/
                        menuErrorResponseData.postValue(
                            AzerAPIErrorHelperResponse(
                                ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                ""
                            )
                        )
                    }
                } else {
                    /**the response has no data object
                     * in this case we have to handle the ui only*/
                    menuErrorResponseData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                }
            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                /**force log out user case
                 * when the server sends the result code as 7*/
                menuErrorResponseData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT,
                        ""
                    )
                )
            } else {
                /**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*/
                if (hasValue(response.resultDesc.toString())) {
                    menuErrorResponseData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                            response.resultDesc
                        )
                    )
                } else {
                    menuErrorResponseData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                            ""
                        )
                    )
                }
            }
        } else {
            /**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*/
            if (hasValue(response.resultDesc.toString())) {
                menuErrorResponseData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                        response.resultDesc
                    )
                )
            } else {
                menuErrorResponseData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                        ""
                    )
                )
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }//parseMenusResponse ends

    private fun parseMenusResponseSilently(response: MenuResponse) {
        if (hasValue(response.resultCode)) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        menuSilentResponseData.postValue(response)
                    }
                }
            }
        }
    }//parseMenusResponseSilently ends

    fun getGotoPlayStoreIntent(): Intent? {
        if (hasValue(UserDataManager.getPlayStoreRedirectionURL())) {
            val uri = Uri.parse(UserDataManager.getPlayStoreRedirectionURL())
            val goToMarket = Intent(Intent.ACTION_VIEW, uri)
            // to taken back to our application, we need to add following flags to intent.
            goToMarket.addFlags(
                Intent.FLAG_ACTIVITY_NO_HISTORY or
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK
            )

            return goToMarket
        }

        return null
    }//getGotoPlayStoreIntent ends

    fun requestInternetOffers(offeringName: String, brandName: String) {
        logE(
            "suppleOffers",
            "called:::offeringName:::".plus(offeringName).plus(" brandName:::").plus(brandName),
            fromClass,
            "requestSupplementaryOffers"
        )
        view()?.showLoading()
        var offersSpecialIds = listOf<String>()
        if (UserDataManager.getCustomerData() != null && UserDataManager.getCustomerData()!!.specialOffersTariffData != null &&
            UserDataManager.getCustomerData()!!.specialOffersTariffData!!.offersSpecialIds != null
        ) {
            offersSpecialIds =
                UserDataManager.getCustomerData()!!.specialOffersTariffData!!.offersSpecialIds!!
        }
        val supplementaryOffersRequest =
            SupplementaryOffersRequest(offeringName, brandName, offersSpecialIds)

        var mObserver = mMainRepositry.requestInternetOffers(supplementaryOffersRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })
        disposable = mObserver
            .compose(applyIOSchedulers())
            .subscribe(
                { result ->
                    parseInternetResponse(result)
                },
                { error ->
                    view()?.hideLoading()
                    internetResponseErrorLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                }
            )

        compositeDisposables?.add(disposable)
    }//requestSupplementaryOffers ends

    private fun parseInternetResponse(response: InternetPacksResponse?) {
        logE("INTERNET", "Api response is $response", fromClass, "parseInternetResponse")
        //check the response code
        if (hasValue(response?.resultCode.toString())) {
            if (response?.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        internetResponseLiveData.postValue(response)
                    } else {
                        /**the response has no data object
                         * in this case we have to handle the ui only*/
                        internetResponseErrorLiveData.postValue(
                            AzerAPIErrorHelperResponse(
                                ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                ""
                            )
                        )
                    }
                } else {
                    /**the response has no data object
                     * in this case we have to handle the ui only*/
                    internetResponseErrorLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                }
            } else if (response?.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                /**force log out user case
                 * when the server sends the result code as 7*/
                internetResponseErrorLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT,
                        ""
                    )
                )
            } else {
                /**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*/
                if (hasValue(response?.msg.toString())) {
                    internetResponseErrorLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                            response?.msg
                        )
                    )
                } else {
                    internetResponseErrorLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                            ""
                        )
                    )
                }
            }
        } else {
            /**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*/
            if (hasValue(response?.msg.toString())) {
                internetResponseErrorLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                        response?.msg
                    )
                )
            } else {
                internetResponseErrorLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                        ""
                    )
                )
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }//parseResponse ends

    fun subscribeToSupplementaryOffer(
        offeringName: String,
        offeringId: String,
        actionType: String
    ) {
        view()?.showLoading()
        val supplementaryOfferSubscribeRequest =
            SupplementaryOfferSubscribeRequest(offeringName, offeringId, actionType)

        var mObserver =
            mMainRepositry.subscribeToSupplementaryOffer(supplementaryOfferSubscribeRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
            .compose(applyIOSchedulers())
            .subscribe(
                { result ->
                    parseSubscribeToSupplementaryOfferResponseLiveData(result)
                    Analytics.logAppEvent(
                        EventValues.PackageSubscriptionEvents.package_subscription,
                        EventValues.PackageSubscriptionEvents.subscription,
                        EventValues.GenericValues.Success
                    )
                },
                { error ->
                    view()?.hideLoading()
                    subscribeToSupplementaryOfferErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                    Analytics.logAppEvent(
                        EventValues.PackageSubscriptionEvents.package_subscription,
                        EventValues.PackageSubscriptionEvents.subscription,
                        EventValues.GenericValues.Failure
                    )
                }
            )

        compositeDisposables?.add(disposable)
    }//subscribeToSupplementaryOffer ends

    private fun parseSubscribeToSupplementaryOfferResponseLiveData(response: MySubscriptionsSuccessAfterOfferSubscribeResponse) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        subscribeToSupplementaryOfferResponseLiveData.postValue(response)
                    } else {
                        /**the response has no data object
                         * in this case we have to handle the ui only*/
                        subscribeToSupplementaryOfferErrorResponseLiveData.postValue(
                            AzerAPIErrorHelperResponse(
                                ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                ""
                            )
                        )
                    }
                } else {
                    /**the response has no data object
                     * in this case we have to handle the ui only*/
                    subscribeToSupplementaryOfferErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                }
            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                /**force log out user case
                 * when the server sends the result code as 7*/
                subscribeToSupplementaryOfferErrorResponseLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT,
                        ""
                    )
                )
            } else {
                /**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*/
                if (hasValue(response.resultDesc.toString())) {
                    subscribeToSupplementaryOfferErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                            response.resultDesc
                        )
                    )
                } else {
                    subscribeToSupplementaryOfferErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                            ""
                        )
                    )
                }
            }
        } else {
            /**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*/
            if (hasValue(response.resultDesc.toString())) {
                subscribeToSupplementaryOfferErrorResponseLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                        response.resultDesc
                    )
                )
            } else {
                subscribeToSupplementaryOfferErrorResponseLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                        ""
                    )
                )
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }//parseSubscribeToSupplementaryOfferResponseLiveData ends


}//class ends