package com.azarphone.ui.activities.storelocator

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import com.google.android.material.tabs.TabLayout
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.azarphone.R
import com.azarphone.api.pojo.response.storelocator.StoreLocatorResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.LayoutStoreLocatorMainFragmentBinding
import com.azarphone.ui.adapters.pagers.StoreLocatorPagerAdapter
import com.azarphone.ui.fragment.storelocator.MapFragment
import com.azarphone.ui.fragment.storelocator.StoreListFragment
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.widgets.popups.showMessageDialog
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.layout_store_locator_main_fragment.*
import java.util.*
import android.view.Gravity
import android.widget.LinearLayout
import android.widget.RelativeLayout


class StoreLocatorActivity : BaseActivity<LayoutStoreLocatorMainFragmentBinding, StoreLocatorFactory, StoreLocatorViewModel>(), ActivityCompat.OnRequestPermissionsResultCallback {

    private lateinit var mPagerAdapter: StoreLocatorPagerAdapter
    val LOCATION_PERMISSION_REQUEST_CODE = 1001
    private var storeLocatorResponse: StoreLocatorResponse? = null

    override fun getLayoutId(): Int {
        return R.layout.layout_store_locator_main_fragment
    }

    override fun getViewModelClass(): Class<StoreLocatorViewModel> {
        return StoreLocatorViewModel::class.java
    }

    override fun getFactory(): StoreLocatorFactory {
        return StoreLocatorInjectionUtils.provideStoreLocatorFactory()
    }

    override fun init() {
        setupHeaderActionBar()
        initUiEvents()


        mPagerAdapter = StoreLocatorPagerAdapter(this, supportFragmentManager)
        viewDataBinding.viewpager.apply {
            adapter = mPagerAdapter
        }
        setupTabView()

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            //  map.setMyLocationEnabled(true)
            loadData()

        } else {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)

        }
    }

    private fun loadData() {
        /**get the response from the cache*/
        val cachedStoreData = getStoreLocatorCachedDataFromLocalPreferences()
        if (cachedStoreData.data != null && hasValue(cachedStoreData.data.toString())) {
            storeLocatorResponse = cachedStoreData

            mViewModel.storeLocatorResponse.postValue(storeLocatorResponse)

            setStoreLocatorCachedDataToLocalPreferences(storeLocatorResponse!!)

            /**call the api in the background*/
            mViewModel.getStoreLocatorInBackground()
        } else {
            if (isNetworkAvailable(applicationContext)) {
                mViewModel.getStoreLocator()
            } else {
                showMessageDialog(applicationContext, this@StoreLocatorActivity,
                        "", resources.getString(R.string.message_no_internet))
            }
        }
    }

    private fun setupTabView() {
//        viewDataBinding.slidingTabs.setupWithViewPager(viewDataBinding.viewpager)
//        for (i in 0 until mFragmentViewDataBinding.slidingTabs.getTabCount()) {
//            mFragmentViewDataBinding.slidingTabs.getTabAt(i)!!.setCustomView(R.layout.customer_map_tab)
////            val tab_name = tabLayout.getTabAt(i).getCustomView().findViewById(R.id.txt_tab_name) as TextView
////            tab_name.text = "" + tabNames[i]
//        }
        sliding_tabs.addTab(sliding_tabs.newTab().setText(resources.getString(R.string.title_activity_maps)))
        sliding_tabs.addTab(sliding_tabs.newTab().setText(resources.getString(R.string.storelist)))

        if (sliding_tabs.tabCount > 0) {
            //set the tab by index
            sliding_tabs.getTabAt(0)!!.select()
        }

        for (i in 0 until viewDataBinding.slidingTabs.tabCount) {
            val tab = (viewDataBinding.slidingTabs.getChildAt(0) as ViewGroup).getChildAt(i)
            val p = tab.layoutParams as ViewGroup.MarginLayoutParams
            p.setMargins(16, 0, 16, 0)
            tab.requestLayout()

            //set the custom view
            val tabView = sliding_tabs.getTabAt(i)
            if (tabView != null) {
                val tabTextView = TextView(this@StoreLocatorActivity)
                tabView.customView = tabTextView
                //set the text
                tabTextView.text = tabView.text
                tabTextView.ellipsize = TextUtils.TruncateAt.MARQUEE
                tabTextView.isSelected = true
                tabTextView.marqueeRepeatLimit = -1
                tabTextView.setSingleLine(true)
                tabTextView.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                tabTextView.gravity= Gravity.CENTER
                //set the currently selected tab font
                if (i == 0) {
                    setSelectedTabFont(applicationContext, tabTextView)
                } else {
                    setUnSelectedTabFont(applicationContext, tabTextView)
                }
            }
        }

        val paggerFragmentsList = ArrayList<androidx.fragment.app.Fragment>()
        paggerFragmentsList.add(StoreListFragment())
        paggerFragmentsList.add(MapFragment())

        sliding_tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                if (tab != null && tab.customView != null) {
                    val tabTextView = tab.customView as TextView
                    setUnSelectedTabFont(applicationContext, tabTextView)
                }
            }//onTabUnselected ends

            override fun onTabSelected(tab: TabLayout.Tab?) {
                viewpager.setCurrentItem(tab!!.position, false)
                if (tab.customView != null) {
                    val tabTextView = tab.customView as TextView
                    setSelectedTabFont(applicationContext, tabTextView)
                }
            }//onTabSelected ends
        })//tabLayout.addOnTabSelectedListener ends

    }

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.storelist)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }

    private fun initUiEvents() {
        azerActionBarBackIcon.setOnClickListener {
            onBackPressed()
        }
    }


    companion object {
        fun start(context: Context) {
            val intent = Intent(context, StoreLocatorActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        /* if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
             if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

             }
         }*/
        when (requestCode) {

            LOCATION_PERMISSION_REQUEST_CODE -> {

                if (requestCode == LOCATION_PERMISSION_REQUEST_CODE && grantResults != null && grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (ContextCompat.checkSelfPermission(this,
                                    Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        loadData()
                    }
                } else {
                    loadData()
                }
            }
        }
    }

}