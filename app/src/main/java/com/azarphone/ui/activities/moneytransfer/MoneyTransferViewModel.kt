package com.azarphone.ui.activities.moneytransfer

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.request.MoneyTransferRequest
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.moneytransfer.MoneyTransferResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.ui.dialogs.InAppThankYouDialog
import com.azarphone.util.ConstantsUtility
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

/**
 * @author Junaid Hassan on 08, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class MoneyTransferViewModel(val mMoneytransferRepositry: MoneyTransferRepositry) : BaseViewModel() {

    private val fromClass = "TopupViewModel"
    private lateinit var disposable: Disposable
    val moneyTransferLiveDataResponse = MutableLiveData<MoneyTransferResponse>()
    val inAppSurveyResponseErrorResponseData = MutableLiveData<AzerAPIErrorHelperResponse>()

    init {

    }//init ends
    fun uploadInAppSurvey(context: Context, uploadSurvey: UploadSurvey, onTransactionComplete: String) {
        view()?.showLoading()
        var observable = mMoneytransferRepositry.saveInAppSurvey(uploadSurvey)
        observable = observable.onErrorReturn { throwable -> null }
        disposable = observable
            .compose(applyIOSchedulers())
            .subscribe(
                { appSurvey ->
                    view()?.hideLoading()
                    when {
                        appSurvey?.data != null -> {
                            UserDataManager.setInAppSurvey(appSurvey)
                            val thankYouDialog =
                                InAppThankYouDialog(context, onTransactionComplete)
                            thankYouDialog.show()
                        }
                        appSurvey?.resultDesc != null -> {
                            inAppSurveyResponseErrorResponseData.postValue(
                                AzerAPIErrorHelperResponse(
                                    ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                    appSurvey.resultDesc
                                )
                            )
                        }
                        else -> {
                            inAppSurveyResponseErrorResponseData.postValue(
                                AzerAPIErrorHelperResponse(
                                    ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                    context.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            )
                        }
                    }
                },
                { error ->
                    inAppSurveyResponseErrorResponseData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            error.message
                        )
                    )
                }
            )
        compositeDisposables?.add(disposable)
    }

    fun requestMoneyTransfer(msisdn: String, amount: String) {
        view()?.showLoading()
        val subscriberType = UserDataManager.getCustomerData()?.subscriberType
        val moneyTransferRequest = MoneyTransferRequest(msisdn, amount, subscriberType)

        var mObserver = mMoneytransferRepositry.requestMoneyTransfer(moneyTransferRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    moneyTransferLiveDataResponse.postValue(result)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestMoneyTransfer ends

}//class ends