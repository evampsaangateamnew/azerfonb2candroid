package com.azarphone.ui.activities.freesms

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

/**
 * @author Junaid Hassan on 08, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class FreeSmsFactory(val mFreeSmsRepositry: FreeSmsRepositry) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(FreeSmsViewModel::class.java)) {
            return FreeSmsViewModel(mFreeSmsRepositry) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}