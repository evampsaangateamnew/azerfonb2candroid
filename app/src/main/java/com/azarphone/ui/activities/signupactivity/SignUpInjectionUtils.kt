package com.azarphone.ui.activities.signupactivity

import com.azarphone.ui.activities.loginactivity.LoginFactory
import com.azarphone.ui.activities.loginactivity.LoginRepository

object SignUpInjectionUtils{


    fun provideSignUpRepostiory():SignUpRepository{
        return SignUpRepository()
    }

    fun provideSignUpFactory(): SignUpFactory {
        return SignUpFactory(provideSignUpRepostiory())
    }



}