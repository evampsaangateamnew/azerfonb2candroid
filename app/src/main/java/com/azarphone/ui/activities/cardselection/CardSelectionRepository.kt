package com.azarphone.ui.activities.cardselection

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.InitiatePaymentRequest
import com.azarphone.api.pojo.response.topup.initiatepayment.InitiatePaymentResponse
import io.reactivex.Observable

class CardSelectionRepository {

    fun requestInitiatePayment(initiatePaymentRequest: InitiatePaymentRequest): Observable<InitiatePaymentResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestInitiatePayment(initiatePaymentRequest)
    }
}