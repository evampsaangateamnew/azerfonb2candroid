package com.azarphone.ui.activities.datawithoutpack

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class DataWithOutPackFactory(val dataWithOutPackRepository: DataWithOutPackRepository) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DataWithOutPackViewModel::class.java)) {
            return DataWithOutPackViewModel(dataWithOutPackRepository) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}