package com.azarphone.ui.activities.mainactivity

import android.util.Log
import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.*
import com.azarphone.api.pojo.response.fcmtoken.FCMResponse
import com.azarphone.api.pojo.response.gethomepageresponse.GetHomePageResponse
import com.azarphone.api.pojo.response.getrateusresponse.GetRateUsResponse
import com.azarphone.api.pojo.response.inappsurvey.InAppSurvey
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.internetpackresponse.InternetPacksResponse
import com.azarphone.api.pojo.response.loginappresumeresponse.LoginAppResumeResponse
import com.azarphone.api.pojo.response.logoutresponse.LogOutResponse
import com.azarphone.api.pojo.response.menuresponse.MenuResponse
import com.azarphone.api.pojo.response.mysubscriptions.successresponse.MySubscriptionsSuccessAfterOfferSubscribeResponse
import com.azarphone.api.pojo.response.notificationscountresponse.NotificationsCountResponse
import com.azarphone.api.pojo.response.rateusbeforeredirectionresponse.RateUsBeforeRedirectingToPlayStoreRespone
import com.azarphone.api.pojo.response.tariffsresponse.TariffsResponse
import com.azarphone.api.pojo.response.tariffsubscriptionresponse.TariffSubscriptionResponse
import io.reactivex.Observable

/**
 * @author Junaid Hassan on 13, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class MainRepositry {

    init {

    }//init ends

    fun getSurvey(): Observable<InAppSurvey> {
        Log.e("base_", ApiClient.newApiClientInstance.setBaseUrl())
        return ApiClient.newApiClientInstance.getServerAPI().requestGetSurveys()
    }

    fun requestMenus(menuRequest: MenuRequest): Observable<MenuResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestMenus(menuRequest)
    }//requestMenus ends

    fun requestAppResume(appResumeRequest: AppResumeRequest): Observable<LoginAppResumeResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestAppResume(appResumeRequest)
    }//requestAppResume ends


    fun requestGetHomePage(getHomePageRequest: GetHomePageRequest): Observable<GetHomePageResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestGetHomePage(getHomePageRequest)
    }

    fun requestLogOut(): Observable<LogOutResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestLogOut()
    }


    fun requestGetRateUs(getRateUsRequest: GetRateUsRequest): Observable<GetRateUsResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestGetRateUs(getRateUsRequest)
    }

    fun requestGetTariffs(tariffsRequest: TariffsRequest): Observable<TariffsResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestGetTariffs(tariffsRequest)
    }

    fun requestGetNotificationsCount(): Observable<NotificationsCountResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestGetNotificationsCount()
    }

    fun requestRateUsBeforeRedirectingToPlayStore(getRateUsRequest: GetRateUsRequest): Observable<RateUsBeforeRedirectingToPlayStoreRespone> {
        return ApiClient.newApiClientInstance.getServerAPI()
            .requestRateUsBeforeRedirectingToPlayStore(getRateUsRequest)
    }

    fun requestFCMTokenSentToServerService(fcmRequest: FCMRequest): Observable<FCMResponse> {
        return ApiClient.newApiClientInstance.getServerAPI()
            .requestFCMTokenSentToServerService(fcmRequest)
    }

    fun requestSubscribeToTariff(tariffSubscribeRequest: TariffSubscribeRequest): Observable<TariffSubscriptionResponse> {
        return ApiClient.newApiClientInstance.getServerAPI()
            .requestSubscribeToTariff(tariffSubscribeRequest)
    }

    fun requestInternetOffers(supplementaryOffersRequest: SupplementaryOffersRequest): Observable<InternetPacksResponse> {
        return ApiClient.newApiClientInstance.getServerAPI()
            .requestInternetOffers(supplementaryOffersRequest)
    }//requestSupplementaryOffers ends

    fun subscribeToSupplementaryOffer(supplementaryOfferSubscribeRequest: SupplementaryOfferSubscribeRequest): Observable<MySubscriptionsSuccessAfterOfferSubscribeResponse> {
        return ApiClient.newApiClientInstance.getServerAPI()
            .subscribeToSupplementaryOffer(supplementaryOfferSubscribeRequest)
    }//requestSuspendNumber ends

    fun saveInAppSurvey(uploadSurvey: UploadSurvey): Observable<InAppSurvey> {
        return ApiClient.newApiClientInstance.getServerAPI().saveInAppSurvey(uploadSurvey)
    }
}//class ends