package com.azarphone.ui.activities.supplementaryoffers

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.azarphone.R
import com.azarphone.analytics.Analytics
import com.azarphone.analytics.EventValues
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.request.MySubscriptionRequest
import com.azarphone.api.pojo.request.SupplementaryOfferSubscribeRequest
import com.azarphone.api.pojo.request.SupplementaryOffersRequest
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.mysubscriptions.response.MySubscriptionResponse
import com.azarphone.api.pojo.response.mysubscriptions.successresponse.MySubscriptionsSuccessAfterOfferSubscribeResponse
import com.azarphone.api.pojo.response.supplementaryoffers.response.SupplementaryResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.UpdateListOnSuccessfullySurvey
import com.azarphone.ui.dialogs.InAppThankYouDialog
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.logE
import com.azarphone.validators.hasValue
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

/**
 * @author Junaid Hassan on 22, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class SupplementaryOffersViewModel(val mSupplementaryOffersRepositry: SupplementaryOffersRepositry) :
    BaseViewModel() {

    private lateinit var disposable: Disposable
    val supplementaryOffersResponseLiveData = MutableLiveData<SupplementaryResponse>()
    val supplementaryOffersErrorResponseLiveData = MutableLiveData<AzerAPIErrorHelperResponse>()
    private val fromClass = "SupplementaryOffersViewModel"
    val supplementaryOffersBGResponseLiveData = MutableLiveData<SupplementaryResponse>()
    val mySubscriptionsBeforePackagesResponseLiveData = MutableLiveData<MySubscriptionResponse>()
    val subscribeToSupplementaryOfferResponseLiveData =
        MutableLiveData<MySubscriptionsSuccessAfterOfferSubscribeResponse>()
    val mySubscriptionsBeforePackagesErrorResponseLiveData =
        MutableLiveData<AzerAPIErrorHelperResponse>()
    val subscribeToSupplementaryOfferErrorResponseLiveData =
        MutableLiveData<AzerAPIErrorHelperResponse>()
    val inAppSurveyResponseErrorResponseData = MutableLiveData<AzerAPIErrorHelperResponse>()

    init {

    }//init ends

    fun uploadInAppSurvey(context: Context, uploadSurvey: UploadSurvey, onTransactionComplete: String, updateListOnSuccessfullySurvey: UpdateListOnSuccessfullySurvey?) {
        view()?.showLoading()
        var observable = mSupplementaryOffersRepositry.saveInAppSurvey(uploadSurvey)
        observable = observable.onErrorReturn { throwable -> null }
        disposable = observable
            .compose(applyIOSchedulers())
            .subscribe(
                { appSurvey ->
                    view()?.hideLoading()
                    when {
                        appSurvey?.data != null -> {
                            UserDataManager.setInAppSurvey(appSurvey)
                            val thankYouDialog =
                                InAppThankYouDialog(context, onTransactionComplete)
                            thankYouDialog.show()
                        }
                        appSurvey?.resultDesc != null -> {
                            inAppSurveyResponseErrorResponseData.postValue(
                                AzerAPIErrorHelperResponse(
                                    ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                    appSurvey.resultDesc
                                )
                            )
                        }
                        else -> {
                            inAppSurveyResponseErrorResponseData.postValue(
                                AzerAPIErrorHelperResponse(
                                    ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                    context.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            )
                        }
                    }
                    updateListOnSuccessfullySurvey?.updateListOnSuccessfullySurvey()
                },
                { error ->
                    inAppSurveyResponseErrorResponseData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            error.message
                        )
                    )
                }
            )
        compositeDisposables?.add(disposable)
    }


    fun requestSupplementaryOffersBackground(offeringName: String, brandName: String) {
        logE(
            "suppleOffers",
            "calledbg:::offeringName:::".plus(offeringName).plus(" brandName:::").plus(brandName),
            fromClass,
            "requestSupplementaryOffersBackground"
        )
        var offersSpecialIds = listOf<String>()
        if (UserDataManager.getCustomerData() != null && UserDataManager.getCustomerData()!!.specialOffersTariffData != null &&
            UserDataManager.getCustomerData()!!.specialOffersTariffData!!.offersSpecialIds != null
        ) {
            offersSpecialIds =
                UserDataManager.getCustomerData()!!.specialOffersTariffData!!.offersSpecialIds!!
        }
        val supplementaryOffersRequest =
            SupplementaryOffersRequest(offeringName, brandName, offersSpecialIds)

        var mObserver =
            mSupplementaryOffersRepositry.requestSupplementaryOffers(supplementaryOffersRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })
        disposable = mObserver
            .compose(applyIOSchedulers())
            .subscribe(
                { result ->
                    logE(
                        "suppleOffers",
                        "responseBG:::".plus(result),
                        fromClass,
                        "requestSupplementaryOffersBackground"
                    )
                    parsePackagesResponseResponseBG(result)
                },
                { error ->
                }
            )

        compositeDisposables?.add(disposable)
    }

    fun requestSupplementaryOffers(offeringName: String, brandName: String) {
        logE(
            "suppleOffers",
            "called:::offeringName:::".plus(offeringName).plus(" brandName:::").plus(brandName),
            fromClass,
            "requestSupplementaryOffers"
        )
        view()?.showLoading()
        var offersSpecialIds = listOf<String>()
        if (UserDataManager.getCustomerData() != null && UserDataManager.getCustomerData()!!.specialOffersTariffData != null &&
            UserDataManager.getCustomerData()!!.specialOffersTariffData!!.offersSpecialIds != null
        ) {
            offersSpecialIds =
                UserDataManager.getCustomerData()!!.specialOffersTariffData!!.offersSpecialIds!!
        }
        val supplementaryOffersRequest =
            SupplementaryOffersRequest(offeringName, brandName, offersSpecialIds)

        var mObserver =
            mSupplementaryOffersRepositry.requestSupplementaryOffers(supplementaryOffersRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })
        disposable = mObserver
            .compose(applyIOSchedulers())
            .subscribe(
                { result ->
                    parseResponse(result)
                },
                { error ->
                    view()?.hideLoading()
                    supplementaryOffersErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                }
            )

        compositeDisposables?.add(disposable)
    }//requestSupplementaryOffers ends

    fun subscribeToSupplementaryOffer(
        offeringName: String,
        offeringId: String,
        actionType: String
    ) {
        view()?.showLoading()
        val supplementaryOfferSubscribeRequest =
            SupplementaryOfferSubscribeRequest(offeringName, offeringId, actionType)

        var mObserver = mSupplementaryOffersRepositry.subscribeToSupplementaryOffer(
            supplementaryOfferSubscribeRequest
        )
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
            .compose(applyIOSchedulers())
            .subscribe(
                { result ->
                    parseSubscribeToSupplementaryOfferResponseLiveData(result)
                    Analytics.logAppEvent(
                        EventValues.PackageSubscriptionEvents.package_subscription,
                        EventValues.PackageSubscriptionEvents.subscription,
                        EventValues.GenericValues.Success
                    )
                },
                { error ->
                    view()?.hideLoading()
                    subscribeToSupplementaryOfferErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                    Analytics.logAppEvent(
                        EventValues.PackageSubscriptionEvents.package_subscription,
                        EventValues.PackageSubscriptionEvents.subscription,
                        EventValues.GenericValues.Failure
                    )
                }
            )

        compositeDisposables?.add(disposable)
    }//subscribeToSupplementaryOffer ends

    private fun parseSubscribeToSupplementaryOfferResponseLiveData(response: MySubscriptionsSuccessAfterOfferSubscribeResponse) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        subscribeToSupplementaryOfferResponseLiveData.postValue(response)
                    } else {
                        /**the response has no data object
                         * in this case we have to handle the ui only*/
                        subscribeToSupplementaryOfferErrorResponseLiveData.postValue(
                            AzerAPIErrorHelperResponse(
                                ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                ""
                            )
                        )
                    }
                } else {
                    /**the response has no data object
                     * in this case we have to handle the ui only*/
                    subscribeToSupplementaryOfferErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                }
            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                /**force log out user case
                 * when the server sends the result code as 7*/
                subscribeToSupplementaryOfferErrorResponseLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT,
                        ""
                    )
                )
            } else {
                /**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*/
                if (hasValue(response.resultDesc.toString())) {
                    subscribeToSupplementaryOfferErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                            response.resultDesc
                        )
                    )
                } else {
                    subscribeToSupplementaryOfferErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                            ""
                        )
                    )
                }
            }
        } else {
            /**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*/
            if (hasValue(response.resultDesc.toString())) {
                subscribeToSupplementaryOfferErrorResponseLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                        response.resultDesc
                    )
                )
            } else {
                subscribeToSupplementaryOfferErrorResponseLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                        ""
                    )
                )
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }//parseSubscribeToSupplementaryOfferResponseLiveData ends

    fun requestMySubscriptions() {
        var offeringId = ""
        var brandName = ""

        if (UserDataManager.getCustomerData() != null) {
            if (hasValue(UserDataManager.getCustomerData()!!.offeringId)) {
                offeringId = UserDataManager.getCustomerData()!!.offeringId!!
            }

            if (hasValue(UserDataManager.getCustomerData()!!.brandName)) {
                brandName = UserDataManager.getCustomerData()!!.brandName!!
            }
        }

        logE(
            "subXYR",
            "MySubcriptionsCalling:::offeringName:::".plus(offeringId).plus(" brandName:::")
                .plus(brandName),
            fromClass,
            "requestMySubscriptions"
        )
        view()?.showLoading()
        val mySubscriptionRequest = MySubscriptionRequest(offeringId)

        var mObserver = mSupplementaryOffersRepositry.requestMySubscriptions(mySubscriptionRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
            .compose(applyIOSchedulers())
            .subscribe(
                { result ->
                    logE(
                        "subXYR",
                        "response:::".plus(result.toString()),
                        fromClass,
                        "requestMySubscriptions"
                    )
                    parseMySubscriptionsResponse(result)
                },
                { error ->
                    view()?.hideLoading()
                    mySubscriptionsBeforePackagesErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                }
            )

        compositeDisposables?.add(disposable)
    }//requestTopUp ends

    private fun parseMySubscriptionsResponse(response: MySubscriptionResponse) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        mySubscriptionsBeforePackagesResponseLiveData.postValue(response)
                    } else {
                        /**the response has no data object
                         * in this case we have to handle the ui only*/
                        mySubscriptionsBeforePackagesErrorResponseLiveData.postValue(
                            AzerAPIErrorHelperResponse(
                                ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                ""
                            )
                        )
                    }
                } else {
                    /**the response has no data object
                     * in this case we have to handle the ui only*/
                    mySubscriptionsBeforePackagesErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                }
            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                /**force log out user case
                 * when the server sends the result code as 7*/
                mySubscriptionsBeforePackagesErrorResponseLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT,
                        ""
                    )
                )
            } else {
                /**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*/
                if (hasValue(response.resultDesc.toString())) {
                    mySubscriptionsBeforePackagesErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                            response.resultDesc
                        )
                    )
                } else {
                    mySubscriptionsBeforePackagesErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                            ""
                        )
                    )
                }
            }
        } else {
            /**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*/
            if (hasValue(response.resultDesc.toString())) {
                mySubscriptionsBeforePackagesErrorResponseLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                        response.resultDesc
                    )
                )
            } else {
                mySubscriptionsBeforePackagesErrorResponseLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                        ""
                    )
                )
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }//parseResponse ends

    private fun parsePackagesResponseResponseBG(response: SupplementaryResponse) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        supplementaryOffersBGResponseLiveData.postValue(response)
                    }
                }
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }//parseResponse ends

    private fun parseResponse(response: SupplementaryResponse) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        supplementaryOffersResponseLiveData.postValue(response)
                    } else {
                        /**the response has no data object
                         * in this case we have to handle the ui only*/
                        supplementaryOffersErrorResponseLiveData.postValue(
                            AzerAPIErrorHelperResponse(
                                ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                ""
                            )
                        )
                    }
                } else {
                    /**the response has no data object
                     * in this case we have to handle the ui only*/
                    supplementaryOffersErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                }
            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                /**force log out user case
                 * when the server sends the result code as 7*/
                supplementaryOffersErrorResponseLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT,
                        ""
                    )
                )
            } else {
                /**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*/
                if (hasValue(response.resultDesc.toString())) {
                    supplementaryOffersErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                            response.resultDesc
                        )
                    )
                } else {
                    supplementaryOffersErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                            ""
                        )
                    )
                }
            }
        } else {
            /**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*/
            if (hasValue(response.resultDesc.toString())) {
                supplementaryOffersErrorResponseLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                        response.resultDesc
                    )
                )
            } else {
                supplementaryOffersErrorResponseLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                        ""
                    )
                )
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }//parseResponse ends
}//class ends