package com.azarphone.ui.activities.topup

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.request.DeleteSavedCardRequest
import com.azarphone.api.pojo.request.InitiatePaymentRequest
import com.azarphone.api.pojo.request.MakePaymentRequest
import com.azarphone.api.pojo.response.fasttopupresponse.deletefasttopup.DeleteItemResponse
import com.azarphone.api.pojo.response.inappsurvey.InAppSurvey
import com.azarphone.api.pojo.response.inappsurvey.Surveys
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.makepayment.MakePaymentTopUpResponse
import com.azarphone.api.pojo.response.topup.TopupResponse
import com.azarphone.api.pojo.response.topup.initiatepayment.InitiatePaymentResponse
import com.azarphone.api.pojo.response.topup.savedcards.SavedCardsItem
import com.azarphone.api.pojo.response.topup.savedcards.SavedCardsResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.LayoutTopupActivityBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.OnClickSurveySubmit
import com.azarphone.eventhandler.TopUpItemSelectEvents
import com.azarphone.eventhandler.UpdateListOnSuccessfullySurvey
import com.azarphone.ui.adapters.recyclerviews.TopUpCardsAdapter
import com.azarphone.ui.dialogs.InAppFeedbackDialog
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.validators.isNumeric
import com.azarphone.validators.isValidMsisdn
import com.azarphone.widgets.popups.showCardTypeSelectionDialog
import com.azarphone.widgets.popups.showConfirmationDialog
import com.azarphone.widgets.popups.showMessageDialog
import com.es.tec.LocalSharedPrefStorage
import kotlinx.android.synthetic.main.activity_fast_topup.*
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.layout_faq_fragment.*
import kotlinx.android.synthetic.main.layout_topup_activity.*
import kotlinx.android.synthetic.main.nartv_item.view.*
import java.util.*


/**
 * @author Junaid Hassan on 03, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class TopupActivity : BaseActivity<LayoutTopupActivityBinding, TopupFactory, TopupViewModel>() {

    private val fromClass = TopupActivity::class.simpleName

    private var isSavedCardsAvailable = false
    private var isSaveCard: Boolean = true
    private val isValAvailable = false
    var isDelKeyPressed = false

    private var deletedPosition: Int = -1

    var paymentKey = ""
    var cardType = ""

    override fun getLayoutId(): Int {
        return R.layout.layout_topup_activity
    }//getLayoutId ends

    override fun getViewModelClass(): Class<TopupViewModel> {
        return TopupViewModel::class.java
    }//getViewModelClass ensd

    override fun getFactory(): TopupFactory {
        return TopupInjection.provideTopupFactory()
    }//getFactory ends

    override fun init() {
        subscribe()
        addPrefixToUserNameField(msisdnET)  // add on request of irfan bhai :P
        setupInputFields()
        setupHeaderActionBar()
        initData()
        initUiEvents()
    }//init ends

    private fun initData() {
        msisdnET.setText(UserDataManager.getCustomerMSISDN())
    }//initData

    override fun onResume() {
        super.onResume()
        mViewModel.requestTopUpCards()


    }//onResume

    private fun setupInputFields() {
        msisdnTL.defaultHintTextColor = resources.getColorStateList(R.color.brown_grey)
        msisdnET.typeface = getALSBoldFont()

        cardNumberTL.defaultHintTextColor = resources.getColorStateList(R.color.brown_grey)
        cardNumberET.typeface = getALSBoldFont()

        amountTL.defaultHintTextColor = resources.getColorStateList(R.color.brown_grey)
        amountET.typeface = getALSBoldFont()
    }//setupInputFields ends

    private fun subscribe() {
        val topupResponseLiveData = object : Observer<TopupResponse> {
            override fun onChanged(t: TopupResponse?) {
                processTopUpResponse(t)
            }
        }

        mViewModel.topupResponseLiveData.observe(this@TopupActivity, topupResponseLiveData)


        val savedCardsLiveData = object : Observer<SavedCardsResponse> {
            override fun onChanged(t: SavedCardsResponse?) {
                if (t != null)
                    processSavedCardResponse(t)
            }
        }

        mViewModel.savedCardsResponseLiveData.observe(this@TopupActivity, savedCardsLiveData)

        val inAppSurveyResponseErrorResponseData =
            Observer<AzerAPIErrorHelperResponse> { response ->
                if (!isNetworkAvailable(this@TopupActivity)) {
                    showMessageDialog(
                        this@TopupActivity, this@TopupActivity,
                        resources.getString(R.string.popup_note_title),
                        resources.getString(R.string.message_no_internet)
                    )
                } else {
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                        showMessageDialog(
                            this@TopupActivity, this@TopupActivity,
                            this@TopupActivity.resources.getString(R.string.popup_error_title),
                            response.errorDetail.toString()
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                        //force logout the user here
                        logOut(
                            this@TopupActivity,
                            this@TopupActivity,
                            false,
                            fromClass?:"TopUp",
                            "initSupplementaryOffersCardAdapterEventsa34324"
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                        //handle the ui in this case
                        //hide or show the no data found layout in this case
                        showMessageDialog(
                            this@TopupActivity, this@TopupActivity,
                            this@TopupActivity.resources.getString(R.string.popup_error_title),
                            this@TopupActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                        showMessageDialog(
                            this@TopupActivity, this@TopupActivity,
                            this@TopupActivity.resources.getString(R.string.popup_error_title),
                            this@TopupActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    } else {
                        showMessageDialog(
                            this@TopupActivity, this@TopupActivity,
                            this@TopupActivity.resources.getString(R.string.popup_error_title),
                            this@TopupActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    }
                }
            }
        mViewModel.inAppSurveyResponseErrorResponseData.observe(
            this@TopupActivity,
            inAppSurveyResponseErrorResponseData
        )
        val makePaymentLiveData = object : Observer<MakePaymentTopUpResponse> {
            override fun onChanged(t: MakePaymentTopUpResponse?) {
                Log.e(
                    "makePaymentLiveData = ${t?.resultDesc}",
                    "onChanged: makePaymentLiveData success "
                )
                //refresh dashboard check
                inAppFeedback(this@TopupActivity,resources.getString(R.string.popup_note_title),t?.resultDesc?:"")
                UserDataManager.setIsGetHomePageCalledWithSuccess(true)
                removeHomePageResponseFromLocalPreferences()
            }
        }

        mViewModel.makePaymentResponseLiveData.observe(this@TopupActivity, makePaymentLiveData)


        val initiatePaymentLiveData = object : Observer<InitiatePaymentResponse> {
            override fun onChanged(t: InitiatePaymentResponse?) {
                Log.e(
                    "initiatePaymentLiveData",
                    "onChanged: initiatePaymentLiveData success " + t?.data?.paymentKey
                )
                processInitiateResponse(t)
            }
        }

        mViewModel.initiatePaymentResponseLiveData.observe(
            this@TopupActivity,
            initiatePaymentLiveData
        )


        val deleteItemErrorLiveData = object : Observer<String> {
            override fun onChanged(t: String?) {
                logE("FastTopUpActivity", "error:::", "FastTopUpActivity", "error on delete item ")

                processDeleteItemError(t)
            }
        }
        mViewModel.deleteSavedCardsErrorLiveData.observe(
            this@TopupActivity,
            deleteItemErrorLiveData
        )

        val deleteFastTopUpPaymentsResponseLiveData = object : Observer<DeleteItemResponse> {
            override fun onChanged(deleteFastTopUpResponse: DeleteItemResponse?) {
                if (!this@TopupActivity.isFinishing && deletedPosition != -1 && cardSelectionRV?.adapter != null && deleteFastTopUpResponse != null) {
                    logE(
                        "FastTopUpActivity",
                        "error:::",
                        "FastTopUpActivity",
                        "1st condittion matched on delete item "
                    )
                    (cardSelectionRV.adapter as TopUpCardsAdapter).deleteItem(deletedPosition)
                    deletedPosition = -1
                } else {
                    logE(
                        "FastTopUpActivity",
                        "error:::",
                        "FastTopUpActivity",
                        "else on delete item "
                    )
                    deletedPosition = -1
                    (cardSelectionRV.adapter as TopUpCardsAdapter).notifyDataSetChanged()
                }
            }
        }
        mViewModel.deleteSavedCardsResponseLiveData.observe(
            this@TopupActivity,
            deleteFastTopUpPaymentsResponseLiveData
        )

    }//subscribe ends

    private fun processInitiateResponse(response: InitiatePaymentResponse?) {

        var responseDescription = ""
        if (!this@TopupActivity.isFinishing && response != null &&
            hasValue(response.resultDesc)
        ) {
            responseDescription = response.resultDesc
        }

        if (!this@TopupActivity.isFinishing && response != null) {
            if (response.data != null && hasValue(response.data.paymentKey)) {
                PlasticCardActivity.start(this, response.data.paymentKey)
            } else {
                showMessageDialog(
                    this@TopupActivity, this@TopupActivity,
                    getString(R.string.popup_error_title), responseDescription
                )
            }
        } else {
            showMessageDialog(
                this@TopupActivity, this@TopupActivity,
                getString(R.string.popup_error_title), responseDescription
            )
        }
    }//processInitiateResponse ends

    private fun processDeleteItemError(t: String?) {
        cardSelectionRV?.adapter?.notifyDataSetChanged()
    }//processDeleteItemError ends
    private fun inAppFeedback(context: Context, title: String, message: String) {

        var inAppFeedbackDialog : InAppFeedbackDialog?= null
        var currentVisit: Int =
            LocalSharedPrefStorage(context).getInt(LocalSharedPrefStorage.PREF_TOP_UP_PAGE)
        currentVisit += 1
        val inAppSurvey: InAppSurvey? = UserDataManager.getInAppSurvey()
        if ((inAppSurvey?.data) != null) {
            val surveys: Surveys? =
                inAppSurvey.data.findSurvey(ConstantsUtility.InAppSurveyConstants.TOP_UP_PAGE)
            if (surveys != null) {
                if (surveys.surveyLimit > surveys.surveyCount) {
                    if (surveys.visitLimit <= currentVisit && surveys.visitLimit > -1) {
                        inAppFeedbackDialog =
                            InAppFeedbackDialog(context, object : OnClickSurveySubmit {
                                override fun onClickSubmit(
                                    uploadSurvey: UploadSurvey,
                                    onTransactionComplete: String,updateListOnSuccessfullySurvey: UpdateListOnSuccessfullySurvey?
                                ) {
                                    mViewModel.uploadInAppSurvey(
                                        context,
                                        uploadSurvey,
                                        onTransactionComplete
                                    )
                                    if (inAppFeedbackDialog?.isShowing == true) {
                                        inAppFeedbackDialog?.dismiss()
                                    }
                                }

                            })
                        if (surveys.questions != null) {
                            currentVisit = 0
                            inAppFeedbackDialog.showTitleWithMessageDialog(
                                surveys,
                                title,
                                message
                            )
                        } else {
                            showMessageDialog(
                                context, this@TopupActivity,
                                title,
                                message
                            )
                        }
                    } else {
                        showMessageDialog(
                            context, this@TopupActivity,
                            title,
                            message
                        )
                    }
                } else {
                    showMessageDialog(
                        context, this@TopupActivity,
                        title,
                        message
                    )
                }

            }  else {
                showMessageDialog(
                    applicationContext, this@TopupActivity,
                    title,
                    message
                )
            }
        } else {
            showMessageDialog(
                applicationContext, this@TopupActivity,
                resources.getString(R.string.popup_note_title),
                message
            )
        }
        LocalSharedPrefStorage(this@TopupActivity).addInt(
            LocalSharedPrefStorage.PREF_TOP_UP_PAGE,
            currentVisit
        )
    }
    private fun processTopUpResponse(t: TopupResponse?) {
        if (t != null) {
            if (t.data != null) {
                if (t.resultDesc != null && hasValue(t.resultDesc)) {

                    inAppFeedback(this@TopupActivity,resources.getString(R.string.popup_note_title),t.resultDesc)

                } else {
                    showMessageDialog(
                        applicationContext, this@TopupActivity,
                        "", getHardcodedNoResultDescFromServer()
                    )
                }
                UserDataManager.setIsGetHomePageCalledWithSuccess(false)
                removeHomePageResponseFromLocalPreferences()
            } else {
                showMessageDialog(
                    applicationContext, this@TopupActivity,
                    resources.getString(R.string.popup_error_title),
                    resources.getString(R.string.server_stopped_responding_please_try_again)
                )
            }
        } else {
            showMessageDialog(
                applicationContext, this@TopupActivity,
                resources.getString(R.string.popup_error_title),
                resources.getString(R.string.server_stopped_responding_please_try_again)
            )
        }
    }//processTopUpResponse ends

    private fun processSavedCardResponse(savedCardsResponse: SavedCardsResponse?) {

        if (savedCardsResponse?.data != null && !savedCardsResponse.data!!.cardDetails.isNullOrEmpty()
        ) {
            initUi(savedCardsResponse.data!!.cardDetails as ArrayList<SavedCardsItem>?)
        } else {
            initUi(ArrayList<SavedCardsItem>())
        }
    }//processSavedCardResponse ends

    private fun initUi(savedCardsResponse: ArrayList<SavedCardsItem>?) {

        isSavedCardsAvailable =
            savedCardsResponse != null && savedCardsResponse.isNotEmpty() && savedCardsResponse.size > 0

        savedCardsResponse?.let { setUpCardsRecyclerView(it) }

    }//initUi ends

    private fun initUiEvents() {
        azerActionBarBackIcon.setOnClickListener { onBackPressed() }
        nextButton.setOnClickListener {
            var msisdn = msisdnET.text.toString()
            var amount = amountET.text.toString()

            msisdn = msisdn.replace(ConstantsUtility.UserConstants.USERNAME_PREFIX, "")
            if (!hasValue(msisdn)) {
                showMessageDialog(
                    applicationContext, this@TopupActivity,
                    "", resources.getString(R.string.enter_mobile_number)
                )
            } else {
                if (!isValidMsisdn(msisdn)) {
                    showMessageDialog(
                        applicationContext, this@TopupActivity,
                        "", resources.getString(R.string.invalid_number)
                    )
                } else if (!hasValue(amount) || !isNumeric(amount)) {
                    showMessageDialog(
                        applicationContext, this@TopupActivity,
                        "", getString(R.string.error_msg_invalid_amount)
                    )
                } else {
                    if (isSavedCardsAvailable && hasValue(paymentKey)) {
                        var dialogMessage = ""

                        dialogMessage = if (ProjectApplication.mLocaleManager.getLanguage()
                                .equals(ConstantsUtility.LanguageKeywords.AZ, ignoreCase = true)
                        ) {
                            getString(R.string.payment_loading) + msisdn + " nömrəsinə " + amount + getString(
                                R.string.azn_to
                            )
                        } else {
                            getString(R.string.payment_loading) + amount + getString(R.string.azn_to) + msisdn + "."
                        }
                        showConfirmationDialog(applicationContext, this@TopupActivity,
                            "", dialogMessage, object : OnClickListener {
                                override fun onClick() {
                                    //call make payment api, check amount , card
                                    var makePaymentRequest =
                                        MakePaymentRequest(paymentKey, amount, msisdn)
                                    mViewModel.requestMakePayment(makePaymentRequest)
                                }
                            })
                    } else {
                        // amount
                        //initiate payment
                        if (UserDataManager.getPredefineData() != null && UserDataManager.getPredefineData()!!.topup?.plasticCard?.cardTypes != null
                            && UserDataManager.getPredefineData()!!.topup?.plasticCard?.cardTypes!!.isNotEmpty()
                        ) {
                            val spinnerTitlesList =
                                UserDataManager.getPredefineData()!!.topup?.plasticCard?.cardTypes
                            showCardTypeSelectionDialog(
                                this@TopupActivity,
                                this@TopupActivity,
                                getString(R.string.select_card),
                                spinnerTitlesList,
                                object : OnCardTypeClicked {
                                    override fun onCardClicked(cardKey: Long) {
                                        mViewModel.requestInitiatePayment(
                                            InitiatePaymentRequest(
                                                cardKey.toString(),
                                                amount,
                                                "" + false,
                                                msisdn
                                            )
                                        )
                                    }

                                })
                        }

                    }
                }
            }
        }

        radioGroupTopup.setOnCheckedChangeListener { group, checkedId ->
            val checkedRadioButton = group!!.findViewById(checkedId) as RadioButton
            val isChecked = checkedRadioButton.isChecked
            if (checkedId == R.id.radioScratchCard) {
                if (isChecked) {
                    payButton.visibility = View.VISIBLE
                    cardNumberTL.visibility = View.VISIBLE
                    manageScratchClick(View.GONE)
                    ivManat.visibility = View.GONE

//                    enableEditText(cardNumberET, R.color.colorFloatingHintsEnabled, applicationContext)
//                    setHintColor(cardNumberTL, R.color.colorFloatingHints, applicationContext)
                }
            } else {
                if (isChecked) {
                    payButton.visibility = View.GONE
                    cardNumberTL.visibility = View.GONE
                    manageScratchClick(View.VISIBLE)
                    ivManat.visibility = View.VISIBLE
//                    disableEditText(cardNumberET, R.color.colorFloatingHintsDisabled, applicationContext)
//                    setHintColor(cardNumberTL, R.color.colorFloatingHintsDisabled, applicationContext)
                }
            }
        }//radioGroupTopup.setOnCheckedChangeListener  ends

        payButton.setOnClickListener {
            requestTopUp()
        }//payButton.setOnClickListener ends

        cardNumberET.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if (event!!.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (payButton.isClickable == true) {
                        requestTopUp()
                    }
                }//if ends
                return false
            }//onKey ends
        })

        cardNumberET.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (payButton.isClickable == true) {
                        requestTopUp()
                    }
                }//if ends
                return false
            }//onEditorAction ends
        })

        cardNumberET.addTextChangedListener(object : TextWatcher {
            var isChageProg = false
            override fun afterTextChanged(s: Editable?) {
                if (!isChageProg) {
                    if (!isDelKeyPressed) {
                        isChageProg = true
                        val textFormated = getCreditCardFormatedText(s.toString())
                        cardNumberET.setText(textFormated)
                        cardNumberET.setSelection(cardNumberET.text!!.length)
                        isChageProg = false
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        cb_SaveCard.setOnCheckedChangeListener { buttonView, isChecked ->
            isSaveCard = isChecked
        }

        amountET.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(text: Editable) {
                val value = text.toString()
                val valueSplits = value.split("\\.".toRegex()).toTypedArray()
                if (value.length <= 2 && valueSplits.isNotEmpty() && hasValue(valueSplits[0]) && valueSplits[0] == "0") {
                    amountET.setText("")
                } else if (value.startsWith(".")) {
                    amountET.setText("")
                }
                amountET.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(100, 2))
            }
        })

    }//initUiEvents ends

    private fun manageScratchClick(visibility: Int) {
        if (isValAvailable) {
            selectCardTypeLabel.visibility = visibility
        } else {
            cardSelectionRV.visibility = visibility
            selectCardTypeLabel.visibility = visibility
        }
        nextButton.visibility = visibility
        amountTL.visibility = visibility
    }//manageScratchClick ends

    private fun setUpCardsRecyclerView(savedCardsItems: ArrayList<SavedCardsItem>) {
        //placeholder data for mock display
        savedCardsItems.add(SavedCardsItem("", "", "Add New Card", ""))
        //Adapter creation
        val topUpCardsAdapter =
            TopUpCardsAdapter(savedCardsItems, this@TopupActivity, object : TopUpItemSelectEvents {
                override fun onItemSelected(position: Int, searchKey: String) {
                    if (hasValue(searchKey)) {
                        paymentKey = searchKey
                    } else if (position != -1) {
                        val amountToInitiatePayment = amountET.text.toString()
                        var mobileNumber: String = msisdnET.text.toString()
                        var isValidNumber = true
                        var isValidAmount = true
                        val userMain = UserDataManager.getPredefineData()

                        mobileNumber =
                            mobileNumber.replace(ConstantsUtility.UserConstants.USERNAME_PREFIX, "")

                        if (!hasValue(mobileNumber)) {
                            isValidNumber = false
                            showMessageDialog(
                                applicationContext, this@TopupActivity,
                                resources.getString(R.string.popup_error_title),
                                resources.getString(R.string.enter_mobile_number)
                            )
                        }
                        if (isValidNumber && !isValidMsisdn(mobileNumber)) {
                            isValidNumber = false
                            showMessageDialog(
                                applicationContext, this@TopupActivity,
                                resources.getString(R.string.popup_error_title),
                                resources.getString(R.string.invalid_number)
                            )
                        }

                        if (isValidNumber && userMain != null && userMain.topup?.plasticCard?.cardTypes != null && userMain.topup.plasticCard.cardTypes!!.isNotEmpty()) {
                            /*       val bundle = Bundle()
                                   CardSelectionActivity.start(this@TopupActivity, mobileNumber, amount)*/


                            val spinnerTitlesList = userMain.topup.plasticCard.cardTypes
                            showCardTypeSelectionDialog(
                                this@TopupActivity,
                                this@TopupActivity,
                                getString(R.string.select_card),
                                spinnerTitlesList,
                                object : OnCardTypeClicked {
                                    override fun onCardClicked(cardKey: Long) {
                                        mViewModel.requestInitiatePayment(
                                            InitiatePaymentRequest(
                                                cardKey.toString(),
                                                "",
                                                "" + isSaveCard,
                                                mobileNumber
                                            )
                                        )
                                    }

                                })
                        }

                    } else {
                        paymentKey = ""
                    }
                }
            }).also {
                it.onDelete = { item, position ->
                    deletedPosition = position
                    val deleteSavedCardRequest =
                        DeleteSavedCardRequest(item.id)
                    mViewModel.requestDeleteSavedCard(deleteSavedCardRequest)
                }
            }
        if (savedCardsItems.isNotEmpty() && savedCardsItems[0] != null) {
            paymentKey = savedCardsItems[0].paymentKey.toString()
        }

        cardSelectionRV.layoutManager = LinearLayoutManager(this)
//        if (savedCardsItems.size > 1) {
//            val itemTouchHelper =
//                ItemTouchHelper(enableSwipeToDeleteAndUndo(topUpCardsAdapter, savedCardsItems)!!)
//            itemTouchHelper.attachToRecyclerView(cardSelectionRV)
//        }
        cardSelectionRV.adapter = topUpCardsAdapter

        cb_SaveCard.visibility = View.GONE
//        spinnerCL.visibility = View.GONE
        cardSelectionRV.visibility = View.VISIBLE
        selectCardTypeLabel.text = getString(R.string.top_up_lbl_select_card)
    }//setUpCardsRecyclerView ends

    private fun getCreditCardFormatedText(word: String): String {
        var string = word
        string = string.replace("-", "")
        if (hasValue(string)) {
            var s1 = ""
            var s2 = ""
            var s3 = ""
            if (string.length >= 4) {
                s1 = string.substring(0, 4)
            }

            if (string.length >= 9) {
                s2 = string.substring(4, 9)
            } else if (string.length >= 5) {
                s2 = string.substring(4, string.length)
            }

            if (string.length > 9) {
                s3 = string.substring(9, string.length)
            }

            if (!s1.equals("", ignoreCase = true)) {
                string = s1
            }
            if (!s2.equals("", ignoreCase = true)) {
                string = "$string-$s2"
            }
            if (!s3.equals("", ignoreCase = true)) {
                string = "$string-$s3"
            }
        }
        return string
    }//getCreditCardFormatedText ends

    private fun showConfirmationPopup() {
        if (this@TopupActivity.isFinishing) return

        val dialogBuilder = AlertDialog.Builder(this@TopupActivity)
        val inflater =
            applicationContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val dialogView = inflater.inflate(R.layout.azer_topup_confirmation_popup, null)
        dialogBuilder.setView(dialogView)

        val alertDialog = dialogBuilder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.show()

        var recieverNumber = msisdnET.text.toString()
        recieverNumber = recieverNumber.replace(ConstantsUtility.UserConstants.USERNAME_PREFIX, "")

        var message = getLocalizedConfirmationMessage()
        message = message.replace("XXXXXXX", recieverNumber)

        val title = dialogView.findViewById(R.id.popupTitle) as TextView
        title.text = message


        val popupOkayButton = dialogView.findViewById(R.id.yesButton) as Button
        val popupCancelButton = dialogView.findViewById(R.id.noButton) as Button
        popupOkayButton.text = getString(R.string.popup_text_update_ok)

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN) {
            //english
            popupCancelButton.text = "No"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ) {
            //azri
            popupCancelButton.text = "Xeyr"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.RU) {
            //russian
            popupCancelButton.text = "Нет"
        }


        popupOkayButton.setOnClickListener {
            alertDialog.dismiss()

            processTopUp()

        }

        popupCancelButton.setOnClickListener {
            alertDialog.dismiss()

        }
    }//showConfirmationPopup ends

    private fun getLocalizedConfirmationMessage(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "Are you sure you want to top up XXXXXXX?"
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "XXXXXXX balansını artırmaq istədiyindən əminsən?"
            else -> "Ты уверен, что хочешь пополнить баланс XXXXXXX?"
        }
    }//getLocalizedConfirmationMessage ends

    private fun processTopUp() {
        if (UserDataManager.getCustomerData() != null) {
            var subscribeType = ""
            if (hasValue(UserDataManager.getCustomerData()!!.subscriberType)) {
                subscribeType = UserDataManager.getCustomerData()!!.subscriberType!!
            }

            var recieverNumber = msisdnET.text.toString()
            recieverNumber =
                recieverNumber.replace(ConstantsUtility.UserConstants.USERNAME_PREFIX, "")

            if (!isValidMsisdn(recieverNumber)) {
                showMessageDialog(
                    applicationContext, this@TopupActivity,
                    resources.getString(R.string.popup_error_title),
                    resources.getString(R.string.error_msg_invalid_number)
                )
                return
            }
            if (!hasValue(recieverNumber)) {
                showMessageDialog(
                    applicationContext, this@TopupActivity,
                    resources.getString(R.string.popup_error_title),
                    resources.getString(R.string.enter_mobile_number)
                )
                return
            }

            var cardNumber = cardNumberET.text.toString()
            if (!hasValue(cardNumber) || cardNumber.length != 15) {
                showMessageDialog(
                    applicationContext, this@TopupActivity,
                    resources.getString(R.string.popup_error_title),
                    resources.getString(R.string.topup_invalid_scratch_card)
                )
                return
            }

            mViewModel.requestTopUp(cardNumber.replace("-", ""), recieverNumber, subscribeType)
        } else {
            showMessageDialog(
                applicationContext, this@TopupActivity,
                resources.getString(R.string.popup_error_title),
                resources.getString(R.string.error_message_no_customer_data)
            )
        }
    }//processTopUp ends

    private fun requestTopUp() {
        hideKeyboard(this@TopupActivity)
        var msisdn = msisdnET.text.toString()
        msisdn = msisdn.replace(ConstantsUtility.UserConstants.USERNAME_PREFIX, "")
        if (!hasValue(msisdn)) {
            showMessageDialog(
                applicationContext, this@TopupActivity,
                "", resources.getString(R.string.enter_mobile_number)
            )
            return
        } else {
            if (!isValidMsisdn(msisdn)) {
                showMessageDialog(
                    applicationContext, this@TopupActivity,
                    "", resources.getString(R.string.invalid_number)
                )
                return
            }
        }
        showConfirmationPopup()
    }//requestTopUp ends

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.topup_screen_title)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, TopupActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends
    }//object ends

    private fun enableSwipeToDeleteAndUndo(
        topUpCardsAdapter: TopUpCardsAdapter,
        savedCardsItems: ArrayList<SavedCardsItem>
    ): ItemTouchHelper.SimpleCallback? {
        return object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
                //Remove swiped item from list and notify the RecyclerView
                val position = viewHolder.adapterPosition
                if (savedCardsItems != null && position != savedCardsItems.size - 1 && savedCardsItems[position] != null && hasValue(
                        savedCardsItems[position].id
                    )
                ) {
                    deletedPosition = position
                    val deleteSavedCardRequest =
                        DeleteSavedCardRequest(savedCardsItems[position].id)
                    mViewModel.requestDeleteSavedCard(deleteSavedCardRequest)

                } else {
                    topUpCardsAdapter.notifyItemChanged(position)
                }
            }
        }
    }//enableSwipeToDeleteAndUndo ends
}//class ends