package com.azarphone.ui.activities.loan

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.azarphone.R
import com.azarphone.SingleLiveEvent
import com.azarphone.analytics.Analytics
import com.azarphone.analytics.EventValues
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.request.GetLoanRequest
import com.azarphone.api.pojo.request.PaymentHistoryRequest
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.loan.GetLoanResponse
import com.azarphone.api.pojo.response.paymenthistory.PaymentHistoryResponse
import com.azarphone.api.pojo.response.requesthistory.RequestHistoryResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.ui.dialogs.InAppThankYouDialog
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.logE
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

/**
 * @author Junaid Hassan on 05, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class LoanViewModel(val mLoanRepositry: LoanRepositry) : BaseViewModel() {

    private val logKey = "loangetX67"
    private val fromClass = "LoanViewModel"
    private lateinit var disposable: Disposable
    val getLoanResponseLiveData = MutableLiveData<GetLoanResponse>()
    val paymentHistoryResponseLiveData = SingleLiveEvent<PaymentHistoryResponse>()
    val requestHistoryResponseLiveData = SingleLiveEvent<RequestHistoryResponse>()
    val inAppSurveyResponseErrorResponseData = MutableLiveData<AzerAPIErrorHelperResponse>()

    init {

    }//init ends
    fun uploadInAppSurvey(context: Context, uploadSurvey: UploadSurvey, onTransactionComplete: String) {
        view()?.showLoading()
        var observable = mLoanRepositry.saveInAppSurvey(uploadSurvey)
        observable = observable.onErrorReturn { throwable -> null }
        disposable = observable
            .compose(applyIOSchedulers())
            .subscribe(
                { appSurvey ->
                    view()?.hideLoading()
                    when {
                        appSurvey?.data != null -> {
                            UserDataManager.setInAppSurvey(appSurvey)
                            val thankYouDialog =
                                InAppThankYouDialog(context, onTransactionComplete)
                            thankYouDialog.show()
                        }
                        appSurvey?.resultDesc != null -> {
                            inAppSurveyResponseErrorResponseData.postValue(
                                AzerAPIErrorHelperResponse(
                                    ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                    appSurvey.resultDesc
                                )
                            )
                        }
                        else -> {
                            inAppSurveyResponseErrorResponseData.postValue(
                                AzerAPIErrorHelperResponse(
                                    ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                    context.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            )
                        }
                    }
                },
                { error ->
                    inAppSurveyResponseErrorResponseData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            error.message
                        )
                    )
                }
            )
        compositeDisposables?.add(disposable)
    }
    fun requestPaymentHistory(startDate: String, endDate: String) {
        logE(logKey, "requestPaymentHistory Called with startDate:::".plus(startDate).plus(" endDate").plus(endDate), fromClass, "requestGetLoan")
        view()?.showLoading()
        val paymentHistoryRequest = PaymentHistoryRequest(startDate, endDate)

        var mObserver = mLoanRepositry.requestPaymentHistory(paymentHistoryRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    logE(logKey, "response:::".plus(result.toString()), fromClass, "requestGetLoan")
                                    paymentHistoryResponseLiveData.postValue(result)
                                }
                            }
                        },
                        { error ->
                            UserDataManager.setPaymentHistoryResponse(null)
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestPaymentHistory ends

    fun requestHistory(startDate: String, endDate: String) {
        logE(logKey, "requestHistory Called with startDate:::".plus(startDate).plus(" endDate").plus(endDate), fromClass, "requestHistory")
        view()?.showLoading()
        val paymentHistoryRequest = PaymentHistoryRequest(startDate, endDate)

        var mObserver = mLoanRepositry.requestHistory(paymentHistoryRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    logE(logKey, "response:::".plus(result.toString()), fromClass, "requestGetLoan")
                                    requestHistoryResponseLiveData.postValue(result)
                                }
                            }
                        },
                    { error ->
                        UserDataManager.setRequestHistoryResponse(null)
                        view()?.hideLoading()
                        view()?.loadError(error)
                    }
                )

        compositeDisposables?.add(disposable)
    }//requestPaymentHistory ends

    fun requestGetLoan(friendMsisdn: String, me: Boolean, amountVal: String) {
        logE(
                logKey,
                "requestLoanApi Called with msisdn:::$friendMsisdn",
                fromClass,
                "requestGetLoan"
        )
        view()?.showLoading()
        val getLoanRequest = GetLoanRequest(friendMsisdn, amountVal)

        var mObserver = mLoanRepositry.requestGetLoan(getLoanRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
            .compose(applyIOSchedulers())
            .subscribe(
                { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    logE(logKey, "response:::".plus(result.toString()), fromClass, "requestGetLoan")
                                    getLoanResponseLiveData.postValue(result)
                                    if(me) {
                                        Analytics.logAppEvent(EventValues.GetCreditEvents.get_credit, EventValues.GetCreditEvents.me_credit, EventValues.GenericValues.Success)
                                      //  Analytics.logGetCreditEvent("get_credit",true)
                                    }else{
                                        Analytics.logAppEvent(EventValues.GetCreditEvents.get_credit, EventValues.GetCreditEvents.friends_credit, EventValues.GenericValues.Success)
                                    }

                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                            if(me) {
                                Analytics.logAppEvent(EventValues.GetCreditEvents.get_credit, EventValues.GetCreditEvents.me_credit, EventValues.GenericValues.Failure)
                            }else{
                                Analytics.logAppEvent(EventValues.GetCreditEvents.get_credit, EventValues.GetCreditEvents.friends_credit, EventValues.GenericValues.Failure)
                            }
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestGetLoan
}//class ends