package com.azarphone.ui.activities.operationshistory

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.View
import android.widget.AdapterView
import android.widget.TextView
import androidx.lifecycle.Observer
import com.azarphone.R
import com.azarphone.api.pojo.response.operationshistory.OperationHistoryResponse
import com.azarphone.api.pojo.response.operationshistory.RecordsItem
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.LayoutOperationHistoryActivityBinding
import com.azarphone.ui.adapters.expandablelists.OperationHistoryAdapter
import com.azarphone.ui.adapters.spinners.OperationsHistorySpinnerAdapter
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.widgets.calendar.AzerCalendar
import com.azarphone.widgets.popups.showMessageDisclaimerDialog
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.layout_operation_history_activity.*
import kotlinx.android.synthetic.main.no_data_found.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * @author Junaid Hassan on 08, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class OperationsHistoryActivity : BaseActivity<LayoutOperationHistoryActivityBinding, OperationsHistoryFactory, OperationsHistoryViewModel>() {

    private var transitionType = ""
    private val fromClass = "OperationsHistoryActivity"
    private var date = ""
    private var startDate = ""
    private var endDate = ""
    private var dateOne = ""
    private var dateTwo = ""
    private var flagDate = false
    private var logKey = "operationHistory34Z"
    private var operationHistoryAdapter: OperationHistoryAdapter? = null
    private var operationHistoryResponse: OperationHistoryResponse? = null
    private var filterList = ArrayList<RecordsItem?>()
    private val TRANSITION_ALL_EN = "All"
    private val TRANSITION_ALL_RU = "Все"
    private val TRANSITION_ALL_AZ = "Hamısı"

    private val TRANSITION_OTHERS_EN = "Others"
    private val TRANSITION_OTHERS_RU = "Другие"
    private val TRANSITION_OTHERS_AZ = "Digərləri"

    override fun getLayoutId(): Int {
        return R.layout.layout_operation_history_activity
    }

    override fun getViewModelClass(): Class<OperationsHistoryViewModel> {
        return OperationsHistoryViewModel::class.java
    }

    override fun getFactory(): OperationsHistoryFactory {
        return OperationsHistoryInjection.provideOperationsHistoryFactory()
    }

    override fun init() {
        subscribe()
        initUI()
        initUIEvents()
        onCreateViewProcess()
    }

    private fun showDisclaimerDialog() {
        if (getDisclaimerCheckFromLocalPreferences("o"))
            showMessageDisclaimerDialog(applicationContext, this@OperationsHistoryActivity, getString(R.string.lbl_disclaimer), getString(R.string.message_disclaimer_message), "o")
    }

    private fun onCreateViewProcess() {
        try {
            //set the default dates for the spinner period date views
            setDefaultDates()

            //get the date
            date = getLastDateMinusDays(getCurrentDate(), ConstantsUtility.DateFormats.CURRENT_DAY)
            updateDates(date, getCurrentDate())
        } catch (exp: Exception) {
            logE(logKey, "error:::".plus(exp.toString()), fromClass, "initUI")
        }
    }//onCreateViewProcess ends

    private fun setDefaultDates() {
        dateOneView.text = changeDateFormatForTextView(getCurrentDate())
        dateTwoView.text = changeDateFormatForTextView(getCurrentDate())
    }//setDefaultDates ends

    private fun initUIEvents() {
        categoriesSpinnerHolder.setOnClickListener {
            spinnerCategory.performClick()
        }

        periodSpinnerHolder.setOnClickListener {
            spinnerPeriod.performClick()
        }

        dateOneView.setOnClickListener {
            showCalendar(dateOneView, true)//false for max date
        }//dateOne.setOnClickListener  ends

        dateTwoView.setOnClickListener {
            showCalendar(dateTwoView, false)//true for min date
        }

        spinnerCategory.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                transitionType = spinnerCategory.selectedItem.toString()
                filterUsageDetails(transitionType)
            }
        }

        spinnerPeriod.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                //do nothing
            }//onNothingSelected ends

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position == 0) {
                    //current day
                    try {
                        if (flagDate) {
                            calendarLayout.visibility = View.GONE
                            updateDates(getCurrentDate(), getCurrentDate())
                        } else {
                            flagDate = true
                        }
                    } catch (exp: Exception) {
                        logE(logKey, "error1:::".plus(exp.toString()), fromClass, "onItemSelected")
                    }
                } else if (position == 1) {
                    //last seven days
                    try {
                        calendarLayout.visibility = View.GONE
                        date = ""
                        date = getLastDateMinusDays(getCurrentDate(), ConstantsUtility.DateFormats.LAST_SEVEN_DAY)
                        updateDates(date, getCurrentDate())
                    } catch (exp: Exception) {
                        logE(logKey, "error2:::".plus(exp.toString()), fromClass, "onItemSelected")
                    }
                } else if (position == 2) {
                    //last 30 days
                    try {
                        calendarLayout.visibility = View.GONE
                        date = ""
                        date = getLastDateMinusDays(getCurrentDate(), ConstantsUtility.DateFormats.LAST_THIRTY_DAY)
                        updateDates(date, getCurrentDate())
                    } catch (exp: Exception) {
                        logE(logKey, "error3:::".plus(exp.toString()), fromClass, "onItemSelected")
                    }
                } else if (position == 3) {
                    //previous month
                    try {
                        calendarLayout.visibility = View.GONE
                        date = ""
                        var maxDate = ""
                        date = getLastMonthFirstDate()
                        maxDate = getLastMonthLastDate()
                        updateDates(date, maxDate)
                    } catch (exp: Exception) {
                        logE(logKey, "error4:::".plus(exp.toString()), fromClass, "onItemSelected")
                    }
                } else if (position == 4) {
                    //custom dates
                    try {
                        setDefaultDates()
                        dateOneView.setTextColor(resources.getColor(R.color.colorTextGray))
                        dateTwoView.setTextColor(resources.getColor(R.color.colorTextGray))
                        calendarLayout.visibility = View.VISIBLE
                    } catch (exp: Exception) {
                        logE(logKey, "error5:::".plus(exp.toString()), fromClass, "onItemSelected")
                    }
                } else {
                    calendarLayout.visibility = View.GONE
                }
            }//onItemSelected ends
        }

        azerActionBarBackIcon.setOnClickListener { onBackPressed() }
    }//initUIEvents ends

    @Throws(ParseException::class)
    fun showCalendar(textViewNormal: TextView, isStartDateSelected: Boolean) {

        try {
            dateOne = changeDateFormatForMethods(dateOneView.text.toString())
            dateTwo = changeDateFormatForMethods(dateTwoView.text.toString())

            val minDate = SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd, Locale.ENGLISH).parse(dateOne)
            val maxDate = SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd, Locale.ENGLISH).parse(dateTwo)

            val azerCalendar: AzerCalendar

            val calendar = Calendar.getInstance()
            if (isStartDateSelected) {
                calendar.time = minDate
            } else {
                calendar.time = maxDate
            }
            val year = calendar.get(Calendar.YEAR)
            val month = calendar.get(Calendar.MONTH)
            val day = calendar.get(Calendar.DAY_OF_MONTH)

            azerCalendar = AzerCalendar(textViewNormal, ondate)


            val datePickerDialog = DatePickerDialog(this@OperationsHistoryActivity, azerCalendar, year, month, day)
            //calculate min and max dates (for older versions use System Current TimeMillis

            if (isStartDateSelected) {

                val calndPre = Calendar.getInstance()
                calndPre.add(Calendar.MONTH, -3)
                calndPre.set(Calendar.DATE, 1)
                datePickerDialog.datePicker.minDate = calndPre.timeInMillis

                val dateMax = SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd, Locale.ENGLISH).parse(dateTwo)
                calendar.time = dateMax
                datePickerDialog.datePicker.maxDate = calendar.timeInMillis

            } else {

                val dateMin = SimpleDateFormat(ConstantsUtility.DateFormats.yyyyDASHmmDASHdd, Locale.ENGLISH).parse(dateOne)
                calendar.time = dateMin
                datePickerDialog.datePicker.minDate = calendar.timeInMillis
                datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
            }

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                datePickerDialog.setTitle("")//Prevent Date picker from creating extra Title.!
            }

            datePickerDialog.show()
        } catch (e: ParseException) {
            logE(logKey, "error:::".plus(e.toString()), fromClass, "showCalendar")
        }
    }//showCalendar ends

    private var ondate: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        try {
            dateOne = changeDateFormatForMethods(dateOneView.text.toString())
        } catch (e: ParseException) {
            logE(logKey, "error1:::".plus(e.toString()), fromClass, "ondate")
        }

        try {
            dateTwo = changeDateFormatForMethods(dateTwoView.text.toString())
        } catch (e: ParseException) {
            logE(logKey, "error2:::".plus(e.toString()), fromClass, "ondate")
        }

        updateDates(dateOne, dateTwo)
    }//ondate ends

    private fun updateDates(newStartDate: String, newEndDate: String) {
        startDate = newStartDate
        endDate = newEndDate

        var formattedStartedDate = ""
        if (hasValue(changeDateFormat(startDate))) {
            formattedStartedDate = changeDateFormat(startDate)!!
        }

        var formattedEndDate = ""
        if (hasValue(changeDateFormat(endDate))) {
            formattedEndDate = changeDateFormat(endDate)!!
        }

        //call the api here
        mViewModel.requestOperationsHistory(formattedStartedDate, formattedEndDate)
    }//updateDates ends

    private fun initUI() {
        setupHeaderActionBar()
        amountTitle.isSelected = true
        descriptionTitle.isSelected = true
        statusTitle.isSelected = true
        dateTimeTitle.isSelected = true
        spinnerCategoryHeading.isSelected = true
        spinnerPeriodHeading.isSelected = true

        val periodArray = resources.getStringArray(R.array.period_filter)
        spinnerPeriod.adapter = OperationsHistorySpinnerAdapter(applicationContext, periodArray)

        val categoriesArray = resources.getStringArray(R.array.transation_type_filter)
        spinnerCategory.adapter = OperationsHistorySpinnerAdapter(applicationContext, categoriesArray)
    }//initUI ends

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.operations_history_title)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

    private fun subscribe() {
        val operationsHistoryResponseLiveData = object : Observer<OperationHistoryResponse> {
            override fun onChanged(reponse: OperationHistoryResponse?) {
                if (reponse != null) {
                    logE(logKey, "response:::".plus(reponse.toString()), fromClass, "subscribe")
                    if (reponse.data.records != null && !reponse.data.records!!.isEmpty()) {
                        showContents()
                        operationHistoryResponse = null
                        operationHistoryResponse = reponse
                        populateUI(reponse.data.records!!)
                        filterUsageDetails(transitionType)
                        showDisclaimerDialog()
                    } else {
                        hideContents()
                    }
                } else {
                    hideContents()
                }
            }
        }

        mViewModel.operationsHistoryResponseLiveData.observe(this@OperationsHistoryActivity, operationsHistoryResponseLiveData)
    }//subscribe ends

    private fun filterUsageDetails(item: String) {
        if (!hasValue(item)) return

        if (item.equals(TRANSITION_ALL_EN, ignoreCase = true) || item.equals(TRANSITION_ALL_AZ, ignoreCase = true)
                || item.equals(TRANSITION_ALL_RU, ignoreCase = true)) {
            if (operationHistoryResponse != null && operationHistoryResponse!!.data != null && operationHistoryResponse!!.data.records != null) {
                populateUI(operationHistoryResponse!!.data.records!!)
            }
        } else if (item.equals(TRANSITION_OTHERS_EN, ignoreCase = true) || item.equals(TRANSITION_OTHERS_AZ, ignoreCase = true)
                || item.equals(TRANSITION_OTHERS_RU, ignoreCase = true)) {
            if (operationHistoryResponse == null) return
            filterList.clear()
            val filterArray = resources.getStringArray(R.array.transation_type_filter)
            val hashMap = HashMap<String, String>()
            val list = Arrays.asList(*filterArray)

            for (i in 0 until list.size - 1) {
                hashMap[list[i].toLowerCase()] = list[i]
            }
            if (operationHistoryResponse != null && operationHistoryResponse!!.data != null && operationHistoryResponse!!.data.records != null) {
                for (i in 0 until operationHistoryResponse!!.data.records!!.size) {
                    if (operationHistoryResponse!!.data.records!![i] != null
                            && hasValue(operationHistoryResponse!!.data.records!![i]!!.transactionType)) {
                        if (hashMap.containsKey(operationHistoryResponse!!.data.records!![i]!!.transactionType!!.toLowerCase())) {

                        } else {
                            filterList.add(operationHistoryResponse!!.data.records!![i])
                        }
                    }
                }//for ends

                if (filterList.isNullOrEmpty()) {
                    noDataFoundLayout.visibility = View.VISIBLE
                    operationsHistoryList.visibility = View.GONE
                    noDataFoundTextLabelBlack.text = getNoDataFoundOnSearchMessage()
                    noDataFoundTextLabelBlack.isSelected = true
                } else {
                    showContents()
                    populateUI(filterList)
                }
            }
        } else {
            filterList.clear()
            if (operationHistoryResponse != null && operationHistoryResponse!!.data != null && operationHistoryResponse!!.data.records != null) {
                for (i in 0 until operationHistoryResponse!!.data.records!!.size) {
                    if (operationHistoryResponse!!.data.records!![i] != null
                            && hasValue(operationHistoryResponse!!.data.records!![i]!!.transactionType)) {
                        if (item.equals(operationHistoryResponse!!.data.records!![i]!!.transactionType!!.trim(), ignoreCase = true)) {
                            filterList.add(operationHistoryResponse!!.data.records!![i])
                        }
                    }
                }

                if (filterList.isNullOrEmpty()) {
                    noDataFoundLayout.visibility = View.VISIBLE
                    operationsHistoryList.visibility = View.GONE
                    noDataFoundTextLabelBlack.text = getNoDataFoundOnSearchMessage()
                    noDataFoundTextLabelBlack.isSelected = true
                } else {
                    showContents()
                    populateUI(filterList)
                }
            }
        }
    }//filterUsageDetails ends

    private fun populateUI(records: List<RecordsItem?>) {
        operationHistoryAdapter = OperationHistoryAdapter(applicationContext, this@OperationsHistoryActivity, records)
        operationsHistoryList.setAdapter(operationHistoryAdapter)
    }//populateUI ends

    private fun hideContents() {
        noDataFoundLayout.visibility = View.VISIBLE
        operationsHistoryList.visibility = View.GONE
        noDataFoundTextLabelBlack.text = getNoDataFoundSimpleMessage()
        noDataFoundTextLabelBlack.isSelected = true
    }//hideContents ends

    private fun showContents() {
        noDataFoundLayout.visibility = View.GONE
        operationsHistoryList.visibility = View.VISIBLE
    }//showContents ends

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, OperationsHistoryActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends
    }//object ends

}//class ends