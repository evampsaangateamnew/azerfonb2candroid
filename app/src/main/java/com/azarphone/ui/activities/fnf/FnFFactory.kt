package com.azarphone.ui.activities.fnf

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class FnFFactory(val fnfRepositry: FnFRepositry) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(FnFViewModel::class.java)) {
            return FnFViewModel(fnfRepositry) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}