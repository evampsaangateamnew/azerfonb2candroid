package com.azarphone.ui.activities.operationshistory

import androidx.lifecycle.MutableLiveData
import com.azarphone.api.pojo.request.OperationsHistoryRequest
import com.azarphone.api.pojo.response.operationshistory.OperationHistoryResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.util.logE
import com.azarphone.validators.hasValue
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

/**
 * @author Junaid Hassan on 08, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class OperationsHistoryViewModel(val mOperationsHistoryRepositry: OperationsHistoryRepositry) : BaseViewModel() {

    private val logKey = "opHis"
    private val fromClass = "TopupViewModel"
    private lateinit var disposable: Disposable
    val operationsHistoryResponseLiveData = MutableLiveData<OperationHistoryResponse>()

    init {

    }//class

    fun requestOperationsHistory(startDate: String, endDate: String) {
        var accountId = ""
        var customerId = ""
        if (UserDataManager.getCustomerData() != null) {
            if (UserDataManager.getCustomerData()!!.accountId != null
                    && hasValue(UserDataManager.getCustomerData()!!.accountId)) {
                accountId = UserDataManager.getCustomerData()!!.accountId!!
            }

            if (UserDataManager.getCustomerData()!!.customerId != null
                    && hasValue(UserDataManager.getCustomerData()!!.customerId)) {
                customerId = UserDataManager.getCustomerData()!!.customerId!!
            }
        }
        logE(logKey, "calling Op History:::StartDate:::".plus(startDate).plus(" endDate:::")
                .plus(endDate).plus(" accountId:::").plus(accountId)
                .plus(" accountId:::").plus(accountId), fromClass, "requestOperationsHistory")

        view()?.showLoading()
        val operationsHistoryRequest = OperationsHistoryRequest(startDate, endDate, accountId, customerId)

        var mOpertionObserver= mOperationsHistoryRepositry.requestOperationsHistory(operationsHistoryRequest)
        mOpertionObserver=mOpertionObserver.onErrorReturn ({ throwable -> null } )
        disposable = mOpertionObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    operationsHistoryResponseLiveData.postValue(result)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestOperationsHistory ends
}