package com.azarphone.ui.activities.newautopayment

import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.Observer
import com.azarphone.R
import com.azarphone.api.pojo.request.AddPaymentSchedulerRequest
import com.azarphone.api.pojo.response.autopaymentresponse.daily.AddSchedulePaymentResponse
import com.azarphone.api.pojo.response.topup.savedcards.SavedCardsResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.ActivityAutoPaymentNewBinding
import com.azarphone.eventhandler.PaymentSchedulerClickListener
import com.azarphone.ui.adapters.pagers.AutoPaymentPagerAdapter
import com.azarphone.ui.fragment.newautopayment.daily.DailyAutoPaymentFragment
import com.azarphone.ui.fragment.newautopayment.monthly.MonthlyAutoPaymentFragment
import com.azarphone.ui.fragment.newautopayment.weekly.WeeklyAutoPaymentFragment
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.widgets.popups.showMessageDialogWithOkCallBack
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_auto_payment_new.*
import kotlinx.android.synthetic.main.activity_auto_payment_new.tabLayout
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.internet_fragment_layout.*

class AutoPaymentNewActivity :
    BaseActivity<ActivityAutoPaymentNewBinding, NewAutoPaymentFactory, NewAutoPaymentViewModel>() {


    private var autoPaymentPagerAdapter: AutoPaymentPagerAdapter? = null

    override fun getLayoutId(): Int {
        return R.layout.activity_auto_payment_new
    }

    override fun getViewModelClass(): Class<NewAutoPaymentViewModel> {
        return NewAutoPaymentViewModel::class.java
    }

    override fun getFactory(): NewAutoPaymentFactory {
        return NewAutoPaymentInjection.provideNewAutoPaymentFactory()
    }

    override fun init() {
        logE("abc8734", "reached init", "AutoPaymentNewActivity", "init")
        subscribe()
        setupHeaderActionBar()
        mViewModel.requestTopUpCards()
        intiUiEvents()
        setUpTabs()
    }

    override fun onResume() {
        initializePaymentSchedulerListener()
        super.onResume()

    }

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text =
            resources.getString(R.string.lbl_auto_payment) // lbl_schedule_payment
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

    private fun intiUiEvents() {

        azerActionBarBackIcon.setOnClickListener { onBackPressed() }

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

                for (i in 0 until tabLayout.tabCount) {
                    val tabView = tabLayout.getTabAt(i)
                    if (tabView != null && tabView.customView != null) {
                        val tabTextView = tabView.customView as TextView
                        setUnSelectedAutoPaymentTabFont(applicationContext!!, tabTextView)
                    }
                }

//                if (tab != null && tab.customView != null) {
//                    val tabTextView = tab.customView as TextView
//                    setUnSelectedAutoPaymentTabFont(applicationContext, tabTextView)
//                }
            }//onTabUnselected ends

            override fun onTabSelected(tab: TabLayout.Tab?) {
                autoViewpager.setCurrentItem(tab!!.position, false)
                if (tab != null && tab.customView != null) {
                    val tabTextView = tab.customView as TextView
                    setSelectedAutoPaymentTabFont(applicationContext, tabTextView)
                }
            }//onTabSelected ends
        })//tabLayout.addOnTabSelectedListener ends
    }

    private fun setUpTabs() {
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.add_auto_payment_daily)))
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.add_auto_payment_weekly)))
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.add_auto_payment_monthly)))

        tabLayout.getTabAt(0)!!.select()

        for (i in 0 until tabLayout.tabCount) {
            val tab = (tabLayout.getChildAt(0) as ViewGroup).getChildAt(i)
            val p = tab.layoutParams as ViewGroup.MarginLayoutParams
            val margin = (15 * resources.displayMetrics.density)
            p.setMargins(0, 0, margin.toInt(), 0)
            tab.requestLayout()

            //set the custom view
            val tabView = tabLayout.getTabAt(i)
            if (tabView != null) {
                val tabTextView = TextView(this)
                tabView.customView = tabTextView
                //set the text
                tabTextView.text = tabView.text
                tabTextView.ellipsize = TextUtils.TruncateAt.MARQUEE
                tabTextView.isSelected = true
                tabTextView.marqueeRepeatLimit = -1
                tabTextView.isSingleLine = true
                tabTextView.layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                tabTextView.gravity = Gravity.CENTER
                //set the currently selected tab font
                if (i == 0) {
                    setSelectedAutoPaymentTabFont(applicationContext, tabTextView)
                } else {
                    setUnSelectedAutoPaymentTabFont(applicationContext, tabTextView)
                }
            }
        }

    }

    private fun subscribe() {
        //Get Saved Cards
        val savedCardsLiveData = object : Observer<SavedCardsResponse> {
            override fun onChanged(t: SavedCardsResponse?) {
                if (t != null) {
                    noDataFound.visibility = View.GONE
                    autoViewpager.visibility = View.VISIBLE
                    setUpViewPager(t)
                    RootValues.setSavedCardsResponse(t)
                } else {
                    noDataFound.visibility = View.VISIBLE
                    autoViewpager.visibility = View.GONE
                }
            }
        }
        mViewModel.savedCardsResponseLiveData.observe(this, savedCardsLiveData)


        //Schedule Paymnt
        val addSchedulePaymentLiveData = object : Observer<AddSchedulePaymentResponse> {
            override fun onChanged(t: AddSchedulePaymentResponse?) {
                processAddSchedulePaymentLiveData(t)
            }
        }

        mViewModel.addPaymentSchedulerResponseLiveData.observe(this, addSchedulePaymentLiveData)
    }

    private fun processAddSchedulePaymentLiveData(response: AddSchedulePaymentResponse?) {

        var responseDescription = ""
        if (!this@AutoPaymentNewActivity.isFinishing && response != null &&
            hasValue(response.resultDesc)
        ) {
            responseDescription = response.resultDesc
        }

        if (!this@AutoPaymentNewActivity.isFinishing && response != null) {
            showMessageDialogWithOkCallBack(this@AutoPaymentNewActivity,
                this@AutoPaymentNewActivity,
                getString(R.string.app_name),
                responseDescription,
                object : OnClickListener {
                    override fun onClick() {
                        init()
                    }

                })
        }
    }

    private fun setUpViewPager(t: SavedCardsResponse?) {
        val pagerFragmentsList = ArrayList<androidx.fragment.app.Fragment>()
        pagerFragmentsList.add(DailyAutoPaymentFragment.getInstance(t))
        pagerFragmentsList.add(WeeklyAutoPaymentFragment.getInstance(t))
        pagerFragmentsList.add(MonthlyAutoPaymentFragment.getInstance(t))

        autoPaymentPagerAdapter =
            AutoPaymentPagerAdapter(supportFragmentManager, pagerFragmentsList)
        autoViewpager.adapter = autoPaymentPagerAdapter
    }

    private fun initializePaymentSchedulerListener() {
        val paymentSchedulerClickListener: PaymentSchedulerClickListener
        paymentSchedulerClickListener = object : PaymentSchedulerClickListener {
            override fun paymentSchedulerClickListener(
                amount: String,
                recurrence: String,
                billingCycleDaily: String,
                savedCardId: String,
                dateForAPI: String
            ) {
                val addPaymentSchedulerRequest = AddPaymentSchedulerRequest(
                    amount = amount, billingCycle = billingCycleDaily,
                    startDate = dateForAPI, recurrenceNumber = recurrence, savedCardId = savedCardId
                )
                mViewModel.requestPaymentScheduler(addPaymentSchedulerRequest)
            }
        }

        RootValues.setPymentSchedlerClickListener(paymentSchedulerClickListener)
    }

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, AutoPaymentNewActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends
    }//object
}