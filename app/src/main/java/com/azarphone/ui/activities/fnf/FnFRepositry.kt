package com.azarphone.ui.activities.fnf

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.*
import com.azarphone.api.pojo.response.addfnfresponse.AddFnfResponse
import com.azarphone.api.pojo.response.deletefnfresponse.DeleteFnfResponse
import com.azarphone.api.pojo.response.fnfresponse.FNFResponse
import com.azarphone.api.pojo.response.inappsurvey.InAppSurvey
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.moneytransfer.MoneyTransferResponse
import com.azarphone.api.pojo.response.updatefnfresponse.UpdateFNFResponse
import io.reactivex.Observable

class FnFRepositry {
    init {

    }//init ends

    fun requestFnFList(fnFRequest: FnFRequest): Observable<FNFResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().getFNFList(fnFRequest)
    }

    fun requestAddFNF(fnFRequest: AddFnfRequest): Observable<AddFnfResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().addFNF(fnFRequest)
    }

    fun requestUpdateFNF(updateFnFRequest: UpdateFNFRequest): Observable<UpdateFNFResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().updateFNF(updateFnFRequest)
    }

    fun requestDeleteFNF(deleteFnFRequest: DeleteFnfRequest): Observable<DeleteFnfResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().deleteFNF(deleteFnFRequest)
    }
    fun saveInAppSurvey(uploadSurvey: UploadSurvey): Observable<InAppSurvey> {
        return ApiClient.newApiClientInstance.getServerAPI().saveInAppSurvey(uploadSurvey)
    }
}