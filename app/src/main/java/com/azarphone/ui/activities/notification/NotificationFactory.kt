package com.azarphone.ui.activities.notification

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class NotificationFactory(val notificationRepository: NotificationRepositry) : ViewModelProvider.NewInstanceFactory() {


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(NotificationViewModel::class.java)) {
            return NotificationViewModel(notificationRepository) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}