package com.azarphone.ui.activities.changepassword

import androidx.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.text.method.PasswordTransformationMethod
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.helperpojo.UserAccounts
import com.azarphone.api.pojo.response.changepassword.ChangePasswordResponse
import com.azarphone.api.pojo.response.customerdata.CustomerData
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.LayoutChangePasswordBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.ui.activities.loginactivity.LoginActivity
import com.azarphone.ui.activities.manageaccount.ManageAccountActivity
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.validators.isValidPassword
import com.azarphone.widgets.popups.showMessageDialog
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.layout_change_password.*

/**
 * @author Junaid Hassan on 04, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class ChangePasswordActivity : BaseActivity<LayoutChangePasswordBinding, ChangePasswordFactory, ChangePasswordViewModel>() {

    private val fromClass = "ChangePasswordActivity"

    override fun getLayoutId(): Int {
        return R.layout.layout_change_password
    }

    override fun getViewModelClass(): Class<ChangePasswordViewModel> {
        return ChangePasswordViewModel::class.java
    }

    override fun getFactory(): ChangePasswordFactory {
        return ChangePasswordInjection.provideChangePasswordFactory()
    }

    override fun init() {
        setupHeaderActionBar()
        setupInputFields()
        subscribe()
        initUI()
        initUIEvents()
    }//init ends

    private fun subscribe() {
        val changePasswordResponseLiveData = object : Observer<ChangePasswordResponse> {
            override fun onChanged(t: ChangePasswordResponse?) {
                if (!this@ChangePasswordActivity.isFinishing) {
                    if (t != null) {
                        if (t.data.message != null) {
                            /**handle the response for success case
                             * if there are multiple users logged in, show the manage account screen
                             * other wise if there is only single user logged in logout the user*/
                            logE("changePassword", "response:::".plus(t.data.message), fromClass, "subscribe")

                            var list: ArrayList<CustomerData>? = ArrayList()
                            if (getUserAccountsDataFromLocalPreferences() != null && getUserAccountsDataFromLocalPreferences().usersList != null) {
                                list = getUserAccountsDataFromLocalPreferences().usersList as ArrayList<CustomerData>
                            }

                            if (list!!.size > 1) {
                                logE("listXSize", "size:::".plus(list.size), fromClass, "subscribe")
                                val newList: ArrayList<CustomerData> = ArrayList()
                                for (customer in list) {
                                    if (customer.msisdn == UserDataManager.getCustomerData()!!.msisdn!!) {
                                        //skip this data
                                    } else {
                                        newList.add(customer)
                                    }
                                }

                                /**update the list*/
                                ProjectApplication.getInstance().getLocalPreferneces().setResponse(
                                        ProjectApplication.getInstance().getLocalPreferneces().KEY_USER_ACCOUNTS_DATA,
                                        UserAccounts(newList))

                                /**add the change password flag in preferences*/
                                ProjectApplication.getInstance().getLocalPreferneces().addBool(
                                        ProjectApplication.getInstance().getLocalPreferneces().IS_PASSWORD_CHANGE_MULTI_USER_LOGGED_OUT,
                                        true)

                                /**goto manage accounts activity*/
                                val intent = Intent(applicationContext, ManageAccountActivity::class.java)
                                intent.addFlags(
                                        Intent.FLAG_ACTIVITY_NEW_TASK
                                                or
                                                Intent.FLAG_ACTIVITY_CLEAR_TASK
                                                or
                                                Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                startActivity(intent)
                                finish()
                            } else {
                                logE("listXSize", "size:::0".plus(list.size), fromClass, "subscribe")
                                //remove the shared preferences
                                ProjectApplication.getInstance().getLocalPreferneces().deleteAllPreferences()
                                //    resetWrongPasswordAttempts(context)

                                //reset the user data manager
                                UserDataManager.resetUserDataManager()

                                //start from the new process
                                //show the user the login activity
                                LoginActivity.start(applicationContext)
                                //finish the previous activity
                                finish()
                            }
                        } else {
                            logE("changePassword", "message is null", fromClass, "subscribe")
                        }
                    } else {
                        logE("changePassword", "response is null", fromClass, "subscribe")
                    }
                }
            }
        }

        mViewModel.changePasswordResponseLiveData.observe(this@ChangePasswordActivity, changePasswordResponseLiveData)

        val changePasswordErrorResponseLiveData = object : Observer<AzerAPIErrorHelperResponse> {
            override fun onChanged(response: AzerAPIErrorHelperResponse?) {
                if (!this@ChangePasswordActivity.isFinishing) {
                    //check internet
                    if (!isNetworkAvailable(this@ChangePasswordActivity)) {
                        showMessageDialog(applicationContext, this@ChangePasswordActivity,
                                resources.getString(R.string.popup_note_title),
                                resources.getString(R.string.message_no_internet))
                    } else {
                        /**check the error code
                         * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                         * else show a general api failure popup*/
                        if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                            showMessageDialog(applicationContext, this@ChangePasswordActivity,
                                    this@ChangePasswordActivity.resources.getString(R.string.popup_error_title),
                                    response.errorDetail.toString())
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                            //force logout the user here
                            logOut(applicationContext, this@ChangePasswordActivity, false, fromClass, "changePassword122798")
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                            //handle the ui in this case
                            //hide or show the no data found layout in this case
                            showMessageDialog(applicationContext, this@ChangePasswordActivity,
                                    this@ChangePasswordActivity.resources.getString(R.string.popup_error_title),
                                    this@ChangePasswordActivity.resources.getString(R.string.server_stopped_responding_please_try_again))
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                            showMessageDialog(applicationContext, this@ChangePasswordActivity,
                                    this@ChangePasswordActivity.resources.getString(R.string.popup_error_title),
                                    this@ChangePasswordActivity.resources.getString(R.string.server_stopped_responding_please_try_again))
                        } else {
                            showMessageDialog(applicationContext, this@ChangePasswordActivity,
                                    this@ChangePasswordActivity.resources.getString(R.string.popup_error_title),
                                    this@ChangePasswordActivity.resources.getString(R.string.server_stopped_responding_please_try_again))
                        }
                    }
                }// if (activity != null && !this@ChangePasswordActivity.isFinishing)  ends
            }
        }

        mViewModel.changePasswordErrorResponseLiveData.observe(this@ChangePasswordActivity, changePasswordErrorResponseLiveData)
    }//subscribe ends

    private fun setupInputFields() {
        oldPasswordTIL.defaultHintTextColor = resources.getColorStateList(R.color.brown_grey)
        oldPasswordET.typeface = getALSBoldFont()

        oldPasswordET.transformationMethod = PasswordTransformationMethod.getInstance()

        newPasswordTIL.defaultHintTextColor = resources.getColorStateList(R.color.brown_grey)
        newPasswordET.typeface = getALSBoldFont()

        newPasswordET.transformationMethod = PasswordTransformationMethod.getInstance()

        confirmNewPasswordTIL.defaultHintTextColor = resources.getColorStateList(R.color.brown_grey)
        confirmNewPasswordET.typeface = getALSBoldFont()

        confirmNewPasswordET.transformationMethod = PasswordTransformationMethod.getInstance()
    }//setupInputFields ends

    private fun initUIEvents() {
        submitButton.setOnClickListener {
            proceedOnUpdatePassword()
        }//submitButton.setOnClickListener ends

/*        confirmNewPasswordET.setOnKeyListener { _, keyCode, event ->
            if (event!!.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                proceedOnUpdatePassword()
            }
            false
        }//confirmPasswordInput.setOnKeyListener  ends*/

        confirmNewPasswordET.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if (event!!.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    proceedOnUpdatePassword()
                }//if ends
                return false
            }//onKey ends
        })

        confirmNewPasswordET.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    proceedOnUpdatePassword()
                }//if ends
                return false
            }//onEditorAction ends
        })

        azerActionBarBackIcon.setOnClickListener {
            onBackPressed()
        }//azerActionBarBackIcon.setOnClickListener ends
    }//initUIEvents ends

    private fun initUI() {

    }//initUI ends

    private fun proceedOnUpdatePassword() {
        val oldPassword = oldPasswordET.text.toString()
        val newPassword = newPasswordET.text.toString()
        val confirmNewPassword = confirmNewPasswordET.text.toString()

        if (!isValidPassword(oldPassword)) {
            showMessageDialog(applicationContext, this@ChangePasswordActivity,
                    getString(R.string.popup_error_title),
                    resources.getString(R.string.change_password_not_match_label))
            return
        }//if (!isValidPassword(oldPassword))  ends

        if (newPassword != confirmNewPassword) {
            showMessageDialog(applicationContext, this@ChangePasswordActivity,
                    resources.getString(R.string.popup_error_title),
                    resources.getString(R.string.change_password_not_match_label))
            return//safe passage, go back
        } else {
            var msisdn = ""
            if (UserDataManager.getCustomerData() != null && hasValue(UserDataManager.getCustomerData()!!.msisdn)) {
                msisdn = UserDataManager.getCustomerData()!!.msisdn!!
            }
            hideKeyboard(this@ChangePasswordActivity)
            mViewModel.changePassword(oldPassword, newPassword, confirmNewPassword, msisdn)
        }//if (newPassword != confirmNewPassword) ends
    }//proceedOnSubmit ends

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.change_password_title_label)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, ChangePasswordActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends
    }//object ends

}//class ends