package com.azarphone.ui.activities.contactus

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider


class ContactUsFactory(val contactUsRepository: ContactUsRepositry) : ViewModelProvider.NewInstanceFactory() {


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ContactUsViewModel::class.java)) {
            return ContactUsViewModel(contactUsRepository) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}