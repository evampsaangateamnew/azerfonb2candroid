package com.azarphone.ui.activities.loginactivity

import androidx.lifecycle.MutableLiveData
import com.azarphone.analytics.Analytics
import com.azarphone.analytics.EventValues
import com.azarphone.api.pojo.request.LoginRequest
import com.azarphone.api.pojo.response.loginappresumeresponse.LoginAppResumeResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.util.ConstantsUtility.ManageAccount.LOGIN_USECASE_LOGIN
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable


class LoginViewModel(val mLoginRepository: LoginRepository) : BaseViewModel() {

    lateinit var disposable: Disposable

    val loginResponseData = MutableLiveData<LoginAppResumeResponse>()
    var useCase_Type = LOGIN_USECASE_LOGIN

    init {
    }

    fun processUserLogin(msisdn: String, password: String) {
        view()?.showLoading()
        val loginRequest = LoginRequest(msisdn, password)

        var mObserver = mLoginRepository.processUserLogin(loginRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    loginResponseData.postValue(result)
                                    Analytics.logAppEvent(EventValues.LoginEvents.login, EventValues.LoginEvents.user_login, EventValues.GenericValues.Success)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                            Analytics.logAppEvent(EventValues.LoginEvents.login, EventValues.LoginEvents.user_login, EventValues.GenericValues.Failure)

                        }
                )
        compositeDisposables?.add(disposable)

    }
}