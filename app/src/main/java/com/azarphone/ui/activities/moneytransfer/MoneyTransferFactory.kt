package com.azarphone.ui.activities.moneytransfer

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

/**
 * @author Junaid Hassan on 08, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class MoneyTransferFactory(val mMoneyTransferRepositry: MoneyTransferRepositry) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MoneyTransferViewModel::class.java)) {
            return MoneyTransferViewModel(mMoneyTransferRepositry) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}