package com.azarphone.ui.activities.newautopayment

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class NewAutoPaymentFactory(private val newAutoPaymentRepository: NewAutoPaymentRepository) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(NewAutoPaymentViewModel::class.java)) {
            return NewAutoPaymentViewModel(newAutoPaymentRepository) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }

}