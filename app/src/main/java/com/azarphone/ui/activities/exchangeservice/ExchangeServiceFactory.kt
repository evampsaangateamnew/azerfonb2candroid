package com.azarphone.ui.activities.exchangeservice

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ExchangeServiceFactory(val mLoginRepository: ExchangeServiceRepositry) : ViewModelProvider.NewInstanceFactory() {


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ExchangeServiceViewModel::class.java)) {
            return ExchangeServiceViewModel(mLoginRepository) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}