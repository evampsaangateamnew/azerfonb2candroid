package com.azarphone.ui.activities.operationshistory

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.OperationsHistoryRequest
import com.azarphone.api.pojo.response.operationshistory.OperationHistoryResponse
import io.reactivex.Observable

/**
 * @author Junaid Hassan on 08, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class OperationsHistoryRepositry {
    init {

    }//init

    fun requestOperationsHistory(operationsHistoryRequest : OperationsHistoryRequest): Observable<OperationHistoryResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestOperationsHistory(operationsHistoryRequest)
    }
}