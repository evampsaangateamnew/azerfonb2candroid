package com.azarphone.ui.activities.vasservices

import android.annotation.SuppressLint
import androidx.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.response.coreservices.CoreServicesResponse
import com.azarphone.api.pojo.response.coreservicesprocessnumber.CoreServicesProcessResponse
import com.azarphone.api.pojo.response.mysubscriptions.successresponse.MySubscriptionsSuccessAfterOfferSubscribeResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.LayoutCoreServicesActivityBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.CoreServiceAdapterUpdaterEvents
import com.azarphone.eventhandler.CoreServiceProcessEvents
import com.azarphone.eventhandler.CoreServicesCallDivertDisableEvents
import com.azarphone.ui.adapters.recyclerviews.CoreServicesAdapter
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.widgets.popups.showMessageDialog
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.layout_core_services_activity.*

/**
 * @author Junaid Hassan on 24, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class CoreServicesActivity : BaseActivity<LayoutCoreServicesActivityBinding, CoreServicesFactory, CoreServicesViewModel>(), CoreServiceProcessEvents, CoreServicesCallDivertDisableEvents {

    private var coreServicesResponse: CoreServicesResponse? = null
    private val fromClass = "CoreServicesActivity"
    private var coreServicesAdapter: CoreServicesAdapter? = null

    override fun onResume() {
        super.onResume()
        UserDataManager.setSuccessOfferingIdForCoreService("")
        if (hasValue(UserDataManager.getCallDivertNumber()) && coreServicesResponse != null) {
            if(coreServicesResponse!!.data!=null&&!coreServicesResponse!!.data!!.coreServices.isNullOrEmpty()){
                for(its in coreServicesResponse!!.data!!.coreServices!!.indices){
                    if(!coreServicesResponse!!.data!!.coreServices!![its]!!.coreServicesList.isNullOrEmpty()){
                        for(innerIts in coreServicesResponse!!.data!!.coreServices!![its]!!.coreServicesList!!.indices){
                            if(coreServicesResponse!!.data!!.coreServices!![its]!!.coreServicesList!![innerIts]!!.offeringId!=null
                                    && hasValue(coreServicesResponse!!.data!!.coreServices!![its]!!.coreServicesList!![innerIts]!!.offeringId)
                                    &&coreServicesResponse!!.data!!.coreServices!![its]!!.coreServicesList!![innerIts]!!.offeringId==ConstantsUtility.CoreServicesConstants.CALL_FORWARD_OFFERING_ID){
                                coreServicesResponse!!.data!!.coreServices!![its]!!.coreServicesList!![innerIts]!!.forwardNumber=UserDataManager.getCallDivertNumber().replace(" ","").trim()
                                coreServicesResponse!!.data!!.coreServices!![its]!!.coreServicesList!![innerIts]!!.status=ConstantsUtility.CoreServicesConstants.STATUS_ACTIVE
                                break
                            }
                        }
                    }
                }
            }
        }

        if(coreServicesAdapter!=null){
            coreServicesAdapter!!.notifyDataSetChanged()
        }
        UserDataManager.setCallDivertNumber("")
    }

    override fun getLayoutId(): Int {
        return R.layout.layout_core_services_activity
    }//getLayoutId ends

    override fun getViewModelClass(): Class<CoreServicesViewModel> {
        return CoreServicesViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): CoreServicesFactory {
        return CoreServicesInjection.provideCoreServicesFactory()
    }//getFactory ends

    override fun init() {
        setupHeaderActionBar()
        subscribe()
        initCoreServiceAdapterUpdateEvents()
        initUI()
        initUIEvents()
        /**get the data from the cache*/
        val cachedVASResponse = getVASServicesDataFromLocalPreferences()
        if (cachedVASResponse.data != null && hasValue(cachedVASResponse.data.toString())) {
            coreServicesResponse = cachedVASResponse
            populateCoreServices(coreServicesResponse!!)
            setVASServicesDataToLocalPreferences(coreServicesResponse!!)
            showContents()

            /**call the vas services in the background*/
            requestVASServicesBackground()
        } else {
            hideContents()
            if (isNetworkAvailable(applicationContext)) {
                requestVASServices()
            } else {
                showMessageDialog(applicationContext, this@CoreServicesActivity,
                        "", resources.getString(R.string.message_no_internet))
            }
        }

    }//init ends

    private fun initCoreServiceAdapterUpdateEvents() {
        val coreServiceAdapterUpdaterEvents = object : CoreServiceAdapterUpdaterEvents {
            override fun onCallDivertDisabledUpdateAdapter(offeringId: String) {
                /**remove the call forward number from the call divert section
                 * and change the status to the inactive*/
                if (hasValue(offeringId)) {
                    if (coreServicesResponse != null) {
                        if (coreServicesResponse!!.data != null && coreServicesResponse!!.data!!.coreServices != null
                                && coreServicesResponse!!.data!!.coreServices!!.isNotEmpty()) {
                            for (its in coreServicesResponse!!.data!!.coreServices!!.indices) {
                                if (coreServicesResponse!!.data!!.coreServices!![its]!!.coreServicesList != null &&
                                        coreServicesResponse!!.data!!.coreServices!![its]!!.coreServicesList!!.isNotEmpty()) {
                                    for (coreIts in coreServicesResponse!!.data!!.coreServices!![its]!!.coreServicesList!!.indices) {
                                        var offeringIdFromResponse = ""
                                        if (coreServicesResponse!!.data!!.coreServices!![its]!!.coreServicesList!![coreIts]!!.offeringId != null &&
                                                hasValue(coreServicesResponse!!.data!!.coreServices!![its]!!.coreServicesList!![coreIts]!!.offeringId)) {
                                            offeringIdFromResponse = coreServicesResponse!!.data!!.coreServices!![its]!!.coreServicesList!![coreIts]!!.offeringId!!
                                        }//check for offering id from response should not be null

                                        if (offeringIdFromResponse == ConstantsUtility.CoreServicesConstants.CALL_FORWARD_OFFERING_ID &&
                                                offeringId == offeringIdFromResponse) {
                                            /**remove the forward to number*/
                                            coreServicesResponse!!.data!!.coreServices!![its]!!.coreServicesList!![coreIts]!!.forwardNumber = getForwardNumberNotSetLabel()
                                            /**change the status to inactive*/
                                            coreServicesResponse!!.data!!.coreServices!![its]!!.coreServicesList!![coreIts]!!.status = ConstantsUtility.CoreServicesConstants.STATUS_INACTIVE
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                /**update the adapter, the status have been changed now*/
                if (coreServicesAdapter != null) {
                    coreServicesAdapter!!.notifyDataSetChanged()
                }
            }

            override fun onCoreServiceAdapterUpdate(offeringId: String) {
                if (hasValue(offeringId)) {
                    /**update the adapter
                     * first match this offering id with core services response,
                     * on matching just set the status of matched core service as Active*/
                    if (coreServicesResponse != null) {
                        if (coreServicesResponse!!.data != null && coreServicesResponse!!.data!!.coreServices != null
                                && coreServicesResponse!!.data!!.coreServices!!.isNotEmpty()) {
                            for (its in coreServicesResponse!!.data!!.coreServices!!.indices) {
                                if (coreServicesResponse!!.data!!.coreServices!![its]!!.coreServicesList != null &&
                                        coreServicesResponse!!.data!!.coreServices!![its]!!.coreServicesList!!.isNotEmpty()) {
                                    for (coreIts in coreServicesResponse!!.data!!.coreServices!![its]!!.coreServicesList!!.indices) {
                                        var offeringIdFromResponse = ""
                                        if (coreServicesResponse!!.data!!.coreServices!![its]!!.coreServicesList!![coreIts]!!.offeringId != null &&
                                                hasValue(coreServicesResponse!!.data!!.coreServices!![its]!!.coreServicesList!![coreIts]!!.offeringId)) {
                                            offeringIdFromResponse = coreServicesResponse!!.data!!.coreServices!![its]!!.coreServicesList!![coreIts]!!.offeringId!!
                                        }//check for offering id from response should not be null

                                        if (offeringId == offeringIdFromResponse) {
                                            if (coreServicesResponse!!.data!!.coreServices!![its]!!.coreServicesList!![coreIts]!!.status
                                                            .equals(ConstantsUtility.CoreServicesConstants.STATUS_ACTIVE, ignoreCase = true)) {
                                                coreServicesResponse!!.data!!.coreServices!![its]!!.coreServicesList!![coreIts]!!.status = ConstantsUtility.CoreServicesConstants.STATUS_INACTIVE
                                            } else {
                                                coreServicesResponse!!.data!!.coreServices!![its]!!.coreServicesList!![coreIts]!!.status = ConstantsUtility.CoreServicesConstants.STATUS_ACTIVE
                                            }
                                        }
                                    }//for (coreIts in coreServicesResponse!!.data!!.coreServices!![its]!!.coreServicesList!!.indices)  ends
                                }//check for core services list should not be null
                            }//for (its in coreServicesResponse!!.data!!.coreServices!!.indices) ends
                        }
                    }
                }

                /**update the adapter, the status have been changed now*/
                if (coreServicesAdapter != null) {
                    coreServicesAdapter!!.notifyDataSetChanged()
                }
            }
        }

        RootValues.setCoreServiceAdapterUpdaterEvents(coreServiceAdapterUpdaterEvents)
    }//initCoreServiceAdapterUpdateEvents ends

    private fun requestVASServicesBackground() {
        mViewModel.requestCoreServicesBackground("", "", "", "")
    }//requestVASServices ends

    private fun requestVASServices() {
        mViewModel.requestCoreServices("", "", "", "")
    }//requestVASServices ends

    private fun initUIEvents() {
        azerActionBarBackIcon.setOnClickListener {
            onBackPressed()
        }
    }//initUIEvents ends

    private fun initUI() {

    }//initUI ends

    private fun subscribe() {
        val coreServicesBackgroundResponseLiveData = object : Observer<CoreServicesResponse> {
            override fun onChanged(response: CoreServicesResponse?) {
                coreServicesResponse = response
                setVASServicesDataToLocalPreferences(coreServicesResponse!!)
                if (coreServicesAdapter != null) {
                    coreServicesAdapter!!.notifyDataSetChanged()
                    logE("adapterXVAS", "notified", fromClass, "coreServicesBackgroundResponseLiveData")
                }
            }
        }

        mViewModel.coreServicesBackgroundResponseLiveData.observe(this@CoreServicesActivity, coreServicesBackgroundResponseLiveData)

        val callDivertDisableResponseData = object : Observer<MySubscriptionsSuccessAfterOfferSubscribeResponse> {
            override fun onChanged(response: MySubscriptionsSuccessAfterOfferSubscribeResponse?) {
                if (response != null &&
                        response.data != null &&
                        hasValue(response.data.toString()) &&
                        response.data.message != null &&
                        hasValue(response.data.message)) {
                    showMessageDialog(applicationContext, this@CoreServicesActivity,
                            "",
                            response.data.message)
                }
            }
        }

        mViewModel.callDivertDisableResponseData.observe(this@CoreServicesActivity, callDivertDisableResponseData)

        val callDivertDisableErrorResponseData = object : Observer<AzerAPIErrorHelperResponse> {
            override fun onChanged(response: AzerAPIErrorHelperResponse?) {
                if (coreServicesAdapter != null) {
                    coreServicesAdapter!!.notifyDataSetChanged()
                }
                if (!this@CoreServicesActivity.isFinishing) {
                    if (!isNetworkAvailable(this@CoreServicesActivity)) {
                        showMessageDialog(applicationContext, this@CoreServicesActivity,
                                resources.getString(R.string.popup_note_title),
                                resources.getString(R.string.message_no_internet))
                    } else {
                        /**check the error code
                         * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                         * else show a general api failure popup*/
                        when {
                            response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION -> showMessageDialog(applicationContext, this@CoreServicesActivity,
                                    this@CoreServicesActivity.resources.getString(R.string.popup_error_title),
                                    response.errorDetail.toString())
                            response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT -> //force logout the user here
                                logOut(applicationContext, this@CoreServicesActivity, false, fromClass, "specialOffersErrorResponseData2321sf")
                            response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE -> //handle the ui in this case
                                //hide or show the no data found layout in this case
                                showMessageDialog(applicationContext, this@CoreServicesActivity,
                                        this@CoreServicesActivity.resources.getString(R.string.popup_error_title),
                                        this@CoreServicesActivity.resources.getString(R.string.server_stopped_responding_please_try_again))
                            response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE -> showMessageDialog(applicationContext, this@CoreServicesActivity,
                                    this@CoreServicesActivity.resources.getString(R.string.popup_error_title),
                                    this@CoreServicesActivity.resources.getString(R.string.server_stopped_responding_please_try_again))
                            else -> showMessageDialog(applicationContext, this@CoreServicesActivity,
                                    this@CoreServicesActivity.resources.getString(R.string.popup_error_title),
                                    this@CoreServicesActivity.resources.getString(R.string.server_stopped_responding_please_try_again))
                        }
                    }
                }// if (activity != null && !this@CoreServicesActivity.isFinishing)  ends
            }
        }

        mViewModel.callDivertDisableErrorResponseData.observe(this@CoreServicesActivity, callDivertDisableErrorResponseData)

        val coreServicesResponseLiveData = object : Observer<CoreServicesResponse> {
            override fun onChanged(response: CoreServicesResponse?) {
                if (response != null) {
                    if (response.data != null) {
                        logE("coreServices", "response:::".plus(response.toString()), fromClass, "subscribe")
                        coreServicesResponse = response
                        populateCoreServices(coreServicesResponse!!)
                        setVASServicesDataToLocalPreferences(coreServicesResponse!!)
                    } else {
                        hideContents()
                    }
                } else {
                    hideContents()
                }
            }
        }

        mViewModel.coreServicesResponseLiveData.observe(this@CoreServicesActivity, coreServicesResponseLiveData)

        val coreServicesErrorResponseLiveData = object : Observer<AzerAPIErrorHelperResponse> {
            override fun onChanged(response: AzerAPIErrorHelperResponse?) {
                if (!this@CoreServicesActivity.isFinishing) {
                    //check internet
                    hideContents()
                    if (!isNetworkAvailable(this@CoreServicesActivity)) {
                        showMessageDialog(applicationContext, this@CoreServicesActivity,
                                resources.getString(R.string.popup_note_title),
                                resources.getString(R.string.message_no_internet))
                    } else {
                        /**check the error code
                         * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                         * else show a general api failure popup*/
                        if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                            showMessageDialog(applicationContext, this@CoreServicesActivity,
                                    this@CoreServicesActivity.resources.getString(R.string.popup_error_title),
                                    response.errorDetail.toString())
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                            //force logout the user here
                            logOut(applicationContext, this@CoreServicesActivity, false, fromClass, "specialOffersErrorResponseData2321sf")
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                            //handle the ui in this case
                            //hide or show the no data found layout in this case
                            showMessageDialog(applicationContext, this@CoreServicesActivity,
                                    this@CoreServicesActivity.resources.getString(R.string.popup_error_title),
                                    this@CoreServicesActivity.resources.getString(R.string.server_stopped_responding_please_try_again))
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                            showMessageDialog(applicationContext, this@CoreServicesActivity,
                                    this@CoreServicesActivity.resources.getString(R.string.popup_error_title),
                                    this@CoreServicesActivity.resources.getString(R.string.server_stopped_responding_please_try_again))
                        } else {
                            showMessageDialog(applicationContext, this@CoreServicesActivity,
                                    this@CoreServicesActivity.resources.getString(R.string.popup_error_title),
                                    this@CoreServicesActivity.resources.getString(R.string.server_stopped_responding_please_try_again))
                        }
                    }
                }// if (activity != null && !this@CoreServicesActivity.isFinishing)  ends
            }
        }

        mViewModel.coreServicesErrorResponseLiveData.observe(this@CoreServicesActivity, coreServicesErrorResponseLiveData)

        val coreServiceProcessErrorResponseLiveData = object : Observer<AzerAPIErrorHelperResponse> {
            override fun onChanged(response: AzerAPIErrorHelperResponse?) {
                if (!this@CoreServicesActivity.isFinishing) {
                    /**notify the adapter changed*/
                    if (coreServicesAdapter != null) {
                        coreServicesAdapter!!.notifyDataSetChanged()
                    }
                    //check internet
                    if (!isNetworkAvailable(this@CoreServicesActivity)) {
                        showMessageDialog(applicationContext, this@CoreServicesActivity,
                                resources.getString(R.string.popup_note_title),
                                resources.getString(R.string.message_no_internet))
                    } else {
                        /**check the error code
                         * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                         * else show a general api failure popup*/
                        if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                            showMessageDialog(applicationContext, this@CoreServicesActivity,
                                    this@CoreServicesActivity.resources.getString(R.string.popup_error_title),
                                    response.errorDetail.toString())
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                            //force logout the user here
                            logOut(applicationContext, this@CoreServicesActivity, false, fromClass, "specialOffersErrorResponseData2321sf")
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                            //handle the ui in this case
                            //hide or show the no data found layout in this case
                            showMessageDialog(applicationContext, this@CoreServicesActivity,
                                    this@CoreServicesActivity.resources.getString(R.string.popup_error_title),
                                    this@CoreServicesActivity.resources.getString(R.string.server_stopped_responding_please_try_again))
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                            showMessageDialog(applicationContext, this@CoreServicesActivity,
                                    this@CoreServicesActivity.resources.getString(R.string.popup_error_title),
                                    this@CoreServicesActivity.resources.getString(R.string.server_stopped_responding_please_try_again))
                        } else {
                            showMessageDialog(applicationContext, this@CoreServicesActivity,
                                    this@CoreServicesActivity.resources.getString(R.string.popup_error_title),
                                    this@CoreServicesActivity.resources.getString(R.string.server_stopped_responding_please_try_again))
                        }
                    }
                }// if (activity != null && !this@CoreServicesActivity.isFinishing)  ends
            }
        }

        mViewModel.coreServiceProcessErrorResponseLiveData.observe(this@CoreServicesActivity, coreServiceProcessErrorResponseLiveData)

        val coreServiceProcessResponseLiveData = object : Observer<CoreServicesProcessResponse> {
            override fun onChanged(t: CoreServicesProcessResponse?) {
                if (t != null) {
                    if (t.resultDesc != null && hasValue(t.resultDesc)) {
                        showMessageDialog(applicationContext, this@CoreServicesActivity,
                                resources.getString(R.string.popup_note_title),
                                t.resultDesc)
                    }
                    RootValues.getCoreServiceAdapterUpdaterEvents().onCoreServiceAdapterUpdate(UserDataManager.getSuccessOfferingIdForCoreService())
                    UserDataManager.setSuccessOfferingIdForCoreService("")
                } else {
                    showMessageDialog(applicationContext, this@CoreServicesActivity,
                            resources.getString(R.string.popup_error_title),
                            resources.getString(R.string.server_stopped_responding_please_try_again))
                }
            }
        }

        mViewModel.coreServiceProcessResponseLiveData.observe(this@CoreServicesActivity, coreServiceProcessResponseLiveData)
    }//subscribe ends

    override fun onCoreServiceProcessed(messageTitle: String, actionType: String, offeringId: String) {
        var groupType = ""
        /* if (hasValue(UserDataManager.getCustomerData()!!.groupType)) {
             groupType = UserDataManager.getCustomerData()!!.groupType!!
         }*/

        var accountType = ""
        /* if (hasValue(UserDataManager.getCustomerData()!!.accountType)) {
             accountType = UserDataManager.getCustomerData()!!.accountType!!
         }*/
        showVASServiceSubscribeUnsubscribeConfirmationPopup(messageTitle, actionType, offeringId, "", groupType, accountType)
    }//onCoreServiceProcessed ends

    @SuppressLint("InflateParams")
    private fun showVASServiceSubscribeUnsubscribeConfirmationPopup(title: String, actionType: String, offeringId: String,
                                                                    msisdnToForwardNumber: String, groupType: String, accountType: String) {
        if (this@CoreServicesActivity.isFinishing) return

        val dialogBuilder = AlertDialog.Builder(this@CoreServicesActivity)
        val inflater = this@CoreServicesActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val dialogView = inflater.inflate(R.layout.azer_vas_services_confirmation_popup, null)
        dialogBuilder.setView(dialogView)

        val alertDialog = dialogBuilder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.show()

        val popupTitle = dialogView.findViewById(R.id.popupTitle) as TextView
        popupTitle.typeface = getALSNormalFont()
        popupTitle.text = title
        val popupOkayButton = dialogView.findViewById(R.id.yesButton) as Button
        popupOkayButton.typeface = getALSBoldFont()
        val popupCancelButton = dialogView.findViewById(R.id.noButton) as Button
        popupCancelButton.typeface = getALSBoldFont()

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN) {
            //english
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "No"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ) {
            //azri
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "Xeyr"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.RU) {
            //russian
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "Нет"
        }

        popupOkayButton.setOnClickListener {
            mViewModel.requestProcessCoreService(actionType, offeringId, groupType, msisdnToForwardNumber, accountType)
            alertDialog.dismiss()
        }

        popupCancelButton.setOnClickListener {
            if (coreServicesAdapter != null) {
                coreServicesAdapter!!.notifyDataSetChanged()
            }
            alertDialog.dismiss()
        }
    }//showVASServiceSubscribeUnsubscribeConfirmationPopup ends

    override fun onCallDivertDisabled(offeringId: String, offeringName: String, actionType: String) {
        mViewModel.changeSupplementaryOfferForCallDivert(offeringName, offeringId, actionType)
    }//onCallDivertDisabled ends

    private fun populateCoreServices(response: CoreServicesResponse) {
        if (response.data!!.coreServices != null && response.data!!.coreServices!!.isNotEmpty()) {
            coreServicesAdapter = CoreServicesAdapter(applicationContext, this@CoreServicesActivity, response.data!!.coreServices!!, false, this, this)
            val mLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(applicationContext, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
            coreServicesRecycler.layoutManager = mLayoutManager
            coreServicesRecycler.adapter = coreServicesAdapter
            showContents()
        } else {
            hideContents()
        }
    }//populateCoreServices ends

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.core_services_title_screen)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

    private fun showContents() {
        coreServicesRecycler.visibility = View.VISIBLE
        noDataFoundLayout.visibility = View.GONE
    }//showContents ends

    private fun hideContents() {
        coreServicesRecycler.visibility = View.GONE
        noDataFoundLayout.visibility = View.VISIBLE
    }//hideContents ends

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, CoreServicesActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends
    }//object ends

}//class ends