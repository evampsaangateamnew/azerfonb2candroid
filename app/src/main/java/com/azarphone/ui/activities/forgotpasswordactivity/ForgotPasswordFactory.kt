package com.azarphone.ui.activities.forgotpasswordactivity

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ForgotPasswordFactory(val mForgotPasswordReporsitory: ForgotPasswordReporsitory) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ForgotPasswordViewModel::class.java)) {
            return ForgotPasswordViewModel(mForgotPasswordReporsitory) as T
        }
        throw IllegalStateException("ViewModel Cannot be Converted")
    }


}