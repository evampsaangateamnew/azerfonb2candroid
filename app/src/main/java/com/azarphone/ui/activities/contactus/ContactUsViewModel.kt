package com.azarphone.ui.activities.contactus

import androidx.lifecycle.MutableLiveData
import com.azarphone.api.pojo.response.contactusresponse.ContactUsResponse
import com.azarphone.bases.BaseViewModel
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

class ContactUsViewModel(val contactUsRepositry: ContactUsRepositry) : BaseViewModel() {
    val contactUsResponseLiveData = MutableLiveData<ContactUsResponse>()
    val contactUsResponseBackgroundLiveData = MutableLiveData<ContactUsResponse>()
    private lateinit var disposable: Disposable

    init {

    }

    fun requestContactUsDetails() {
        view()?.showLoading()

        var mObserver = contactUsRepositry.requestContactUsDetails()
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    contactUsResponseLiveData.postValue(result)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestContactUsDetails ends

    fun requestContactUsDetailsBackground() {

        var mObserver = contactUsRepositry.requestContactUsDetails()
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            contactUsResponseBackgroundLiveData.postValue(result)
                        },
                        { error ->
                            view()?.hideLoading()
                        }
                )

        compositeDisposables?.add(disposable)
    }
}
