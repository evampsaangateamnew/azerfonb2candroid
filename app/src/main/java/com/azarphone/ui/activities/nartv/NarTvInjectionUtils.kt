package com.azarphone.ui.activities.nartv

object NarTvInjectionUtils{
    fun provideNarTvRepository(): NarTvRepositry {
        return NarTvRepositry()
    }

    fun provideNarTvFactory(): NarTvFactory {
        return NarTvFactory(provideNarTvRepository())
    }
}