package com.azarphone.ui.activities.emailactivity

import androidx.lifecycle.MutableLiveData
import com.azarphone.api.pojo.request.ChangeBillingRequest
import com.azarphone.api.pojo.request.UpdateEmailRequest
import com.azarphone.api.pojo.response.updateemailreponse.UpdateEmailResponse
import com.azarphone.bases.BaseViewModel
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

class EmailViewModel(val mEmailRepositry: EmailRepositry) : BaseViewModel() {

    private lateinit var disposable: Disposable
    val updateEmailResponse = MutableLiveData<UpdateEmailResponse>()

    init {

    }

    fun requestUpdateEmail(email: String) {
        view()?.showLoading()
        val updateEmailRequest = UpdateEmailRequest(email)

        var mObserver = mEmailRepositry.requestUpdateEmail(updateEmailRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    updateEmailResponse.postValue(result)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestCoreServices ends

}
