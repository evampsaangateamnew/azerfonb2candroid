package com.azarphone.ui.activities.freesms

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.SendSMSRequest
import com.azarphone.api.pojo.response.freesmsstatusresponse.FreeSmsStatusResponse
import com.azarphone.api.pojo.response.sendsmsresponse.SendSMSResponse
import io.reactivex.Observable

/**
 * @author Junaid Hassan on 08, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class FreeSmsRepositry{
    init {

    }


    fun sendSMS(forgotPasswordRequest: SendSMSRequest): Observable<SendSMSResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().sendSMS(forgotPasswordRequest)
    }

    fun getFreeSmsStatus(): Observable<FreeSmsStatusResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().getFreeSmsStatus()
    }
}