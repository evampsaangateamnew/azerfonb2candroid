package com.azarphone.ui.activities.fnf

import com.azarphone.ui.activities.moneytransfer.MoneyTransferRepositry

object FnFInjectionUtils{
    fun provideFnFRepository(): FnFRepositry {
        return FnFRepositry()
    }

    fun provideFnFFactory(): FnFFactory {
        return FnFFactory(provideFnFRepository())
    }
}