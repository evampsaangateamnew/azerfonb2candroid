package com.azarphone.ui.activities.settings

/**
 * @author Junaid Hassan on 18, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
object SettingsInjection {

    fun provideSettingsRepository(): SettingsRepositry {
        return SettingsRepositry()
    }

    fun provideSettingsFactory(): SettingsFactory {
        return SettingsFactory(provideSettingsRepository())
    }

}