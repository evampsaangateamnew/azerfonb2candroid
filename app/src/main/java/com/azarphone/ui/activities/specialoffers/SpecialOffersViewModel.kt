package com.azarphone.ui.activities.specialoffers

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.azarphone.R
import com.azarphone.analytics.Analytics
import com.azarphone.analytics.EventValues
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.request.MySubscriptionRequest
import com.azarphone.api.pojo.request.SpecialOffersRequest
import com.azarphone.api.pojo.request.SupplementaryOfferSubscribeRequest
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.mysubscriptions.response.MySubscriptionResponse
import com.azarphone.api.pojo.response.mysubscriptions.successresponse.MySubscriptionsSuccessAfterOfferSubscribeResponse
import com.azarphone.api.pojo.response.supplementaryoffers.response.SupplementaryResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.UpdateListOnSuccessfullySurvey
import com.azarphone.ui.dialogs.InAppThankYouDialog
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.logE
import com.azarphone.validators.hasValue
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

/**
 * @author Junaid Hassan on 25, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class SpecialOffersViewModel(val mSpecialOffersRepositry: SpecialOffersRepositry) : BaseViewModel() {

    private val logKey = "spOff"
    private val fromClass = "SpecialOffersViewModel"
    private lateinit var disposable: Disposable
    val specialOffersResponseData = MutableLiveData<SupplementaryResponse>()
    val specialOffersErrorResponseData = MutableLiveData<AzerAPIErrorHelperResponse>()
    var subscribeToSpecialOfferErrorResponseLiveData = MutableLiveData<AzerAPIErrorHelperResponse>()
   val subscribeToSpecialOfferOfferResponseLiveData = MutableLiveData<MySubscriptionsSuccessAfterOfferSubscribeResponse>()
    val mySubscriptionsBeforeSpecialsResponseLiveData = MutableLiveData<MySubscriptionResponse>()
    val mySubscriptionsBeforeSpecialsErrorResponseLiveData = MutableLiveData<AzerAPIErrorHelperResponse>()
    val inAppSurveyResponseErrorResponseData = MutableLiveData<AzerAPIErrorHelperResponse>()

    init {

    }//init ends
    fun uploadInAppSurvey(context: Context, uploadSurvey: UploadSurvey, onTransactionComplete: String, updateListOnSuccessfullySurvey: UpdateListOnSuccessfullySurvey?) {
        view()?.showLoading()
        var observable = mSpecialOffersRepositry.saveInAppSurvey(uploadSurvey)
        observable = observable.onErrorReturn { throwable -> null }
        disposable = observable
            .compose(applyIOSchedulers())
            .subscribe(
                { appSurvey ->
                    view()?.hideLoading()
                    when {
                        appSurvey?.data != null -> {
                            UserDataManager.setInAppSurvey(appSurvey)
                            val thankYouDialog =
                                InAppThankYouDialog(context, onTransactionComplete)
                            thankYouDialog.show()
                        }
                        appSurvey?.resultDesc != null -> {
                            inAppSurveyResponseErrorResponseData.postValue(
                                AzerAPIErrorHelperResponse(
                                    ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                    appSurvey.resultDesc
                                )
                            )
                        }
                        else -> {
                            inAppSurveyResponseErrorResponseData.postValue(
                                AzerAPIErrorHelperResponse(
                                    ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                    context.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            )
                        }
                    }
                    updateListOnSuccessfullySurvey?.updateListOnSuccessfullySurvey()
                },
                { error ->
                    inAppSurveyResponseErrorResponseData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            error.message
                        )
                    )
                }
            )
        compositeDisposables?.add(disposable)
    }

    fun subscribeToSupplementaryOffer(offeringName: String, offeringId: String, actionType: String) {
        view()?.showLoading()
        val supplementaryOfferSubscribeRequest = SupplementaryOfferSubscribeRequest(offeringName, offeringId, actionType)

        var mObserver = mSpecialOffersRepositry.subscribeToSupplementaryOffer(supplementaryOfferSubscribeRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            parseSubscribeToSupplementaryOfferResponseLiveData(result)
                        },
                        { error ->
                            view()?.hideLoading()
                            subscribeToSpecialOfferErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                            Analytics.logAppEvent(EventValues.SpecialOfferSubscriptionEvents.special_offer_subscription, EventValues.SpecialOfferSubscriptionEvents.subscription, EventValues.GenericValues.Failure)
                        }
                )

        compositeDisposables?.add(disposable)
    }

    private fun parseSubscribeToSupplementaryOfferResponseLiveData(response: MySubscriptionsSuccessAfterOfferSubscribeResponse) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        subscribeToSpecialOfferOfferResponseLiveData.postValue(response)
                        Analytics.logAppEvent(EventValues.SpecialOfferSubscriptionEvents.special_offer_subscription, EventValues.SpecialOfferSubscriptionEvents.subscription, EventValues.GenericValues.Success)
                    } else {
                        /**the response has no data object
                         * in this case we have to handle the ui only*/
                        subscribeToSpecialOfferErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                        Analytics.logAppEvent(EventValues.SpecialOfferSubscriptionEvents.special_offer_subscription, EventValues.SpecialOfferSubscriptionEvents.subscription, EventValues.GenericValues.Failure)
                    }
                } else {
                    /**the response has no data object
                     * in this case we have to handle the ui only*/
                    subscribeToSpecialOfferErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                    Analytics.logAppEvent(EventValues.SpecialOfferSubscriptionEvents.special_offer_subscription, EventValues.SpecialOfferSubscriptionEvents.subscription, EventValues.GenericValues.Failure)
                }
            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                /**force log out user case
                 * when the server sends the result code as 7*/
                subscribeToSpecialOfferErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT, ""))
            } else {
                /**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*/
                if (hasValue(response.resultDesc.toString())) {
                    subscribeToSpecialOfferErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
                } else {
                    subscribeToSpecialOfferErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
                }
            }
        } else {
            /**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*/
            if (hasValue(response.resultDesc.toString())) {
                subscribeToSpecialOfferErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
            } else {
                subscribeToSpecialOfferErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }

    fun requestMySubscriptions(offeringName: String, brandName: String) {
        logE("subXYR", "MySubcriptionsCalling:::offeringName:::".plus(offeringName).plus(" brandName:::").plus(brandName), fromClass, "requestMySubscriptions")
        view()?.showLoading()
        val mySubscriptionRequest = MySubscriptionRequest(offeringName)

        var mObserver = mSpecialOffersRepositry.requestMySubscriptions(mySubscriptionRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            logE("subXYR", "response:::".plus(result.toString()), fromClass, "requestMySubscriptions")
                            parseMySubscriptionsResponse(result)
                        },
                        { error ->
                            view()?.hideLoading()
                            mySubscriptionsBeforeSpecialsErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestTopUp ends

    private fun parseMySubscriptionsResponse(response: MySubscriptionResponse) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        mySubscriptionsBeforeSpecialsResponseLiveData.postValue(response)
                    } else {
                        /**the response has no data object
                         * in this case we have to handle the ui only*/
                        mySubscriptionsBeforeSpecialsErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                    }
                } else {
                    /**the response has no data object
                     * in this case we have to handle the ui only*/
                    mySubscriptionsBeforeSpecialsErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                }
            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                /**force log out user case
                 * when the server sends the result code as 7*/
                mySubscriptionsBeforeSpecialsErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT, ""))
            } else {
                /**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*/
                if (hasValue(response.resultDesc.toString())) {
                    mySubscriptionsBeforeSpecialsErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
                } else {
                    mySubscriptionsBeforeSpecialsErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
                }
            }
        } else {
            /**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*/
            if (hasValue(response.resultDesc.toString())) {
                mySubscriptionsBeforeSpecialsErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
            } else {
                mySubscriptionsBeforeSpecialsErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }//parseResponse ends

    fun requestSpecialOffers(offeringName: String, specialOfferIds: List<String>) {
        logE(logKey, "calling SpecialOffers:::offeringName:::".plus(offeringName).plus(" brandName:::")
                .plus(specialOfferIds), fromClass, "requestSpecialOffers")

        view()?.showLoading()

        var groupIds = ""
        if (UserDataManager.getCustomerData() != null && UserDataManager.getCustomerData()!!.groupIds != null &&
                hasValue(UserDataManager.getCustomerData()!!.groupIds)) {
            groupIds = UserDataManager.getCustomerData()!!.groupIds!!
            logE("gIdX", "groupId:::".plus(groupIds), fromClass, "requestSpecialOffers")
        } else {
            logE("gIdX", "groupId:::".plus("nullllll"), fromClass, "requestSpecialOffers")
        }
        val specialOffersRequest = SpecialOffersRequest(offeringName, specialOfferIds, groupIds)

        var mObserver = mSpecialOffersRepositry.requestSpecialOffers(specialOffersRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })
        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            parseResponse(result)
                        },
                        { error ->
                            view()?.hideLoading()
                            specialOffersErrorResponseData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestOperationsHistory ends

    private fun parseResponse(response: SupplementaryResponse) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        specialOffersResponseData.postValue(response)
                    } else {
                        /**the response has no data object
                         * in this case we have to handle the ui only*/
                        specialOffersErrorResponseData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                    }
                } else {
                    /**the response has no data object
                     * in this case we have to handle the ui only*/
                    specialOffersErrorResponseData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                }
            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                /**force log out user case
                 * when the server sends the result code as 7*/
                specialOffersErrorResponseData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT, ""))
            } else {
                /**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*/
                if (hasValue(response.resultDesc.toString())) {
                    specialOffersErrorResponseData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
                } else {
                    specialOffersErrorResponseData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
                }
            }
        } else {
            /**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*/
            if (hasValue(response.resultDesc.toString())) {
                specialOffersErrorResponseData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
            } else {
                specialOffersErrorResponseData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }//parseResponse ends
}//class ends