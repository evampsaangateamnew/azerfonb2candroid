package com.azarphone.ui.activities.faq

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.response.faqresponse.FAQResponse
import io.reactivex.Observable

class FaqRepositry {

   /* fun getNotifications(): Observable<NotificationResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().getNotifications()
    }*/

    fun requestFAQs(): Observable<FAQResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestFAQs()
    }
}