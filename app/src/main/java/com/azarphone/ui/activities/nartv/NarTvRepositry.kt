package com.azarphone.ui.activities.nartv

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.NarTvMigrationRequest
import com.azarphone.api.pojo.request.NarTvSubscriptionsRequest
import com.azarphone.api.pojo.response.nartvmigrationresponse.NarTvMigrationResponse
import com.azarphone.api.pojo.response.nartvsubscriptionsresponse.NarTvSubscriptionResponse
import io.reactivex.Observable

class NarTvRepositry {
    init {

    }//init ends
    fun requestNarTvSubscriptions(narTvSubscriptionsRequest: NarTvSubscriptionsRequest): Observable<NarTvSubscriptionResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().getNarTvSubscriptions(narTvSubscriptionsRequest)
    }

    fun requestNarTvMigration(narTvMigrationRequest: NarTvMigrationRequest): Observable<NarTvMigrationResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestNarTvMigration(narTvMigrationRequest)
    }
}