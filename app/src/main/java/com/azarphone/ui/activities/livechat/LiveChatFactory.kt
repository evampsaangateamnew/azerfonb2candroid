package com.azarphone.ui.activities.livechat

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class LiveChatFactory(private val liveChatRepository: LiveChatRepository) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LiveChatViewModel::class.java)) {
            return LiveChatViewModel(liveChatRepository) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}