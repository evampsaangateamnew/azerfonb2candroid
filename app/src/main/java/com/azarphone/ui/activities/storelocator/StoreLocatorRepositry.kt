package com.azarphone.ui.activities.storelocator

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.response.storelocator.StoreLocatorResponse
import io.reactivex.Observable

class StoreLocatorRepositry {


    init {

    }

    fun getStoreDetails(): Observable<StoreLocatorResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().getStoreDetail()
    }
}