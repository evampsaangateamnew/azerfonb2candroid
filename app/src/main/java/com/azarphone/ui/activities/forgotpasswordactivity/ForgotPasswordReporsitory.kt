package com.azarphone.ui.activities.forgotpasswordactivity

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.ForgotPasswordRequest
import com.azarphone.api.pojo.request.OTPRequest
import com.azarphone.api.pojo.request.OTPVerifyRequest
import com.azarphone.api.pojo.request.ResendPinRequest
import com.azarphone.api.pojo.response.forgotpassword.ForgotPasswordResponse
import com.azarphone.api.pojo.response.otpgetresponse.OTPResponse
import com.azarphone.api.pojo.response.otpverifyresponse.OTPVerifyResponse
import com.azarphone.api.pojo.response.resendpin.ResendPinResponse
import io.reactivex.Observable

class ForgotPasswordReporsitory {

    init {

    }

    fun updatePassword(forgotPasswordRequest: ForgotPasswordRequest): Observable<ForgotPasswordResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().forgotPassword(forgotPasswordRequest)
    }


    fun requestOTP(otpRequest: OTPRequest): Observable<OTPResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestOTP(otpRequest)
    }


    fun resendPin(resendPinRequest: ResendPinRequest):Observable<ResendPinResponse>{
        return ApiClient.newApiClientInstance.getServerAPI().resendPin(resendPinRequest)
    }

    fun verifyOtp(requestOTPVerifyRequest: OTPVerifyRequest): Observable<OTPVerifyResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestVerifyOTP(requestOTPVerifyRequest)
    }
}