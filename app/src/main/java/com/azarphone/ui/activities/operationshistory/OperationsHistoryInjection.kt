package com.azarphone.ui.activities.operationshistory

/**
 * @author Junaid Hassan on 08, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
object OperationsHistoryInjection{
    fun provideOperationsHistoryRepository(): OperationsHistoryRepositry {
        return OperationsHistoryRepositry()
    }

    fun provideOperationsHistoryFactory(): OperationsHistoryFactory {
        return OperationsHistoryFactory(provideOperationsHistoryRepository())
    }
}