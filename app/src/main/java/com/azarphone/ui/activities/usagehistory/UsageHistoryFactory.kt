package com.azarphone.ui.activities.usagehistory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

/**
 * @author Junaid Hassan on 04, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class UsageHistoryFactory(val mUsageHistoryRepositry: UsageHistoryRepositry) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UsageHistoryViewModel::class.java)) {
            return UsageHistoryViewModel(mUsageHistoryRepositry) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}