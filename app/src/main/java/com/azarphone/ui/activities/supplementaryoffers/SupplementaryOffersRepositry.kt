package com.azarphone.ui.activities.supplementaryoffers

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.MySubscriptionRequest
import com.azarphone.api.pojo.request.SupplementaryOfferSubscribeRequest
import com.azarphone.api.pojo.request.SupplementaryOffersRequest
import com.azarphone.api.pojo.response.inappsurvey.InAppSurvey
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.mysubscriptions.response.MySubscriptionResponse
import com.azarphone.api.pojo.response.mysubscriptions.successresponse.MySubscriptionsSuccessAfterOfferSubscribeResponse
import com.azarphone.api.pojo.response.supplementaryoffers.response.SupplementaryResponse
import io.reactivex.Observable

/**
 * @author Junaid Hassan on 22, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class SupplementaryOffersRepositry {
    init {

    }//init ends

    fun requestSupplementaryOffers(supplementaryOffersRequest: SupplementaryOffersRequest): Observable<SupplementaryResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestSupplementaryOffers(supplementaryOffersRequest)
    }//requestSupplementaryOffers ends

    fun requestMySubscriptions(mySubscriptionRequest: MySubscriptionRequest): Observable<MySubscriptionResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestMySubscriptions(mySubscriptionRequest)
    }//requestMySubscriptions ends

    fun subscribeToSupplementaryOffer(supplementaryOfferSubscribeRequest: SupplementaryOfferSubscribeRequest): Observable<MySubscriptionsSuccessAfterOfferSubscribeResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().subscribeToSupplementaryOffer(supplementaryOfferSubscribeRequest)
    }//requestSuspendNumber ends

    fun saveInAppSurvey(uploadSurvey: UploadSurvey): Observable<InAppSurvey> {
        return ApiClient.newApiClientInstance.getServerAPI().saveInAppSurvey(uploadSurvey)
    }

}//class ends