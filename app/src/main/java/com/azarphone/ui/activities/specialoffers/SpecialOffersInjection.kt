package com.azarphone.ui.activities.specialoffers

/**
 * @author Junaid Hassan on 25, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
object SpecialOffersInjection {

    fun provideSpecialOffersRepository(): SpecialOffersRepositry {
        return SpecialOffersRepositry()
    }

    fun provideSpecialOffersFactory(): SpecialOffersFactory {
        return SpecialOffersFactory(provideSpecialOffersRepository())
    }

}