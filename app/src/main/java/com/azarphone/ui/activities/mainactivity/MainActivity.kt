@file:Suppress("DEPRECATION")

package com.azarphone.ui.activities.mainactivity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.*
import android.webkit.*
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.onNavDestinationSelected
import androidx.navigation.ui.setupWithNavController
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.helperpojo.menus.HorizontalMenuHelperModel
import com.azarphone.api.pojo.helperpojo.menus.VerticalMenuHelperModel
import com.azarphone.api.pojo.response.customerdata.CustomerData
import com.azarphone.api.pojo.response.fcmtoken.FCMResponse
import com.azarphone.api.pojo.response.gethomepageresponse.GetHomePageResponse
import com.azarphone.api.pojo.response.inappsurvey.InAppSurvey
import com.azarphone.api.pojo.response.inappsurvey.Surveys
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.internetpackresponse.InternetPacksResponse
import com.azarphone.api.pojo.response.loginappresumeresponse.LoginAppResumeResponse
import com.azarphone.api.pojo.response.logoutresponse.LogOutResponse
import com.azarphone.api.pojo.response.menuresponse.MenuHorizontalItem
import com.azarphone.api.pojo.response.menuresponse.MenuResponse
import com.azarphone.api.pojo.response.menuresponse.MenuVerticalItem
import com.azarphone.api.pojo.response.mysubscriptions.successresponse.MySubscriptionsSuccessAfterOfferSubscribeResponse
import com.azarphone.api.pojo.response.rateusbeforeredirectionresponse.RateUsBeforeRedirectingToPlayStoreRespone
import com.azarphone.api.pojo.response.tariffsresponse.TariffsResponse
import com.azarphone.api.pojo.response.tariffsubscriptionresponse.TariffSubscriptionResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.LayoutActivityMainBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.*
import com.azarphone.ui.activities.contactus.ContactUsActivity
import com.azarphone.ui.activities.faq.FaqActivity
import com.azarphone.ui.activities.loginactivity.LoginActivity
import com.azarphone.ui.activities.manageaccount.ManageAccountActivity
import com.azarphone.ui.activities.notification.NotificationActivity
import com.azarphone.ui.activities.settings.SettingsActivity
import com.azarphone.ui.activities.storelocator.StoreLocatorActivity
import com.azarphone.ui.activities.termsandconditions.TermsAndConditionsActivity
import com.azarphone.ui.activities.tutorials.TutorialsActivity
import com.azarphone.ui.activities.userprofile.UserProfileActivity
import com.azarphone.ui.adapters.recyclerviews.ManageAccountPopUpRecyclerAdapter
import com.azarphone.ui.dialogs.InAppFeedbackDialog
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.widgets.popups.showMessageDialog
import com.azarphone.widgets.verticalmenu.ViewItemVerticalMenus
import com.bumptech.glide.Glide
import com.es.tec.LocalSharedPrefStorage
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import com.google.firebase.messaging.FirebaseMessaging
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.layout_activity_main.*
import kotlinx.android.synthetic.main.manage_account_popup.*
import kotlinx.android.synthetic.main.manage_account_popup.view.*
import kotlinx.android.synthetic.main.navigation_drawer_layout.*
import kotlinx.android.synthetic.main.web_view_header.*
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : BaseActivity<LayoutActivityMainBinding, MainFactory, MainViewModel>(),
    MainActivityEventsHandler {

    private var webViewProgressDialog: AlertDialog? = null
    private var mUploadMessage: ValueCallback<Uri>? = null
    private var uploadMessage: ValueCallback<Array<Uri>>? = null
    private val REQUEST_SELECT_FILE = 134
    private val FILECHOOSER_RESULTCODE = 128
    private var isURLLoadSuccess = false
    private var target: Target? = null
    private val fromClass = "MainActivity"
    private var verticalMenuClickListener: VerticalMenuClickEvents? = null
    private var appResumeCallingData = ""
    private var listener: MainActivityEventsHandler = this@MainActivity

    //Dynamic Links
    var screenNameToRedirect = ""
    var subScreenNameToRedirect = ""
    var stepOneScreenNameToRedirect = ""
    var tabNameToRedirect = ""
    var offeringIdToRedirect = ""
    var isBottomTabAvailable = false

    var totalCountOfApis: Int = -1

    override fun getLayoutId(): Int {
        return R.layout.layout_activity_main
    }//getLayoutId ends

    override fun getViewModelClass(): Class<MainViewModel> {
        return MainViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): MainFactory {
        return MainInjection.provideMainFactory()
    }//getFactory ends

    override fun onResume() {
        super.onResume()
        logE("onReXum", "called", fromClass, "onResume")
        RootValues.setAzeriSymbolFont(applicationContext)
        RootValues.setALSBoldFont(applicationContext)
        RootValues.setALSNormalFont(applicationContext)

        if (UserDataManager.getCustomerData() != null && hasValue(UserDataManager.getCustomerData()!!.msisdn) && hasValue(
                UserDataManager.getCustomerData()!!.subscriberType
            ) && hasValue(UserDataManager.getCustomerData()!!.brandName)
        ) {
            sendFCMToken(
                UserDataManager.getCustomerData()!!.msisdn!!,
                UserDataManager.getCustomerData()!!.subscriberType!!,
                UserDataManager.getCustomerData()!!.brandName!!
            )
        }

        initFCMEvents()
        initNavigationDrawerHeader()
        setManageAccountPopUpRecycler()

        if (isURLLoadSuccess) {
            webView.resumeTimers()
        }

        webView.onResume()
    }//onResume ends

    private fun redirectToTariffs(actionIdFromServer: String) {
        val mBundle = Bundle()
        mBundle.putString(
            ConstantsUtility.TariffConstants.NOTIFICATION_TO_TARIFF_ACTION_ID,
            actionIdFromServer
        )
        Navigation.findNavController(this@MainActivity, R.id.fragmentContainer)
            .navigate(R.id.tarif, mBundle)

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS)
                .setIcon(getTariffsSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD)
                .setIcon(getDashboardUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES)
                .setIcon(getServicesUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET)
                .setIcon(getInternetUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT)
                .setIcon(getMyAccountUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT).title.toString()
            )
        }

        UserDataManager.setActionIdFromServer("")
    }

    override fun onPause() {
        super.onPause()
        killWebViewProgressDialog()
        webView.onPause()
    }

    private fun initFCMEvents() {
        val fCMEvents = object : FCMEvents {
            override fun onFCMTokenRefreshed(
                token: String,
                ringingStatus: String,
                isEnable: String,
                cause: String,
                isBackground: Boolean
            ) {
                if (!this@MainActivity.isFinishing) {
                    var msisdnFCM = ""
                    var subscriberTypeFCM = ""
                    var tariffTypeFCM = ""
                    //get the fcm token and send to the server
                    if (hasValue(getCustomerDataFromLocalPreferences().toString())) {
                        if (hasValue(getCustomerDataFromLocalPreferences().msisdn)) {
                            msisdnFCM = getCustomerDataFromLocalPreferences().msisdn!!
                        }

                        if (hasValue(getCustomerDataFromLocalPreferences().subscriberType)) {
                            subscriberTypeFCM =
                                getCustomerDataFromLocalPreferences().subscriberType!!
                        }


                        if (hasValue(getCustomerDataFromLocalPreferences().brandName)) {
                            tariffTypeFCM = getCustomerDataFromLocalPreferences().brandName!!
                        }

                    }
                    mViewModel.requestFCMTokenSentToServerService(
                        token,
                        ringingStatus,
                        isEnable,
                        cause,
                        msisdnFCM,
                        true,
                        subscriberTypeFCM,
                        tariffTypeFCM
                    )
                }
            }
        }

        RootValues.setFCMEvents(fCMEvents)
    }//initFCMEvents ends

    private fun getSurvey() {
        val errorObserver = Observer<AzerAPIErrorHelperResponse> {
            it.errorDetail?.let { detail ->
                showMessageDialog(
                    applicationContext, this@MainActivity,
                    resources.getString(R.string.popup_error_title),
                    detail
                )
            } ?: run {
                showMessageDialog(
                    applicationContext, this@MainActivity,
                    resources.getString(R.string.popup_error_title),
                    resources.getString(R.string.server_stopped_responding_please_try_again)
                )
            }

        }
        mViewModel.surveyResponseErrorResponse.observe(this@MainActivity, errorObserver)
        mViewModel.getSurvey()
    }

    override fun init() {
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        getDataFromIntent()
        registerAzerFonSetActionBarListener()
        // Set up Action Bar
        setSupportActionBar(toolbar)
        getSurvey()
        /**marquee*/
        manageAccountsAdderButton.isSelected = true

        val host: NavHostFragment = supportFragmentManager
            .findFragmentById(R.id.fragmentContainer) as NavHostFragment? ?: return

        val navController = host.navController

        setupNavigationMenu(navController)
//        setBottomNav(navController)

        /**update the isPassword changed by the user
         * or logged out incase of multiple users*/
        ProjectApplication.getInstance().getLocalPreferneces().addBool(
            ProjectApplication.getInstance()
                .getLocalPreferneces().IS_PASSWORD_CHANGE_MULTI_USER_LOGGED_OUT,
            false
        )

        initRateUsAndroidOfflineEvents()
        subscribeToAppResumeObserver()
        subscribeToMenusObserver()
        subscribeToFCMObserver()
        subscribeToNotificationsCountLiveDataResponse()
        subscribeToLogOutObserver()
        subscribeToHomePageResponseLiveData()
        subscribeToTariffsResponseLiveData()
        initInternetOffersDataEvents()
        initInternetOffersSubscriptionEvents()
        registerTariffsRedirectionFromDashboardEvents()
        registerVerticalMenuClickListener()
        subscribeTochangeTariffsResponseLiveData()
        val toggle = ActionBarDrawerToggle(
            this,
            drawer_layout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        /**check if the app resume data string has some values
         *if the app resume data string has some values,
         * check if has value from the splash (empty)
         * or from the login or the signup
         * if is empty means that the user has already logged in so call the app resume
         * otherwise just get the menus from the api*/
        if (appResumeCallingData.equals(
                ConstantsUtility.BundleKeys.BUNDLE_CALLED_FROM_SETTINGS,
                ignoreCase = true
            )
        ) {
            val settingsActivityIntent = Intent(applicationContext, SettingsActivity::class.java)
            startActivity(settingsActivityIntent)
        } else if (appResumeCallingData.equals(
                ConstantsUtility.BundleKeys.BUNDLE_CALLED_FROM_USER_PROFILE,
                ignoreCase = true
            )
        ) {
            val settingsActivityIntent = Intent(applicationContext, UserProfileActivity::class.java)
            startActivity(settingsActivityIntent)
        } else {
            if (appResumeCallingData.equals(
                    ConstantsUtility.BundleKeys.BUNDLE_DONT_CALL_APP_RESUME,
                    ignoreCase = true
                )
            ) {
                logE("fcm", "logging in...:::", fromClass, "sendFCMToken")

                var msisdnFCM = ""
                var subscriberTypeFCM = ""
                var tariffTypeFCM = ""
                //get the fcm token and send to the server
                if (hasValue(getCustomerDataFromLocalPreferences().toString())) {
                    if (hasValue(getCustomerDataFromLocalPreferences().msisdn)) {
                        msisdnFCM = getCustomerDataFromLocalPreferences().msisdn!!
                    }

                    if (hasValue(getCustomerDataFromLocalPreferences().subscriberType)) {
                        subscriberTypeFCM = getCustomerDataFromLocalPreferences().subscriberType!!
                    }


                    if (hasValue(getCustomerDataFromLocalPreferences().brandName)) {
                        tariffTypeFCM = getCustomerDataFromLocalPreferences().brandName!!
                    }

                }



                sendFCMToken(msisdnFCM, subscriberTypeFCM, tariffTypeFCM)
                //simply get the menus api
                requestAppMenus()
                callGetHomePageAPI()
            } else {
                //call the app resume api here
                if (hasValue(getCustomerDataFromLocalPreferences().toString())) {
                    logE("apisCountX", "inside else hehe", fromClass, "insideIfHehe")
                    val customerData: CustomerData = getCustomerDataFromLocalPreferences()
                    var customerId = ""
                    var entityId = ""

                    if (hasValue(customerData.msisdn)) {

                        var msisdnFCM = ""
                        var subscriberTypeFCM = ""
                        var tariffTypeFCM = ""
                        //get the fcm token and send to the server
                        if (hasValue(customerData.toString())) {
                            if (hasValue(customerData.msisdn)) {
                                msisdnFCM = customerData.msisdn!!
                            }

                            if (hasValue(customerData.subscriberType)) {
                                subscriberTypeFCM = customerData.subscriberType!!
                            }


                            if (hasValue(customerData.brandName)) {
                                tariffTypeFCM = customerData.brandName!!
                            }

                        }

                        sendFCMToken(msisdnFCM, subscriberTypeFCM, tariffTypeFCM)


                    }

                    if (hasValue(customerData.customerId)) {
                        customerId = customerData.customerId!!
                    }

                    if (hasValue(customerData.entityId)) {
                        entityId = customerData.entityId!!
                    }

                    /**on request app resume, if it gives success
                     * on success of app resume get the menus from the cache
                     * or the menus api
                     * if app resume fails, logout the user forcefully
                     * juni1289*/
                    if (!UserDataManager.isAppResumeCalledWithSuccess()) {
                        mViewModel.requestAppResume(customerId, entityId)
                    }
                } else {
                    //customer data is null so logout the user
                    logOut(
                        applicationContext,
                        this@MainActivity,
                        false,
                        fromClass,
                        "csak23lkj2kljc"
                    )
                    return
                }
            }
        }
        setManageAccountPopUpRecycler()

        contentMainLayout.visibility = View.VISIBLE
        webViewContentLayout.visibility = View.GONE
        initUIEvents()

        val apisCalledCount = object : Observer<Int> {
            override fun onChanged(t: Int?) {
                if (t != null && t == 1) {
                    totalCountOfApis = t
                    logE("onapisCallCount", "Count:::".plus("111"), fromClass, "onChanged")

                } else {
                    logE("onapisCallCount", "Count:::".plus(t), fromClass, "onChanged")
                }
            }
        }

        mViewModel.apisCalledCount.observe(this@MainActivity, apisCalledCount)

    }//init ends

    private fun initRateUsAndroidOfflineEvents() {
        val rateUsAndroidOfflineEvents = object : RateUsAndroidOfflineEvents {
            override fun onNeedToShowRateUsPopUp() {
                processRateUsDialog()
            }
        }

        RootValues.setRateUsAndroidOfflineEvents(rateUsAndroidOfflineEvents)
    }

    private fun subscribeToTariffsResponseLiveData() {
        val tariffsSilentResponseLiveData = object : Observer<TariffsResponse> {
            override fun onChanged(tariffsResponse: TariffsResponse?) {
                if (!this@MainActivity.isFinishing) {
                    if (tariffsResponse != null && hasValue(tariffsResponse.toString()) &&
                        tariffsResponse.data != null && hasValue(tariffsResponse.data.toString()) &&
                        tariffsResponse.data.tariffResponseList != null && tariffsResponse.data.tariffResponseList.isNotEmpty()
                    ) {
                        RootValues.getTariffsResponseEvents()
                            .onTariffsBackgroundResponse(tariffsResponse)
                        RootValues.getTariffsResponseEvents().onTariffsResponseShowData()
                    }
                }
            }
        }

        mViewModel.tariffsSilentResponseLiveData.observe(
            this@MainActivity,
            tariffsSilentResponseLiveData
        )

        val tariffsResponseLiveData = object : Observer<TariffsResponse> {
            override fun onChanged(tariffsResponse: TariffsResponse?) {
                if (!this@MainActivity.isFinishing) {
                    RootValues.getTariffsResponseEvents().onNoTariffsResponseHideData()
                    if (tariffsResponse != null && hasValue(tariffsResponse.toString())) {
                        if (tariffsResponse.data != null && hasValue(tariffsResponse.data.toString())) {
                            if (tariffsResponse.data.tariffResponseList != null && tariffsResponse.data.tariffResponseList.isNotEmpty()) {
                                RootValues.getTariffsResponseEvents().onTariffsResponseShowData()
                                RootValues.getTariffsResponseEvents()
                                    .onTariffsResponse(tariffsResponse)
                            } else {
                                RootValues.getTariffsResponseEvents().onNoTariffsResponseHideData()
                            }
                        } else {
                            RootValues.getTariffsResponseEvents().onNoTariffsResponseHideData()
                        }
                    } else {
                        RootValues.getTariffsResponseEvents().onNoTariffsResponseHideData()
                    }
                }
            }//onChanged ends
        }//val tariffsResponseLiveData = object : Observer<TariffsResponse> ends

        mViewModel.tariffsResponseLiveData.observe(this@MainActivity, tariffsResponseLiveData)

        val tariffsResponseErrorLiveData = object : Observer<AzerAPIErrorHelperResponse> {
            override fun onChanged(response: AzerAPIErrorHelperResponse?) {
                if (!this@MainActivity.isFinishing) {
                    //check internet
                    RootValues.getTariffsResponseEvents().onNoTariffsResponseHideData()
                    if (!isNetworkAvailable(this@MainActivity)) {
                        showMessageDialog(
                            applicationContext, this@MainActivity,
                            resources.getString(R.string.popup_note_title),
                            resources.getString(R.string.message_no_internet)
                        )
                    } else {
                        /**check the error code
                         * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                         * else show a general api failure popup*/
                        if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                            showMessageDialog(
                                applicationContext, this@MainActivity,
                                resources.getString(R.string.popup_error_title),
                                response.errorDetail.toString()
                            )
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                            //force logout the user here
                            logOut(
                                applicationContext,
                                this@MainActivity,
                                false,
                                fromClass,
                                "tariffsResponseErrorLiveDataasd23232weas"
                            )
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                            //handle the ui in this case
                            //hide or show the no data found layout in this case
                            showMessageDialog(
                                applicationContext, this@MainActivity,
                                resources.getString(R.string.popup_error_title),
                                resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                            showMessageDialog(
                                applicationContext, this@MainActivity,
                                resources.getString(R.string.popup_error_title),
                                resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                        } else {
                            showMessageDialog(
                                applicationContext, this@MainActivity,
                                resources.getString(R.string.popup_error_title),
                                resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                        }
                    }
                }// if (activity != null && !this@MainActivity.isFinishing)  ends
            }
        }

        mViewModel.tariffsResponseErrorLiveData.observe(
            this@MainActivity,
            tariffsResponseErrorLiveData
        )
    }//subscribeToTariffsResponseLiveData ends

    private fun subscribeToHomePageResponseLiveData() {
        val getRateUsResponseLiveData = object : Observer<String> {
            override fun onChanged(t: String?) {
                if (!this@MainActivity.isFinishing) {
                    if (t != null && hasValue(t) && t == "0") {
                        processRateUsDialog()
                        UserDataManager.setRateUsAndroid("0")
                        logE(
                            "rateUsX",
                            "rate_android:::".plus(t),
                            fromClass,
                            "subscribeToGetRateResponse"
                        )
                    } else {
                        UserDataManager.setRateUsAndroid("1")
                        logE(
                            "rateUsX",
                            "rate_android:::Problem",
                            fromClass,
                            "subscribeToGetRateResponse"
                        )
                    }
                }
            }
        }

        mViewModel.getRateUsResponseLiveData.observe(this@MainActivity, getRateUsResponseLiveData)

        val getHomePageResponseSilentData = object : Observer<GetHomePageResponse> {
            override fun onChanged(t: GetHomePageResponse?) {
                if (!this@MainActivity.isFinishing) {
                    if (t != null && t.data != null && hasValue(t.data.toString())) {
                        val apisCount = mViewModel.apisCalledCount.getValue()!!
                        mViewModel.apisCalledCount.value = apisCount + 1

                        Handler().postDelayed({
                            if (RootValues.getHomePageResponseEvents() != null) {
                                RootValues.getHomePageResponseEvents()!!.onHomePageResponse(t)
                            }
                        }, 50L)

                        UserDataManager.setHomePageResponse(t)

                        //save the response in shared preferences
                        setHomePageResponseToLocalPreferences(t)
                        mViewModel.requestGetNotificationsCount()

                        if (hasValue(getCustomerDataFromLocalPreferences().entityId)) {
                            logE(
                                "entity1289",
                                "entity id:::".plus(getCustomerDataFromLocalPreferences().entityId),
                                fromClass,
                                "subscribe"
                            )
                            mViewModel.requestGetRateUs(getCustomerDataFromLocalPreferences().entityId!!)
                        } else {
                            logE("entity1289", "no entity id", fromClass, "subscribe")
                        }
                    } else {
                        logE("entity1289", "response errrorrrr", fromClass, "subscribe")
                    }
                }
            }
        }

        mViewModel.getHomePageResponseSilentData.observe(
            this@MainActivity,
            getHomePageResponseSilentData
        )

        val getHomePageResponseData = object : Observer<GetHomePageResponse> {
            override fun onChanged(t: GetHomePageResponse?) {
                if (!this@MainActivity.isFinishing) {
                    loadDashboardScreen()

                    if (t != null) {
                        if (t.data != null) {
                            if (hasValue(t.data.toString())) {
                                val apisCount = mViewModel.apisCalledCount.getValue()!!
                                mViewModel.apisCalledCount.value = apisCount + 1

                                Handler().postDelayed({
                                    if (RootValues.getHomePageResponseEvents() != null) {
                                        RootValues.getHomePageResponseEvents()!!
                                            .onHomePageResponse(t)
                                    }
                                }, 50L)

                                UserDataManager.setIsGetHomePageCalledWithSuccess(true)
                                UserDataManager.setHomePageResponse(t)

                                //save the response in shared preferences
                                setHomePageResponseToLocalPreferences(t)
                                mViewModel.requestGetNotificationsCount()

                                /**check if the promo message is there*/
                                if (UserDataManager.getPromoMessage() != null && hasValue(
                                        UserDataManager.getPromoMessage()
                                    )
                                ) {
                                    /**check if the promo message is displayed already*/
                                    if (!UserDataManager.isPromoMessageDisplayed()) {
                                        //promo message not displayed
                                        showPromoMessage(UserDataManager.getPromoMessage()!!)
                                    } else {
                                        //promo message is already displayed
                                        if (hasValue(getCustomerDataFromLocalPreferences().entityId)) {
                                            mViewModel.requestGetRateUs(
                                                getCustomerDataFromLocalPreferences().entityId!!
                                            )
                                        }
                                    }
                                } else {
                                    //promo message is null or empty
                                    if (hasValue(getCustomerDataFromLocalPreferences().entityId)) {
                                        mViewModel.requestGetRateUs(
                                            getCustomerDataFromLocalPreferences().entityId!!
                                        )
                                    }
                                }
                            } else {
                                UserDataManager.setIsGetHomePageCalledWithSuccess(false)
                                showMessageDialog(
                                    applicationContext, this@MainActivity,
                                    this@MainActivity.resources.getString(R.string.popup_error_title),
                                    this@MainActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            }
                        } else {
                            UserDataManager.setIsGetHomePageCalledWithSuccess(false)
                            showMessageDialog(
                                applicationContext, this@MainActivity,
                                this@MainActivity.resources.getString(R.string.popup_error_title),
                                this@MainActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                        }
                    } else {
                        UserDataManager.setIsGetHomePageCalledWithSuccess(false)
                        showMessageDialog(
                            applicationContext, this@MainActivity,
                            this@MainActivity.resources.getString(R.string.popup_error_title),
                            this@MainActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    }
                }
            }
        }

        mViewModel.getHomePageResponseData.observe(this@MainActivity, getHomePageResponseData)

        val getHomePageErrorResponseData = object : Observer<AzerAPIErrorHelperResponse> {
            override fun onChanged(response: AzerAPIErrorHelperResponse?) {
                if (!this@MainActivity.isFinishing) {
                    //refreshAllViews()
                    logE("homePageResponse", "response:::Error", fromClass, "subscribe1")
                    loadDashboardScreen()

                    Handler().postDelayed({
                        if (RootValues.getHomePageResponseEvents() != null) {
                            RootValues.getHomePageResponseEvents()!!.onCancelSwipeRefresh()
                        }
                    }, 50L)
                    UserDataManager.setIsGetHomePageCalledWithSuccess(false)
                    //check internet
                    if (!isNetworkAvailable(this@MainActivity)) {
                        showMessageDialog(
                            applicationContext, this@MainActivity,
                            this@MainActivity.resources.getString(R.string.popup_error_title),
                            this@MainActivity.resources.getString(R.string.message_no_internet)
                        )
                    } else {
                        /**check the error code
                         * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                         * else show a general api failure popup*/
                        if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                            showMessageDialog(
                                applicationContext, this@MainActivity,
                                this@MainActivity.resources.getString(R.string.popup_error_title),
                                response.errorDetail.toString()
                            )
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                            //force logout the user here
                            logOut(
                                applicationContext,
                                this@MainActivity,
                                false,
                                fromClass,
                                "onChanged3342321asdd"
                            )
                            return
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                            //handle the ui in this case
                            //hide or show the no data found layout in this case
                            showMessageDialog(
                                applicationContext, this@MainActivity,
                                this@MainActivity.resources.getString(R.string.popup_error_title),
                                this@MainActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                            showMessageDialog(
                                applicationContext, this@MainActivity,
                                this@MainActivity.resources.getString(R.string.popup_error_title),
                                this@MainActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                        } else {
                            showMessageDialog(
                                applicationContext, this@MainActivity,
                                this@MainActivity.resources.getString(R.string.popup_error_title),
                                this@MainActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                        }
                    }
                }// if (activity != null && !this@MainActivity.isFinishing)  ends
            }
        }

        mViewModel.getHomePageErrorResponseData.observe(
            this@MainActivity,
            getHomePageErrorResponseData
        )

        val rateUsBeforePlayStoreRedirectionLiveDataResponse =
            object : Observer<RateUsBeforeRedirectingToPlayStoreRespone> {
                override fun onChanged(t: RateUsBeforeRedirectingToPlayStoreRespone?) {
                    if (!this@MainActivity.isFinishing) {
                        //redirect to the play store
                        //success here
                        redirectUserToPlayStore()
                    }
                }
            }

        mViewModel.rateUsBeforePlayStoreRedirectionLiveDataResponse.observe(
            this@MainActivity,
            rateUsBeforePlayStoreRedirectionLiveDataResponse
        )

    }//subscribeToHomePageResponseLiveData ends

    @SuppressLint("InflateParams")
    private fun showPromoMessage(promoMessage: String) {
        if (!this@MainActivity.isFinishing) {

            val dialogBuilder = AlertDialog.Builder(this@MainActivity)
            val inflater =
                this@MainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val dialogView = inflater.inflate(R.layout.promo_message_popup, null)
            dialogBuilder.setView(dialogView)

            val alertDialog = dialogBuilder.create()
            alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            alertDialog.setCancelable(false)
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()

            val popupTitle = dialogView.findViewById(R.id.popupTitle) as TextView
            popupTitle.typeface = getALSNormalFont()
            popupTitle.text = promoMessage
            val popupOkayButton = dialogView.findViewById(R.id.yesButton) as Button
            popupOkayButton.typeface = getALSBoldFont()

            popupOkayButton.text = getOkButtonLabel()

            popupOkayButton.setOnClickListener {
                alertDialog.dismiss()
                if (hasValue(getCustomerDataFromLocalPreferences().entityId)) {
                    mViewModel.requestGetRateUs(getCustomerDataFromLocalPreferences().entityId!!)
                }
            }

            /**the promo message is displayed
             * dont show it anymore
             * because it should be shown only once*/
            UserDataManager.setIsPromoMessageDisplayed(true)
            UserDataManager.setPromoMessage(null)

        }//activity is not null or finishing
    }//showPromoMessage ends

    private fun processRateUsDialog() {
        if (this@MainActivity.isFinishing) {
            return //safe passage
        }

        if (rateUsPopUpAlert != null && rateUsPopUpAlert!!.isShowing) {
            logE("rateUsVXW", "already Showing", fromClass, "processRateUsDialog")
            return
        }

        try {
            //check for app rater dialog
            //check if app rater dialog has been shown to the user or not
            if (isAppRaterDialogShownToUser()) {
                //user already have been shown the dialog
                //get the current date
                val currentDateTime = getCurrentDateTimeForRateUs()
                logE("timeX", "if showTime:::".plus(currentDateTime), fromClass, "dialogTime")
                //get the may be later date and time
                val mayBeLaterDateTime = getMayBeLaterDateTime()
                if (hasValue(currentDateTime) && hasValue(mayBeLaterDateTime)) {
                    val differenceInDays =
                        getDatesDifferenceForRateUs(mayBeLaterDateTime, currentDateTime, "d")
                    logE(
                        "timeX",
                        "if showDifference:::".plus(differenceInDays),
                        fromClass,
                        "dialogTime"
                    )
                    //get latepopon
                    if (UserDataManager.getCustomerData() != null) {
                        if (UserDataManager.getCustomerData()!!.lateOnPopup != null) {
                            if (hasValue(UserDataManager.getCustomerData()!!.lateOnPopup)) {
                                val latePopupOn =
                                    (UserDataManager.getCustomerData()!!.lateOnPopup!!).toInt()
                                logE(
                                    "timeX",
                                    "lateon popup:::".plus(latePopupOn),
                                    fromClass,
                                    "dialogTime"
                                )
                                if (differenceInDays >= latePopupOn) {
                                    if (hasValue(UserDataManager.getCustomerData()!!.popupTitle)) {
                                        val dialogTitle =
                                            UserDataManager.getCustomerData()!!.popupTitle
                                        //please show the dialog
                                        showRateUsPopup(dialogTitle!!)
                                    }
                                }//if (differenceInDays >= latePopupOn) ends
                            }//if(TextCheckerTool.hasValue(UserDataManager.Singleton.getInstance().getPredefinedData()!!.lateOnPopup)) ends
                        }//if(UserDataManager.Singleton.getInstance().getPredefinedData()!!.lateOnPopup!=null) ends
                    }//if(UserDataManager.Singleton.getInstance().getPredefinedData()!=null) ends
                }//if (TextCheckerTool.hasValue(currentDateTime) && TextCheckerTool.hasValue(mayBeLaterDateTime)) ends
            } else {
                //get the current date
                val currentDateTime = getCurrentDateTimeForRateUs()
                logE("timeX", "else showTime:::".plus(currentDateTime), fromClass, "dialogTime")
                //get the login date and time
                val loginDateTime = getUserLoginTime()
                logE("timeX", "loginTime3263:::".plus(loginDateTime), fromClass, "dialogTime")
                if (hasValue(currentDateTime) && hasValue(loginDateTime)) {
                    val differenceInHrs =
                        getDatesDifferenceForRateUs(currentDateTime, loginDateTime, "h")
                    logE(
                        "timeX",
                        "else showDifference:::".plus(differenceInHrs),
                        fromClass,
                        "dialogTime"
                    )
                    if (UserDataManager.getCustomerData() != null) {
                        if (UserDataManager.getCustomerData()!!.firstPopup != null) {
                            if (hasValue(UserDataManager.getCustomerData()!!.firstPopup)) {
                                //get the firstpop value
                                val firstPopUpValue =
                                    (UserDataManager.getCustomerData()!!.firstPopup!!).toInt()
                                logE(
                                    "timeX",
                                    "else firstpopup:::".plus(firstPopUpValue),
                                    fromClass,
                                    "dialogTime"
                                )
                                if (differenceInHrs >= firstPopUpValue) {
                                    if (hasValue(UserDataManager.getCustomerData()!!.popupTitle)) {
                                        val dialogTitle =
                                            UserDataManager.getCustomerData()!!.popupTitle
                                        //please show the dialog
                                        showRateUsPopup(dialogTitle!!)
                                    }
                                }//if(differenceInHrs>=firstPopUpValue) ends
                            }//if(TextCheckerTool.hasValue(UserDataManager.Singleton.getInstance().getPredefinedData()!!.firstPopup)) ends
                        }// if(UserDataManager.Singleton.getInstance().getPredefinedData()!!.firstPopup!=null) ends
                    }// if(UserDataManager.Singleton.getInstance().getPredefinedData()!=null) ends
                }//if(TextCheckerTool.hasValue(currentDateTime)&&TextCheckerTool.hasValue(loginDateTime)) ends
            }//if (isAppRaterDialogShownToUser()) ends
        } catch (exp: Exception) {
            logE("rateUs", "error:::".plus(exp.toString()), fromClass, "processRateUsDialog")
        }
    }//processRateUsDialog ends

    var rateUsPopUpAlert: AlertDialog? = null

    @SuppressLint("InflateParams")
    private fun showRateUsPopup(title: String) {
        if (!this@MainActivity.isFinishing) {
            if (rateUsPopUpAlert != null && rateUsPopUpAlert!!.isShowing) {
                return
            }

            val dialogBuilder = AlertDialog.Builder(this@MainActivity)
            val inflater =
                this@MainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val dialogView = inflater.inflate(R.layout.azer_rateus_popup, null)
            dialogBuilder.setView(dialogView)

            rateUsPopUpAlert = dialogBuilder.create()
            rateUsPopUpAlert!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            rateUsPopUpAlert!!.setCancelable(false)
            rateUsPopUpAlert!!.setCanceledOnTouchOutside(false)
            rateUsPopUpAlert!!.show()

            val rateUsButton = dialogView.findViewById(R.id.rateUsButton) as Button
            val rateUsThanksTitle = dialogView.findViewById(R.id.rateUsThanksTitle) as TextView
            val rateUsTitle = dialogView.findViewById(R.id.rateUsTitle) as TextView
            rateUsTitle.text = title

            rateUsButton.setOnClickListener {
                ProjectApplication.getInstance().getLocalPreferneces().addBool(
                    ProjectApplication.getInstance().getLocalPreferneces().PREF_APP_RATER_SHOWN,
                    true
                )
                rateUsPopUpAlert!!.dismiss()
                if (hasValue(UserDataManager.getPlayStoreRedirectionURL())) {
                    //proceed user to the play store on success of below api
                    callRateUsAPIBeforeRedirection()
                } else {
                    showMessageDialog(
                        applicationContext,
                        this@MainActivity,
                        "",
                        getLocalizedWebViewErrorLabel()
                    )
                }
            }

            rateUsThanksTitle.setOnClickListener {
                //save the current date time
                setMayBeLaterDateTime(getCurrentDateTime())
                ProjectApplication.getInstance().getLocalPreferneces().addBool(
                    ProjectApplication.getInstance().getLocalPreferneces().PREF_APP_RATER_SHOWN,
                    true
                )
                rateUsPopUpAlert!!.dismiss()
                //do nothing
            }
        }
    }

    private fun callRateUsAPIBeforeRedirection() {
        mViewModel.requestRateUsBeforeRedirectingToPlayStore()
    }//callRateUsAPIBeforeRedirection ends

    private fun redirectUserToPlayStore() {
        if (mViewModel.getGotoPlayStoreIntent() != null) {
            startActivity(mViewModel.getGotoPlayStoreIntent())
        } else {
            showMessageDialog(
                applicationContext,
                this@MainActivity,
                "",
                getLocalizedWebViewErrorLabel()
            )
        }
    }//redirectUserToPlayStore ends

    private fun getLocalizedWebViewErrorLabel(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "Something went wrong during redirection. We apologize for any inconvenience."
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Yönləndirmə zamanı səhv baş verdi. Narahatçılığa görə üzr istəyirik."
            else -> "Произошла ошибка во время перенаправления. Приносим извинения за доставленные неудобства."
        }
    }

    private fun subscribeTochangeTariffsResponseLiveData() {
        val changeTariffResponseLiveData = object : Observer<TariffSubscriptionResponse> {
            override fun onChanged(response: TariffSubscriptionResponse?) {
                if (!this@MainActivity.isFinishing) {
                    if (response != null &&
                        hasValue(response.data.toString()) &&
                        hasValue(response.data.message)
                    ) {
//                        showMessageDialog(
//                            applicationContext,
//                            this@MainActivity,
//                            "",
//                            response.data.message!!
//                        )

                        inAppFeedback(
                            this@MainActivity,
                            resources.getString(R.string.popup_note_title),
                            response.data.message!!
                        )
                        /* UserDataManager.setIsGetHomePageCalledWithSuccess(false)
                         removeHomePageResponseFromLocalPreferences()*/
                    } else {
                        showMessageDialog(
                            applicationContext,
                            this@MainActivity,
                            "",
                            "response not set by the backend"
                        )
                    }
                }
            }
        }

        mViewModel.changeTariffResponseLiveData.observe(
            this@MainActivity,
            changeTariffResponseLiveData
        )
    }
    private fun inAppFeedback(context: Context, title: String, message: String) {

        var inAppFeedbackDialog: InAppFeedbackDialog? = null
        var currentVisit: Int =
            LocalSharedPrefStorage(context).getInt(LocalSharedPrefStorage.PREF_TARIFF_PAGE)
        currentVisit += 1
        val inAppSurvey: InAppSurvey? = UserDataManager.getInAppSurvey()
        if ((inAppSurvey?.data) != null) {
            val surveys: Surveys? =
                inAppSurvey.data.findSurvey(ConstantsUtility.InAppSurveyConstants.TARIFF_PAGE)
            if (surveys != null) {
                inAppFeedbackDialog = InAppFeedbackDialog(context, object : OnClickSurveySubmit {
                    override fun onClickSubmit(
                        uploadSurvey: UploadSurvey,
                        onTransactionComplete: String,updateListOnSuccessfullySurvey: UpdateListOnSuccessfullySurvey?
                    ) {
                        mViewModel.uploadInAppSurvey(context, uploadSurvey, onTransactionComplete,updateListOnSuccessfullySurvey)
                        if (inAppFeedbackDialog?.isShowing == true) {
                            inAppFeedbackDialog?.dismiss()
                        }
                    }

                })
                if (surveys.questions != null) {
                    currentVisit = 0
                    inAppFeedbackDialog.showTitleWithMessageDialog(
                        surveys,
                        title,
                        message
                    )
                } else {
                    showMessageDialog(
                        applicationContext, this@MainActivity,
                        title,
                        message
                    )
                }
            } else {
                showMessageDialog(
                    applicationContext, this@MainActivity,
                    title,
                    message
                )
            }
        } else {
            showMessageDialog(
                applicationContext, this@MainActivity,
                resources.getString(R.string.popup_note_title),
                message
            )
        }
        LocalSharedPrefStorage(this@MainActivity).addInt(
            LocalSharedPrefStorage.PREF_TARIFF_PAGE,
            currentVisit
        )
    }
    private fun initInternetOffersDataEvents() {

        val internetOffersResponseLiveData = object : Observer<InternetPacksResponse> {
            override fun onChanged(offers: InternetPacksResponse?) {
                if (!this@MainActivity.isFinishing) {
                    RootValues.getInternetOffersResponseEvents()?.onResponseSuccessful(offers)
                }
            }
        }

        mViewModel.internetResponseLiveData.observe(
            this@MainActivity,
            internetOffersResponseLiveData
        )

        val internetOffersErrorResponseLiveData = object : Observer<AzerAPIErrorHelperResponse> {
            override fun onChanged(response: AzerAPIErrorHelperResponse?) {
                if (!this@MainActivity.isFinishing) {
                    RootValues.getInternetOffersResponseEvents()?.onResponseError()
                    if (!isNetworkAvailable(this@MainActivity)) {
                        showMessageDialog(
                            this@MainActivity, this@MainActivity,
                            resources.getString(R.string.popup_note_title),
                            resources.getString(R.string.message_no_internet)
                        )
                    } else {
                        /**check the error code
                         * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                         * else show a general api failure popup*/
                        /**check the error code
                         * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                         * else show a general api failure popup*/
                        /**check the error code
                         * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                         * else show a general api failure popup*/
                        /**check the error code
                         * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                         * else show a general api failure popup*/
                        when {
                            response?.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION -> showMessageDialog(
                                this@MainActivity, this@MainActivity,
                                this@MainActivity.resources.getString(R.string.popup_error_title),
                                response.errorDetail.toString()
                            )
                            response?.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT -> //force logout the user here
                                logOut(
                                    this@MainActivity,
                                    this@MainActivity,
                                    false,
                                    fromClass,
                                    "specialOffersErrorResponseData2321sf"
                                )
                            response?.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE -> //handle the ui in this case
                                //hide or show the no data found layout in this case
                                showMessageDialog(
                                    this@MainActivity, this@MainActivity,
                                    this@MainActivity.resources.getString(R.string.popup_error_title),
                                    this@MainActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            response?.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE -> showMessageDialog(
                                this@MainActivity, this@MainActivity,
                                this@MainActivity.resources.getString(R.string.popup_error_title),
                                this@MainActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                            else -> showMessageDialog(
                                this@MainActivity, this@MainActivity,
                                this@MainActivity.resources.getString(R.string.popup_error_title),
                                this@MainActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                        }
                    }
                }
            }
        }

        mViewModel.internetResponseErrorLiveData.observe(
            this@MainActivity,
            internetOffersErrorResponseLiveData
        )
    }

    private fun initInternetOffersSubscriptionEvents() {

        /** subscribe to the observers for the api called above*/
        val subscribeToSupplementaryOfferResponseLiveData =
            object : Observer<MySubscriptionsSuccessAfterOfferSubscribeResponse> {
                override fun onChanged(t: MySubscriptionsSuccessAfterOfferSubscribeResponse?) {
                    if (!this@MainActivity.isFinishing) {
                        if (t != null && hasValue(t.toString()) &&
                            t.data != null && hasValue(t.data.toString()) &&
                            t.data.mySubscriptionsData != null
                        ) {
                            RootValues.getInternetOfferSubscriptionResponseEvents()
                                ?.onInternetSubscribeSuccessful(t)
                        }
                    }
                }
            }

        mViewModel.subscribeToSupplementaryOfferResponseLiveData.observe(
            this@MainActivity,
            subscribeToSupplementaryOfferResponseLiveData
        )

        val surveyResponseErrorResponse =
            Observer<AzerAPIErrorHelperResponse> { response ->
                if (!isNetworkAvailable(this@MainActivity)) {
                    showMessageDialog(
                        this@MainActivity, this@MainActivity,
                        resources.getString(R.string.popup_note_title),
                        resources.getString(R.string.message_no_internet)
                    )
                } else {
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                        showMessageDialog(
                            this@MainActivity, this@MainActivity,
                            this@MainActivity.resources.getString(R.string.popup_error_title),
                            response.errorDetail.toString()
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                        //force logout the user here
                        logOut(
                            this@MainActivity,
                            this@MainActivity,
                            false,
                            fromClass,
                            "initSupplementaryOffersCardAdapterEventsa34324"
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                        //handle the ui in this case
                        //hide or show the no data found layout in this case
                        showMessageDialog(
                            this@MainActivity, this@MainActivity,
                            this@MainActivity.resources.getString(R.string.popup_error_title),
                            this@MainActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                        showMessageDialog(
                            this@MainActivity, this@MainActivity,
                            this@MainActivity.resources.getString(R.string.popup_error_title),
                            this@MainActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    } else {
                        showMessageDialog(
                            this@MainActivity, this@MainActivity,
                            this@MainActivity.resources.getString(R.string.popup_error_title),
                            this@MainActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    }
                }
            }
        mViewModel.surveyResponseErrorResponse.observe(
            this@MainActivity,
            surveyResponseErrorResponse
        )
        mViewModel.inAppSurveyResponseErrorResponseData.observe(
            this@MainActivity,
            surveyResponseErrorResponse
        )

        val subscribeToSupplementaryOfferErrorResponseLiveData =
            object : Observer<AzerAPIErrorHelperResponse> {
                override fun onChanged(response: AzerAPIErrorHelperResponse?) {
                    if (!this@MainActivity?.isFinishing!!) {
                        RootValues.getInternetOfferSubscriptionResponseEvents()
                            ?.onInternetSubscribeError()
                        //check internet
                        if (!isNetworkAvailable(this@MainActivity)) {
                            showMessageDialog(
                                this@MainActivity, this@MainActivity,
                                resources.getString(R.string.popup_note_title),
                                resources.getString(R.string.message_no_internet)
                            )
                        } else {
                            /**check the error code
                             * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                             * else show a general api failure popup*/
                            if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                                showMessageDialog(
                                    this@MainActivity, this@MainActivity,
                                    this@MainActivity.resources.getString(R.string.popup_error_title),
                                    response.errorDetail.toString()
                                )
                            } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                                //force logout the user here
                                logOut(
                                    this@MainActivity,
                                    this@MainActivity,
                                    false,
                                    fromClass,
                                    "initSupplementaryOffersCardAdapterEventsa34324"
                                )
                            } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                                //handle the ui in this case
                                //hide or show the no data found layout in this case
                                showMessageDialog(
                                    this@MainActivity, this@MainActivity,
                                    this@MainActivity.resources.getString(R.string.popup_error_title),
                                    this@MainActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                                showMessageDialog(
                                    this@MainActivity, this@MainActivity,
                                    this@MainActivity.resources.getString(R.string.popup_error_title),
                                    this@MainActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            } else {
                                showMessageDialog(
                                    this@MainActivity, this@MainActivity,
                                    this@MainActivity.resources.getString(R.string.popup_error_title),
                                    this@MainActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            }
                        }
                    }// if (activity != null && !this@SupplementaryOffersActivity.isFinishing)  ends
                }
            }

        mViewModel.subscribeToSupplementaryOfferErrorResponseLiveData.observe(
            this@MainActivity,
            subscribeToSupplementaryOfferErrorResponseLiveData
        )
    }

    private fun registerTariffsRedirectionFromDashboardEvents() {
        val tariffRedirectionFromDashboardEvents = object : TariffRedirectionFromDashboardEvents {
            override fun onRedirectToTariffsScreen(offeringId: String) {
                logE(
                    "OidX",
                    "offeringId:::".plus(offeringId),
                    fromClass,
                    "registerTariffsRedirectionFromDashboardEvents"
                )
                //tariffs
                val mBundle = Bundle()
                mBundle.putString(
                    ConstantsUtility.TariffConstants.TARIFF_REDIRECTION_OFFER_ID,
                    offeringId
                )
                Navigation.findNavController(this@MainActivity, R.id.fragmentContainer)
                    .navigate(R.id.tarif, mBundle)

                if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS) != null) {
                    navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS)
                        .setIcon(getTariffsSelectedIcon())
                        .title = getMenuTextColoredString(
                        resources.getColor(R.color.colorMenuSelected),
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS).title.toString()
                    )
                }

                if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD) != null) {
                    navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD)
                        .setIcon(getDashboardUnSelectedIcon())
                        .title = getMenuTextColoredString(
                        resources.getColor(R.color.colorMenuUnSelected),
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD).title.toString()
                    )
                }

                if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES) != null) {
                    navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES)
                        .setIcon(getServicesUnSelectedIcon())
                        .title = getMenuTextColoredString(
                        resources.getColor(R.color.colorMenuUnSelected),
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES).title.toString()
                    )
                }

                if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET) != null) {
                    navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET)
                        .setIcon(getInternetUnSelectedIcon())
                        .title = getMenuTextColoredString(
                        resources.getColor(R.color.colorMenuUnSelected),
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET).title.toString()
                    )
                }

                if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT) != null) {
                    navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT)
                        .setIcon(getMyAccountUnSelectedIcon())
                        .title = getMenuTextColoredString(
                        resources.getColor(R.color.colorMenuUnSelected),
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT).title.toString()
                    )
                }
            }
        }

        RootValues.setTariffRedirectionFromDashboardEvents(tariffRedirectionFromDashboardEvents)
    }//registerTariffsRedirectionFromDashboardEvents ends

    private fun loadProfileImage() {
        if (hasValue(getProfileImageFromLocalPreferences())) {
            val imageUrl = getProfileImageFromLocalPreferences()
            logE("profileImageX", "url:::".plus(imageUrl), fromClass, "loadProfileImage")
            createTarget()
            if (target != null) {
                Picasso.with(this@MainActivity).load(imageUrl).networkPolicy(NetworkPolicy.NO_CACHE)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(target)
            } //if (target != null) { ends
        }//if url is null
        else {
            //get the url from the login data and recursively call this method again!
            if (UserDataManager.getCustomerData() != null &&
                UserDataManager.getCustomerData()!!.imageURL != null &&
                hasValue(UserDataManager.getCustomerData()!!.imageURL)
            ) {
                val url = UserDataManager.getCustomerData()!!.imageURL
                setProfileImageToLocalPreferences(url!!)
                loadProfileImage()
            }
        }
    }//loadProfileImage ends

    private fun createTarget() {
        target = object : Target {
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {

            }//onPrepareLoad ends

            override fun onBitmapFailed(errorDrawable: Drawable?) {

            }//onBitmapFailed ends

            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                try {
                    profile_image.setImageBitmap(bitmap)
                } catch (e: Exception) {
                    logE("picasso", "error:::".plus(e.toString()), fromClass, "onBitmapLoaded")
                }//catch ends
            }//onBitmapLoaded ends
        }//target ends
    }//create target ends

    private fun sendFCMToken(msisdn: String, subscriberTypeFCM: String, tariffTypeFCM: String) {
        logE("fcm", "token sending:::", fromClass, "sendFCMToken")
        val ringingStatus = ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_TONE
        val isEnable = ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_ON
        val cause = ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_API_CAUSE_LOGIN
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                return@OnCompleteListener
            }

            // Get new Instance ID token
            val token = task.result!!
            //request send token service
            mViewModel.requestFCMTokenSentToServerService(
                token,
                ringingStatus,
                isEnable,
                cause,
                msisdn,
                true,
                subscriberTypeFCM,
                tariffTypeFCM
            )
            logE("fcm", "token success:::".plus(token), fromClass, "sendFCMToken")
        })
    }//sendFCMToken ends

    private fun initNavigationDrawerHeader() {
        if (getCustomerDataFromLocalPreferences() != null && hasValue(
                getCustomerDataFromLocalPreferences().msisdn
            )
        ) {

            var firstName = ""
            if (hasValue(getCustomerDataFromLocalPreferences().firstName)) {
                firstName = getCustomerDataFromLocalPreferences().firstName!!
            }

            var lastName = ""
            if (hasValue(getCustomerDataFromLocalPreferences().lastName)) {
                lastName = getCustomerDataFromLocalPreferences().lastName!!
            }

            val fullName = firstName.plus(" ").plus(lastName)
            profileUserNameLabel.text = fullName
            profileUserNameLabel.isSelected = true

            if (hasValue(getCustomerDataFromLocalPreferences().msisdn)) {
                profileUserNumberLabel.text = getCustomerDataFromLocalPreferences().msisdn
                profileUserNumberLabel.isSelected = true
            }
        }

        //load the profile picture
        loadProfileImage()
    }//initNavigationDrawerHeader ends

    private fun initUIEvents() {

        packagesSearchIcon.setOnClickListener {
            azerActionBarTitle.visibility = View.GONE
            searchCrossIcon.visibility = View.VISIBLE
            searchInputField.visibility = View.VISIBLE

            packagesSearchIcon.visibility = View.GONE
            azerActionBarHamburgerIcon.visibility = View.GONE
        }//packagesSearchIcon.setOnClickListener ends

        searchCrossIcon.setOnClickListener {
            azerActionBarTitle.visibility = View.VISIBLE
            searchCrossIcon.visibility = View.GONE
            searchInputField.visibility = View.GONE
            searchInputField.setText("")
            searchInputField.hint = resources.getString(R.string.supplementary_offers_search_hint)

            packagesSearchIcon.visibility = View.VISIBLE
            azerActionBarHamburgerIcon.visibility = View.VISIBLE
        }//searchCrossIcon.setOnClickListener ends

        searchInputField.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }//afterTextChanged ends

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }//beforeTextChanged ends

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                //search using the interface
                if (RootValues.getActionBarSearchEvents() != null) {
                    RootValues.getActionBarSearchEvents()!!.onSearchFilterApply(s.toString())
                }
            }//onTextChanged ends
        })//searchInputField.addTextChangedListener ends

        profileUserNameLabel.setOnClickListener {
            showAccountsList()
        }//profileUserNameLabel.setOnClickListener ends

        userNameTV.setOnClickListener {
            showAccountsList()
        }
        profileUserNumberLabel.setOnClickListener {
            showAccountsList()
        }

        azerActionBarHamburgerIcon.setOnClickListener {
            isManageAccountsLayoutHidden()
            hamBurgerMenuOpen()
        }//azerActionBarHamburgerIcon.setOnClickListener  ends

        azerActionBarBackIcon.setOnClickListener {
            selectDashBoardMenu()
            Navigation.findNavController(this@MainActivity, R.id.fragmentContainer)
                .navigate(R.id.home_dest)
        }//azerActionBarBackIcon.setOnClickListener ends

        azerActionBarBellIcon.setOnClickListener {
            if (isManageAccountsLayoutHidden()) {
                hamBurgerMenuClose()

                NotificationActivity.start(this@MainActivity)
                /*resetBottomMenuIcons()
                Navigation.findNavController(this@MainActivity, R.id.fragmentContainer).navigate(R.id.notification_dest)*/
            }
        }//azerActionBarBellIcon.setOnClickListener ends

        profile_image.setOnClickListener {
            hamBurgerMenuClose()
            isManageAccountsLayoutHidden()
            val userProfileActivityIntent =
                Intent(applicationContext, UserProfileActivity::class.java)
            startActivity(userProfileActivityIntent)
        }

        headerLayoutt.setOnClickListener {
            isManageAccountsLayoutHidden()
        }

        navigationView.setOnNavigationItemSelectedListener(object :
            BottomNavigationView.OnNavigationItemSelectedListener {
            @Suppress("DEPRECATION")
            override fun onNavigationItemSelected(menu: MenuItem): Boolean {
                val host: NavHostFragment = supportFragmentManager
                    .findFragmentById(R.id.fragmentContainer) as NavHostFragment

                val navController = host.navController

                if (menu.itemId == ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD) {
                    //dashboard
                    /**when clicking on this button the
                     * dashboard will be reloaded and so as
                     * don't call the gethomepage api in the background*/
                    val mBundle = Bundle()
                    //    mBundle.putString("Title",navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD).title.toString())
                    mBundle.putString(
                        ConstantsUtility.UserConstants.CALL_HOME_PAGE_API_KEY,
                        "NotCall"
                    )
                    if (navController.currentDestination!!.id != R.id.home_dest) {
                        Navigation.findNavController(this@MainActivity, R.id.fragmentContainer)
                            .navigate(R.id.home_dest, mBundle)
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD)
                            .setIcon(getDashboardSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS)
                            .setIcon(getTariffsUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES)
                            .setIcon(getServicesUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET)
                            .setIcon(getInternetUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP)
                            .setIcon(getTopUpUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT)
                            .setIcon(getMyAccountUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT).title.toString()
                        )
                    }
                } else if (menu.itemId == ConstantsUtility.HorizontalMenuIds.ID_TARIFFS) {
                    if (navController.currentDestination!!.id != R.id.tarif) {
                        //tariffs
                        Navigation.findNavController(this@MainActivity, R.id.fragmentContainer)
                            .navigate(R.id.tarif)
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS)
                            .setIcon(getTariffsSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD)
                            .setIcon(getDashboardUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES)
                            .setIcon(getServicesUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET)
                            .setIcon(getInternetUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP)
                            .setIcon(getTopUpUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT)
                            .setIcon(getMyAccountUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT).title.toString()
                        )
                    }
                } else if (menu.itemId == ConstantsUtility.HorizontalMenuIds.ID_SERVICES) {
                    if (navController.currentDestination!!.id != R.id.services) {
                        Navigation.findNavController(this@MainActivity, R.id.fragmentContainer)
                            .navigate(R.id.services)
                    }

                    /**select the services menu
                     * unselect all other menus*/
                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES)
                            .setIcon(getServicesSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD)
                            .setIcon(getDashboardUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS)
                            .setIcon(getTariffsUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP)
                            .setIcon(getTopUpUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET)
                            .setIcon(getInternetUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT)
                            .setIcon(getMyAccountUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT).title.toString()
                        )
                    }
                } else if (menu.itemId == ConstantsUtility.HorizontalMenuIds.ID_TOPUP) {
                    if (navController.currentDestination!!.id != R.id.topup) {
                        Navigation.findNavController(this@MainActivity, R.id.fragmentContainer)
                            .navigate(R.id.topup)
                    }

                    /**select the topup menu
                     * unselect all other menus*/
                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP)
                            .setIcon(getTopUpSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD)
                            .setIcon(R.drawable.dashboardunselectedicon)
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS)
                            .setIcon(getTariffsUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES)
                            .setIcon(getServicesUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET)
                            .setIcon(getInternetUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT)
                            .setIcon(R.drawable.accountunselectedicon)
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT).title.toString()
                        )
                    }
                } else if (menu.itemId == ConstantsUtility.HorizontalMenuIds.ID_INTERNET) {
                    if (navController.currentDestination!!.id != R.id.internet) {
                        Navigation.findNavController(this@MainActivity, R.id.fragmentContainer)
                            .navigate(R.id.internet)
                    }

                    /**select the internet menu
                     * unselect all other menus*/
                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET)
                            .setIcon(getInternetSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD)
                            .setIcon(getDashboardUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS)
                            .setIcon(getTariffsUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES)
                            .setIcon(getServicesUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP)
                            .setIcon(getTopUpUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT)
                            .setIcon(getMyAccountUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT).title.toString()
                        )
                    }
                } else if (menu.itemId == ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT) {
                    if (navController.currentDestination!!.id != R.id.myaccount) {
                        Navigation.findNavController(this@MainActivity, R.id.fragmentContainer)
                            .navigate(R.id.myaccount)
                    }

                    /**select myaccount menu
                     * deselect all other menus*/
                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT)
                            .setIcon(getMyAccountSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD)
                            .setIcon(getDashboardUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS)
                            .setIcon(getTariffsUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES)
                            .setIcon(getServicesUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP)
                            .setIcon(getTopUpUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP).title.toString()
                        )
                    }

                    if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET) != null) {
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET)
                            .setIcon(getInternetUnSelectedIcon())
                            .title = getMenuTextColoredString(
                            resources.getColor(R.color.colorMenuUnSelected),
                            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET).title.toString()
                        )
                    }
                }

                return false //because we want to show the marquee each time any item clicked
            }
        })
    }

    private fun loadDashboardScreen() {
        val host: NavHostFragment = supportFragmentManager
            .findFragmentById(R.id.fragmentContainer) as NavHostFragment? ?: return

        val navController = host.navController
        if (navController != null && navController.currentDestination != null && navController.currentDestination!!.id == R.id.home_dest) {
            //do nothing
        } else {
            Handler().postDelayed(
                {
                    val mBundle = Bundle()
                    mBundle.putString(
                        ConstantsUtility.UserConstants.CALL_HOME_PAGE_API_KEY,
                        "NotCall"
                    )
                    Navigation.findNavController(this@MainActivity, R.id.fragmentContainer)
                        .navigate(R.id.home_dest, mBundle)
                }, 50L
            )

        }
    }

    private fun showAccountsList() {
        if (manageAccountRecyclerLayout.visibility == View.GONE) {
            manageAccountRecyclerLayout.visibility = View.VISIBLE
        } else {
            manageAccountRecyclerLayout.visibility = View.GONE
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            if (webViewContentLayout.visibility == View.VISIBLE) {
                //check if the webview is visible
                //if visible, check if it can go back
                //if it can go back, redirect it to back
                //else hide the webview container
                //if its not visible, check for other statements!
                if (webView.copyBackForwardList().currentIndex > 0) {
                    webView.goBack()
                } else {
                    webView.pauseTimers()
                    webViewContentLayout.visibility = View.GONE
                    contentMainLayout.visibility = View.VISIBLE
                    drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
                    window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
                }//if (webView.copyBackForwardList().getCurrentIndex() > 0)
            } else {
                drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
                webView.pauseTimers()
                val host: NavHostFragment = supportFragmentManager
                    .findFragmentById(R.id.fragmentContainer) as NavHostFragment? ?: return

                val navController = host.navController
                if (navController.currentDestination!!.id == R.id.home_dest) {
                    finish()
                } else {
                    selectDashBoardMenu()
                    Navigation.findNavController(this@MainActivity, R.id.fragmentContainer)
                        .navigate(R.id.home_dest)
                    Handler().postDelayed({ callGetHomePageAPI() }, 100L)
                    //go back
                }
            }
        }
    }//onBackPressed ends

    private fun setManageAccountPopUpRecycler() {
        manageAccountsAdderButton.isSelected = true
        val mLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(
            this,
            androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
            false
        )

        var list: ArrayList<CustomerData>? = ArrayList()
        if (getUserAccountsDataFromLocalPreferences() != null && getUserAccountsDataFromLocalPreferences().usersList != null) {
            list = getUserAccountsDataFromLocalPreferences().usersList as ArrayList<CustomerData>
        }

        if (UserDataManager.getCustomerData()?.msisdn == null) {
            //update the user data manager
            UserDataManager.saveCustomerData(getCustomerDataFromLocalPreferences())
        }
        for (i in 0 until list?.size!!) {
            if (UserDataManager.getCustomerData()?.msisdn.equals(list.get(i).msisdn)) {
                val currentAccount = list.get(i)
                list.removeAt(i)
                list.add(0, currentAccount)

            }
        }

        manageAccountsRecycler.apply {
            adapter = ManageAccountPopUpRecyclerAdapter(list, listener)
            layoutManager = mLayoutManager
            addItemDecoration(
                androidx.recyclerview.widget.DividerItemDecoration(
                    manageAccountsRecycler.context,
                    androidx.recyclerview.widget.DividerItemDecoration.VERTICAL
                )
            )
        }
    }//setManageAccountPopUpRecycler ends

    fun manageAccounts(view: View) {
        hamBurgerMenuClose()
        /*    Navigation.findNavController(this@MainActivity, R.id.fragmentContainer)
                   .navigate(R.id.manage_account_dest)*/

        ManageAccountActivity.start(this)
    }//manageAccounts ends

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val retValue = super.onCreateOptionsMenu(menu)
        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        if (navigationView == null) {
            return true
        }
        return retValue
    }//onCreateOptionsMenu ends

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return item.onNavDestinationSelected(findNavController(R.id.fragmentContainer)) || super.onOptionsItemSelected(
            item
        )
    }//onOptionsItemSelected ends

    override fun onSupportNavigateUp(): Boolean {
        return findNavController(R.id.fragmentContainer).navigateUp()
    }//onSupportNavigateUp ends

    private fun setupNavigationMenu(navController: NavController) {
        nav_view?.setupWithNavController(navController)
    }//setupNavigationMenu ends

    /**not binding the bottom navigation menu to the navController
     * because on each click event we don't want to change the behavior of menu item
     * for example the marquee stops working unless we return false to update the menu item
     * inside the "navigationView.setOnNavigationItemSelectedListener"
     * juni1289*/
    fun setBottomNav(navController: NavController) {
        navigationView.setupWithNavController(navController)
    }//setBottomNav ends

    private fun requestAppMenus() {
        logE("requestAppMenus", "called", fromClass, "requestAppMenus")
        //get the app menus from the cache if available
        val menusCacheResponse = getMenusDataFromLocalPreferences()
        if (menusCacheResponse.data != null && hasValue(menusCacheResponse.data.toString())) {

            val menuHorizontalList = getHorizontalMenuLocaleBased(menusCacheResponse)
            val menuVerticalList = getVerticalMenuLocaleBased(menusCacheResponse)

            if (menuVerticalList != null && menuHorizontalList != null && menuVerticalList.isNotEmpty() && menuHorizontalList.isNotEmpty()) {
                logE("X89M", "menusCache:::".plus(menuHorizontalList), fromClass, "requestAppMenus")
                processMenus(menuVerticalList, menuHorizontalList)
                /**request the app menus in the background call
                 * if any new menus is added will be shown in this call
                 * keep this call silent in background
                 * dont show the loader at any cost
                 * also dont show any error message if this call fails to consume
                 * simply a silent call in the background
                 * juni1289*/
                if (UserDataManager.getCustomerData() != null) {
                    if (hasValue(UserDataManager.getCustomerData()!!.msisdn)) {
                        if (hasValue(UserDataManager.getCustomerData()!!.offeringName)) {
                            mViewModel.requestMenusInBackground(
                                UserDataManager.getCustomerData()!!.msisdn!!,
                                UserDataManager.getCustomerData()!!.offeringName!!
                            )
                        }
                    }
                }
            } else {
                logE("menus", "menusCache:::No2", fromClass, "requestAppMenus")
                //call the app menu API
                //request get app menus
                if (UserDataManager.getCustomerData() != null) {
                    if (hasValue(UserDataManager.getCustomerData()!!.msisdn)) {
                        if (hasValue(UserDataManager.getCustomerData()!!.offeringName)) {
                            mViewModel.showLoader()
                            mViewModel.requestMenus(
                                UserDataManager.getCustomerData()!!.msisdn!!,
                                UserDataManager.getCustomerData()!!.offeringName!!
                            )
                        } else {
                            addLogOutMenuInSideDrawer()
                        }
                    } else {
                        addLogOutMenuInSideDrawer()
                    }
                } else {
                    addLogOutMenuInSideDrawer()
                }
            }
        } else {
            logE("menus", "menusCache:::No1", fromClass, "requestAppMenus")
            //call the app menu API
            //request get app menus
            if (UserDataManager.getCustomerData() != null) {
                if (hasValue(UserDataManager.getCustomerData()!!.msisdn)) {
                    if (hasValue(UserDataManager.getCustomerData()!!.offeringName)) {
                        mViewModel.showLoader()
                        mViewModel.requestMenus(
                            UserDataManager.getCustomerData()!!.msisdn!!,
                            UserDataManager.getCustomerData()!!.offeringName!!
                        )
                    } else {
                        addLogOutMenuInSideDrawer()
                    }
                } else {
                    addLogOutMenuInSideDrawer()
                }
            } else {
                addLogOutMenuInSideDrawer()
            }
        }
    }//requestAppMenus ends

    /**all the API observers should
     * be aligned below here
     * juni1289*/
    private fun subscribeToFCMObserver() {
        val fcmResponseLiveData = object : Observer<FCMResponse> {
            override fun onChanged(response: FCMResponse?) {
                if (!this@MainActivity.isFinishing) {
                    if (response != null) {
                        if (response.data != null) {
                            setFCMDataToLocalPreferences(response)
                        }
                    }
                }
            }
        }

        mViewModel.fcmResponseLiveData.observe(this@MainActivity, fcmResponseLiveData)
    }

    private fun subscribeToNotificationsCountLiveDataResponse() {
        val notificationsUnreadCountResponseLiveData = object : Observer<Int> {
            override fun onChanged(t: Int?) {
                if (!this@MainActivity.isFinishing) {
                    if (t != null) {
                        if (t > 0) {
                            var count = ""
                            if (t > 99) {
                                count = "99+"
                            } else {
                                count = t.toString()
                            }
                            logE(
                                "countX",
                                "count:::".plus(count),
                                fromClass,
                                "subscribeToNotificationsCountLiveDataResponse"
                            )
                            notificationCountText.text = count
                            notificationCountText.visibility = View.VISIBLE
                        }
                    }
                }
            }
        }

        mViewModel.notificationsUnreadCountResponseLiveData.observe(
            this@MainActivity,
            notificationsUnreadCountResponseLiveData
        )
    }

    private fun subscribeToMenusObserver() {
        val menuResponseData = object : Observer<MenuResponse> {
            override fun onChanged(menu: MenuResponse?) {
                if (!this@MainActivity.isFinishing) {
                    mViewModel.hideLoader()
                    if (menu != null && menu.data != null && hasValue(menu.data.toString())) {
                        val apisCount = mViewModel.apisCalledCount.getValue()!!
                        mViewModel.apisCalledCount.value = apisCount + 1
                        logE(
                            "menusResponseSuccess",
                            "response:::".plus(menu.toString()),
                            fromClass,
                            "subscribeToMenusObserver"
                        )

                        val menuHorizontalList = getHorizontalMenuLocaleBased(menu)
                        val menuVerticalList = getVerticalMenuLocaleBased(menu)

                        if (menuVerticalList != null && menuHorizontalList != null) {
                            processMenus(menuVerticalList, menuHorizontalList)//process the menus
                        } else {
                            addLogOutMenuInSideDrawer()
                        }

                        //save the menu response in the shared preferences
                        setMenusDataToLocalPreferences(menu)

                    } else {
                        addLogOutMenuInSideDrawer()
                    }
                }
            }
        }

        mViewModel.menuResponseData.observe(this@MainActivity, menuResponseData)

        val menuErrorResponseData = object : Observer<AzerAPIErrorHelperResponse> {
            override fun onChanged(response: AzerAPIErrorHelperResponse?) {
                if (!this@MainActivity.isFinishing) {
                    mViewModel.hideLoader()
                    addLogOutMenuInSideDrawer()
                    //check internet
                    if (!isNetworkAvailable(this@MainActivity)) {
                        showMessageDialog(
                            applicationContext, this@MainActivity,
                            resources.getString(R.string.popup_error_title),
                            resources.getString(R.string.message_no_internet)
                        )
                    } else {
                        /**check the error code
                         * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                         * else show a general api failure popup*/
                        if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                            showMessageDialog(
                                applicationContext, this@MainActivity,
                                resources.getString(R.string.popup_error_title),
                                response.errorDetail.toString()
                            )
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                            //force logout the user here
                            logOut(
                                applicationContext,
                                this@MainActivity,
                                false,
                                fromClass,
                                "subscribeToLogOutObserver4672edasdh"
                            )
                            return
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                            //handle the ui in this case
                            showMessageDialog(
                                applicationContext, this@MainActivity,
                                resources.getString(R.string.popup_error_title),
                                resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                            showMessageDialog(
                                applicationContext, this@MainActivity,
                                resources.getString(R.string.popup_error_title),
                                resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                        } else {
                            showMessageDialog(
                                applicationContext, this@MainActivity,
                                resources.getString(R.string.popup_error_title),
                                resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                        }
                    }
                }// if (activity != null && !this@MainActivity.isFinishing)  ends
            }
        }

        mViewModel.menuErrorResponseData.observe(this@MainActivity, menuErrorResponseData)

        val menuSilentResponseData = object : Observer<MenuResponse> {
            override fun onChanged(menu: MenuResponse?) {
                if (!this@MainActivity.isFinishing) {
                    if (menu != null) {
                        if (menu.data != null) {
                            if (hasValue(menu.data.toString())) {
                                val apisCount = mViewModel.apisCalledCount.getValue()!!
                                mViewModel.apisCalledCount.value = apisCount + 1
                                val menuHorizontalList = getHorizontalMenuLocaleBased(menu)
                                val menuVerticalList = getVerticalMenuLocaleBased(menu)

                                if (menuVerticalList != null && menuHorizontalList != null) {
                                    processMenus(
                                        menuVerticalList,
                                        menuHorizontalList
                                    )//process the menus
                                }


                                //save the menu response in the shared preferences
                                setMenusDataToLocalPreferences(menu)
                            }
                        }
                    }
                }
            }
        }

        mViewModel.menuSilentResponseData.observe(this@MainActivity, menuSilentResponseData)
    }//subscribeToMenusObserver ends

    private fun addLogOutMenuInSideDrawer() {
        //create the view for menu items
        //remove all existing menu views
        verticalMenuLayout.removeAllViews()

        val viewItemVerticalMenus = ViewItemVerticalMenus(applicationContext)

        viewItemVerticalMenus.getVerticalMenuItemTitle().text =
            resources.getString(R.string.logout_default_case_title)
        viewItemVerticalMenus.getVerticalMenuItemTitle().isSelected = true
        logE(
            "menuXName",
            "name:::".plus(resources.getString(R.string.logout_default_case_title)),
            fromClass,
            "addLogOutMenuInSideDrawer"
        )
        //set the font
        viewItemVerticalMenus.getVerticalMenuItemTitle().typeface = getALSBoldFont()
        viewItemVerticalMenus.getVerticalMenuItemTitle()
            .setTextColor(resources.getColor(R.color.purplish_brown))
        viewItemVerticalMenus.getVerticalMenuKey().text =
            ConstantsUtility.VerticalMenuIdentifiers.DEFAULT_LOG_OUT_KEY

        viewItemVerticalMenus.verticalMenuIcon().setImageResource(R.drawable.sidemenulogout)

        //add the view
        verticalMenuLayout.addView(viewItemVerticalMenus)
    }//addLogOutMenuInSideDrawer ends

    private fun logOutUserFromMain(causeKey: String) {
        //remove the shared preferences
        ProjectApplication.getInstance().getLocalPreferneces().deleteAllPreferences()
        //    resetWrongPasswordAttempts(context)

        //reset the user data manager
        UserDataManager.resetUserDataManager()

        //start from the new process
        //show the user the login activity
        LoginActivity.start(applicationContext)
        //finish the current activity
        finish()
    }//logOutUserFromMain ends

    private fun subscribeToLogOutObserver() {
        val logOutResponseLiveData = object : Observer<LogOutResponse> {
            override fun onChanged(t: LogOutResponse?) {
                if (!this@MainActivity.isFinishing) {
                    logOutUserFromMain("mainlogoutasd2344")
                    return
                }
            }
        }

        mViewModel.logOutResponseLiveData.observe(this@MainActivity, logOutResponseLiveData)

        val logOutResponseErrorLiveData = object : Observer<AzerAPIErrorHelperResponse> {
            override fun onChanged(response: AzerAPIErrorHelperResponse?) {
                if (!this@MainActivity.isFinishing) {
                    //check internet
                    if (!isNetworkAvailable(this@MainActivity)) {
                        showLogOutMessageDialogOnNoInternetConnection(
                            applicationContext, this@MainActivity,
                            resources.getString(R.string.popup_error_title),
                            resources.getString(R.string.message_no_internet)
                        )
                    } else {
                        /**check the error code
                         * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                         * else show a general api failure popup*/
                        if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                            showMessageDialog(
                                applicationContext, this@MainActivity,
                                resources.getString(R.string.popup_error_title),
                                response.errorDetail.toString()
                            )
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                            //force logout the user here
                            logOut(
                                applicationContext,
                                this@MainActivity,
                                false,
                                fromClass,
                                "subscribeToLogOutObserver4672edasdh"
                            )
                            return
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                            //handle the ui in this case
                            showMessageDialog(
                                applicationContext, this@MainActivity,
                                resources.getString(R.string.popup_error_title),
                                resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                            showMessageDialog(
                                applicationContext, this@MainActivity,
                                resources.getString(R.string.popup_error_title),
                                resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                        } else {
                            showMessageDialog(
                                applicationContext, this@MainActivity,
                                resources.getString(R.string.popup_error_title),
                                resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                        }
                    }
                }// if (activity != null && !this@MainActivity.isFinishing)  ends
            }
        }

        mViewModel.logOutResponseErrorLiveData.observe(
            this@MainActivity,
            logOutResponseErrorLiveData
        )
    }//subscribeToLogOutObserver ends

    @SuppressLint("InflateParams")
    private fun showLogOutMessageDialogOnNoInternetConnection(
        context: Context,
        activity: Activity,
        title: String,
        message: String
    ) {
        try {
            if (activity.isFinishing) return

            val dialogBuilder = AlertDialog.Builder(activity)
            val inflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val dialogView = inflater.inflate(R.layout.azer_popup_layout_new, null)
            dialogBuilder.setView(dialogView)

            val alertDialog = dialogBuilder.create()
            alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            if (alertDialog != null) {
                if (alertDialog.isShowing) {
                    return
                }
            }
            alertDialog.show()

            val popupTitle = dialogView.findViewById(R.id.popupTitle) as TextView
            popupTitle.typeface = getALSNormalFont()
            val okButton = dialogView.findViewById(R.id.okButton) as Button

            okButton.text = getOkButtonLabel()

            popupTitle.text = message
            okButton.setOnClickListener {
                alertDialog.dismiss()
                logOutUserFromMain("logoutfromnointernet")
            }
        } catch (e: Exception) {
            logE("AzerPopup", e.toString(), "AzerFonPopups", "showMessageDialog")
        }//catch ends
    }//show message dialog ends

    private fun subscribeToAppResumeObserver() {
        val appResumeResponseData = object : Observer<LoginAppResumeResponse> {
            override fun onChanged(appResumeResponse: LoginAppResumeResponse?) {
                if (!this@MainActivity.isFinishing) {
                    if (appResumeResponse != null && appResumeResponse.data != null && appResumeResponse.data.customerData != null) {
                        //save the response in shared preferences
                        setLoginAppResumeDataToLocalPreferences(appResumeResponse)
                        //save the response in shared preferences
                        setCustomerDataToLocalPreferences(appResumeResponse.data.customerData)
                        //update the user data manager
                        UserDataManager.saveCustomerData(appResumeResponse.data.customerData)

                        //set the terms and conditions
                        if (appResumeResponse.data.predefinedData != null) {
                            UserDataManager.savePredefineData(appResumeResponse.data.predefinedData)
                            if (appResumeResponse.data.predefinedData!!.tnc != null) {
                                if (hasValue(appResumeResponse.data.predefinedData!!.tnc!!.content)) {
                                    UserDataManager.setTermsAndConditionsDataString(
                                        appResumeResponse.data.predefinedData!!.tnc!!.content
                                    )
                                }
                            }
                        }
                        requestAppMenus()
                        logE(
                            "msisdnMain",
                            "msisdn:::".plus(UserDataManager.getCustomerData()!!.msisdn),
                            fromClass,
                            "subscribeToAppResumeObserver"
                        )
                        UserDataManager.setIsAppResumeCalledWithSuccess(true)
                        callGetHomePageAPI()
                    } else {
                        processAppResumeOffline("lagoutfromappresumesubscriber1234")
                    }
                }
            }
        }

        mViewModel.appResumeResponseData.observe(this@MainActivity, appResumeResponseData)

        val appResumeErrorResponseData = object : Observer<AzerAPIErrorHelperResponse> {
            override fun onChanged(response: AzerAPIErrorHelperResponse?) {
                if (!this@MainActivity.isFinishing) {
                    if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                        logOut(
                            applicationContext,
                            this@MainActivity,
                            false,
                            fromClass,
                            "appResumeErrorResponseData1234dfaa"
                        )
                    } else {
                        processAppResumeOffline("lagoutfromappresumeERRORsubscriber2345")
                    }
                }// if (activity != null && !this@MainActivity.isFinishing)  ends
            }
        }

        mViewModel.appResumeErrorResponseData.observe(this@MainActivity, appResumeErrorResponseData)
    }//subscribeToAppResumeObserver ends

    private fun callGetHomePageAPI() {
        //get the homepage response from the cache
        val homePageCachedResponse = getHomePageResponseFromLocalPreferences("cvaaaa2333")
        logE(
            "homePageCachedResponse",
            "cache:::".plus(homePageCachedResponse.data),
            fromClass,
            "callGetHomePageAPI"
        )
        if (homePageCachedResponse.data != null && hasValue(homePageCachedResponse.data.toString())) {
            logE("homePageCachedResponse", "cache hai", fromClass, "callGetHomePageAPI")
            loadDashboardScreen()
            Handler().postDelayed({
                if (RootValues.getHomePageResponseEvents() != null) {
                    RootValues.getHomePageResponseEvents()!!
                        .onHomePageResponse(homePageCachedResponse)
                }
            }, 50L)
            mViewModel.requestGetHomePageSilently(false)
        } else {
            logE("homePageCachedResponse", "cache nai hai", fromClass, "callGetHomePageAPI")
            if (!UserDataManager.isGetHomePageCalledWithSuccess()) {
                logE(
                    "homePageCachedResponse",
                    "not called with success",
                    fromClass,
                    "callGetHomePageAPI"
                )
                mViewModel.requestGetHomePage(false)
            }
        }
    }//callGetHomePageAPI ends

    private fun processAppResumeOffline(logOutLogKey: String) {
        //get the app resume data from the local preferences
        val cachedAppResumeData = getLoginAppResumeDataFromLocalPreferences()
        logE(
            "cachedAppResume",
            "cache:::".plus(cachedAppResumeData.toString()),
            fromClass,
            "processAppResumeOffline"
        )
        if (cachedAppResumeData.data != null &&
            hasValue(cachedAppResumeData.data.toString())
        ) {
            logE("cachedAppResume", "have cache", fromClass, "processAppResumeOffline")
            UserDataManager.savePredefineData(cachedAppResumeData.data.predefinedData)
            //set the terms and conditions
            if (cachedAppResumeData.data.predefinedData != null) {
                if (cachedAppResumeData.data.predefinedData!!.tnc != null) {
                    if (hasValue(cachedAppResumeData.data.predefinedData!!.tnc!!.content)) {
                        UserDataManager.setTermsAndConditionsDataString(cachedAppResumeData.data.predefinedData!!.tnc!!.content)
                    }
                }
            }

            requestAppMenus()
            UserDataManager.setIsAppResumeCalledWithSuccess(false)
            callGetHomePageAPI()
        } else {
            logE("cachedAppResume", "no cache data", fromClass, "processAppResumeOffline")
            logOut(applicationContext, this@MainActivity, false, fromClass, logOutLogKey)
        }

        if (!this@MainActivity.isFinishing) {
            if (!isNetworkAvailable(applicationContext)) {
                showMessageDialog(
                    applicationContext,
                    this@MainActivity,
                    "",
                    resources.getString(R.string.message_no_internet)
                )
            }
        }
    }

    /**these are the
     * methods to read the menus response for creating the UI
     * they need to be commented out late on*/
    private fun processMenus(
        menuVerticalList: List<MenuVerticalItem?>,
        menuHorizontalList: List<MenuHorizontalItem?>
    ) {
        logE("X89M", "menusCache:::".plus(menuHorizontalList), fromClass, "processMenus")
        if (menuVerticalList.isNotEmpty() && menuHorizontalList.isNotEmpty()) {
            setVerticalMenuForFirstTime(getVerticalMenuModel(menuVerticalList))
            setHorizontalMenuForFirstTime(
                getHorizontalMenuModel(menuHorizontalList),
                menuHorizontalList
            )
        } else {
            addLogOutMenuInSideDrawer()
        }
    }//processMenus ends

    private fun fixBottomNavigationText(bottomNavigationView: BottomNavigationView) {
        bottomNavigationView.itemIconTintList = null //set the itemicon tint color on selection
        for (i in 0 until bottomNavigationView.childCount) {
            val item = bottomNavigationView.getChildAt(i)

            if (item is BottomNavigationMenuView) {
                val menu = item

                for (j in 0 until menu.childCount) {
                    val menuItem = menu.getChildAt(j)

                    val small: View =
                        menuItem.findViewById(com.google.android.material.R.id.smallLabel)
                    if (small is TextView) {
                        small.ellipsize = TextUtils.TruncateAt.MARQUEE
                        small.isSelected = true
                        small.marqueeRepeatLimit = -1
                        small.setSingleLine(true)
                        small.typeface = getALSNormalFont()
                        small.setTextColor(Color.WHITE)
                    }
                    val large: View =
                        menuItem.findViewById(com.google.android.material.R.id.largeLabel)
                    if (large is TextView) {
                        large.ellipsize = TextUtils.TruncateAt.MARQUEE
                        large.isSelected = true
                        large.marqueeRepeatLimit = -1
                        large.setSingleLine(true)
                        large.typeface = getALSNormalFont()
                        large.setTextColor(Color.WHITE)
                    }
                }
            }
        }
    }//fixBottomNavigationText ends

    /**method for setting up the horizontal
     * menu/bottom navigation view for the first time
     * we are just setting the font and colors
     * and adding the menus to the bottom navigation view
     * for the very first time only*/
    private fun getHorizontalMenuModel(menuList: List<MenuHorizontalItem?>): ArrayList<HorizontalMenuHelperModel> {
        val menus = ArrayList<HorizontalMenuHelperModel>()

        for (its in 0 until menuList.size) {
            if (hasValue(menuList[its]!!.identifier) &&
                hasValue(menuList[its]!!.title) &&
                hasValue(menuList[its]!!.sortOrder)
            ) {
                menus.add(
                    HorizontalMenuHelperModel(
                        menuList[its]!!.identifier,
                        menuList[its]!!.title,
                        menuList[its]!!.sortOrder!!
                    )
                )
            }//if has menu data
        }//for ends

        //sort the menus
        Collections.sort(menus, object : Comparator<HorizontalMenuHelperModel> {
            override fun compare(
                o1: HorizontalMenuHelperModel,
                o2: HorizontalMenuHelperModel
            ): Int {
                return o1.sortOrder!!.compareTo(o2.sortOrder!!)
            }
        })

        return menus
    }//getHorizontalMenuModel ends

    private fun getVerticalMenuModel(menuList: List<MenuVerticalItem?>): ArrayList<VerticalMenuHelperModel> {
        val menus = ArrayList<VerticalMenuHelperModel>()

        for (its in 0 until menuList.size) {
            if (hasValue(menuList[its]!!.identifier) &&
                hasValue(menuList[its]!!.title) &&
                hasValue(menuList[its]!!.sortOrder)
            ) {
                menus.add(
                    VerticalMenuHelperModel(
                        menuList[its]!!.identifier,
                        menuList[its]!!.title,
                        menuList[its]!!.sortOrder!!
                    )
                )
            }//if has menu data
        }//for ends

        //sort the menus
        Collections.sort(menus, object : Comparator<VerticalMenuHelperModel> {
            override fun compare(o1: VerticalMenuHelperModel, o2: VerticalMenuHelperModel): Int {
                return o1.sortOrder?.toIntOrNull() ?: 0.compareTo(o2.sortOrder?.toIntOrNull() ?: 0)
            }
        })

        return menus
    }//getVerticalMenuModel ends

    private fun setVerticalMenuForFirstTime(menusHelperModel: ArrayList<VerticalMenuHelperModel>) {
        //create the view for menu items
        //remove all existing menu views
        verticalMenuLayout.removeAllViews()
        for (item in 0 until menusHelperModel.size) {
            val viewItemVerticalMenus = ViewItemVerticalMenus(applicationContext)

            //set the title
            viewItemVerticalMenus.getVerticalMenuItemTitle().text = menusHelperModel[item].title
            viewItemVerticalMenus.getVerticalMenuItemTitle().isSelected = true
            logE(
                "menuXName",
                "name:::".plus(menusHelperModel[item].title),
                fromClass,
                "setVerticalMenuForFirstTime"
            )
            //set the font
            if (getALSBoldFont() != null) {
                viewItemVerticalMenus.getVerticalMenuItemTitle().typeface = getALSBoldFont()
            } else {
                val tf = Typeface.createFromAsset(assets, "fonts/af/als_schlange_sans_bold.otf")
                viewItemVerticalMenus.getVerticalMenuItemTitle().typeface = tf
            }

            viewItemVerticalMenus.getVerticalMenuItemTitle()
                .setTextColor(resources.getColor(R.color.purplish_brown))
            viewItemVerticalMenus.getVerticalMenuKey().text = menusHelperModel[item].identifier

            //set the image
            if (menusHelperModel[item].identifier.equals(
                    ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_NOTIFICATIONS,
                    ignoreCase = true
                )
            ) {
                viewItemVerticalMenus.verticalMenuIcon()
                    .setImageResource(R.drawable.sidemenunotifications)
            }

            if (menusHelperModel[item].identifier.equals(
                    ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_STORE_LOCATOR,
                    ignoreCase = true
                )
            ) {
                viewItemVerticalMenus.verticalMenuIcon()
                    .setImageResource(R.drawable.sidemenustorelocator)
            }

            if (menusHelperModel[item].identifier.equals(
                    ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_FAQ,
                    ignoreCase = true
                )
            ) {
                viewItemVerticalMenus.verticalMenuIcon().setImageResource(R.drawable.sidemenufaq)
            }

            if (menusHelperModel[item].identifier.equals(
                    ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_CONTACT_US,
                    ignoreCase = true
                )
            ) {
                viewItemVerticalMenus.verticalMenuIcon()
                    .setImageResource(R.drawable.sidemenucontactus)
            }

            if (menusHelperModel[item].identifier.equals(
                    ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_TERMS_AND_CONDITIONS,
                    ignoreCase = true
                )
            ) {
                viewItemVerticalMenus.verticalMenuIcon().setImageResource(R.drawable.sidemenutandcs)
            }

            if (menusHelperModel[item].identifier.equals(
                    ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_LIVE_CHAT,
                    ignoreCase = true
                )
            ) {
                viewItemVerticalMenus.verticalMenuIcon()
                    .setImageResource(R.drawable.sidemenulivechat)
            }

            if (menusHelperModel[item].identifier.equals(
                    ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_SETTINGS,
                    ignoreCase = true
                )
            ) {
                viewItemVerticalMenus.verticalMenuIcon()
                    .setImageResource(R.drawable.sidemenusettings)
            }

            if (menusHelperModel[item].identifier.equals(
                    ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_LOG_OUT,
                    ignoreCase = true
                )
            ) {
                viewItemVerticalMenus.verticalMenuIcon().setImageResource(R.drawable.sidemenulogout)
            }

            if (menusHelperModel[item].identifier.equals(
                    ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_TUTORIAL_AND_FAQ,
                    ignoreCase = true
                )
            ) {
                viewItemVerticalMenus.verticalMenuIcon()
                    .setImageResource(R.drawable.sidemenututorial)
            }

            //add the view
            verticalMenuLayout.addView(viewItemVerticalMenus)

        }//for ends
    }//setVerticalMenuForFirstTime ends

    private fun setHorizontalMenuForFirstTime(
        menusHelperModel: ArrayList<HorizontalMenuHelperModel>,
        horizontalMenu: List<MenuHorizontalItem?>
    ) {
        navigationView.menu.clear()//clear all the menus before setting any menu items into it
        for (its in 0 until menusHelperModel.size) {
            logE(
                "menuH",
                "menu:::".plus(menusHelperModel[its].identifier).plus(" sort:::")
                    .plus(menusHelperModel[its].title),
                fromClass,
                "setHorizontalMenuForFirstTime"
            )

            if (menusHelperModel[its].identifier!!.equals(
                    ConstantsUtility.HorizontalMenuIdentifiers.IDENTIFIER_DASHBOARD,
                    ignoreCase = true
                )
            ) {
                navigationView.menu.add(
                    Menu.NONE,
                    ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD,
                    menusHelperModel[its].sortOrder!!.toInt(),
                    menusHelperModel[its].title
                )
                    .setIcon(getDashboardUnSelectedIcon())
            }//if (menusHelperModel[its].identifier!!.equals("dashboard", ignoreCase = true)) ends

            if (menusHelperModel[its].identifier!!.equals(
                    ConstantsUtility.HorizontalMenuIdentifiers.IDENTIFIER_TARIFFS,
                    ignoreCase = true
                )
            ) {
                navigationView.menu.add(
                    Menu.NONE,
                    ConstantsUtility.HorizontalMenuIds.ID_TARIFFS,
                    menusHelperModel[its].sortOrder!!.toInt(),
                    menusHelperModel[its].title
                )
                    .setIcon(getTariffsUnSelectedIcon())
            }//if (menusHelperModel[its].identifier!!.equals("tariffs", ignoreCase = true)) ends

            if (menusHelperModel[its].identifier!!.equals(
                    ConstantsUtility.HorizontalMenuIdentifiers.IDENTIFIER_SERVICES,
                    ignoreCase = true
                )
            ) {
                navigationView.menu.add(
                    Menu.NONE,
                    ConstantsUtility.HorizontalMenuIds.ID_SERVICES,
                    menusHelperModel[its].sortOrder!!.toInt(),
                    menusHelperModel[its].title
                )
                    .setIcon(getServicesUnSelectedIcon())

                if (horizontalMenu[its] != null && !horizontalMenu[its]!!.items.isNullOrEmpty()) {
                    //set the child menus for services
                    processServicesChildMenuItems(its, horizontalMenu)
                } else {
                    HorizontalMenuItems.servicesMenuModel = ArrayList()
                    HorizontalMenuItems.servicesMenuModel!!.clear()
                }

            }//if (menusHelperModel[its].identifier!!.equals("services", ignoreCase = true)) ends

            if (menusHelperModel[its].identifier!!.equals(
                    ConstantsUtility.HorizontalMenuIdentifiers.IDENTIFIER_INTERNET,
                    ignoreCase = true
                )
            ) {
                navigationView.menu.add(
                    Menu.NONE,
                    ConstantsUtility.HorizontalMenuIds.ID_INTERNET,
                    menusHelperModel[its].sortOrder!!.toInt(),
                    menusHelperModel[its].title
                )
                    .setIcon(getInternetUnSelectedIcon())

                if (horizontalMenu[its] != null && !horizontalMenu[its]!!.items.isNullOrEmpty()) {
                    //set the child menus for topup
                    processTopupChildMenuItems(its, horizontalMenu)
                }

            } else if (menusHelperModel[its].identifier!!.equals(
                    ConstantsUtility.HorizontalMenuIdentifiers.IDENTIFIER_TOPUP,
                    ignoreCase = true
                )
            ) {
                navigationView.menu.add(
                    Menu.NONE,
                    ConstantsUtility.HorizontalMenuIds.ID_TOPUP,
                    menusHelperModel[its].sortOrder!!.toInt(),
                    menusHelperModel[its].title
                )
                    .setIcon(getTopUpUnSelectedIcon())

                if (horizontalMenu[its] != null && !horizontalMenu[its]!!.items.isNullOrEmpty()) {
                    //set the child menus for topup
                    processTopupChildMenuItems(its, horizontalMenu)
                } else {
                    HorizontalMenuItems.topupMenuModel = ArrayList()
                    HorizontalMenuItems.topupMenuModel!!.clear()
                }

            }//if (menusHelperModel[its].identifier!!.equals("topup", ignoreCase = true)) ends


            if (menusHelperModel[its].identifier!!.equals(
                    ConstantsUtility.HorizontalMenuIdentifiers.IDENTIFIER_MYACCOUNT,
                    ignoreCase = true
                )
            ) {
                navigationView.menu.add(
                    Menu.NONE,
                    ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT,
                    menusHelperModel[its].sortOrder!!.toInt(),
                    menusHelperModel[its].title
                )
                    .setIcon(getMyAccountUnSelectedIcon())

                if (horizontalMenu[its] != null && !horizontalMenu[its]!!.items.isNullOrEmpty()) {
                    //set the child menus for myaccount
                    processMyAccountChildMenuItems(its, horizontalMenu)
                } else {
                    HorizontalMenuItems.myaccountMenuModel = ArrayList()
                    HorizontalMenuItems.myaccountMenuModel!!.clear()
                }

            }//if (menusHelperModel[its].identifier!!.equals("myaccount", ignoreCase = true)) ends
        }//for ends

        fixBottomNavigationText(navigationView)
        selectDashBoardMenu()
        if (totalCountOfApis != -1 && totalCountOfApis == 1) {
            if (appResumeCallingData.equals("fromFCMPush", ignoreCase = true)) {
                NotificationActivity.start(applicationContext, "fromFCMPush")
                finish()
            } else if (appResumeCallingData.equals("X1289Y", ignoreCase = true)) {
                redirectToTariffs(UserDataManager.getActionIdFromServer())
            } else {
                logE("onapisCallCount", "else case:::", fromClass, "onChanged")
                dynamicLinksNavigation()
            }
        }
    }//setHorizontalMenuForFirstTime ends

    private fun registerVerticalMenuClickListener() {
        verticalMenuClickListener = object : VerticalMenuClickEvents {
            override fun onVerticalMenuClicked(verticalMenuKey: String) {

                if (verticalMenuKey.equals(
                        ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_NOTIFICATIONS,
                        ignoreCase = true
                    )
                ) {
                    if (isManageAccountsLayoutHidden()) {
                        hamBurgerMenuClose()
                        NotificationActivity.start(this@MainActivity)
                    }
                }

                if (verticalMenuKey.equals(
                        ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_STORE_LOCATOR,
                        ignoreCase = true
                    )
                ) {
                    //load maps
                    if (isManageAccountsLayoutHidden()) {
                        hamBurgerMenuClose()
                        StoreLocatorActivity.start(this@MainActivity)
                    }
                }

                if (verticalMenuKey.equals(
                        ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_FAQ,
                        ignoreCase = true
                    )
                ) {
                    //load frequently asked questions
                    if (isManageAccountsLayoutHidden()) {
                        hamBurgerMenuClose()
                        FaqActivity.start(this@MainActivity)
                    }
                }

                if (verticalMenuKey.equals(
                        ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_TUTORIAL_AND_FAQ,
                        ignoreCase = true
                    )
                ) {
                    if (isManageAccountsLayoutHidden()) {
                        hamBurgerMenuClose()
                        TutorialsActivity.start(applicationContext)
                    }
                }

                if (verticalMenuKey.equals(
                        ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_CONTACT_US,
                        ignoreCase = true
                    )
                ) {
                    //load contact us fragment
                    if (isManageAccountsLayoutHidden()) {
                        hamBurgerMenuClose()
                        ContactUsActivity.start(this@MainActivity)
                    }
                }

                if (verticalMenuKey.equals(
                        ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_TERMS_AND_CONDITIONS,
                        ignoreCase = true
                    )
                ) {
                    //load terms and conditions
                    if (isManageAccountsLayoutHidden()) {
                        hamBurgerMenuClose()
                        /*  resetBottomMenuIcons()
                          Navigation.findNavController(this@MainActivity, R.id.fragmentContainer).navigate(R.id.terms_conditions_dest)*/

                        TermsAndConditionsActivity.start(this@MainActivity)
                    }
                }

                if (verticalMenuKey.equals(
                        ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_LIVE_CHAT,
                        ignoreCase = true
                    )
                ) {
                    if (isManageAccountsLayoutHidden()) {
                        hamBurgerMenuClose()
                        //setup the webview
                        setupNewWebView()
//                        LiveChatActivity.start(this@MainActivity)
                    }
                }

                if (verticalMenuKey.equals(
                        ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_SETTINGS,
                        ignoreCase = true
                    )
                ) {
                    //load settings
                    if (isManageAccountsLayoutHidden()) {
                        hamBurgerMenuClose()
                        val settingsActivityIntent =
                            Intent(applicationContext, SettingsActivity::class.java)
                        startActivity(settingsActivityIntent)
                    }
                }

                if (verticalMenuKey.equals(
                        ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_LOG_OUT,
                        ignoreCase = true
                    )
                    || verticalMenuKey.equals(
                        ConstantsUtility.VerticalMenuIdentifiers.DEFAULT_LOG_OUT_KEY,
                        ignoreCase = true
                    )
                ) {
                    //logout
                    if (isManageAccountsLayoutHidden()) {
                        hamBurgerMenuClose()

                        /**count the number of users to be logged in
                         * if there are multiple users logged in; show the different popup
                         * other than that of the of the single user logout case popup*/
                        var list: ArrayList<CustomerData>? = ArrayList()
                        if (getUserAccountsDataFromLocalPreferences().usersList != null) {
                            list =
                                getUserAccountsDataFromLocalPreferences().usersList as ArrayList<CustomerData>
                        } else {
                            //single user logged in
                            showLogoutPopupForSingleUser(getLocalizedLogoutForSingleUserLabel())
                        }//get the currently users logged in

                        if (list != null && list.size > 1) {
                            //multiple users logged in
                            showLogoutPopupForMultipleUsers()
                        } else {
                            //single user logged in
                            showLogoutPopupForSingleUser(getLocalizedLogoutForSingleUserLabel())
                        }

                    }
                }
            }//onVerticalMenuClicked ends
        }//verticalMenuClickListener=object:VerticalMenuClickEvents ends

        //save the listener for usage
        RootValues.setVerticalMenuClickListener(verticalMenuClickListener!!)
    }//registerVerticalMenuClickListener ends

    private fun getLocalizedLogoutForSingleUserLabel(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "Are you sure you want to sign out from account?"
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Hesabdan çıxmaq istədiyindən əminsən?"
            else -> "Ты уверен, что хочешь выйти из аккаунта?"
        }
    }

    private fun setupNewWebView() {
        try {
            if (this@MainActivity.isFinishing) {
                //safe catch for activity not running and do not show the dialog etc
                return
            }//if (this.isFinishing())

            //user have no active data connection show error
            if (!isNetworkAvailable(this@MainActivity)) {
                if (this@MainActivity.isFinishing) {
                    return
                }
                killWebViewProgressDialog()
                isURLLoadSuccess = false
                contentMainLayout.visibility = View.VISIBLE
                webViewContentLayout.visibility = View.GONE
                //show error dialog
                showMessageDialog(
                    applicationContext, this@MainActivity,
                    resources.getString(R.string.popup_error_title),
                    resources.getString(R.string.message_no_internet)
                )
            } else {
                if (this@MainActivity.isFinishing) {
                    return
                }

                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

                //if progress dialog already is showing kill it
                killWebViewProgressDialog()

                /*hide the main content*/
                contentMainLayout.visibility = View.GONE
                webViewContentLayout.visibility = View.VISIBLE

                //disable the drawer
                drawer_layout.setDrawerLockMode(androidx.drawerlayout.widget.DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
                //show the loading dialog
                showWebViewProgressPopup()

                //enable zoom view controls
                webView.resumeTimers()
                webView.settings.builtInZoomControls = true
                webView.settings.displayZoomControls = true

                //load the links inside the web view
                webView.settings.allowContentAccess = true
                webView.settings.allowFileAccess = true
                webView.settings.javaScriptCanOpenWindowsAutomatically = true

                //enable javascript
                webView.settings.javaScriptEnabled = true

                //enable the dom storage
                webView.settings.domStorageEnabled = true

                //enable sounds
                webView.settings.mediaPlaybackRequiresUserGesture = false

                //set cache settings
                webView.settings.cacheMode = WebSettings.LOAD_NO_CACHE

                //for images loading
                webView.settings.loadsImagesAutomatically = true

                //set the plugin state
                webView.settings.pluginState = WebSettings.PluginState.ON

                //load the url inside the webview
                var url = "https://secure-fra.livechatinc.com/licence/12830742/v2/open_chat.cgi"

                val json = JSONObject()
                if (UserDataManager.getPredefineData() != null &&
                    hasValue(UserDataManager.getPredefineData()!!.liveChat)
                ) {
                    val name = UserDataManager.getCustomerData()?.lastName
                    val email = UserDataManager.getCustomerData()?.email
                    val msisdn = UserDataManager.getCustomerData()?.msisdn
                    var group = 3
                    if (ProjectApplication.mLocaleManager.getLanguage()
                            .equals(ConstantsUtility.LanguageKeywords.AZ, ignoreCase = true)
                    ) {
                        group = 5
                    } else if (ProjectApplication.mLocaleManager.getLanguage()
                            .equals(ConstantsUtility.LanguageKeywords.RU, ignoreCase = true)
                    ) {
                        group = 4
                    } else {
                        group = 3
                    }

                    json.put("group", group)

                    url =
                        url + "?name=$name&email=$email&params=PhoneNumber%3D$msisdn%26channel%3Dnar&group=$group"
                }

                logE("liveXhat", "url:::".plus(url), fromClass, "setupWebView")
                //register the webview listener
                registerWebViewListener()
                initWebViewHeaderContent()
                setupChromeClientWithWebView()

                if (!isURLLoadSuccess) {
                    webView.resumeTimers()
                    webView.loadUrl(url)
                }// if (!isURLLoadSuccess) ends
                else {
                    killWebViewProgressDialog()
                    webView.resumeTimers()
                }

            }//else for internet check ends here
        } catch (exp: Exception) {
            if (this@MainActivity.isFinishing) {
                return
            }//if ends
            logE("webVX", "error:::".plus(exp.toString()), fromClass, "setupWebView")
            killWebViewProgressDialog()
            //show error dialog
            showMessageDialog(
                applicationContext, this@MainActivity,
                resources.getString(R.string.popup_error_title),
                resources.getString(R.string.server_stopped_responding_please_try_again)
            )
        }
        //catch ends
    }//setupNewWebView ends

    private fun setupOldWebView() {
        try {
            if (this@MainActivity.isFinishing) {
                //safe catch for activity not running and do not show the dialog etc
                return
            }//if (this.isFinishing())

            //user have no active data connection show error
            if (!isNetworkAvailable(this@MainActivity)) {
                if (this@MainActivity.isFinishing) {
                    return
                }
                killWebViewProgressDialog()
                isURLLoadSuccess = false
                contentMainLayout.visibility = View.VISIBLE
                webViewContentLayout.visibility = View.GONE
                //show error dialog
                showMessageDialog(
                    applicationContext, this@MainActivity,
                    resources.getString(R.string.popup_error_title),
                    resources.getString(R.string.message_no_internet)
                )
            } else {
                if (this@MainActivity.isFinishing) {
                    return
                }

                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

                //if progress dialog already is showing kill it
                killWebViewProgressDialog()

                /*hide the main content*/
                contentMainLayout.visibility = View.GONE
                webViewContentLayout.visibility = View.VISIBLE

                //disable the drawer
                drawer_layout.setDrawerLockMode(androidx.drawerlayout.widget.DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
                //show the loading dialog
                showWebViewProgressPopup()

                //enable zoom view controls
                webView.resumeTimers()
                webView.settings.builtInZoomControls = true
                webView.settings.displayZoomControls = true

                //load the links inside the web view
                webView.settings.allowContentAccess = true
                webView.settings.allowFileAccess = true
                webView.settings.javaScriptCanOpenWindowsAutomatically = true

                //enable javascript
                webView.settings.javaScriptEnabled = true

                //enable the dom storage
                webView.settings.domStorageEnabled = true

                //enable sounds
                webView.settings.mediaPlaybackRequiresUserGesture = false

                //set cache settings
                webView.settings.cacheMode = WebSettings.LOAD_NO_CACHE

                //for images loading
                webView.settings.loadsImagesAutomatically = true

                //set the plugin state
                webView.settings.pluginState = WebSettings.PluginState.ON

                //load the url inside the webview
                var url = ""

                if (UserDataManager.getPredefineData() != null &&
                    hasValue(UserDataManager.getPredefineData()!!.liveChat)
                ) {

                    url = UserDataManager.getPredefineData()!!.liveChat!!
                    logE("liveXhat", "url:::".plus(url), fromClass, "setupWebView")
                } else {
                    //show error dialog
                    showMessageDialog(
                        applicationContext, this@MainActivity,
                        resources.getString(R.string.popup_error_title),
                        resources.getString(R.string.server_stopped_responding_please_try_again)
                    )
                }

                val json = JSONObject()
                json.put("authenticate", "Ev@mp1iv3Ch@t")

                //register the webview listener
                registerWebViewListener()
                initWebViewHeaderContent()
                setupChromeClientWithWebView()

                if (!isURLLoadSuccess) {
                    webView.resumeTimers()
                    webView.postUrl(url, json.toString().toByteArray(charset("UTF-8")))
                }// if (!isURLLoadSuccess) ends
                else {
                    killWebViewProgressDialog()
                    webView.resumeTimers()
                }

            }//else for internet check ends here
        } catch (exp: Exception) {
            if (this@MainActivity.isFinishing) {
                return
            }//if ends
            logE("webVX", "error:::".plus(exp.toString()), fromClass, "setupWebView")
            killWebViewProgressDialog()
            //show error dialog
            showMessageDialog(
                applicationContext, this@MainActivity,
                resources.getString(R.string.popup_error_title),
                resources.getString(R.string.server_stopped_responding_please_try_again)
            )
        }
        //catch ends
    }//setupWebView ends

    private fun setupChromeClientWithWebView() {
        try {
            webView.webChromeClient = object : WebChromeClient() {
                // For 3.0+ Devices (Start)
                // onActivityResult attached before constructor
                protected fun openFileChooser(uploadMsg: ValueCallback<Uri>, acceptType: String) {
                    mUploadMessage = uploadMsg
                    val i = Intent(Intent.ACTION_GET_CONTENT)
                    i.addCategory(Intent.CATEGORY_OPENABLE)
                    i.type = "image/*"
                    startActivityForResult(
                        Intent.createChooser(i, "File Browser"),
                        FILECHOOSER_RESULTCODE
                    )
                }


                // For Lollipop 5.0+ Devices
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                override fun onShowFileChooser(
                    mWebView: WebView,
                    filePathCallback: ValueCallback<Array<Uri>>,
                    fileChooserParams: WebChromeClient.FileChooserParams
                ): Boolean {
                    if (uploadMessage != null) {
                        uploadMessage!!.onReceiveValue(null)
                        uploadMessage = null
                    }

                    uploadMessage = filePathCallback

                    val intent = fileChooserParams.createIntent()
                    try {
                        startActivityForResult(intent, REQUEST_SELECT_FILE)
                    } catch (e: ActivityNotFoundException) {
                        uploadMessage = null
                        Toast.makeText(
                            this@MainActivity,
                            "Cannot Open File Chooser",
                            Toast.LENGTH_LONG
                        ).show()
                        return false
                    }

                    return true
                }

                //For Android 4.1 only
                protected fun openFileChooser(
                    uploadMsg: ValueCallback<Uri>,
                    acceptType: String,
                    capture: String
                ) {
                    mUploadMessage = uploadMsg
                    val intent = Intent(Intent.ACTION_GET_CONTENT)
                    intent.addCategory(Intent.CATEGORY_OPENABLE)
                    intent.type = "image/*"
                    startActivityForResult(
                        Intent.createChooser(intent, "File Browser"),
                        FILECHOOSER_RESULTCODE
                    )
                }

                protected fun openFileChooser(uploadMsg: ValueCallback<Uri>) {
                    mUploadMessage = uploadMsg
                    val i = Intent(Intent.ACTION_GET_CONTENT)
                    i.addCategory(Intent.CATEGORY_OPENABLE)
                    i.type = "image/*"
                    startActivityForResult(
                        Intent.createChooser(i, "File Chooser"),
                        FILECHOOSER_RESULTCODE
                    )
                }
            }
        } catch (exp: Exception) {
            logE(
                "WebVX",
                "error:::".plus(exp.toString()),
                fromClass,
                "setupChromeClientWithWebView"
            )
        }
    }//setupChromeClientWithWebView ends

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //code for file handling for the webview
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (requestCode == REQUEST_SELECT_FILE) {
                if (uploadMessage == null) {
                    logE("fileX2", "upload:::Null", fromClass, "onActivityResult")
                    return
                }
                uploadMessage!!.onReceiveValue(
                    WebChromeClient.FileChooserParams.parseResult(
                        resultCode,
                        data
                    )
                )
                logE(
                    "fileX2",
                    "upload:::".plus(uploadMessage.toString()),
                    fromClass,
                    "onActivityResult"
                )
                uploadMessage = null
            }
        } else if (requestCode == FILECHOOSER_RESULTCODE) {
            if (null == mUploadMessage) {
                return
            }
            val result = if (intent == null || resultCode != RESULT_OK) null else intent.data
            mUploadMessage!!.onReceiveValue(result)
            mUploadMessage = null
        } else {
            Toast.makeText(this@MainActivity, "Failed to Upload File", Toast.LENGTH_LONG).show()
        }
    }

    private fun initWebViewHeaderContent() {
        webViewTitle.text = resources.getString(R.string.live_chat_screen_title)
        webViewHeaderImage.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))
        webViewHeaderImage.setOnClickListener {
            //check if it can go back
            //if it can go back, redirect it to back
            //else hide the webview container
            //if its not visible, check for other statements!
            if (webView.copyBackForwardList().currentIndex > 0) {
                webView.goBack()
            } else {
                webView.pauseTimers()
                webViewContentLayout.visibility = View.GONE
                contentMainLayout.visibility = View.VISIBLE
                drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
            }//if (binding.webView.copyBackForwardList().getCurrentIndex() > 0)
        }
    }//initWebViewHeaderContent ends

    private fun registerWebViewListener() {
        try {

            webView.webViewClient = object : WebViewClient() {

                override fun onPageFinished(view: WebView, url: String) {
                    if (!isNetworkAvailable(this@MainActivity)) {
                        //check if there is no active network connection
                        //if no active connection, hide the view and show the toast
                        showMessageDialog(
                            applicationContext, this@MainActivity,
                            resources.getString(R.string.popup_error_title),
                            resources.getString(R.string.live_chat_load_failed)
                        )
                        isURLLoadSuccess = false
                        webView.visibility = View.GONE
                        noWebData.visibility = View.VISIBLE
                    } else {
                        //there is an active connection show the web view and kill the progress dialog
                        webView.visibility = View.VISIBLE
                        noWebData.visibility = View.GONE
                        isURLLoadSuccess = true
                    }

                    view.evaluateJavascript(
                        "(function(){return window.document.body.outerHTML})();",
                        object : ValueCallback<String> {
                            override fun onReceiveValue(value: String?) {
                                if (hasValue(value)) {
                                    logE(
                                        "asdh2jk3h",
                                        "string".plus(value!!),
                                        fromClass,
                                        "onReceiveValue"
                                    )
                                    if (isHTMLContainsError(value)) {
                                        isURLLoadSuccess = false
                                        webView.visibility = View.GONE
                                        noWebData.visibility = View.VISIBLE
                                    }
                                } else {
                                    logE("asdh2jk3h", "nnn 000 string", fromClass, "onReceiveValue")
                                }

                            }
                        })
                    //hide the dialog
                    killWebViewProgressDialog()
                    // do your stuff here for url being loaded in the webview
                    logE(
                        "webVX",
                        "onPageFinished->webview:::",
                        fromClass,
                        "registerWebViewListener"
                    )
                }//onPageFinished ends

                override fun onReceivedError(
                    view: WebView,
                    errorCode: Int,
                    description: String,
                    failingUrl: String
                ) {
                    isURLLoadSuccess = false
                    //hide the dialog
                    if (this@MainActivity.isFinishing) {
                        return
                    }

                    //hide the webview and kill the progress dialog
                    webView.visibility = View.GONE
                    noWebData.visibility = View.VISIBLE
                    killWebViewProgressDialog()
                    //url load success is false because on received error called
                    isURLLoadSuccess = false
                    //Your code to do for handling the error
                    logE(
                        "webVX",
                        "onReceivedError2->webview:::$errorCode description:::$description",
                        fromClass,
                        "registerWebViewListener"
                    )
                    //show error dialog
                    showMessageDialog(
                        applicationContext, this@MainActivity,
                        resources.getString(R.string.popup_error_title),
                        resources.getString(R.string.server_stopped_responding_please_try_again)
                    )
                }//onReceivedError ends

                override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                    if (url.toLowerCase().contains("&file=")) {
                        //if url contains &file= it means that the url is pointing to some file
                        //open the url in the external browser and let the file to download itself to user device storage
                        val i1 = Intent(Intent.ACTION_VIEW, Uri.parse("about:blank"))
                        startActivity(i1)
                        val i2 = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                        startActivity(i2)
                    } else {
                        //the url has no file param
                        //so we have to load the url inside the webview
                        //show the progress dialog
                        showWebViewProgressPopup()

                        //load the respective url inside the web view
                        view.loadUrl(url)
                    }
                    return true
                }//shouldOverrideUrlLoading ends
            }
        } catch (exp: Exception) {
            if (this@MainActivity.isFinishing) {
                return
            }
            webView.visibility = View.GONE
            noWebData.visibility = View.VISIBLE
            isURLLoadSuccess = false
            logE("webVX", "error:::".plus(exp.toString()), fromClass, "registerWebViewListener")
            //hide the dialog
            killWebViewProgressDialog()
            //show error dialog
            showMessageDialog(
                applicationContext, this@MainActivity,
                resources.getString(R.string.popup_error_title),
                resources.getString(R.string.server_stopped_responding_please_try_again)
            )
        } //catch ends
    }//registerWebViewListener ends

    private fun isHTMLContainsError(value: String): Boolean {
        return when {
            value.contains("403 Forbidden", ignoreCase = true) -> true
            value.contains("400 Bad Request", ignoreCase = true) -> true
            value.contains("401 Unauthorized", ignoreCase = true) -> true
            else -> value.contains("402 Payment Required", ignoreCase = true)
        }
    }

    @SuppressLint("InflateParams")
    fun showWebViewProgressPopup() {

        if (this@MainActivity.isFinishing) return
        logE("popX", "called", fromClass, "showWebViewProgressPopup")
        val dialogBuilder = AlertDialog.Builder(this@MainActivity)
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val dialogView = inflater.inflate(R.layout.loader_layout, null)
        dialogBuilder.setView(dialogView)

        webViewProgressDialog = dialogBuilder.create()
        webViewProgressDialog!!.setCancelable(false)
        webViewProgressDialog!!.setCanceledOnTouchOutside(false)
        webViewProgressDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val loaderImage = dialogView.findViewById(R.id.loaderImage) as ImageView

        Glide
            .with(this@MainActivity)
            .asGif()
            .load(R.drawable.azerloader)
            .into(loaderImage)

        webViewProgressDialog!!.show()
    }//showWebViewProgressPopup ends

    private fun killWebViewProgressDialog() {
        if (this@MainActivity.isFinishing) {
            return  //safe passage
        }// if (this.isFinishing()) ends
        if (webViewProgressDialog != null) {
            if (webViewProgressDialog!!.isShowing) {
                webViewProgressDialog!!.dismiss()
                webViewProgressDialog = null
                logE("webVX", "killed", fromClass, "killWebViewProgressDialog")
            }//if(webViewProgressDialog.isShowing())
        }//if(webViewProgressDialog!=null)
    }//killWebViewProgressDialog ends

    private fun registerAzerFonSetActionBarListener() {
        val azerFonActionBarEvents = object : AzerFonActionBarEvents {
            override fun onDummyFragmentSetActionBar() {
                azerActionBarBubblesBg?.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
                azerActionBarHamburgerIcon?.visibility = View.VISIBLE//show the hamburger icon
                azerActionBarNarLogo?.visibility =
                    View.VISIBLE//show the nar logo in the center of action bar
                azerActionBarBackIcon?.visibility = View.GONE//hide the back icon
                azerActionBarTitle?.visibility = View.GONE//hide the title
                azerActionBarBellIcon?.visibility = View.VISIBLE//shwo the bell icon
                packagesFilterIcon?.visibility = View.GONE
                packagesLayoutChangerIcon?.visibility = View.GONE
                packagesSearchIcon?.visibility = View.GONE
                searchInputField?.visibility = View.GONE
                searchInputField?.setText("")
                searchCrossIcon?.visibility = View.GONE
                searchInputField?.hint =
                    resources.getString(R.string.supplementary_offers_search_hint)
            }

            override fun onInternetFragmentSetActionBar() {
                azerActionBarBubblesBg?.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
                azerActionBarHamburgerIcon?.visibility = View.VISIBLE//show the hamburger icon
                azerActionBarNarLogo?.visibility =
                    View.GONE//show the nar logo in the center of action bar
                azerActionBarBackIcon?.visibility = View.GONE//show the back icon
                azerActionBarTitle?.visibility = View.VISIBLE//show the title
                azerActionBarTitle?.text = resources.getString(R.string.internet)
                azerActionBarBellIcon?.visibility = View.GONE//hide the bell icon
                packagesFilterIcon?.visibility = View.GONE
                packagesLayoutChangerIcon?.visibility = View.GONE
                packagesSearchIcon?.visibility = View.GONE

                if (RootValues.getOffersListOrientation() == LinearLayout.VERTICAL) {
                    packagesLayoutChangerIcon.setImageResource(R.drawable.ic_packages_vertical)
                } else {
                    packagesLayoutChangerIcon.setImageResource(R.drawable.ic_window)
                }
            }

            override fun onTopupMenuSetActionBar() {
                azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
                azerActionBarHamburgerIcon.visibility = View.VISIBLE//show the hamburger icon
                azerActionBarNarLogo.visibility =
                    View.GONE//show the nar logo in the center of action bar
                azerActionBarBackIcon.visibility = View.GONE//hide the back icon
                azerActionBarTitle.visibility = View.VISIBLE//hide the title
                azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
                notificationCountText.visibility = View.GONE
                packagesFilterIcon.visibility = View.GONE
                packagesLayoutChangerIcon.visibility = View.GONE
                packagesSearchIcon.visibility = View.GONE
                searchInputField.visibility = View.GONE
                searchInputField.setText("")
                searchCrossIcon.visibility = View.GONE
                searchInputField.hint =
                    resources.getString(R.string.supplementary_offers_search_hint)
                if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET) != null) {
                    azerActionBarTitle.text =
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET).title.toString()
                    logE(
                        "kuja123",
                        "title:::".plus(navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET).title.toString()),
                        fromClass,
                        "1289-d"
                    )
                }
            }

            override fun onMyAccountMenuSetActionBar() {
                azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
                azerActionBarHamburgerIcon.visibility = View.VISIBLE//show the hamburger icon
                azerActionBarNarLogo.visibility =
                    View.GONE//show the nar logo in the center of action bar
                azerActionBarBackIcon.visibility = View.GONE//hide the back icon
                azerActionBarTitle.visibility = View.VISIBLE//hide the title
                azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
                notificationCountText.visibility = View.GONE
                packagesFilterIcon.visibility = View.GONE
                packagesLayoutChangerIcon.visibility = View.GONE
                packagesSearchIcon.visibility = View.GONE
                searchInputField.visibility = View.GONE
                searchInputField.setText("")
                searchCrossIcon.visibility = View.GONE
                searchInputField.hint =
                    resources.getString(R.string.supplementary_offers_search_hint)
                if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT) != null) {
                    azerActionBarTitle.text =
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT).title.toString()
                    logE(
                        "kuja123",
                        "title:::".plus(navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT).title.toString()),
                        fromClass,
                        "1289-c"
                    )
                }
            }

            override fun onServicesMenuSetActionBar() {
                azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
                azerActionBarHamburgerIcon.visibility = View.VISIBLE//show the hamburger icon
                azerActionBarNarLogo.visibility =
                    View.GONE//show the nar logo in the center of action bar
                azerActionBarBackIcon.visibility = View.GONE//hide the back icon
                azerActionBarTitle.visibility = View.VISIBLE//hide the title
                azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
                notificationCountText.visibility = View.GONE
                packagesFilterIcon.visibility = View.GONE
                packagesLayoutChangerIcon.visibility = View.GONE
                packagesSearchIcon.visibility = View.GONE
                searchInputField.visibility = View.GONE
                searchInputField.setText("")
                searchCrossIcon.visibility = View.GONE
                searchInputField.hint =
                    resources.getString(R.string.supplementary_offers_search_hint)
                if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES) != null) {
                    azerActionBarTitle.text =
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES).title.toString()
                    logE(
                        "kuja123",
                        "title:::".plus(navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES).title.toString()),
                        fromClass,
                        "1289-b"
                    )
                }
            }

            override fun onTariffsSetActionBar() {
                azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
                azerActionBarHamburgerIcon.visibility = View.VISIBLE//show the hamburger icon
                azerActionBarNarLogo.visibility =
                    View.GONE//hide the nar logo in the center of action bar
                azerActionBarBackIcon.visibility = View.GONE//hide the back icon
                azerActionBarTitle.visibility = View.VISIBLE//show the title
                azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
                notificationCountText.visibility = View.GONE
                packagesFilterIcon.visibility = View.GONE
                packagesLayoutChangerIcon.visibility = View.GONE
                packagesSearchIcon.visibility = View.VISIBLE
                searchInputField.visibility = View.GONE
                searchInputField.setText("")
                searchCrossIcon.visibility = View.GONE
                searchInputField.hint =
                    resources.getString(R.string.supplementary_offers_search_hint)

                /**hide the header layout in this case
                 * because it has been shown in the tariff fragment xml*/
                if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS) != null) {
                    azerActionBarTitle.text =
                        navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS).title.toString()
                }
            }

            override fun onDashboardSetActionBar() {
                azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
                azerActionBarHamburgerIcon.visibility = View.VISIBLE//show the hamburger icon
                azerActionBarNarLogo.visibility =
                    View.VISIBLE//show the nar logo in the center of action bar
                azerActionBarBackIcon.visibility = View.GONE//hide the back icon
                azerActionBarTitle.visibility = View.GONE//hide the title
                azerActionBarBellIcon.visibility = View.VISIBLE//shwo the bell icon
                packagesFilterIcon.visibility = View.GONE
                packagesLayoutChangerIcon.visibility = View.GONE
                packagesSearchIcon.visibility = View.GONE
                searchInputField.visibility = View.GONE
                searchInputField.setText("")
                searchCrossIcon.visibility = View.GONE
                searchInputField.hint =
                    resources.getString(R.string.supplementary_offers_search_hint)
            }//onDashboardSetActionBar ends
        }

        //save in the root values
        RootValues.setAzerFonActionBarEvents(azerFonActionBarEvents)
    }

    /**methods for getting and setting
     * the visibility of specific ui views
     * juni1289*/

    private fun isManageAccountsLayoutHidden(): Boolean {
        /**check if the manage account layout is visible
         * then hide it and don't load any fragment
         * if its already hidden, load the particular fragment
         * juni1289*/

        if (manageAccountRecyclerLayout.visibility == View.VISIBLE) {
            manageAccountRecyclerLayout.visibility = View.GONE
            return false
        } else {
            return true
        }

    }//hideManageAccountLayout ends

    /**to close the hamburger menu
     * after clicking on any particular item of the ui
     * juni1289*/
    private fun hamBurgerMenuClose() {
        if (drawer_layout != null) {
            //check if drawer is already opened
            if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                //if opened then close it
                drawer_layout.closeDrawer(GravityCompat.START)
            }
        }//if ends
    }//hamBurgerMenuOpen ends

    private fun hamBurgerMenuOpen() {
        if (drawer_layout != null) {
            //check if drawer is already opened
            if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                //if opened then close it
                drawer_layout.closeDrawer(GravityCompat.START)
            } else {
                //if not opened already, open it now
                drawer_layout.openDrawer(GravityCompat.START)
            }//else ends
        }//if ends
    }//hamBurgerMenuOpen ends

    /**whenever the user comes back to the dashboard
     * select the dashboard icon by default
     * juni1289*/
    private fun selectDashBoardMenu() {

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD)
                .setIcon(getDashboardSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS)
                .setIcon(getTariffsUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES)
                .setIcon(getServicesUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET)
                .setIcon(getInternetUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT)
                .setIcon(getMyAccountUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT).title.toString()
            )
        }
    }

    /**when redirecting from the side menu
     * the bottom navigation should show all icons as unselected and their color should also be unselected gray color
     * except that we are redirecting from side menu
     * suppose user goes to notification screen
     * so there should not be any menu item selected in the botton navigation view
     * juni1289*/
    private fun resetBottomMenuIcons() {
        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD)
                .setIcon(getDashboardSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS)
                .setIcon(getTariffsUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES)
                .setIcon(getServicesUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET)
                .setIcon(getInternetUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT)
                .setIcon(getMyAccountUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT).title.toString()
            )
        }
    }//resetBottomMenuIcons ends

    companion object {
        fun start(context: Context, bundleParam: String) {
            val intent = Intent(context, MainActivity::class.java)
            intent.putExtra(
                ConstantsUtility.BundleKeys.BUNDLE_FROM_LOGIN_OR_SIGNUP_SCREEN_OR_SWITCH_NUMBER,
                bundleParam
            )
            intent.addFlags(
                Intent.FLAG_ACTIVITY_NEW_TASK
                        or
                        Intent.FLAG_ACTIVITY_CLEAR_TASK
                        or
                        Intent.FLAG_ACTIVITY_CLEAR_TOP
            )
            context.startActivity(intent)
        }//start ends

        fun start(
            context: Context,
            bundleParam: String,
            screenToRedirect: String,
            subScreenToRedirect: String,
            stepOneScreenToRedirect: String,
            tabToRedirect: String,
            offeringIdToRedirect: String,
            isBottomTabAvailable: Boolean
        ) {
            val intent = Intent(context, MainActivity::class.java).apply {
                putExtra(
                    ConstantsUtility.BundleKeys.BUNDLE_FROM_LOGIN_OR_SIGNUP_SCREEN_OR_SWITCH_NUMBER,
                    bundleParam
                )
                putExtra(ConstantsUtility.DynamicLinkConstants.SCREEN_NAME_DATA, screenToRedirect)
                putExtra(
                    ConstantsUtility.DynamicLinkConstants.SUB_SCREEN_NAME_DATA,
                    subScreenToRedirect
                )
                putExtra(
                    ConstantsUtility.DynamicLinkConstants.STEP_ONE_SCREEN_NAME_DATA,
                    stepOneScreenToRedirect
                )
                putExtra(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA, tabToRedirect)
                putExtra(
                    ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA,
                    offeringIdToRedirect
                )
                putExtra(ConstantsUtility.DynamicLinkConstants.BOTTOM_MENU, isBottomTabAvailable)
                addFlags(
                    Intent.FLAG_ACTIVITY_NEW_TASK
                            or
                            Intent.FLAG_ACTIVITY_CLEAR_TASK
                            or
                            Intent.FLAG_ACTIVITY_CLEAR_TOP
                )
            }

            context.startActivity(intent)
        }//start ends
    }//object ends

    @SuppressLint("InflateParams")
    private fun showLogoutPopupForMultipleUsers() {
        if (this@MainActivity.isFinishing) return

        val dialogBuilder = AlertDialog.Builder(this@MainActivity)
        val inflater =
            applicationContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val dialogView = inflater.inflate(R.layout.azer_logout_popup_for_multiple_users, null)
        dialogBuilder.setView(dialogView)

        val alertDialog = dialogBuilder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.show()

        val popupOkayButton = dialogView.findViewById(R.id.yesButton) as Button
        popupOkayButton.typeface = getALSBoldFont()
        popupOkayButton.isSelected = true
        val popupCancelButton = dialogView.findViewById(R.id.noButton) as Button
        popupCancelButton.typeface = getALSBoldFont()
        popupCancelButton.isSelected = true
        val manageAccountsButton = dialogView.findViewById(R.id.manageAccountsButton) as Button
        manageAccountsButton.typeface = getALSBoldFont()
        manageAccountsButton.isSelected = true
        val popupTitle = dialogView.findViewById(R.id.popupTitle) as TextView

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN) {
            //english
            popupTitle.text =
                "Are you sure you want to sign out from all accounts? You can also delete single accounts from Manage account."
            popupOkayButton.text = "Sign out"
            popupCancelButton.text = "Cancel"
            manageAccountsButton.text = "Manage Accounts"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ) {
            //azri
            popupTitle.text =
                "Bütün hesablardan çıxmaq istədiyindən əminsən? Fərdi hesabları \"Hesabı idarə et\" bölümündən silə bilərsən."
            popupOkayButton.text = "Çıx"
            popupCancelButton.text = "Ləğv et"
            manageAccountsButton.text = "Hesabın idarə edilməsi"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.RU) {
            //russian
            popupTitle.text =
                "Ты уверен, что хочешь выйти из всех аккаунтов? Ты также можешь удалить отдельные учетные записи из управления аккаунтом."
            popupOkayButton.text = "Выйти"
            popupCancelButton.text = "Отменить"
            manageAccountsButton.text = "Управление аккаунтом"
        }

        popupOkayButton.isSelected = true
        popupCancelButton.isSelected = true
        manageAccountsButton.isSelected = true

        popupTitle.typeface = getALSNormalFont()
        manageAccountsButton.typeface = getALSBoldFont()
        popupOkayButton.typeface = getALSBoldFont()
        popupCancelButton.typeface = getALSBoldFont()

        manageAccountsButton.setOnClickListener {
            alertDialog.dismiss()
            ManageAccountActivity.start(this)
        }//manageAccountsButton.setOnClickListener ends

        popupOkayButton.setOnClickListener {
            alertDialog.dismiss()
            //call the logout api
            mViewModel.requestLogOut()
        }//popupOkayButton.setOnClickListener ends

        popupCancelButton.setOnClickListener {
            alertDialog.dismiss()
            //do nothing
        }//popupCancelButton.setOnClickListener  ends
    }//showLogoutPopupForMultipleUsers ends

    @SuppressLint("InflateParams")
    private fun showLogoutPopupForSingleUser(title: String) {
        if (this@MainActivity.isFinishing) return

        val dialogBuilder = AlertDialog.Builder(this@MainActivity)
        val inflater =
            applicationContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val dialogView = inflater.inflate(R.layout.azer_logout_popup, null)
        dialogBuilder.setView(dialogView)

        val alertDialog = dialogBuilder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.show()

        val popupOkayButton = dialogView.findViewById(R.id.yesButton) as Button
        popupOkayButton.isSelected = true
        popupOkayButton.typeface = getALSBoldFont()
        val popupCancelButton = dialogView.findViewById(R.id.noButton) as Button
        popupCancelButton.isSelected = true
        popupCancelButton.typeface = getALSBoldFont()
        val popupTitle = dialogView.findViewById(R.id.popupTitle) as TextView
        popupTitle.text = title
        popupTitle.typeface = getALSNormalFont()
        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN) {
            //english
            popupOkayButton.text = "Sign out"
            popupCancelButton.text = "Cancel"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ) {
            //azri
            popupOkayButton.text = "Çıx"
            popupCancelButton.text = "Ləğv et"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.RU) {
            //russian
            popupOkayButton.text = "Выйти"
            popupCancelButton.text = "Отменить"
        }

        popupOkayButton.setOnClickListener {
            alertDialog.dismiss()
            //call the logout api
            mViewModel.requestLogOut()
        }

        popupCancelButton.setOnClickListener {
            alertDialog.dismiss()
            //do nothing
        }
    }//showLogoutPopupForSingleUser ends

    override fun onSwitchNumberClick(customerData: CustomerData) {
        hamBurgerMenuClose()
        val isPasswordChanged = ProjectApplication.getInstance().getLocalPreferneces().getBool(
            ProjectApplication.getInstance()
                .getLocalPreferneces().IS_PASSWORD_CHANGE_MULTI_USER_LOGGED_OUT,
            false
        )

        if (!UserDataManager.getCustomerData()!!.msisdn.equals(customerData.msisdn) || isPasswordChanged) {
            logE("switch", "called", fromClass, "onSwitchNumberClick")

            setCustomerDataToLocalPreferences(getUpdatedCustomerDataLocaliy(customerData))
            UserDataManager.saveCustomerData(getUpdatedCustomerDataLocaliy(customerData))
            UserDataManager.resetUserDataManager()
            start(applicationContext, "")
            finish()
        }
    }//onSwitchNumberClick ends


    private fun getDataFromIntent() {

        intent?.let {

            if (it.hasExtra(ConstantsUtility.DynamicLinkConstants.SCREEN_NAME_DATA)
                && hasValue(it.getStringExtra(ConstantsUtility.DynamicLinkConstants.SCREEN_NAME_DATA))
            ) {
                screenNameToRedirect =
                    it.getStringExtra(ConstantsUtility.DynamicLinkConstants.SCREEN_NAME_DATA)!!
            }

            if (it.hasExtra(ConstantsUtility.DynamicLinkConstants.SUB_SCREEN_NAME_DATA)
                && hasValue(it.getStringExtra(ConstantsUtility.DynamicLinkConstants.SUB_SCREEN_NAME_DATA))
            ) {
                subScreenNameToRedirect =
                    it.getStringExtra(ConstantsUtility.DynamicLinkConstants.SUB_SCREEN_NAME_DATA)!!
            }

            if (it.hasExtra(ConstantsUtility.DynamicLinkConstants.STEP_ONE_SCREEN_NAME_DATA)
                && hasValue(it.getStringExtra(ConstantsUtility.DynamicLinkConstants.STEP_ONE_SCREEN_NAME_DATA))
            ) {
                stepOneScreenNameToRedirect =
                    it.getStringExtra(ConstantsUtility.DynamicLinkConstants.STEP_ONE_SCREEN_NAME_DATA)!!
            }

            if (it.hasExtra(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA)
                && hasValue(it.getStringExtra(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA))
            ) {
                tabNameToRedirect =
                    it.getStringExtra(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA)!!
            }

            if (it.hasExtra(ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA)
                && hasValue(it.getStringExtra(ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA))
            ) {
                offeringIdToRedirect =
                    it.getStringExtra(ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA)!!
            }

            if (it.hasExtra(ConstantsUtility.DynamicLinkConstants.BOTTOM_MENU)
            ) {
                isBottomTabAvailable =
                    it.getBooleanExtra(ConstantsUtility.DynamicLinkConstants.BOTTOM_MENU, false)
            }

            if (it.hasExtra(ConstantsUtility.BundleKeys.BUNDLE_FROM_LOGIN_OR_SIGNUP_SCREEN_OR_SWITCH_NUMBER)
                && hasValue(it.getStringExtra(ConstantsUtility.BundleKeys.BUNDLE_FROM_LOGIN_OR_SIGNUP_SCREEN_OR_SWITCH_NUMBER))
            ) {
                appResumeCallingData =
                    it.getStringExtra(ConstantsUtility.BundleKeys.BUNDLE_FROM_LOGIN_OR_SIGNUP_SCREEN_OR_SWITCH_NUMBER)!!
            }
            logE(
                ConstantsUtility.DynamicLinkConstants.DYNAMIC_LINK_TAG,
                "isBottomTabAvailable = $isBottomTabAvailable, subScreenNameToRedirect = $subScreenNameToRedirect, stepOneScreenNameToRedirect = $stepOneScreenNameToRedirect" +
                        "tabNameToRedirect = $tabNameToRedirect, offeringIdToRedirect = $offeringIdToRedirect" +
                        "appResumeCallingData = $appResumeCallingData",
                fromClass,
                "getDataFromIntent"
            )
        }

    }//getDataFromIntent ends

    /** Dynamic Links Redirection Starts */
    private fun dynamicLinksNavigation() {
        if (isBottomTabAvailable) {
            dynamicLinkBottomNavigation()
        } else {
            dynamicLinkDrawerNavigation()
        }
    }//dynamicLinksNavigation ends

    private fun dynamicLinkBottomNavigation() {
        when {
            screenNameToRedirect.equals(
                ConstantsUtility.HorizontalMenuIdentifiers.IDENTIFIER_DASHBOARD,
                ignoreCase = true
            ) -> {
                redirectToDashboardFromDynamicLink()
            }
            screenNameToRedirect.equals(
                ConstantsUtility.HorizontalMenuIdentifiers.IDENTIFIER_TARIFFS,
                ignoreCase = true
            ) -> {
                redirectToTariffsFromDynamicLink()
            }
            screenNameToRedirect.equals(
                ConstantsUtility.HorizontalMenuIdentifiers.IDENTIFIER_SERVICES,
                ignoreCase = true
            ) -> {
                redirectToServicesFromDynamicLink()
            }
            screenNameToRedirect.equals(
                ConstantsUtility.HorizontalMenuIdentifiers.IDENTIFIER_INTERNET,
                ignoreCase = true
            ) -> {
                redirectToInternetFromDynamicLink()
            }

            screenNameToRedirect.equals(
                ConstantsUtility.HorizontalMenuIdentifiers.IDENTIFIER_TOPUP,
                ignoreCase = true
            ) -> {
                redirectToTopUpFromDynamicLink()
            }

            screenNameToRedirect.equals(
                ConstantsUtility.HorizontalMenuIdentifiers.IDENTIFIER_MYACCOUNT,
                ignoreCase = true
            ) -> {
                redirectToMyAccountFromDynamicLink()
            }
        }
    }//dynamicLinkBottomNavigation ends

    private fun dynamicLinkDrawerNavigation() {
        if (hasValue(screenNameToRedirect))
            when {
                screenNameToRedirect.equals(
                    ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_STORE_LOCATOR,
                    ignoreCase = true
                ) -> {
                    val intent = Intent(this, StoreLocatorActivity::class.java)
                    startActivity(intent)
                }

                screenNameToRedirect.equals(
                    ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_NOTIFICATIONS,
                    ignoreCase = true
                ) -> {
                    val intent = Intent(this, NotificationActivity::class.java)
                    startActivity(intent)
                }


                screenNameToRedirect.equals(
                    ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_MANAGE_ACCOUNT,
                    ignoreCase = true
                ) -> {
                    val intent = Intent(this, ManageAccountActivity::class.java)
                    startActivity(intent)
                }

                screenNameToRedirect.equals(
                    ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_FAQ,
                    ignoreCase = true
                ) -> {
                    val intent = Intent(this, FaqActivity::class.java)
                    startActivity(intent)
                }
                screenNameToRedirect.equals(
                    ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_TUTORIAL_AND_FAQ,
                    ignoreCase = true
                ) -> {
                    val intent = Intent(this, TutorialsActivity::class.java)
                    startActivity(intent)
                }

                screenNameToRedirect.equals(
                    ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_SETTINGS,
                    ignoreCase = true
                ) -> {
                    val intent = Intent(this, SettingsActivity::class.java)
                    startActivity(intent)
                }


                screenNameToRedirect.equals(
                    ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_CONTACT_US,
                    ignoreCase = true
                ) -> {
                    val intent = Intent(this, ContactUsActivity::class.java)
                    startActivity(intent)
                }


                screenNameToRedirect.equals(
                    ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_LIVE_CHAT,
                    ignoreCase = true
                ) -> {
                    setupNewWebView()
                }

                screenNameToRedirect.equals(
                    ConstantsUtility.VerticalMenuIdentifiers.IDENTIFIER_TERMS_AND_CONDITIONS,
                    ignoreCase = true
                ) -> {
                    val intent = Intent(this, TermsAndConditionsActivity::class.java)
                    startActivity(intent)
                }
            }

        logE(
            ConstantsUtility.DynamicLinkConstants.DYNAMIC_LINK_TAG,
            "isBottomTabAvailable = $isBottomTabAvailable, subScreenNameToRedirect = $subScreenNameToRedirect, stepOneScreenNameToRedirect = $stepOneScreenNameToRedirect" +
                    "tabNameToRedirect = $tabNameToRedirect, offeringIdToRedirect = $offeringIdToRedirect" +
                    "appResumeCallingData = $appResumeCallingData",
            fromClass,
            "dynamicLinkDrawerNavigation"
        )
    }  //dynamicLinkDrawerNavigation ends

    private fun redirectToDashboardFromDynamicLink() {
        val mBundle = Bundle()

        mBundle.apply {
            if (hasValue(subScreenNameToRedirect)) {
                putString(
                    ConstantsUtility.DynamicLinkConstants.SUB_SCREEN_NAME_DATA,
                    subScreenNameToRedirect
                )
            }

            if (hasValue(tabNameToRedirect)) {
                putString(
                    ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA,
                    tabNameToRedirect
                )
            }

            if (hasValue(offeringIdToRedirect)) {
                putString(
                    ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA,
                    offeringIdToRedirect
                )
            }
        }

        Navigation.findNavController(this@MainActivity, R.id.fragmentContainer)
            .navigate(R.id.home_dest, mBundle)

        /**select the dashboard menu
         * unselect all other menus*/
        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD)
                .setIcon(getDashboardSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS)
                .setIcon(getTariffsUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES)
                .setIcon(getServicesUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP)
                .setIcon(getTopUpUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET)
                .setIcon(getInternetUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT)
                .setIcon(getMyAccountUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT).title.toString()
            )
        }
    }//redirectToDashboardFromDynamicLink ends

    private fun redirectToTariffsFromDynamicLink() {
        val mBundle = Bundle()

        mBundle.apply {

            if (hasValue(tabNameToRedirect)) {
                putString(
                    ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA,
                    tabNameToRedirect
                )
            }

            if (hasValue(offeringIdToRedirect)) {
                putString(
                    ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA,
                    offeringIdToRedirect
                )
            }
        }

        Navigation.findNavController(this@MainActivity, R.id.fragmentContainer)
            .navigate(R.id.tarif, mBundle)

        /**select the tariff menu
         * unselect all other menus*/

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS)
                .setIcon(getTariffsSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD)
                .setIcon(getDashboardUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES)
                .setIcon(getServicesUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP)
                .setIcon(getTopUpUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET)
                .setIcon(getInternetUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT)
                .setIcon(getMyAccountUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT).title.toString()
            )
        }
    }//redirectToTariffsFromDynamicLink ends

    private fun redirectToServicesFromDynamicLink() {
        val mBundle = Bundle()

        mBundle.apply {
            if (hasValue(subScreenNameToRedirect)) {
                putString(
                    ConstantsUtility.DynamicLinkConstants.SUB_SCREEN_NAME_DATA,
                    subScreenNameToRedirect
                )
            }

            if (hasValue(stepOneScreenNameToRedirect)) {
                putString(
                    ConstantsUtility.DynamicLinkConstants.STEP_ONE_SCREEN_NAME_DATA,
                    stepOneScreenNameToRedirect
                )
            }

            if (hasValue(tabNameToRedirect)) {
                putString(
                    ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA,
                    tabNameToRedirect
                )
            }

            if (hasValue(offeringIdToRedirect)) {
                putString(
                    ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA,
                    offeringIdToRedirect
                )
            }
        }

        Navigation.findNavController(this@MainActivity, R.id.fragmentContainer)
            .navigate(R.id.services, mBundle)

        /**select the services menu
         * unselect all other menus*/
        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES) != null) {
            logE(
                "xxyyzz123",
                "Yes Confition mathed",
                fromClass,
                "redirectToServicesFromDynamicLink"
            )
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES)
                .setIcon(getServicesSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD)
                .setIcon(getDashboardUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS)
                .setIcon(getTariffsUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP)
                .setIcon(getTopUpUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET)
                .setIcon(getInternetUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT)
                .setIcon(getMyAccountUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT).title.toString()
            )
        }
    }//redirectToServicesFromDynamicLink ends

    private fun redirectToInternetFromDynamicLink() {
        val mBundle = Bundle()

        mBundle.apply {

            if (hasValue(subScreenNameToRedirect)) {
                putString(
                    ConstantsUtility.DynamicLinkConstants.SUB_SCREEN_NAME_DATA,
                    subScreenNameToRedirect
                )
            }

            if (hasValue(tabNameToRedirect)) {
                putString(
                    ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA,
                    tabNameToRedirect
                )
            }

            if (hasValue(offeringIdToRedirect)) {
                putString(
                    ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA,
                    offeringIdToRedirect
                )
            }

        }

        Navigation.findNavController(this@MainActivity, R.id.fragmentContainer)
            .navigate(R.id.internet, mBundle)

        /**select the internet menu
         * unselect all other menus*/
        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET)
                .setIcon(getInternetSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD)
                .setIcon(getDashboardUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS)
                .setIcon(getTariffsUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES)
                .setIcon(getServicesUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP)
                .setIcon(getTopUpUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT)
                .setIcon(getMyAccountUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT).title.toString()
            )
        }

    }//redirectToTopUpFromDynamicLink ends

    private fun redirectToTopUpFromDynamicLink() {
        val mBundle = Bundle()

        mBundle.apply {

            if (hasValue(subScreenNameToRedirect)) {
                putString(
                    ConstantsUtility.DynamicLinkConstants.SUB_SCREEN_NAME_DATA,
                    subScreenNameToRedirect
                )
            }

            if (hasValue(tabNameToRedirect)) {
                putString(
                    ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA,
                    tabNameToRedirect
                )
            }

            if (hasValue(offeringIdToRedirect)) {
                putString(
                    ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA,
                    offeringIdToRedirect
                )
            }

        }

        Navigation.findNavController(this@MainActivity, R.id.fragmentContainer)
            .navigate(R.id.topup, mBundle)

        /**select the topup menu
         * unselect all other menus*/
        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP)
                .setIcon(getTopUpSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD)
                .setIcon(R.drawable.dashboardunselectedicon)
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS)
                .setIcon(getTariffsUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES)
                .setIcon(getServicesUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET)
                .setIcon(getInternetUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT)
                .setIcon(R.drawable.accountunselectedicon)
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT).title.toString()
            )
        }

    }//redirectToTopUpFromDynamicLink ends

    private fun redirectToMyAccountFromDynamicLink() {
        val mBundle = Bundle()

        mBundle.apply {
            if (hasValue(subScreenNameToRedirect)) {
                putString(
                    ConstantsUtility.DynamicLinkConstants.SUB_SCREEN_NAME_DATA,
                    subScreenNameToRedirect
                )
            }

            if (hasValue(tabNameToRedirect)) {
                putString(
                    ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA,
                    tabNameToRedirect
                )
            }

            if (hasValue(offeringIdToRedirect)) {
                putString(
                    ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA,
                    offeringIdToRedirect
                )
            }
        }

        Navigation.findNavController(this@MainActivity, R.id.fragmentContainer)
            .navigate(R.id.myaccount, mBundle)

        /**select myaccount menu
         * deselect all other menus*/
        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT)
                .setIcon(getMyAccountSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_MYACCOUNT).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD)
                .setIcon(getDashboardUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_DASHBOARD).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS)
                .setIcon(getTariffsUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TARIFFS).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES)
                .setIcon(getServicesUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_SERVICES).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP)
                .setIcon(getTopUpUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_TOPUP).title.toString()
            )
        }

        if (navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET) != null) {
            navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET)
                .setIcon(getInternetUnSelectedIcon())
                .title = getMenuTextColoredString(
                resources.getColor(R.color.colorMenuUnSelected),
                navigationView.menu.findItem(ConstantsUtility.HorizontalMenuIds.ID_INTERNET).title.toString()
            )
        }
    }//redirectToMyAccountFromDynamicLink ends
    /** Dynamic Links Redirection Ends */
}//class ends
