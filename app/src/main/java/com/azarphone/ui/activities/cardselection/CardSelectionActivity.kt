package com.azarphone.ui.activities.cardselection

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.azarphone.R
import com.azarphone.api.pojo.request.InitiatePaymentRequest
import com.azarphone.api.pojo.response.predefinedata.CardType
import com.azarphone.api.pojo.response.topup.initiatepayment.InitiatePaymentResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.ActivityCardSelectionBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.TopUpItemSelectEvents
import com.azarphone.ui.activities.topup.PlasticCardActivity
import com.azarphone.ui.adapters.spinners.CardTypeSpinnerAdapter
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.OnClickListener
import com.azarphone.validators.hasValue
import com.azarphone.widgets.popups.showConfirmationDialog
import com.azarphone.widgets.popups.showMessageDialog
import kotlinx.android.synthetic.main.activity_card_selection.*
import kotlinx.android.synthetic.main.activity_card_selection.cardSpinnerTitle
import kotlinx.android.synthetic.main.activity_card_selection.cardTypeSP
import kotlinx.android.synthetic.main.activity_card_selection.cb_SaveCard
import kotlinx.android.synthetic.main.activity_card_selection.spinnerCL
import kotlinx.android.synthetic.main.layout_faq_fragment.*
import kotlinx.android.synthetic.main.layout_topup_activity.*
import java.util.*

class CardSelectionActivity : BaseActivity<ActivityCardSelectionBinding, CardSelectionFactory, CardSelectionViewModel>() {

    private var cardType: String = ""
    private var isSaveCard: Boolean = true
    private var mobileNumber = ""
    private var amount = ""
    private lateinit var cardTypeSpinnerAdapter: CardTypeSpinnerAdapter

    override fun getLayoutId(): Int {
        return R.layout.activity_card_selection
    }

    override fun getViewModelClass(): Class<CardSelectionViewModel> {
        return CardSelectionViewModel::class.java
    }

    override fun getFactory(): CardSelectionFactory {
        return CardSelectionInjection.provideCardSelectionFactory()
    }

    override fun init() {
        initData()
        setUpSpinnerAdapter()
        subscribe()
        intitUiEvent()
    }

    private fun initData() {
        if (intent.extras != null) {
            mobileNumber = intent.extras?.get(ConstantsUtility.TopUpConstants.MOBILE_NUMBER) as String
            amount = intent.extras?.get(ConstantsUtility.TopUpConstants.AMOUNT) as String
        }
    }

    private fun intitUiEvent() {
        continueButton.setOnClickListener {
            //initiate payment
            if (hasValue(cardType) && hasValue(mobileNumber) && hasValue(amount)) {
                val dialogMessage = getString(R.string.payment_loading) + amount + getString(R.string.azn_to) + mobileNumber
                showConfirmationDialog(this@CardSelectionActivity, this@CardSelectionActivity,
                        "", dialogMessage, object : OnClickListener {
                    override fun onClick() {
                        var initiatePaymentRequest = InitiatePaymentRequest(cardType, amount, "" + isSaveCard, mobileNumber)
                        mViewModel.requestInitiatePayment(initiatePaymentRequest)
                    }
                })

            }
        }

        cb_SaveCard.setOnCheckedChangeListener { buttonView, isChecked ->
            isSaveCard = isChecked
        }
    }

    private fun subscribe() {
        val initiatePaymentLiveData = object : Observer<InitiatePaymentResponse> {
            override fun onChanged(t: InitiatePaymentResponse?) {
                processInitiateResponse(t)
            }
        }

        mViewModel.initiatePaymentResponseLiveData.observe(this@CardSelectionActivity, initiatePaymentLiveData)

    }

    private fun setUpSpinnerAdapter() {

        val userMain = UserDataManager.getPredefineData()
        if (userMain != null && userMain.topup?.plasticCard?.cardTypes != null && userMain.topup.plasticCard.cardTypes!!.isNotEmpty()) {

            val spinnerTitlesList = userMain.topup?.plasticCard?.cardTypes

            //now populate the spinner
            cardTypeSpinnerAdapter = CardTypeSpinnerAdapter(this@CardSelectionActivity, spinnerTitlesList as ArrayList<CardType>, object : TopUpItemSelectEvents {
                override fun onItemSelected(position: Int, searchKey: String) {
                    cardType = searchKey
                    cardSpinnerTitle.text = spinnerTitlesList[position].value
                    cardTypeSP.visibility = View.GONE
                    cardTypeSP.onDetachedFromWindow()
                }

            })
            cardType = userMain.topup?.plasticCard?.cardTypes!!.get(0)?.key.toString();
            cardSpinnerTitle.text = spinnerTitlesList[0].value
            cardTypeSP.adapter = cardTypeSpinnerAdapter

        }
        spinnerCL.setOnClickListener {
            cardTypeSP.visibility = View.VISIBLE
            cardTypeSP.performClick()
        }

    }


    private fun processInitiateResponse(response: InitiatePaymentResponse?) {

        var responseDescription = ""
        if (!this@CardSelectionActivity.isFinishing() && response != null &&
                hasValue(response.resultDesc)) {
            responseDescription = response.resultDesc
        }

        if (!this@CardSelectionActivity.isFinishing() && response != null) {
            if (response.data != null && hasValue(response.data.paymentKey)) {
                val bundle = Bundle()
                PlasticCardActivity.start(this, response.data.paymentKey)
                finish()
            } else {
                showMessageDialog(this@CardSelectionActivity, this@CardSelectionActivity,
                        getString(R.string.popup_error_title), responseDescription)
            }
        } else {
            showMessageDialog(this@CardSelectionActivity, this@CardSelectionActivity,
                    getString(R.string.popup_error_title), responseDescription)
        }
    }

    companion object {
        fun start(context: Context, mobileNumber: String, amount: String) {
            val intent = Intent(context, CardSelectionActivity::class.java)
            intent.putExtra(ConstantsUtility.TopUpConstants.MOBILE_NUMBER, mobileNumber)
            intent.putExtra(ConstantsUtility.TopUpConstants.AMOUNT, amount)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends
    }//object ends
}