package com.azarphone.ui.activities.settings

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.hardware.fingerprint.FingerprintManager
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.lifecycle.Observer
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.response.fcmtoken.FCMResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.LayoutSettingsActivityBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.ui.activities.changepassword.ChangePasswordActivity
import com.azarphone.ui.activities.mainactivity.MainActivity
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.widgets.popups.showMessageDialog
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.layout_settings_activity.*


/**
 * @author Junaid Hassan on 18, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class SettingsActivity :
    BaseActivity<LayoutSettingsActivityBinding, SettingsFactory, SettingsViewModel>() {

    private val fromClass = "SettingsActivity"
    private var selectedLanguage = ""

    override fun getLayoutId(): Int {
        return R.layout.layout_settings_activity
    }//getLayoutId ends

    override fun getViewModelClass(): Class<SettingsViewModel> {
        return SettingsViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): SettingsFactory {
        return SettingsInjection.provideSettingsFactory()
    }//getFactory ends

    override fun init() {
        subscribe()
        initUI()
        initUIEvents()
        initFingerPrintScanner()
    }//init ends

    private fun subscribe() {
        val fcmResponseLiveData = object : Observer<FCMResponse> {
            override fun onChanged(t: FCMResponse?) {
                if (t != null && hasValue(t.resultDesc)) {
                    showMessageDialog(
                        applicationContext, this@SettingsActivity,
                        "",
                        t.resultDesc
                    )
                    if (t.data != null) {
                        setFCMDataToLocalPreferences(t)
                        processFCMUI(t)
                    }
                }
            }
        }

        mViewModel.fcmResponseLiveData.observe(this@SettingsActivity, fcmResponseLiveData)

        val fcmResponseSilentLiveData = object : Observer<FCMResponse> {
            override fun onChanged(t: FCMResponse?) {
                if (t != null && t.data != null && hasValue(t.data.toString())) {
                    setFCMDataToLocalPreferences(t)
                    processFCMUI(t)
                }
            }
        }

        mViewModel.fcmResponseSilentLiveData.observe(
            this@SettingsActivity,
            fcmResponseSilentLiveData
        )
    }//subscribe ends

    private fun initFingerPrintScanner() {
        /**detect the finger print scanner
         * if is present in the device show the scanner layout
         * else hide the layout*/
        fingerPrintSwitch.isChecked = getIsBiometricEnabledFlagFromLocalPreferences()
        logE(
            "biometric",
            "biomtericFlag:::".plus(getIsBiometricEnabledFlagFromLocalPreferences()),
            fromClass,
            "initFingerPrintScanner"
        )

        if (android.os.Build.VERSION.SDK_INT >= 23) {
            // Initializing Fingerprint Manager
            val fingerprintManager =
                getSystemService(Context.FINGERPRINT_SERVICE) as? FingerprintManager
            if (fingerprintManager != null && fingerprintManager.isHardwareDetected) {
                logE("biometric", "hardware detected:::", fromClass, "initFingerPrintScanner")
                fingerPrintScannerLayout.visibility = View.VISIBLE
            } else {
                logE("biometric", "hardware not detected:::", fromClass, "initFingerPrintScanner")
                fingerPrintScannerLayout.visibility = View.GONE
            }
        } else {
            logE("biometric", "build version not matched:::", fromClass, "initFingerPrintScanner")
            fingerPrintScannerLayout.visibility = View.GONE
        }

        /**event listeners for the finger print switch*/
        fingerPrintSwitch.setOnCheckedChangeListener { _, p1 ->

            if (p1) {
                if (android.os.Build.VERSION.SDK_INT >= 23) {
                    // Initializing Fingerprint Manager
                    val fingerprintManager =
                        getSystemService(Context.FINGERPRINT_SERVICE) as? FingerprintManager
                    if (fingerprintManager != null && fingerprintManager.isHardwareDetected) {
                        if (fingerprintManager.hasEnrolledFingerprints()) {
                            fingerPrintSwitch.isChecked = true
                            setIsBiometricEnabledFlagToLocalPreferences(true)
                        } else {
                            showMessageDialog(
                                applicationContext, this@SettingsActivity,
                                applicationContext.resources.getString(R.string.popup_error_title),
                                applicationContext.resources.getString(R.string.biometric_no_finger_prints_enrolled_error)
                            )
                            fingerPrintSwitch.isChecked = false
                            setIsBiometricEnabledFlagToLocalPreferences(false)
                        }
                    } else {
                        fingerPrintSwitch.isChecked = false
                        setIsBiometricEnabledFlagToLocalPreferences(false)
                        showMessageDialog(
                            applicationContext,
                            this@SettingsActivity,
                            applicationContext.resources.getString(R.string.popup_error_title),
                            applicationContext.resources.getString(R.string.biometric_not_available_error_label)
                        )
                    }
                } else {
                    fingerPrintSwitch.isChecked = false
                    setIsBiometricEnabledFlagToLocalPreferences(false)
                    showMessageDialog(
                        applicationContext,
                        this@SettingsActivity,
                        applicationContext.resources.getString(R.string.popup_error_title),
                        applicationContext.resources.getString(R.string.biometric_not_available_error_label)
                    )
                }
            } else {
                fingerPrintSwitch.isChecked = false
                setIsBiometricEnabledFlagToLocalPreferences(false)
            }
        }//view.fingerPrintSwitch.setOnCheckedChangeListener ends
    }//initFingerPrintScanner ends

    private fun initUI() {
        setupHeaderActionBar()
        setCurrentLanguageLabel()
        setAppVersion()

        fingerPrintLoginHeading.isSelected = true
        if (!this@SettingsActivity.isFinishing) {
            //set the ui views
            if (getFCMDataFromLocalPreferences().data != null &&
                hasValue(getFCMDataFromLocalPreferences().data.toString()) &&
                hasValue(getFCMDataFromLocalPreferences().data.isEnable) &&
                hasValue(getFCMDataFromLocalPreferences().data.ringingStatus)
            ) {
                processFCMUI(getFCMDataFromLocalPreferences())
                getFCMToken(
                    getFCMDataFromLocalPreferences().data.ringingStatus!!,
                    ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_ON,
                    ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_API_CAUSE_LOGIN,
                    true,
                    false
                )
            }
        }
    }//initUI ends

    private fun getFCMToken(
        ringingStatus: String,
        isEnable: String,
        cause: String,
        isBackground: Boolean,
        isShowMessage: Boolean
    ) {
        if (!this@SettingsActivity.isFinishing) {
            var token = ""
            if (FirebaseMessaging.getInstance() != null && FirebaseMessaging.getInstance().token != null) {
                token = FirebaseMessaging.getInstance().token.toString()
            }
            try {
                var msisdnFCM = ""
                var subscriberTypeFCM = ""
                var tariffTypeFCM = ""
                msisdnFCM = UserDataManager?.getCustomerData()?.msisdn!!
                subscriberTypeFCM = UserDataManager?.getCustomerData()?.subscriberType!!
                tariffTypeFCM = UserDataManager?.getCustomerData()?.brandName!!
                mViewModel.addFCMTokenToServerFromSettings(
                    isShowMessage,
                    token,
                    ringingStatus,
                    isEnable,
                    cause,
                    msisdnFCM,
                    isBackground,
                    subscriberTypeFCM,
                    tariffTypeFCM
                )
            } catch (exp: Exception) {
                //dafuq
            }
            logE("FCM", "FCM token = $token", fromClass, "getFCMToken")
        }
    }//getFCMToken ends

    private fun processFCMUI(fcmResponse: FCMResponse) {
        val fcmResponseData = fcmResponse.data
        if (hasValue(fcmResponseData.isEnable)) {
            if (fcmResponseData.isEnable.equals(
                    ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_ON,
                    ignoreCase = true
                )
            ) {
                notificationsSwitch.isChecked = true

                radioToneView.isEnabled = true
                radioVibrateView.isEnabled = true
                radioMuteView.isEnabled = true
            } else if (fcmResponseData.isEnable.equals(
                    ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_OFF,
                    ignoreCase = true
                )
            ) {
                notificationsSwitch.isChecked = false

                radioToneView.isEnabled = false
                radioVibrateView.isEnabled = false
                radioMuteView.isEnabled = false
            }
        }

        if (hasValue(fcmResponseData.ringingStatus)) {
            if (ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_TONE.equals(
                    fcmResponseData.ringingStatus,
                    ignoreCase = true
                )
            ) {
                radioTone.isChecked = true
                radioVibrate.isChecked = false
                radioMute.isChecked = false
            } else if (ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_VIBRATE.equals(
                    fcmResponseData.ringingStatus,
                    ignoreCase = true
                )
            ) {
                radioTone.isChecked = false
                radioVibrate.isChecked = true
                radioMute.isChecked = false
            } else if (ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_MUTE.equals(
                    fcmResponseData.ringingStatus,
                    ignoreCase = true
                )
            ) {
                radioTone.isChecked = false
                radioVibrate.isChecked = false
                radioMute.isChecked = true
            }
        }
    }//processFCMUI ends

    private fun setAppVersion() {
        appVersionValue.text = getAppVersion(applicationContext)
    }//setAppVersion ends

    private fun setCurrentLanguageLabel() {
        selectedLanguage = ProjectApplication.mLocaleManager.getLanguage()
        when {
            selectedLanguage.equals(
                ConstantsUtility.LanguageKeywords.AZ,
                ignoreCase = true
            ) -> changeLanguageValue.text = resources.getString(R.string.azeri)
            selectedLanguage.equals(
                ConstantsUtility.LanguageKeywords.EN,
                ignoreCase = true
            ) -> changeLanguageValue.text = resources.getString(R.string.english)
            selectedLanguage.equals(
                ConstantsUtility.LanguageKeywords.RU,
                ignoreCase = true
            ) -> changeLanguageValue.text = resources.getString(R.string.russian)
            else -> changeLanguageValue.text = resources.getString(R.string.azeri)
        }

        changeLanguageValue.isSelected = true
    }//setCurrentLanguageLabel ends

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (UserDataManager.isChangeLanguageSuccess()) {
                UserDataManager.setChangeLanguageSuccess(false)
                MainActivity.start(applicationContext, "")
                finish()
            } else {
                finish()
            }
            return true
        }
        return super.onKeyDown(keyCode, event)
    }//onKeyDown ends

    private fun initUIEvents() {
        azerActionBarBackIcon.setOnClickListener {
            if (UserDataManager.isChangeLanguageSuccess()) {
                UserDataManager.setChangeLanguageSuccess(false)
                MainActivity.start(applicationContext, "")
                finish()
            } else {
                finish()
            }
        }//azerActionBarBackIcon.setOnClickListener ends

        languageControlsHolder.setOnClickListener {
            showChangeLanguagePopup()
        }//languageControlsHolder.setOnClickListener ends

        radioToneView.setOnClickListener {
            if (!radioTone.isChecked) {
                if (getFCMDataFromLocalPreferences().data != null &&
                    hasValue(getFCMDataFromLocalPreferences().data.toString())
                    && hasValue(getFCMDataFromLocalPreferences().data.isEnable)
                ) {
                    getFCMToken(
                        ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_TONE,
                        getFCMDataFromLocalPreferences().data.isEnable!!,
                        ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_API_CAUSE_SETTINGS,
                        false,
                        true
                    )
                }
            }
        }//radioToneView.setOnClickListener ends

        radioVibrateView.setOnClickListener {
            if (!radioVibrate.isChecked) {
                if (getFCMDataFromLocalPreferences().data != null &&
                    hasValue(getFCMDataFromLocalPreferences().data.toString()) &&
                    hasValue(getFCMDataFromLocalPreferences().data.isEnable)
                ) {
                    getFCMToken(
                        ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_VIBRATE,
                        getFCMDataFromLocalPreferences().data.isEnable!!,
                        ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_API_CAUSE_SETTINGS,
                        false,
                        true
                    )
                }
            }
        }//radioVibrateView.setOnClickListener ends

        radioMuteView.setOnClickListener {
            if (!radioMute.isChecked) {
                if (getFCMDataFromLocalPreferences().data != null &&
                    hasValue(getFCMDataFromLocalPreferences().data.toString()) &&
                    hasValue(getFCMDataFromLocalPreferences().data.isEnable)
                ) {
                    getFCMToken(
                        ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_MUTE,
                        getFCMDataFromLocalPreferences().data.isEnable!!,
                        ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_API_CAUSE_SETTINGS,
                        false,
                        true
                    )
                }
            }
        }//radioMuteView.setOnClickListener  ends

        notificationsSwitch.setOnClickListener {
            logE(
                "clickSwitch",
                "clicked",
                fromClass,
                "notificationsSwitchLayout.setOnClickListener"
            )
            if (getFCMDataFromLocalPreferences().data != null &&
                hasValue(getFCMDataFromLocalPreferences().data.toString()) &&
                hasValue(getFCMDataFromLocalPreferences().data.isEnable) &&
                hasValue(getFCMDataFromLocalPreferences().data.ringingStatus)
            ) {
                notificationSwithOnOff(getFCMDataFromLocalPreferences())
            }
        }//notificationsSwitch.setOnClickListener ends

        notificationsSwitch.setOnTouchListener { _, event -> event!!.actionMasked == MotionEvent.ACTION_MOVE }

        passwordRow.setOnClickListener {
            ChangePasswordActivity.start(applicationContext)
        }//passwordRow.setOnClickListener ends
    }//initUIEvents ends

    private fun notificationSwithOnOff(fcmResponse: FCMResponse) {
        if (!notificationsSwitch.isChecked) {
            logE("clickSwitch", "isChecked", fromClass, "notificationSwithOnOff")
            getFCMToken(
                fcmResponse.data.ringingStatus!!,
                ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_OFF,
                ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_API_CAUSE_SETTINGS,
                false,
                true
            )
        } else {
            logE("clickSwitch", "isNotChecked", fromClass, "notificationSwithOnOff")
            getFCMToken(
                fcmResponse.data.ringingStatus!!,
                ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_ON,
                ConstantsUtility.FCMNotificationConfigurations.FCM_NOTIFICATION_API_CAUSE_SETTINGS,
                false,
                true
            )
        }
    }//notificationSwithOnOff ends

    @SuppressLint("InflateParams")
    private fun showChangeLanguagePopup() {
        if (this@SettingsActivity.isFinishing) {
            return//safe passage
        }
        val languagedialoge = Dialog(this)
        languagedialoge.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val inflater = layoutInflater
        val dialogView = inflater.inflate(R.layout.language_dialoge_layout, null)
        languagedialoge.setContentView(dialogView)
        if (languagedialoge.window != null) {
            languagedialoge.window!!.setBackgroundDrawable(null)
        }
        languagedialoge.setCancelable(true)
        languagedialoge.setCanceledOnTouchOutside(true)

        val radioGroup = dialogView.findViewById<RadioGroup>(R.id.radioGroup)
        setRadioGroup(radioGroup)
        setRadiosListener(radioGroup)

        val button = dialogView.findViewById<Button>(R.id.applyBtn)
        button.isSelected = true
        button.setOnClickListener {
            if (!selectedLanguage.equals(
                    ProjectApplication.mLocaleManager.getLanguage(),
                    ignoreCase = true
                )
            ) {
                var isUsageDetailsVerified = false
                isUsageDetailsVerified = UserDataManager.isUsageDetailsVerified()
                UserDataManager.resetUserDataManager()
                UserDataManager.setUsageDetailsVerified(isUsageDetailsVerified)
                UserDataManager.setChangeLanguageSuccess(true)
                ProjectApplication.mLocaleManager.setNewLocale(
                    this@SettingsActivity,
                    selectedLanguage
                )
                val intent = Intent(applicationContext, SettingsActivity::class.java)
                startActivity(intent)
                if (languagedialoge.isShowing) {
                    languagedialoge.dismiss()
                }
                finish()
            }
        }//button.setOnClickListener ends
        languagedialoge.show()
    }//showChangeLanguagePopup ends

    private fun setRadioGroup(radioGroup: RadioGroup) {
        val language = ProjectApplication.mLocaleManager.getLanguage()
        when (language) {
            ConstantsUtility.LanguageKeywords.EN -> radioGroup.findViewById<RadioButton>(R.id.en).isChecked =
                true
            ConstantsUtility.LanguageKeywords.AZ -> radioGroup.findViewById<RadioButton>(R.id.az).isChecked =
                true
            ConstantsUtility.LanguageKeywords.RU -> radioGroup.findViewById<RadioButton>(R.id.ru).isChecked =
                true
        }
    }//setRadioGroup ends

    private fun setRadiosListener(radioGroup: RadioGroup) {
        radioGroup.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(p0: RadioGroup?, checkedId: Int) {
                setUserSelectedLanguage(checkedId)
            }
        })
    }//setRadiosListener ends

    fun setUserSelectedLanguage(checkedID: Int) {
        when (checkedID) {
            R.id.en -> selectedLanguage = ConstantsUtility.LanguageKeywords.EN
            R.id.ru -> selectedLanguage = ConstantsUtility.LanguageKeywords.RU
            R.id.az -> selectedLanguage = ConstantsUtility.LanguageKeywords.AZ
        }
    }//setUserSelectedLangauge ends

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.settings_screen_title)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

}//class ends