package com.azarphone.ui.activities.tutorials

/**
 * @author Junaid Hassan on 20, June, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
object TutorialsInjection {

    fun provideTutorialsRepository(): TutorialsRepositry {
        return TutorialsRepositry()
    }

    fun provideTutorialsFactory(): TutorialsFactory {
        return TutorialsFactory(provideTutorialsRepository())
    }

}