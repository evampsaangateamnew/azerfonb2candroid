package com.azarphone.ui.activities.contactus

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.response.contactusresponse.ContactUsResponse
import com.azarphone.api.pojo.response.notification.NotificationResponse
import io.reactivex.Observable

class ContactUsRepositry {

    fun requestContactUsDetails(): Observable<ContactUsResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestContactUsDetails()
    }
}