package com.azarphone.ui.activities.specialoffers

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

/**
 * @author Junaid Hassan on 25, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class SpecialOffersFactory(val mSpecialOffersRepositry: SpecialOffersRepositry) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SpecialOffersViewModel::class.java)) {
            return SpecialOffersViewModel(mSpecialOffersRepositry) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}