package com.azarphone.ui.activities.fasttopup

object FastTopUpInjectionUtils {

    fun provideFastTopUpRepository(): FastTopUpRepository {
        return FastTopUpRepository()
    }

    fun provideFastTopUpFactory(): FastTopUpFactory {
        return FastTopUpFactory(provideFastTopUpRepository())
    }
}