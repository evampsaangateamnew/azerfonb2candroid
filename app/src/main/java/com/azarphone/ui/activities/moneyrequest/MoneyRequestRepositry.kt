package com.azarphone.ui.activities.moneyrequest

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.RequestMoneyRequest
import com.azarphone.api.pojo.response.requestmoneyresponse.RequestMoneyResponse
import io.reactivex.Observable


/**
 * @author Junaid Hassan on 08, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class MoneyRequestRepositry {
    init {

    }//init

    fun requestMoney(moneyTransferRequest: RequestMoneyRequest): Observable<RequestMoneyResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestMoneyRequest(moneyTransferRequest)
    }

}