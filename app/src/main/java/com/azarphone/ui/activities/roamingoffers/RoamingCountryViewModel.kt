package com.azarphone.ui.activities.roamingoffers

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.azarphone.R
import com.azarphone.analytics.Analytics
import com.azarphone.analytics.EventValues
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.request.SupplementaryOfferSubscribeRequest
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.mysubscriptions.successresponse.MySubscriptionsSuccessAfterOfferSubscribeResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.UpdateListOnSuccessfullySurvey
import com.azarphone.ui.dialogs.InAppThankYouDialog
import com.azarphone.util.ConstantsUtility
import com.azarphone.validators.hasValue
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

/**
 * @author Junaid Hassan on 24, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class RoamingCountryViewModel(val mRoamingCountryRepositry: RoamingCountryRepositry) : BaseViewModel() {

    private lateinit var disposable: Disposable
    val subscribeToRoamingOfferErrorResponseLiveData = MutableLiveData<AzerAPIErrorHelperResponse>()
    val subscribeToRoamingOfferOfferResponseLiveData = MutableLiveData<MySubscriptionsSuccessAfterOfferSubscribeResponse>()
    private val fromClass = "RoamingCountryViewModel"
    val inAppSurveyResponseErrorResponseData = MutableLiveData<AzerAPIErrorHelperResponse>()

    init {

    }//init ends
    fun uploadInAppSurvey(context: Context, uploadSurvey: UploadSurvey, onTransactionComplete: String, updateListOnSuccessfullySurvey: UpdateListOnSuccessfullySurvey?) {
        view()?.showLoading()
        var observable = mRoamingCountryRepositry.saveInAppSurvey(uploadSurvey)
        observable = observable.onErrorReturn { throwable -> null }
        disposable = observable
            .compose(applyIOSchedulers())
            .subscribe(
                { appSurvey ->
                    view()?.hideLoading()
                    when {
                        appSurvey?.data != null -> {
                            UserDataManager.setInAppSurvey(appSurvey)
                            val thankYouDialog =
                                InAppThankYouDialog(context, onTransactionComplete)
                            thankYouDialog.show()
                        }
                        appSurvey?.resultDesc != null -> {
                            inAppSurveyResponseErrorResponseData.postValue(
                                AzerAPIErrorHelperResponse(
                                    ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                    appSurvey.resultDesc
                                )
                            )
                        }
                        else -> {
                            inAppSurveyResponseErrorResponseData.postValue(
                                AzerAPIErrorHelperResponse(
                                    ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                    context.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            )
                        }
                    }
                    updateListOnSuccessfullySurvey?.updateListOnSuccessfullySurvey()
                },
                { error ->
                    inAppSurveyResponseErrorResponseData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            error.message
                        )
                    )
                }
            )
        compositeDisposables?.add(disposable)
    }


    fun subscribeToSupplementaryOffer(offeringName: String, offeringId: String, actionType: String) {
        view()?.showLoading()
        val supplementaryOfferSubscribeRequest = SupplementaryOfferSubscribeRequest(offeringName, offeringId, actionType)

        var mObserver = mRoamingCountryRepositry.subscribeToSupplementaryOffer(supplementaryOfferSubscribeRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            parseSubscribeToSupplementaryOfferResponseLiveData(result)
                            Analytics.logAppEvent(EventValues.RoamingSubscriptionEvents.roaming_subscription, EventValues.RoamingSubscriptionEvents.subscription, EventValues.GenericValues.Success)
                        },
                        { error ->
                            view()?.hideLoading()
                            subscribeToRoamingOfferErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                            Analytics.logAppEvent(EventValues.RoamingSubscriptionEvents.roaming_subscription, EventValues.RoamingSubscriptionEvents.subscription, EventValues.GenericValues.Failure)
                        }
                )

        compositeDisposables?.add(disposable)
    }//subscribeToSupplementaryOffer ends

    private fun parseSubscribeToSupplementaryOfferResponseLiveData(response: MySubscriptionsSuccessAfterOfferSubscribeResponse) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        subscribeToRoamingOfferOfferResponseLiveData.postValue(response)
                    } else {
                        /**the response has no data object
                         * in this case we have to handle the ui only*/
                        subscribeToRoamingOfferErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                    }
                } else {
                    /**the response has no data object
                     * in this case we have to handle the ui only*/
                    subscribeToRoamingOfferErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                }
            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                /**force log out user case
                 * when the server sends the result code as 7*/
                subscribeToRoamingOfferErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT, ""))
            } else {
                /**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*/
                if (hasValue(response.resultDesc.toString())) {
                    subscribeToRoamingOfferErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
                } else {
                    subscribeToRoamingOfferErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
                }
            }
        } else {
            /**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*/
            if (hasValue(response.resultDesc.toString())) {
                subscribeToRoamingOfferErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
            } else {
                subscribeToRoamingOfferErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }

}//class ends