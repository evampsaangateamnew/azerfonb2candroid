package com.azarphone.ui.activities.usagehistory

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.*
import com.azarphone.api.pojo.response.otpverifyresponse.OTPVerifyResponse
import com.azarphone.api.pojo.response.resendpin.ResendPinResponse
import com.azarphone.api.pojo.response.usagedetailsresponse.UsageDetailsResponse
import com.azarphone.api.pojo.response.usagehistoryresponse.UsageHistorySummaryResponse
import com.azarphone.api.pojo.response.verifypassportresponse.VerifyPassportResponse
import io.reactivex.Observable

/**
 * @author Junaid Hassan on 04, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class UsageHistoryRepositry {
    init {

    }

    fun requestUsageHistorySummary(paymentHistoryRequest: PaymentHistoryRequest): Observable<UsageHistorySummaryResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestUsageHistorySummary(paymentHistoryRequest)
    }

    fun requestVerifyPassportDetails(verifyPassportRequest: VerifyPassportRequest): Observable<VerifyPassportResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().verifyPassport(verifyPassportRequest)
    }

    fun resendPin(resendPinRequest: ResendPinRequest):Observable<ResendPinResponse>{
        return ApiClient.newApiClientInstance.getServerAPI().resendPin(resendPinRequest)
    }


    fun requestVerifyOTP(otpVerifyRequest: OTPVerifyRequest): Observable<OTPVerifyResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestVerifyOTP(otpVerifyRequest)
    }

    fun requestUsageDetails(usageDetailsRequest: UsageDetailsRequest): Observable<UsageDetailsResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().getUsageDetails(usageDetailsRequest)
    }

}