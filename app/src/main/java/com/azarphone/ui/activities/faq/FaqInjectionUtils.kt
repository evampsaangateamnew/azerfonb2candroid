package com.azarphone.ui.activities.faq

object FaqInjectionUtils{
    fun provideFaqRepository():FaqRepositry{
        return FaqRepositry()
    }

    fun provideFaqFactory():FaqFactory{
        return FaqFactory(provideFaqRepository())
    }
}