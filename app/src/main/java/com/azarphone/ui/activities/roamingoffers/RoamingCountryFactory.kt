package com.azarphone.ui.activities.roamingoffers

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

/**
 * @author Junaid Hassan on 24, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class RoamingCountryFactory(val mRoamingCountryRepositry: RoamingCountryRepositry) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(RoamingCountryViewModel::class.java)) {
            return RoamingCountryViewModel(mRoamingCountryRepositry) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}