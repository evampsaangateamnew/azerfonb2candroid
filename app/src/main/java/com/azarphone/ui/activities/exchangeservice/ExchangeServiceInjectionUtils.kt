package com.azarphone.ui.activities.exchangeservice

object ExchangeServiceInjectionUtils{
    fun provideExchangeServiceRepository():ExchangeServiceRepositry{
        return ExchangeServiceRepositry()
    }

    fun provideExchangeServiceFactory():ExchangeServiceFactory{
        return ExchangeServiceFactory(provideExchangeServiceRepository())
    }
}