package com.azarphone.ui.activities.signupactivity

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders

class SignUpFactory(val mSignUpRepository: SignUpRepository): ViewModelProvider.NewInstanceFactory(){

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        if(modelClass.isAssignableFrom(SignUpViewModel::class.java)){
            return SignUpViewModel(mSignUpRepository) as T
        }
        throw IllegalStateException("ViewModel Cannot be Casted")
    }

}