package com.azarphone.ui.activities.exchangeservice

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.ExchangeServiceRequest
import com.azarphone.api.pojo.request.ExchangeServiceValuesRequest
import com.azarphone.api.pojo.response.exchangeserviceresponse.ExchangeServiceResponse
import com.azarphone.api.pojo.response.exchangeservicesvaluesresponse.ExchangeServicesValuesResponse
import io.reactivex.Observable

class ExchangeServiceRepositry{


    init {

    }

    fun getExchangeServiceValues(ExchangeServicesValuesRequest: ExchangeServiceValuesRequest): Observable<ExchangeServicesValuesResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().getExchangeServicesValues(ExchangeServicesValuesRequest)
    }

    fun requestExchangeService(exchangeServiceRequest: ExchangeServiceRequest): Observable<ExchangeServiceResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestExchangeService(exchangeServiceRequest)
    }
}