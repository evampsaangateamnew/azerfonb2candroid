package com.azarphone.ui.activities.topupmenu

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.DeleteSavedCardRequest
import com.azarphone.api.pojo.request.InitiatePaymentRequest
import com.azarphone.api.pojo.request.MakePaymentRequest
import com.azarphone.api.pojo.request.TopupRequest
import com.azarphone.api.pojo.response.fasttopupresponse.deletefasttopup.DeleteItemResponse
import com.azarphone.api.pojo.response.makepayment.MakePaymentTopUpResponse
import com.azarphone.api.pojo.response.topup.TopupResponse
import com.azarphone.api.pojo.response.topup.initiatepayment.InitiatePaymentResponse
import com.azarphone.api.pojo.response.topup.savedcards.SavedCardsResponse
import io.reactivex.Observable

/**
 * @author Muhammad Ahmed on 14, July, 2021
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * muhammad.ahmed@evampsaanga.com
 */
class TopupMenuRepositry {
    init {

    }
}