package com.azarphone.ui.activities.contactus

object ContactUsInjectionUtils{
    fun provideContactUsRepository():ContactUsRepositry{
        return ContactUsRepositry()
    }

    fun provideContactUsFactory():ContactUsFactory{
        return ContactUsFactory(provideContactUsRepository())
    }
}