package com.azarphone.ui.activities.topupmenu

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.azarphone.ui.activities.mainactivity.MainRepositry

/**
 * @author Muhammad Ahmed on 14, July, 2021
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * muhammad.ahmed@evampsaanga.com
 */
class TopUpMenuFactory(val mTopupRepositry: TopupMenuRepositry) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TopUpMenuViewModel::class.java)) {
            return TopUpMenuViewModel(mTopupRepositry) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}