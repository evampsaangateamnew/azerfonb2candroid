package com.azarphone.ui.activities.storelocator

object StoreLocatorInjectionUtils{
    fun provideStoreLocatorRepository():StoreLocatorRepositry{
        return StoreLocatorRepositry()
    }

    fun provideStoreLocatorFactory():StoreLocatorFactory{
        return StoreLocatorFactory(provideStoreLocatorRepository())
    }
}