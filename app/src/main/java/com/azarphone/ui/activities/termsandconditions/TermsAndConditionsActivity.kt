package com.azarphone.ui.activities.termsandconditions

import android.content.Context
import android.content.Intent
import android.view.View
import com.azarphone.R
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.LayoutTermsConditionsBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.util.RootValues
import com.azarphone.validators.hasValue
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.layout_terms_conditions.*

class TermsAndConditionsActivity : BaseActivity<LayoutTermsConditionsBinding, TermsAndConditionsFactory, TermsAndConditionsViewModel>() {
    override fun getLayoutId(): Int {
        return R.layout.layout_terms_conditions
    }//getLayoutId ends

    override fun getViewModelClass(): Class<TermsAndConditionsViewModel> {
        return TermsAndConditionsViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): TermsAndConditionsFactory {
        return TermsAndConditionsInjectionUtils.provideTermsFactory()
    }//getFactory ends

    override fun init() {
        setupHeaderActionBar()
        initUIEvents()
        showData()
    }

    private fun showData() {
            if (UserDataManager.getTermsAndConditionsDataString() != null) {
                if (hasValue(UserDataManager.getTermsAndConditionsDataString())) {

                    var htmlText =  "<html><body><style type='text/css'>@font-face {font-family: 'ALSSchlangesans';src: url('file:///android_asset/fonts/af/alsschlangesans.otf')} body {font-family: 'ALSSchlangesans';}</style>"+UserDataManager.getTermsAndConditionsDataString()+"</body></html>";

                    termsConditionsWebView.settings.javaScriptEnabled = true
                    termsConditionsWebView.loadDataWithBaseURL("", htmlText, "text/html", "UTF-8", "")
                    dataGroup.visibility = View.VISIBLE
                    noDataFoundLayout.visibility = View.GONE
                } else {
                    dataGroup.visibility = View.GONE
                    noDataFoundLayout.visibility = View.VISIBLE
                }
            } else {
                dataGroup.visibility = View.GONE
                noDataFoundLayout.visibility = View.VISIBLE
            }

    }

    private fun initUIEvents() {
        azerActionBarBackIcon.setOnClickListener {
            onBackPressed()
        }
    }

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.update_password_terms_conditions_label)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, TermsAndConditionsActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }
    }
}