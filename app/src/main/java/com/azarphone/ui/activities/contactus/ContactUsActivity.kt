package com.azarphone.ui.activities.contactus

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import com.azarphone.R
import com.azarphone.analytics.Analytics
import com.azarphone.analytics.EventValues
import com.azarphone.api.pojo.response.contactusresponse.ContactUsResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.FragmentContactusBinding
import com.azarphone.util.getContactUsFromLocalPreferences
import com.azarphone.util.isNetworkAvailable
import com.azarphone.util.logE
import com.azarphone.util.setContactUsDataToLocalPreferences
import com.azarphone.validators.hasValue
import com.azarphone.widgets.popups.showMessageDialog
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.fragment_contactus.*


class ContactUsActivity : BaseActivity<FragmentContactusBinding, ContactUsFactory, ContactUsViewModel>() {

    private val fromClass = "ContactUsActivity"
    private val logKey = "ContactUsLogs"
    private var contactUsResponse: ContactUsResponse? = null

    override fun getLayoutId(): Int {
        return R.layout.fragment_contactus
    }//getLayoutId ends

    override fun getViewModelClass(): Class<ContactUsViewModel> {
        return ContactUsViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): ContactUsFactory {
        return ContactUsInjectionUtils.provideContactUsFactory()
    }//getFactory ends

    override fun init() {
        setupHeaderActionBar()
        subscribe()
        initUIEvents()
        /**get the data from the local preferences*/
        val cachedContactUs = getContactUsFromLocalPreferences()
        if (cachedContactUs.data != null && hasValue(cachedContactUs.data.toString())) {
            contactUsResponse = cachedContactUs
            setContactUsDataToLocalPreferences(contactUsResponse!!)
            processContactUsUI(contactUsResponse!!)
            noDataLayout.visibility = View.GONE
            dataLayout.visibility = View.VISIBLE

            /**call the api in the background*/
            mViewModel.requestContactUsDetailsBackground()
        } else {
            noDataLayout.visibility = View.VISIBLE
            dataLayout.visibility = View.GONE
            if (isNetworkAvailable(applicationContext)) {
                mViewModel.requestContactUsDetails()
            } else {
                showMessageDialog(applicationContext, this@ContactUsActivity,
                        "", resources.getString(R.string.message_no_internet))
            }
        }

        sendLabel.isSelected = true
        inviteFriendsLabel.isSelected = true
    }

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.contactus_screen_title)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }

    private fun initUIEvents() {
        azerActionBarBackIcon.setOnClickListener {
            onBackPressed()
        }
    }

    private fun subscribe() {
        val contactUsResponseLiveData = object : Observer<ContactUsResponse> {
            override fun onChanged(t: ContactUsResponse?) {
                if (!this@ContactUsActivity.isFinishing) {
                    if (t != null) {
                        if (t.data != null) {
                            contactUsResponse = t
                            setContactUsDataToLocalPreferences(contactUsResponse!!)
                            processContactUsUI(contactUsResponse!!)
                            noDataLayout.visibility = View.GONE
                            dataLayout.visibility = View.VISIBLE
                        } else {
                            noDataLayout.visibility = View.VISIBLE
                            dataLayout.visibility = View.GONE
                        }
                    } else {
                        noDataLayout.visibility = View.VISIBLE
                        dataLayout.visibility = View.GONE
                    }

                }
            }
        }

        mViewModel.contactUsResponseLiveData.observe(this@ContactUsActivity, contactUsResponseLiveData)

        val contactUsResponseBackgroundLiveData = object : Observer<ContactUsResponse> {
            override fun onChanged(t: ContactUsResponse?) {
                if (!this@ContactUsActivity.isFinishing) {
                    contactUsResponse = t
                    setContactUsDataToLocalPreferences(contactUsResponse!!)
                    processContactUsUI(contactUsResponse!!)
                    noDataLayout.visibility = View.GONE
                    dataLayout.visibility = View.VISIBLE
                }
            }
        }

        mViewModel.contactUsResponseBackgroundLiveData.observe(this@ContactUsActivity, contactUsResponseBackgroundLiveData)
    }//subscribe ends

    private fun processContactUsUI(response: ContactUsResponse) {
        if (response.data.address != null &&
                hasValue(response.data.address)) {
            addressLabel.text = response.data.address
            addressLabel.isSelected = true
        }

        if (response.data.customerCareNo != null &&
                hasValue(response.data.customerCareNo)) {
            azerNumber.text = response.data.customerCareNo
            azerNumber.isSelected = true
        }

        if (response.data.phone != null &&
                hasValue(response.data.phone)) {
            azerLandlineNumberLabel.text = response.data.phone
            azerLandlineNumberLabel.isSelected = true
        }

        if (response.data.email != null &&
                hasValue(response.data.email)) {
            azerEmailLabel.text = response.data.email
            azerEmailLabel.isSelected = true
        }

        if (response.data.website != null &&
                hasValue(response.data.website)) {
            azerWebsiteLabel.text = response.data.website
            azerWebsiteLabel.isSelected = true
        }

        azerNumber.setOnClickListener {
            if (response.data.customerCareNo != null &&
                    hasValue(response.data.customerCareNo)) {
                openDialer(Uri.parse("tel:" + response.data.customerCareNo))
            } else {
                Toast.makeText(this@ContactUsActivity, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show()
            }
        }

        azerLandlineNumberLabel.setOnClickListener {
            if (response.data.phone != null &&
                    hasValue(response.data.phone)) {
                openDialer(Uri.parse("tel:" + response.data.phone))
            } else {
                Toast.makeText(this@ContactUsActivity, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show()
            }
        }

        azerEmailLabel.setOnClickListener {
            if (response.data.email != null &&
                    hasValue(response.data.email)) {
                composeEmail(arrayOf(response.data.email!!), "")
            } else {
                Toast.makeText(this@ContactUsActivity, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show()
            }
        }

        sendLabel.setOnClickListener {
            if (response.data.inviteFriendText != null &&
                    hasValue(response.data.inviteFriendText)) {
                Analytics.logAppEvent(EventValues.RedirectEvents.share, EventValues.RedirectEvents.redirected_to_invite, EventValues.GenericValues.Success)
                val sharingIntent = Intent(Intent.ACTION_SEND)
                sharingIntent.type = "text/plain"
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, R.string.contactus_invite_friends)
                sharingIntent.putExtra(Intent.EXTRA_TEXT, response.data.inviteFriendText)
                startActivity(Intent.createChooser(sharingIntent, getString(R.string.contactus_social_media_label)))
            }
        }//externalView!!.sendLabel.setOnClickListener  ends

        azerFacebookIcon.setOnClickListener {
            if (response.data.facebookLink != null &&
                    hasValue(response.data.facebookLink)) {
                openBrowser(Uri.parse(response.data.facebookLink.toString().trim()))
            } else {
                Toast.makeText(this@ContactUsActivity, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show()
            }
        }//externalView!!.facebookLayout.setOnClickListener ends

        azerInstagramIcon.setOnClickListener {
            if (response.data.instagram != null &&
                    hasValue(response.data.instagram)) {
                openBrowser(Uri.parse(response.data.instagram.toString().trim()))
            } else {
                Toast.makeText(this@ContactUsActivity, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show()
            }
        }//externalView!!.azerInstagramIcon.setOnClickListener ends

        azerYoutubeIcon.setOnClickListener {
            if (response.data.youtubeLink != null &&
                    hasValue(response.data.youtubeLink)) {
                openBrowser(Uri.parse(response.data.youtubeLink.toString().trim()))
            } else {
                Toast.makeText(this@ContactUsActivity, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show()
            }
        }//externalView!!.azerYoutubeIcon.setOnClickListener ends

        azerTwitterIcon.setOnClickListener {
            if (response.data.twitterLink != null &&
                    hasValue(response.data.twitterLink)) {
                openBrowser(Uri.parse(response.data.twitterLink.toString().trim()))
            } else {
                Toast.makeText(this@ContactUsActivity, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show()
            }
        }//externalView!!.azerTwitterIcon.setOnClickListener ends

        azerLinkedInIcon.setOnClickListener {
            if (response.data.linkedinLink != null && hasValue(response.data.linkedinLink)) {
                openBrowser(Uri.parse(response.data.linkedinLink.toString().trim()))
            } else {
                Toast.makeText(this@ContactUsActivity, R.string.error_message_no_customer_data, Toast.LENGTH_SHORT).show()
            }
        }//azerLinkedInIcon.setOnClickListener ends
    }//processContactUsUI ends

    private fun openBrowser(parse: Uri?) {
        if (parse == null) return
        try {
            val i = Intent(Intent.ACTION_VIEW, parse)
            startActivity(i)
        } catch (e: Exception) {
            logE(logKey, "error:::".plus(e.message), fromClass, "openBrowser")
        }
    }//openBrowser ends

    private fun composeEmail(addresses: Array<String>, subject: String) {
        try {
            val intent = Intent(Intent.ACTION_SENDTO)
            intent.data = Uri.parse("mailto:") // only email apps should handle this
            intent.putExtra(Intent.EXTRA_EMAIL, addresses)
            intent.putExtra(Intent.EXTRA_SUBJECT, subject)
            startActivity(Intent.createChooser(intent, getString(R.string.contactus_send_button_label)))
        } catch (e: Exception) {
            logE(logKey, "error:::".plus(e.message), fromClass, "composeEmail")
        }
    }//composeEmail ends

    private fun openDialer(parse: Uri) {
        try {
            val intent = Intent(Intent.ACTION_DIAL, parse)
            startActivity(intent)
        } catch (e: Exception) {
            logE(logKey, "error:::".plus(e.message), fromClass, "openDialer")
        }
    }//openDialer ends

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, ContactUsActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }
    }


}
