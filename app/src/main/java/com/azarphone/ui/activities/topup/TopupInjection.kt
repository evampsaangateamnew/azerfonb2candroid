package com.azarphone.ui.activities.topup

/**
 * @author Junaid Hassan on 03, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
object TopupInjection {

    fun provideTopupRepository(): TopupRepositry {
        return TopupRepositry()
    }

    fun provideTopupFactory(): TopupFactory {
        return TopupFactory(provideTopupRepository())
    }

}