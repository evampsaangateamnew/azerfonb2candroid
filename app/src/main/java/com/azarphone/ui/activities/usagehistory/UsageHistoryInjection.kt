package com.azarphone.ui.activities.usagehistory

/**
 * @author Junaid Hassan on 04, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
object UsageHistoryInjection {

    fun provideUsageHistoryRepository(): UsageHistoryRepositry {
        return UsageHistoryRepositry()
    }

    fun provideUsageHistoryFactory(): UsageHistoryFactory {
        return UsageHistoryFactory(provideUsageHistoryRepository())
    }

}