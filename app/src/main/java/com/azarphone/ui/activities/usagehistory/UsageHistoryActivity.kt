package com.azarphone.ui.activities.usagehistory

import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.azarphone.R
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.LayoutUsageHistoryBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.ui.adapters.pagers.UsageHistoryPagerAdapter
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.layout_usage_history.*


/**
 * @author Junaid Hassan on 04, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class UsageHistoryActivity : BaseActivity<LayoutUsageHistoryBinding, UsageHistoryFactory, UsageHistoryViewModel>() {

    private val fromClass = "UsageHistoryActivity"

    override fun getLayoutId(): Int {
        return R.layout.layout_usage_history
    }

    override fun getViewModelClass(): Class<UsageHistoryViewModel> {
        return UsageHistoryViewModel::class.java
    }

    override fun getFactory(): UsageHistoryFactory {
        return UsageHistoryInjection.provideUsageHistoryFactory()
    }

    override fun init() {
        setupHeaderActionBar()
        UserDataManager.setUsageDetailsCalled(false)
        initUi()
        initUiEvents()
        setupTabLayout()

        val isCorporateCustomer = (UserDataManager.getCustomerData() != null && hasValue(UserDataManager.getCustomerData()!!.customerType)
                && (UserDataManager.getCustomerData()!!.customerType!!.equals(ConstantsUtility.UserConstants.CORPORATE_CUSTOMER, ignoreCase = true)) ||
                (UserDataManager.getCustomerData()!!.customerType!!.equals(ConstantsUtility.UserConstants.CUSTOMER_TYPE_DATA_SIM_POSTPAID_CORPORATE, ignoreCase = true)))

        //setup viewPager
        usageHistoryViewpager.adapter = UsageHistoryPagerAdapter(supportFragmentManager, "", isCorporateCustomer)

        val limit = if (UsageHistoryPagerAdapter(supportFragmentManager, "", isCorporateCustomer).count > 1) UsageHistoryPagerAdapter(supportFragmentManager, "", isCorporateCustomer).count - 1 else 1
        usageHistoryViewpager.offscreenPageLimit = limit
    }//init ends

    private fun setupTabLayout() {
        val isCorporateCustomer = (UserDataManager.getCustomerData() != null && hasValue(UserDataManager.getCustomerData()!!.customerType)
                && (UserDataManager.getCustomerData()!!.customerType!!.equals(ConstantsUtility.UserConstants.CORPORATE_CUSTOMER, ignoreCase = true) ||
                (UserDataManager.getCustomerData()!!.customerType!!.equals(ConstantsUtility.UserConstants.CUSTOMER_TYPE_DATA_SIM_POSTPAID_CORPORATE, ignoreCase = true))))

        if (isCorporateCustomer) {
            //do nothing, just show summary
            tabLayout.visibility = View.GONE
        } else {
            tabLayout.visibility = View.VISIBLE
            tabLayout.addTab(tabLayout.newTab().setText(resources.getString(R.string.usage_history_summary_tab)))
            tabLayout.addTab(tabLayout.newTab().setText(resources.getString(R.string.usage_history_details_tab)))
        }

        if (tabLayout != null && tabLayout.tabCount > 0) {
            //set the first tab by default
            tabLayout.getTabAt(0)!!.select()

            for (i in 0 until tabLayout.tabCount) {
                val tab = (tabLayout.getChildAt(0) as ViewGroup).getChildAt(i)
                val p = tab.layoutParams as ViewGroup.MarginLayoutParams
                val margin = (15 * resources.displayMetrics.density)
                p.setMargins(0, 0, margin.toInt(), 0)
                tab.requestLayout()

                //set the custom view
                val tabView = tabLayout.getTabAt(i)
                if (tabView != null) {
                    val tabTextView = TextView(this)
                    tabView.customView = tabTextView
                    //set the text
                    tabTextView.text = tabView.text
                    tabTextView.ellipsize = TextUtils.TruncateAt.MARQUEE
                    tabTextView.isSelected = true
                    tabTextView.marqueeRepeatLimit = -1
                    tabTextView.isSingleLine = true
                    tabTextView.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                    tabTextView.gravity = Gravity.CENTER
                    //set the currently selected tab font
                    if (i == 0) {
                        setSelectedTabFont(applicationContext, tabTextView)
                    } else {
                        setUnSelectedTabFont(applicationContext, tabTextView)
                    }
                }
            }
        }
    }//setupTabLayout ends

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.usage_history_title)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

    override fun onBackPressed() {
        super.onBackPressed()
        UserDataManager.setUsageDetailsCalled(false)
    }

    private fun initUiEvents() {
        azerActionBarBackIcon.setOnClickListener {
            UserDataManager.setUsageDetailsCalled(false)
            onBackPressed()
        }//azerActionBarBackIcon.setOnClickListener ends

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                if (tab != null && tab.customView != null) {
                    val tabTextView = tab.customView as TextView
                    setUnSelectedTabFont(applicationContext, tabTextView)
                }
            }//onTabUnselected ends

            override fun onTabSelected(tab: TabLayout.Tab?) {
                usageHistoryViewpager.setCurrentItem(tab!!.position, false)
                if (tab.customView != null) {
                    val tabTextView = tab.customView as TextView
                    setSelectedTabFont(applicationContext, tabTextView)
                }

                logE("ViewPaX", "position:::".plus(usageHistoryViewpager.currentItem), fromClass, "onTabSelected")

                if (usageHistoryViewpager.currentItem == 1) {
                    /**on details screen*/
                    if (UserDataManager.isUsageDetailsVerified()) {
                        /**otp is already being verified*/
                        if (!UserDataManager.isUsageDetailsCalled()) {
                            /**user has not called the usage details yet*/
                            onCreateUsageHistoryDetailsProcess()
                        }
                    }
                }
            }//onTabSelected ends
        })//tabLayout.addOnTabSelectedListener ends
    }//initUiEvents ends

    private fun onCreateUsageHistoryDetailsProcess() {
        try {
            //get the date
            val date = getLastDateMinusDays(getCurrentDate(), ConstantsUtility.DateFormats.CURRENT_DAY)
            updateUsageDetailsHistory(date, getCurrentDate())
        } catch (exp: Exception) {
            logE("On&8", "error:::".plus(exp.toString()), fromClass, "initUI")
        }
    }

    private fun updateUsageDetailsHistory(newStartDate: String, newEndDate: String) {
        var formattedStartedDate = ""
        if (hasValue(changeDateFormat(newStartDate))) {
            formattedStartedDate = changeDateFormat(newStartDate)!!
        }

        var formattedEndDate = ""
        if (hasValue(changeDateFormat(newEndDate))) {
            formattedEndDate = changeDateFormat(newEndDate)!!
        }

        var accountIdId = ""
        if (UserDataManager.getCustomerData() != null && hasValue(UserDataManager.getCustomerData()!!.accountId)) {
            accountIdId = UserDataManager.getCustomerData()!!.accountId!!
        }

        var customerId = ""
        if (UserDataManager.getCustomerData() != null && hasValue(UserDataManager.getCustomerData()!!.customerId)) {
            customerId = UserDataManager.getCustomerData()!!.customerId!!
        }

        mViewModel.requestUsageHistoryDetails(formattedStartedDate, formattedEndDate, accountIdId, customerId)

    }

    private fun initUi() {

    }//initUi ends

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, UsageHistoryActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends
    }//object ends

}//class ends