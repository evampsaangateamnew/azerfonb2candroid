package com.azarphone.ui.activities.settings

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.AppResumeRequest
import com.azarphone.api.pojo.request.FCMRequest
import com.azarphone.api.pojo.response.fcmtoken.FCMResponse
import com.azarphone.api.pojo.response.loginappresumeresponse.LoginAppResumeResponse
import io.reactivex.Observable

/**
 * @author Junaid Hassan on 18, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class SettingsRepositry{
    init {

    }

    fun requestAppResume(appResumeRequest: AppResumeRequest): Observable<LoginAppResumeResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestAppResume(appResumeRequest)
    }//requestAppResume ends

    fun requestFCMTokenSentToServerService(fcmRequest: FCMRequest): Observable<FCMResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestFCMTokenSentToServerService(fcmRequest)
    }//requestFCMTokenSentToServerService ends
}