package com.azarphone.ui.activities.cardselection

import androidx.lifecycle.MutableLiveData
import com.azarphone.analytics.Analytics
import com.azarphone.analytics.EventValues
import com.azarphone.api.pojo.request.InitiatePaymentRequest
import com.azarphone.api.pojo.response.topup.initiatepayment.InitiatePaymentResponse
import com.azarphone.bases.BaseViewModel
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

class CardSelectionViewModel(val cardSelectionRepository: CardSelectionRepository) : BaseViewModel() {

    private lateinit var disposable: Disposable
    val initiatePaymentResponseLiveData = MutableLiveData<InitiatePaymentResponse>()

    fun requestInitiatePayment(initiatePaymentRequest: InitiatePaymentRequest) {
        view()?.showLoading()

        var mObserver = cardSelectionRepository.requestInitiatePayment(initiatePaymentRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    initiatePaymentResponseLiveData.postValue(result)
                                    Analytics.logAppEvent(EventValues.TopUpEvents.top_up, EventValues.TopUpEvents.nar_card, EventValues.GenericValues.Success)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                            Analytics.logAppEvent(EventValues.TopUpEvents.top_up, EventValues.TopUpEvents.nar_card, EventValues.GenericValues.Failure)
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestInitiatePayment ends

}