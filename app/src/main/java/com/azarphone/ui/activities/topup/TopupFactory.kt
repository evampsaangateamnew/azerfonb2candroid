package com.azarphone.ui.activities.topup

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

/**
 * @author Junaid Hassan on 03, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class TopupFactory(val mTopupRepositry: TopupRepositry) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TopupViewModel::class.java)) {
            return TopupViewModel(mTopupRepositry) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}