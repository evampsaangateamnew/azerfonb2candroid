package com.azarphone.ui.activities.topupmenu

/**
 * @author Muhammad Ahmed on 14, July, 2021
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * muhammad.ahmed@evampsaanga.com
 */
object TopUpMenuInjection {

    fun provideTopUpMenuRepository(): TopupMenuRepositry {
        return TopupMenuRepositry()
    }

    fun provideTopUpMenuFactory(): TopUpMenuFactory {
        return TopUpMenuFactory(provideTopUpMenuRepository())
    }

}