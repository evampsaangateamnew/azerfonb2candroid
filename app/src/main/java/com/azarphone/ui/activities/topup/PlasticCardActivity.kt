package com.azarphone.ui.activities.topup

import android.content.Context
import android.content.Intent
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.bases.BaseActivity
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.OnClickListener
import com.azarphone.util.logE
import com.azarphone.util.removeHomePageResponseFromLocalPreferences
import com.azarphone.validators.hasValue
import com.azarphone.widgets.popups.showMessageDialog
import com.azarphone.widgets.popups.showMessageDialogWithOkCallBack
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.layout_plastic_card_activity.*


class PlasticCardActivity : BaseActivity<com.azarphone.databinding.LayoutPlasticCardActivityBinding, TopupFactory, TopupViewModel>() {
    private val fromClass = "PlasticCard"
    override fun getLayoutId(): Int {
        return R.layout.layout_plastic_card_activity
    }//getLayoutId ends

    override fun getViewModelClass(): Class<TopupViewModel> {
        return TopupViewModel::class.java
    }//getViewModelClass ensd

    override fun getFactory(): TopupFactory {
        return TopupInjection.provideTopupFactory()
    }//getFactory ends

    override fun init() {
        /* val  mWebview =  WebView(this);
           mWebview.loadUrl("http://www.google.com");*/

        setupHeaderActionBar()
        initUiEvents()

        var url = intent.getStringExtra("paymentKey")

//        mViewModel.showLoader()

//        Analytics.logAppEvent(EventValues.TopUpEvents.top_up, EventValues.TopUpEvents.plastic_card_redirection, EventValues.GenericValues.Success)

        logE("WebViewUrl", url.toString(), fromClass, "init")
        //enable zoom view controls
        plastic_card_webview.resumeTimers() //resume the timers if needed by the DOM

        plastic_card_webview.settings.domStorageEnabled = true // the url specified need DOM kind a model
//        plastic_card_webview.isVerticalScrollBarEnabled = true
//        plastic_card_webview.isHorizontalScrollBarEnabled = true
        plastic_card_webview.settings.javaScriptEnabled = true
        plastic_card_webview.settings.setAppCacheEnabled(false) //for loading language based urls
        plastic_card_webview.settings.setSupportZoom(true);
        plastic_card_webview.settings.builtInZoomControls = true;
        plastic_card_webview.setInitialScale(100);

        plastic_card_webview.settings.cacheMode = WebSettings.LOAD_NO_CACHE //load no cache at all

        if (hasValue(url)) {
            logE("WebViewUrl", "url:::$url", "TopupPlasticCardActivity", "loadURL in webview")
//            url = "www.google.com"
            plastic_card_webview.loadUrl(url!!)
        }
    }

    private fun getLocalizedWebViewErrorLabel(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "Something went wrong during redirection. We apologize for any inconvenience."
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Yönləndirmə zamanı səhv baş verdi. Narahatçılığa görə üzr istəyirik."
            else -> "Произошла ошибка во время перенаправления. Приносим извинения за доставленные неудобства."
        }
    }

    private fun initUiEvents() {
        azerActionBarBackIcon.setOnClickListener { onBackPressed() }

        plastic_card_webview.webViewClient = object : WebViewClient() {

            override fun onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String) {
                mViewModel.hideLoader()
                logE("WebViewUrl", "Getting Error", fromClass, "onReceivedError")

                showMessageDialog(applicationContext, this@PlasticCardActivity, "", getLocalizedWebViewErrorLabel())
            }

            override fun onPageFinished(view: WebView, url: String) {
                logE("WebViewUrl", url, fromClass, "onPageFinished")

                mViewModel.hideLoader()
            }

            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {

                if (request != null && request.url?.toString()!!.contains("ok.jsp?")) {
                    //go Back and show success dialog
                    logE(
                        "WebViewUrl -> SUCCESS",
                        request.url?.toString()!!,
                        fromClass,
                        "shouldOverrideUrlLoading"
                    )
                    request.url.let {
                        view!!.loadUrl(it.toString())
                    }
                    showMessageDialogWithOkCallBack(this@PlasticCardActivity,
                        this@PlasticCardActivity,
                        getString(R.string.popup_ok),
                        getString(R.string.topup_plastic_card_successful_message),
                        object : OnClickListener {
                            override fun onClick() {
                                UserDataManager.setIsGetHomePageCalledWithSuccess(true)
                                removeHomePageResponseFromLocalPreferences()
                                finish()
                            }
                        })
                } else if (request != null && request.url?.toString()!!.contains("error.jsp?")) {
                    logE(
                        "WebViewUrl -> ERROR",
                        request.url?.toString()!!,
                        fromClass,
                        "shouldOverrideUrlLoading"
                    )
                    showMessageDialogWithOkCallBack(
                        this@PlasticCardActivity,
                        this@PlasticCardActivity,
                        getString(R.string.popup_error_title),
                        getString(R.string.topup_plastic_card_error_message),
                        object : OnClickListener {
                            override fun onClick() {
                                finish()
                            }
                        })
                    //goBack annd show something went wrong dialog
                } else {
                    request?.url?.toString()?.let {
                        view!!.loadUrl(it)
                    }
                }

                request?.url?.toString()?.let { logE("WebViewUrl", it, fromClass, "shouldOverrideUrlLoading") }
                return true
            }
        }
    }

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.topup_screen_title)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

    companion object {
        fun start(context: Context, paymentKey: String) {
            val intent = Intent(context, PlasticCardActivity::class.java)
            intent.putExtra("paymentKey", paymentKey)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends
    }
}