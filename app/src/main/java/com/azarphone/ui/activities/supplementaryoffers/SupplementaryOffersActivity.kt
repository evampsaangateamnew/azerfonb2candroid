package com.azarphone.ui.activities.supplementaryoffers

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.analytics.Analytics
import com.azarphone.analytics.EventValues
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.response.inappsurvey.InAppSurvey
import com.azarphone.api.pojo.response.inappsurvey.Surveys
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.mysubscriptions.successresponse.MySubscriptionsSuccessAfterOfferSubscribeResponse
import com.azarphone.api.pojo.response.supplementaryoffers.helper.OfferFiltersMain
import com.azarphone.api.pojo.response.supplementaryoffers.helper.SupplementaryOffer
import com.azarphone.api.pojo.response.supplementaryoffers.response.SupplementaryResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.LayoutSupplementaryOffersActivityBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.OnClickSurveySubmit
import com.azarphone.eventhandler.SupplementaryOffersCardAdapterEvents
import com.azarphone.eventhandler.SupplementaryOffersFilterEvents
import com.azarphone.eventhandler.UpdateListOnSuccessfullySurvey
import com.azarphone.ui.adapters.pagers.SupplementaryOffersPagerAdapter
import com.azarphone.ui.dialogs.InAppFeedbackDialog
import com.azarphone.ui.fragment.supplementaryoffers.SupplementaryOffersPaggerFragment
import com.azarphone.ui.fragment.supplementaryoffers.SupplementaryOffersRoamingFragment
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.widgets.popups.SupplementaryOffersFilterPopup
import com.azarphone.widgets.popups.showMessageDialog
import com.es.tec.LocalSharedPrefStorage
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.layout_supplementary_offers_activity.*
import java.util.*

/**
 * @author Junaid Hassan on 22, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class SupplementaryOffersActivity :
    BaseActivity<LayoutSupplementaryOffersActivityBinding, SupplementaryOffersFactory, SupplementaryOffersViewModel>() {

    private var actionIdFromServer = ""
    private var actionIdFromMySubscriptions = 1
    private var supplementaryOffersPagerAdapter: SupplementaryOffersPagerAdapter? = null
    private var redirectionIndex = 1
    private val DIALOG_FILTER = "supplementary.filter"
    private val fromClass = "SupplementaryOffersActivity"
    private var offerFilter: OfferFiltersMain? = null
    private var packagePlanModel: SupplementaryResponse? = null
    private var dashboardRedirectionKey = ""
    private val tabNames = ArrayList<String>()

    override fun getLayoutId(): Int {
        return R.layout.layout_supplementary_offers_activity
    }//getLayoutId ends

    override fun getViewModelClass(): Class<SupplementaryOffersViewModel> {
        return SupplementaryOffersViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): SupplementaryOffersFactory {
        return SupplementaryOffersInjection.provideSupplementaryOffersFactory()
    }//getFactory ends

    override fun init() {
        UserDataManager.getSupplementaryFilterKeysList().clear()//reset the filters
        getIntentBundleExtras()
        setupHeaderActionBar()
        initUIEvents()
        initFilterApplyEvents()
        initSupplementaryOffersCardAdapterEvents()
        subscribeToSupplementaryOffersResponseLiveData()
        /*subscribeToMySubscriptionsResponseLiveData()*/
        initUI()

        android.os.Handler().postDelayed({
            requestSupplementaryOffers()
        }, 50)

    }//init ends

    private fun getIntentBundleExtras() {
        intent?.extras?.let {
            if (it.containsKey(ConstantsUtility.PackagesConstants.DASHBOARD_INTENT_DATA)
                && hasValue(it.getString(ConstantsUtility.PackagesConstants.DASHBOARD_INTENT_DATA))
            ) {
                dashboardRedirectionKey =
                    it.getString(ConstantsUtility.PackagesConstants.DASHBOARD_INTENT_DATA)!!
            }

            if (it.containsKey(ConstantsUtility.PackagesConstants.NOTIFICATIONS_REDIRECTION_KEY)
                && hasValue(it.getString(ConstantsUtility.PackagesConstants.NOTIFICATIONS_REDIRECTION_KEY))
            ) {
                actionIdFromServer =
                    it.getString(ConstantsUtility.PackagesConstants.NOTIFICATIONS_REDIRECTION_KEY)!!
            }

            if (it.containsKey(ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA)
                && hasValue(it.getString(ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA))
            ) {
                actionIdFromServer =
                    it.getString(ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA)!!
            } else if (it.containsKey(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA)
                && hasValue(it.getString(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA))
            ) {
                dashboardRedirectionKey =
                    it.getString(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA)!!

            }

            if (it.containsKey(ConstantsUtility.MySubscriptionsConstants.REDIRECTION_INTENT_DATA_MYSUBSCRIPTION)) {
                actionIdFromMySubscriptions =
                    it.getInt(
                        ConstantsUtility.MySubscriptionsConstants.REDIRECTION_INTENT_DATA_MYSUBSCRIPTION,
                        0
                    )
            }
        }

        logE(
            ConstantsUtility.DynamicLinkConstants.DYNAMIC_LINK_TAG,
            "actionIdFromServer = $actionIdFromServer" +
                    "dashboardRedirectionKey = $dashboardRedirectionKey",
            fromClass, "getIntentBundleExtras"
        )
    }//getIntentBundleExtras ends

    @SuppressLint("InflateParams")
    private fun showSupplementaryOfferSubscribePopup(
        title: String,
        offeringName: String,
        offeringId: String,
        actionType: String
    ) {
        if (this@SupplementaryOffersActivity.isFinishing) return

        val dialogBuilder = AlertDialog.Builder(this@SupplementaryOffersActivity)
        val inflater =
            this@SupplementaryOffersActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val dialogView = inflater.inflate(R.layout.azer_vas_services_confirmation_popup, null)
        dialogBuilder.setView(dialogView)

        val alertDialog = dialogBuilder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.show()

        val popupTitle = dialogView.findViewById(R.id.popupTitle) as TextView
        popupTitle.typeface = getALSNormalFont()
        popupTitle.text = title
        val popupOkayButton = dialogView.findViewById(R.id.yesButton) as Button
        popupOkayButton.typeface = getALSBoldFont()
        val popupCancelButton = dialogView.findViewById(R.id.noButton) as Button
        popupCancelButton.typeface = getALSBoldFont()

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN) {
            //english
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "No"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ) {
            //azri
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "Xeyr"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.RU) {
            //russian
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "Нет"
        }

        popupOkayButton.setOnClickListener {
            alertDialog.dismiss()
            mViewModel.subscribeToSupplementaryOffer(offeringName, offeringId, actionType)
        }

        popupCancelButton.setOnClickListener {
            alertDialog.dismiss()
        }
    }

    private fun initSupplementaryOffersCardAdapterEvents() {
        val supplementaryOffersCardAdapterEvents = object : SupplementaryOffersCardAdapterEvents {
            override fun onSupplementaryOfferSubscribeButtonClick(
                titleMessage: String,
                offeringId: String,
                offeringName: String,
                actionType: String
            ) {
                if (!this@SupplementaryOffersActivity.isFinishing) {
                    logE(
                        "offerSubscribe",
                        "offername:::".plus(offeringName).plus(" actionType:::$actionType")
                            .plus(" offeringId:::").plus(offeringId),
                        fromClass,
                        "initSupplementaryOffersCardAdapterEvents"
                    )
                    /**call the api to subscribe the offer*/
                    showSupplementaryOfferSubscribePopup(
                        titleMessage,
                        offeringName,
                        offeringId,
                        actionType
                    )
                }
            }
        }

        RootValues.setSupplementaryOffersCardAdapterEvents(supplementaryOffersCardAdapterEvents)
        val inAppSurveyResponseErrorResponseData =
            Observer<AzerAPIErrorHelperResponse> { response ->
                if (!isNetworkAvailable(this@SupplementaryOffersActivity)) {
                    showMessageDialog(
                        this@SupplementaryOffersActivity, this@SupplementaryOffersActivity,
                        resources.getString(R.string.popup_note_title),
                        resources.getString(R.string.message_no_internet)
                    )
                } else {
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                        showMessageDialog(
                            this@SupplementaryOffersActivity, this@SupplementaryOffersActivity,
                            this@SupplementaryOffersActivity.resources.getString(R.string.popup_error_title),
                            response.errorDetail.toString()
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                        //force logout the user here
                        logOut(
                            this@SupplementaryOffersActivity,
                            this@SupplementaryOffersActivity,
                            false,
                            fromClass,
                            "initSupplementaryOffersCardAdapterEventsa34324"
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                        //handle the ui in this case
                        //hide or show the no data found layout in this case
                        showMessageDialog(
                            this@SupplementaryOffersActivity, this@SupplementaryOffersActivity,
                            this@SupplementaryOffersActivity.resources.getString(R.string.popup_error_title),
                            this@SupplementaryOffersActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                        showMessageDialog(
                            this@SupplementaryOffersActivity, this@SupplementaryOffersActivity,
                            this@SupplementaryOffersActivity.resources.getString(R.string.popup_error_title),
                            this@SupplementaryOffersActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    } else {
                        showMessageDialog(
                            this@SupplementaryOffersActivity, this@SupplementaryOffersActivity,
                            this@SupplementaryOffersActivity.resources.getString(R.string.popup_error_title),
                            this@SupplementaryOffersActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    }
                }
            }
        mViewModel.inAppSurveyResponseErrorResponseData.observe(
            this@SupplementaryOffersActivity,
            inAppSurveyResponseErrorResponseData
        )
        /** subscribe to the observers for the api called above*/
        val subscribeToSupplementaryOfferResponseLiveData =
            object : Observer<MySubscriptionsSuccessAfterOfferSubscribeResponse> {
                override fun onChanged(t: MySubscriptionsSuccessAfterOfferSubscribeResponse?) {
                    if (!this@SupplementaryOffersActivity.isFinishing) {
                        if (t != null && t.data != null && t.data.mySubscriptionsData != null) {

                            if (t.data.message != null && hasValue(t.data.message)) {
                                inAppFeedback(
                                    this@SupplementaryOffersActivity,
                                    t.data.message
                                )
                            }

                            /** update the mysubscriptions response*/
                            if (UserDataManager.getMySubscriptionResponse() != null) {
                                val mySubscriptions = UserDataManager.getMySubscriptionResponse()

                                if (mySubscriptions!!.data != null &&
                                    t.data.mySubscriptionsData.internet != null
                                ) {
                                    mySubscriptions.data!!.internet =
                                        t.data.mySubscriptionsData.internet
                                }

                                if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.sms != null
                                ) {
                                    mySubscriptions.data!!.sms = t.data.mySubscriptionsData.sms
                                }

                                if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.call != null
                                ) {
                                    mySubscriptions.data!!.call = t.data.mySubscriptionsData.call
                                }

                                if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.campaign != null
                                ) {
                                    mySubscriptions.data!!.campaign =
                                        t.data.mySubscriptionsData.campaign
                                }

                                if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.hybrid != null
                                ) {
                                    mySubscriptions.data!!.hybrid =
                                        t.data.mySubscriptionsData.hybrid
                                }

                                if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.voiceInclusiveOffers != null
                                ) {
                                    mySubscriptions.data!!.voiceInclusiveOffers =
                                        t.data.mySubscriptionsData.voiceInclusiveOffers
                                }

                                if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.roaming != null
                                ) {
                                    mySubscriptions.data!!.roaming =
                                        t.data.mySubscriptionsData.roaming
                                }

                                if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.smsInclusiveOffers != null
                                ) {
                                    mySubscriptions.data!!.smsInclusiveOffers =
                                        t.data.mySubscriptionsData.smsInclusiveOffers
                                }

                                if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.tm != null
                                ) {
                                    mySubscriptions.data!!.tm = t.data.mySubscriptionsData.tm
                                }

                                if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.internetInclusiveOffers != null
                                ) {
                                    mySubscriptions.data!!.internetInclusiveOffers =
                                        t.data.mySubscriptionsData.internetInclusiveOffers
                                }

                                UserDataManager.setMySubscriptionResponse(mySubscriptions)
                            }

                            /**notify the data adapter in each fragment of this activity*/
                            if (RootValues.getSupplementaryOffersSubscriptionEvents() != null) {
                                RootValues.getSupplementaryOffersSubscriptionEvents()!!
                                    .onSupplementaryOffersSubscriptionSuccess()
                            } else {
                                logE(
                                    "AdapterXu",
                                    "listener is null",
                                    fromClass,
                                    "initSupplementaryOffersCardAdapterEvents"
                                )
                            }
                        }
                    }
                }
            }

        mViewModel.subscribeToSupplementaryOfferResponseLiveData.observe(
            this@SupplementaryOffersActivity,
            subscribeToSupplementaryOfferResponseLiveData
        )

        val subscribeToSupplementaryOfferErrorResponseLiveData =
            object : Observer<AzerAPIErrorHelperResponse> {
                override fun onChanged(response: AzerAPIErrorHelperResponse?) {
                    if (!this@SupplementaryOffersActivity.isFinishing) {
                        //check internet
                        if (!isNetworkAvailable(this@SupplementaryOffersActivity)) {
                            showMessageDialog(
                                applicationContext, this@SupplementaryOffersActivity,
                                resources.getString(R.string.popup_note_title),
                                resources.getString(R.string.message_no_internet)
                            )
                        } else {
                            /**check the error code
                             * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                             * else show a general api failure popup*/
                            if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                                showMessageDialog(
                                    applicationContext, this@SupplementaryOffersActivity,
                                    this@SupplementaryOffersActivity.resources.getString(R.string.popup_error_title),
                                    response.errorDetail.toString()
                                )
                            } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                                //force logout the user here
                                logOut(
                                    applicationContext,
                                    this@SupplementaryOffersActivity,
                                    false,
                                    fromClass,
                                    "initSupplementaryOffersCardAdapterEventsa34324"
                                )
                            } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                                //handle the ui in this case
                                //hide or show the no data found layout in this case
                                showMessageDialog(
                                    applicationContext, this@SupplementaryOffersActivity,
                                    this@SupplementaryOffersActivity.resources.getString(R.string.popup_error_title),
                                    this@SupplementaryOffersActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                                showMessageDialog(
                                    applicationContext, this@SupplementaryOffersActivity,
                                    this@SupplementaryOffersActivity.resources.getString(R.string.popup_error_title),
                                    this@SupplementaryOffersActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            } else {
                                showMessageDialog(
                                    applicationContext, this@SupplementaryOffersActivity,
                                    this@SupplementaryOffersActivity.resources.getString(R.string.popup_error_title),
                                    this@SupplementaryOffersActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            }
                        }
                    }// if (activity != null && !this@SupplementaryOffersActivity.isFinishing)  ends
                }
            }

        mViewModel.subscribeToSupplementaryOfferErrorResponseLiveData.observe(
            this@SupplementaryOffersActivity,
            subscribeToSupplementaryOfferErrorResponseLiveData
        )
    }//initSupplementaryOffersCardAdapterEvents ends

    private fun inAppFeedback(context: Context, message: String) {

        var inAppFeedbackDialog: InAppFeedbackDialog? = null
        var currentVisit: Int =
            LocalSharedPrefStorage(this@SupplementaryOffersActivity).getInt(LocalSharedPrefStorage.PREF_BUNDLE_PAGE)
        currentVisit += 1
        val inAppSurvey: InAppSurvey? = UserDataManager.getInAppSurvey()
        if ((inAppSurvey?.data) != null) {
            val surveys: Surveys? =
                inAppSurvey.data.findSurvey(ConstantsUtility.InAppSurveyConstants.BUNDLE_PAGE)
            if (surveys != null) {
                inAppFeedbackDialog = InAppFeedbackDialog(context, object : OnClickSurveySubmit {
                    override fun onClickSubmit(
                        uploadSurvey: UploadSurvey,
                        onTransactionComplete: String,
                        updateListOnSuccessfullySurvey: UpdateListOnSuccessfullySurvey?
                    ) {
                        mViewModel.uploadInAppSurvey(
                            this@SupplementaryOffersActivity,
                            uploadSurvey,
                            onTransactionComplete,
                            updateListOnSuccessfullySurvey
                        )
                        if (inAppFeedbackDialog?.isShowing == true) {
                            inAppFeedbackDialog?.dismiss()
                        }
                    }

                })
                if (surveys.questions != null) {
                    currentVisit = 0
                    inAppFeedbackDialog.showTitleWithMessageDialog(surveys,resources.getString(R.string.popup_note_title),message)
                } else {
                    showMessageDialog(
                        applicationContext, this@SupplementaryOffersActivity,
                        resources.getString(R.string.popup_note_title),
                        message
                    )
                }
            } else {
                showMessageDialog(
                    applicationContext, this@SupplementaryOffersActivity,
                    resources.getString(R.string.popup_note_title),
                    message
                )
            }
        } else {
            showMessageDialog(
                applicationContext, this@SupplementaryOffersActivity,
                resources.getString(R.string.popup_note_title),
                message
            )
        }
        LocalSharedPrefStorage(this@SupplementaryOffersActivity).addInt(
            LocalSharedPrefStorage.PREF_BUNDLE_PAGE,
            currentVisit
        )
    }

    private fun initFilterApplyEvents() {
        val supplementaryOffersFilterEvents = object : SupplementaryOffersFilterEvents {
            override fun onFilterApply() {
                if (getString(R.string.supplementary_offers_internet_tab_title).equals(
                        getCurrentSelectedTabName(packagesViewpager.currentItem),
                        ignoreCase = true
                    )
                ) {
                    if (UserDataManager.getSupplementaryFilterKeysList().isNotEmpty()) {
                        val sb = StringBuilder()
                        val filterToApplyKey = "fromPopup"
                        for (its in UserDataManager.getSupplementaryFilterKeysList().indices) {
                            sb.append(UserDataManager.getSupplementaryFilterKeysList()[its])
                                .append("juni1289")
                        }

                        val searchFilter = filterToApplyKey.plus(sb)
                        onFilterCall(searchFilter)
                    } else {
                        onFilterCall("")
                    }
                }

                //dismiss the filter popup
                val prev = supportFragmentManager.findFragmentByTag(DIALOG_FILTER)
                if (prev != null) {
                    val df = prev as SupplementaryOffersFilterPopup
                    df.dismiss()
                }
            }
        }

        RootValues.setSupplementaryOffersFilterEvents(supplementaryOffersFilterEvents)
    }

    private fun subscribeToSupplementaryOffersResponseLiveData() {
        val supplementaryOffersResponseLiveData = object : Observer<SupplementaryResponse> {
            override fun onChanged(offers: SupplementaryResponse?) {
                if (!this@SupplementaryOffersActivity.isFinishing) {
                    if (offers != null && offers.data != null) {
                        logE(
                            "suppleOffers",
                            "response:::".plus(offers.toString()),
                            fromClass,
                            "subscribe"
                        )
                        packagePlanModel = null
                        packagePlanModel = offers
                        setupTabLayout(packagePlanModel!!)
                        initViewPager(packagePlanModel!!)
                        setSupplementaryOffersDataToLocalPreferences(offers)
                        showContent()
                    } else {
                        hideContent()
                    }
                }
            }
        }

        mViewModel.supplementaryOffersResponseLiveData.observe(
            this@SupplementaryOffersActivity,
            supplementaryOffersResponseLiveData
        )

        val supplementaryOffersBGResponseLiveData = object : Observer<SupplementaryResponse> {
            override fun onChanged(response: SupplementaryResponse?) {
                if (!this@SupplementaryOffersActivity.isFinishing) {
                    if (response != null && response.data != null) {
                        packagePlanModel = null
                        packagePlanModel = response
                        setSupplementaryOffersDataToLocalPreferences(response)
                        setupTabLayout(packagePlanModel!!)
                        initViewPager(packagePlanModel!!)
                    }
                }
            }
        }

        mViewModel.supplementaryOffersBGResponseLiveData.observe(
            this@SupplementaryOffersActivity,
            supplementaryOffersBGResponseLiveData
        )

        val supplementaryOffersErrorResponseLiveData =
            object : Observer<AzerAPIErrorHelperResponse> {
                override fun onChanged(response: AzerAPIErrorHelperResponse?) {
                    if (!this@SupplementaryOffersActivity.isFinishing) {
                        //check internet
                        hideContent()
                        if (!isNetworkAvailable(this@SupplementaryOffersActivity)) {
                            showMessageDialog(
                                applicationContext, this@SupplementaryOffersActivity,
                                resources.getString(R.string.popup_note_title),
                                resources.getString(R.string.message_no_internet)
                            )
                        } else {
                            /**check the error code
                             * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                             * else show a general api failure popup*/
                            if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                                showMessageDialog(
                                    applicationContext, this@SupplementaryOffersActivity,
                                    this@SupplementaryOffersActivity.resources.getString(R.string.popup_error_title),
                                    response.errorDetail.toString()
                                )
                            } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                                //force logout the user here
                                logOut(
                                    applicationContext,
                                    this@SupplementaryOffersActivity,
                                    false,
                                    fromClass,
                                    "subscribeToSupplementaryOffersResponseLiveData341231as"
                                )
                            } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                                //handle the ui in this case
                                //hide or show the no data found layout in this case
                                showMessageDialog(
                                    applicationContext, this@SupplementaryOffersActivity,
                                    this@SupplementaryOffersActivity.resources.getString(R.string.popup_error_title),
                                    this@SupplementaryOffersActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                                showMessageDialog(
                                    applicationContext, this@SupplementaryOffersActivity,
                                    this@SupplementaryOffersActivity.resources.getString(R.string.popup_error_title),
                                    this@SupplementaryOffersActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            } else {
                                showMessageDialog(
                                    applicationContext, this@SupplementaryOffersActivity,
                                    this@SupplementaryOffersActivity.resources.getString(R.string.popup_error_title),
                                    this@SupplementaryOffersActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            }
                        }
                    }// if (activity != null && !this@SupplementaryOffersActivity.isFinishing)  ends
                }
            }

        mViewModel.supplementaryOffersErrorResponseLiveData.observe(
            this@SupplementaryOffersActivity,
            supplementaryOffersErrorResponseLiveData
        )

    }//subscribe ends

    private fun initUI() {
        UserDataManager.getCountryRoamingDataList().clear()
    }//initUI ends

    private fun setupTabLayout(offers: SupplementaryResponse) {
        removeTabs()
        tabNames.clear()//refresh it everytime when this method called
        /**call tab*/
        if (offers.data.call != null) {
            tabLayout.addTab(
                tabLayout.newTab()
                    .setText(resources.getString(R.string.supplementary_offers_call_tab_title))
            )
            tabNames.add(resources.getString(R.string.supplementary_offers_call_tab_title))
        }

        /**internet tab*/
        if (offers.data.internet != null) {
            tabLayout.addTab(
                tabLayout.newTab()
                    .setText(resources.getString(R.string.supplementary_offers_internet_tab_title))
            )
            tabNames.add(resources.getString(R.string.supplementary_offers_internet_tab_title))
        }

        /**sms tab*/
        if (offers.data.sms != null) {
            tabLayout.addTab(
                tabLayout.newTab()
                    .setText(resources.getString(R.string.supplementary_offers_sms_tab_title))
            )
            tabNames.add(resources.getString(R.string.supplementary_offers_sms_tab_title))
        }

        /**hybrid tab*/
        if (offers.data.hybrid != null) {
            tabLayout.addTab(
                tabLayout.newTab()
                    .setText(resources.getString(R.string.supplementary_offers_hybrid_tab_title))
            )
            tabNames.add(resources.getString(R.string.supplementary_offers_hybrid_tab_title))
        }

        /**roaming tab*/
        if (offers.data.roaming != null) {
            tabLayout.addTab(
                tabLayout.newTab()
                    .setText(resources.getString(R.string.supplementary_offers_roaming_tab_title))
            )
            tabNames.add(resources.getString(R.string.supplementary_offers_roaming_tab_title))
        }

        /* */
        /**campaign tab*//*
        if (offers.data.campaign != null) {
            tabLayout.addTab(tabLayout.newTab().setText(resources.getString(R.string.supplementary_offers_campaign_tab_title)))
            tabNames.add(resources.getString(R.string.supplementary_offers_campaign_tab_title))
        }*/

        /**tm tab*//*
        if (offers.data.tm != null) {
            tabLayout.addTab(tabLayout.newTab().setText(resources.getString(R.string.supplementary_offers_tm_tab_title)))
            tabNames.add(resources.getString(R.string.supplementary_offers_tm_tab_title))
        }*/

        /**internetInclusiveOffers tab*//*
        if (offers.data.internetInclusiveOffers != null) {
            tabLayout.addTab(tabLayout.newTab().setText(resources.getString(R.string.supplementary_offers_internetInclusiveOffers_tab_title)))
            tabNames.add(resources.getString(R.string.supplementary_offers_internetInclusiveOffers_tab_title))
        }

        */
        /**voiceInclusiveOffers tab*//*
        if (offers.data.voiceInclusiveOffers != null) {
            tabLayout.addTab(tabLayout.newTab().setText(resources.getString(R.string.supplementary_offers_voiceInclusiveOffers_tab_title)))
            tabNames.add(resources.getString(R.string.supplementary_offers_voiceInclusiveOffers_tab_title))
        }

        */
        /**smsInclusiveOffers tab*//*
        if (offers.data.smsInclusiveOffers != null) {
            tabLayout.addTab(tabLayout.newTab().setText(resources.getString(R.string.supplementary_offers_smsInclusiveOffers_tab_title)))
            tabNames.add(resources.getString(R.string.supplementary_offers_smsInclusiveOffers_tab_title))
        }*/

        if (hasValue(dashboardRedirectionKey)) {
            if (dashboardRedirectionKey.equals(
                    ConstantsUtility.ResourceTypeConstants.RESOURCE_TYPE_DATA,
                    ignoreCase = true
                ) || dashboardRedirectionKey.equals(
                    ConstantsUtility.ResourceTypeConstants.RESOURCE_TYPE_INTERNET,
                    ignoreCase = true
                )
            ) {
                /**select internet tab*/
                redirectionIndex = 1
            } else if (dashboardRedirectionKey.equals(
                    ConstantsUtility.ResourceTypeConstants.RESOURCE_TYPE_SMS,
                    ignoreCase = true
                )
            ) {
                /**select sms tab*/
                redirectionIndex = 2
            } else if (dashboardRedirectionKey.equals(
                    ConstantsUtility.ResourceTypeConstants.RESOURCE_TYPE_BONUSES,
                    ignoreCase = true
                )
            ) {
                /**select sms tab*/
                redirectionIndex = 3
            } else if (dashboardRedirectionKey.equals(
                    ConstantsUtility.ResourceTypeConstants.RESOURCE_TYPE_ROAMING,
                    ignoreCase = true
                )
            ) {
                /**select sms tab*/
                redirectionIndex = 4
            } else {
                /**select call tab*/
                redirectionIndex = 0
            }
        } else {
            if (hasValue(actionIdFromServer) && !actionIdFromServer.equals("")) {
                redirectionIndex = 0
                redirectionIndex = getRedirectionIndexFromActionId(actionIdFromServer, offers)
            } else
                redirectionIndex = actionIdFromMySubscriptions
            //MEER
        }

        logE(
            ConstantsUtility.DynamicLinkConstants.DYNAMIC_LINK_TAG,
            "redirectionIndex = $redirectionIndex",
            fromClass, "setupTabLayout"
        )

        if (tabLayout.tabCount > 0) {
            //set the tab by index
            gotoTabByIndex(redirectionIndex)
        }

        for (i in 0 until tabLayout.tabCount) {
            val tab = (tabLayout.getChildAt(0) as ViewGroup).getChildAt(i)
            val p = tab.layoutParams as ViewGroup.MarginLayoutParams
            val margin = (15 * resources.displayMetrics.density)
            p.setMargins(0, 0, margin.toInt(), 0)
            tab.requestLayout()

            //set the custom view
            val tabView = tabLayout.getTabAt(i)
            if (tabView != null) {
                val tabTextView = TextView(this@SupplementaryOffersActivity)
                tabView.customView = tabTextView
                //set the text
                tabTextView.text = tabView.text
                //set the currently selected tab font
                if (redirectionIndex == i) {
                    setSelectedTabFont(applicationContext, tabTextView)
                } else {
                    setUnSelectedTabFont(applicationContext, tabTextView)
                }
            }
        }
    }//setupTabLayout ends

    private fun getRedirectionIndexFromActionId(
        offerId: String,
        offers: SupplementaryResponse
    ): Int {
        var index = 0
        if (offers.data.call != null && !offers.data.call!!.offers.isNullOrEmpty()) {
            for (its in offers.data.call!!.offers!!.indices) {
                if (offers.data.call!!.offers!![its]!!.header != null
                    && hasValue(offers.data.call!!.offers!![its]!!.header!!.offeringId)
                    && offers.data.call!!.offers!![its]!!.header!!.offeringId == offerId
                ) {
                    index = 0
                    break
                }
            }
        }

        if (offers.data.internet != null && !offers.data.internet!!.offers.isNullOrEmpty()) {
            for (its in offers.data.internet!!.offers!!.indices) {
                if (offers.data.internet!!.offers!![its]!!.header != null
                    && hasValue(offers.data.internet!!.offers!![its]!!.header!!.offeringId)
                    && offers.data.internet!!.offers!![its]!!.header!!.offeringId == offerId
                ) {
                    index = 1
                    break
                }
            }
        }

        if (offers.data.sms != null && !offers.data.sms!!.offers.isNullOrEmpty()) {
            for (its in offers.data.sms!!.offers!!.indices) {
                if (offers.data.sms!!.offers!![its]!!.header != null
                    && hasValue(offers.data.sms!!.offers!![its]!!.header!!.offeringId)
                    && offers.data.sms!!.offers!![its]!!.header!!.offeringId == offerId
                ) {
                    index = 2
                    break
                }
            }
        }

        if (offers.data.hybrid != null && !offers.data.hybrid!!.offers.isNullOrEmpty()) {
            for (its in offers.data.hybrid!!.offers!!.indices) {
                if (offers.data.hybrid!!.offers!![its]!!.header != null
                    && hasValue(offers.data.hybrid!!.offers!![its]!!.header!!.offeringId)
                    && offers.data.hybrid!!.offers!![its]!!.header!!.offeringId == offerId
                ) {
                    index = 3
                    break
                }
            }
        }

        return index
    }

    private fun removeTabs() {
        if (tabLayout != null) {
            tabLayout.removeAllTabs()
        }
    }//removeTabs ends

    private fun initViewPager(offers: SupplementaryResponse) {
        val paggerFragmentsList = ArrayList<androidx.fragment.app.Fragment>()
        var foundTabPosFromNotifications = -1

        if (tabLayout != null && tabLayout.tabCount > 0) {
            for (its in 0..tabLayout.tabCount) {
                if (getString(R.string.supplementary_offers_internet_tab_title).equals(
                        getCurrentSelectedTabName(its),
                        ignoreCase = true
                    )
                    && offers.data.internet != null
                    && offers.data.internet!!.offers != null
                ) {

                    val foundOfferItemFromNotifications =
                        findOfferIdAfterRedirectingFromNotifications(
                            actionIdFromServer,
                            offers.data.internet!!.offers as ArrayList<SupplementaryOffer?>
                        )
                    if (foundOfferItemFromNotifications != -1) {
                        foundTabPosFromNotifications = 0
                    }

                    logE("pacClick", "internet populated", fromClass, "initViewPager")
                    paggerFragmentsList.add(
                        SupplementaryOffersPaggerFragment.getInstance(
                            foundOfferItemFromNotifications,
                            getCurrentSelectedTabName(its),
                            offers.data.internet!!.offers!!
                        )
                    )
                } else if (getString(R.string.supplementary_offers_sms_tab_title).equals(
                        getCurrentSelectedTabName(its),
                        ignoreCase = true
                    )
                    && offers.data.sms != null
                    && offers.data.sms!!.offers != null
                ) {

                    val foundOfferItemFromNotifications =
                        findOfferIdAfterRedirectingFromNotifications(
                            actionIdFromServer,
                            offers.data.sms!!.offers as ArrayList<SupplementaryOffer?>
                        )
                    if (foundOfferItemFromNotifications != -1) {
                        foundTabPosFromNotifications = 0
                    }

                    logE("pacClick", "sms populated", fromClass, "initViewPager")
                    paggerFragmentsList.add(
                        SupplementaryOffersPaggerFragment.getInstance(
                            foundOfferItemFromNotifications,
                            getCurrentSelectedTabName(its),
                            offers.data.sms!!.offers!!
                        )
                    )
                } /*else if (getString(R.string.supplementary_offers_campaign_tab_title).equals(getCurrentSelectedTabName(its), ignoreCase = true)
                        && offers.data.campaign != null
                        && offers.data.campaign!!.offers != null) {

                    logE("pacClick", "campaign populated", fromClass, "initViewPager")
                    paggerFragmentsList.add(SupplementaryOffersPaggerFragment.getInstance(-1, getCurrentSelectedTabName(its), offers.data.campaign!!.offers!!))
                }*/ else if (getString(R.string.supplementary_offers_call_tab_title).equals(
                        getCurrentSelectedTabName(its),
                        ignoreCase = true
                    )
                    && offers.data.call != null
                    && offers.data.call!!.offers != null
                ) {

                    val foundOfferItemFromNotifications =
                        findOfferIdAfterRedirectingFromNotifications(
                            actionIdFromServer,
                            offers.data.call!!.offers as ArrayList<SupplementaryOffer?>
                        )
                    if (foundOfferItemFromNotifications != -1) {
                        foundTabPosFromNotifications = 0
                    }

                    logE("pacClick", "calls populated", fromClass, "initViewPager")
                    paggerFragmentsList.add(
                        SupplementaryOffersPaggerFragment.getInstance(
                            foundOfferItemFromNotifications,
                            getCurrentSelectedTabName(its),
                            offers.data.call!!.offers!!
                        )
                    )
                } /*else if (getString(R.string.supplementary_offers_tm_tab_title).equals(getCurrentSelectedTabName(its), ignoreCase = true)
                        && offers.data.tm != null
                        && offers.data.tm!!.offers != null) {

                    logE("pacClick", "tm populated", fromClass, "initViewPager")
                    paggerFragmentsList.add(SupplementaryOffersPaggerFragment.getInstance(-1, getCurrentSelectedTabName(its), offers.data.tm!!.offers!!))
                }*/ else if (getString(R.string.supplementary_offers_hybrid_tab_title).equals(
                        getCurrentSelectedTabName(its),
                        ignoreCase = true
                    )
                    && offers.data.hybrid != null
                    && offers.data.hybrid!!.offers != null
                ) {

                    val foundOfferItemFromNotifications =
                        findOfferIdAfterRedirectingFromNotifications(
                            actionIdFromServer,
                            offers.data.hybrid!!.offers as ArrayList<SupplementaryOffer?>
                        )
                    if (foundOfferItemFromNotifications != -1) {
                        foundTabPosFromNotifications = 0
                    }

                    logE("pacClick", "hybrid populated", fromClass, "initViewPager")
                    paggerFragmentsList.add(
                        SupplementaryOffersPaggerFragment.getInstance(
                            foundOfferItemFromNotifications,
                            getCurrentSelectedTabName(its),
                            offers.data.hybrid!!.offers!!
                        )
                    )
                } else if (getString(R.string.supplementary_offers_roaming_tab_title).equals(
                        getCurrentSelectedTabName(its),
                        ignoreCase = true
                    )
                    && offers.data.roaming != null
                    && offers.data.roaming!!.offers != null
                ) {

                    logE("pacClick", "roaming populated", fromClass, "initViewPager")
                    paggerFragmentsList.add(SupplementaryOffersRoamingFragment.getInstance(offers.data.roaming!!))
                } /*else if (getString(R.string.supplementary_offers_internetInclusiveOffers_tab_title).equals(getCurrentSelectedTabName(its), ignoreCase = true)
                        && offers.data.internetInclusiveOffers != null
                        && offers.data.internetInclusiveOffers!!.offers != null) {

                    logE("pacClick", "internetInclusiveOffers populated", fromClass, "initViewPager")
                    paggerFragmentsList.add(SupplementaryOffersPaggerFragment.getInstance(-1, getCurrentSelectedTabName(its), offers.data.internetInclusiveOffers!!.offers!!))
                } else if (getString(R.string.supplementary_offers_voiceInclusiveOffers_tab_title).equals(getCurrentSelectedTabName(its), ignoreCase = true)
                        && offers.data.voiceInclusiveOffers != null
                        && offers.data.voiceInclusiveOffers!!.offers != null) {

                    logE("pacClick", "voiceInclusiveOffers populated", fromClass, "initViewPager")
                    paggerFragmentsList.add(SupplementaryOffersPaggerFragment.getInstance(-1, getCurrentSelectedTabName(its), offers.data.voiceInclusiveOffers!!.offers!!))
                } else if (getString(R.string.supplementary_offers_smsInclusiveOffers_tab_title).equals(getCurrentSelectedTabName(its), ignoreCase = true)
                        && offers.data.smsInclusiveOffers != null
                        && offers.data.smsInclusiveOffers!!.offers != null) {

                    logE("pacClick", "smsInclusiveOffers populated", fromClass, "initViewPager")
                    paggerFragmentsList.add(SupplementaryOffersPaggerFragment.getInstance(-1, getCurrentSelectedTabName(its), offers.data.smsInclusiveOffers!!.offers!!))
                }*/
            }//for ends
        }//if(tabLayout!=null&&tabLayout.tabCount>0) ends

        supplementaryOffersPagerAdapter =
            SupplementaryOffersPagerAdapter(supportFragmentManager, paggerFragmentsList)
        packagesViewpager.adapter = supplementaryOffersPagerAdapter

        if (foundTabPosFromNotifications != -1 && hasValue(actionIdFromServer)) {
            redirectionIndex = foundTabPosFromNotifications
        }

        if (paggerFragmentsList.isNotEmpty()) {
            packagesViewpager.currentItem = redirectionIndex
        }

    }//initViewPager ends

    private fun gotoTabByIndex(index: Int) {
        if (tabLayout != null && tabLayout.tabCount > 0) {
            Handler().postDelayed({
                logE(
                    "gotoTabByIndex",
                    "index = $index, tabCount = " + tabLayout.tabCount,
                    fromClass,
                    "gotoTabByIndex"
                )

                tabLayout.getTabAt(index)!!.select()
                tabLayout.isSmoothScrollingEnabled = true
                tabLayout.setScrollPosition(index, 0f, true)
            }, 50L)
        }
    }//gotoTabByIndex ends

    private fun findOfferIdAfterRedirectingFromNotifications(
        offeringId: String,
        offersList: ArrayList<SupplementaryOffer?>
    ): Int {
        var foundOfferPosFromNotification = -1
        try {
            if (hasValue(offeringId) && !offersList.isNullOrEmpty()) {
                for (i in offersList.indices) {
                    if (offersList[i] != null &&
                        offersList[i]!!.header != null &&
                        hasValue(offersList[i]!!.header!!.offeringId) &&
                        offersList[i]!!.header!!.offeringId == offeringId
                    ) {
                        foundOfferPosFromNotification = i
                        break
                    }//if ends
                }//for ends
            }//outer if ends
        } catch (e: Exception) {
            logE(
                "Packages",
                "error:::".plus(e.toString()),
                "PackagesActivity",
                "findOfferIdAfterRedirectingFromNotifications"
            )
        }//catch ends
        logE(
            ConstantsUtility.DynamicLinkConstants.DYNAMIC_LINK_TAG,
            "foundOfferPosFromNotification = $foundOfferPosFromNotification",
            fromClass,
            "findOfferIdAfterRedirectingFromNotifications"
        )
        return foundOfferPosFromNotification
    }//findOfferIdAfterRedirectingFromNotifications ends

    private fun getCurrentSelectedTabName(index: Int): String {
        var tabName = ""

        try {
            tabName = tabNames[index]
        } catch (exp: Exception) {

        }

        return tabName
    }//getCurrentSelectedTabName ends

    private fun requestSupplementaryOffers() {
        /**check the response from the cache*/
        /* val cachedSupplementaryOffers = getSupplementaryOffersFromLocalPreferences()
         if (cachedSupplementaryOffers.data != null && hasValue(cachedSupplementaryOffers.data.toString())) {
             packagePlanModel = cachedSupplementaryOffers
             setSupplementaryOffersDataToLocalPreferences(packagePlanModel!!)
             setupTabLayout(packagePlanModel!!)
             initViewPager(packagePlanModel!!)
             showContent()

             */
        /**call the api in the background*//*
            callSupplementaryOffersAPIBackground()
        } else {
            hideContent()*/
        if (isNetworkAvailable(applicationContext)) {
            Handler().postDelayed({ callSupplementaryOffersAPI() }, 50L)
        } else {
            val cachedSupplementaryOffers = getSupplementaryOffersFromLocalPreferences()
            if (cachedSupplementaryOffers.data != null && hasValue(cachedSupplementaryOffers.data.toString())) {
                packagePlanModel = cachedSupplementaryOffers
                setSupplementaryOffersDataToLocalPreferences(packagePlanModel!!)
                setupTabLayout(packagePlanModel!!)
                initViewPager(packagePlanModel!!)
                showContent()
            } else {
                showMessageDialog(
                    applicationContext, this@SupplementaryOffersActivity,
                    "", resources.getString(R.string.message_no_internet)
                )
            }
        }
//        }
    }//requestSupplementaryOffers ends

    private fun callSupplementaryOffersAPIBackground() {
        var offeringName = ""
        var brandName = ""
        if (UserDataManager.getCustomerData() != null) {

            if (hasValue(UserDataManager.getCustomerData()!!.offeringId)) {
                offeringName = UserDataManager.getCustomerData()!!.offeringId!!
            }

            if (hasValue(UserDataManager.getCustomerData()!!.brandName)) {
                brandName = UserDataManager.getCustomerData()!!.brandName!!
            }
        }

        mViewModel.requestSupplementaryOffersBackground(offeringName, brandName)
    }//callSupplementaryOffersAPIBackground ends

    private fun callSupplementaryOffersAPI() {
        var offeringName = ""
        var brandName = ""
        if (UserDataManager.getCustomerData() != null) {

            if (hasValue(UserDataManager.getCustomerData()!!.offeringId)) {
                offeringName = UserDataManager.getCustomerData()!!.offeringId!!
            }

            if (hasValue(UserDataManager.getCustomerData()!!.brandName)) {
                brandName = UserDataManager.getCustomerData()!!.brandName!!
            }
        }

        mViewModel.requestSupplementaryOffers(offeringName, brandName)
    }//callSupplementaryOffersAPI ends

    private fun initUIEvents() {
        packagesViewpager.addOnPageChangeListener(object :
            androidx.viewpager.widget.ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {

            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {

            }

            override fun onPageSelected(position: Int) {

            }
        })
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                if (tab != null && tab.customView != null) {
                    val tabTextView = tab.customView as TextView
                    setUnSelectedTabFont(applicationContext, tabTextView)
                }
            }//onTabUnselected ends

            override fun onTabSelected(tab: TabLayout.Tab?) {
                searchCrossIcon?.callOnClick()
                packagesViewpager.setCurrentItem(tab!!.position, false)
                if (tab.customView != null) {
                    val tabTextView = tab.customView as TextView
                    setSelectedTabFont(applicationContext, tabTextView)
                }

                if (getCurrentSelectedTabName(tab.position).equals(
                        resources.getString(R.string.supplementary_offers_internet_tab_title),
                        ignoreCase = true
                    )
                ) {
                    packagesLayoutChangerIcon.visibility = View.VISIBLE
                    packagesSearchIcon.visibility = View.VISIBLE
                    packagesFilterIcon.visibility = View.VISIBLE
                } else if (getCurrentSelectedTabName(tab.position).equals(
                        resources.getString(R.string.supplementary_offers_roaming_tab_title),
                        ignoreCase = true
                    )
                ) {
                    packagesFilterIcon.visibility = View.GONE
                    packagesLayoutChangerIcon.visibility = View.GONE
                    packagesSearchIcon.visibility = View.GONE
                } else {
                    packagesFilterIcon.visibility = View.GONE
                    packagesLayoutChangerIcon.visibility = View.VISIBLE
                    packagesSearchIcon.visibility = View.VISIBLE
                }
            }//onTabSelected ends
        })//tabLayout.addOnTabSelectedListener ends

        packagesLayoutChangerIcon.setOnClickListener {
            if (RootValues.getOffersListOrientation() == LinearLayout.VERTICAL) {
                RootValues.setOffersListOrientation(LinearLayout.HORIZONTAL)
                packagesLayoutChangerIcon.setImageResource(R.drawable.ic_window)
            } else {
                RootValues.setOffersListOrientation(LinearLayout.VERTICAL)
                packagesLayoutChangerIcon.setImageResource(R.drawable.ic_packages_vertical)
            }

            if (packagePlanModel != null) {
                setupTabLayout(packagePlanModel!!)
                initViewPager(packagePlanModel!!)
            }
        }//packagesLayoutChangerIcon.setOnClickListener ends

        packagesSearchIcon.setOnClickListener {
            azerActionBarTitle.visibility = View.GONE
            searchCrossIcon.visibility = View.VISIBLE
            searchInputField.visibility = View.VISIBLE

            packagesSearchIcon.visibility = View.GONE
            packagesLayoutChangerIcon.visibility = View.GONE
            packagesFilterIcon.visibility = View.GONE
        }//packagesSearchIcon.setOnClickListener ends
        searchCrossIcon.setOnClickListener {
            azerActionBarTitle.visibility = View.VISIBLE
            searchCrossIcon.visibility = View.GONE
            searchInputField.visibility = View.GONE
            searchInputField.setText("")
            searchInputField.hint = resources.getString(R.string.supplementary_offers_search_hint)

            packagesSearchIcon.visibility = View.VISIBLE
            packagesLayoutChangerIcon.visibility = View.VISIBLE
            packagesFilterIcon.visibility = View.VISIBLE
        }//searchCrossIcon.setOnClickListener ends

        azerActionBarBackIcon.setOnClickListener {
            onBackPressed()
        }//azerActionBarBackIcon.setOnClickListener  ends

        packagesFilterIcon.setOnClickListener {
            showFilterAlert()
        }//packagesFilterIcon.setOnClickListener ends

        searchInputField.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }//afterTextChanged ends

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }//beforeTextChanged ends

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                Analytics.logSearchEvent(
                    EventValues.SearchEvents.search,
                    EventValues.SearchEvents.packages_screen,
                    s.toString()
                )
                if (getString(R.string.supplementary_offers_call_tab_title)
                        .equals(
                            getCurrentSelectedTabName(packagesViewpager.currentItem),
                            ignoreCase = true
                        )
                ) {
                    RootValues.getOfferFilterListenerCall()?.onFilterCall(s?.toString()?:"")
                } else if (getString(R.string.supplementary_offers_internet_tab_title)
                        .equals(
                            getCurrentSelectedTabName(
                                packagesViewpager
                                    .currentItem
                            ), ignoreCase = true
                        )
                ) {
                    onFilterCall(s.toString())
                } else if (getString(R.string.supplementary_offers_sms_tab_title)
                        .equals(
                            getCurrentSelectedTabName(
                                packagesViewpager
                                    .currentItem
                            ), ignoreCase = true
                        )
                ) {
                    RootValues.getOfferFilterListenerSms()?.onFilterCall(s.toString())
                } else if (getString(R.string.supplementary_offers_campaign_tab_title)
                        .equals(
                            getCurrentSelectedTabName(
                                packagesViewpager
                                    .currentItem
                            ), ignoreCase = true
                        )
                ) {
                    RootValues.getOfferFilterListenerCampaign().onFilterCall(s.toString())
                } else if (getString(R.string.supplementary_offers_tm_tab_title)
                        .equals(
                            getCurrentSelectedTabName(
                                packagesViewpager
                                    .currentItem
                            ), ignoreCase = true
                        )
                ) {
                    RootValues.getOfferFilterListenerTM().onFilterCall(s.toString())
                } else if (getString(R.string.supplementary_offers_hybrid_tab_title)
                        .equals(
                            getCurrentSelectedTabName(
                                packagesViewpager
                                    .currentItem
                            ), ignoreCase = true
                        )
                ) {
                    RootValues.getOfferFilterListenerHybrid().onFilterCall(s.toString())
                } else if (getString(R.string.supplementary_offers_voiceInclusiveOffers_tab_title)
                        .equals(
                            getCurrentSelectedTabName(
                                packagesViewpager
                                    .currentItem
                            ), ignoreCase = true
                        )
                ) {
                    RootValues.getOfferFilterListenerVoiceInclusive().onFilterCall(s.toString())
                } else if (getString(R.string.supplementary_offers_internetInclusiveOffers_tab_title)
                        .equals(
                            getCurrentSelectedTabName(
                                packagesViewpager
                                    .currentItem
                            ), ignoreCase = true
                        )
                ) {
                    onFilterCall(s.toString())
                } else if (getString(R.string.supplementary_offers_smsInclusiveOffers_tab_title)
                        .equals(
                            getCurrentSelectedTabName(
                                packagesViewpager
                                    .currentItem
                            ), ignoreCase = true
                        )
                ) {
                    RootValues.getOfferFilterListenerSmsInclusive().onFilterCall(s.toString())
                }
            }//onTextChanged ends
        })//searchInputField.addTextChangedListener ends
    }//initUIEvents ends

    private fun showFilterAlert() {
        if (packagePlanModel != null) {
            offerFilter = null
            if (packagePlanModel!!.data.internet != null && packagePlanModel!!.data.internet!!.filters != null && packagePlanModel!!.data.internet!!.filters!!.app != null) {
                if (getString(R.string.supplementary_offers_internet_tab_title).equals(
                        getCurrentSelectedTabName(packagesViewpager.currentItem),
                        ignoreCase = true
                    )
                ) {
                    offerFilter = packagePlanModel!!.data.internet!!.filters
                    logE(
                        "sizeoffilter",
                        "size:::".plus(packagePlanModel!!.data.internet!!.filters!!.app!!.size),
                        fromClass,
                        "showFilterAlert"
                    )
                }
            }

            if (offerFilter != null) {
                logE("offerFilter", "notNull", fromClass, "showFilterAlert")
                SupplementaryOffersFilterPopup
                    .newInstance(offerFilter!!, false)
                    .show(supportFragmentManager, DIALOG_FILTER)
            } else {
                logE("offerFilter", "null", fromClass, "showFilterAlert")
                showMessageDialog(
                    applicationContext, this@SupplementaryOffersActivity,
                    resources.getString(R.string.popup_note_title),
                    resources.getString(R.string.lbl_no_filter_msg)
                )
            }
        }
    }//showFilterAlert

    private fun onFilterCall(constrainString: String) {
        if (RootValues.getOfferFilterListenerInternetInclusive() != null) {
            RootValues.getOfferFilterListenerInternetInclusive()!!.onFilterCall(constrainString)
        }
    }

    private fun hideContent() {
        noDataFoundLayout.visibility = View.VISIBLE
        tabLayout.visibility = View.GONE
        packagesViewpager.visibility = View.GONE
    }//hideContent ends

    private fun showContent() {
        noDataFoundLayout.visibility = View.GONE
        tabLayout.visibility = View.VISIBLE
        packagesViewpager.visibility = View.VISIBLE
    }//hideContent ends

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.supplementary_offers_title)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
        packagesFilterIcon.visibility = View.VISIBLE
        packagesLayoutChangerIcon.visibility = View.VISIBLE
        packagesSearchIcon.visibility = View.VISIBLE

        if (RootValues.getOffersListOrientation() == LinearLayout.VERTICAL) {
            packagesLayoutChangerIcon.setImageResource(R.drawable.ic_packages_vertical)
        } else {
            packagesLayoutChangerIcon.setImageResource(R.drawable.ic_window)
        }
    }//setupHeaderActionBar ends

    companion object {

        fun start(context: Context, tabNameToRedirect: String, actionIdFromServer: String) {
            if (hasValue(actionIdFromServer) || hasValue(tabNameToRedirect)) {
                val intent = Intent(context, SupplementaryOffersActivity::class.java)
                intent.putExtra(
                    ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA,
                    tabNameToRedirect
                )
                intent.putExtra(
                    ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA,
                    actionIdFromServer
                )
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                context.startActivity(intent)
            } else {
                val intent = Intent(context, SupplementaryOffersActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                context.startActivity(intent)
            }
        }//start ends

        fun start(context: Context, actionIdFromServer: String) {
            if (hasValue(actionIdFromServer)) {
                val intent = Intent(context, SupplementaryOffersActivity::class.java)
                intent.putExtra(
                    ConstantsUtility.PackagesConstants.NOTIFICATIONS_REDIRECTION_KEY,
                    actionIdFromServer
                )
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                context.startActivity(intent)
            } else {
                val intent = Intent(context, SupplementaryOffersActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                context.startActivity(intent)
            }
        }//start ends

        fun start(context: Context, actionIdFromMySubscriptions: Int) {
            val intent = Intent(context, SupplementaryOffersActivity::class.java)
            intent.putExtra(
                ConstantsUtility.MySubscriptionsConstants.REDIRECTION_INTENT_DATA_MYSUBSCRIPTION,
                actionIdFromMySubscriptions
            )
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends


    }//object ends

}//class ends