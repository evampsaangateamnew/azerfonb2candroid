package com.azarphone.ui.activities.autopayment

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

/**
 * @author Umair Mustafa on 02/04/2021
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * umair.mustafa@evampsaanga.com
 */
class AutoPaymentFactory(val mAutoPaymentRepositry: AutoPaymentRepositry) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AutoPaymentViewModel::class.java)) {
            return AutoPaymentViewModel(mAutoPaymentRepositry) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}