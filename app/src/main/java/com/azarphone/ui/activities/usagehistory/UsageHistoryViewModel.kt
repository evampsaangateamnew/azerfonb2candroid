package com.azarphone.ui.activities.usagehistory

import androidx.lifecycle.MutableLiveData
import com.azarphone.api.pojo.request.*
import com.azarphone.api.pojo.response.otpverifyresponse.OTPVerifyResponse
import com.azarphone.api.pojo.response.resendpin.ResendPinResponse
import com.azarphone.api.pojo.response.usagedetailsresponse.UsageDetailsResponse
import com.azarphone.api.pojo.response.usagehistoryresponse.UsageHistorySummaryResponse
import com.azarphone.api.pojo.response.verifypassportresponse.VerifyPassportResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.logE
import com.azarphone.validators.hasValue
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable


/**
 * @author Junaid Hassan on 04, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class UsageHistoryViewModel(val mUsageHistoryRepositry: UsageHistoryRepositry) : BaseViewModel() {

    private val fromClass = "UsageHistoryViewModel"
    private val logKey = "usageX2kl"
    private lateinit var disposable: Disposable
    val usageHistorySummaryResponseLiveData = MutableLiveData<UsageHistorySummaryResponse>()

    val mResendPinResponse = MutableLiveData<ResendPinResponse>()
    val verifyOTPResponse = MutableLiveData<OTPVerifyResponse>()
    val verifyPassportResponse = MutableLiveData<VerifyPassportResponse>()
    val usageHistoryDetailsResponse = MutableLiveData<UsageDetailsResponse>()

    init {

    }

    fun requestUsageHistorySummary(startDate: String, endDate: String) {
        var accountId = ""
        var customerId = ""
        if (UserDataManager.getCustomerData() != null) {
            if (UserDataManager.getCustomerData()!!.accountId != null
                    && hasValue(UserDataManager.getCustomerData()!!.accountId)) {
                accountId = UserDataManager.getCustomerData()!!.accountId!!
            }

            if (UserDataManager.getCustomerData()!!.customerId != null
                    && hasValue(UserDataManager.getCustomerData()!!.customerId)) {
                customerId = UserDataManager.getCustomerData()!!.customerId!!
            }
        }
        logE(logKey, "requestUsageHistorySummary Called with startDate:::".plus(startDate).plus(" endDate").plus(endDate), fromClass, "requestUsageHistorySummary")
        view()?.showLoading()
        val paymentHistoryRequest = PaymentHistoryRequest(startDate, endDate,accountId,customerId)

        var mObserver = mUsageHistoryRepositry.requestUsageHistorySummary(paymentHistoryRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            logE(logKey, "response:::".plus(result.toString()), fromClass, "requestUsageHistorySummary")
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    usageHistorySummaryResponseLiveData.postValue(result)
                                }
                            }
                        },
                        { error ->
                            logE(logKey, "error:::".plus(error.localizedMessage), fromClass, "requestUsageHistorySummary")
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestPaymentHistory ends

    fun requestVerifyOTP(otpToVerify: String, msisdn: String) {
        view()?.showLoading()
        val otpVerifyRequest = OTPVerifyRequest(ConstantsUtility.APICauses.CAUSE_USAGE_HISTORY_DETAILS, msisdn, otpToVerify)

        var mObserver = mUsageHistoryRepositry.requestVerifyOTP(otpVerifyRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    verifyOTPResponse.postValue(result)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        addDisposable(disposable)
    }//verifySignupOTP ends

    fun resendPin(mMsisdn: String) {
        val resendPinRequest = ResendPinRequest(ConstantsUtility.APICauses.CAUSE_USAGE_HISTORY_DETAILS, mMsisdn)
        view()?.showLoading()

        var mObserver = mUsageHistoryRepositry.resendPin(resendPinRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    mResendPinResponse.postValue(result)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)


    }

    fun requestVerifyPassportID(passportID: String, accountID: String?, customerID: String?) {
        val verifyPassportRequest = VerifyPassportRequest(passportID, accountID, customerID)
        view()?.showLoading()

        var mObserver = mUsageHistoryRepositry.requestVerifyPassportDetails(verifyPassportRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    verifyPassportResponse.postValue(result)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)


    }

    fun requestUsageHistoryDetails(startDate: String, endDate: String, accountID: String?, customerID: String?) {
        logE(logKey, "requestUsageHistoryDetails Called with startDate:::".plus(startDate).plus(" endDate").plus(endDate), fromClass, "requestUsageHistoryDetails")
        view()?.showLoading()
        val usageHistoryDetailsRequest = UsageDetailsRequest(startDate, endDate, accountID, customerID)

        var mObserver = mUsageHistoryRepositry.requestUsageDetails(usageHistoryDetailsRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            logE(logKey, "response:::".plus(result.toString()), fromClass, "requestUsageHistoryDetails")
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    usageHistoryDetailsResponse.postValue(result)
                                    UserDataManager.setUsageDetailsCalled(true)
                                }
                            }
                        },
                        { error ->
                            UserDataManager.setUsageDetailsCalled(false)
                            logE(logKey, "error:::".plus(error.localizedMessage), fromClass, "requestUsageHistoryDetails")
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestPaymentHistory ends
}