package com.azarphone.ui.activities.splashactivity

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.azarphone.BuildConfig
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.response.checkappupdate.CheckAppUpdateResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.SplashActivityBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.ui.activities.loginactivity.LoginActivity
import com.azarphone.ui.activities.mainactivity.MainActivity
import com.azarphone.ui.activities.manageaccount.ManageAccountActivity
import com.azarphone.util.*
import com.azarphone.util.RootedDeviceCheckerUtil.isDeviceRooted
import com.azarphone.util.RootedDeviceCheckerUtil.isEmulator
import com.azarphone.validators.hasValue
import com.azarphone.widgets.popups.showMessageDialog
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.dynamiclinks.ktx.dynamicLinks
import com.google.firebase.installations.FirebaseInstallations
import com.google.firebase.ktx.Firebase
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener

class SplashActivity : BaseActivity<SplashActivityBinding, SplashFactory, SplashViewModel>() {

    private var notifyDataKey = ""
    private val fromClass = "SplashActivity"
    private val screenDelayTime: Long = 1000
    private var alertDialog: AlertDialog? = null

    // Dynamic Links variables
    private var isBottomTabAvailable = false
    private var screenToRedirect = ""
    private var subScreenToRedirect = ""
    private var stepOneScreenToRedirect = ""
    private var tabToRedirect = ""
    private var offeringIdToRedirect = ""

    override fun getViewModelClass(): Class<SplashViewModel> {
        return SplashViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): SplashFactory {
        return SplashInjectionUtils.provideSplashFactory()
    }//getFactory ends

    override fun getLayoutId(): Int {
        return R.layout.splash_activity
    }//getLayoutId ends

    override fun onResume() {
        super.onResume()
        if (alertDialog != null && alertDialog!!.isShowing) {
            alertDialog!!.dismiss()//safe passge, check if it is already showing dismiss it
        }
        initIntentData()
        loadNDKValues()
        /**check if the device is rooted, finish the app*/
        if (!BuildConfig.LOGS_ENABLED) {
            if (isDeviceRooted() || isEmulator()) {
                finish()
            }
        }

        UserDataManager.setPromoMessage(null)
        UserDataManager.setIsPromoMessageDisplayed(false)
//        proceedOnPermissionsChecked()
        applyDynamicLinks()
    }

    private fun applyDynamicLinks() {
        Firebase.dynamicLinks
            .getDynamicLink(intent)
            .addOnSuccessListener(this) { pendingDynamicLinkData ->
                /** Get deep link from result (may be null if no link is found)*/
                var deepLink: Uri? = null
                if (pendingDynamicLinkData != null) {
                    deepLink = pendingDynamicLinkData.link
                }
                if (deepLink != null && deepLink.toString().isNotEmpty()) {
                    logE(
                        "dynamic_links",
                        "dynamic_link = $deepLink",
                        fromClass,
                        "applyDynamicLinks"
                    )
                    getParamsFromLink(deepLink)
                } else {
                    logE("dynamic_links", "Link not found", fromClass, "applyDynamicLinks")
                }

                /** Continuing app flow after getting dynamic link query parameters*/
                proceedOnPermissionsChecked()
            }
            .addOnFailureListener(this) { e ->
                logE(
                    "dynamic_links",
                    "error message = " + e.message,
                    fromClass,
                    "addOnFailureListener"
                )
                /** Continuing app flow if dynamic link query parameters are not available*/
                proceedOnPermissionsChecked()
            }
    }

    private fun getParamsFromLink(deepLink: Uri) {
        if (deepLink.toString()
                .contains(ConstantsUtility.DynamicLinkConstants.BOTTOM_MENU, ignoreCase = true)
        ) {
            /**Select Screen From Bottom Menu*/
            isBottomTabAvailable = true
            if (hasValue(deepLink.getQueryParameter(ConstantsUtility.DynamicLinkConstants.SCREEN_NAME))) {
                screenToRedirect =
                    deepLink.getQueryParameter(ConstantsUtility.DynamicLinkConstants.SCREEN_NAME)
                        .toString()
            }

            if (hasValue(deepLink.getQueryParameter(ConstantsUtility.DynamicLinkConstants.SUB_SCREEN))) {
                subScreenToRedirect =
                    deepLink.getQueryParameter(ConstantsUtility.DynamicLinkConstants.SUB_SCREEN)
                        .toString()
            }

            if (hasValue(deepLink.getQueryParameter(ConstantsUtility.DynamicLinkConstants.STEP_ONE_SCREEN))) {
                stepOneScreenToRedirect =
                    deepLink.getQueryParameter(ConstantsUtility.DynamicLinkConstants.STEP_ONE_SCREEN)
                        .toString()
            }

            if (hasValue(deepLink.getQueryParameter(ConstantsUtility.DynamicLinkConstants.TAB_NAME))) {
                tabToRedirect =
                    deepLink.getQueryParameter(ConstantsUtility.DynamicLinkConstants.TAB_NAME)
                        .toString()
            }

            if (hasValue(deepLink.getQueryParameter(ConstantsUtility.DynamicLinkConstants.OFFERING_ID))) {
                offeringIdToRedirect =
                    deepLink.getQueryParameter(ConstantsUtility.DynamicLinkConstants.OFFERING_ID)
                        .toString()
            }
        } else {
            /**Select Screen from Side Menu*/
            isBottomTabAvailable = false
            if (hasValue(deepLink.getQueryParameter(ConstantsUtility.DynamicLinkConstants.SCREEN_NAME))) {
                screenToRedirect =
                    deepLink.getQueryParameter(ConstantsUtility.DynamicLinkConstants.SCREEN_NAME)
                        .toString()
            }

            if (hasValue(deepLink.getQueryParameter(ConstantsUtility.DynamicLinkConstants.SUB_SCREEN))) {
                subScreenToRedirect =
                    deepLink.getQueryParameter(ConstantsUtility.DynamicLinkConstants.SUB_SCREEN)
                        .toString()
            }

            if (hasValue(deepLink.getQueryParameter(ConstantsUtility.DynamicLinkConstants.TAB_NAME))) {
                tabToRedirect =
                    deepLink.getQueryParameter(ConstantsUtility.DynamicLinkConstants.TAB_NAME)
                        .toString()
            }

            if (hasValue(deepLink.getQueryParameter(ConstantsUtility.DynamicLinkConstants.OFFERING_ID))) {
                offeringIdToRedirect =
                    deepLink.getQueryParameter(ConstantsUtility.DynamicLinkConstants.OFFERING_ID)
                        .toString()
            }
        }
    }

    private fun initIntentData() {
        //get the extras data
        if (intent.extras != null) {
            val bundle = intent.extras
            if (bundle != null) {
                if (bundle.containsKey("notifyDataKey")) {
                    notifyDataKey = bundle.getString("notifyDataKey")!!
                }
            }
        }
    }

    override fun init() {
        /*  loadNDKValues()
          */
        /**check if the device is rooted, finish the app*//*
       if(BuildConfig.LOGS_ENABLED.equals("off",ignoreCase = true)){
           if (isDeviceRooted()|| isEmulator()) {
               finish()
           }
       }
//        processPermissions()
        RootValues.setAzeriSymbolFont(applicationContext)
        RootValues.setALSBoldFont(applicationContext)
        RootValues.setALSNormalFont(applicationContext)
        UserDataManager.setPromoMessage(null)
        UserDataManager.setIsPromoMessageDisplayed(false)
        proceedOnPermissionsChecked()*/
    }//init ends


    private fun processPermissions() {
        Dexter.withActivity(this@SplashActivity)
            .withPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if (report!!.areAllPermissionsGranted()) {
                        proceedOnPermissionsChecked()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    token!!.continuePermissionRequest()
                }

            })
            .onSameThread()
            .check()
    }//processPermissions ends

    private fun proceedOnPermissionsChecked() {
        //reset the user data manager
        UserDataManager.resetUserDataManager()
        FirebaseInstallations.getInstance()
            .getToken(false/*	Options to get an auth token either by force refreshing or not.*/)
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    logE("fcm", "token failed:::", "SplashActivity", "onCreate")
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result!!.token
                logE("fcm", "token:::".plus(token), "SplashActivity", "onCreate")
            })

        //check the app language for testing
        logE(
            "zuban",
            "language:::".plus(ProjectApplication.mLocaleManager.getCurrentLanguageNumber()),
            fromClass,
            "init"
        )
        logE(
            "zuban",
            "language:::".plus(ProjectApplication.mLocaleManager.getLanguage()),
            fromClass,
            "init"
        )
        //init needed values
        initNeededValues()

        subscribe()
        //get the current app version
        var currentAppVersion = ""
        try {
            currentAppVersion = BuildConfig.VERSION_NAME
            logE("splash", "currentAppVersion:::".plus(currentAppVersion), "SplashActivity", "init")
        } catch (exp: Exception) {
            logE("splash", "error:::".plus(exp.toString()), "SplashActivity", "init")
        }//catch ends

        if (isNetworkAvailable(applicationContext)) {
            mViewModel.checkAppUpdate(currentAppVersion)
        } else {
            addSplashScreenDelay()
        }
    }//proceedOnPermissionsChecked ends

    private fun addSplashScreenDelay() {
        try {
            Handler().postDelayed({
                checkUserAlreadyLoggedIn()
            }, screenDelayTime)
        } catch (e: Exception) {
            logE("SplashTag", e.toString(), fromClass, "add SPlash screen delay")
        }//catch ends
    }//addSplashScreenDelay ends

    private fun checkUserAlreadyLoggedIn() {
        /**check if the user had changed the password,
         * if had changed password; redirect the user to the
         * manage account screen*/
        val isPasswordChanged = ProjectApplication.getInstance().getLocalPreferneces().getBool(
            ProjectApplication.getInstance()
                .getLocalPreferneces().IS_PASSWORD_CHANGE_MULTI_USER_LOGGED_OUT,
            false
        )

        if (isPasswordChanged) {
            /**update the user data manager*/
            UserDataManager.saveCustomerData(getCustomerDataFromLocalPreferences())
            /**goto the manage accounts screen*/
            val intent = Intent(applicationContext, ManageAccountActivity::class.java)
            intent.addFlags(
                Intent.FLAG_ACTIVITY_NEW_TASK
                        or
                        Intent.FLAG_ACTIVITY_CLEAR_TASK
                        or
                        Intent.FLAG_ACTIVITY_CLEAR_TOP
            )
            startActivity(intent)
            finish()
        } else {
            /**update the is password changed by the user*/
            ProjectApplication.getInstance().getLocalPreferneces().addBool(
                ProjectApplication.getInstance()
                    .getLocalPreferneces().IS_PASSWORD_CHANGE_MULTI_USER_LOGGED_OUT,
                false
            )

            if (getCustomerDataFromLocalPreferences().msisdn != null
                && hasValue(getCustomerDataFromLocalPreferences().msisdn)
            ) {

                if (getIsBiometricEnabledFlagFromLocalPreferences()) {
                    LoginActivity.start(applicationContext)
                    finish()
                } else {
                    if (hasValue(notifyDataKey)) {
                        //user is being logged in already
                        MainActivity.start(
                            applicationContext,
                            notifyDataKey,
                            screenToRedirect,
                            subScreenToRedirect,
                            stepOneScreenToRedirect,
                            tabToRedirect,
                            offeringIdToRedirect,
                            isBottomTabAvailable
                        )
                        finish()
                    } else {
                        //user is being logged in already
                        MainActivity.start(
                            applicationContext,
                            "",
                            screenToRedirect,
                            subScreenToRedirect,
                            stepOneScreenToRedirect,
                            tabToRedirect,
                            offeringIdToRedirect,
                            isBottomTabAvailable
                        )
                        finish()
                    }
                }
            } else {
                //user is not already logged in
                LoginActivity.start(applicationContext)
                finish()
            }
        }
    }//checkUserAlreadyLoggedIn ends

    private fun getNeededValue(): String {
        return "s@@ng@tibe*&~~~jkm@@st@r:::hbnnV6jRcxEUEcdKHs4jMuTbpQyTfEdt+7+Zloen5KU="
    }//getNeededValue ends

    private fun initNeededValues() {
        val neededValeus = getNeededValue()
        if (hasValue(neededValeus)) {
            val values = neededValeus.split(":::")
            if (values.size == 2) {
                RootValues.setCLASS_NAME_M(values[0])
                RootValues.setCLASS_NAME_SE(values[1])
            }
        }
    }//initNeededValues ends

    fun subscribe() {
        val checkAppUpdateResponse = object : Observer<CheckAppUpdateResponse> {
            override fun onChanged(appUpdateResponse: CheckAppUpdateResponse?) {

                if (appUpdateResponse != null &&
                    appUpdateResponse.data != null &&
                    hasValue(appUpdateResponse.data.toString()) &&
                    appUpdateResponse.resultCode != null &&
                    hasValue(appUpdateResponse.resultCode)
                ) {


                    if (appUpdateResponse.data.playStore != null &&
                        hasValue(appUpdateResponse.data.playStore)
                    ) {
                        UserDataManager.setPlayStoreRedirectionURL(appUpdateResponse.data.playStore)
                    }
                    if (appUpdateResponse.resultCode == "7") {
                        logOut(
                            applicationContext,
                            this@SplashActivity,
                            false,
                            fromClass,
                            "updatesubscribe123hkjakjsnasd"
                        )
                    } else if (appUpdateResponse.resultCode == "8") {
                        if (hasValue(appUpdateResponse.resultDesc)) {
                            //force update
                            showMessageDialogAppUpdate(appUpdateResponse.resultDesc!!, 8)
                        } else {
                            //force update
                            showMessageDialogAppUpdate("Update.en", 8)
                        }
                    } else if (appUpdateResponse.resultCode == "9") {

                        if (hasValue(appUpdateResponse.resultDesc)) {
                            //optional update
                            showMessageDialogAppUpdate(appUpdateResponse.resultDesc!!, 9)
                        } else {
                            //optional update
                            showMessageDialogAppUpdate("Update.en", 9)
                        }
                    } else if (appUpdateResponse.resultCode == "10") {
                        if (hasValue(appUpdateResponse.resultDesc)) {
                            //server down
                            showMessageDialog(
                                applicationContext!!,
                                this@SplashActivity,
                                resources.getString(R.string.popup_error_title),
                                appUpdateResponse.resultDesc.toString()
                            )
                        } else {
                            showMessageDialog(
                                applicationContext, this@SplashActivity,
                                resources.getString(R.string.popup_error_title),
                                resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                        }
                    } else {
                        addSplashScreenDelay()
                    }
                } else {
                    addSplashScreenDelay()
                }
            }
        }

        mViewModel.checkAppUpdateResponse.observe(this@SplashActivity, checkAppUpdateResponse)

        val checkAppUpdateErrorResponse = object : Observer<AzerAPIErrorHelperResponse> {
            override fun onChanged(t: AzerAPIErrorHelperResponse?) {
                addSplashScreenDelay()
            }
        }

        mViewModel.checkAppUpdateErrorResponse.observe(
            this@SplashActivity,
            checkAppUpdateErrorResponse
        )
    }//subscribe ends

    @SuppressLint("InflateParams")
    private fun showMessageDialogAppUpdate(message: String, code: Int) {
        if (this@SplashActivity.isFinishing) return

        if (alertDialog?.isShowing == true) {
            alertDialog?.dismiss()
        }
        try {
            val dialogBuilder = AlertDialog.Builder(this@SplashActivity)
            val inflater =
                applicationContext.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val dialogView = inflater.inflate(R.layout.azer_app_update_popup, null)
            dialogBuilder.setView(dialogView)

            alertDialog = dialogBuilder.create()
            alertDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            alertDialog!!.setCanceledOnTouchOutside(false)
            alertDialog!!.setCancelable(false)
            alertDialog!!.show()

            val updateButton = dialogView.findViewById(R.id.updateButton) as Button
            val cancelButton = dialogView.findViewById(R.id.cancelButton) as Button
            val popupTitle = dialogView.findViewById(R.id.popupTitle) as TextView
            val forceUpdateButton = dialogView.findViewById(R.id.forceUpdateButton) as Button
            val optionalUpdateLayout =
                dialogView.findViewById(R.id.optionalUpdateLayout) as LinearLayout

            if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN) {
                //english
                updateButton.text = "Update"
                cancelButton.text = "Cancel"
            }

            if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ) {
                //azri
                updateButton.text = "Yenilə"
                cancelButton.text = "Ləğv et"
            }

            if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.RU) {
                //russian
                updateButton.text = "Обновить"
                cancelButton.text = "Отменить"
            }

            popupTitle.typeface = getALSNormalFont()
            updateButton.typeface = getALSBoldFont()
            cancelButton.typeface = getALSBoldFont()
            forceUpdateButton.typeface = getALSBoldFont()
            popupTitle.text = message

            updateButton.isSelected = true
            cancelButton.isSelected = true
            forceUpdateButton.isSelected = true

            updateButton.setOnClickListener {
                alertDialog!!.dismiss()
                // Update
                goToPlayStoreForUpdate(this@SplashActivity)
            }//updateButton.setOnClickListener ends

            forceUpdateButton.setOnClickListener {
                alertDialog!!.dismiss()
                // Update
                goToPlayStoreForUpdate(this@SplashActivity)
            }//updateButton.setOnClickListener ends

            cancelButton.setOnClickListener {
                alertDialog!!.dismiss()
                // Continue
                addSplashScreenDelay()
            }//cancelButton.setOnClickListener ends

            if (code == 8) {
                forceUpdateButton.visibility = View.VISIBLE
                optionalUpdateLayout.visibility = View.GONE
            } else if (code == 9) {
                forceUpdateButton.visibility = View.GONE
                optionalUpdateLayout.visibility = View.VISIBLE
            }
        } catch (exp: Exception) {
            logE("erroX!", "error:::$exp", fromClass, "showMessageDialogAppUpdate")
            addSplashScreenDelay()
        }

    }//showLogoutPopupForMultipleUsers ends

    // NDK methods Start
    external fun getSecureKeyValues(): String

    external fun getKeysPublicServer(): String

    companion object {

        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("native-lib")
        }
    }

    fun loadNDKValues() {

        try {// The Key used for encription decription
            KeyValues.getInstance().keyValueFromNdk = getSecureKeyValues()

            // get Server Public Keys
            val keysPublicServer = getKeysPublicServer()
            val separator = ":::::::" //7 chars
            if (keysPublicServer != null) {
                val keysArray = keysPublicServer.split(separator)
                if (keysArray != null && keysArray.size > 0) {
                    if (KeyValues.getInstance().keysPublicServerFromNdk == null) {
                        KeyValues.getInstance().keysPublicServerFromNdk = ArrayList()
                    }
                    KeyValues.getInstance().keysPublicServerFromNdk.addAll(keysArray)
                }
            }
        } catch (e: Exception) {
        }

    }
// NDK methods End

    private fun goToPlayStoreForUpdate(context: Context?) {
        if (context != null && !(context as Activity).isFinishing) {
            val appPackageName = context.packageName
            try {
                context.startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=$appPackageName")
                    )
                )
            } catch (anfe: ActivityNotFoundException) {
                try {
                    context.startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                        )
                    )
                } catch (e: Exception) {
                    addSplashScreenDelay()
                }
            }
            try {
            } catch (e: Exception) {
                addSplashScreenDelay()
            }
        }
    }//goToPlayStoreForUpdate ends
}//class ends