package com.azarphone.ui.activities.manageaccount

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ManageAccountFactory(val mManageAccountRepository: ManageAccountRepository): ViewModelProvider.NewInstanceFactory(){

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        if(modelClass.isAssignableFrom(ManageAccountViewModel::class.java)){
            return ManageAccountViewModel(mManageAccountRepository) as T
        }
        throw IllegalStateException("ViewModel Cannot be Casted")
    }

}