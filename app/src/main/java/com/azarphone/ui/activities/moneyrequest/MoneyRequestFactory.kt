package com.azarphone.ui.activities.moneyrequest

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

/**
 * @author Junaid Hassan on 08, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class MoneyRequestFactory(val mMoneyRequestRepositry: MoneyRequestRepositry) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MoneyRequestViewModel::class.java)) {
            return MoneyRequestViewModel(mMoneyRequestRepositry) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}