package com.azarphone.ui.activities.emailactivity

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.UpdateEmailRequest
import com.azarphone.api.pojo.response.updateemailreponse.UpdateEmailResponse
import io.reactivex.Observable

class EmailRepositry{

    fun requestUpdateEmail(updateRequest: UpdateEmailRequest): Observable<UpdateEmailResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestUpdateEmail(updateRequest)
    }
}