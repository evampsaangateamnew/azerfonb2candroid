package com.azarphone.ui.activities.forgotpasswordactivity

import androidx.lifecycle.MutableLiveData
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import android.os.CountDownTimer
import com.azarphone.analytics.Analytics
import com.azarphone.analytics.EventValues
import com.azarphone.api.pojo.request.ForgotPasswordRequest
import com.azarphone.api.pojo.request.OTPRequest
import com.azarphone.api.pojo.request.OTPVerifyRequest
import com.azarphone.api.pojo.request.ResendPinRequest
import com.azarphone.api.pojo.response.forgotpassword.ForgotPasswordResponse
import com.azarphone.api.pojo.response.otpgetresponse.OTPResponse
import com.azarphone.api.pojo.response.otpverifyresponse.OTPVerifyResponse
import com.azarphone.api.pojo.response.resendpin.ResendPinResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.util.ConstantsUtility
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

class ForgotPasswordViewModel(val mForgotPasswordReporsitory: ForgotPasswordReporsitory) : BaseViewModel() {

    lateinit var disposable: Disposable

    val forgotPasswordStepperData = MutableLiveData<String>()
    var forgotPasswordNumberLiveDataObserver = MutableLiveData<String>()

    var updatePasswordNewPin = MutableLiveData<String>()
    var updatePasswordConfirmPin = MutableLiveData<String>()
//    var updatePasswordIsChecked = MutableLiveData<Boolean>()

    val forgotpasswordPinET = MutableLiveData<String>()
    val forgotPasswordOTPLiveData = MutableLiveData<OTPResponse>()
    val mOTPVerifyResponse = MutableLiveData<OTPVerifyResponse>()

    val passwordET = MutableLiveData<String>()
    val confirmPasswordET = MutableLiveData<String>()
    val isChecked = ObservableBoolean()

    val mForgotPasswordResponse = MutableLiveData<ForgotPasswordResponse>()

    val requestOTPResponse = MutableLiveData<OTPResponse>()

    val mResendPinResponse = MutableLiveData<ResendPinResponse>()

    val showBackBtn = ObservableField<Boolean>()

    var mMsisdn =""

    var countDownTimer: CountDownTimer? = null
    val timerField = MutableLiveData<Int>()


    public var enableBackPress: Boolean = false

    init {
        showBackBtn.set(false)
        enableBackPress = true
    }

    fun setForgotPasswordStep(stepInfo: String) {
        forgotPasswordStepperData.value = stepInfo
    }//setForgotPasswordStep ends


    fun setBackPressEnable(isEnabled: Boolean) {
        enableBackPress = isEnabled
    }


    fun set25SecondsTimerOff() {
        setBackPressEnable(true)
        showBackBtn.set(true)
        timerField.postValue(-12)
        if (countDownTimer != null) {
            countDownTimer!!.cancel()
        }
    }//set25SecondsTimerOff ends

    fun set25SecondsTimer() {
        var count = 26
        setBackPressEnable(false)
        countDownTimer = object : CountDownTimer(26000, 1000) {

            override fun onTick(millisUntilFinished: Long) {
                count -= 1
                timerField.value = count
            }

            override fun onFinish() {
                timerField.value = -12
                showBackBtn.set(true)
                setBackPressEnable(true)
            }
        }.start()
    }//set25SecondsTimer ends


    fun updatePassword() {
        view()?.showLoading()

        val forgotPasswordRequest = ForgotPasswordRequest(updatePasswordNewPin.value.toString(), forgotpasswordPinET.value.toString(), updatePasswordConfirmPin.value.toString(), mMsisdn)

        var mObserver = mForgotPasswordReporsitory.updatePassword(forgotPasswordRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            if (result.resultCode.equals("00")) {
                                mForgotPasswordResponse.postValue(result)
                                Analytics.logAppEvent(EventValues.ForgotEvents.forgot, EventValues.ForgotEvents.recovered, EventValues.GenericValues.Success)

                            } else {
                                result.resultDesc?.let {
                                    view()?.loadError(it)
                                    Analytics.logAppEvent(EventValues.ForgotEvents.forgot, EventValues.ForgotEvents.recovered, EventValues.GenericValues.Failure)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }


    fun verfityPin() {
        view()?.showLoading()

        val mRequestOTPVerifyRequest = OTPVerifyRequest(ConstantsUtility.APICauses.CAUSE_FORGOT_PASSWORD_ENTER_PIN, mMsisdn, forgotpasswordPinET.value.toString())

        var mObserver = mForgotPasswordReporsitory.verifyOtp(mRequestOTPVerifyRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            if (result.resultCode.equals("00")) {
                                mOTPVerifyResponse.postValue(result)
                            } else {
                                result.resultDesc?.let {
                                    view()?.loadError(it)
                                }
                            }
                        },
                        { error ->
                            view()?.loadError(error)
                            view()?.hideLoading()
                        }
                )

        compositeDisposables?.add(disposable)
    }


    fun requestOTP(msisdn: String) {
        mMsisdn = msisdn
        view()?.showLoading()
        val otpRequest = OTPRequest(ConstantsUtility.APICauses.CAUSE_FORGOT_PASSWORD_ENTER_PIN, msisdn)

        var mObserver = mForgotPasswordReporsitory.requestOTP(otpRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            if (result.resultCode.equals("00")) {
                                requestOTPResponse.postValue(result)
                            } else {
                                result.resultDesc?.let {
                                    view()?.loadError(it)
                                }
                            }

                        },
                        { error ->
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)


    }//requestOTP ends

    private fun parseOTPResponse(response: OTPResponse) {
        //        //check the response code
        //        if (hasValue(response.resultCode.toString())) {
        //            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
        //                /**handle the success case
        //                 * when the result code from the server is 00*/
        //                if (hasValue(response.data.toString())) {
        //                    //process the response here
        //                    forgotPasswordOTPLiveData.postValue(response)
        //                } else {
        //                    /**the response has no data object
        //                     * in this case we have to handle the ui only*/
        //                    forgotPasswordOTPErrorLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
        //                }
        //            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
        //                forgotPasswordOTPErrorLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT, ""))
        //                /**force log out user case
        //                 * when the server sends the result code as 7*/
        //                forgotPasswordOTPErrorLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT, ""))
        //            } else {
        //                /**check if has description
        //                 * in case of result code other than 00
        //                 * if has description show it in the popup
        //                 * else show a simple api failure popup*/
        //                if (hasValue(response.resultDesc.toString())) {
        //                    forgotPasswordOTPErrorLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
        //                } else {
        //                    forgotPasswordOTPErrorLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
        //                }
        //            }
        //        } else {
        //            /**check if has description
        //             * in case of no result code
        //             * if has description show it in the popup
        //             * else show a simple api failure popup*/
        //            if (hasValue(response.resultDesc.toString())) {
        //                forgotPasswordOTPErrorLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
        //            } else {
        //                forgotPasswordOTPErrorLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
        //            }
        //        }
    }


    fun resendPin() {
        val resendPinRequest = ResendPinRequest(ConstantsUtility.APICauses.CAUSE_FORGOT_PASSWORD_ENTER_PIN, mMsisdn)
        view()?.showLoading()
        disposable = mForgotPasswordReporsitory.resendPin(resendPinRequest)
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            if (result.resultCode.equals("00")) {
                                mResendPinResponse.postValue(result)
                            } else {
                                result.resultDesc?.let {
                                    view()?.loadError(it)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)


    }


}//class ends

