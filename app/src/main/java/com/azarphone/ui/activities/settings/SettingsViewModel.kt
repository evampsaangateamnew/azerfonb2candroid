package com.azarphone.ui.activities.settings

import androidx.lifecycle.MutableLiveData
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.request.AppResumeRequest
import com.azarphone.api.pojo.request.FCMRequest
import com.azarphone.api.pojo.response.fcmtoken.FCMResponse
import com.azarphone.api.pojo.response.loginappresumeresponse.LoginAppResumeResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.logE
import com.azarphone.validators.hasValue
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

/**
 * @author Junaid Hassan on 18, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class SettingsViewModel(val mSettingsRepositry: SettingsRepositry) : BaseViewModel() {

    private val fromClass = "SettingsViewModel"
    private lateinit var disposable: Disposable
    val fcmResponseLiveData = MutableLiveData<FCMResponse>()
    val fcmResponseSilentLiveData = MutableLiveData<FCMResponse>()
    val appResumeErrorResponseDataSettings = MutableLiveData<AzerAPIErrorHelperResponse>()
    val appResumeResponseDataSettings = MutableLiveData<LoginAppResumeResponse>()

    init {

    }

    fun addFCMTokenToServerFromSettings(isShowMessage: Boolean, token: String, ringingStatus: String, isEnable: String, cause: String, msisdn: String, isBackground: Boolean,subscriberTypeFCM:String,tariffTypeFCM:String) {
        logE("fcm", "token called:::".plus(token), fromClass, "addFCMTokenToServerFromSettings")
        if (isBackground) {
            //run in background
            val fcmRequest = FCMRequest(token, ringingStatus, isEnable, cause, msisdn,subscriberTypeFCM,tariffTypeFCM)
            disposable = mSettingsRepositry.requestFCMTokenSentToServerService(fcmRequest)
                    .compose(applyIOSchedulers())
                    .subscribe(
                            { result ->
                                fcmResponseSilentLiveData.postValue(result)
                                logE("fcm", "tokenSentSuccess:::".plus(result.toString()), fromClass, "addFCMTokenToServerFromSettings")
                            },
                            { error ->
                                //do nothing
                                logE("fcm", "tokenSentFailure:::".plus(error.toString()), fromClass, "addFCMTokenToServerFromSettings")
                            }
                    )

            compositeDisposables?.add(disposable)
        } else {
            view()?.showLoading()
            val fcmRequest = FCMRequest(token, ringingStatus, isEnable, cause, msisdn,subscriberTypeFCM,tariffTypeFCM)
            disposable = mSettingsRepositry.requestFCMTokenSentToServerService(fcmRequest)
                    .compose(applyIOSchedulers())
                    .subscribe(
                            { result ->
                                view()?.hideLoading()
                                view()?.isErrorErrorDialoge(result)?.let {
                                    if (it != true) {
                                        fcmResponseLiveData.postValue(result)
                                    }
                                }
                            },
                            { error ->
                                view()?.loadError(error)
                                view()?.hideLoading()
                            }
                    )

            compositeDisposables?.add(disposable)
        }
    }//addFCMTokenToServerFromSettings ends

    private fun parseAppResumeResponse(response: LoginAppResumeResponse) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        appResumeResponseDataSettings.postValue(response)
                    } else {
                        /**the response has no data object
                         * in this case we have to handle the ui only*/
                        appResumeErrorResponseDataSettings.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                    }
                } else {
                    /**the response has no data object
                     * in this case we have to handle the ui only*/
                    appResumeErrorResponseDataSettings.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                }
            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                /**force log out user case
                 * when the server sends the result code as 7*/
                appResumeErrorResponseDataSettings.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT, ""))
            } else {
                /**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*/
                if (hasValue(response.resultDesc.toString())) {
                    appResumeErrorResponseDataSettings.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
                } else {
                    appResumeErrorResponseDataSettings.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
                }
            }
        } else {
            /**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*/
            if (hasValue(response.resultDesc.toString())) {
                appResumeErrorResponseDataSettings.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
            } else {
                appResumeErrorResponseDataSettings.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }//parseAppResumeResponse ends
}