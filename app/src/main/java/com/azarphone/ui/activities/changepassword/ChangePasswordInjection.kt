package com.azarphone.ui.activities.changepassword

/**
 * @author Junaid Hassan on 04, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
object ChangePasswordInjection {

    fun provideChangePasswordRepository(): ChangePasswordRepositry {
        return ChangePasswordRepositry()
    }

    fun provideChangePasswordFactory(): ChangePasswordFactory {
        return ChangePasswordFactory(provideChangePasswordRepository())
    }

}