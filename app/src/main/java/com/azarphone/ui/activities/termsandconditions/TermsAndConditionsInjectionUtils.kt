package com.azarphone.ui.activities.termsandconditions



object TermsAndConditionsInjectionUtils{
    fun provideTermsRepository(): TermsAndConditionsRepositry {
        return TermsAndConditionsRepositry()
    }

    fun provideTermsFactory(): TermsAndConditionsFactory {
        return TermsAndConditionsFactory(provideTermsRepository())
    }
}