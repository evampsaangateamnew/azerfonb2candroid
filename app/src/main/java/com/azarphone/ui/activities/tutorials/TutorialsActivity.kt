package com.azarphone.ui.activities.tutorials

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.analytics.Analytics
import com.azarphone.analytics.EventValues
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.TutorialsActivityBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.isNetworkAvailable
import com.azarphone.util.logE
import com.azarphone.validators.hasValue
import com.devbrackets.android.exomedia.core.video.scale.ScaleType
import com.devbrackets.android.exomedia.listener.OnCompletionListener
import com.devbrackets.android.exomedia.listener.OnErrorListener
import kotlinx.android.synthetic.main.tutorials_activity.*
import java.lang.Exception

/**
 * @author Junaid Hassan on 20, June, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class TutorialsActivity : BaseActivity<TutorialsActivityBinding, TutorialsFactory, TutorialsViewModel>() {

    private val fromClass = "TutorialsActivity"

    override fun getLayoutId(): Int {
        return R.layout.tutorials_activity
    }//getLayoutId ends

    override fun getViewModelClass(): Class<TutorialsViewModel> {
        return TutorialsViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): TutorialsFactory {
        return TutorialsInjection.provideTutorialsFactory()
    }//getFactory ends

    override fun init() {
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)

        initUiContent()

        initUiListener()
    }//init ends

    private fun initUiListener() {
        tutorialsVideo.setOnPreparedListener {
            mViewModel.hideLoader()
            bgRed.visibility = View.GONE
            tutorialsVideo.start()
        }//tutorialsVideo.setOnPreparedListener ends

        iv_skip.setOnClickListener {
            Analytics.logAppEvent(EventValues.TutorialsEvents.tutorial_complete, EventValues.TutorialsEvents.complete, EventValues.GenericValues.Success)
            finish()
        }// iv_skip.setOnClickListener ends

        tutorialsVideo.setOnErrorListener(object : OnErrorListener {
            override fun onError(e: Exception?): Boolean {
                Toast.makeText(applicationContext, resources.getString(R.string.server_stopped_responding_please_try_again), Toast.LENGTH_SHORT).show()
                finish()
                return true
            }
        })

        tutorialsVideo.setOnCompletionListener(object : OnCompletionListener {
            override fun onCompletion() {
                logE("finishX1", "called", fromClass, "setOnCompletionListener")
                this@TutorialsActivity.finish()
            }
        })

    }//initUiListener ends

    private fun initUiContent() {
        var videoURL = "https://plusapp.nar.az/azerfon/videos/"

        /**check the subscriber type*/
        var subscriberType = ""
        if (UserDataManager.getCustomerData() != null &&
                UserDataManager.getCustomerData()!!.subscriberType != null &&
                hasValue(UserDataManager.getCustomerData()!!.subscriberType)) {
            subscriberType = if (UserDataManager.getCustomerData()!!.subscriberType.equals(ConstantsUtility.UserConstants.USER_SUBSCRIBER_TYPE_PREPAID, ignoreCase = true)) {
                "prepaid"
            } else {
                "postpaid"
            }
        }

        /**check the current language, and get the video extention*/
        var videoTypeExtenstion = ""
        if (hasValue(subscriberType) && ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.EN, ignoreCase = true)) {
            videoTypeExtenstion = "_en.mp4"
        } else if (hasValue(subscriberType) && ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.AZ, ignoreCase = true)) {
            videoTypeExtenstion = "_az.mp4"
        } else if (hasValue(subscriberType) && ProjectApplication.mLocaleManager.getLanguage().equals(ConstantsUtility.LanguageKeywords.RU, ignoreCase = true)) {
            videoTypeExtenstion = "_ru.mp4"
        } else {
            subscriberType = "prepaid"
            videoTypeExtenstion = "_az.mp4"
        }

        /**create the final url*/
        videoURL = videoURL.plus(subscriberType).plus(videoTypeExtenstion)
        logE("videoXR", "url:::".plus(videoURL), fromClass, "PlayingVideoViaURL")
        if (isNetworkAvailable(applicationContext)) {
            val vidUri = Uri.parse(videoURL)
            tutorialsVideo.setVideoURI(vidUri)
            Analytics.logAppEvent(EventValues.TutorialsEvents.tutorial_begin, EventValues.TutorialsEvents.begin, EventValues.GenericValues.Success)
            mViewModel.showLoader()
        } else {
            Analytics.logAppEvent(EventValues.TutorialsEvents.tutorial_begin, EventValues.TutorialsEvents.begin, EventValues.GenericValues.Failure)
            Toast.makeText(this, R.string.message_no_internet, Toast.LENGTH_SHORT).show()
            finish()
        }
    }//initUiContent ends

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, TutorialsActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends
    }//object ends

}//class ends