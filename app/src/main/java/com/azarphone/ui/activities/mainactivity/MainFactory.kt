package com.azarphone.ui.activities.mainactivity

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

/**
 * @author Junaid Hassan on 13, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class MainFactory(val mMainRepositry: MainRepositry) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(mMainRepositry) as T
        }
        throw IllegalStateException("ViewModel Cannot be Converted")
    }//create ends

}//class ends