package com.azarphone.ui.activities.vasservices

/**
 * @author Junaid Hassan on 24, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
object CoreServicesInjection {

    fun provideCoreServicesRepository(): CoreServicesRepositry {
        return CoreServicesRepositry()
    }

    fun provideCoreServicesFactory(): CoreServicesFactory {
        return CoreServicesFactory(provideCoreServicesRepository())
    }

}