package com.azarphone.ui.activities.faq

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class FaqFactory(val faqRepository: FaqRepositry) : ViewModelProvider.NewInstanceFactory() {


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(FaqViewModel::class.java)) {
            return FaqViewModel(faqRepository) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}