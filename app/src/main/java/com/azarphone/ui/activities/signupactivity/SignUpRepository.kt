package com.azarphone.ui.activities.signupactivity

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.OTPRequest
import com.azarphone.api.pojo.request.OTPVerifyRequest
import com.azarphone.api.pojo.request.ResendPinRequest
import com.azarphone.api.pojo.request.SaveCustomerRequest
import com.azarphone.api.pojo.response.otpgetresponse.OTPResponse
import com.azarphone.api.pojo.response.otpverifyresponse.OTPVerifyResponse
import com.azarphone.api.pojo.response.resendpin.ResendPinResponse
import com.azarphone.api.pojo.response.savecustomerresponse.SaveCustomerResponse
import io.reactivex.Observable

class SignUpRepository {

    init {
    }//init ends

    fun requestOTP(otpRequest: OTPRequest): Observable<OTPResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestOTP(otpRequest)
    }//requestOTP ends


    fun resendPin(resendPinRequest: ResendPinRequest):Observable<ResendPinResponse>{
        return ApiClient.newApiClientInstance.getServerAPI().resendPin(resendPinRequest)
    }


    fun requestVerifyOTP(otpVerifyRequest: OTPVerifyRequest): Observable<OTPVerifyResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestVerifyOTP(otpVerifyRequest)
    }//requestVerifyOTP ends

    fun requestSaveCustomer(saveCustomerRequest: SaveCustomerRequest): Observable<SaveCustomerResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestSaveCustomer(saveCustomerRequest)
    }//requestSaveCustomer ends
}//class ends