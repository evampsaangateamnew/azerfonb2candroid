package com.azarphone.ui.activities.fasttopup

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class FastTopUpFactory(private val fastTopUpRepository: FastTopUpRepository) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(FastTopUpViewModel::class.java)) {
            return FastTopUpViewModel(fastTopUpRepository = fastTopUpRepository) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}