package com.azarphone.ui.activities.datawithoutpack

import androidx.lifecycle.MutableLiveData
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.request.CoreServiceProcessRequest
import com.azarphone.api.pojo.response.coreservices.CoreServicesResponse
import com.azarphone.api.pojo.response.coreservicesprocessnumber.CoreServicesProcessResponse
import com.azarphone.api.pojo.response.mysubscriptions.successresponse.MySubscriptionsSuccessAfterOfferSubscribeResponse
import com.azarphone.api.pojo.response.payg.WithOutPackData
import com.azarphone.api.pojo.response.payg.WithOutPackResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.logE
import com.azarphone.validators.hasValue
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

class DataWithOutPackViewModel(val dataWithOutPackRepository: DataWithOutPackRepository) :
    BaseViewModel() {
    private lateinit var disposable: Disposable
    val dataWithOutPackBackgroundResponseLiveData = MutableLiveData<CoreServicesResponse>()
    val dataWithOutPackResponseLiveData = MutableLiveData<WithOutPackResponse>()
    val dataWithOutPackErrorResponseLiveData = MutableLiveData<AzerAPIErrorHelperResponse>()

    val coreServiceProcessErrorResponseLiveData = MutableLiveData<AzerAPIErrorHelperResponse>()
    val coreServiceProcessResponseLiveData = MutableLiveData<CoreServicesProcessResponse>()

    private val fromClass = "dataWithOutPackViewModel"

    fun getPayGCurrentStatus() {
        view()?.showLoading()

        var mObserver =
            dataWithOutPackRepository.requestCurrentPayGStatus()
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
            .compose(applyIOSchedulers())
            .subscribe(
                { result ->
                    parseCurrentPayGStatusResponseLiveData(result)
                },
                { error ->
                    view()?.hideLoading()
                    dataWithOutPackErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                }
            )

        compositeDisposables?.add(disposable)
    }//subscribeToSupplementaryOffer ends


    private fun parseCurrentPayGStatusResponseLiveData(data: WithOutPackResponse) {
        //check the response code
        if (hasValue(data.resultCode.toString())) {
            if (data.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (data.data != null) {
                    if (hasValue(data.data.toString())) {
                        //process the response here
                        dataWithOutPackResponseLiveData.postValue(data)
                    } else {
                        /**the response has no data object
                         * in this case we have to handle the ui only*/
                        dataWithOutPackErrorResponseLiveData.postValue(
                            AzerAPIErrorHelperResponse(
                                ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                ""
                            )
                        )
                    }
                } else {
                    /**the response has no data object
                     * in this case we have to handle the ui only*/
                    dataWithOutPackErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                }
            } else if (data.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                /**force log out user case
                 * when the server sends the result code as 7*/
                dataWithOutPackErrorResponseLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT,
                        ""
                    )
                )
            } else {
                /**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*/
                if (hasValue(data.resultDesc.toString())) {
                    dataWithOutPackErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                            data.resultDesc
                        )
                    )
                } else {
                    dataWithOutPackErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                            ""
                        )
                    )
                }
            }
        } else {
            /**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*/
            if (hasValue(data.resultDesc.toString())) {
                dataWithOutPackErrorResponseLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                        data.resultDesc
                    )
                )
            } else {
                dataWithOutPackErrorResponseLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                        ""
                    )
                )
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }//parseSubscribeToSupplementaryOfferResponseLiveData ends

    fun requestProcessCoreService(
        actionType: String,
        offeringId: String,
        groupType: String,
        msisdnToForwardNumberOn: String,
        accountType: String
    ) {
        logE(
            "coreServices",
            "called:::requestProcessCoreService",
            fromClass,
            "requestProcessCoreService"
        )
        view()?.showLoading()
        val coreServiceProcessRequest = CoreServiceProcessRequest(
            actionType,
            offeringId,
            groupType,
            msisdnToForwardNumberOn,
            accountType
        )


        var mObserver =
            dataWithOutPackRepository.requestProcessCoreService(coreServiceProcessRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
            .compose(applyIOSchedulers())
            .subscribe(
                { result ->
                    logE(
                        "coreServices",
                        "response:::".plus(result.toString()),
                        fromClass,
                        "requestProcessCoreService"
                    )
                    parseProcessCoreServiceResponse(result, offeringId)
                },
                { error ->
                    view()?.hideLoading()
                    coreServiceProcessErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                }
            )

        compositeDisposables?.add(disposable)
    }//requestProcessCoreService ends
    private fun parseProcessCoreServiceResponse(
        response: CoreServicesProcessResponse,
        offeringId: String
    ) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                * when the result code from the server is 00*/
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        UserDataManager.setSuccessOfferingIdForCoreService(offeringId)
                        //process the response here
                        coreServiceProcessResponseLiveData.postValue(response)
                    } else {
                        /**the response has no data object
                        * in this case we have to handle the ui only*/
                                coreServiceProcessErrorResponseLiveData.postValue(
                                    AzerAPIErrorHelperResponse(
                                        ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                        ""
                                    )
                                )
                    }
                } else {
                    /**the response has no data object
                    * in this case we have to handle the ui only*/
                            coreServiceProcessErrorResponseLiveData.postValue(
                                AzerAPIErrorHelperResponse(
                                    ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                    ""
                                )
                            )
                }
            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                /**force log out user case
                * when the server sends the result code as 7*/
                        coreServiceProcessErrorResponseLiveData.postValue(
                            AzerAPIErrorHelperResponse(
                                ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT,
                                ""
                            )
                        )
            } else {
                /**check if has description
                * in case of result code other than 00
                * if has description show it in the popup
                * else show a simple api failure popup*/
                if (hasValue(response.resultDesc.toString())) {
                    coreServiceProcessErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                            response.resultDesc
                        )
                    )
                } else {
                    coreServiceProcessErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                            ""
                        )
                    )
                }
            }
        } else {
            /**check if has description
            * in case of no result code
            * if has description show it in the popup
            * else show a simple api failure popup*/
            if (hasValue(response.resultDesc.toString())) {
                coreServiceProcessErrorResponseLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                        response.resultDesc
                    )
                )
            } else {
                coreServiceProcessErrorResponseLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                        ""
                    )
                )
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }//parseProcessCoreServiceResponse ends

/*    fun requestDataWithOutPackBackground(
        groupType: String,
        brand: String,
        userType: String,
        isFrom: String
    ) {
        var accountType = "Individual"
        if (UserDataManager.getCustomerData() != null) {
            if (hasValue(UserDataManager.getCustomerData()!!.customerType)) {
                accountType =
                    if (UserDataManager.getCustomerData()!!.customerType.equals(ConstantsUtility.UserConstants.CUSTOMER_TYPE_DATA_SIM_PREPAID) ||
                        UserDataManager.getCustomerData()!!.customerType.equals(ConstantsUtility.UserConstants.CUSTOMER_TYPE_DATA_SIM_POSTPAID)
                    ) {
                        "wxxt"
                    } else if (UserDataManager.getCustomerData()!!.customerType.equals(
                            ConstantsUtility.UserConstants.CUSTOMER_TYPE_INDIVIDUAL
                        )
                    ) {
                        "individual"
                    } else if (UserDataManager.getCustomerData()!!.customerType.equals(
                            ConstantsUtility.UserConstants.CUSTOMER_TYPE_FULL
                        )
                    ) {
                        "full"
                    } else if (UserDataManager.getCustomerData()!!.customerType.equals(
                            ConstantsUtility.UserConstants.CORPORATE_CUSTOMER
                        )
                    ) {
                        "corporate"
                    } else {
                        "Individual"
                    }
            }
        }


        logE(
            "coreServices",
            "calledBG:::requestCoreServices",
            fromClass,
            "requestCoreServicesBackground"
        )
        val coreServicesRequest =
            CoreServicesRequest(accountType, groupType, brand, userType, isFrom)

        var mObserver = dataWithOutPackRepository.requestCoreServices(coreServicesRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
            .compose(applyIOSchedulers())
            .subscribe(
                { result ->
                    logE(
                        "coreServices",
                        "response1BG:::".plus(result.toString()),
                        fromClass,
                        "requestCoreServicesBackground"
                    )
                    dataWithOutPackBackgroundResponseLiveData.postValue(result)
                },
                { error ->
                    view()?.hideLoading()
                }
            )

        compositeDisposables?.add(disposable)
    }

    fun requestDataWithOutPackServices(
        groupType: String,
        brand: String,
        userType: String,
        isFrom: String
    ) {
        var accountType = "Individual"
        if (UserDataManager.getCustomerData() != null) {
            if (hasValue(UserDataManager.getCustomerData()!!.customerType)) {
                accountType =
                    if (UserDataManager.getCustomerData()!!.customerType.equals(ConstantsUtility.UserConstants.CUSTOMER_TYPE_DATA_SIM_PREPAID) ||
                        UserDataManager.getCustomerData()!!.customerType.equals(ConstantsUtility.UserConstants.CUSTOMER_TYPE_DATA_SIM_POSTPAID)
                    ) {
                        "wxxt"
                    } else if (UserDataManager.getCustomerData()!!.customerType.equals(
                            ConstantsUtility.UserConstants.CUSTOMER_TYPE_INDIVIDUAL
                        )
                    ) {
                        "individual"
                    } else if (UserDataManager.getCustomerData()!!.customerType.equals(
                            ConstantsUtility.UserConstants.CUSTOMER_TYPE_FULL
                        )
                    ) {
                        "full"
                    } else if (UserDataManager.getCustomerData()!!.customerType.equals(
                            ConstantsUtility.UserConstants.CORPORATE_CUSTOMER
                        )
                    ) {
                        "corporate"
                    } else {
                        "Individual"
                    }
            }
        }

        logE(
            "coreServices",
            "called:::requestCoreServices",
            fromClass,
            "requestCoreServicesBackground"
        )
        view()?.showLoading()
        val coreServicesRequest =
            CoreServicesRequest(accountType, groupType, brand, userType, isFrom)

        var mObserver = dataWithOutPackRepository.requestCoreServices(coreServicesRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
            .compose(applyIOSchedulers())
            .subscribe(
                { result ->
                    logE(
                        "coreServices",
                        "response1:::".plus(result.toString()),
                        fromClass,
                        "subscribe"
                    )
                    parseResponse(result)
                },
                { error ->
                    view()?.hideLoading()
                    dataWithOutPackErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                }
            )

        compositeDisposables?.add(disposable)
    }//requestCoreServices ends*/
/*

    fun changeSupplementaryOfferForCallDivert(
        offeringName: String,
        offeringId: String,
        actionType: String
    ) {
        view()?.showLoading()
        val supplementaryOfferSubscribeRequest =
            SupplementaryOfferSubscribeRequest(offeringName, offeringId, actionType)

        var mObserver = dataWithOutPackRepository.changeSupplementaryOfferForCallDivert(
            supplementaryOfferSubscribeRequest
        )
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
            .compose(applyIOSchedulers())
            .subscribe(
                { result ->
                    parseCallDivertDisableResponse(result, offeringId)
                },
                { error ->
                    view()?.hideLoading()
                    callDivertDisableErrorResponseData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""
                        )
                    )
                }
            )

        compositeDisposables?.add(disposable)
    }//subscribeToSupplementaryOffer ends

    private fun parseCallDivertDisableResponse(
        response: MySubscriptionsSuccessAfterOfferSubscribeResponse,
        offeringId: String
    ) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                *//**handle the success case
                 * when the result code from the server is 00*//*
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        callDivertDisableResponseData.postValue(response)
                        RootValues.getCoreServiceAdapterUpdaterEvents()
                            .onCallDivertDisabledUpdateAdapter(offeringId)
                    } else {
                        *//**the response has no data object
                         * in this case we have to handle the ui only*//*
                        callDivertDisableErrorResponseData.postValue(
                            AzerAPIErrorHelperResponse(
                                ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                ""
                            )
                        )
                    }
                } else {
                    *//**the response has no data object
                     * in this case we have to handle the ui only*//*
                    callDivertDisableErrorResponseData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                }
            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                *//**force log out user case
                 * when the server sends the result code as 7*//*
                callDivertDisableErrorResponseData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT,
                        ""
                    )
                )
            } else {
                *//**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*//*
                if (hasValue(response.resultDesc.toString())) {
                    callDivertDisableErrorResponseData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                            response.resultDesc
                        )
                    )
                } else {
                    callDivertDisableErrorResponseData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                            ""
                        )
                    )
                }
            }
        } else {
            *//**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*//*
            if (hasValue(response.resultDesc.toString())) {
                callDivertDisableErrorResponseData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                        response.resultDesc
                    )
                )
            } else {
                callDivertDisableErrorResponseData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                        ""
                    )
                )
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }





    private fun parseNumberForwardResponse(response: CoreServicesProcessResponse) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                *//**handle the success case
                 * when the result code from the server is 00*//*
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        dataWithOutPackForwardNumberResponseLiveData.postValue(response)
                    } else {
                        *//**the response has no data object
                         * in this case we have to handle the ui only*//*
                        dataWithOutPackForwardNumberErrorResponseLiveData.postValue(
                            AzerAPIErrorHelperResponse(
                                ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                ""
                            )
                        )
                    }
                } else {
                    *//**the response has no data object
                     * in this case we have to handle the ui only*//*
                    dataWithOutPackForwardNumberErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                }
            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                *//**force log out user case
                 * when the server sends the result code as 7*//*
                dataWithOutPackForwardNumberErrorResponseLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT,
                        ""
                    )
                )
            } else {
                *//**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*//*
                if (hasValue(response.resultDesc.toString())) {
                    dataWithOutPackForwardNumberErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                            response.resultDesc
                        )
                    )
                } else {
                    dataWithOutPackForwardNumberErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                            ""
                        )
                    )
                }
            }
        } else {
            *//**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*//*
            if (hasValue(response.resultDesc.toString())) {
                dataWithOutPackForwardNumberErrorResponseLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                        response.resultDesc
                    )
                )
            } else {
                dataWithOutPackForwardNumberErrorResponseLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                        ""
                    )
                )
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }//parseNumberForwardResponse ends

    private fun parseResponse(response: CoreServicesResponse) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                *//**handle the success case
                 * when the result code from the server is 00*//*
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        dataWithOutPackResponseLiveData.postValue(response)
                    } else {
                        *//**the response has no data object
                         * in this case we have to handle the ui only*//*
                        dataWithOutPackErrorResponseLiveData.postValue(
                            AzerAPIErrorHelperResponse(
                                ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                ""
                            )
                        )
                    }
                } else {
                    *//**the response has no data object
                     * in this case we have to handle the ui only*//*
                    dataWithOutPackErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                }
            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                *//**force log out user case
                 * when the server sends the result code as 7*//*
                dataWithOutPackErrorResponseLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT,
                        ""
                    )
                )
            } else {
                *//**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*//*
                if (hasValue(response.resultDesc.toString())) {
                    dataWithOutPackErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                            response.resultDesc
                        )
                    )
                } else {
                    dataWithOutPackErrorResponseLiveData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                            ""
                        )
                    )
                }
            }
        } else {
            *//**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*//*
            if (hasValue(response.resultDesc.toString())) {
                dataWithOutPackErrorResponseLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION,
                        response.resultDesc
                    )
                )
            } else {
                dataWithOutPackErrorResponseLiveData.postValue(
                    AzerAPIErrorHelperResponse(
                        ConstantsUtility.APIResponseErrorCodes.API_FAILURE,
                        ""
                    )
                )
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }//parseResponse ends*/
}
