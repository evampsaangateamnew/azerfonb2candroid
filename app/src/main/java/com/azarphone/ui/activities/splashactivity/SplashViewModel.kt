package com.azarphone.ui.activities.splashactivity

import androidx.lifecycle.MutableLiveData
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.request.CheckAppUpdateRequest
import com.azarphone.api.pojo.response.checkappupdate.CheckAppUpdateResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.util.ConstantsUtility
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable


/**
 * Created by Junaid Hassan on 19, February, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */

/**
 * the class is intended for the splash
 * will contain the api for checking if app update is available
 * there will be two types of the updates i.e. optional and the force*/
class SplashViewModel(val mSplashRepository: SplashRepository) : BaseViewModel() {

    private val fromClass = "SplashViewModel"
    lateinit var disposable: Disposable
    val checkAppUpdateResponse = MutableLiveData<CheckAppUpdateResponse>()
    val checkAppUpdateErrorResponse = MutableLiveData<AzerAPIErrorHelperResponse>()

    init {

    }//init ends

    fun checkAppUpdate(currentAppVersion: String) {
        val checkAppUpdateRequest = CheckAppUpdateRequest(currentAppVersion)
        var mObserver = mSplashRepository.checkAppUpdate(checkAppUpdateRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
            .compose(applyIOSchedulers())
            .subscribe(
                { result ->
                    checkAppUpdateResponse.postValue(result)
                },
                { error ->
                    //don't do anything
                    checkAppUpdateErrorResponse.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            ""
                        )
                    )
                }
            )
    }//checkAppUpdate ends



}//class ends