package com.azarphone.ui.activities.faq

import androidx.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.view.View
import android.widget.AdapterView
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.faqhelpers.FAQListHelperModel
import com.azarphone.api.pojo.helperpojo.faqhelpers.FAQSpinnerHelperModel
import com.azarphone.api.pojo.response.faqresponse.FAQResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.LayoutFaqFragmentBinding
import com.azarphone.eventhandler.FAQItemSelectEvents
import com.azarphone.ui.adapters.expandablelists.FAQListAdapter
import com.azarphone.ui.adapters.spinners.FAQSpinnerAdapter
import com.azarphone.util.getFAQCachedDataFromLocalPreferences
import com.azarphone.util.isNetworkAvailable
import com.azarphone.util.logE
import com.azarphone.util.setFAQCachedDataToLocalPreferences
import com.azarphone.validators.hasValue
import com.azarphone.widgets.popups.showMessageDialog
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.layout_faq_fragment.*
import java.util.*

class FaqActivity : BaseActivity<LayoutFaqFragmentBinding, FaqFactory, FaqViewModel>(), FAQItemSelectEvents {

    private val fromClass = "FaqActivity"
    private var fAQResponse: FAQResponse? = null
    private var adapterFAQListAdapter: FAQListAdapter? = null
    private var fAQSpinnerAdapter: FAQSpinnerAdapter? = null

    override fun getLayoutId(): Int {
        return R.layout.layout_faq_fragment
    }//getLayoutId ends

    override fun getViewModelClass(): Class<FaqViewModel> {
        return FaqViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): FaqFactory {
        return FaqInjectionUtils.provideFaqFactory()
    }//getFactory ends

    override fun onItemSelectForSearch(searchKey: String) {
        if (adapterFAQListAdapter != null) {
            adapterFAQListAdapter!!.filter.filter(searchKey)
        }

        spinnerTitle.text = searchKey
        spinnerCategories.onDetachedFromWindow()
    }//onItemSelectForSearch ends

    override fun init() {
        setupHeaderActionBar()
        subscribe()
        initUIEvents()

        spinnerTitle.isSelected = true

        /**get the response from the cache*/
        val cachedFAQ = getFAQCachedDataFromLocalPreferences()
        if (cachedFAQ.data != null && hasValue(cachedFAQ.data.toString())) {
            fAQResponse = cachedFAQ
            populateUI(fAQResponse!!)
            setFAQCachedDataToLocalPreferences(fAQResponse!!)

            /**call the api in the background*/
            mViewModel.requestFAQsBackground()
        } else {
            if (isNetworkAvailable(applicationContext)) {
                Handler().postDelayed({ mViewModel.requestFAQs() }, 50L)
            } else {
                showMessageDialog(applicationContext, this@FaqActivity,
                        "", resources.getString(R.string.message_no_internet))
            }
        }

    }//init ends

    private fun initUIEvents() {
        azerActionBarBackIcon.setOnClickListener {
            onBackPressed()
        }//azerActionBarBackIcon.setOnClickListener ends

        spinnerTitle.setOnClickListener {
            spinnerCategories.performClick()
        }//spinnerTitle.setOnClickListener ends

        spinnerArrowRight.setOnClickListener {
            spinnerCategories.performClick()
        }//spinnerArrowRight.setOnClickListener ends
    }//initUIEvents ends

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.faq_screen_title)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }

    private fun subscribe() {
        val faqResponseData = object : Observer<FAQResponse> {
            override fun onChanged(t: FAQResponse?) {
                if (!this@FaqActivity.isFinishing) {
                    if (t != null) {
                        if (hasValue(t.toString()) && t.data != null) {
                            fAQResponse = t
                            setFAQCachedDataToLocalPreferences(fAQResponse!!)
                            populateUI(fAQResponse!!)
                            logE("faqResponse", "response:::".plus(t.toString()), fromClass, "subscribe")
                        } else {
                            logE("faqResponse", "response:::No1", fromClass, "subscribe")
                            showMessageDialog(this@FaqActivity, this@FaqActivity,
                                    resources.getString(R.string.popup_error_title),
                                    resources.getString(R.string.server_stopped_responding_please_try_again))
                        }
                    } else {
                        logE("faqResponse", "response:::No2", fromClass, "subscribe")
                        showMessageDialog(this@FaqActivity, this@FaqActivity,
                                resources.getString(R.string.popup_error_title),
                                resources.getString(R.string.server_stopped_responding_please_try_again))
                    }

                }
            }
        }

        mViewModel.faqResponseData.observe(this@FaqActivity, faqResponseData)

        val faqResponseBackgroundData = object : Observer<FAQResponse> {
            override fun onChanged(t: FAQResponse?) {
                if (!this@FaqActivity.isFinishing) {
                    if (t != null && t.data != null) {
                        fAQResponse = t
                        setFAQCachedDataToLocalPreferences(fAQResponse!!)
                        adapterFAQListAdapter?.notifyDataSetChanged()
                        logE("adapterXF", "notified", fromClass, "faqResponseBackgroundData")
                    }
                }
            }
        }

        mViewModel.faqResponseBackgroundData.observe(this@FaqActivity, faqResponseBackgroundData)
    }

    private fun populateUI(response: FAQResponse) {
        if (response.data.faqlist != null) {
            if (response.data.faqlist!!.isNotEmpty()) {
                //iterate over the faqlist
                val spinnerTitlesList = ArrayList<FAQSpinnerHelperModel>()
                val QAList = ArrayList<FAQListHelperModel>()
                for (its in 0 until response.data.faqlist!!.size) {
                    //get the title
                    var title = ""
                    if (hasValue(response.data.faqlist!![its]!!.title)) {
                        title = response.data.faqlist!![its]!!.title!!
                        spinnerTitlesList.add(FAQSpinnerHelperModel(title))
                        if (its == 0) {
                            spinnerTitle.text = title
                        }
                    }//if (hasValue(response.data.faqlist!![its]!!.title)) ends

                    if (response.data.faqlist!![its]!!.qalist != null) {
                        //populate the list model
                        //iterate over qalist
                        for (items in 0 until response.data.faqlist!![its]!!.qalist!!.size) {
                            //check answer string
                            var answer = ""
                            if (hasValue(response.data.faqlist!![its]!!.qalist!![items]!!.answer)) {
                                answer = response.data.faqlist!![its]!!.qalist!![items]!!.answer!!
                            }

                            var question = ""
                            if (hasValue(response.data.faqlist!![its]!!.qalist!![items]!!.question)) {
                                question = response.data.faqlist!![its]!!.qalist!![items]!!.question!!
                            }

                            var sortOrder = "0"
                            if (hasValue(response.data.faqlist!![its]!!.qalist!![items]!!.sortOrder)) {
                                sortOrder = response.data.faqlist!![its]!!.qalist!![items]!!.sortOrder!!
                            }

                            QAList.add(FAQListHelperModel(question, answer, sortOrder, title))
                        }//for ends

                        //sort the list
                        Collections.sort(QAList, object : Comparator<FAQListHelperModel> {
                            override fun compare(o1: FAQListHelperModel, o2: FAQListHelperModel): Int {
                                return o1.sortOrder?.toIntOrNull() ?: 0.compareTo(o2.sortOrder?.toIntOrNull() ?: 0)
                            }
                        })
                    }//if (response.data.faqlist!![its]!!.qalist != null)  ends
                }//for ends

                //now populate the spinner
                fAQSpinnerAdapter = FAQSpinnerAdapter(this@FaqActivity, spinnerTitlesList, this)
                spinnerCategories.setAdapter(fAQSpinnerAdapter)
                //now populate the expandable list
                adapterFAQListAdapter = FAQListAdapter(this@FaqActivity, this@FaqActivity, QAList)
                faqsList.setAdapter(adapterFAQListAdapter)
                faqsList.visibility = View.VISIBLE
                noDataFound.visibility = View.GONE
            }
        }
    }//populateUI ends

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, FaqActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }
    }

}