package com.azarphone.ui.activities.fnf

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.response.addfnfresponse.AddFnfResponse
import com.azarphone.api.pojo.response.deletefnfresponse.DeleteFnfResponse
import com.azarphone.api.pojo.response.fnfresponse.FNFResponse
import com.azarphone.api.pojo.response.fnfresponse.FnfListItem
import com.azarphone.api.pojo.response.inappsurvey.InAppSurvey
import com.azarphone.api.pojo.response.inappsurvey.Surveys
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.updatefnfresponse.UpdateFNFResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.FnfActivityBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.FnFEditButtonEvents
import com.azarphone.eventhandler.OnClickSurveySubmit
import com.azarphone.eventhandler.UpdateListOnSuccessfullySurvey
import com.azarphone.ui.adapters.recyclerviews.FnFListAdapter
import com.azarphone.ui.dialogs.InAppFeedbackDialog
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.validators.isValidMsisdn
import com.azarphone.widgets.popups.showMessageDialog
import com.es.tec.LocalSharedPrefStorage
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.fnf_activity.*

@Suppress("DEPRECATION")
class FnFActivity : BaseActivity<FnfActivityBinding, FnFFactory, FnFViewModel>() {

    var fnfList = listOf<FnfListItem?>()
    private var fnfLimit: String? = null
    private val fromClass = "FnFActivity"
    var isUpdateButtonClicked = false
    var msisdn: String = ""
    var oldMsisdn = ""
    var newMsisdn = ""

    override fun getLayoutId(): Int {
        return R.layout.fnf_activity
    }

    override fun getViewModelClass(): Class<FnFViewModel> {
        return FnFViewModel::class.java
    }

    override fun getFactory(): FnFFactory {
        return FnFInjectionUtils.provideFnFFactory()
    }

    override fun init() {
        subscribe()
        subscribeForAddFNF()
        subscribeForClearAllFnf()
        subscribeForUpdateFNF()
        mViewModel.requestFNFList()
        initUI()
        initUIEvents()
        listenersForInAppReview()
    }

    private fun initUI() {
        setupHeaderActionBar()
        addPrefixToUserNameField(userNameET)

        userNameET.typeface = getALSBoldFont()
    }

    private fun subscribe() {
        val fnfListResponseObserver = object : Observer<FNFResponse> {
            override fun onChanged(response: FNFResponse?) {
                if (!this@FnFActivity.isFinishing) {
                    if (response != null && response.data != null && !response.data.fnfList.isNullOrEmpty()) {
                        mViewModel.isFirstTime = "false"
                        fnfList = response.data.fnfList
                        if (hasValue(response.data.fnfLimit)) {
                            fnfLimit = response.data.fnfLimit.toString()
                        }
                        fnf_count.text = response.data.fnfList.size.toString() + "/" + fnfLimit
                        val fnfListAdapter = FnFListAdapter(this@FnFActivity, response.data.fnfList)
                        fnf_recycler.adapter = fnfListAdapter
                        fnf_recycler.layoutManager =
                            androidx.recyclerview.widget.LinearLayoutManager(
                                this@FnFActivity,
                                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                                false
                            )
                        fnf_recycler.visibility = View.VISIBLE
                        noDataFoundLayout.visibility = View.GONE

                        fnf_count.setText(response.data.fnfList.size.toString() + "/" + fnfLimit)
                        val descrip = resources.getString(R.string.fnf_description)
                        description.text = descrip.replace("2", response?.data?.fnfLimit!!)
                        clear_all.isEnabled = true
                    } else {
                        fnf_recycler.visibility = View.GONE
                        noDataFoundLayout.visibility = View.VISIBLE

                        clear_all.isEnabled = false
                    }
                }
            }
        }

        mViewModel.fnfListResponse.observe(this@FnFActivity, fnfListResponseObserver)

        val fnfListErrorResponse = object : Observer<AzerAPIErrorHelperResponse> {
            override fun onChanged(response: AzerAPIErrorHelperResponse?) {
                if (!this@FnFActivity.isFinishing) {
                    fnf_recycler.visibility = View.GONE
                    noDataFoundLayout.visibility = View.VISIBLE
                    //check internet
                    if (!isNetworkAvailable(this@FnFActivity)) {
                        showMessageDialog(
                            applicationContext, this@FnFActivity,
                            resources.getString(R.string.popup_note_title),
                            resources.getString(R.string.message_no_internet)
                        )
                    } else {
                        /**check the error code
                         * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                         * else show a general api failure popup*/
                        if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                            showMessageDialog(
                                applicationContext, this@FnFActivity,
                                resources.getString(R.string.popup_error_title),
                                response.errorDetail.toString()
                            )
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                            //force logout the user here
                            logOut(
                                applicationContext,
                                this@FnFActivity,
                                false,
                                fromClass,
                                "specialOffersErrorResponseData2321sf"
                            )
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                            //handle the ui in this case
                            //hide or show the no data found layout in this case
                            showMessageDialog(
                                applicationContext, this@FnFActivity,
                                resources.getString(R.string.popup_error_title),
                                resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                            showMessageDialog(
                                applicationContext, this@FnFActivity,
                                resources.getString(R.string.popup_error_title),
                                resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                        } else {
                            showMessageDialog(
                                applicationContext, this@FnFActivity,
                                resources.getString(R.string.popup_error_title),
                                resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                        }
                    }
                }
            }
        }

        mViewModel.fnfListErrorResponse.observe(this@FnFActivity, fnfListErrorResponse)
    }

    private fun subscribeForAddFNF() {
        val addFNFResponseObserver = object : Observer<AddFnfResponse> {
            override fun onChanged(response: AddFnfResponse?) {
                if (!this@FnFActivity.isFinishing) {
                    if (response?.data?.fnfList != null) {
                        fnfList = response.data.fnfList!!
                        val fnfListAdapter =
                            FnFListAdapter(this@FnFActivity, response?.data?.fnfList!!)
                        fnf_recycler.adapter = fnfListAdapter
                        fnf_recycler.layoutManager =
                            androidx.recyclerview.widget.LinearLayoutManager(
                                this@FnFActivity,
                                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                                false
                            )

                    }

                    if (response != null && response.data != null && response.resultDesc != null && hasValue(
                            response.resultDesc
                        )
                    ) {
//                        showMessageDialog(
//                            applicationContext, this@FnFActivity,
//                            resources.getString(R.string.popup_note_title),
//                            response.resultDesc
//                        )
                        inAppFeedback(
                            this@FnFActivity,
                            resources.getString(R.string.popup_note_title),
                            response.resultDesc
                        )
                    }

                    if (hasValue(response!!.data.fnfLimit)) {
                        fnfLimit = response.data.fnfLimit
                        fnf_count.setText(response.data.fnfList!!.size.toString() + "/" + response!!.data!!.fnfLimit)
                    } else {
                        if (response.data.fnfList != null && response.data.fnfLimit != null) {
                            fnf_count.setText(response.data.fnfList!!.size.toString() + "/" + fnfLimit)
                        } else {
                            if (fnfLimit == null) {
                                fnfLimit = "0"
                            }
                            var listSize = "0"
                            if (response.data.fnfList != null) {
                                listSize = response.data.fnfList!!.size.toString()
                            }
                            fnf_count.setText(listSize + "/" + fnfLimit)
                        }
                    }

                    userNameET.setText("")

                }
            }
        }

        mViewModel.addFNFResponse.observe(this@FnFActivity, addFNFResponseObserver)
    }

    private fun listenersForInAppReview() {
        val inAppSurveyResponseErrorResponseData =
            Observer<AzerAPIErrorHelperResponse> { response ->
                if (!isNetworkAvailable(this@FnFActivity)) {
                    showMessageDialog(
                        this@FnFActivity, this@FnFActivity,
                        resources.getString(R.string.popup_note_title),
                        resources.getString(R.string.message_no_internet)
                    )
                } else {
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                        showMessageDialog(
                            this@FnFActivity, this@FnFActivity,
                            this@FnFActivity.resources.getString(R.string.popup_error_title),
                            response.errorDetail.toString()
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                        //force logout the user here
                        logOut(
                            this@FnFActivity,
                            this@FnFActivity,
                            false,
                            fromClass ?: "TopUp",
                            "initSupplementaryOffersCardAdapterEventsa34324"
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                        //handle the ui in this case
                        //hide or show the no data found layout in this case
                        showMessageDialog(
                            this@FnFActivity, this@FnFActivity,
                            this@FnFActivity.resources.getString(R.string.popup_error_title),
                            this@FnFActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                        showMessageDialog(
                            this@FnFActivity, this@FnFActivity,
                            this@FnFActivity.resources.getString(R.string.popup_error_title),
                            this@FnFActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    } else {
                        showMessageDialog(
                            this@FnFActivity, this@FnFActivity,
                            this@FnFActivity.resources.getString(R.string.popup_error_title),
                            this@FnFActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    }
                }
            }
        mViewModel.inAppSurveyResponseErrorResponseData.observe(
            this@FnFActivity,
            inAppSurveyResponseErrorResponseData
        )
    }

    private fun subscribeForUpdateFNF() {
        val updateFNFResponseObserver = object : Observer<UpdateFNFResponse> {
            override fun onChanged(response: UpdateFNFResponse?) {
                if (!this@FnFActivity.isFinishing) {
                    isUpdateButtonClicked = false
                    if (response?.data?.fnfList != null) {
                        fnfList = response.data.fnfList!!
                        val fnfListAdapter = FnFListAdapter(this@FnFActivity, response?.data?.fnfList!!)
                        fnf_recycler.adapter = fnfListAdapter
                        fnf_recycler.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@FnFActivity, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
                        fnf_recycler.visibility = View.VISIBLE
                        noDataFoundLayout.visibility = View.GONE
                    } else {
                        fnf_recycler.visibility = View.GONE
                        noDataFoundLayout.visibility = View.VISIBLE
                    }
                    if (response != null && response.data != null && response.resultDesc != null && hasValue(response.resultDesc)) {
//                        showMessageDialog(applicationContext, this@FnFActivity,
//                                resources.getString(R.string.popup_note_title),
//                                response.resultDesc)

                        inAppFeedback(
                            this@FnFActivity,
                            resources.getString(R.string.popup_note_title),
                            response?.resultDesc!!
                        )
                    }
                    userNameET.setText("")
                }
            }
        }

        mViewModel.updateFNFResponse.observe(this@FnFActivity, updateFNFResponseObserver)

    }

    private fun inAppFeedback(context: Context, title: String, message: String) {

        var inAppFeedbackDialog: InAppFeedbackDialog? = null
        var currentVisit: Int =
            LocalSharedPrefStorage(context).getInt(LocalSharedPrefStorage.PREF_FNF_PAGE)
        currentVisit += 1
        val inAppSurvey: InAppSurvey? = UserDataManager.getInAppSurvey()
        if ((inAppSurvey?.data) != null) {
            val surveys: Surveys? =
                inAppSurvey.data.findSurvey(ConstantsUtility.InAppSurveyConstants.FNF_PAGE)
            if (surveys != null) {
                if (surveys.surveyLimit > surveys.surveyCount) {
                    if (surveys.visitLimit <= currentVisit && surveys.visitLimit > -1) {
                        inAppFeedbackDialog =
                            InAppFeedbackDialog(context, object : OnClickSurveySubmit {
                                override fun onClickSubmit(
                                    uploadSurvey: UploadSurvey,
                                    onTransactionComplete: String,updateListOnSuccessfullySurvey: UpdateListOnSuccessfullySurvey?
                                ) {
                                    mViewModel.uploadInAppSurvey(
                                        context,
                                        uploadSurvey,
                                        onTransactionComplete
                                    )
                                    if (inAppFeedbackDialog?.isShowing == true) {
                                        inAppFeedbackDialog?.dismiss()
                                    }
                                }

                            })
                        if (surveys.questions != null) {
                            currentVisit = 0
                            inAppFeedbackDialog.showTitleWithMessageDialog(
                                surveys,
                                title,
                                message
                            )
                        } else {
                            showMessageDialog(
                                context, this@FnFActivity,
                                title,
                                message
                            )
                        }
                    } else {
                        showMessageDialog(
                            context, this@FnFActivity,
                            title,
                            message
                        )
                    }
                } else {
                    showMessageDialog(
                        context, this@FnFActivity,
                        title,
                        message
                    )
                }

            }  else {
                showMessageDialog(
                    applicationContext, this@FnFActivity,
                    title,
                    message
                )
            }
        } else {
            showMessageDialog(
                applicationContext, this@FnFActivity,
                resources.getString(R.string.popup_note_title),
                message
            )
        }
        LocalSharedPrefStorage(this@FnFActivity).addInt(
            LocalSharedPrefStorage.PREF_FNF_PAGE,
            currentVisit
        )
    }

    private fun subscribeForClearAllFnf() {
        val clearAllResponseObserver = object : Observer<DeleteFnfResponse> {
            override fun onChanged(response: DeleteFnfResponse?) {
                if (!this@FnFActivity.isFinishing) {
                    fnf_recycler.visibility = View.GONE
                    noDataFoundLayout.visibility = View.VISIBLE

                    fnf_count.setText("0" + "/" + fnfLimit)

                    val list: List<FnfListItem> = ArrayList<FnfListItem>()
                    val fnfListAdapter = FnFListAdapter(this@FnFActivity, list)
                    fnf_recycler.adapter = fnfListAdapter
                    fnf_recycler.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
                        this@FnFActivity,
                        androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                        false
                    )
//                    showMessageDialog(
//                        this@FnFActivity, this@FnFActivity,
//                        resources.getString(R.string.popup_ok),
//                        response?.resultDesc!!
//                    )
                    inAppFeedback(
                        this@FnFActivity,
                        resources.getString(R.string.popup_note_title),
                        response?.resultDesc!!
                    )


                    fnfList = ArrayList()
                    if (response.data.fnfList != null && response.data.fnfLimit != null) {
                        fnf_count.setText(response.data.fnfList!!.size.toString() + "/" + response.data.fnfLimit)
                    }
                    if (response?.data?.fnfLimit != null) {
                        fnfLimit = response.data.fnfLimit.toString()
                        val descrip = resources.getString(R.string.fnf_description)
                        description.text = descrip.replace("2", response?.data?.fnfLimit!!)
                    }
                }
            }
        }

        mViewModel.deleteFNFResponse.observe(this@FnFActivity, clearAllResponseObserver)
    }

    private fun initUIEvents() {
        azerActionBarBackIcon.setOnClickListener {
            onBackPressed()
        }

        addNumber.setOnClickListener {
            msisdn = userNameET.text.toString()
                .replace(ConstantsUtility.UserConstants.USERNAME_PREFIX, "")
            if (isValidMsisdn(msisdn)) {
                if (isUpdateButtonClicked) {
                    if (UserDataManager.getCustomerData()!!.msisdn.equals(msisdn)) {
                        showMessageDialog(
                            applicationContext, this@FnFActivity,
                            resources.getString(R.string.popup_note_title),
                            resources.getString(R.string.fnf_message_own_number)
                        )
                    } else {
                        showAddNumberConfirmationPopUp(
                            resources.getString(R.string.fnf_update),
                            "Update"
                        )
                    }
                } else {
                    if (fnfLimit != null && fnfList.size >= fnfLimit!!.toInt()) {
                        showMessageDialog(
                            applicationContext, this@FnFActivity,
                            "", resources.getString(R.string.fnf_max_limit)
                        )
                    } else {
                        if (UserDataManager.getCustomerData()!!.msisdn.equals(msisdn)) {
                            showMessageDialog(
                                applicationContext, this@FnFActivity,
                                resources.getString(R.string.popup_note_title),
                                resources.getString(R.string.fnf_message_own_number)
                            )
                        } else {
                            showAddNumberConfirmationPopUp(
                                resources.getString(R.string.fnf_add),
                                "Add"
                            )
                        }

                    }
                }
            } else {
                showMessageDialog(
                    applicationContext, this@FnFActivity,
                    "", resources.getString(R.string.invalid_number)
                )
            }
        }

        clear_all.setOnClickListener {
            showAddNumberConfirmationPopUp(
                resources.getString(R.string.fnf_clear_all_popup),
                "Delete"
            )
        }//transferButton.setOnClickListener ends

        val fnFEditButtonEvents = object : FnFEditButtonEvents {
            override fun onEditButtonClicked(msisdnToEdit: String) {
                isUpdateButtonClicked = true
                oldMsisdn = msisdnToEdit
                newMsisdn = msisdnToEdit
                logE("fnfX", "number:::$msisdnToEdit", fromClass, "fnFEditButtonEvents")
                userNameET.setText(ConstantsUtility.UserConstants.USERNAME_PREFIX.plus(msisdnToEdit))
            }
        }

        RootValues.setFnFEditButtonEvents(fnFEditButtonEvents)
    }//initUIEvents ends

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.fnf_title)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

    private fun showAddNumberConfirmationPopUp(msg: String, useCaseType: String) {
        var msgToDisplay = msg
        if (this@FnFActivity.isFinishing) return

        val dialogBuilder = AlertDialog.Builder(this@FnFActivity)
        val inflater =
            applicationContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val dialogView = inflater.inflate(R.layout.azer_addnumber_popup, null)
        dialogBuilder.setView(dialogView)

        val alertDialog = dialogBuilder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.show()

        val popupOkayButton = dialogView.findViewById(R.id.yesButton) as Button
        val popupCancelButton = dialogView.findViewById(R.id.noButton) as Button
        val popupTitle = dialogView.findViewById(R.id.popupTitle) as TextView

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN) {
            //english
            popupOkayButton.text = "Ok"
            popupCancelButton.text = "No"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ) {
            //azri
//            popupOkayButton.text = "Bəli"
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "Xeyr"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.RU) {
            //russian
//            popupOkayButton.text = "Да"
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "Нет"
        }

        if (useCaseType.equals("Add")) {
            msgToDisplay = msgToDisplay.replace("%@", msisdn)
        }
        popupTitle.text = msgToDisplay


        popupOkayButton.setOnClickListener {
            alertDialog.dismiss()
            if (useCaseType.equals("Update")) {
                var newFnf = ""
                if (hasValue(userNameET.text.toString())) {
                    newFnf = userNameET.text.toString()
                        .replace(ConstantsUtility.UserConstants.USERNAME_PREFIX, "")
                }
                mViewModel.requestUpdateFNF("2", oldMsisdn, newFnf)
            } else if (useCaseType.equals("Add")) {
                mViewModel.requestAddFNF(msisdn)
            } else if (useCaseType.equals("Delete")) {
                clearAllNumbers()
            }

        }

        popupCancelButton.setOnClickListener {
            alertDialog.dismiss()
            //do nothing
        }
    }

    private fun clearAllNumbers() {
        val listOfMsisdnsToDelete = ArrayList<String>()
        for (its in fnfList.indices) {
            if (fnfList[its] != null && hasValue(fnfList[its]!!.msisdn)) {
                listOfMsisdnsToDelete.add(fnfList[its]!!.msisdn!!)
            }
        }
        mViewModel.requestClearAllFNF(listOfMsisdnsToDelete)
    }

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, FnFActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends
    }
}