package com.azarphone.ui.activities.exchangeservice

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.response.exchangeserviceresponse.ExchangeServiceResponse
import com.azarphone.api.pojo.response.exchangeservicesvaluesresponse.Data
import com.azarphone.api.pojo.response.exchangeservicesvaluesresponse.ExchangeServicesValuesResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.ActivityExchangeServiceBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.widgets.popups.showMessageDialog
import com.azarphone.widgets.seekbar.CustomSeekBar
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_exchange_service.*
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*


@Suppress("DEPRECATION")
class ExchangeServiceActivity :
    BaseActivity<ActivityExchangeServiceBinding, ExchangeServiceFactory, ExchangeServiceViewModel>() {

    private val fromClass = "ExchangeServiceActivity"
    var responseData: Data? = null
    private var exchangeServiceResponse: ExchangeServicesValuesResponse? = null
    lateinit var customSeekBar: CustomSeekBar
    var isDataToVoice = false
    private var tabToRedirect: String? = null

    override fun getLayoutId(): Int {
        return R.layout.activity_exchange_service
    }

    override fun getViewModelClass(): Class<ExchangeServiceViewModel> {
        return ExchangeServiceViewModel::class.java
    }

    override fun getFactory(): ExchangeServiceFactory {
        return ExchangeServiceInjectionUtils.provideExchangeServiceFactory()
    }

    override fun init() {
        setupHeaderActionBar()
        getIntentData()
        initUi()
        initUiEvents()
        setupTabLayout()
        subscribe()
        subscribeForExchangeService()

        /**get the response from the cache*/
        val cachedExchangeService = getExchangeServiceCachedDataFromLocalPreferences()
        if (cachedExchangeService.data != null && hasValue(cachedExchangeService.data.toString())) {
            exchangeServiceResponse = cachedExchangeService
            populateUI(exchangeServiceResponse!!)
            setExchangeServiceCachedDataToLocalPreferences(exchangeServiceResponse!!)

            /**call the api in the background*/
            mViewModel.requestExchangeServiceValuesInBackground()
        } else {
            if (isNetworkAvailable(applicationContext)) {
                mViewModel.requestExchangeServiceValues()
            } else {
                showMessageDialog(
                    applicationContext, this@ExchangeServiceActivity,
                    "", resources.getString(R.string.message_no_internet)
                )
            }
        }


        /*  //setup viewPager
          usageHistoryViewpager.adapter = UsageHistoryPagerAdapter(supportFragmentManager, "")

          val limit = if (UsageHistoryPagerAdapter(supportFragmentManager, "").count > 1) UsageHistoryPagerAdapter(supportFragmentManager, "").count - 1 else 1
          usageHistoryViewpager.offscreenPageLimit = limit*/


    }//init ends

    private fun setSeekBarFromResponse(data: Data?, voiceToData: Boolean) {
        responseData = data
//        if (data?.isServiceEnabled.equals("false", true)) {
//            exchangeButtonDisabled.visibility = View.VISIBLE
//            exchangeButtonEnabled.visibility = View.INVISIBLE
//        } else
//        {
        exchangeButtonDisabled.visibility = View.INVISIBLE
        exchangeButtonEnabled.visibility = View.VISIBLE
//        }
//            if (voiceToData) data?.voicevalues?.size?.plus(1) else data?.datavalues?.size?.plus(1)
        var size = 1
        if (data != null && data.voicevalues != null && data.voicevalues.size != null) {
            size = data.datavalues?.size!! + 1
        }
        val mSeekLin = findViewById(com.azarphone.R.id.seekbar_LinearView) as LinearLayout
        mSeekLin.removeAllViews()
        customSeekBar = CustomSeekBar(this, size, Color.DKGRAY, data, voiceToData)

        customSeekBar.addSeekBar(mSeekLin)
    }


    private fun subscribe() {
        val valuesResponseObserver = object : Observer<ExchangeServicesValuesResponse> {
            override fun onChanged(response: ExchangeServicesValuesResponse?) {
                if (!this@ExchangeServiceActivity.isFinishing) {
                    setExchangeServiceCachedDataToLocalPreferences(response!!)
                    populateUI(response)

                }
            }
        }

        mViewModel.exchangeServiceValuesResponse.observe(
            this@ExchangeServiceActivity,
            valuesResponseObserver
        )
    }

    private fun populateUI(response: ExchangeServicesValuesResponse?) {
        seekbar_LinearView.visibility = View.VISIBLE
        noDataFoundLayout.visibility = View.GONE
        details.visibility = View.VISIBLE
        setSeekBarFromResponse(response?.data, true)
    }

    private fun subscribeForExchangeService() {
        val moneyRequestResponseObserver = object : Observer<ExchangeServiceResponse> {
            override fun onChanged(response: ExchangeServiceResponse?) {
                if (!this@ExchangeServiceActivity.isFinishing) {
                    showMessageDialog(
                        this@ExchangeServiceActivity, this@ExchangeServiceActivity,
                        resources.getString(R.string.popup_ok),
                        response?.resultDesc!!
                    )


                }
            }
        }

        mViewModel.exchangeServiceResponse.observe(
            this@ExchangeServiceActivity,
            moneyRequestResponseObserver
        )
    }

    private fun setupTabLayout() {
        tabLayout.addTab(
            tabLayout.newTab()
                .setText(resources.getString(com.azarphone.R.string.exchange_service_voice_to_data))
        )
        tabLayout.addTab(
            tabLayout.newTab()
                .setText(resources.getString(com.azarphone.R.string.exchange_service_data_to_voice))
        )
        //set the first tab by default
        tabLayout.getTabAt(0)!!.select()

        for (i in 0 until tabLayout.tabCount) {
            val tab = (tabLayout.getChildAt(0) as ViewGroup).getChildAt(i)
            val p = tab.layoutParams as ViewGroup.MarginLayoutParams
            val margin = (15 * resources.displayMetrics.density)
            p.setMargins(0, 0, margin.toInt(), 0)
            tab.requestLayout()

            //set the custom view
            val tabView = tabLayout.getTabAt(i)
            if (tabView != null) {
                val tabTextView = TextView(this)
                tabView.customView = tabTextView
                //set the text
                tabTextView.text = tabView.text
                //set the currently selected tab font
                if (i == 0) {
                    setSelectedTabFont(applicationContext, tabTextView)
                } else {
                    setUnSelectedTabFont(applicationContext, tabTextView)
                }
            }
        }
    }//setupTabLayout ends

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(null)//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.exchange_service_title)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

    private fun initUiEvents() {
        azerActionBarBackIcon.setOnClickListener {
            onBackPressed()
        }//azerActionBarBackIcon.setOnClickListener ends

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                if (tab != null && tab.customView != null) {
                    val tabTextView = tab.customView as TextView
                    setUnSelectedTabFont(applicationContext, tabTextView)
                }
            }//onTabUnselected ends

            override fun onTabSelected(tab: TabLayout.Tab?) {
                // usageHistoryViewpager.setCurrentItem(tab!!.position, false)
                if (tab?.customView != null) {
                    val tabTextView = tab.customView as TextView
                    setSelectedTabFont(applicationContext, tabTextView)
                    if (responseData != null) {
                        if (tab.text!!.equals(resources.getString(R.string.exchange_service_voice_to_data))) {
                            setSeekBarFromResponse(responseData, true)
                            isDataToVoice = false

                            heading.text =
                                UserDataManager.getPredefineData()?.exchangeService?.vod?.header
                            details.text =
                                UserDataManager.getPredefineData()?.exchangeService?.vod?.footer

                        } else {
                            setSeekBarFromResponse(responseData, false)
                            isDataToVoice = true


                            heading.text =
                                UserDataManager.getPredefineData()?.exchangeService?.dov?.header
                            details.text =
                                UserDataManager.getPredefineData()?.exchangeService?.dov?.footer
                        }
                    }
                }


            }//onTabSelected ends
        })//tabLayout.addOnTabSelectedListener ends

        exchangeButtonEnabled.setOnClickListener {
            if (checkIfCurrentTariffAllowed())
                showExchangeServicePopup(getCoreServicesActivateProcessConfirmationPopup())
            else {
                var cevirErrorMessage = ""
                if (cachedAppResumeData != null &&
                    cachedAppResumeData.data != null &&
                    cachedAppResumeData.data.predefinedData != null &&
                    hasValue(cachedAppResumeData.data.predefinedData!!.cevirErrorMessage)
                ) {
                    cevirErrorMessage =
                        cachedAppResumeData.data.predefinedData!!.cevirErrorMessage!!
                }

                showTariffNotAllowedPopup(cevirErrorMessage)
            }
        }
    }

    var cachedAppResumeData = getLoginAppResumeDataFromLocalPreferences()
    var currentTariff = getCustomerDataFromLocalPreferences()

    private fun checkIfCurrentTariffAllowed(): Boolean {
        currentTariff ?: return false    //check if currentTariff Null then return
        cachedAppResumeData ?: return false //check if currentTariff Null then return

        if (cachedAppResumeData.data != null &&
            hasValue(cachedAppResumeData.data.toString()) && hasValue(cachedAppResumeData.data.predefinedData.toString()) && hasValue(
                cachedAppResumeData.data.predefinedData?.cevirErrorMessage.toString()
            ) && currentTariff != null
        ) {
            for (its in cachedAppResumeData.data.predefinedData?.cevirEligibleTariffs!!) {
                if (currentTariff?.offeringName.equals(its, true))
                    return true

            }
        }


        return false
    }

    private fun showTariffNotAllowedPopup(message: String?) {

        showMessageDialog(this, this, "", message!!)
    }

    val isAllowedTariff = true

    @SuppressLint("InflateParams")
    private fun showExchangeServicePopup(title: String) {
        if (this@ExchangeServiceActivity.isFinishing) return

        val dialogBuilder = AlertDialog.Builder(this@ExchangeServiceActivity)
        val inflater =
            this@ExchangeServiceActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val dialogView = inflater.inflate(R.layout.azer_vas_services_confirmation_popup, null)
        dialogBuilder.setView(dialogView)

        val alertDialog = dialogBuilder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.show()

        val popupTitle = dialogView.findViewById(R.id.popupTitle) as TextView
        popupTitle.typeface = getALSNormalFont()
        popupTitle.text = title
        val popupOkayButton = dialogView.findViewById(R.id.yesButton) as Button
        popupOkayButton.typeface = getALSBoldFont()
        val popupCancelButton = dialogView.findViewById(R.id.noButton) as Button
        popupCancelButton.typeface = getALSBoldFont()

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN) {
            //english
//            popupOkayButton.text = "Yes"
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "No"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ) {
            //azri
//            popupOkayButton.text = "Bəli"
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "Xeyr"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.RU) {
            //russian
//            popupOkayButton.text = "Да"
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "Нет"
        }

        popupOkayButton.setOnClickListener {
            alertDialog.dismiss()
            mViewModel.requestExchangeService(
                isDataToVoice,
                "",
                customSeekBar.voiceValue,
                customSeekBar.dataValue
            )
        }

        popupCancelButton.setOnClickListener {
            alertDialog.dismiss()
        }
    }

    private fun initUi() {
        heading.text = UserDataManager.getPredefineData()?.exchangeService?.vod?.header
        details.text = UserDataManager.getPredefineData()?.exchangeService?.vod?.footer
    }//initUi ends

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, ExchangeServiceActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends

        fun start(context: Context, tabToRedirect: String) {
            val intent = Intent(context, ExchangeServiceActivity::class.java)
            intent.apply {
                if (hasValue(tabToRedirect)) {
                    this.putExtra(
                        ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA,
                        tabToRedirect
                    )
                }
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends

    }//object ends


    private fun getIntentData() {
        intent?.let {
            if (it.hasExtra(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA)
                && hasValue(it.getStringExtra(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA))
            ) {
                tabToRedirect =
                    it.getStringExtra(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA)!!
            }
        }

    }//getIntentData ends

}//class ends