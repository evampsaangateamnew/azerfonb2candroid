package com.azarphone.ui.activities.mainactivity

/**
 * @author Junaid Hassan on 13, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
object MainInjection {

    public fun provideMainRepository(): MainRepositry {
        return MainRepositry()
    }

    public fun provideMainFactory(): MainFactory {
        return MainFactory(provideMainRepository())
    }
}