package com.azarphone.ui.activities.fasttopup

import androidx.lifecycle.MutableLiveData
import com.azarphone.api.pojo.request.DeleteFastPaymentRequest
import com.azarphone.api.pojo.request.MakePaymentRequest
import com.azarphone.api.pojo.response.fasttopupresponse.FastTopUpResponse
import com.azarphone.api.pojo.response.fasttopupresponse.deletefasttopup.DeleteItemResponse
import com.azarphone.api.pojo.response.makepayment.MakePaymentTopUpResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.logE
import com.azarphone.validators.hasValue
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

class FastTopUpViewModel(val fastTopUpRepository: FastTopUpRepository) : BaseViewModel() {
    val getFastPaymentResponseLiveData = MutableLiveData<FastTopUpResponse>()
    val makePaymentResponseLiveData = MutableLiveData<MakePaymentTopUpResponse>()
    val deleteFastPaymentResponseLiveData = MutableLiveData<DeleteItemResponse>()
    val deleteFastPaymentErrorLiveData = MutableLiveData<String>()
    private lateinit var disposable: Disposable

    init {

    }

    fun requestGetFastPayments() {
        view()?.showLoading()

        var mObserver = fastTopUpRepository.requestGetFastPayment()
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    getFastPaymentResponseLiveData.postValue(result)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestGetFastPayments ends

    fun requestDeleteFastPayment(deleteFastPaymentRequest: DeleteFastPaymentRequest) {
        view()?.showLoading()

        var mObserver = fastTopUpRepository.requestDeleteFastPayment(deleteFastPaymentRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true || result.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
//                                    parseDeleteItemResponseInResponse(result)
                                    deleteFastPaymentResponseLiveData.postValue(result)
                                } else {
                                    deleteFastPaymentErrorLiveData.postValue("error")
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                            deleteFastPaymentErrorLiveData.postValue("error")
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestGetFastPayments ends

    fun requestMakePayment(makePaymentRequest: MakePaymentRequest) {
        view()?.showLoading()

        var mObserver = fastTopUpRepository.requestMakePayment(makePaymentRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    makePaymentResponseLiveData.postValue(result)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestGetFastPayments ends

    private fun parseDeleteItemResponseInResponse(response: DeleteItemResponse) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null && hasValue(response.data.toString())) {

                    logE("mySubBG", "bgResponse:::".plus(response.toString()), "FastTopUpViewModel", "parseDeleteItemResponseInResponse")
                    //process the response here
                    deleteFastPaymentResponseLiveData.postValue(response)
                } else {
                    deleteFastPaymentErrorLiveData.postValue("error")
                }
            } else {
                deleteFastPaymentErrorLiveData.postValue("error")
            }
        } else {
            deleteFastPaymentErrorLiveData.postValue("error")
        }
        //hide the dialog
        view()?.hideLoading()
    }//parseResponse ends
}