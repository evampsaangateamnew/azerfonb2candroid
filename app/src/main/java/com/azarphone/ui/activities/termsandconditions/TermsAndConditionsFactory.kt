package com.azarphone.ui.activities.termsandconditions

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class TermsAndConditionsFactory(val termsRepository: TermsAndConditionsRepositry) : ViewModelProvider.NewInstanceFactory() {


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TermsAndConditionsViewModel::class.java)) {
            return TermsAndConditionsViewModel(termsRepository) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}