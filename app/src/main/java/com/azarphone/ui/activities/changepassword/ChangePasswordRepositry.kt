package com.azarphone.ui.activities.changepassword

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.ChangePasswordRequest
import com.azarphone.api.pojo.response.changepassword.ChangePasswordResponse
import io.reactivex.Observable

/**
 * @author Junaid Hassan on 04, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */

class ChangePasswordRepositry {
    init {

    }


    fun changePassword(changePassword:ChangePasswordRequest): Observable<ChangePasswordResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().changePassword(changePassword)
    }

}