package com.azarphone.ui.activities.freesms

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.response.freesmsstatusresponse.FreeSmsStatusResponse
import com.azarphone.api.pojo.response.sendsmsresponse.SendSMSResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.LayoutFreeSmsActivityBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.util.*
import com.azarphone.util.ConstantsUtility.FreeSMSConstants.SMS_LENGTH_AZ
import com.azarphone.util.ConstantsUtility.FreeSMSConstants.SMS_LENGTH_EN
import com.azarphone.validators.hasValue
import com.azarphone.validators.isValidMsisdn
import com.azarphone.widgets.popups.showMessageDialog
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.layout_free_sms_activity.*


/**
 * @author Junaid Hassan on 08, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class FreeSmsActivity : BaseActivity<LayoutFreeSmsActivityBinding, FreeSmsFactory, FreeSmsViewModel>() {

    private val fromClass = "FreeSmsActivity"

    override fun onResume() {
        super.onResume()
        hideKeyboard(this@FreeSmsActivity)
        val text = resources.getString(R.string.free_sms_count, 0)
        smsCountValue.text = text
        val text1 = resources.getString(R.string.free_sms_symbols_lenght, 0)
        symbolLengthValue.text = text1
        detailDescription.setText("")
    }//onResume ends

    override fun getLayoutId(): Int {
        return R.layout.layout_free_sms_activity
    }

    override fun getViewModelClass(): Class<FreeSmsViewModel> {
        return FreeSmsViewModel::class.java
    }

    override fun getFactory(): FreeSmsFactory {
        return FreeSmsInjection.provideFreeSmsFactory()
    }

    override fun init() {
        subscribeForAPIs()
        initUI()
        initUIEvents()
    }

    private fun initUI() {
        setupHeaderActionBar()
        addPrefixToUserNameField(userNameET)
        setupInputFields()
        sendButton.text = getLocalizedSendTitle()
        sendButton.isSelected = true
    }

    private fun getLocalizedSendTitle(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "Send"
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Göndər"
            else -> "Отправить"
        }
    }

    private fun initUIEvents() {
        azerActionBarBackIcon.setOnClickListener {
            onBackPressed()
        }

        sendButton.setOnClickListener {
            sendFreeSms()
        }

        detailDescription.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                val textLength = s.length
                val smsLength: Int
                if (checkIfTextContainOtherThenEnglishCharacters(this@FreeSmsActivity, s.toString())) {
                    smsLength = SMS_LENGTH_AZ
                } else {
                    smsLength = SMS_LENGTH_EN
                }
                if (textLength >= smsLength * 2) {
                    showErrorMessageDialog(applicationContext, this@FreeSmsActivity, "", getString(R.string.more_char_error_message, smsLength * 2))
                }
            }//beforeTextChanged

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                //new code copied from b2c
                val textLength = s.length
                val smsLength: Int

                if (checkIfTextContainOtherThenEnglishCharacters(this@FreeSmsActivity, s.toString())) {
                    smsLength = SMS_LENGTH_AZ
                    if (textLength > SMS_LENGTH_AZ * 2) {
                        showErrorMessageDialog(applicationContext, this@FreeSmsActivity, "", getString(R.string.more_char_error_message, SMS_LENGTH_AZ * 2))
                    }
                } else {
                    smsLength = SMS_LENGTH_EN
                }

                setEditTextMaxLength(smsLength * 2, detailDescription)

                val text = resources.getString(R.string.free_sms_symbols_lenght, textLength)
                symbolLengthValue.text = text
                var smsCount = 0
                if (textLength in 1..smsLength) {
                    smsCount = 1
                } else if (textLength > smsLength && textLength <= smsLength * 2) {
                    smsCount = 2
                }

                val smsText = resources.getString(R.string.free_sms_count, smsCount)
                smsCountValue.text = smsText
            }//onTextChanged ends

            override fun afterTextChanged(s: Editable) {
            }//afterTextChanged ends
        })
    }

    @SuppressLint("InflateParams")
    private fun showErrorMessageDialog(context: Context, activity: Activity, title: String, message: String) {
        try {
            if (activity.isFinishing) return

            val dialogBuilder = AlertDialog.Builder(activity)
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val dialogView = inflater.inflate(R.layout.azer_popup_layout_new, null)
            dialogBuilder.setView(dialogView)

            val alertDialog = dialogBuilder.create()
            alertDialog.setCancelable(false)
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            if (alertDialog != null) {
                if (alertDialog.isShowing) {
                    return
                }
            }
            alertDialog.show()

            val popupTitle = dialogView.findViewById(R.id.popupTitle) as TextView
            popupTitle.typeface = getALSNormalFont()
            val okButton = dialogView.findViewById(R.id.okButton) as Button

            okButton.text = getOkButtonLabel()
            popupTitle.text = message
            okButton.setOnClickListener {
                alertDialog.dismiss()
            }
        } catch (e: Exception) {
            logE("AzerPopup", e.toString(), "AzerFonPopups", "showMessageDialog")
        }//catch ends
    }

    private fun sendFreeSms() {
        val msisdn = userNameET.text.toString().replace(ConstantsUtility.UserConstants.USERNAME_PREFIX, "")
        if (!hasValue(msisdn)) {
            showMessageDialog(applicationContext, this@FreeSmsActivity,
                    resources.getString(R.string.popup_error_title),
                    resources.getString(R.string.no_number))
            return
        } else {
            if (!isValidMsisdn(msisdn)) {
                showMessageDialog(applicationContext, this@FreeSmsActivity,
                        resources.getString(R.string.popup_error_title),
                        resources.getString(R.string.invalid_number))
                return
            }
        }

        if (UserDataManager.getCustomerData() != null &&
                hasValue(UserDataManager.getCustomerData()!!.msisdn) &&
                msisdn == UserDataManager.getCustomerData()!!.msisdn) {
            showMessageDialog(applicationContext, this@FreeSmsActivity,
                    resources.getString(R.string.popup_error_title),
                    getLocalizedErrorTitle())
            return
        }

        if (checkIfTextContainOtherThenEnglishCharacters(this@FreeSmsActivity, detailDescription.text.toString())) {
            if (detailDescription.text.toString().length > SMS_LENGTH_AZ * 2) {
                showErrorMessageDialog(applicationContext, this@FreeSmsActivity, "", resources.getString(R.string.more_char_error_message, SMS_LENGTH_AZ * 2))
                return
            }
        }

        if (!hasValue(detailDescription.text.toString())) {
            showMessageDialog(applicationContext, this@FreeSmsActivity, resources.getString(R.string.popup_error_title),
                    resources.getString(R.string.free_sms_message_error_label))
            return
        }

        var encryptedMessage = ""
        if (hasValue(getBase64EncodedMessage(detailDescription.text.toString(), this@FreeSmsActivity))) {
            encryptedMessage = getBase64EncodedMessage(detailDescription.text.toString(), this@FreeSmsActivity)
        }
        mViewModel.requestSendFreeSms(msisdn, encryptedMessage, ProjectApplication.mLocaleManager.getLanguage())
    }

    private fun getLocalizedErrorTitle(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "You cannot send message to your own number."
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Öz nömrənə mesaj göndərmək mümkün deyil."
            else -> "Ты не можешь посылать сообщения на собственный номер."
        }
    }

    private fun setEditTextMaxLength(length: Int, editText: EditText?) {
        if (editText != null) {
            editText.filters = arrayOf(InputFilter.LengthFilter(length))
        }// if (editText != null) { ends
    }//setEditTextMaxLength ends

    private fun setupInputFields() {
        mobileNumberTIL.defaultHintTextColor = resources.getColorStateList(R.color.brown_grey)
        userNameET.typeface = getALSBoldFont()
    }//setupInputFields ends

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.free_sms_title)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

    private fun subscribeForAPIs() {
        val freeSmsStatustResponseObserver = object : Observer<FreeSmsStatusResponse> {
            override fun onChanged(response: FreeSmsStatusResponse?) {

                onNetSmsLeftTitle.text = resources.getString(R.string.free_sms_on_net_sms_left).plus(" ").plus(response?.data?.onNetSMS)
                /*   showMessageDialog(this@FreeSmsActivity, this@FreeSmsActivity,
                           resources.getString(R.string.popup_ok),
                           response?.resultDesc!!)*/
            }
        }

        val sendFreeSmsResponseObserver = object : Observer<SendSMSResponse> {
            override fun onChanged(response: SendSMSResponse?) {
                detailDescription.setText("")
                showMessageDialog(this@FreeSmsActivity, this@FreeSmsActivity,
                        resources.getString(R.string.popup_ok),
                        response?.resultDesc!!)

                onNetSmsLeftTitle.text = resources.getString(R.string.free_sms_on_net_sms_left).plus(" ").plus(response?.data?.onNetSMS)
            }
        }
        mViewModel.freeSmsStatusResponseLiveData.observe(this@FreeSmsActivity, freeSmsStatustResponseObserver)
        mViewModel.sendFreeSmsResponseLiveData.observe(this@FreeSmsActivity, sendFreeSmsResponseObserver)
    }

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, FreeSmsActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends
    }//object ends

}