package com.azarphone.ui.activities.storelocator

import androidx.lifecycle.MutableLiveData
import com.azarphone.api.pojo.response.storelocator.StoreLocatorResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.util.setStoreLocatorCachedDataToLocalPreferences
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

class StoreLocatorViewModel(val mStoreRepository: StoreLocatorRepositry) : BaseViewModel() {

    private lateinit var disposable: Disposable
    val storeLocatorResponse = MutableLiveData<StoreLocatorResponse>()
    init {

    }

    fun getStoreLocator() {
        view()?.showLoading()
        var mObserver = mStoreRepository.getStoreDetails()
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            if (view()?.isErrorErrorDialoge(result) != true) {
                                storeLocatorResponse.postValue(result)
                                setStoreLocatorCachedDataToLocalPreferences(result)
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)

    }

    fun getStoreLocatorInBackground() {
        var mObserver = mStoreRepository.getStoreDetails()
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            storeLocatorResponse.postValue(result)
                            setStoreLocatorCachedDataToLocalPreferences(result)
                        },
                        { error ->
                            view()?.hideLoading()
                        }
                )

        compositeDisposables?.add(disposable)

    }
}