package com.azarphone.ui.activities.notification

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.response.notification.NotificationResponse
import io.reactivex.Observable

class NotificationRepositry {

    fun getNotifications(): Observable<NotificationResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().getNotifications()
    }
}