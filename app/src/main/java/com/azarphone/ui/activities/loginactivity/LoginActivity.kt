package com.azarphone.ui.activities.loginactivity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.azarphone.R
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.LoginActivityBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.ConstantsUtility.ManageAccount.LOGIN_USECASE_TYPE_KEY
import kotlinx.android.synthetic.main.app_header_layout.*

class LoginActivity() : BaseActivity<LoginActivityBinding, LoginFactory, LoginViewModel>() {

    override fun onResume() {
        super.onResume()
        UserDataManager.setPromoMessage(null)
        UserDataManager.setIsPromoMessageDisplayed(false)
    }

    override fun getViewModelClass(): Class<LoginViewModel> {
        return LoginViewModel::class.java
    }

    override fun getFactory(): LoginFactory {
        return LoginInjectionUtils.provideLoginFactory()
    }

    override fun getLayoutId(): Int {
        return R.layout.login_activity
    }

    override fun init() {

        if (intent.extras != null) {
            getDataFromIntent(intent.extras!!)

        }

        if (mViewModel.useCase_Type.equals(ConstantsUtility.ManageAccount.LOGIN_USECASE_ADDACCOUNT)) {
            languageTV.visibility = View.INVISIBLE
            azerActionBarBackIconBacker.visibility = View.VISIBLE
        } else {
            languageTV.visibility = View.VISIBLE
            azerActionBarBackIconBacker.visibility = View.INVISIBLE
        }

        azerActionBarBackIconBacker.setOnClickListener {
            onBackPressed()
        }
    }

    private fun getDataFromIntent(extras: Bundle) {
        if (extras.containsKey(LOGIN_USECASE_TYPE_KEY)) {
            mViewModel.useCase_Type = extras.getString(LOGIN_USECASE_TYPE_KEY)!!
        }
    }


    companion object {
        fun start(context: Context) {
            val intent = Intent(context, LoginActivity::class.java)
            intent.addFlags(
                    Intent.FLAG_ACTIVITY_NEW_TASK
                            or
                            Intent.FLAG_ACTIVITY_CLEAR_TASK
                            or
                            Intent.FLAG_ACTIVITY_CLEAR_TOP)
            context.startActivity(intent)
        }


    }

}