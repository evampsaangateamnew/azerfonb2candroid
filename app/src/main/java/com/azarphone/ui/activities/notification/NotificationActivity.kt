package com.azarphone.ui.activities.notification

import android.content.Context
import android.content.Intent
import android.os.Handler
import android.view.KeyEvent
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.azarphone.R
import com.azarphone.api.pojo.response.notification.NotificationsListItem
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.NotificationLayoutBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.NotificationsCloserEvents
import com.azarphone.ui.activities.mainactivity.MainActivity
import com.azarphone.ui.adapters.recyclerviews.NotificationsAdapterNew
import com.azarphone.util.RootValues
import com.azarphone.util.isNetworkAvailable
import com.azarphone.validators.hasValue
import com.azarphone.widgets.popups.showMessageDialog
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.notification_layout.*
import me.leolin.shortcutbadger.ShortcutBadger


@Suppress("DEPRECATION")
class NotificationActivity : BaseActivity<NotificationLayoutBinding, NotificationFactory, NotificationViewModel>() {

    private var notifyDataKey = ""

    override fun onResume() {
        super.onResume()
        initIntentData()
    }

    override fun getLayoutId(): Int {
        return R.layout.notification_layout
    }//getLayoutId ends

    override fun getViewModelClass(): Class<NotificationViewModel> {
        return NotificationViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): NotificationFactory {
        return NotificationInjection.provideNotificationFactory()
    }//getFactory ends

    override fun init() {
        setupHeaderActionBar()
        subscribe()
        initUIEvents()
        if (isNetworkAvailable(applicationContext)) {
            Handler().postDelayed({ mViewModel.getNotifications() }, 50L)
        } else {
            showMessageDialog(applicationContext, this@NotificationActivity, "",
                    resources.getString(R.string.message_no_internet))
        }
        initCloseThisScreenEvents()
    }

    private fun initIntentData() {
        //get the extras data
        if (intent.extras != null) {
            val bundle = intent.extras
            if (bundle != null) {
                if (bundle.containsKey("notifyDataKey")) {
                    notifyDataKey = bundle.getString("notifyDataKey")!!
                }
            }
        }
    }

    private fun initCloseThisScreenEvents() {
        val notificationsCloserEvents = object : NotificationsCloserEvents {
            override fun onCloseThisScreen() {
                finish()
            }
        }

        RootValues.setNotificationsCloserEvents(notificationsCloserEvents)
    }

    private fun initUIEvents() {
        azerActionBarBackIcon.setOnClickListener {
            if (hasValue(notifyDataKey)) {
                UserDataManager.resetUserDataManager()
                MainActivity.start(applicationContext, "")
            } else {
                finish()
            }
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (hasValue(notifyDataKey)) {
                UserDataManager.resetUserDataManager()
                MainActivity.start(applicationContext, "")
            } else {
                finish()
            }
            return true
        }
        return super.onKeyDown(keyCode, event)
    }//onKeyDown ends

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.notifications_screen_title)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

    fun subscribe() {
        val mNotificationListObserver = object : Observer<List<NotificationsListItem>> {
            override fun onChanged(list: List<NotificationsListItem>?) {

                list?.let {
                    if (list != null && list.isNotEmpty()) {
                        showContent()
                        viewDataBinding.notificationRecycler.apply {
                            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
                            adapter = NotificationsAdapterNew(applicationContext, list)
                            ShortcutBadger.removeCount(applicationContext)//remove the app icon counter for notifications
                        }
                    } else {
                        hideContent()
                    }

                }
            }

        }
        mViewModel.mNotificationList.observe(this@NotificationActivity, mNotificationListObserver)

    }

    private fun hideContent() {
        noDataFoundLayout.visibility = View.VISIBLE
        notificationRecycler.visibility = View.GONE
    }

    private fun showContent() {
        noDataFoundLayout.visibility = View.GONE
        notificationRecycler.visibility = View.VISIBLE
    }

    companion object {
        fun start(context: Context, fcmKey: String) {
            val intent = Intent(context, NotificationActivity::class.java)
            intent.addFlags(
                    Intent.FLAG_ACTIVITY_NEW_TASK
                            or
                            Intent.FLAG_ACTIVITY_CLEAR_TASK
                            or
                            Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.putExtra("notifyDataKey", fcmKey)
            context.startActivity(intent)
        }

        fun start(context: Context) {
            val intent = Intent(context, NotificationActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }
    }

}