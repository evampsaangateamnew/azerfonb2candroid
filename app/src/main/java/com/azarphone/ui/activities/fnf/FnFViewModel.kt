package com.azarphone.ui.activities.fnf

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.request.AddFnfRequest
import com.azarphone.api.pojo.request.DeleteFnfRequest
import com.azarphone.api.pojo.request.FnFRequest
import com.azarphone.api.pojo.request.UpdateFNFRequest
import com.azarphone.api.pojo.response.addfnfresponse.AddFnfResponse
import com.azarphone.api.pojo.response.deletefnfresponse.DeleteFnfResponse
import com.azarphone.api.pojo.response.fnfresponse.FNFResponse
import com.azarphone.api.pojo.response.fnfresponse.FnfListItem
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.updatefnfresponse.UpdateFNFResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.ui.dialogs.InAppThankYouDialog
import com.azarphone.util.ConstantsUtility
import com.azarphone.validators.hasValue
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

class FnFViewModel(val fnfRepositry: FnFRepositry) : BaseViewModel() {

    private val fromClass = "FNFViewModel"
    private lateinit var disposable: Disposable
    val fnfListResponse = MutableLiveData<FNFResponse>()
    val fnfListErrorResponse = MutableLiveData<AzerAPIErrorHelperResponse>()
    val addFNFResponse = MutableLiveData<AddFnfResponse>()
    val updateFNFResponse = MutableLiveData<UpdateFNFResponse>()
    val deleteFNFResponse = MutableLiveData<DeleteFnfResponse>()
    val inAppSurveyResponseErrorResponseData = MutableLiveData<AzerAPIErrorHelperResponse>()
    var isFirstTime="true"

    init {

    }
    fun uploadInAppSurvey(context: Context, uploadSurvey: UploadSurvey, onTransactionComplete: String) {
        view()?.showLoading()
        var observable = fnfRepositry.saveInAppSurvey(uploadSurvey)
        observable = observable.onErrorReturn { throwable -> null }
        disposable = observable
            .compose(applyIOSchedulers())
            .subscribe(
                { appSurvey ->
                    view()?.hideLoading()
                    when {
                        appSurvey?.data != null -> {
                            UserDataManager.setInAppSurvey(appSurvey)
                            val thankYouDialog =
                                InAppThankYouDialog(context, onTransactionComplete)
                            thankYouDialog.show()
                        }
                        appSurvey?.resultDesc != null -> {
                            inAppSurveyResponseErrorResponseData.postValue(
                                AzerAPIErrorHelperResponse(
                                    ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                    appSurvey.resultDesc
                                )
                            )
                        }
                        else -> {
                            inAppSurveyResponseErrorResponseData.postValue(
                                AzerAPIErrorHelperResponse(
                                    ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                    context.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            )
                        }
                    }
                },
                { error ->
                    inAppSurveyResponseErrorResponseData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            error.message
                        )
                    )
                }
            )
        compositeDisposables?.add(disposable)
    }
    fun requestFNFList() {
        view()?.showLoading()
        val offeringID = UserDataManager.getCustomerData()?.offeringId
        val moneyTransferRequest = FnFRequest(offeringID)

        var mObserver = fnfRepositry.requestFnFList(moneyTransferRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            parseFnFListResponse(result)
                        },
                        { error ->
                            view()?.hideLoading()
                           // fnfListErrorResponse.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                        }
                )

        compositeDisposables?.add(disposable)
    }

    private fun parseFnFListResponse(response: FNFResponse) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        fnfListResponse.postValue(response)
                    } else {
                        /**the response has no data object
                         * in this case we have to handle the ui only*/
                      //  fnfListErrorResponse.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                    }
                } else {
                    /**the response has no data object
                     * in this case we have to handle the ui only*/
                  //  fnfListErrorResponse.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                }
            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                /**force log out user case
                 * when the server sends the result code as 7*/
                fnfListErrorResponse.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT, ""))
            } else {
                /**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*/
                if (hasValue(response.resultDesc.toString())) {
                  //  fnfListErrorResponse.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
                } else {
                  //  fnfListErrorResponse.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
                }
            }
        } else {
            /**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*/
            if (hasValue(response.resultDesc.toString())) {
                fnfListErrorResponse.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
            } else {
                fnfListErrorResponse.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }//parseFnFListResponse ends

    fun requestAddFNF(msisdnToBeAdded: String) {
        view()?.showLoading()
        val offeringID = UserDataManager.getCustomerData()?.offeringId
        val addFnfRequest = AddFnfRequest(msisdnToBeAdded, offeringID,isFirstTime)

        var mObserver = fnfRepositry.requestAddFNF(addFnfRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    addFNFResponse.postValue(result)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }

    fun requestUpdateFNF(actionType:String,oldMsisdn:String,newMsisdn: String) {
        view()?.showLoading()
        val offeringID = UserDataManager.getCustomerData()?.offeringId
        val updateFnfRequest = UpdateFNFRequest(actionType,oldMsisdn,newMsisdn, offeringID)

        var mObserver = fnfRepositry.requestUpdateFNF(updateFnfRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    updateFNFResponse.postValue(result)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }

    fun requestClearAllFNF(listOfMsisdnsToDelete: List<String>) {
        view()?.showLoading()
        val offeringID = UserDataManager.getCustomerData()?.offeringId
        val fnfRequest = DeleteFnfRequest(offeringID, "",listOfMsisdnsToDelete)
        disposable = fnfRepositry.requestDeleteFNF(fnfRequest)
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    deleteFNFResponse.postValue(result)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }
}