package com.azarphone.ui.activities.splashactivity

import com.azarphone.ui.activities.loginactivity.LoginFactory
import com.azarphone.ui.activities.loginactivity.LoginRepository

object SplashInjectionUtils{

    fun provideSplashRepository(): SplashRepository {
        return SplashRepository()
    }

    fun provideSplashFactory(): SplashFactory {
        return SplashFactory(provideSplashRepository())
    }

}