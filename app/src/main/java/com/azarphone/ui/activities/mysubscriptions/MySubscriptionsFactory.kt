package com.azarphone.ui.activities.mysubscriptions

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

/**
 * @author Junaid Hassan on 22, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class MySubscriptionsFactory(val mMySubscriptionsRepositry: MySubscriptionsRepositry) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MySubscriptionsViewModel::class.java)) {
            return MySubscriptionsViewModel(mMySubscriptionsRepositry) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}