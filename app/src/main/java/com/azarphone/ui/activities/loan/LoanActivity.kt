package com.azarphone.ui.activities.loan

import android.content.Context
import android.content.Intent
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.azarphone.R
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.LayoutActivityLoanBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.ui.adapters.pagers.LoanPagerAdapter
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.layout_activity_loan.*


/**
 * @author Junaid Hassan on 05, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class LoanActivity : BaseActivity<LayoutActivityLoanBinding, LoanFactory, LoanViewModel>() {

    private val fromClass = "LoanActivity"

    override fun getLayoutId(): Int {
        return R.layout.layout_activity_loan
    }//getLayoutId ends

    override fun getViewModelClass(): Class<LoanViewModel> {
        return LoanViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): LoanFactory {
        return LoanInjection.provideLoanFactory()
    }//getFactory ends

    override fun init() {
        setupHeaderActionBar()

        initUi()
        initUiEvents()
        setupTabLayout()

        //setup viewPager
        loanViewpager.adapter = LoanPagerAdapter(supportFragmentManager)

        val limit = if (LoanPagerAdapter(supportFragmentManager).count > 1) LoanPagerAdapter(supportFragmentManager).count - 1 else 1
        loanViewpager.offscreenPageLimit = limit
    }//init ends

    private fun setupTabLayout() {
        tabLayout.addTab(tabLayout.newTab().setText(resources.getString(R.string.loan_tab_title)))
        tabLayout.addTab(tabLayout.newTab().setText(resources.getString(R.string.loan_request_tab_title)))
        tabLayout.addTab(tabLayout.newTab().setText(resources.getString(R.string.loan_payment_tab_title)))

        //set the first tab by default
        tabLayout.getTabAt(0)!!.select()

        for (i in 0 until tabLayout.tabCount) {
            val tab = (tabLayout.getChildAt(0) as ViewGroup).getChildAt(i)
            val p = tab.layoutParams as ViewGroup.MarginLayoutParams
            val margin = (15 * resources.displayMetrics.density)
            p.setMargins(0, 0, margin.toInt(), 0)
            tab.requestLayout()

            //set the custom view
            val tabView = tabLayout.getTabAt(i)
            if (tabView != null) {
                val tabTextView = TextView(this)
                tabView.customView = tabTextView
                //set the text
                tabTextView.text = tabView.text
                //set the currently selected tab font
                if (i == 0) {
                    setSelectedTabFont(applicationContext, tabTextView)
                } else {
                    setUnSelectedTabFont(applicationContext, tabTextView)
                }
            }
        }
    }//setupTabLayout ends

    private fun initUiEvents() {
        azerActionBarBackIcon.setOnClickListener {
            onBackPressed()
        }//azerActionBarBackIcon.setOnClickListener ends

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {

            }//onTabReselected ends

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                if (tab != null && tab.customView != null) {
                    val tabTextView = tab.customView as TextView
                    setUnSelectedTabFont(applicationContext, tabTextView)
                }
            }//onTabUnselected ends

            override fun onTabSelected(tab: TabLayout.Tab?) {
                loanViewpager.setCurrentItem(tab!!.position, false)
                if (tab.customView != null) {
                    val tabTextView = tab.customView as TextView
                    setSelectedTabFont(applicationContext, tabTextView)
                }

                logE("ViewPaX", "position:::".plus(loanViewpager.currentItem), fromClass, "onTabSelected")
                if (loanViewpager.currentItem == 0) {

                }

                if (loanViewpager.currentItem == 1) {
                    if (UserDataManager.getRequestHistoryResponse() == null) {
                        onCreateRequestHistoryProcess()
                    }
                }

                if (loanViewpager.currentItem == 2) {
                    if (UserDataManager.getPaymentHistoryResponse() == null) {
                        onCreatePaymentHistoryProcess()
                    }
                }
            }//onTabSelected ends
        })//tabLayout.addOnTabSelectedListener ends
    }//initUiEvents ends

    private fun onCreatePaymentHistoryProcess() {
        try {
            //get the date
            val date = getLastDateMinusDays(getCurrentDate(), ConstantsUtility.DateFormats.CURRENT_DAY)
            updateDatesForPaymentHistory(date, getCurrentDate())
        } catch (exp: Exception) {
            logE("On&8", "error:::".plus(exp.toString()), fromClass, "initUI")
        }
    }

    private fun updateDatesForPaymentHistory(date: String, currentDate: String) {
        var formattedStartedDate = ""
        if (hasValue(changeDateFormat(date))) {
            formattedStartedDate = changeDateFormat(date)!!
        }

        var formattedEndDate = ""
        if (hasValue(changeDateFormat(currentDate))) {
            formattedEndDate = changeDateFormat(currentDate)!!
        }
        mViewModel.requestPaymentHistory(formattedStartedDate, formattedEndDate)
    }

    private fun onCreateRequestHistoryProcess() {
        try {
            //get the date
            val date = getLastDateMinusDays(getCurrentDate(), ConstantsUtility.DateFormats.CURRENT_DAY)
            updateDatesForRequestHistory(date, getCurrentDate())
        } catch (exp: Exception) {
            logE("On&8", "error:::".plus(exp.toString()), fromClass, "initUI")
        }
    }

    private fun updateDatesForRequestHistory(date: String, currentDate: String) {
        var formattedStartedDate = ""
        if (hasValue(changeDateFormat(date))) {
            formattedStartedDate = changeDateFormat(date)!!
        }

        var formattedEndDate = ""
        if (hasValue(changeDateFormat(currentDate))) {
            formattedEndDate = changeDateFormat(currentDate)!!
        }
        mViewModel.requestHistory(formattedStartedDate, formattedEndDate)
    }

    private fun initUi() {

    }//initUi ends

    override fun onBackPressed() {
        super.onBackPressed()
        UserDataManager.setRequestHistoryResponse(null)
        UserDataManager.setPaymentHistoryResponse(null)
        finish()
    }

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(null)//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.loan_screen_title)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, LoanActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends
    }//object ends

}//class