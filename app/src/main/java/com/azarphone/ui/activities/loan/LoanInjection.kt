package com.azarphone.ui.activities.loan

/**
 * @author Junaid Hassan on 05, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
object LoanInjection {

    fun provideLoanRepository(): LoanRepositry {
        return LoanRepositry()
    }

    fun provideLoanFactory(): LoanFactory {
        return LoanFactory(provideLoanRepository())
    }

}