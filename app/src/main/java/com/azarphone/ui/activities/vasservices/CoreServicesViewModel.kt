package com.azarphone.ui.activities.vasservices

import androidx.lifecycle.MutableLiveData
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.request.CoreServiceProcessRequest
import com.azarphone.api.pojo.request.CoreServicesRequest
import com.azarphone.api.pojo.request.SupplementaryOfferSubscribeRequest
import com.azarphone.api.pojo.response.coreservices.CoreServicesResponse
import com.azarphone.api.pojo.response.coreservicesprocessnumber.CoreServicesProcessResponse
import com.azarphone.api.pojo.response.mysubscriptions.successresponse.MySubscriptionsSuccessAfterOfferSubscribeResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.RootValues
import com.azarphone.util.logE
import com.azarphone.validators.hasValue
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

/**
 * @author Junaid Hassan on 24, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class CoreServicesViewModel(val mCoreServicesRepositry: CoreServicesRepositry) : BaseViewModel() {

    private lateinit var disposable: Disposable
    val coreServicesBackgroundResponseLiveData= MutableLiveData<CoreServicesResponse>()
    val coreServicesResponseLiveData = MutableLiveData<CoreServicesResponse>()
    val coreServicesErrorResponseLiveData = MutableLiveData<AzerAPIErrorHelperResponse>()
    private val fromClass = "CoreServicesViewModel"
    val coreServicesForwardNumberErrorResponseLiveData = MutableLiveData<AzerAPIErrorHelperResponse>()
    val coreServicesForwardNumberResponseLiveData = MutableLiveData<CoreServicesProcessResponse>()

    val coreServiceProcessErrorResponseLiveData = MutableLiveData<AzerAPIErrorHelperResponse>()
    val coreServiceProcessResponseLiveData = MutableLiveData<CoreServicesProcessResponse>()

    val callDivertDisableResponseData = MutableLiveData<MySubscriptionsSuccessAfterOfferSubscribeResponse>()
    val callDivertDisableErrorResponseData = MutableLiveData<AzerAPIErrorHelperResponse>()

    init {

    }//init ends

    fun requestCoreServicesBackground(groupType: String, brand: String, userType: String, isFrom: String) {
        var accountType = "Individual"
        if (UserDataManager.getCustomerData() != null) {
            if (hasValue(UserDataManager.getCustomerData()!!.customerType)) {
                accountType = if (UserDataManager.getCustomerData()!!.customerType.equals(ConstantsUtility.UserConstants.CUSTOMER_TYPE_DATA_SIM_PREPAID) ||
                        UserDataManager.getCustomerData()!!.customerType.equals(ConstantsUtility.UserConstants.CUSTOMER_TYPE_DATA_SIM_POSTPAID)) {
                    "wxxt"
                } else if (UserDataManager.getCustomerData()!!.customerType.equals(ConstantsUtility.UserConstants.CUSTOMER_TYPE_INDIVIDUAL)) {
                    "individual"
                } else if (UserDataManager.getCustomerData()!!.customerType.equals(ConstantsUtility.UserConstants.CUSTOMER_TYPE_FULL)) {
                    "full"
                } else if (UserDataManager.getCustomerData()!!.customerType.equals(ConstantsUtility.UserConstants.CORPORATE_CUSTOMER)) {
                    "corporate"
                } else {
                    "Individual"
                }
            }
        }


        logE("coreServices", "calledBG:::requestCoreServices", fromClass, "requestCoreServicesBackground")
        val coreServicesRequest = CoreServicesRequest(accountType, groupType, brand, userType, isFrom)

        var mObserver = mCoreServicesRepositry.requestCoreServices(coreServicesRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            logE("coreServices", "response1BG:::".plus(result.toString()), fromClass, "requestCoreServicesBackground")
                            coreServicesBackgroundResponseLiveData.postValue(result)
                        },
                        { error ->
                            view()?.hideLoading()
                        }
                )

        compositeDisposables?.add(disposable)
    }

    fun requestCoreServices(groupType: String, brand: String, userType: String, isFrom: String) {
        var accountType = "Individual"
        if (UserDataManager.getCustomerData() != null) {
            if (hasValue(UserDataManager.getCustomerData()!!.customerType)) {
                accountType = if (UserDataManager.getCustomerData()!!.customerType.equals(ConstantsUtility.UserConstants.CUSTOMER_TYPE_DATA_SIM_PREPAID) ||
                        UserDataManager.getCustomerData()!!.customerType.equals(ConstantsUtility.UserConstants.CUSTOMER_TYPE_DATA_SIM_POSTPAID)) {
                    "wxxt"
                } else if (UserDataManager.getCustomerData()!!.customerType.equals(ConstantsUtility.UserConstants.CUSTOMER_TYPE_INDIVIDUAL)) {
                    "individual"
                } else if (UserDataManager.getCustomerData()!!.customerType.equals(ConstantsUtility.UserConstants.CUSTOMER_TYPE_FULL)) {
                    "full"
                } else if (UserDataManager.getCustomerData()!!.customerType.equals(ConstantsUtility.UserConstants.CORPORATE_CUSTOMER)) {
                    "corporate"
                } else {
                    "Individual"
                }
            }
        }

        logE("coreServices", "called:::requestCoreServices", fromClass, "requestCoreServicesBackground")
        view()?.showLoading()
        val coreServicesRequest = CoreServicesRequest(accountType, groupType, brand, userType, isFrom)

        var mObserver = mCoreServicesRepositry.requestCoreServices(coreServicesRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            logE("coreServices", "response1:::".plus(result.toString()), fromClass, "subscribe")
                            parseResponse(result)
                        },
                        { error ->
                            view()?.hideLoading()
                            coreServicesErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestCoreServices ends

    fun requestSetForwardNumberForCoreService(actionType: String, offeringId: String, groupType: String, msisdnToForwardNumberOn: String, accountType: String) {
        logE("forwardNumber", "called:::requestSetForwardNumberForCore", fromClass, "requestSetForwardNumberForCore")
        view()?.showLoading()
        val coreServiceProcessRequest = CoreServiceProcessRequest(actionType, offeringId, groupType, msisdnToForwardNumberOn, accountType)

        var mObserver = mCoreServicesRepositry.requestProcessCoreService(coreServiceProcessRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            logE("coreServices", "response1:::".plus(result.toString()), fromClass, "subscribe")
                            parseNumberForwardResponse(result)
                        },
                        { error ->
                            view()?.hideLoading()
                            coreServicesForwardNumberErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestCoreServices ends

    fun changeSupplementaryOfferForCallDivert(offeringName: String, offeringId: String, actionType: String) {
        view()?.showLoading()
        val supplementaryOfferSubscribeRequest = SupplementaryOfferSubscribeRequest(offeringName, offeringId, actionType)

        var mObserver = mCoreServicesRepositry.changeSupplementaryOfferForCallDivert(supplementaryOfferSubscribeRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            parseCallDivertDisableResponse(result, offeringId)
                        },
                        { error ->
                            view()?.hideLoading()
                            callDivertDisableErrorResponseData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                        }
                )

        compositeDisposables?.add(disposable)
    }//subscribeToSupplementaryOffer ends

    fun requestProcessCoreService(actionType: String, offeringId: String, groupType: String, msisdnToForwardNumberOn: String, accountType: String) {
        logE("coreServices", "called:::requestProcessCoreService", fromClass, "requestProcessCoreService")
        view()?.showLoading()
        val coreServiceProcessRequest = CoreServiceProcessRequest(actionType, offeringId, groupType, msisdnToForwardNumberOn, accountType)


        var mObserver = mCoreServicesRepositry.requestProcessCoreService(coreServiceProcessRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            logE("coreServices", "response:::".plus(result.toString()), fromClass, "requestProcessCoreService")
                            parseProcessCoreServiceResponse(result, offeringId)
                        },
                        { error ->
                            view()?.hideLoading()
                            coreServiceProcessErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestProcessCoreService ends

    private fun parseCallDivertDisableResponse(response: MySubscriptionsSuccessAfterOfferSubscribeResponse, offeringId: String) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        callDivertDisableResponseData.postValue(response)
                        RootValues.getCoreServiceAdapterUpdaterEvents().onCallDivertDisabledUpdateAdapter(offeringId)
                    } else {
                        /**the response has no data object
                         * in this case we have to handle the ui only*/
                        callDivertDisableErrorResponseData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                    }
                } else {
                    /**the response has no data object
                     * in this case we have to handle the ui only*/
                    callDivertDisableErrorResponseData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                }
            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                /**force log out user case
                 * when the server sends the result code as 7*/
                callDivertDisableErrorResponseData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT, ""))
            } else {
                /**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*/
                if (hasValue(response.resultDesc.toString())) {
                    callDivertDisableErrorResponseData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
                } else {
                    callDivertDisableErrorResponseData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
                }
            }
        } else {
            /**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*/
            if (hasValue(response.resultDesc.toString())) {
                callDivertDisableErrorResponseData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
            } else {
                callDivertDisableErrorResponseData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }

    private fun parseProcessCoreServiceResponse(response: CoreServicesProcessResponse, offeringId: String) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        UserDataManager.setSuccessOfferingIdForCoreService(offeringId)
                        //process the response here
                        coreServiceProcessResponseLiveData.postValue(response)
                    } else {
                        /**the response has no data object
                         * in this case we have to handle the ui only*/
                        coreServiceProcessErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                    }
                } else {
                    /**the response has no data object
                     * in this case we have to handle the ui only*/
                    coreServiceProcessErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                }
            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                /**force log out user case
                 * when the server sends the result code as 7*/
                coreServiceProcessErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT, ""))
            } else {
                /**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*/
                if (hasValue(response.resultDesc.toString())) {
                    coreServiceProcessErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
                } else {
                    coreServiceProcessErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
                }
            }
        } else {
            /**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*/
            if (hasValue(response.resultDesc.toString())) {
                coreServiceProcessErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
            } else {
                coreServiceProcessErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }//parseProcessCoreServiceResponse ends

    private fun parseNumberForwardResponse(response: CoreServicesProcessResponse) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        coreServicesForwardNumberResponseLiveData.postValue(response)
                    } else {
                        /**the response has no data object
                         * in this case we have to handle the ui only*/
                        coreServicesForwardNumberErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                    }
                } else {
                    /**the response has no data object
                     * in this case we have to handle the ui only*/
                    coreServicesForwardNumberErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                }
            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                /**force log out user case
                 * when the server sends the result code as 7*/
                coreServicesForwardNumberErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT, ""))
            } else {
                /**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*/
                if (hasValue(response.resultDesc.toString())) {
                    coreServicesForwardNumberErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
                } else {
                    coreServicesForwardNumberErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
                }
            }
        } else {
            /**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*/
            if (hasValue(response.resultDesc.toString())) {
                coreServicesForwardNumberErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
            } else {
                coreServicesForwardNumberErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }//parseNumberForwardResponse ends

    private fun parseResponse(response: CoreServicesResponse) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        coreServicesResponseLiveData.postValue(response)
                    } else {
                        /**the response has no data object
                         * in this case we have to handle the ui only*/
                        coreServicesErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                    }
                } else {
                    /**the response has no data object
                     * in this case we have to handle the ui only*/
                    coreServicesErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                }
            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                /**force log out user case
                 * when the server sends the result code as 7*/
                coreServicesErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT, ""))
            } else {
                /**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*/
                if (hasValue(response.resultDesc.toString())) {
                    coreServicesErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
                } else {
                    coreServicesErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
                }
            }
        } else {
            /**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*/
            if (hasValue(response.resultDesc.toString())) {
                coreServicesErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
            } else {
                coreServicesErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }//parseResponse ends
}//class ends