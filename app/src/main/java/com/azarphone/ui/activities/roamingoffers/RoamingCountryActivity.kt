package com.azarphone.ui.activities.roamingoffers

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.response.inappsurvey.InAppSurvey
import com.azarphone.api.pojo.response.inappsurvey.Surveys
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.mysubscriptions.successresponse.MySubscriptionsSuccessAfterOfferSubscribeResponse
import com.azarphone.api.pojo.response.supplementaryoffers.helper.OfferFiltersMain
import com.azarphone.api.pojo.response.supplementaryoffers.helper.SupplementaryOffer
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.LayoutRoamingOffersActivityBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.*
import com.azarphone.ui.adapters.recyclerviews.SupplementaryOffersCardAdapter
import com.azarphone.ui.dialogs.InAppFeedbackDialog
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.widgets.popups.SupplementaryOffersFilterPopup
import com.azarphone.widgets.popups.showMessageDialog
import com.es.tec.LocalSharedPrefStorage
import com.yarolegovich.discretescrollview.Orientation
import com.yarolegovich.discretescrollview.transform.ScaleTransformer
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.layout_roaming_offers_activity.*
import kotlinx.android.synthetic.main.layout_roaming_offers_activity.mDiscreteView
import kotlinx.android.synthetic.main.layout_roaming_offers_activity.noDataFoundLayout
import kotlinx.android.synthetic.main.layout_supplementary_offers_pagger_fragment.view.*
import kotlinx.android.synthetic.main.layout_tariff_card_fragment.*
import kotlinx.android.synthetic.main.no_data_found_white_no_bg.*
import java.util.*

/**
 * @author Junaid Hassan on 24, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class RoamingCountryActivity :
    BaseActivity<LayoutRoamingOffersActivityBinding, RoamingCountryFactory, RoamingCountryViewModel>(),
    OffersSearchResultListener {

    private val DIALOG_FILTER = "supplementary.filter.roaming"
    private var offerFilter: OfferFiltersMain? = null
    private var roaming: ArrayList<SupplementaryOffer?>? = null
    private val fromClass = "RoamingCountryActivity"
    private var selectedCountryName = ""
    private var selectedCountryFlag = ""
    private var adapterSupplementaryOffersCard: SupplementaryOffersCardAdapter? = null

    override fun getLayoutId(): Int {
        return R.layout.layout_roaming_offers_activity
    }//getLayoutId ends

    override fun getViewModelClass(): Class<RoamingCountryViewModel> {
        return RoamingCountryViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): RoamingCountryFactory {
        return RoamingCountryInjection.provideRoamingCountryFactory()
    }//getFactory ends

    override fun init() {
        setupHeaderActionBar()
        subscribe()
        getIntentExtrasData()
        initFilterApplyEvents()
        initRoamingCardEvents()
        initUI()
        initUiEvents()
    }//init ends

    private fun subscribe() {
        val subscribeToRoamingOfferOfferResponseLiveData =
            object : Observer<MySubscriptionsSuccessAfterOfferSubscribeResponse> {
                override fun onChanged(t: MySubscriptionsSuccessAfterOfferSubscribeResponse?) {
                    if (!this@RoamingCountryActivity.isFinishing) {
                        if (t != null && t.data != null && t.data.mySubscriptionsData != null) {

                            if (t.data.message != null && hasValue(t.data.message)) {
                                inAppFeedback(
                                    this@RoamingCountryActivity,
                                    t.data.message
                                )
//                                showMessageDialog(
//                                    applicationContext, this@RoamingCountryActivity,
//                                    resources.getString(R.string.popup_note_title),
//                                    t.data.message
//                                )
                            }

                            /** update the mysubscriptions response*/
                            if (UserDataManager.getMySubscriptionResponse() != null) {
                                val mySubscriptions = UserDataManager.getMySubscriptionResponse()

                                if (mySubscriptions!!.data != null &&
                                    t.data.mySubscriptionsData.internet != null
                                ) {
                                    mySubscriptions.data!!.internet =
                                        t.data.mySubscriptionsData.internet
                                }

                                if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.sms != null
                                ) {
                                    mySubscriptions.data!!.sms = t.data.mySubscriptionsData.sms
                                }

                                if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.call != null
                                ) {
                                    mySubscriptions.data!!.call = t.data.mySubscriptionsData.call
                                }

                                if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.campaign != null
                                ) {
                                    mySubscriptions.data!!.campaign =
                                        t.data.mySubscriptionsData.campaign
                                }

                                if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.hybrid != null
                                ) {
                                    mySubscriptions.data!!.hybrid =
                                        t.data.mySubscriptionsData.hybrid
                                }

                                if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.voiceInclusiveOffers != null
                                ) {
                                    mySubscriptions.data!!.voiceInclusiveOffers =
                                        t.data.mySubscriptionsData.voiceInclusiveOffers
                                }

                                if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.roaming != null
                                ) {
                                    mySubscriptions.data!!.roaming =
                                        t.data.mySubscriptionsData.roaming
                                }

                                if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.smsInclusiveOffers != null
                                ) {
                                    mySubscriptions.data!!.smsInclusiveOffers =
                                        t.data.mySubscriptionsData.smsInclusiveOffers
                                }

                                if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.tm != null
                                ) {
                                    mySubscriptions.data!!.tm = t.data.mySubscriptionsData.tm
                                }

                                if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.internetInclusiveOffers != null
                                ) {
                                    mySubscriptions.data!!.internetInclusiveOffers =
                                        t.data.mySubscriptionsData.internetInclusiveOffers
                                }

                                UserDataManager.setMySubscriptionResponse(mySubscriptions)
                            }

                            /**notify the data adapter in each fragment of this activity*/
                            if (RootValues.getSupplementaryOffersSubscriptionEvents() != null) {
                                RootValues.getSupplementaryOffersSubscriptionEvents()!!
                                    .onSupplementaryOffersSubscriptionSuccess()
                            } else {
                                logE(
                                    "AdapterXu",
                                    "listener is null",
                                    fromClass,
                                    "initSupplementaryOffersCardAdapterEvents"
                                )
                            }
                        }
                    }
                }
            }

        mViewModel.subscribeToRoamingOfferOfferResponseLiveData.observe(
            this@RoamingCountryActivity,
            subscribeToRoamingOfferOfferResponseLiveData
        )

        val subscribeToRoamingOfferErrorResponseLiveData =
            object : Observer<AzerAPIErrorHelperResponse> {
                override fun onChanged(response: AzerAPIErrorHelperResponse?) {
                    if (!this@RoamingCountryActivity.isFinishing) {
                        //check internet
                        if (!isNetworkAvailable(this@RoamingCountryActivity)) {
                            showMessageDialog(
                                applicationContext, this@RoamingCountryActivity,
                                resources.getString(R.string.popup_note_title),
                                resources.getString(R.string.message_no_internet)
                            )
                        } else {
                            /**check the error code
                             * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                             * else show a general api failure popup*/
                            if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                                showMessageDialog(
                                    applicationContext, this@RoamingCountryActivity,
                                    this@RoamingCountryActivity.resources.getString(R.string.popup_error_title),
                                    response.errorDetail.toString()
                                )
                            } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                                //force logout the user here
                                logOut(
                                    applicationContext,
                                    this@RoamingCountryActivity,
                                    false,
                                    fromClass,
                                    "initSupplementaryOffersCardAdapterEventsa34324"
                                )
                            } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                                //handle the ui in this case
                                //hide or show the no data found layout in this case
                                showMessageDialog(
                                    applicationContext, this@RoamingCountryActivity,
                                    this@RoamingCountryActivity.resources.getString(R.string.popup_error_title),
                                    this@RoamingCountryActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                                showMessageDialog(
                                    applicationContext, this@RoamingCountryActivity,
                                    this@RoamingCountryActivity.resources.getString(R.string.popup_error_title),
                                    this@RoamingCountryActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            } else {
                                showMessageDialog(
                                    applicationContext, this@RoamingCountryActivity,
                                    this@RoamingCountryActivity.resources.getString(R.string.popup_error_title),
                                    this@RoamingCountryActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            }
                        }
                    }// if (activity != null && !this@SupplementaryOffersActivity.isFinishing)  ends
                }
            }

        mViewModel.subscribeToRoamingOfferErrorResponseLiveData.observe(
            this@RoamingCountryActivity,
            subscribeToRoamingOfferErrorResponseLiveData
        )
        val inAppSurveyResponseErrorResponseData =
            Observer<AzerAPIErrorHelperResponse> { response ->
                if (!isNetworkAvailable(this@RoamingCountryActivity)) {
                    showMessageDialog(
                        this@RoamingCountryActivity, this@RoamingCountryActivity,
                        resources.getString(R.string.popup_note_title),
                        resources.getString(R.string.message_no_internet)
                    )
                } else {
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                        showMessageDialog(
                            this@RoamingCountryActivity, this@RoamingCountryActivity,
                            this@RoamingCountryActivity.resources.getString(R.string.popup_error_title),
                            response.errorDetail.toString()
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                        //force logout the user here
                        logOut(
                            this@RoamingCountryActivity,
                            this@RoamingCountryActivity,
                            false,
                            fromClass,
                            "initSupplementaryOffersCardAdapterEventsa34324"
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                        //handle the ui in this case
                        //hide or show the no data found layout in this case
                        showMessageDialog(
                            this@RoamingCountryActivity, this@RoamingCountryActivity,
                            this@RoamingCountryActivity.resources.getString(R.string.popup_error_title),
                            this@RoamingCountryActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                        showMessageDialog(
                            this@RoamingCountryActivity, this@RoamingCountryActivity,
                            this@RoamingCountryActivity.resources.getString(R.string.popup_error_title),
                            this@RoamingCountryActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    } else {
                        showMessageDialog(
                            this@RoamingCountryActivity, this@RoamingCountryActivity,
                            this@RoamingCountryActivity.resources.getString(R.string.popup_error_title),
                            this@RoamingCountryActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    }
                }
            }
        mViewModel.inAppSurveyResponseErrorResponseData.observe(
            this@RoamingCountryActivity,
            inAppSurveyResponseErrorResponseData
        )
    }//subscribe ends
    private fun inAppFeedback(context: Context, message: String) {

        var inAppFeedbackDialog: InAppFeedbackDialog? = null
        var currentVisit: Int =
            LocalSharedPrefStorage(this@RoamingCountryActivity).getInt(LocalSharedPrefStorage.PREF_BUNDLE_PAGE)
        currentVisit += 1
        val inAppSurvey: InAppSurvey? = UserDataManager.getInAppSurvey()
        if ((inAppSurvey?.data) != null) {
            val surveys: Surveys? =
                inAppSurvey.data.findSurvey(ConstantsUtility.InAppSurveyConstants.BUNDLE_PAGE)
            if (surveys != null) {
                inAppFeedbackDialog = InAppFeedbackDialog(context, object : OnClickSurveySubmit {
                    override fun onClickSubmit(
                        uploadSurvey: UploadSurvey,
                        onTransactionComplete: String,
                        updateListOnSuccessfullySurvey: UpdateListOnSuccessfullySurvey?
                    ) {
                        mViewModel.uploadInAppSurvey(
                            this@RoamingCountryActivity,
                            uploadSurvey,
                            onTransactionComplete,
                            updateListOnSuccessfullySurvey
                        )
                        if (inAppFeedbackDialog?.isShowing == true) {
                            inAppFeedbackDialog?.dismiss()
                        }
                    }

                })
                if (surveys.questions != null) {
                    currentVisit = 0
                    inAppFeedbackDialog.showTitleWithMessageDialog(surveys,resources.getString(R.string.popup_note_title),message)
                } else {
                    showMessageDialog(
                        applicationContext, this@RoamingCountryActivity,
                        resources.getString(R.string.popup_note_title),
                        message
                    )
                }
            } else {
                showMessageDialog(
                    applicationContext, this@RoamingCountryActivity,
                    resources.getString(R.string.popup_note_title),
                    message
                )
            }
        } else {
            showMessageDialog(
                applicationContext, this@RoamingCountryActivity,
                resources.getString(R.string.popup_note_title),
                message
            )
        }
        LocalSharedPrefStorage(this@RoamingCountryActivity).addInt(
            LocalSharedPrefStorage.PREF_BUNDLE_PAGE,
            currentVisit
        )
    }

    private fun updateList()
    {
        adapterSupplementaryOffersCard?.notifyDataSetChanged()
    }
    private fun initRoamingCardEvents() {
        val supplementaryOffersRoamingCardAdapterEvents =
            object : SupplementaryOffersRoamingCardAdapterEvents {
                override fun onSupplementaryOfferSubscribeButtonClick(
                    titleMessage: String,
                    offeringId: String,
                    offeringName: String,
                    actionType: String
                ) {
                    if (!this@RoamingCountryActivity.isFinishing) {
                        logE(
                            "offerSubscribe",
                            "offername:::".plus(offeringName).plus(" actionType:::$actionType")
                                .plus(" offeringId:::").plus(offeringId),
                            fromClass,
                            "initRoamingCardEvents"
                        )
                        /**call the api to subscribe the offer*/
                        showRoamingOfferSubscribePopup(
                            titleMessage,
                            offeringName,
                            offeringId,
                            actionType
                        )
                    }
                }
            }

        RootValues.setSupplementaryOffersRoamingCardAdapterEvents(
            supplementaryOffersRoamingCardAdapterEvents
        )
    }//initRoamingCardEvents ends

    private fun showRoamingOfferSubscribePopup(
        title: String,
        offeringName: String,
        offeringId: String,
        actionType: String
    ) {
        if (this@RoamingCountryActivity.isFinishing) return

        val dialogBuilder = AlertDialog.Builder(this@RoamingCountryActivity)
        val inflater =
            this@RoamingCountryActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val dialogView = inflater.inflate(R.layout.azer_vas_services_confirmation_popup, null)
        dialogBuilder.setView(dialogView)

        val alertDialog = dialogBuilder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.show()

        val popupTitle = dialogView.findViewById(R.id.popupTitle) as TextView
        popupTitle.typeface = getALSNormalFont()
        popupTitle.text = title
        val popupOkayButton = dialogView.findViewById(R.id.yesButton) as Button
        popupOkayButton.typeface = getALSBoldFont()
        val popupCancelButton = dialogView.findViewById(R.id.noButton) as Button
        popupCancelButton.typeface = getALSBoldFont()

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN) {
            //english
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "No"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ) {
            //azri
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "Xeyr"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.RU) {
            //russian
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "Нет"
        }

        popupOkayButton.setOnClickListener {
            alertDialog.dismiss()
            mViewModel.subscribeToSupplementaryOffer(offeringName, offeringId, actionType)
        }

        popupCancelButton.setOnClickListener {
            alertDialog.dismiss()
        }
    }//showSupplementaryOfferSubscribePopup ends

    override fun onSearchResult(isResultFound: Boolean) {
        runOnUiThread { foundSearchResult(isResultFound) }
    }//onSearchResult ends

    private fun foundSearchResult(isResultFound: Boolean) {
        if (isResultFound) {
            logE("foundResults", "found:::", fromClass, "initOnSearchResults")
            mainView.visibility = View.VISIBLE
            noDataFoundLayout.visibility = View.GONE
        } else {
            logE("foundResults", "not found:::", fromClass, "initOnSearchResults")
            mainView.visibility = View.GONE
            noDataFoundLayout.visibility = View.VISIBLE
            noDataFoundTextLabel.text = getNoDataFoundOnSearchMessage()
            noDataFoundTextLabel.isSelected = true
        }
    }//foundSearchResult ends

    private fun initUI() {
        logE("countrySent", "country:::".plus(selectedCountryName), fromClass, "initUI")
        roaming = UserDataManager.getCountryRoamingDataList()
        if (roaming != null) {
            if (roaming != null && !roaming.isNullOrEmpty()) {
                if (RootValues.getOffersListOrientation() == androidx.recyclerview.widget.LinearLayoutManager.VERTICAL) {
                    img_arrowleft.visibility = View.GONE
                    img_arrowright.visibility = View.GONE

                    img_arrowup.visibility = View.VISIBLE
                    img_arrowdown.visibility = View.VISIBLE

                    mRecylerview.visibility = View.VISIBLE
                    mDiscreteView.visibility = View.GONE
                    mRecylerview.recycledViewPool.setMaxRecycledViews(0, 0)
                    mRecylerview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
                        applicationContext,
                        androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                        false
                    )
                    adapterSupplementaryOffersCard = SupplementaryOffersCardAdapter(
                        this@RoamingCountryActivity,
                        roaming!!,
                        true,
                        selectedCountryName,
                        selectedCountryFlag,
                        false,
                        this,
                        resources.getString(R.string.supplementary_offers_roaming_tab_title),
                        mViewModel
                    )
                    mRecylerview.adapter = adapterSupplementaryOffersCard
                    setRecycleViewArrowListener(mRecylerview.layoutManager as LinearLayoutManager)

                } else {
                    img_arrowleft.visibility = View.VISIBLE
                    img_arrowright.visibility = View.VISIBLE

                    img_arrowup.visibility = View.GONE
                    img_arrowdown.visibility = View.GONE

                    mRecylerview.visibility = View.GONE
                    mDiscreteView.visibility = View.VISIBLE
                    mDiscreteView.recycledViewPool.setMaxRecycledViews(0, 0)
                    mDiscreteView.setOrientation(Orientation.HORIZONTAL)
                    mDiscreteView.setItemTransformer(ScaleTransformer.Builder().build())
                    adapterSupplementaryOffersCard = SupplementaryOffersCardAdapter(
                        this@RoamingCountryActivity,
                        roaming!!,
                        true,
                        selectedCountryName,
                        selectedCountryFlag,
                        false,
                        this,
                        resources.getString(R.string.supplementary_offers_roaming_tab_title),mViewModel
                    )
                    mDiscreteView.adapter = adapterSupplementaryOffersCard
                    mDiscreteView.addOnItemChangedListener { viewHolder, i ->
                        updateArrowDescreteView(
                            i
                        )
                    }
                }
            } else {
                mainView.visibility = View.GONE
                noDataFoundLayout.visibility = View.VISIBLE
                noDataFoundTextLabel.text = getNoDataFoundSimpleMessage()
                noDataFoundTextLabel.isSelected = true
            }
        } else {
            mainView.visibility = View.GONE
            noDataFoundLayout.visibility = View.VISIBLE
            noDataFoundTextLabel.text = getNoDataFoundSimpleMessage()
            noDataFoundTextLabel.isSelected = true
        }

        disposeIntentData()
    }//initUI ends

    private fun initUiEvents() {
        searchCrossIcon.setOnClickListener {
            azerActionBarTitle.visibility = View.VISIBLE
            searchCrossIcon.visibility = View.GONE
            searchInputField.visibility = View.GONE
            searchInputField.setText("")
            searchInputField.hint = resources.getString(R.string.supplementary_offers_search_hint)

            packagesSearchIcon.visibility = View.VISIBLE
            packagesLayoutChangerIcon.visibility = View.VISIBLE
            packagesFilterIcon.visibility = View.VISIBLE
        }//searchCrossIcon.setOnClickListener ends

        packagesSearchIcon.setOnClickListener {
            azerActionBarTitle.visibility = View.GONE
            searchCrossIcon.visibility = View.VISIBLE
            searchInputField.visibility = View.VISIBLE

            packagesSearchIcon.visibility = View.GONE
            packagesLayoutChangerIcon.visibility = View.GONE
            packagesFilterIcon.visibility = View.GONE
        }//packagesSearchIcon.setOnClickListener ends

        packagesLayoutChangerIcon.setOnClickListener {
            if (RootValues.getOffersListOrientation() == LinearLayout.VERTICAL) {
                RootValues.setOffersListOrientation(LinearLayout.HORIZONTAL)
                packagesLayoutChangerIcon.setImageResource(R.drawable.ic_window)
            } else {
                RootValues.setOffersListOrientation(LinearLayout.VERTICAL)
                packagesLayoutChangerIcon.setImageResource(R.drawable.ic_packages_vertical)
            }

            if (roaming != null) {
                initUI()
            }
        }//packagesLayoutChangerIcon.setOnClickListener ends

        azerActionBarBackIcon.setOnClickListener {
            onBackPressed()
        }//azerActionBarBackIcon.setOnClickListener  ends

        packagesFilterIcon.setOnClickListener {
            showFilterAlert()
        }//packagesFilterIcon.setOnClickListener ends

        searchInputField.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }//afterTextChanged ends

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }//beforeTextChanged ends

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (adapterSupplementaryOffersCard == null) {
                    return
                } else {
                    adapterSupplementaryOffersCard!!.filter.filter(s.toString())
                }
            }//onTextChanged ends
        })//searchInputField.addTextChangedListener ends


        img_arrowleft?.setOnClickListener { leftArrowClickListener() }
        img_arrowright?.setOnClickListener { rightArrowClickListener() }

    }//initUiEvents ends


    private fun rightArrowClickListener() {
        var index = mDiscreteView?.currentItem?.plus(1)
        if (index != null) {
            if (index >= 0 && index < roaming?.size!! && index < mDiscreteView?.adapter?.itemCount!!)
                mDiscreteView?.smoothScrollToPosition(index)
        }
    }

    private fun leftArrowClickListener() {
        var index = mDiscreteView?.currentItem?.minus(1)
        if (index != null) {
            if (index >= 0 && index < roaming?.size!! && index < mDiscreteView?.adapter?.itemCount!!)
                mDiscreteView?.smoothScrollToPosition(index)
        }
    }


    private fun showFilterAlert() {
        if (offerFilter != null) {
            logE("offerFilter", "notNull", fromClass, "showFilterAlert")
            SupplementaryOffersFilterPopup
                .newInstance(offerFilter!!, false)
                .show(supportFragmentManager, DIALOG_FILTER)
        } else {
            logE("offerFilter", "null", fromClass, "showFilterAlert")
            showMessageDialog(
                applicationContext, this@RoamingCountryActivity,
                resources.getString(R.string.popup_note_title),
                resources.getString(R.string.lbl_no_filter_msg)
            )
        }
    }//showFilterAlert

    private fun initFilterApplyEvents() {
        val supplementaryOffersFilterEvents = object : SupplementaryOffersFilterEvents {
            override fun onFilterApply() {
                if (UserDataManager.getSupplementaryFilterKeysList().isNotEmpty()) {
                    val sb = StringBuilder()
                    val filterToApplyKey = "fromPopup"
                    for (its in UserDataManager.getSupplementaryFilterKeysList().indices) {
                        sb.append(UserDataManager.getSupplementaryFilterKeysList()[its])
                            .append("juni1289")
                    }

                    val searchFilter = filterToApplyKey.plus(sb)
                    if (adapterSupplementaryOffersCard == null) {
                        return
                    } else {
                        adapterSupplementaryOffersCard!!.filter.filter(searchFilter)
                    }
                } else {
                    if (adapterSupplementaryOffersCard == null) {
                        return
                    } else {
                        adapterSupplementaryOffersCard!!.filter.filter("")
                    }
                }

                //dismiss the filter popup
                val prev = supportFragmentManager.findFragmentByTag(DIALOG_FILTER)
                if (prev != null) {
                    val df = prev as SupplementaryOffersFilterPopup
                    df.dismiss()
                }
            }
        }

        RootValues.setSupplementaryOffersFilterEvents(supplementaryOffersFilterEvents)
    }//initFilterApplyEvents ends

    private fun getIntentExtrasData() {
        if (intent.extras != null) {
            val bundle = intent.extras ?: return
            /*if (bundle.containsKey(ConstantsUtility.PackagesConstants.KEYWORD_ROAMING_OFFERS_DATA)) {
                roaming = bundle.getParcelableArrayList(ConstantsUtility.PackagesConstants.KEYWORD_ROAMING_OFFERS_DATA)
            }*/
            if (bundle.containsKey(ConstantsUtility.PackagesConstants.SELECTED_ROAMING_COUNTRY_NAME)) {
                selectedCountryName =
                    bundle.getString(ConstantsUtility.PackagesConstants.SELECTED_ROAMING_COUNTRY_NAME)!!
                logE(
                    "countryX1Sent",
                    "country111:::".plus(selectedCountryName),
                    fromClass,
                    "initUI"
                )
            }
            if (bundle.containsKey(ConstantsUtility.PackagesConstants.SELECTED_ROAMING_COUNTRY_FLAG)) {
                selectedCountryFlag =
                    bundle.getString(ConstantsUtility.PackagesConstants.SELECTED_ROAMING_COUNTRY_FLAG)!!
                logE("countryX1Sent", "flag111:::".plus(selectedCountryFlag), fromClass, "initUI")
            }
            if (bundle.containsKey(ConstantsUtility.PackagesConstants.SELECTED_ROAMING_FILTERS)) {
                offerFilter =
                    bundle.getParcelable(ConstantsUtility.PackagesConstants.SELECTED_ROAMING_FILTERS)
            }
        }
    }//getIntentExtrasData ends

    private fun disposeIntentData() {
        val bundle = intent.extras

        if (bundle != null) {
            if (bundle.containsKey(ConstantsUtility.PackagesConstants.KEYWORD_ROAMING_OFFERS_DATA)) {
                bundle.clear()
            }
            if (bundle.containsKey(ConstantsUtility.PackagesConstants.SELECTED_ROAMING_COUNTRY_NAME)) {
                bundle.clear()
            }
            if (bundle.containsKey(ConstantsUtility.PackagesConstants.SELECTED_ROAMING_COUNTRY_FLAG)) {
                bundle.clear()
            }
        }
    }//disposeIntentData ends

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text =
            resources.getString(R.string.supplementary_offers_roaming_activity_title)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
        packagesFilterIcon.visibility = View.VISIBLE
        packagesLayoutChangerIcon.visibility = View.VISIBLE
        packagesSearchIcon.visibility = View.VISIBLE

        if (RootValues.getOffersListOrientation() == LinearLayout.VERTICAL) {
            packagesLayoutChangerIcon.setImageResource(R.drawable.ic_packages_vertical)
        } else {
            packagesLayoutChangerIcon.setImageResource(R.drawable.ic_window)
        }
    }//setupHeaderActionBar ends

    companion object {
        fun start(context: Context, bundle: Bundle) {
            val intent = Intent(context, RoamingCountryActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtras(bundle)
            context.startActivity(intent)
        }//start ends
    }//object ends

    private fun updateArrowDescreteView(position: Int) {
        if ((roaming?.size!!) <= 1 || (mDiscreteView?.adapter?.itemCount!!) <= 1) {
            img_arrowleft?.visibility = View.GONE
            img_arrowright?.visibility = View.GONE
        } else
            if (position == 0) {
                img_arrowleft?.visibility = View.GONE
                img_arrowright?.visibility = View.VISIBLE
            } else if (position == (roaming?.size?.minus(1)) || position == (mDiscreteView?.adapter?.itemCount?.minus(
                    1
                ))
            ) {
                img_arrowleft?.visibility = View.VISIBLE
                img_arrowright?.visibility = View.GONE
            } else {
                img_arrowleft?.visibility = View.VISIBLE
                img_arrowright?.visibility = View.VISIBLE
            }
    }  //updateArrowDescreteView

    private fun setRecycleViewArrowListener(linearLayoutManager: LinearLayoutManager) {

        val recyclerViewOnScrollListener: RecyclerView.OnScrollListener =
            object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                }

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    val visibleItemCount: Int = linearLayoutManager.childCount
                    val totalItemCount: Int = linearLayoutManager.itemCount
                    val firstVisibleItemPosition: Int =
                        linearLayoutManager.findFirstVisibleItemPosition()
                    val lastItem = firstVisibleItemPosition + visibleItemCount
                    var position = 0;
                    if (!recyclerView.canScrollVertically(1)) {
                        position = 2
                    }
                    if (!recyclerView.canScrollVertically(-1)) {
                        position = 1
                    }
                    updateArrowRecyclerView(
                        firstVisibleItemPosition,
                        lastItem,
                        visibleItemCount,
                        position
                    )

                }
            }
        mRecylerview.addOnScrollListener(recyclerViewOnScrollListener)
    }

    private fun updateArrowRecyclerView(
        firstItemShowing: Int,
        lastItemShowing: Int,
        visibleItemCount: Int,
        position: Int
    ) {
        if (visibleItemCount < 2) {
            if (img_arrowdown?.visibility == View.VISIBLE)
                img_arrowdown?.visibility = View.GONE
            if (img_arrowup?.visibility == View.VISIBLE)
                img_arrowup?.visibility = View.GONE
        } else {
            if (firstItemShowing.equals(0)) {
                if (img_arrowup?.visibility == View.VISIBLE)
                    img_arrowup?.visibility = View.GONE
            } else {
                if (img_arrowup?.visibility == View.GONE)
                    img_arrowup?.visibility = View.VISIBLE

            }
            if (roaming?.size?.equals(lastItemShowing)!! || mRecylerview?.adapter?.itemCount!!.equals(
                    lastItemShowing
                )
            ) {
                if (img_arrowdown?.visibility == View.VISIBLE)
                    img_arrowdown?.visibility = View.GONE
            } else {
                if (img_arrowdown?.visibility == View.GONE)
                    img_arrowdown?.visibility = View.VISIBLE

            }
        }

        if (position == 1 && mRecylerview?.adapter?.itemCount!! > 1) {
            if (img_arrowdown?.visibility == View.GONE)
                img_arrowdown?.visibility = View.VISIBLE
            if (img_arrowup?.visibility == View.VISIBLE)
                img_arrowup?.visibility = View.GONE
        } else
            if (position == 2 && mRecylerview?.adapter?.itemCount!! > 1) {
                if (img_arrowdown?.visibility == View.VISIBLE)
                    img_arrowdown?.visibility = View.GONE
                if (img_arrowup?.visibility == View.GONE)
                    img_arrowup?.visibility = View.VISIBLE

            }

    } //updateArrowRecyclerView ends


}//class ends