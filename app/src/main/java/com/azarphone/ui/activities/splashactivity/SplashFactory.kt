package com.azarphone.ui.activities.splashactivity

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

/**
 * Created by Junaid Hassan on 19, February, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class SplashFactory(val mSplashRepository: SplashRepository) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SplashViewModel::class.java)) {
            return SplashViewModel(mSplashRepository) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}