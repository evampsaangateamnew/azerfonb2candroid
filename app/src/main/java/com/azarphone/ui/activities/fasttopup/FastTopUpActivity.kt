package com.azarphone.ui.activities.fasttopup

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.request.DeleteFastPaymentRequest
import com.azarphone.api.pojo.request.MakePaymentRequest
import com.azarphone.api.pojo.response.fasttopupresponse.FastTopUpDetail
import com.azarphone.api.pojo.response.fasttopupresponse.FastTopUpResponse
import com.azarphone.api.pojo.response.fasttopupresponse.deletefasttopup.DeleteItemResponse
import com.azarphone.api.pojo.response.makepayment.MakePaymentTopUpResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.ActivityFastTopupBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.ui.adapters.recyclerviews.FastTopUpAdapter
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.widgets.popups.showConfirmationDialog
import com.azarphone.widgets.popups.showMessageDialog
import kotlinx.android.synthetic.main.activity_fast_topup.*
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import java.util.*

class FastTopUpActivity :
    BaseActivity<ActivityFastTopupBinding, FastTopUpFactory, FastTopUpViewModel>() {

    var deletedPosition: Int = -1

    /**
     * Returns layout to assign activity
     */
    override fun getLayoutId(): Int {
        return R.layout.activity_fast_topup
    }//getLayoutId ends

    /**
     * Returns ViewModel to assign activity
     */
    override fun getViewModelClass(): Class<FastTopUpViewModel> {
        return FastTopUpViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): FastTopUpFactory {
        return FastTopUpInjectionUtils.provideFastTopUpFactory()
    }

    override fun init() {
        setupHeaderActionBar()
        initUI()
        subscribe()
        initUiEvents()
        if (isNetworkAvailable(applicationContext)) {
            mViewModel.requestGetFastPayments()
        } else {
            showMessageDialog(
                applicationContext, this@FastTopUpActivity,
                "", resources.getString(R.string.message_no_internet)
            )
        }

    }

    private fun initUI() {}

    private fun initUiEvents() {
        azerActionBarBackIcon.setOnClickListener { onBackPressed() }

    }

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.lbl_fast_topup)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

    private fun initializeRecyclerView(fastPaymentDetails: ArrayList<FastTopUpDetail>) {
        //we add empty element for our last item i.e Add more payments
        fastPaymentDetails.add(FastTopUpDetail("", "", "", "", ""))

        //Adapter creation
        val fastTopUpAdapter = FastTopUpAdapter(
            fastPaymentDetails,
            this@FastTopUpActivity,
            object : FastTopUpAdapter.Companion.SetOnItemSelected {
                override fun onSelected(fastTopUpDetail: FastTopUpDetail?) {
                    if (fastTopUpDetail != null && hasValue(fastTopUpDetail.topupNumber) && hasValue(
                            fastTopUpDetail.paymentKey
                        ) && hasValue(fastTopUpDetail.amount)
                    ) {
                        var dialogMessage = ""

                        dialogMessage = if (ProjectApplication.mLocaleManager.getLanguage()
                                .equals(ConstantsUtility.LanguageKeywords.AZ, ignoreCase = true)
                        ) {
                            getString(R.string.payment_loading) + fastTopUpDetail.topupNumber + " nömrəsinə " + fastTopUpDetail.amount + getString(
                                R.string.azn_to
                            )
                        } else {
                            getString(R.string.payment_loading) + fastTopUpDetail.amount + getString(
                                R.string.azn_to
                            ) + fastTopUpDetail.topupNumber + "."
                        }

                        showConfirmationDialog(applicationContext, this@FastTopUpActivity,
                            "", dialogMessage, object : OnClickListener {
                                override fun onClick() {
                                    val makePaymentRequest = MakePaymentRequest(
                                        fastTopUpDetail!!.paymentKey,
                                        fastTopUpDetail.amount,
                                        fastTopUpDetail.topupNumber
                                    )
                                    mViewModel.requestMakePayment(makePaymentRequest)
                                }
                            })

                    }
                }

                override fun onDelete(fastTopUpDetail: FastTopUpDetail?, position: Int) {
                    deletedPosition = position
                    val deleteFastPayment = DeleteFastPaymentRequest(fastTopUpDetail?.id)
                    mViewModel.requestDeleteFastPayment(deleteFastPayment)
                }

            })
        fast_topup_recycleview.layoutManager = LinearLayoutManager(this)
//        if (fastPaymentDetails != null && fastPaymentDetails.size > 1) {
//            val itemTouchHelper = ItemTouchHelper(enableSwipeToDeleteAndUndo(fastTopUpAdapter, fastPaymentDetails)!!)
//            itemTouchHelper.attachToRecyclerView(fast_topup_recycleview)
//        }
        fast_topup_recycleview.adapter = fastTopUpAdapter

    }

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, FastTopUpActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends
    }//object

    private fun enableSwipeToDeleteAndUndo(
        fastTopUpAdapter: FastTopUpAdapter,
        fastTopUpDetails: List<FastTopUpDetail>
    ): ItemTouchHelper.SimpleCallback? {
        return object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
                //Remove swiped item from list and notify the RecyclerView
                val position = viewHolder.adapterPosition
                if (fastTopUpDetails != null && position != fastTopUpDetails.size - 1 && fastTopUpDetails[position] != null && hasValue(
                        fastTopUpDetails[position].id
                    )
                ) {
                    deletedPosition = position
                    val deleteFastPayment = DeleteFastPaymentRequest(fastTopUpDetails[position].id)
                    mViewModel.requestDeleteFastPayment(deleteFastPayment)

                } else {
                    fastTopUpAdapter.notifyItemChanged(position)
                }
            }
        }
    }

    fun subscribe() {

        val getFastTopUpPaymentsResponseLiveData =
            Observer<FastTopUpResponse> { fastTopUpResponse ->
                processFastTopUpResponse(fastTopUpResponse)
            }
        mViewModel.getFastPaymentResponseLiveData.observe(
            this@FastTopUpActivity,
            getFastTopUpPaymentsResponseLiveData
        )

        val deleteFastTopUpPaymentsResponseLiveData = object : Observer<DeleteItemResponse> {
            override fun onChanged(deleteFastTopUpResponse: DeleteItemResponse?) {
                if (!this@FastTopUpActivity.isFinishing && deletedPosition != -1 && fast_topup_recycleview?.adapter != null && deleteFastTopUpResponse != null) {
                    (fast_topup_recycleview.adapter as FastTopUpAdapter).deleteItem(deletedPosition)
                    (fast_topup_recycleview.adapter as FastTopUpAdapter).notifyDataSetChanged()
                    deletedPosition = -1
                } else {
                    (fast_topup_recycleview.adapter as FastTopUpAdapter).notifyDataSetChanged()
                    deletedPosition = -1
                }
            }
        }
        mViewModel.deleteFastPaymentResponseLiveData.observe(
            this@FastTopUpActivity,
            deleteFastTopUpPaymentsResponseLiveData
        )

        val makePaymentsResponseLiveData = object : Observer<MakePaymentTopUpResponse> {
            override fun onChanged(t: MakePaymentTopUpResponse?) {
                UserDataManager.setIsGetHomePageCalledWithSuccess(true)
                removeHomePageResponseFromLocalPreferences()
            }
        }
        mViewModel.makePaymentResponseLiveData.observe(
            this@FastTopUpActivity,
            makePaymentsResponseLiveData
        )

        val deleteItemErrorLiveData = object : Observer<String> {
            override fun onChanged(t: String?) {
                logE("FastTopUpActivity", "error:::", "FastTopUpActivity", "error on delete item ")

                processDeleteItemResponse(t)
            }
        }
        mViewModel.deleteFastPaymentErrorLiveData.observe(
            this@FastTopUpActivity,
            deleteItemErrorLiveData
        )
    }

    private fun processFastTopUpResponse(fastTopUpResponse: FastTopUpResponse) {
        initializeRecyclerView(fastTopUpResponse.data.fastPaymentDetails as ArrayList<FastTopUpDetail>)
    }

    private fun processDeleteItemResponse(error: String?) {
        fast_topup_recycleview.adapter?.notifyDataSetChanged()
    }


}