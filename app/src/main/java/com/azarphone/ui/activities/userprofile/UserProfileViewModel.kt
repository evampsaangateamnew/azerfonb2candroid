package com.azarphone.ui.activities.userprofile

import androidx.lifecycle.MutableLiveData
import com.azarphone.api.pojo.request.ChangeBillingRequest
import com.azarphone.api.pojo.request.ProfilePictureUploadRequest
import com.azarphone.api.pojo.request.SuspendNumberRequest
import com.azarphone.api.pojo.response.changebillinglanguagereponse.ChangeBillingResponse
import com.azarphone.api.pojo.response.profilepictureupload.ProfilePictureUpdateResponse
import com.azarphone.api.pojo.response.suspendnumber.SuspendNumberResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.validators.hasValue
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

/**
 * @author Junaid Hassan on 19, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class UserProfileViewModel(val mUserProfileRepositry: UserProfileRepositry) : BaseViewModel() {

    private val fromClass = "UserProfileViewModel"
    private lateinit var disposable: Disposable
    val suspendNumberResponseLiveData = MutableLiveData<SuspendNumberResponse>()
    val changeBillingResponse = MutableLiveData<ChangeBillingResponse>()
    val removeProfilePictureResponseLiveData = MutableLiveData<String>()
    val profilePictureUploadResponseLiveData=MutableLiveData<ProfilePictureUpdateResponse>()

    init {

    }

    fun requestRemoveProfileImageFromServer(imageBase64: String, imgExt: String, actionType: String) {
        view()?.showLoading()
        val profilePictureUploadRequest = ProfilePictureUploadRequest(imageBase64, imgExt, actionType)

        var mObserver = mUserProfileRepositry.requestRemoveProfileImageFromServer(profilePictureUploadRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            /**only use the result description here*/
                            if (result.resultDesc != null && hasValue(result.resultDesc)) {
                                removeProfilePictureResponseLiveData.postValue(result.resultDesc)
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestUploadImageToServer ends

    fun requestUploadImageToServer(imageBase64: String, imgExt: String, actionType: String) {
        view()?.showLoading()
        val profilePictureUploadRequest = ProfilePictureUploadRequest(imageBase64, imgExt, actionType)

        var mObserver = mUserProfileRepositry.requestUploadImageToServer(profilePictureUploadRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                           profilePictureUploadResponseLiveData.postValue(result)
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestUploadImageToServer ends

    fun requestSuspendNumber(reasonCode: String) {
        view()?.showLoading()
        val suspendNumberRequest = SuspendNumberRequest(reasonCode)

        var mObserver = mUserProfileRepositry.requestSuspendNumber(suspendNumberRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    suspendNumberResponseLiveData.postValue(result)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestSuspendNumber ends

    fun requestChangeBilling(language: String) {
        view()?.showLoading()
        val changeBillingRequest = ChangeBillingRequest(language)

        var mObserver = mUserProfileRepositry.requestBillingLanguageApi(changeBillingRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    changeBillingResponse.postValue(result)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }
}//class ends