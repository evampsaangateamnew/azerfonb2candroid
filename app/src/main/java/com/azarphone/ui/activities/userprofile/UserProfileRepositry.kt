package com.azarphone.ui.activities.userprofile

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.ChangeBillingRequest
import com.azarphone.api.pojo.request.ProfilePictureUploadRequest
import com.azarphone.api.pojo.request.SuspendNumberRequest
import com.azarphone.api.pojo.response.changebillinglanguagereponse.ChangeBillingResponse
import com.azarphone.api.pojo.response.profilepictureupload.ProfilePictureUpdateResponse
import com.azarphone.api.pojo.response.suspendnumber.SuspendNumberResponse
import io.reactivex.Observable

/**
 * @author Junaid Hassan on 19, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class UserProfileRepositry{
    init {

    }

    fun requestSuspendNumber(suspendNumberRequest : SuspendNumberRequest): Observable<SuspendNumberResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestSuspendNumber(suspendNumberRequest)
    }

    fun requestBillingLanguageApi(changeBillingRequest : ChangeBillingRequest): Observable<ChangeBillingResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestChangeBillingLanguage(changeBillingRequest)
    }

    fun requestUploadImageToServer(profilePictureUploadRequest : ProfilePictureUploadRequest): Observable<ProfilePictureUpdateResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestUploadImageToServer(profilePictureUploadRequest)
    }

    fun requestRemoveProfileImageFromServer(profilePictureUploadRequest : ProfilePictureUploadRequest): Observable<ProfilePictureUpdateResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestRemoveProfileImageFromServer(profilePictureUploadRequest)
    }
}