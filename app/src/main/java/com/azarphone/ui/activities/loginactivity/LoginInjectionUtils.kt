package com.azarphone.ui.activities.loginactivity

object LoginInjectionUtils{

    fun provideLoginRepository():LoginRepository{
        return LoginRepository()
    }

    fun provideLoginFactory():LoginFactory{
        return LoginFactory(provideLoginRepository())
    }


}