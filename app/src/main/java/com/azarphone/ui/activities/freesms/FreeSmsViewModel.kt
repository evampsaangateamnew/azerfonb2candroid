package com.azarphone.ui.activities.freesms

import androidx.lifecycle.MutableLiveData
import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.SendSMSRequest
import com.azarphone.api.pojo.response.freesmsstatusresponse.FreeSmsStatusResponse
import com.azarphone.api.pojo.response.sendsmsresponse.SendSMSResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.util.logE
import com.es.tec.applyIOSchedulers
import io.reactivex.Observable
import io.reactivex.disposables.Disposable

/**
 * @author Junaid Hassan on 08, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class FreeSmsViewModel(val mFreeSmsRepositry: FreeSmsRepositry) : BaseViewModel() {

    private val fromClass = "FreeSMSViewModel"
    private val logKey = "FreeSms"
    private lateinit var disposable: Disposable
    val freeSmsStatusResponseLiveData = MutableLiveData<FreeSmsStatusResponse>()
    val sendFreeSmsResponseLiveData = MutableLiveData<SendSMSResponse>()

    init {
        requestGetFreeSmsStatus()
    }//init


    fun requestGetFreeSmsStatus() {
        logE(logKey, "requestGetFreeSmsStatus Called", fromClass, "requestGetFreeSmsStatus")
        view()?.showLoading()


        var mObserver = mFreeSmsRepositry.getFreeSmsStatus()
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            logE(logKey, "response:::".plus(result.toString()), fromClass, "freeSmsStatusRequest")
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    freeSmsStatusResponseLiveData.postValue(result)
                                }
                            }
                        },
                        { error ->
                            logE(logKey, "error:::".plus(error.localizedMessage), fromClass, "freeSmsStatusRequest")
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }

    fun requestSendFreeSms(recieverMsisdn: String, textmsg: String, msgLang: String) {
        logE(logKey, "requestSendFreeSMS Called ", fromClass, "requestSendFreeSms")
        view()?.showLoading()
        val sendFreeSmsRequest = SendSMSRequest(recieverMsisdn, textmsg, msgLang)

        var mObserver = mFreeSmsRepositry.sendSMS(sendFreeSmsRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            logE(logKey, "response:::".plus(result.toString()), fromClass, "requestSendFreeSms")
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    sendFreeSmsResponseLiveData.postValue(result)
                                }
                            }
                        },
                        { error ->
                            logE(logKey, "error:::".plus(error.localizedMessage), fromClass, "requestSendFreeSms")
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }


}