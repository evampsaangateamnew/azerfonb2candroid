package com.azarphone.ui.activities.exchangeservice

import androidx.lifecycle.MutableLiveData
import com.azarphone.analytics.Analytics
import com.azarphone.analytics.EventValues
import com.azarphone.api.pojo.request.ExchangeServiceRequest
import com.azarphone.api.pojo.request.ExchangeServiceValuesRequest
import com.azarphone.api.pojo.response.exchangeserviceresponse.ExchangeServiceResponse
import com.azarphone.api.pojo.response.exchangeservicesvaluesresponse.ExchangeServicesValuesResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.datamanagers.UserDataManager
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

class ExchangeServiceViewModel(val mServiceRepository: ExchangeServiceRepositry) : BaseViewModel() {

    private lateinit var disposable: Disposable
    val exchangeServiceValuesResponse = MutableLiveData<ExchangeServicesValuesResponse>()
    val exchangeServiceResponse = MutableLiveData<ExchangeServiceResponse>()

    init {

}
    fun requestExchangeServiceValues() {
        view()?.showLoading()
        val offeringID= UserDataManager.getCustomerData()?.offeringId
        val offeringName= UserDataManager.getCustomerData()?.offeringName
        val exchangeServiceValueRequest = ExchangeServiceValuesRequest(offeringID,offeringName)
        disposable = mServiceRepository.getExchangeServiceValues(exchangeServiceValueRequest)
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    exchangeServiceValuesResponse.postValue(result)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }

    fun requestExchangeServiceValuesInBackground() {

        val offeringID= UserDataManager.getCustomerData()?.offeringId
        val offeringName= UserDataManager.getCustomerData()?.offeringName
        val exchangeServiceValueRequest = ExchangeServiceValuesRequest(offeringID,offeringName)
        disposable = mServiceRepository.getExchangeServiceValues(exchangeServiceValueRequest)
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            exchangeServiceValuesResponse.postValue(result)
                        },
                        { error ->
                            view()?.hideLoading()
                        }
                )

        compositeDisposables?.add(disposable)
    }
    fun requestExchangeService(isDataToVoice: Boolean,supplementaryOfferingIds:String, voiceValue: String?, dataValue: String?) {
        view()?.showLoading()

        val offeringID= UserDataManager.getCustomerData()?.offeringId
        val exchangeServiceRequest = ExchangeServiceRequest(isDataToVoice,supplementaryOfferingIds,voiceValue,dataValue,offeringID)
        disposable = mServiceRepository.requestExchangeService(exchangeServiceRequest)
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    exchangeServiceResponse.postValue(result)
                                //    Analytics.logExchangeServiceEvent(EventValues.GenericValues.Success,isDataToVoice)

                                    if(isDataToVoice) {
                                        Analytics.logAppEvent(EventValues.ExchangeServiceEvents.exchange_offer, EventValues.ExchangeServiceEvents.data_to_voice, EventValues.GenericValues.Success)
                                    }
                                    else{
                                        Analytics.logAppEvent(EventValues.ExchangeServiceEvents.exchange_offer, EventValues.ExchangeServiceEvents.voice_to_data, EventValues.GenericValues.Success)
                                    }
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)

                            if(isDataToVoice) {
                                Analytics.logAppEvent(EventValues.ExchangeServiceEvents.exchange_offer, EventValues.ExchangeServiceEvents.data_to_voice, EventValues.GenericValues.Failure)
                            }
                            else{
                                Analytics.logAppEvent(EventValues.ExchangeServiceEvents.exchange_offer, EventValues.ExchangeServiceEvents.voice_to_data, EventValues.GenericValues.Failure)
                            }
                        }
                )

        compositeDisposables?.add(disposable)
    }
}