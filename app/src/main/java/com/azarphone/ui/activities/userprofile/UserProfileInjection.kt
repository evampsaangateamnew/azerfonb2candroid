package com.azarphone.ui.activities.userprofile

/**
 * @author Junaid Hassan on 19, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
object UserProfileInjection {

    fun provideUserProfileRepository(): UserProfileRepositry {
        return UserProfileRepositry()
    }

    fun provideUserProfileFactory(): UserProfileFactory {
        return UserProfileFactory(provideUserProfileRepository())
    }

}