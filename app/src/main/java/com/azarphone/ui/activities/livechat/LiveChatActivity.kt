package com.azarphone.ui.activities.livechat

import android.content.Context
import android.content.Intent
import android.net.Uri
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.ActivityLiveChatBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.logE
import com.livechatinc.inappchat.ChatWindowConfiguration
import com.livechatinc.inappchat.ChatWindowErrorType
import com.livechatinc.inappchat.ChatWindowView.ChatWindowEventsListener
import com.livechatinc.inappchat.models.NewMessageModel
import kotlinx.android.synthetic.main.activity_live_chat.*
import java.util.*

class LiveChatActivity : BaseActivity<ActivityLiveChatBinding, LiveChatFactory, LiveChatViewModel>(), ChatWindowEventsListener {
    private lateinit var configuration: ChatWindowConfiguration

    override fun getLayoutId(): Int {
        return R.layout.activity_live_chat
    }

    override fun getViewModelClass(): Class<LiveChatViewModel> {
        return LiveChatViewModel::class.java
    }

    override fun getFactory(): LiveChatFactory {
        return LiveChatFactory(LiveChatRepository())
    }

    override fun init() {
        setUpChatConfiguration()
    }

    private fun setUpChatConfiguration() {
        var VISITOR_NAME = ""
        var VISITOR_EMAIL = ""
        var phoneNumber = ""
        var groupId = ""


        if (UserDataManager.getCustomerData() != null) {
            VISITOR_NAME = UserDataManager.getCustomerData()?.firstName.toString()
            VISITOR_EMAIL = UserDataManager.getCustomerData()?.email.toString()
            phoneNumber = UserDataManager.getCustomerData()?.msisdn.toString()
        }

        //Setting Up Live Chat Language According To App Language
        when {
            ProjectApplication.mLocaleManager.getLanguage()
                .equals(ConstantsUtility.LanguageKeywords.AZ, ignoreCase = true) -> {
                groupId = ConstantsUtility.LanguageKeywordsForUI.AZ
            }
            ProjectApplication.mLocaleManager.getLanguage()
                .equals(ConstantsUtility.LanguageKeywords.RU, ignoreCase = true) -> {
                groupId = ConstantsUtility.LanguageKeywordsForUI.RU
            }
            ProjectApplication.mLocaleManager.getLanguage()
                .equals(ConstantsUtility.LanguageKeywords.EN, ignoreCase = true) -> {
                groupId = ConstantsUtility.LanguageKeywordsForUI.EN
            }
        }

        val PHONENUMBER = HashMap<String, String>()
        PHONENUMBER[ConstantsUtility.LiveChat.PHONE_NUMBER] = phoneNumber
        configuration = ChatWindowConfiguration(
            ConstantsUtility.LiveChat.LICENCE_NUMBER,
            groupId,
            VISITOR_NAME,
            VISITOR_EMAIL,
            PHONENUMBER
        )
        startEmbeddedChat()
    }

    private fun startEmbeddedChat() {
        // Initializing Live Chat View Here
        if (!embedded_chat_window.isInitialized) {
            embedded_chat_window.setUpWindow(configuration)
            embedded_chat_window.setUpListener(this@LiveChatActivity)
            embedded_chat_window.initialize()
        }
        embedded_chat_window.showChatWindow()
    }

    override fun onChatWindowVisibilityChanged(visible: Boolean) {
        if (!visible) {
            onBackPressed()
        }
    }

    override fun onNewMessage(message: NewMessageModel?, windowVisible: Boolean) {
        if (!windowVisible) {
            logE("error", message!!.text, "LiveChatActivity", "onNewMessage")
        }
    }

    override fun onStartFilePickerActivity(intent: Intent?, requestCode: Int) {
        startActivityForResult(intent, requestCode)
    }

    override fun onError(errorType: ChatWindowErrorType?, errorCode: Int, errorDescription: String?): Boolean {
        return !(errorType == ChatWindowErrorType.WebViewClient && errorCode == -2 && embedded_chat_window.isChatLoaded)
    }

    override fun handleUri(uri: Uri?): Boolean {
        return false
    }

    override fun onBackPressed() {
        embedded_chat_window.onBackPressed()
        super.onBackPressed()
    }

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, LiveChatActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends
    }//object ends
}