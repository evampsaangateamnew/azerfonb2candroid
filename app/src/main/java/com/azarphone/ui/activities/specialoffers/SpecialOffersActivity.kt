package com.azarphone.ui.activities.specialoffers

import android.annotation.SuppressLint
import androidx.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AlertDialog
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.response.inappsurvey.InAppSurvey
import com.azarphone.api.pojo.response.inappsurvey.Surveys
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.mysubscriptions.response.MySubscriptionResponse
import com.azarphone.api.pojo.response.mysubscriptions.successresponse.MySubscriptionsSuccessAfterOfferSubscribeResponse
import com.azarphone.api.pojo.response.supplementaryoffers.helper.SupplementaryOffer
import com.azarphone.api.pojo.response.supplementaryoffers.response.SupplementaryResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.LayoutSpecialOffersActivityBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.OffersSearchResultListener
import com.azarphone.eventhandler.OnClickSurveySubmit
import com.azarphone.eventhandler.SupplementarySpecialOffersCardAdapterEvents
import com.azarphone.eventhandler.UpdateListOnSuccessfullySurvey
import com.azarphone.ui.adapters.recyclerviews.SupplementaryOffersCardAdapter
import com.azarphone.ui.dialogs.InAppFeedbackDialog
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.widgets.popups.showMessageDialog
import com.es.tec.LocalSharedPrefStorage
import com.yarolegovich.discretescrollview.Orientation
import com.yarolegovich.discretescrollview.transform.ScaleTransformer
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.layout_special_offers_activity.*
import kotlinx.android.synthetic.main.no_data_found_white_no_bg.*

/**
 * @author Junaid Hassan on 25, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class SpecialOffersActivity : BaseActivity<LayoutSpecialOffersActivityBinding, SpecialOffersFactory, SpecialOffersViewModel>(), OffersSearchResultListener {

    private var actionIdFromServer = ""
    private val fromClass = "SpecialOffersActivity"
    private var supplementaryOffersCardAdapter: SupplementaryOffersCardAdapter? = null

    override fun getLayoutId(): Int {
        return R.layout.layout_special_offers_activity
    }//getLayoutId ends

    override fun getViewModelClass(): Class<SpecialOffersViewModel> {
        return SpecialOffersViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): SpecialOffersFactory {
        return SpecialOffersInjection.provideSpecialOffersFactory()
    }//getFactory ends

    override fun init() {
        setupHeaderActionBar()
        getIntentBundleExtras()
        subscribeToSpecialOffersResponseLiveData()
        subscribe()
        initUIEvents()
        initSpecialOffersCardEvents()
        android.os.Handler().postDelayed({
            requestSpecialOffers()
        }, 50)

    }//init ends

    private fun getIntentBundleExtras() {
        if (intent.extras != null) {
            val bundle = intent.extras
            if (bundle != null && bundle.containsKey(ConstantsUtility.PackagesConstants.NOTIFICATIONS_SPECIAL_REDIRECTION_KEY)) {
                if (bundle.getString(ConstantsUtility.PackagesConstants.NOTIFICATIONS_SPECIAL_REDIRECTION_KEY) != null
                        && hasValue(bundle.getString(ConstantsUtility.PackagesConstants.NOTIFICATIONS_SPECIAL_REDIRECTION_KEY))) {
                    actionIdFromServer = bundle.getString(ConstantsUtility.PackagesConstants.NOTIFICATIONS_SPECIAL_REDIRECTION_KEY)!!
                }
            }
        }//if (intent.extras != null)  ends
    }//getIntentBundleExtras ends
    private fun inAppFeedback(context: Context, message: String) {

        var inAppFeedbackDialog: InAppFeedbackDialog? = null
        var currentVisit: Int =
            LocalSharedPrefStorage(this@SpecialOffersActivity).getInt(LocalSharedPrefStorage.PREF_BUNDLE_PAGE)
        currentVisit += 1
        val inAppSurvey: InAppSurvey? = UserDataManager.getInAppSurvey()
        if ((inAppSurvey?.data) != null) {
            val surveys: Surveys? =
                inAppSurvey.data.findSurvey(ConstantsUtility.InAppSurveyConstants.BUNDLE_PAGE)
            if (surveys != null) {
                inAppFeedbackDialog = InAppFeedbackDialog(context, object : OnClickSurveySubmit {
                    override fun onClickSubmit(
                        uploadSurvey: UploadSurvey,
                        onTransactionComplete: String,
                        updateListOnSuccessfullySurvey: UpdateListOnSuccessfullySurvey?
                    ) {
                        mViewModel.uploadInAppSurvey(
                            this@SpecialOffersActivity,
                            uploadSurvey,
                            onTransactionComplete,
                            updateListOnSuccessfullySurvey
                        )

                        if (inAppFeedbackDialog?.isShowing == true) {
                            inAppFeedbackDialog?.dismiss()
                        }
                    }

                })
                if (surveys.questions != null) {
                    currentVisit = 0
                    inAppFeedbackDialog.showTitleWithMessageDialog(surveys,resources.getString(R.string.popup_note_title),message)
                } else {
                    showMessageDialog(
                        applicationContext, this@SpecialOffersActivity,
                        resources.getString(R.string.popup_note_title),
                        message
                    )
                }
            } else {
                showMessageDialog(
                    applicationContext, this@SpecialOffersActivity,
                    resources.getString(R.string.popup_note_title),
                    message
                )
            }
        } else {
            showMessageDialog(
                applicationContext, this@SpecialOffersActivity,
                resources.getString(R.string.popup_note_title),
                message
            )
        }
        LocalSharedPrefStorage(this@SpecialOffersActivity).addInt(
            LocalSharedPrefStorage.PREF_BUNDLE_PAGE,
            currentVisit
        )
    }

    private fun subscribe() {
        val subscribeToSpecialOfferOfferResponseLiveData = object : Observer<MySubscriptionsSuccessAfterOfferSubscribeResponse> {
            override fun onChanged(t: MySubscriptionsSuccessAfterOfferSubscribeResponse?) {
                if (!this@SpecialOffersActivity.isFinishing) {
                    if (t != null && t.data != null && t.data.mySubscriptionsData != null) {

                        if (t.data.message != null && hasValue(t.data.message)) {
                            inAppFeedback(this@SpecialOffersActivity,t.data.message)
//                            showMessageDialog(applicationContext, this@SpecialOffersActivity,
//                                    resources.getString(R.string.popup_note_title),
//                                    t.data.message)
                        }

                        /** update the mysubscriptions response*/
                        if (UserDataManager.getMySubscriptionResponse() != null) {
                            val mySubscriptions = UserDataManager.getMySubscriptionResponse()

                            if (mySubscriptions!!.data != null &&
                                    t.data.mySubscriptionsData.internet != null) {
                                mySubscriptions.data!!.internet = t.data.mySubscriptionsData.internet
                            }

                            if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.sms != null) {
                                mySubscriptions.data!!.sms = t.data.mySubscriptionsData.sms
                            }

                            if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.call != null) {
                                mySubscriptions.data!!.call = t.data.mySubscriptionsData.call
                            }

                            if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.campaign != null) {
                                mySubscriptions.data!!.campaign = t.data.mySubscriptionsData.campaign
                            }

                            if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.hybrid != null) {
                                mySubscriptions.data!!.hybrid = t.data.mySubscriptionsData.hybrid
                            }

                            if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.voiceInclusiveOffers != null) {
                                mySubscriptions.data!!.voiceInclusiveOffers = t.data.mySubscriptionsData.voiceInclusiveOffers
                            }

                            if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.roaming != null) {
                                mySubscriptions.data!!.roaming = t.data.mySubscriptionsData.roaming
                            }

                            if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.smsInclusiveOffers != null) {
                                mySubscriptions.data!!.smsInclusiveOffers = t.data.mySubscriptionsData.smsInclusiveOffers
                            }

                            if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.tm != null) {
                                mySubscriptions.data!!.tm = t.data.mySubscriptionsData.tm
                            }

                            if (mySubscriptions.data != null &&
                                    t.data.mySubscriptionsData.internetInclusiveOffers != null) {
                                mySubscriptions.data!!.internetInclusiveOffers = t.data.mySubscriptionsData.internetInclusiveOffers
                            }

                            UserDataManager.setMySubscriptionResponse(mySubscriptions)
                        }

                        /**notify the data adapter in each fragment of this activity*/
                        if (RootValues.getSupplementaryOffersSubscriptionEvents() != null) {
                            RootValues.getSupplementaryOffersSubscriptionEvents()!!.onSupplementaryOffersSubscriptionSuccess()
                        } else {
                            logE("AdapterXu", "listener is null", fromClass, "initSupplementaryOffersCardAdapterEvents")
                        }
                    }
                }
            }
        }

        mViewModel.subscribeToSpecialOfferOfferResponseLiveData.observe(this@SpecialOffersActivity, subscribeToSpecialOfferOfferResponseLiveData)

        val subscribeToSpecialOfferErrorResponseLiveData = object : Observer<AzerAPIErrorHelperResponse> {
            override fun onChanged(response: AzerAPIErrorHelperResponse?) {
                if (!this@SpecialOffersActivity.isFinishing) {
                    //check internet
                    if (!isNetworkAvailable(this@SpecialOffersActivity)) {
                        showMessageDialog(applicationContext, this@SpecialOffersActivity,
                                resources.getString(R.string.popup_note_title),
                                resources.getString(R.string.message_no_internet))
                    } else {
                        /**check the error code
                         * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                         * else show a general api failure popup*/
                        if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                            showMessageDialog(applicationContext, this@SpecialOffersActivity,
                                    this@SpecialOffersActivity.resources.getString(R.string.popup_error_title),
                                    response.errorDetail.toString())
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                            //force logout the user here
                            logOut(applicationContext, this@SpecialOffersActivity, false, fromClass, "initSupplementaryOffersCardAdapterEventsa34324")
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                            //handle the ui in this case
                            //hide or show the no data found layout in this case
                            showMessageDialog(applicationContext, this@SpecialOffersActivity,
                                    this@SpecialOffersActivity.resources.getString(R.string.popup_error_title),
                                    this@SpecialOffersActivity.resources.getString(R.string.server_stopped_responding_please_try_again))
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                            showMessageDialog(applicationContext, this@SpecialOffersActivity,
                                    this@SpecialOffersActivity.resources.getString(R.string.popup_error_title),
                                    this@SpecialOffersActivity.resources.getString(R.string.server_stopped_responding_please_try_again))
                        } else {
                            showMessageDialog(applicationContext, this@SpecialOffersActivity,
                                    this@SpecialOffersActivity.resources.getString(R.string.popup_error_title),
                                    this@SpecialOffersActivity.resources.getString(R.string.server_stopped_responding_please_try_again))
                        }
                    }
                }// if (activity != null && !this@SupplementaryOffersActivity.isFinishing)  ends
            }
        }

        mViewModel.subscribeToSpecialOfferErrorResponseLiveData.observe(this@SpecialOffersActivity, subscribeToSpecialOfferErrorResponseLiveData)
    }

    private fun initSpecialOffersCardEvents() {
        val supplementarySpecialOffersCardAdapterEvents = object : SupplementarySpecialOffersCardAdapterEvents {
            override fun onSupplementaryOfferSubscribeButtonClick(titleMessage: String, offeringId: String, offeringName: String, actionType: String) {
                if (!this@SpecialOffersActivity.isFinishing) {
                    logE("offerSubscribe", "offername:::".plus(offeringName).plus(" actionType:::$actionType")
                            .plus(" offeringId:::").plus(offeringId), fromClass, "initRoamingCardEvents")
                    /**call the api to subscribe the offer*/
                    showSpecialOfferSubscribePopup(titleMessage, offeringName, offeringId, actionType)
                }
            }
        }

        RootValues.setSupplementarySpecialOffersCardAdapterEvents(supplementarySpecialOffersCardAdapterEvents)
    }

    @SuppressLint("InflateParams")
    private fun showSpecialOfferSubscribePopup(title: String, offeringName: String, offeringId: String, actionType: String) {
        if (this@SpecialOffersActivity.isFinishing) return

        val dialogBuilder = AlertDialog.Builder(this@SpecialOffersActivity)
        val inflater = this@SpecialOffersActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val dialogView = inflater.inflate(R.layout.azer_vas_services_confirmation_popup, null)
        dialogBuilder.setView(dialogView)

        val alertDialog = dialogBuilder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.show()

        val popupTitle = dialogView.findViewById(R.id.popupTitle) as TextView
        popupTitle.typeface = getALSNormalFont()
        popupTitle.text = title
        val popupOkayButton = dialogView.findViewById(R.id.yesButton) as Button
        popupOkayButton.typeface = getALSBoldFont()
        val popupCancelButton = dialogView.findViewById(R.id.noButton) as Button
        popupCancelButton.typeface = getALSBoldFont()

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN) {
            //english
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "No"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ) {
            //azri
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "Xeyr"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.RU) {
            //russian
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "Нет"
        }

        popupOkayButton.setOnClickListener {
            alertDialog.dismiss()
            mViewModel.subscribeToSupplementaryOffer(offeringName, offeringId, actionType)
        }

        popupCancelButton.setOnClickListener {
            alertDialog.dismiss()
        }
    }

    private fun requestMySubscriptions() {
        var offeringId = ""
        var brandName = ""

        if (UserDataManager.getCustomerData() != null) {
            if (hasValue(UserDataManager.getCustomerData()!!.offeringName)) {
                offeringId = UserDataManager.getCustomerData()!!.offeringId!!
            }

            if (hasValue(UserDataManager.getCustomerData()!!.brandName)) {
                brandName = UserDataManager.getCustomerData()!!.brandName!!
            }
        }

        mViewModel.requestMySubscriptions(offeringId, brandName)
    }//requestMySubscriptions ends

    private fun subscribeToMySubscriptionsResponseLiveData() {
        val mySubscriptionsBeforeSpecialsResponseLiveData = object : Observer<MySubscriptionResponse> {
            override fun onChanged(t: MySubscriptionResponse?) {
                if (t != null) {
                    UserDataManager.setMySubscriptionResponse(t)
                    requestSpecialOffers()
                } else {
                    hideContents()
                }
            }
        }

        mViewModel.mySubscriptionsBeforeSpecialsResponseLiveData.observe(this@SpecialOffersActivity, mySubscriptionsBeforeSpecialsResponseLiveData)

        val mySubscriptionsBeforeSpecialsErrorResponseLiveData = object : Observer<AzerAPIErrorHelperResponse> {
            override fun onChanged(response: AzerAPIErrorHelperResponse?) {
                if (!this@SpecialOffersActivity.isFinishing) {
                    //check internet
                    hideContents()
                    requestSpecialOffers()
                    /*  if (!isNetworkAvailable(this@SpecialOffersActivity)) {
                          showMessageDialog(applicationContext, this@SpecialOffersActivity,
                                  resources.getString(R.string.popup_note_title),
                                  resources.getString(R.string.message_no_internet))
                      } else {
                          */
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*//*
                        if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                            showMessageDialog(applicationContext, this@SpecialOffersActivity,
                                    resources.getString(R.string.popup_error_title),
                                    response.errorDetail.toString())
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                            //force logout the user here
                            logOut(applicationContext, this@SpecialOffersActivity, false, fromClass, "specialOffersErrorResponseData2321sf")
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                            //handle the ui in this case
                            //hide or show the no data found layout in this case
                            showMessageDialog(applicationContext, this@SpecialOffersActivity,
                                    resources.getString(R.string.popup_error_title),
                                    resources.getString(R.string.server_stopped_responding_please_try_again))
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                            showMessageDialog(applicationContext, this@SpecialOffersActivity,
                                    resources.getString(R.string.popup_error_title),
                                    resources.getString(R.string.server_stopped_responding_please_try_again))
                        } else {
                            showMessageDialog(applicationContext, this@SpecialOffersActivity,
                                    resources.getString(R.string.popup_error_title),
                                    resources.getString(R.string.server_stopped_responding_please_try_again))
                        }
                    }*/
                }// if (activity != null && !this@SpecialOffersActivity.isFinishing)  ends
            }
        }

        mViewModel.mySubscriptionsBeforeSpecialsErrorResponseLiveData.observe(this@SpecialOffersActivity, mySubscriptionsBeforeSpecialsErrorResponseLiveData)
    }//subscribeToMySubscriptionsResponseLiveData ends

    override fun onSearchResult(isResultFound: Boolean) {
        runOnUiThread {
            foundSearchResult(isResultFound)
        }
    }//onSearchResult ends

    private fun foundSearchResult(isResultFound: Boolean) {
        if (isResultFound) {
            logE("foundResults", "found:::", fromClass, "foundSearchResult")
            mDiscreteView.visibility = View.VISIBLE
            noDataFoundLayout.visibility = View.GONE
        } else {
            logE("foundResults", "not found:::", fromClass, "foundSearchResult")
            mDiscreteView.visibility = View.GONE
            noDataFoundLayout.visibility = View.VISIBLE
            noDataFoundTextLabel.text = getNoDataFoundOnSearchMessage()
        }
    }//foundSearchResult ends

    private fun initUIEvents() {
        azerActionBarBackIcon.setOnClickListener {
            onBackPressed()
        }//azerActionBarBackIcon.setOnClickListener ends

        packagesSearchIcon.setOnClickListener {
            azerActionBarTitle.visibility = View.GONE
            searchCrossIcon.visibility = View.VISIBLE
            searchInputField.visibility = View.VISIBLE

            packagesSearchIcon.visibility = View.GONE
            packagesLayoutChangerIcon.visibility = View.GONE
            packagesFilterIcon.visibility = View.GONE
        }//packagesSearchIcon.setOnClickListener ends

        searchCrossIcon.setOnClickListener {
            azerActionBarTitle.visibility = View.VISIBLE
            searchCrossIcon.visibility = View.GONE
            searchInputField.visibility = View.GONE
            searchInputField.setText("")
            searchInputField.hint = resources.getString(R.string.supplementary_offers_search_hint)

            packagesSearchIcon.visibility = View.VISIBLE
        }//searchCrossIcon.setOnClickListener ends

        searchInputField.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (supplementaryOffersCardAdapter != null) {
                    supplementaryOffersCardAdapter!!.filter.filter(s.toString())
                }
            }
        })
    }//initUIEvents ends

    private fun initUI(offers: SupplementaryResponse) {
        var foundTabPosFromNotifications = -1
        val tmAndSpecialOffers = ArrayList<SupplementaryOffer?>()
        if (offers.data.tm != null && offers.data.tm!!.offers != null && !offers.data.tm!!.offers!!.isEmpty()) {
            tmAndSpecialOffers.addAll(offers.data.tm!!.offers!!)
        }

        if (offers.data.special != null && offers.data.special!!.offers != null && !offers.data.special!!.offers!!.isEmpty()) {
            tmAndSpecialOffers.addAll(offers.data.special!!.offers!!)
        }

        if (!tmAndSpecialOffers.isNullOrEmpty()) {
            mDiscreteView.recycledViewPool.setMaxRecycledViews(0, 0)
            mDiscreteView.setOrientation(Orientation.HORIZONTAL)
            mDiscreteView.setItemTransformer(ScaleTransformer.Builder().build())
            supplementaryOffersCardAdapter = SupplementaryOffersCardAdapter(
                    applicationContext,
                    tmAndSpecialOffers,
                    false,
                    "",
                    "",
                    true,
                    this,
                    ConstantsUtility.PackagesConstants.TM_AND_SPECIALS,mViewModel)
            mDiscreteView.adapter = supplementaryOffersCardAdapter
            showContents()

            /**redirection check*/
            foundTabPosFromNotifications = findOfferIdAfterRedirectingFromNotifications(actionIdFromServer, tmAndSpecialOffers)

            if (foundTabPosFromNotifications != -1 && hasValue(actionIdFromServer)) {
                mDiscreteView.scrollToPosition(foundTabPosFromNotifications)
            }
        } else {
            hideContents()
        }
    }//initUI ends

    private fun findOfferIdAfterRedirectingFromNotifications(offeringId: String, offersList: ArrayList<SupplementaryOffer?>): Int {
        var foundOfferPosFromNotification = -1
        try {
            if (hasValue(offeringId) && !offersList.isNullOrEmpty()) {
                for (i in offersList.indices) {
                    if (offersList[i] != null &&
                            offersList[i]!!.header != null &&
                            hasValue(offersList[i]!!.header!!.offeringId) &&
                            offersList[i]!!.header!!.offeringId == offeringId) {
                        foundOfferPosFromNotification = i
                        break
                    }//if ends
                }//for ends
            }//outer if ends
        } catch (e: Exception) {
            logE("Packages", "error:::".plus(e.toString()), "PackagesActivity", "findOfferIdAfterRedirectingFromNotifications")
        }//catch ends

        return foundOfferPosFromNotification
    }//findOfferIdAfterRedirectingFromNotifications ends

    private fun requestSpecialOffers() {
        if (UserDataManager.getCustomerData() != null) {
            var offeringId = ""
            if (UserDataManager.getCustomerData()!!.offeringId != null && hasValue(UserDataManager.getCustomerData()!!.offeringId)) {
                offeringId = UserDataManager.getCustomerData()!!.offeringId!!
            }

            var specialOfferIds = listOf<String>()
            if (UserDataManager.getCustomerData()!!.specialOffersTariffData != null) {
                if (UserDataManager.getCustomerData()!!.specialOffersTariffData!!.offersSpecialIds != null && !UserDataManager.getCustomerData()!!.specialOffersTariffData!!.offersSpecialIds!!.isEmpty()) {
                    specialOfferIds = UserDataManager.getCustomerData()!!.specialOffersTariffData!!.offersSpecialIds!!
                }
            }

            mViewModel.requestSpecialOffers(offeringId, specialOfferIds)
        }
    }

    private fun subscribeToSpecialOffersResponseLiveData() {
        val specialOffersResponseData = object : Observer<SupplementaryResponse> {
            override fun onChanged(t: SupplementaryResponse?) {
                if (t != null) {
                    logE("spOffsX", "response:::".plus(t.toString()), fromClass, "requestSpecialOffers")
                    initUI(t)
                } else {
                    hideContents()
                }
            }
        }
        mViewModel.specialOffersResponseData.observe(this@SpecialOffersActivity, specialOffersResponseData)

        val specialOffersErrorResponseData = object : Observer<AzerAPIErrorHelperResponse> {
            override fun onChanged(response: AzerAPIErrorHelperResponse?) {
                if (!this@SpecialOffersActivity.isFinishing) {
                    //check internet
                    hideContents()
                    if (!isNetworkAvailable(this@SpecialOffersActivity)) {
                        showMessageDialog(applicationContext, this@SpecialOffersActivity,
                                resources.getString(R.string.popup_note_title),
                                resources.getString(R.string.message_no_internet))
                    } else {
                        /**check the error code
                         * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                         * else show a general api failure popup*/
                        if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                            showMessageDialog(applicationContext, this@SpecialOffersActivity,
                                    resources.getString(R.string.popup_error_title),
                                    response.errorDetail.toString())
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                            //force logout the user here
                            logOut(applicationContext, this@SpecialOffersActivity, false, fromClass, "specialOffersErrorResponseData2321sf")
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                            //handle the ui in this case
                            //hide or show the no data found layout in this case
                            showMessageDialog(applicationContext, this@SpecialOffersActivity,
                                    resources.getString(R.string.popup_error_title),
                                    resources.getString(R.string.server_stopped_responding_please_try_again))
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                            showMessageDialog(applicationContext, this@SpecialOffersActivity,
                                    resources.getString(R.string.popup_error_title),
                                    resources.getString(R.string.server_stopped_responding_please_try_again))
                        } else {
                            showMessageDialog(applicationContext, this@SpecialOffersActivity,
                                    resources.getString(R.string.popup_error_title),
                                    resources.getString(R.string.server_stopped_responding_please_try_again))
                        }
                    }
                }// if (activity != null && !this@SpecialOffersActivity.isFinishing)  ends
            }
        }

        mViewModel.specialOffersErrorResponseData.observe(this@SpecialOffersActivity, specialOffersErrorResponseData)

        val inAppSurveyResponseErrorResponseData =
            Observer<AzerAPIErrorHelperResponse> { response ->
                if (!isNetworkAvailable(this@SpecialOffersActivity)) {
                    showMessageDialog(
                        this@SpecialOffersActivity, this@SpecialOffersActivity,
                        resources.getString(R.string.popup_note_title),
                        resources.getString(R.string.message_no_internet)
                    )
                } else {
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                        showMessageDialog(
                            this@SpecialOffersActivity, this@SpecialOffersActivity,
                            this@SpecialOffersActivity.resources.getString(R.string.popup_error_title),
                            response.errorDetail.toString()
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                        //force logout the user here
                        logOut(
                            this@SpecialOffersActivity,
                            this@SpecialOffersActivity,
                            false,
                            fromClass,
                            "initSupplementaryOffersCardAdapterEventsa34324"
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                        //handle the ui in this case
                        //hide or show the no data found layout in this case
                        showMessageDialog(
                            this@SpecialOffersActivity, this@SpecialOffersActivity,
                            this@SpecialOffersActivity.resources.getString(R.string.popup_error_title),
                            this@SpecialOffersActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                        showMessageDialog(
                            this@SpecialOffersActivity, this@SpecialOffersActivity,
                            this@SpecialOffersActivity.resources.getString(R.string.popup_error_title),
                            this@SpecialOffersActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    } else {
                        showMessageDialog(
                            this@SpecialOffersActivity, this@SpecialOffersActivity,
                            this@SpecialOffersActivity.resources.getString(R.string.popup_error_title),
                            this@SpecialOffersActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    }
                }
            }
        mViewModel.inAppSurveyResponseErrorResponseData.observe(
            this@SpecialOffersActivity,
            inAppSurveyResponseErrorResponseData
        )
    }//subscribe ends

    private fun showContents() {
        mDiscreteView.visibility = View.VISIBLE
        noDataFoundLayout.visibility = View.GONE
    }//showContents ends

    private fun hideContents() {
        mDiscreteView.visibility = View.GONE
        noDataFoundLayout.visibility = View.VISIBLE
        noDataFoundTextLabel.text = getNoDataFoundSimpleMessage()
    }//showContents ends

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.special_offers_screen_title)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
        packagesFilterIcon.visibility = View.GONE
        packagesLayoutChangerIcon.visibility = View.GONE
        packagesSearchIcon.visibility = View.VISIBLE
    }//setupHeaderActionBar ends

    companion object {
        fun start(context: Context, actionIdFromServer: String) {
            if (hasValue(actionIdFromServer)) {
                val intent = Intent(context, SpecialOffersActivity::class.java)
                intent.putExtra(ConstantsUtility.PackagesConstants.NOTIFICATIONS_SPECIAL_REDIRECTION_KEY, actionIdFromServer)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                context.startActivity(intent)
            } else {
                val intent = Intent(context, SpecialOffersActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                context.startActivity(intent)
            }
        }//start ends
    }//object ends
}//class ends