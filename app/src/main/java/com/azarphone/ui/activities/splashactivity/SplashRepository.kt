package com.azarphone.ui.activities.splashactivity

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.CheckAppUpdateRequest
import com.azarphone.api.pojo.response.checkappupdate.CheckAppUpdateResponse
import io.reactivex.Observable

/**
 * Created by Junaid Hassan on 19, February, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class SplashRepository {
    init {

    }

    fun checkAppUpdate(checkAppUpdateRequest: CheckAppUpdateRequest): Observable<CheckAppUpdateResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().checkAppUpdate(checkAppUpdateRequest)
    }


}