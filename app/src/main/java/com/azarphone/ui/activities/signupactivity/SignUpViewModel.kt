package com.azarphone.ui.activities.signupactivity

import androidx.lifecycle.MutableLiveData
import android.content.Context
import android.os.CountDownTimer
import com.azarphone.R
import com.azarphone.analytics.Analytics
import com.azarphone.analytics.EventValues
import com.azarphone.api.pojo.helperpojo.OTPTimerHelperModel
import com.azarphone.api.pojo.request.OTPRequest
import com.azarphone.api.pojo.request.OTPVerifyRequest
import com.azarphone.api.pojo.request.ResendPinRequest
import com.azarphone.api.pojo.request.SaveCustomerRequest
import com.azarphone.api.pojo.response.otpgetresponse.OTPResponse
import com.azarphone.api.pojo.response.otpverifyresponse.OTPVerifyResponse
import com.azarphone.api.pojo.response.resendpin.ResendPinResponse
import com.azarphone.api.pojo.response.savecustomerresponse.SaveCustomerResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.util.ConstantsUtility
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

class SignUpViewModel(val mRepository: SignUpRepository) : BaseViewModel() {
    lateinit var disposable: Disposable

    var countDownTimer: CountDownTimer? = null
    val signupPasswordStepperData = MutableLiveData<String>()
    var signupMsisdnLiveDataObserver = MutableLiveData<String>()

    val mResendPinResponse = MutableLiveData<ResendPinResponse>()

    val signupOTPResponseLiveData = MutableLiveData<OTPResponse>()

    val signupOTPVerifyResponseLiveData = MutableLiveData<OTPVerifyResponse>()

    val signupPasswordLiveData = MutableLiveData<String>()
    val signupConfirmPasswordLiveData = MutableLiveData<String>()

    val saveCustomerResponseLiveData = MutableLiveData<SaveCustomerResponse>()

    val signupOTPLiveData = MutableLiveData<String>()

    val signupTermsConditionsCheckLiveData = MutableLiveData<Boolean>()

    val signupOTP25SecondsTimerLiveData = MutableLiveData<OTPTimerHelperModel>()

    val isDataLoading = MutableLiveData<Boolean>()

    val isTermsAndCondtionsAccepted = MutableLiveData<Boolean>()

    init {
    }

    /**all API calling methods
     * should be here*/

    fun requestSaveCustomer(userNumber: String, password: String, confirmPassword: String, signupOTPPin: String, termsConditionsFlag: String) {
        isDataLoading.postValue(true)
        val saveCustomerRequest = SaveCustomerRequest(userNumber, "ip", password, confirmPassword, signupOTPPin, termsConditionsFlag)

        var mObserver = mRepository.requestSaveCustomer(saveCustomerRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    saveCustomerResponseLiveData.postValue(result)
                                    Analytics.logAppEvent(EventValues.SignUpEvents.sign_up, EventValues.SignUpEvents.registered, EventValues.GenericValues.Success)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                            Analytics.logAppEvent(EventValues.SignUpEvents.sign_up, EventValues.SignUpEvents.registered, EventValues.GenericValues.Failure)
                        }
                )

        addDisposable(disposable)
    }//requestSaveCustomer ends

    fun resendPin(mMsisdn: String) {
        val resendPinRequest = ResendPinRequest(ConstantsUtility.APICauses.CAUSE_FORGOT_PASSWORD_ENTER_PIN, mMsisdn)
        view()?.showLoading()

        var mObserver = mRepository.resendPin(resendPinRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    mResendPinResponse.postValue(result)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)


    }

    fun requestOTP(msisdn: String) {
        isDataLoading.postValue(true)
        val otpRequest = OTPRequest(ConstantsUtility.APICauses.CAUSE_SIGN_UP, msisdn)

        var mObserver = mRepository.requestOTP(otpRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    signupOTPResponseLiveData.postValue(result)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        addDisposable(disposable)
    }//requestOTP ends

    fun requestVerifyOTP(otpToVerify: String, msisdn: String) {
        isDataLoading.postValue(true)
        val otpVerifyRequest = OTPVerifyRequest(ConstantsUtility.APICauses.CAUSE_SIGN_UP, msisdn, otpToVerify)

        var mObserver = mRepository.requestVerifyOTP(otpVerifyRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    signupOTPVerifyResponseLiveData.postValue(result)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        addDisposable(disposable)
    }//verifySignupOTP ends

    /**any setters for this viewmodel
     * should be here*/

    fun setSignupPasswordStep(stepInfo: String) {
        signupPasswordStepperData.value = stepInfo
    }//setSignupPasswordStep ends

    fun setTermsConditionsCheck(isChecked: Boolean) {
        signupTermsConditionsCheckLiveData.value = isChecked
    }

    fun set25SecondsTimerOff() {
        signupOTP25SecondsTimerLiveData.postValue(OTPTimerHelperModel(false, "", false))
        if (countDownTimer != null) {
            countDownTimer!!.cancel()
        }
    }//set25SecondsTimerOff ends

    fun set25SecondsTimer(context: Context) {
        var count = 25
        countDownTimer = object : CountDownTimer(26000, 1000) {

            override fun onTick(millisUntilFinished: Long) {
                count -= 1
                if (count == -1) {
                    val text = context.resources.getString(R.string.signup_password_timer_text, "" + 0)
                    signupOTP25SecondsTimerLiveData.postValue(OTPTimerHelperModel(false, text, true))
                } else {
                    val text = context.resources.getString(R.string.signup_password_timer_text, "" + count)
                    signupOTP25SecondsTimerLiveData.postValue(OTPTimerHelperModel(false, text, true))
                }
            }

            override fun onFinish() {
                signupOTP25SecondsTimerLiveData.postValue(OTPTimerHelperModel(true, "", true))
            }
        }.start()
    }//set25SecondsTimer ends
}//class ends