package com.azarphone.ui.activities.mysubscriptions

/**
 * @author Junaid Hassan on 22, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
object MySubscriptionsInjection {

    fun provideMySubscriptionsRepository(): MySubscriptionsRepositry {
        return MySubscriptionsRepositry()
    }

    fun provideMySubscriptionsFactory(): MySubscriptionsFactory {
        return MySubscriptionsFactory(provideMySubscriptionsRepository())
    }

}