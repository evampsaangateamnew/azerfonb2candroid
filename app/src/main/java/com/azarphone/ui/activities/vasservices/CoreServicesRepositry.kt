package com.azarphone.ui.activities.vasservices

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.CoreServiceProcessRequest
import com.azarphone.api.pojo.request.CoreServicesRequest
import com.azarphone.api.pojo.request.SupplementaryOfferSubscribeRequest
import com.azarphone.api.pojo.response.coreservices.CoreServicesResponse
import com.azarphone.api.pojo.response.coreservicesprocessnumber.CoreServicesProcessResponse
import com.azarphone.api.pojo.response.mysubscriptions.successresponse.MySubscriptionsSuccessAfterOfferSubscribeResponse
import io.reactivex.Observable

/**
 * @author Junaid Hassan on 24, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class CoreServicesRepositry {
    init {

    }//init ends

    fun requestCoreServices(coreServicesRequest: CoreServicesRequest): Observable<CoreServicesResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestCoreServices(coreServicesRequest)
    }//requestCoreServices ends

    fun requestProcessCoreService(coreServiceProcessRequest: CoreServiceProcessRequest): Observable<CoreServicesProcessResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestProcessCoreService(coreServiceProcessRequest)
    }//requestSetForwardNumberForCore ends

    fun changeSupplementaryOfferForCallDivert(supplementaryOfferSubscribeRequest: SupplementaryOfferSubscribeRequest): Observable<MySubscriptionsSuccessAfterOfferSubscribeResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().changeSupplementaryOfferForCallDivert(supplementaryOfferSubscribeRequest)
    }//requestSuspendNumber ends
}//class ends