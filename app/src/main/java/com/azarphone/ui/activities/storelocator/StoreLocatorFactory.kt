package com.azarphone.ui.activities.storelocator

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class StoreLocatorFactory(val mStoreRepository: StoreLocatorRepositry) : ViewModelProvider.NewInstanceFactory() {


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(StoreLocatorViewModel::class.java)) {
            return StoreLocatorViewModel(mStoreRepository) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}