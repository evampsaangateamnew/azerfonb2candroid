package com.azarphone.ui.activities.autopayment

import androidx.lifecycle.MutableLiveData
import com.azarphone.api.pojo.response.autopaymentresponse.ScheduledPaymentResponse
import com.azarphone.api.pojo.response.autopaymentresponse.deletepayment.DeleteAutoPayment
import com.azarphone.api.pojo.response.fasttopupresponse.deletefasttopup.DeleteItemResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.logE
import com.azarphone.validators.hasValue
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

/**
 * @author Umair Mustafa on 02/04/2021
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * umair.mustafa@evampsaanga.com
 */
class AutoPaymentViewModel(val mAutoPaymentRepositry: AutoPaymentRepositry) : BaseViewModel() {
    val getScheduledPaymentsResponseLiveData = MutableLiveData<ScheduledPaymentResponse>()
    val deleteScheduledPaymentResponseLiveData = MutableLiveData<DeleteItemResponse>()
    val deleteScheduledPaymentErrorLiveData = MutableLiveData<String>()
    private lateinit var disposable: Disposable

    init {

    }

    fun requestGetScheduledPayments() {
        view()?.showLoading()

        var mObserver = mAutoPaymentRepositry.requestGetScheduledPayments()
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    getScheduledPaymentsResponseLiveData.postValue(result)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestGetFastPayments ends

    fun requestDeleteScheduledPayment(deleteAutoPayment: DeleteAutoPayment) {
        view()?.showLoading()

        var mObserver = mAutoPaymentRepositry.requestDeletePaymentScheduler(deleteAutoPayment)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true || result.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
//                                    parseDeleteItemResponseInResponse(result)
                                    deleteScheduledPaymentResponseLiveData.postValue(result)
                                } else {
                                    deleteScheduledPaymentErrorLiveData.postValue("error")
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                            deleteScheduledPaymentErrorLiveData.postValue("error")
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestGetFastPayments ends

    private fun parseDeleteItemResponseInResponse(response: DeleteItemResponse) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null && hasValue(response.data.toString())) {

                    logE("mySubBG", "bgResponse:::".plus(response.toString()), "AutoPaymentViewModel", "parseDeleteItemResponseInResponse")
                    //process the response here
                    deleteScheduledPaymentResponseLiveData.postValue(response)
                } else {
                    deleteScheduledPaymentErrorLiveData.postValue("error")
                }
            } else {
                deleteScheduledPaymentErrorLiveData.postValue("error")
            }
        } else {
            deleteScheduledPaymentErrorLiveData.postValue("error")
        }
        //hide the dialog
        view()?.hideLoading()
    }//parseResponse ends

}