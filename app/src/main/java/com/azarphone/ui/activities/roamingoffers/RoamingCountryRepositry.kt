package com.azarphone.ui.activities.roamingoffers

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.SupplementaryOfferSubscribeRequest
import com.azarphone.api.pojo.response.inappsurvey.InAppSurvey
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.mysubscriptions.successresponse.MySubscriptionsSuccessAfterOfferSubscribeResponse
import io.reactivex.Observable

/**
 * @author Junaid Hassan on 24, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class RoamingCountryRepositry {
    init {

    }//init ends

    fun subscribeToSupplementaryOffer(supplementaryOfferSubscribeRequest: SupplementaryOfferSubscribeRequest): Observable<MySubscriptionsSuccessAfterOfferSubscribeResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().subscribeToSupplementaryOffer(supplementaryOfferSubscribeRequest)
    }//requestSuspendNumber ends
    fun saveInAppSurvey(uploadSurvey: UploadSurvey): Observable<InAppSurvey> {
        return ApiClient.newApiClientInstance.getServerAPI().saveInAppSurvey(uploadSurvey)
    }

}//class ends