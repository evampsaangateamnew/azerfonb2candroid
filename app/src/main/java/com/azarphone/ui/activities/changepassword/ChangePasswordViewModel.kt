package com.azarphone.ui.activities.changepassword

import androidx.lifecycle.MutableLiveData
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.request.ChangePasswordRequest
import com.azarphone.api.pojo.response.changepassword.ChangePasswordResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

/**
 * @author Junaid Hassan on 04, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class ChangePasswordViewModel(val mChangePasswordRepositry: ChangePasswordRepositry) : BaseViewModel() {

    private val fromClass = "ChangePasswordViewModel"
    lateinit var disposable: Disposable
    val changePasswordResponseLiveData = MutableLiveData<ChangePasswordResponse>()
    val changePasswordErrorResponseLiveData = MutableLiveData<AzerAPIErrorHelperResponse>()

    init {
    }

    fun changePassword(oldPass: String, newPass: String, confirmPass: String, msisdn: String) {
        logE("changePassword", "called:::changingPassword:::", fromClass, "changePassword")
        view()?.showLoading()

        /**encrypt all the password values here*/
        var oldValEncript = ""
        var newValEncript = ""
        var confirmValEncript = ""

        try {
            val decriptedKey = decrypt(
                    RootValues.getCLASS_NAME_SE(),
                    RootValues.getCLASS_NAME_M())

            oldValEncript = encrypt(oldPass, decriptedKey.trim())
            newValEncript = encrypt(newPass, decriptedKey.trim())
            confirmValEncript = encrypt(confirmPass, decriptedKey.trim())
        } catch (e: Exception) {
            logE("changePassword", "error:::".plus(e.message), fromClass, "changePassword")
        }

        val changePasswordRequest = ChangePasswordRequest(confirmValEncript, oldValEncript, newValEncript, msisdn)
        var mObserver = mChangePasswordRepositry.changePassword(changePasswordRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            parseResponse(result)
                        },
                        { error ->
                            view()?.hideLoading()
                            changePasswordErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                        }
                )
        compositeDisposables?.add(disposable)
    }//changePassword ends

    private fun parseResponse(response: ChangePasswordResponse) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        changePasswordResponseLiveData.postValue(response)
                    } else {
                        /**the response has no data object
                         * in this case we have to handle the ui only*/
                        changePasswordErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                    }
                } else {
                    /**the response has no data object
                     * in this case we have to handle the ui only*/
                    changePasswordErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                }
            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                /**force log out user case
                 * when the server sends the result code as 7*/
                changePasswordErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT, ""))
            } else {
                /**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*/
                if (hasValue(response.resultDesc.toString())) {
                    changePasswordErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
                } else {
                    changePasswordErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
                }
            }
        } else {
            /**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*/
            if (hasValue(response.resultDesc.toString())) {
                changePasswordErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
            } else {
                changePasswordErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }//parseResponse ends
}//class ends