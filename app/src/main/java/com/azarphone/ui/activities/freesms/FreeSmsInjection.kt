package com.azarphone.ui.activities.freesms

/**
 * @author Junaid Hassan on 08, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
object FreeSmsInjection{
    fun provideFreeSmsRepository(): FreeSmsRepositry {
        return FreeSmsRepositry()
    }

    fun provideFreeSmsFactory(): FreeSmsFactory {
        return FreeSmsFactory(provideFreeSmsRepository())
    }
}