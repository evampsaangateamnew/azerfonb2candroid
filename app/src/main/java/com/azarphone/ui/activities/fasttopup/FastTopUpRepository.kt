package com.azarphone.ui.activities.fasttopup

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.DeleteFastPaymentRequest
import com.azarphone.api.pojo.request.MakePaymentRequest
import com.azarphone.api.pojo.response.fasttopupresponse.FastTopUpResponse
import com.azarphone.api.pojo.response.makepayment.MakePaymentTopUpResponse
import com.azarphone.api.pojo.response.fasttopupresponse.deletefasttopup.DeleteItemResponse
import io.reactivex.Observable

class FastTopUpRepository {
    fun requestGetFastPayment(): Observable<FastTopUpResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestGetFastPayments()
    }

    fun requestDeleteFastPayment(deleteFastPaymentRequest: DeleteFastPaymentRequest): Observable<DeleteItemResponse>{
        return ApiClient.newApiClientInstance.getServerAPI().deleteFastTopUpPayment(deleteFastPaymentRequest)
    }

    fun requestMakePayment(makePaymentRequest: MakePaymentRequest): Observable<MakePaymentTopUpResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().makePayment(makePaymentRequest)
    }

}