package com.azarphone.ui.activities.emailactivity

object EmailInjectionUtils {

    fun provideEmailRepository(): EmailRepositry {
        return EmailRepositry()
    }

    fun provideEmailFactory(): EmailFactory {
        return EmailFactory(provideEmailRepository())
    }

}