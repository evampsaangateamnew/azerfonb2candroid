package com.azarphone.ui.activities.topup

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.azarphone.R
import com.azarphone.analytics.Analytics
import com.azarphone.analytics.EventValues
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.request.DeleteSavedCardRequest
import com.azarphone.api.pojo.request.InitiatePaymentRequest
import com.azarphone.api.pojo.request.MakePaymentRequest
import com.azarphone.api.pojo.request.TopupRequest
import com.azarphone.api.pojo.response.fasttopupresponse.deletefasttopup.DeleteItemResponse
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.makepayment.MakePaymentTopUpResponse
import com.azarphone.api.pojo.response.topup.TopupResponse
import com.azarphone.api.pojo.response.topup.initiatepayment.InitiatePaymentResponse
import com.azarphone.api.pojo.response.topup.savedcards.SavedCardsResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.ui.dialogs.InAppThankYouDialog
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.logE
import com.azarphone.validators.hasValue
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

/**
 * @author Junaid Hassan on 03, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class TopupViewModel(val mTopupRepositry: TopupRepositry) : BaseViewModel() {

    private val fromClass = "TopupViewModel"
    private lateinit var disposable: Disposable
    val topupResponseLiveData = MutableLiveData<TopupResponse>()
    val makePaymentResponseLiveData = MutableLiveData<MakePaymentTopUpResponse>()
    val savedCardsResponseLiveData = MutableLiveData<SavedCardsResponse>()
    val initiatePaymentResponseLiveData = MutableLiveData<InitiatePaymentResponse>()
    val deleteSavedCardsResponseLiveData = MutableLiveData<DeleteItemResponse>()
    val deleteSavedCardsErrorLiveData = MutableLiveData<String>()
    val inAppSurveyResponseErrorResponseData = MutableLiveData<AzerAPIErrorHelperResponse>()

    init {

    }

    fun showLoader() {
        view()?.showLoading()
    }

    fun hideLoader() {
        view()?.hideLoading()
    }

    fun uploadInAppSurvey(context: Context, uploadSurvey: UploadSurvey, onTransactionComplete: String) {
        view()?.showLoading()
        var observable = mTopupRepositry.saveInAppSurvey(uploadSurvey)
        observable = observable.onErrorReturn { throwable -> null }
        disposable = observable
            .compose(applyIOSchedulers())
            .subscribe(
                { appSurvey ->
                    view()?.hideLoading()
                    when {
                        appSurvey?.data != null -> {
                            UserDataManager.setInAppSurvey(appSurvey)
                            val thankYouDialog =
                                InAppThankYouDialog(context, onTransactionComplete)
                            thankYouDialog.show()
                        }
                        appSurvey?.resultDesc != null -> {
                            inAppSurveyResponseErrorResponseData.postValue(
                                AzerAPIErrorHelperResponse(
                                    ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                    appSurvey.resultDesc
                                )
                            )
                        }
                        else -> {
                            inAppSurveyResponseErrorResponseData.postValue(
                                AzerAPIErrorHelperResponse(
                                    ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                    context.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            )
                        }
                    }
                },
                { error ->
                    inAppSurveyResponseErrorResponseData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            error.message
                        )
                    )
                }
            )
        compositeDisposables?.add(disposable)
    }

    fun requestTopUp(cardNumber: String, recieverNumber: String, subscribeType: String) {
        view()?.showLoading()
        val topupRequest = TopupRequest(cardNumber, recieverNumber, subscribeType)

        var mObserver = mTopupRepositry.requestTopUp(topupRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    topupResponseLiveData.postValue(result)
                                    Analytics.logAppEvent(EventValues.TopUpEvents.top_up, EventValues.TopUpEvents.nar_card, EventValues.GenericValues.Success)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                            Analytics.logAppEvent(EventValues.TopUpEvents.top_up, EventValues.TopUpEvents.nar_card, EventValues.GenericValues.Failure)
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestTopUp ends

    fun requestTopUpCards() {
        view()?.showLoading()

        var mObserver = mTopupRepositry.requestSavedCards()
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()/*
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {*/
                            savedCardsResponseLiveData.postValue(result)
                            Analytics.logAppEvent(EventValues.TopUpEvents.top_up, EventValues.TopUpEvents.nar_card, EventValues.GenericValues.Success)
                            /*  }
                          }*/
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                            Analytics.logAppEvent(EventValues.TopUpEvents.top_up, EventValues.TopUpEvents.nar_card, EventValues.GenericValues.Failure)
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestTopUpCards ends

    fun requestInitiatePayment(initiatePaymentRequest: InitiatePaymentRequest) {
        view()?.showLoading()

        var mObserver = mTopupRepositry.requestInitiatePayment(initiatePaymentRequest)
        mObserver = mObserver.onErrorReturn { null }

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    initiatePaymentResponseLiveData.postValue(result)
                                    Analytics.logAppEvent(EventValues.TopUpEvents.top_up, EventValues.TopUpEvents.nar_card, EventValues.GenericValues.Success)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                            Analytics.logAppEvent(EventValues.TopUpEvents.top_up, EventValues.TopUpEvents.nar_card, EventValues.GenericValues.Failure)
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestInitiatePayment ends

    fun requestMakePayment(makePaymentRequest: MakePaymentRequest) {
        view()?.showLoading()

        var mObserver = mTopupRepositry.requestMakePayment(makePaymentRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    makePaymentResponseLiveData.postValue(result)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestGetFastPayments ends

    fun requestDeleteSavedCard(deleteSavedCardRequest: DeleteSavedCardRequest) {
        view()?.showLoading()

        var mObserver = mTopupRepositry.requestDeleteTopUpCard(deleteSavedCardRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true || result.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                                    deleteSavedCardsResponseLiveData.postValue(result)
                                } else {
                                    deleteSavedCardsErrorLiveData.postValue("error")
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                            deleteSavedCardsErrorLiveData.postValue("error")
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestGetFastPayments ends

    private fun parseDeleteItemResponseInResponse(response: DeleteItemResponse) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null && hasValue(response.data.toString())) {

                    logE("mySubBG", "bgResponse:::".plus(response.toString()), "FastTopUpViewModel", "parseDeleteItemResponseInResponse")
                    //process the response here
                    deleteSavedCardsResponseLiveData.postValue(response)
                } else {
                    deleteSavedCardsErrorLiveData.postValue("error")
                }
            } else {
                deleteSavedCardsErrorLiveData.postValue("error")
            }
        } else {
            deleteSavedCardsErrorLiveData.postValue("error")
        }
        //hide the dialog
        view()?.hideLoading()
    }//parseResponse ends
}