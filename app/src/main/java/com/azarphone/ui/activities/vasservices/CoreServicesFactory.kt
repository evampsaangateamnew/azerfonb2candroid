package com.azarphone.ui.activities.vasservices

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

/**
 * @author Junaid Hassan on 24, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class CoreServicesFactory(val mCoreServicesRepositry: CoreServicesRepositry) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CoreServicesViewModel::class.java)) {
            return CoreServicesViewModel(mCoreServicesRepositry) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}