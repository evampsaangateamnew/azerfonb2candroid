package com.azarphone.ui.activities.userprofile

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import androidx.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.provider.MediaStore
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AlertDialog
import android.util.Base64
import android.view.*
import android.widget.*
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.response.changebillinglanguagereponse.ChangeBillingResponse
import com.azarphone.api.pojo.response.profilepictureupload.ProfilePictureUpdateResponse
import com.azarphone.api.pojo.response.suspendnumber.SuspendNumberResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.LayoutUserProfileBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.ui.activities.emailactivity.EmailActivity
import com.azarphone.ui.activities.mainactivity.MainActivity
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.widgets.popups.showMessageDialog
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.layout_user_profile.*
import java.io.ByteArrayOutputStream
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * @author Junaid Hassan on 19, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class UserProfileActivity : BaseActivity<LayoutUserProfileBinding, UserProfileFactory, UserProfileViewModel>() {

    private val fromClass = "UserProfileActivity"
    private var selectedLanguage = ""
    private var target: Target? = null

    override fun getLayoutId(): Int {
        return R.layout.layout_user_profile
    }//getLayoutId ends

    override fun getViewModelClass(): Class<UserProfileViewModel> {
        return UserProfileViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): UserProfileFactory {
        return UserProfileInjection.provideUserProfileFactory()
    }//getFactory ends

    override fun init() {
        setupHeaderActionBar()
        subscribe()
        subscribeForChangeBilling()
        azerActionBarBackIcon.setOnClickListener { onBackPressed() }

        /**init the profile fields*/
        loadProfileImage()
        initProfileUI()

        initUIEvents()

        billingLanguageLabel.isSelected = true
        iccdLabel.isSelected = true
        pukLabel.isSelected = true
        emailLabel.isSelected = true
        suspendNumberLabel.isSelected = true
        pinLabel.isSelected = true
        iccdDateLabel.isSelected = true
        iccdDateValue.isSelected = true
    }//init ends

    private fun initUIEvents() {
        billingLanguageLabel.setOnClickListener {
            showChangeLanguagePopup()
        }//billingLanguageLabel.setOnClickListener ends

        emailLayout.setOnClickListener {
            EmailActivity.start(this)
        }//emailLabel.setOnClickListener ends

        suspendNumberRow.setOnClickListener {
            confirmSuspendNumber(resources.getString(R.string.profile_suspend_number_message))
        }//suspendNumberRow.setOnClickListener  ends

        profile_image.setOnClickListener {
            //show profile picture load dialog
            showUploadProfilePictureDialog()
        }//profile_image.setOnClickListener ends
    }//initUIEvents ends

    private fun loadProfileImage() {
        if (hasValue(getProfileImageFromLocalPreferences())) {
            val imageUrl = getProfileImageFromLocalPreferences()
            logE("profileImageX", "url:::".plus(imageUrl), fromClass, "loadProfileImage")
            createTarget()
            if (target != null) {
                Picasso.with(this@UserProfileActivity).load(imageUrl).networkPolicy(NetworkPolicy.NO_CACHE)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .into(target)
            } //if target is null
        } //if url is null
        else {
            //get the url from the login data and recursively call this method again!
            if (UserDataManager.getCustomerData() != null &&
                    UserDataManager.getCustomerData()!!.imageURL != null &&
                    hasValue(UserDataManager.getCustomerData()!!.imageURL)) {
                val url = UserDataManager.getCustomerData()!!.imageURL
                setProfileImageToLocalPreferences(url!!)
                loadProfileImage()
            }
        }
    }//loadProfileImage ends

    private fun createTarget() {
        target = object : Target {
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {

            }//onPrepareLoad ends

            override fun onBitmapFailed(errorDrawable: Drawable?) {

            }//onBitmapFailed ends

            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                try {
                    profile_image.setImageBitmap(bitmap)
                } catch (e: Exception) {
                    logE("picasso", "error:::".plus(e.toString()), fromClass, "onBitmapLoaded")
                }//catch ends
            }//onBitmapLoaded ends
        }//target ends
    }//create target ends

    @SuppressLint("InflateParams")
    private fun showUploadProfilePictureDialog() {
        val dialogBuilder = AlertDialog.Builder(this)
        val inflater = layoutInflater
        val dialogView = inflater.inflate(R.layout.profile_picture_picker_dialog, null)
        dialogBuilder.setView(dialogView)
        //create the dialog
        val alertDialog = dialogBuilder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        //ui vars
        val galleryPickerRow = dialogView.findViewById(R.id.galleryPickerRow) as RelativeLayout
        val cameraPickerRow = dialogView.findViewById(R.id.cameraPickerRow) as RelativeLayout
        val removePictureRow = dialogView.findViewById(R.id.removePictureRow) as RelativeLayout
        val param = alertDialog.window!!.attributes
        param.gravity = Gravity.TOP or Gravity.CENTER_HORIZONTAL
        param.y = 400
        alertDialog.window!!.attributes = param
        //show the dialog
        alertDialog.show()
        alertDialog.window!!.setLayout(resources.getDimensionPixelSize(R.dimen._180sdp), WindowManager.LayoutParams.WRAP_CONTENT)
        //ui events
        galleryPickerRow.setOnClickListener {
            //select image from the gallery
            alertDialog.dismiss()
            if (ActivityCompat.checkSelfPermission(this@UserProfileActivity,
                            Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                pickImageFromGallery()
            } else {
                performExternalStoragePermission(ConstantsUtility.PermissionConstants.EXTERNAL_STORAGE_FOR_GALLERY)
            }//else ends
        }//galleryPickerRow.setOnClickListener ends

        cameraPickerRow.setOnClickListener {
            //select image from the gallery
            alertDialog.dismiss()
            if (ActivityCompat.checkSelfPermission(this@UserProfileActivity,
                            Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                captureImageFromCamera()
            } else {
                performCameraPermission(ConstantsUtility.PermissionConstants.FOR_CAMERA)
            }//else ends
        }//galleryPickerRow.setOnClickListener ends

        removePictureRow.setOnClickListener {
            mViewModel.requestRemoveProfileImageFromServer("", ".jpg", "3")
            alertDialog.dismiss()
        }
    }//showUploadProfilePictureDialog ends

    private fun pickImageFromGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, getString(R.string.profile_select_label)),
                ConstantsUtility.RequestCodeConstants.PICK_IMAGE_FROM_GALLERY)
    }//pickImageFromGallery ends

    private fun captureImageFromCamera() {
        //capture image from camera
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(cameraIntent,
                ConstantsUtility.RequestCodeConstants.PICK_IMAGE_FROM_CAMERA)
    }//captureImageFromCamera ends

    private fun performCameraPermission(permissionCode: Int) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE), permissionCode)
        }
    }//performCameraPermission ends

    private fun performExternalStoragePermission(permissionCode: Int) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE), permissionCode)
        }
    }//performExternalStoragePermission ends

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        logE("reqCX", "requestCode:::".plus(requestCode), fromClass, "onRequestPermissionsResult")
        if (requestCode == ConstantsUtility.PermissionConstants.EXTERNAL_STORAGE_FOR_GALLERY) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                pickImageFromGallery()
            }
        } else if (requestCode == ConstantsUtility.PermissionConstants.FOR_CAMERA) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                captureImageFromCamera()
            }
        }
    }//onRequestPermissionsResult ends

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ConstantsUtility.RequestCodeConstants.PICK_IMAGE_FROM_GALLERY
                && resultCode == RESULT_OK && data != null && data.data != null) {
            val uri = data.data
            if (uri != null) {
                CropImage.activity(uri).setCropMenuCropButtonTitle(getLocalizedCropButtonTitle()).start(this)
            }//inner if ends
        }//outer if ends

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (result != null) {
                if (resultCode == RESULT_OK) {
                    val resultUri = result.uri
                    uploadImage(resultUri)
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    val error = result.error
                }//else if ends
            }//inner if ends
        }//outer if ends

        if (data != null && data.extras != null && requestCode ==
                ConstantsUtility.RequestCodeConstants.PICK_IMAGE_FROM_CAMERA && resultCode == RESULT_OK) {
            val photo = data.extras!!.get("data") as Bitmap
            val tempUri = getImageUri(this@UserProfileActivity, photo)
            if (tempUri != null) {
                CropImage.activity(tempUri).setCropMenuCropButtonTitle(getLocalizedCropButtonTitle()).start(this)
            }//inner most if ends
        }//outer if ends
    }//override fun onActivityResult ends

    private fun getImageUri(inContext: Context?, inImage: Bitmap?): Uri? {
        try {
            if (inContext == null || inImage == null) return null
            val path = MediaStore.Images.Media.insertImage(inContext.contentResolver,
                    inImage, getString(R.string.profile_picture), null) ?: return null
            return Uri.parse(path)
        } catch (e: Exception) {
            logE("getImageUri", "error:::".plus(e.message), fromClass, "getImageUri")
        }//catch ends
        return null
    }//getImageUri ends

    private fun uploadImage(uri: Uri) {
        val imageBase64 = convertImageToBase64(uri)
        val imgExt = ".jpg"
        if (imageBase64 != null) {
            mViewModel.requestUploadImageToServer(imageBase64, imgExt, "1")
        }
    }//uploadImage ends

    private fun convertImageToBase64(uri: Uri): String? {
        try {
            val bm = BitmapFactory.decodeFile(uri.path)
            val baos = ByteArrayOutputStream()
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos) //bm is the bitmap object
            val b = baos.toByteArray()
            return Base64.encodeToString(b, Base64.DEFAULT)
        } catch (e: Exception) {
            logE("profilePic", "error:::".plus(e.toString()), fromClass, "convertImageToBase64")
        }

        return null
    }//convertImageToBase64 ends

    @SuppressLint("InflateParams")
    private fun showChangeLanguagePopup() {
        val languagedialoge = Dialog(this)
        languagedialoge.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val inflater = layoutInflater
        val dialogView = inflater.inflate(R.layout.language_dialoge_layout, null)
        languagedialoge.setContentView(dialogView)
        if(languagedialoge.window!=null){
            languagedialoge.window!!.setBackgroundDrawable(null)
        }
        languagedialoge.setCancelable(true)
        languagedialoge.setCanceledOnTouchOutside(true)

        val radioGroup = dialogView.findViewById<RadioGroup>(R.id.radioGroup)
        setRadioGroup(radioGroup)
        setRadiosListener(radioGroup)

        val button = dialogView.findViewById<Button>(R.id.applyBtn)
        button.setOnClickListener {

            if (isNetworkAvailable(applicationContext)) {
                languagedialoge.dismiss()
                mViewModel.requestChangeBilling(selectedLanguage)
            } else {
                showMessageDialog(applicationContext, this@UserProfileActivity,
                        resources.getString(R.string.popup_error_title),
                        resources.getString(R.string.message_no_internet))
            }
        }//button.setOnClickListener ends
        languagedialoge.show()
    }//showChangeLanguagePopup ends

    private fun setRadioGroup(radioGroup: RadioGroup) {
        val language = UserDataManager.getCustomerData()?.billingLanguage
        when (language) {
            "3" /*ConstantsUtility.LanguageKeywords.EN*/ -> radioGroup.findViewById<RadioButton>(R.id.en).isChecked = true
            "4"/* ConstantsUtility.LanguageKeywords.AZ*/ -> radioGroup.findViewById<RadioButton>(R.id.az).isChecked = true
            "2" /*ConstantsUtility.LanguageKeywords.RU*/ -> radioGroup.findViewById<RadioButton>(R.id.ru).isChecked = true
        }
    }//setRadioGroup ends

    private fun setRadiosListener(radioGroup: RadioGroup) {
        radioGroup.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(p0: RadioGroup?, checkedId: Int) {
                setUserSelectedLanguage(checkedId)
            }
        })
    }//setRadiosListener ends

    fun setUserSelectedLanguage(checkedID: Int) {
        when (checkedID) {
            R.id.en -> selectedLanguage = /*ConstantsUtility.LanguageKeywords.EN */"3"
            R.id.ru -> selectedLanguage = /*ConstantsUtility.LanguageKeywords.RU*/ "2"
            R.id.az -> selectedLanguage = /*ConstantsUtility.LanguageKeywords.AZ*/ "4"
        }
    }//setUserSelectedLangauge ends

    fun getUserSelectedLanguage(checkedID: String): String {
        var language = ""
        when (checkedID) {
            "3" -> language = ConstantsUtility.LanguageKeywords.EN
            "2" -> language = ConstantsUtility.LanguageKeywords.RU
            "4" -> language = ConstantsUtility.LanguageKeywords.AZ
        }
        return language
    }

    private fun subscribe() {
        val suspendNumberResponseLiveData = object : Observer<SuspendNumberResponse> {
            override fun onChanged(t: SuspendNumberResponse?) {
                if (t != null) {
                    if (t.resultDesc != null && hasValue(t.resultDesc)) {
                        showMessageDialog(applicationContext, this@UserProfileActivity, "",
                                t.resultDesc)
                    } else {
                        logE("suspendX", "result desc is null or empty", fromClass, "subscribe")
                    }
                } else {
                    logE("suspendX", "response is null", fromClass, "subscribe")
                }
            }//onChanged ends
        }

        mViewModel.suspendNumberResponseLiveData.observe(this@UserProfileActivity, suspendNumberResponseLiveData)

        val removeProfilePictureResponseLiveData = object : Observer<String> {
            override fun onChanged(response: String?) {
                if (response != null) {
                    //save the url in the bakcell shared preferences
                    setProfileImageToLocalPreferences("")
                    loadProfileImage(getProfileImageFromLocalPreferences())
                    showMessageDialog(applicationContext, this@UserProfileActivity,
                            resources.getString(R.string.popup_note_title), response)
                    profile_image.setImageDrawable(resources.getDrawable(R.drawable.profileplaceholder))
                }
            }
        }

        mViewModel.removeProfilePictureResponseLiveData.observe(this@UserProfileActivity, removeProfilePictureResponseLiveData)

        val profilePictureUploadResponseLiveData = object : Observer<ProfilePictureUpdateResponse> {
            override fun onChanged(t: ProfilePictureUpdateResponse?) {
                if (t != null &&
                        t.data != null &&
                        t.data.imageURL != null && hasValue(t.data.imageURL)) {
                    logE("profileImageX", "url:::".plus(t.data.imageURL), fromClass, "uploadImageToServer")
                    //save the url in the bakcell shared preferences
                    setProfileImageToLocalPreferences(t.data.imageURL)
                    loadProfileImage(getProfileImageFromLocalPreferences())
                    if (t.resultDesc != null && hasValue(t.resultDesc)) {
                        showMessageDialog(applicationContext, this@UserProfileActivity,
                                "",
                                t.resultDesc)
                    }
                } else {
                    showMessageDialog(applicationContext, this@UserProfileActivity,
                            resources.getString(R.string.popup_error_title),
                            resources.getString(R.string.server_stopped_responding_please_try_again))
                }
            }
        }

        mViewModel.profilePictureUploadResponseLiveData.observe(this@UserProfileActivity, profilePictureUploadResponseLiveData)
    }//subscribe ends

    private fun loadProfileImage(url: String) {
        logE("profileImageX", "url:::".plus(url), fromClass, "loadProfileImage")
        createTarget()
        if (target != null) {
            if (hasValue(url)) {
                Picasso.with(this@UserProfileActivity).load(url).networkPolicy(NetworkPolicy.NO_CACHE)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .into(target)
            }//if url is null
        }//if target is null
    }//loadProfileImage ends

    private fun subscribeForChangeBilling() {
        val changeBillingResponseLiveData = object : Observer<ChangeBillingResponse> {
            override fun onChanged(t: ChangeBillingResponse?) {
                if (t != null) {
                    if (t.resultDesc != null && hasValue(t.resultDesc)) {
                        showMessageDialog(applicationContext, this@UserProfileActivity, "",
                                t.resultDesc)
                        billingLanguageValue.text = getUserSelectedLanguage(selectedLanguage)
                        UserDataManager.getCustomerData()?.billingLanguage = selectedLanguage
                    } else {
                        logE("suspendX", "result desc is null or empty", fromClass, "subscribeForChangeBilling")
                    }
                } else {
                    logE("suspendX", "response is null", fromClass, "subscribeForChangeBilling")
                }
            }//onChanged ends
        }

        mViewModel.changeBillingResponse.observe(this@UserProfileActivity, changeBillingResponseLiveData)
    }//subscribe ends

    @SuppressLint("InflateParams")
    private fun confirmSuspendNumber(title: String) {
        try {
            if (this@UserProfileActivity.isFinishing) return

            val dialogBuilder = AlertDialog.Builder(this@UserProfileActivity)
            val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val dialogView = inflater.inflate(R.layout.confirm_suspend_number_popup, null)
            dialogBuilder.setView(dialogView)

            val alertDialog = dialogBuilder.create()
            alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            alertDialog.show()

            val popupTitle = dialogView.findViewById(R.id.popupTitle) as TextView
            val yesButton = dialogView.findViewById(R.id.yesButton) as Button
            yesButton.isSelected = true
            val noButton = dialogView.findViewById(R.id.noButton) as Button
            noButton.isSelected = true
            if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN) {
                //english
                yesButton.text = "Suspend"
                noButton.text = "No"
            }

            if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ) {
                //azri
                yesButton.text = "Blok et"
                noButton.text = "Xeyr"
            }

            if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.RU) {
                //russian
                yesButton.text = "Заблокировать"
                noButton.text = "Нет"
            }

            popupTitle.text = title
            yesButton.setOnClickListener {
                alertDialog.dismiss()
                //call the webservice here
                mViewModel.requestSuspendNumber("1")
            }

            noButton.setOnClickListener {
                alertDialog.dismiss()
            }
        } catch (e: Exception) {
            logE("confirmPopup", e.toString(), fromClass, "confirmSuspendNumber")
        }//catch ends
    }//show message dialog ends

    private fun initProfileUI() {
        if (UserDataManager.getCustomerData() != null) {

            val userData = UserDataManager.getCustomerData()
            var firstName = ""
            if (hasValue(userData!!.firstName)) {
                firstName = userData.firstName!!
            }

            var iccdActivationDate = ""
            if (hasValue(userData.effectiveDate)) {
                iccdActivationDate = userData.effectiveDate!!
            }

            iccdDateValue.text = iccdActivationDate

            var lastName = ""
            if (hasValue(userData.lastName)) {
                lastName = userData.lastName!!
            }

            val fullName = firstName.plus(" ").plus(lastName)
            userProfileName.text = fullName
            userProfileName.isSelected = true

            if (hasValue(userData.msisdn)) {
                profileUserNumberLabel.text = userData.msisdn
                profileUserNumberLabel.isSelected = true
            }

            if (userData.billingLanguage != null && hasValue(userData.billingLanguage)) {
                billingLanguageValue.text = getUserSelectedLanguage(userData.billingLanguage!!)
                billingLanguageValue.isSelected = true
            }

            if (userData.sim != null && hasValue(userData.sim)) {
                iccdValue.text = userData.sim
                iccdValue.isSelected = true
            }

            if (userData.pin != null && hasValue(userData.pin)) {
                pinValue.text = userData.pin
                pinValue.isSelected = true
            }

            if (userData.puk != null && hasValue(userData.puk)) {
                pukValue.text = userData.puk
                pukValue.isSelected = true
            }

            if (userData.email != null && hasValue(userData.email)) {
                if (emailValidator(userData.email!!)) {
                    val number = userData.email!!.split("@")[0].toString().toIntOrNull()
                    val isInteger = number != null
                    if (!isInteger) {
                        emailValue.text = userData.email
                    } else {
                        emailValue.text = resources.getString(R.string.add_email_title)
                    }
                }

                emailValue.isSelected = true
            } else {
                emailValue.text = resources.getString(R.string.add_email_title)
            }

            if (UserDataManager.getCustomerData() != null && hasValue(UserDataManager.getCustomerData()!!.customerType)
                    && (UserDataManager.getCustomerData()!!.customerType!!.equals(ConstantsUtility.UserConstants.CORPORATE_CUSTOMER, ignoreCase = true)) ||
                    (UserDataManager.getCustomerData()!!.customerType!!.equals(ConstantsUtility.UserConstants.CUSTOMER_TYPE_DATA_SIM_POSTPAID_CORPORATE, ignoreCase = true))) {
                //hide the suspend number row
                suspendNumberRow.visibility = View.GONE
                view6.visibility = View.GONE
            } else {
                suspendNumberRow.visibility = View.VISIBLE
                view6.visibility = View.VISIBLE
            }
        }
    }//initProfileUI ends

    fun emailValidator(email: String): Boolean {
        val pattern: Pattern
        val matcher: Matcher
        val EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
        pattern = Pattern.compile(EMAIL_PATTERN)
        matcher = pattern.matcher(email)
        return matcher.matches()
    }

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.profile_screen_name)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

    override fun onResume() {
        super.onResume()
        loadProfileImage()
        initProfileUI()
    }//onResume ends

}//class ends