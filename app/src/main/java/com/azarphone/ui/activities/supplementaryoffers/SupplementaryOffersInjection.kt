package com.azarphone.ui.activities.supplementaryoffers

/**
 * @author Junaid Hassan on 22, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
object SupplementaryOffersInjection {

    fun provideSupplementaryOffersRepository(): SupplementaryOffersRepositry {
        return SupplementaryOffersRepositry()
    }

    fun provideSupplementaryOffersFactory(): SupplementaryOffersFactory {
        return SupplementaryOffersFactory(provideSupplementaryOffersRepository())
    }

}