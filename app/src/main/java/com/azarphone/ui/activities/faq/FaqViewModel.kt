package com.azarphone.ui.activities.faq

import androidx.lifecycle.MutableLiveData
import com.azarphone.api.pojo.response.faqresponse.FAQResponse
import com.azarphone.bases.BaseViewModel
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

class FaqViewModel(val faqRepositry: FaqRepositry) : BaseViewModel() {
    private lateinit var disposable: Disposable
    val faqResponseData = MutableLiveData<FAQResponse>()
    val faqResponseBackgroundData = MutableLiveData<FAQResponse>()

    init {

    }

    fun requestFAQs() {
        view()?.showLoading()

        var mObserver = faqRepositry.requestFAQs()
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    faqResponseData.postValue(result)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }

    fun requestFAQsBackground() {

        var mObserver = faqRepositry.requestFAQs()
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            faqResponseBackgroundData.postValue(result)
                        },
                        { error ->
                            view()?.hideLoading()
                        }
                )

        compositeDisposables?.add(disposable)
    }

}