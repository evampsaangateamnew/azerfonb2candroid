package com.azarphone.ui.activities.forgotpasswordactivity

object ForgotPasswordInjection{

    fun provideForgotPasswordRepository(): ForgotPasswordReporsitory {
        return ForgotPasswordReporsitory()
    }

    fun provideForgotPasswordFactory(): ForgotPasswordFactory {
        return ForgotPasswordFactory(provideForgotPasswordRepository())
    }

}