package com.azarphone.ui.activities.loginactivity

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class LoginFactory(val mLoginRepository: LoginRepository) : ViewModelProvider.NewInstanceFactory() {


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(mLoginRepository) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}