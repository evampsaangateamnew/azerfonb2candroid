package com.azarphone.ui.activities.datawithoutpack

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.CoreServiceProcessRequest
import com.azarphone.api.pojo.request.SupplementaryOfferSubscribeRequest
import com.azarphone.api.pojo.response.coreservicesprocessnumber.CoreServicesProcessResponse
import com.azarphone.api.pojo.response.mysubscriptions.successresponse.MySubscriptionsSuccessAfterOfferSubscribeResponse
import com.azarphone.api.pojo.response.payg.WithOutPackData
import com.azarphone.api.pojo.response.payg.WithOutPackResponse
import io.reactivex.Observable

class DataWithOutPackRepository {
    fun requestCurrentPayGStatus(): Observable<WithOutPackResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().getCurrentPayGStatus()
    }//requestCoreServices ends

    fun requestProcessCoreService(coreServiceProcessRequest: CoreServiceProcessRequest): Observable<CoreServicesProcessResponse> {
        return ApiClient.newApiClientInstance.getServerAPI()
            .requestProcessCoreService(coreServiceProcessRequest)
    }//requestSetForwardNumberForCore ends

    fun changeSupplementaryOfferForCallDivert(supplementaryOfferSubscribeRequest: SupplementaryOfferSubscribeRequest): Observable<MySubscriptionsSuccessAfterOfferSubscribeResponse> {
        return ApiClient.newApiClientInstance.getServerAPI()
            .changeSupplementaryOfferForCallDivert(supplementaryOfferSubscribeRequest)
    }//requestSuspendNumber ends

}