package com.azarphone.ui.activities.moneyrequest

/**
 * @author Junaid Hassan on 08, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
object MoneyRequestInjection{
    fun provideMoneyRequestRepository(): MoneyRequestRepositry {
        return MoneyRequestRepositry()
    }

    fun provideMoneyRequestFactory(): MoneyRequestFactory {
        return MoneyRequestFactory(provideMoneyRequestRepository())
    }
}