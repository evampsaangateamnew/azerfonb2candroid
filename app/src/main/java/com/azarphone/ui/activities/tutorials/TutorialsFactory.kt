package com.azarphone.ui.activities.tutorials

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

/**
 * @author Junaid Hassan on 20, June, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class TutorialsFactory(val mUserProfileRepositry: TutorialsRepositry) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TutorialsViewModel::class.java)) {
            return TutorialsViewModel(mUserProfileRepositry) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}