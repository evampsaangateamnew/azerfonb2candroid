package com.azarphone.ui.activities.moneyrequest

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.lifecycle.Observer
import com.azarphone.R
import com.azarphone.api.pojo.response.requestmoneyresponse.RequestMoneyResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.LayoutMoneyRequestActivityBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.ui.adapters.spinners.MoneyTransferSpinnerAdapter
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.addPrefixToUserNameField
import com.azarphone.util.getALSBoldFont
import com.azarphone.util.removeHomePageResponseFromLocalPreferences
import com.azarphone.validators.isValidMsisdn
import com.azarphone.widgets.popups.showMessageDialog
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.layout_money_request_activity.*

/**
 * @author Junaid Hassan on 08, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class MoneyRequestActivity : BaseActivity<LayoutMoneyRequestActivityBinding, MoneyRequestFactory, MoneyRequestViewModel>() {
    override fun getLayoutId(): Int {
        return R.layout.layout_money_request_activity
    }

    override fun getViewModelClass(): Class<MoneyRequestViewModel> {
        return MoneyRequestViewModel::class.java
    }

    override fun getFactory(): MoneyRequestFactory {
        return MoneyRequestInjection.provideMoneyRequestFactory()
    }

    override fun init() {
        subscribe()
        initUI()
        initUIEvents()
    }

    private fun initUIEvents() {
        azerActionBarBackIcon.setOnClickListener {
            onBackPressed()
        }

        transferButton.setOnClickListener {
            val msisdn = userNameET.text.toString().replace(ConstantsUtility.UserConstants.USERNAME_PREFIX, "")
            if (isValidMsisdn(msisdn)) {
                mViewModel.requestMoneyRequestAPI(msisdn, amountSpinner.selectedItem.toString())
            } else {
                showMessageDialog(applicationContext, this@MoneyRequestActivity,
                        resources.getString(R.string.popup_error_title),
                        resources.getString(R.string.invalid_number))
            }
        }
    }

    private fun subscribe() {
        val moneyRequestResponseObserver = object : Observer<RequestMoneyResponse> {
            override fun onChanged(response: RequestMoneyResponse?) {
                showMessageDialog(this@MoneyRequestActivity, this@MoneyRequestActivity,
                        resources.getString(R.string.popup_ok),
                        response?.resultDesc!!)
                UserDataManager.setIsGetHomePageCalledWithSuccess(false)
                removeHomePageResponseFromLocalPreferences()
            }
        }

        mViewModel.moneyRequestResponse.observe(this@MoneyRequestActivity, moneyRequestResponseObserver)
    }

    private fun setupInputFields() {
        mobileNumberTIL.defaultHintTextColor = resources.getColorStateList(R.color.brown_grey)
        userNameET.typeface = getALSBoldFont()

        if (UserDataManager.getPredefineData()?.topup?.moneyTransfer?.selectAmount?.prepaid != null) {
            val amountList = UserDataManager.getPredefineData()?.topup?.moneyTransfer?.selectAmount?.prepaid
            amountSpinner.adapter = MoneyTransferSpinnerAdapter(this, amountList)
        }
    }//setupInputFields ends

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.money_request_title)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

    private fun initUI() {
        setupHeaderActionBar()
        setupInputFields()
        addPrefixToUserNameField(userNameET)
    }

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, MoneyRequestActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends
    }//object ends

}//class ends