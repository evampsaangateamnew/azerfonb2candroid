package com.azarphone.ui.activities.nartv

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class NarTvFactory(val narTvRepositry: NarTvRepositry) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(NarTvViewModel::class.java)) {
            return NarTvViewModel(narTvRepositry) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}