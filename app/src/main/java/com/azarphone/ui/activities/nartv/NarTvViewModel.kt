package com.azarphone.ui.activities.nartv

import androidx.lifecycle.MutableLiveData
import com.azarphone.api.pojo.request.NarTvMigrationRequest
import com.azarphone.api.pojo.request.NarTvSubscriptionsRequest
import com.azarphone.api.pojo.response.nartvmigrationresponse.NarTvMigrationResponse
import com.azarphone.api.pojo.response.nartvsubscriptionsresponse.NarTvSubscriptionResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.datamanagers.UserDataManager
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

class NarTvViewModel(val narTvRepositry: NarTvRepositry) : BaseViewModel() {

    private val fromClass = "NarTvViewModel"
    private lateinit var disposable: Disposable
    val narTvSubscriptionsResponse = MutableLiveData<NarTvSubscriptionResponse>()
    val narTvMigrationResponse = MutableLiveData<NarTvMigrationResponse>()
    init {

    }

    fun requestNarTvSubscriptions() {
        view()?.showLoading()
        val offeringID = UserDataManager.getCustomerData()?.offeringId
        val narTvSubscriptionsRequest = NarTvSubscriptionsRequest(offeringID)

        var mObserver = narTvRepositry.requestNarTvSubscriptions(narTvSubscriptionsRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    narTvSubscriptionsResponse.postValue(result)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }

    fun requestNarTvMigration() {
        view()?.showLoading()
       /* val brandID = UserDataManager.getCustomerData()?.brandId
        val brandID = UserDataManager.getCustomerData()?.accountId
        val brandID = UserDataManager.getCustomerData()?.brandId*/
        val narTvMigrationRequest = NarTvMigrationRequest()

        var mObserver = narTvRepositry.requestNarTvMigration(narTvMigrationRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    narTvMigrationResponse.postValue(result)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }
}