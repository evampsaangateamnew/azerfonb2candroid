package com.azarphone.ui.activities.datawithoutpack

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.MotionEvent
import android.view.View
import androidx.lifecycle.Observer
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.response.coreservices.CoreServicesResponse
import com.azarphone.api.pojo.response.coreservicesprocessnumber.CoreServicesProcessResponse
import com.azarphone.api.pojo.response.payg.WithOutPackResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.ActivityDataWithOutPackBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.widgets.popups.showMessageDialog
import kotlinx.android.synthetic.main.activity_data_with_out_pack.*
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*

class DataWithOutPackActivity :
    BaseActivity<ActivityDataWithOutPackBinding, DataWithOutPackFactory, DataWithOutPackViewModel>() {

    private val fromClass = "DataWithOutPackActivity"
    private var offerId = ""

    override fun getLayoutId(): Int {
        return R.layout.activity_data_with_out_pack
    }

    override fun getViewModelClass(): Class<DataWithOutPackViewModel> {
        return DataWithOutPackViewModel::class.java
    }

    override fun getFactory(): DataWithOutPackFactory {
        return DataWithOutPackInjectionUtils.provideEmailFactory()
    }

    override fun init() {
        dataWithOutHeader.isSelected = true
        setupHeaderActionBar()
        subscribe()
        initUIEvents()
        mViewModel.getPayGCurrentStatus()
    }

    private fun initUIEvents() {
        azerActionBarBackIcon.setOnClickListener {
            onBackPressed()
        }
        dataWithOutSwitch.setOnTouchListener { v: View?, event: MotionEvent -> event.actionMasked == MotionEvent.ACTION_MOVE }
        dataWithOutSwitch.setOnClickListener {
            if (offerId.isEmpty()) {
                offerId = ConstantsUtility.CoreServicesConstants.USAGE_WITHOUT_PACK_OFFERING_ID
            }
            if (dataWithOutSwitch!!.isChecked) {
                /**process to active the service with action type=1*/
                mViewModel.requestProcessCoreService("1", offerId, "", "", "")
            } else {
                /**process to active the service with action type=3*/
                mViewModel.requestProcessCoreService("3", offerId, "", "", "")
            }
        }//coreServiceDescriptionSwitch!!.setOnClickListener ends
    }//initUIEvents ends

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.dataWithOutPack)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

    private fun subscribe() {
        val payGStatusResponseLiveData =
            Observer<WithOutPackResponse> { t ->
                Log.e("payG_", t.data.toString())
                t.data?.let {
                    offerId = it.offeringId
                    dataWithOutSwitch.isChecked =
                        it.status.lowercase() == ConstantsUtility.DataWithOutPack.ACTIVE
                } ?: run {
                    dataWithOutSwitch.isSelected = false
                }
            }
        mViewModel.dataWithOutPackResponseLiveData.observe(
            this@DataWithOutPackActivity,
            payGStatusResponseLiveData
        )

        val payGStatusErrorResponseLiveData =
            Observer<AzerAPIErrorHelperResponse> { response ->
                if (!this@DataWithOutPackActivity.isFinishing) {
                    if (!isNetworkAvailable(this@DataWithOutPackActivity)) {
                        showMessageDialog(
                            applicationContext, this@DataWithOutPackActivity,
                            resources.getString(R.string.popup_note_title),
                            resources.getString(R.string.message_no_internet)
                        )
                    } else {
                        /**check the error code
                         * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                         * else show a general api failure popup*/
                        /**check the error code
                         * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                         * else show a general api failure popup*/
                        /**check the error code
                         * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                         * else show a general api failure popup*/
                        /**check the error code
                         * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                         * else show a general api failure popup*/
                        when {
                            response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION -> showMessageDialog(
                                applicationContext, this@DataWithOutPackActivity,
                                this@DataWithOutPackActivity.resources.getString(R.string.popup_error_title),
                                response.errorDetail.toString()
                            )
                            response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT -> //force logout the user here
                                logOut(
                                    applicationContext,
                                    this@DataWithOutPackActivity,
                                    false,
                                    fromClass,
                                    "specialOffersErrorResponseData2321sf"
                                )
                            response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE -> //handle the ui in this case
                                //hide or show the no data found layout in this case
                                showMessageDialog(
                                    applicationContext, this@DataWithOutPackActivity,
                                    this@DataWithOutPackActivity.resources.getString(R.string.popup_error_title),
                                    this@DataWithOutPackActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE -> showMessageDialog(
                                applicationContext, this@DataWithOutPackActivity,
                                this@DataWithOutPackActivity.resources.getString(R.string.popup_error_title),
                                this@DataWithOutPackActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                            else -> showMessageDialog(
                                applicationContext, this@DataWithOutPackActivity,
                                this@DataWithOutPackActivity.resources.getString(R.string.popup_error_title),
                                this@DataWithOutPackActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                        }
                    }
                }// if (activity != null && !this@CoreServicesActivity.isFinishing)  ends
            }
        mViewModel.dataWithOutPackErrorResponseLiveData.observe(
            this@DataWithOutPackActivity,
            payGStatusErrorResponseLiveData
        )


        val coreServiceProcessResponseLiveData =
            Observer<CoreServicesProcessResponse> { t ->
                if (t != null) {
                    if (t.resultDesc != null && hasValue(t.resultDesc)) {
                        showMessageDialog(
                            applicationContext, this@DataWithOutPackActivity,
                            resources.getString(R.string.popup_note_title),
                            t.resultDesc
                        )
                    }
                    UserDataManager.setSuccessOfferingIdForCoreService("")
                } else {
                    dataWithOutSwitch.isChecked = !dataWithOutSwitch.isChecked
                    showMessageDialog(
                        applicationContext, this@DataWithOutPackActivity,
                        resources.getString(R.string.popup_error_title),
                        resources.getString(R.string.server_stopped_responding_please_try_again)
                    )
                }
            }

        mViewModel.coreServiceProcessResponseLiveData.observe(
            this@DataWithOutPackActivity,
            coreServiceProcessResponseLiveData
        )


        val coreServiceProcessErrorResponseLiveData =
            Observer<AzerAPIErrorHelperResponse> { response ->
                if (!this@DataWithOutPackActivity.isFinishing) {
                    //check internet
                    dataWithOutSwitch.isChecked = !dataWithOutSwitch.isChecked
                    if (!isNetworkAvailable(this@DataWithOutPackActivity)) {
                        showMessageDialog(
                            applicationContext, this@DataWithOutPackActivity,
                            resources.getString(R.string.popup_note_title),
                            resources.getString(R.string.message_no_internet)
                        )
                    } else {
                        /**check the error code
                         * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                         * else show a general api failure popup*/
                        /**check the error code
                         * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                         * else show a general api failure popup*/
                        /**check the error code
                         * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                         * else show a general api failure popup*/
                        /**check the error code
                         * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                         * else show a general api failure popup*/
                        if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                            showMessageDialog(
                                applicationContext, this@DataWithOutPackActivity,
                                this@DataWithOutPackActivity.resources.getString(R.string.popup_error_title),
                                response.errorDetail.toString()
                            )
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                            //force logout the user here
                            logOut(
                                applicationContext,
                                this@DataWithOutPackActivity,
                                false,
                                fromClass,
                                "specialOffersErrorResponseData2321sf"
                            )
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                            //handle the ui in this case
                            //hide or show the no data found layout in this case
                            showMessageDialog(
                                applicationContext, this@DataWithOutPackActivity,
                                this@DataWithOutPackActivity.resources.getString(R.string.popup_error_title),
                                this@DataWithOutPackActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                            showMessageDialog(
                                applicationContext, this@DataWithOutPackActivity,
                                this@DataWithOutPackActivity.resources.getString(R.string.popup_error_title),
                                this@DataWithOutPackActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                        } else {
                            showMessageDialog(
                                applicationContext, this@DataWithOutPackActivity,
                                this@DataWithOutPackActivity.resources.getString(R.string.popup_error_title),
                                this@DataWithOutPackActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                            )
                        }
                    }
                }// if (activity != null && !this@CoreServicesActivity.isFinishing)  ends
            }

        mViewModel.coreServiceProcessErrorResponseLiveData.observe(
            this@DataWithOutPackActivity,
            coreServiceProcessErrorResponseLiveData
        )
    }//subscribe ends

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, DataWithOutPackActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends
    }//object ends

}