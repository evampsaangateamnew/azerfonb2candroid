package com.azarphone.ui.activities.vasservices

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.response.coreservicesprocessnumber.CoreServicesProcessResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.LayoutNumberForwardActivityBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.validators.isValidMsisdn
import com.azarphone.widgets.popups.showMessageDialog
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.layout_number_forward_activity.*

/**
 * @author Junaid Hassan on 03, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class CoreServicesNumberForwardActivity : BaseActivity<LayoutNumberForwardActivityBinding, CoreServicesFactory, CoreServicesViewModel>() {

    private val fromClass = "CoreServicesNumberForwardActivity"
    private var offeringId = ""
    private var callDivertNumber = ""

    override fun getLayoutId(): Int {
        return R.layout.layout_number_forward_activity
    }//getLayoutId ends

    override fun getViewModelClass(): Class<CoreServicesViewModel> {
        return CoreServicesViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): CoreServicesFactory {
        return CoreServicesInjection.provideCoreServicesFactory()
    }//getFactory ends

    override fun init() {
        subscribe()
        getIntentExtras()
        setupHeaderActionBar()
        addPrefixToUserNameField(msisdnET)
        setupInputFields()

        initUI()
        initUIEvents()
    }//init ends

    private fun getIntentExtras() {
        if (intent != null && intent.extras != null) {
            if (intent.extras!!.containsKey(ConstantsUtility.CoreServicesConstants.KEY_NUMBER_FORWARD_DATA)) {
                intent.getStringExtra(ConstantsUtility.CoreServicesConstants.KEY_NUMBER_FORWARD_DATA)
                    .let {
                        offeringId = it.toString()
                    }
            }
        }
    }//getIntentExtras ends

    private fun initUIEvents() {
        azerActionBarBackIcon.setOnClickListener {
            onBackPressed()
        }//azerActionBarBackIcon.setOnClickListener ends

        /*    msisdnET.setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    proceedOnSave(offeringId)
                }
                false
            }//msisdnET.setOnEditorActionListener ends*/

        msisdnET.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if (event!!.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    proceedOnSave(offeringId)
                }//if ends
                return false
            }//onKey ends
        })

        msisdnET.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    proceedOnSave(offeringId)
                }//if ends
                return false
            }//onEditorAction ends
        })

        saveButton.setOnClickListener {
            proceedOnSave(offeringId)
        }//saveButton.setOnClickListener ends
    }//initUIEvents ends

    private fun initUI() {

    }//initUI ends

    private fun subscribe() {
        val coreServicesForwardNumberResponseLiveData = object : Observer<CoreServicesProcessResponse> {
            override fun onChanged(t: CoreServicesProcessResponse?) {
                if (t != null) {
                    if (t.resultDesc != null && hasValue(t.resultDesc)) {
                        showSuccessConfirmMessageDialog(applicationContext, this@CoreServicesNumberForwardActivity,
                                resources.getString(R.string.popup_note_title),
                                t.resultDesc)
                    }
                } else {
                    showMessageDialog(applicationContext, this@CoreServicesNumberForwardActivity,
                            resources.getString(R.string.popup_error_title),
                            resources.getString(R.string.server_stopped_responding_please_try_again))
                }
            }
        }

        mViewModel.coreServicesForwardNumberResponseLiveData.observe(this@CoreServicesNumberForwardActivity, coreServicesForwardNumberResponseLiveData)

        val coreServicesForwardNumberErrorResponseLiveData = object : Observer<AzerAPIErrorHelperResponse> {
            override fun onChanged(response: AzerAPIErrorHelperResponse?) {
                if (!this@CoreServicesNumberForwardActivity.isFinishing) {
                    //check internet
                    if (!isNetworkAvailable(this@CoreServicesNumberForwardActivity)) {
                        showMessageDialog(applicationContext, this@CoreServicesNumberForwardActivity,
                                resources.getString(R.string.popup_note_title),
                                resources.getString(R.string.message_no_internet))
                    } else {
                        /**check the error code
                         * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                         * else show a general api failure popup*/
                        if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                            showMessageDialog(applicationContext, this@CoreServicesNumberForwardActivity,
                                    this@CoreServicesNumberForwardActivity.resources.getString(R.string.popup_error_title),
                                    response.errorDetail.toString())
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                            //force logout the user here
                            logOut(applicationContext, this@CoreServicesNumberForwardActivity, false, fromClass, "specialOffersErrorResponseData2321sf")
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                            //handle the ui in this case
                            //hide or show the no data found layout in this case
                            showMessageDialog(applicationContext, this@CoreServicesNumberForwardActivity,
                                    this@CoreServicesNumberForwardActivity.resources.getString(R.string.popup_error_title),
                                    this@CoreServicesNumberForwardActivity.resources.getString(R.string.server_stopped_responding_please_try_again))
                        } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                            showMessageDialog(applicationContext, this@CoreServicesNumberForwardActivity,
                                    this@CoreServicesNumberForwardActivity.resources.getString(R.string.popup_error_title),
                                    this@CoreServicesNumberForwardActivity.resources.getString(R.string.server_stopped_responding_please_try_again))
                        } else {
                            showMessageDialog(applicationContext, this@CoreServicesNumberForwardActivity,
                                    this@CoreServicesNumberForwardActivity.resources.getString(R.string.popup_error_title),
                                    this@CoreServicesNumberForwardActivity.resources.getString(R.string.server_stopped_responding_please_try_again))
                        }
                    }
                }// if (activity != null && !this@CoreServicesNumberForwardActivity.isFinishing)  ends
            }
        }

        mViewModel.coreServicesForwardNumberErrorResponseLiveData.observe(this@CoreServicesNumberForwardActivity, coreServicesForwardNumberErrorResponseLiveData)
    }//subscribe ends

    @SuppressLint("InflateParams")
    private fun showSuccessConfirmMessageDialog(context: Context, activity: Activity, title: String, message: String) {
        try {
            if (activity.isFinishing) return

            val dialogBuilder = AlertDialog.Builder(activity)
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val dialogView = inflater.inflate(R.layout.azer_popup_layout_new, null)
            dialogBuilder.setView(dialogView)

            val alertDialog = dialogBuilder.create()
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.setCancelable(false)
            alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            if (alertDialog != null) {
                if (alertDialog.isShowing) {
                    return
                }
            }
            alertDialog.show()

            val popupTitle = dialogView.findViewById(R.id.popupTitle) as TextView
            popupTitle.typeface = getALSNormalFont()
            val okButton = dialogView.findViewById(R.id.okButton) as Button

            okButton.text = getOkButtonLabel()
            popupTitle.text = message
            okButton.setOnClickListener {
                UserDataManager.setCallDivertNumber(callDivertNumber)
                finish()
                alertDialog.dismiss()
            }
        } catch (e: Exception) {
            logE("AzerPopup", e.toString(), "AzerFonPopups", "showMessageDialog")
        }//catch ends
    }//show message dialog ends

    private fun getSameNumberCantBeForwardNumberErrorLabel(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "Forwarded number cannot be the same as number you used for log in."
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.RU -> "Переадресованный номер не может совпадать с номером, который был использован для входа."
            else -> "Yönləndirilmiş nömrə ilə sistemə daxil olduğun nömrə eyni ola bilməz."
        }
    }

    private fun proceedOnSave(offeringId: String) {
        var msisdnToForwardNumberOn = msisdnET.text.toString()
        msisdnToForwardNumberOn = msisdnToForwardNumberOn.replace(ConstantsUtility.UserConstants.USERNAME_PREFIX, "")

        if (isValidMsisdn(msisdnToForwardNumberOn)) {

            if (UserDataManager.getCustomerData() != null &&
                    hasValue(UserDataManager.getCustomerData()!!.msisdn) &&
                    msisdnToForwardNumberOn == UserDataManager.getCustomerData()!!.msisdn) {
                showMessageDialog(applicationContext, this@CoreServicesNumberForwardActivity, "",
                        getSameNumberCantBeForwardNumberErrorLabel())
                return
            }

            var groupType = ""
            /* if (hasValue(UserDataManager.getCustomerData()!!.groupType)) {
                 groupType = UserDataManager.getCustomerData()!!.groupType!!
             }*/

            var actionType = "1"

            var accountType = ""
            /* if (hasValue(UserDataManager.getCustomerData()!!.accountType)) {
                 accountType = UserDataManager.getCustomerData()!!.accountType!!
             }*/

            callDivertNumber = msisdnToForwardNumberOn
            //call api to set the forward number on
            mViewModel.requestSetForwardNumberForCoreService(actionType, offeringId, groupType, msisdnToForwardNumberOn, accountType)
        } else {
            showMessageDialog(applicationContext, this@CoreServicesNumberForwardActivity,
                    resources.getString(R.string.popup_error_title),
                    resources.getString(R.string.error_msg_invalid_number))
        }
    }//proceedOnSave ends

    private fun setupInputFields() {
        msisdnTL.defaultHintTextColor = resources.getColorStateList(R.color.brown_grey)
        msisdnET.typeface = getALSBoldFont()
    }//setupInputFields ends

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.core_services_forward_number_title)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

}//class ends