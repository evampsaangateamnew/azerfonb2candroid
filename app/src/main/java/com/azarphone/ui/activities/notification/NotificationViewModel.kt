package com.azarphone.ui.activities.notification

import androidx.lifecycle.MutableLiveData
import com.azarphone.api.pojo.response.notification.NotificationsListItem
import com.azarphone.bases.BaseViewModel
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

class NotificationViewModel(val mNotificationRepositry: NotificationRepositry) : BaseViewModel()
{
    val mNotificationList = MutableLiveData<List<NotificationsListItem>>()
    private lateinit var disposable: Disposable
    init {

    }

    fun getNotifications() {
        view()?.showLoading()
        var mObserver = mNotificationRepositry.getNotifications()
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    result.data.notificationsList?.let {
                                        mNotificationList.postValue(it)
                                    }
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)

    }
}