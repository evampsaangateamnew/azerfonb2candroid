package com.azarphone.ui.activities.loan

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.GetLoanRequest
import com.azarphone.api.pojo.request.PaymentHistoryRequest
import com.azarphone.api.pojo.response.inappsurvey.InAppSurvey
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.loan.GetLoanResponse
import com.azarphone.api.pojo.response.paymenthistory.PaymentHistoryResponse
import com.azarphone.api.pojo.response.requesthistory.RequestHistoryResponse
import io.reactivex.Observable

/**
 * @author Junaid Hassan on 05, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class LoanRepositry {
    init {

    }

    fun requestGetLoan(getLoanRequest: GetLoanRequest): Observable<GetLoanResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestGetLoan(getLoanRequest)
    }//requestGetLoan ends

    fun requestPaymentHistory(paymentHistoryRequest: PaymentHistoryRequest): Observable<PaymentHistoryResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestPaymentHistory(paymentHistoryRequest)
    }//requestPaymentHistory ends

    fun requestHistory(paymentHistoryRequest: PaymentHistoryRequest): Observable<RequestHistoryResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestHistory(paymentHistoryRequest)
    }//requestPaymentHistory ends
    fun saveInAppSurvey(uploadSurvey: UploadSurvey): Observable<InAppSurvey> {
        return ApiClient.newApiClientInstance.getServerAPI().saveInAppSurvey(uploadSurvey)
    }
}