package com.azarphone.ui.activities.tutorials

import com.azarphone.bases.BaseViewModel

/**
 * @author Junaid Hassan on 20, June, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class TutorialsViewModel(val mTutorialsRepositry: TutorialsRepositry) : BaseViewModel() {

    init {

    }//init ends

    fun showLoader() {
        view()?.showLoading()
    }

    fun hideLoader() {
        view()?.hideLoading()
    }
}//class ends