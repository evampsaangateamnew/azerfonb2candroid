package com.azarphone.ui.activities.mysubscriptions

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.azarphone.R
import com.azarphone.analytics.Analytics
import com.azarphone.analytics.EventValues
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.request.MySubscriptionRequest
import com.azarphone.api.pojo.request.SupplementaryOfferSubscribeRequest
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.mysubscriptions.helper.SubscriptionsMain
import com.azarphone.api.pojo.response.mysubscriptions.response.MySubscriptionResponse
import com.azarphone.api.pojo.response.mysubscriptions.successresponse.MySubscriptionsSuccessAfterOfferSubscribeResponse
import com.azarphone.bases.BaseViewModel
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.UpdateListOnSuccessfullySurvey
import com.azarphone.ui.dialogs.InAppThankYouDialog
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.logE
import com.azarphone.validators.hasValue
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

/**
 * @author Junaid Hassan on 22, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class MySubscriptionsViewModel(val mMySubscriptionsRepositry: MySubscriptionsRepositry) : BaseViewModel() {

    private val fromClass = "MySubscriptionsViewModel"
    private lateinit var disposable: Disposable
    val mySubscriptionsBackgroundResponseLiveData = MutableLiveData<MySubscriptionResponse>()
    val mySubscriptionsResponseLiveData = MutableLiveData<MySubscriptionResponse>()
    val mySubscriptionsErrorResponseLiveData = MutableLiveData<AzerAPIErrorHelperResponse>()
    val subscribeToMySubscriptionProcessedResponseLiveData = MutableLiveData<MySubscriptionsSuccessAfterOfferSubscribeResponse>()
    val subscribeToMySubscriptionProcessedErrorResponseLiveData = MutableLiveData<AzerAPIErrorHelperResponse>()
    val inAppSurveyResponseErrorResponseData = MutableLiveData<AzerAPIErrorHelperResponse>()

    fun showLoader(){
        view()?.showLoading()
    }

    fun hideLoader(){
        view()?.hideLoading()
    }

    init {

    }//init ends

    fun uploadInAppSurvey(context: Context, uploadSurvey: UploadSurvey, onTransactionComplete: String, updateListOnSuccessfullySurvey: UpdateListOnSuccessfullySurvey?) {
        view()?.showLoading()
        var observable = mMySubscriptionsRepositry.saveInAppSurvey(uploadSurvey)
        observable = observable.onErrorReturn { throwable -> null }
        disposable = observable
            .compose(applyIOSchedulers())
            .subscribe(
                { appSurvey ->
                    view()?.hideLoading()
                    when {
                        appSurvey?.data != null -> {
                            UserDataManager.setInAppSurvey(appSurvey)
                            val thankYouDialog =
                                InAppThankYouDialog(context, onTransactionComplete)
                            thankYouDialog.show()
                        }
                        appSurvey?.resultDesc != null -> {
                            inAppSurveyResponseErrorResponseData.postValue(
                                AzerAPIErrorHelperResponse(
                                    ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                    appSurvey.resultDesc
                                )
                            )
                        }
                        else -> {
                            inAppSurveyResponseErrorResponseData.postValue(
                                AzerAPIErrorHelperResponse(
                                    ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                                    context.getString(R.string.server_stopped_responding_please_try_again)
                                )
                            )
                        }
                    }
                    updateListOnSuccessfullySurvey?.updateListOnSuccessfullySurvey()
                },
                { error ->
                    inAppSurveyResponseErrorResponseData.postValue(
                        AzerAPIErrorHelperResponse(
                            ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE,
                            error.message
                        )
                    )
                }
            )
        compositeDisposables?.add(disposable)
    }


    fun requestMySubscriptions() {
        var offeringId = ""
        var brandName = ""

        if (UserDataManager.getCustomerData() != null) {
            if (hasValue(UserDataManager.getCustomerData()!!.offeringId)) {
                offeringId = UserDataManager.getCustomerData()!!.offeringId!!
            }

            if (hasValue(UserDataManager.getCustomerData()!!.brandName)) {
                brandName = UserDataManager.getCustomerData()!!.brandName!!
            }
        }

        logE("subXYR", "MySubcriptionsCalling:::offeringName:::".plus(offeringId).plus(" brandName:::").plus(brandName), fromClass, "requestMySubscriptions")
        view()?.showLoading()
        val mySubscriptionRequest = MySubscriptionRequest(offeringId)

        var mObserver = mMySubscriptionsRepositry.requestMySubscriptions(mySubscriptionRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            logE("subXYR", "response:::".plus(result.toString()), fromClass, "requestMySubscriptions")
                            parseResponse(result)
                        },
                        { error ->
                            view()?.hideLoading()
                            mySubscriptionsErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestMySubscriptionsInBackground ends

    fun requestMySubscriptionsInBackground() {
        var offeringId = ""
        var brandName = ""

        if (UserDataManager.getCustomerData() != null) {
            if (hasValue(UserDataManager.getCustomerData()!!.offeringId)) {
                offeringId = UserDataManager.getCustomerData()!!.offeringId!!
            }

            if (hasValue(UserDataManager.getCustomerData()!!.brandName)) {
                brandName = UserDataManager.getCustomerData()!!.brandName!!
            }
        }

        logE("subXYR", "MySubcriptionsBGCalling:::offeringName:::".plus(offeringId).plus(" brandName:::").plus(brandName), fromClass, "requestMySubscriptionsInBackground")
        val mySubscriptionRequest = MySubscriptionRequest(offeringId)

        var mObserver = mMySubscriptionsRepositry.requestMySubscriptions(mySubscriptionRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            logE("subXYR", "responseBG:::".plus(result.toString()), fromClass, "requestMySubscriptionsInBackground")
                            parseMySubscriptionResponseInResponse(result)
                        },
                        { error ->
                            view()?.hideLoading()
                        }
                )

        compositeDisposables?.add(disposable)
    }//requestMySubscriptionsInBackground ends

    fun subscribeToSupplementaryOffer(offeringName: String, offeringId: String, actionType: String) {
        view()?.showLoading()
        val supplementaryOfferSubscribeRequest = SupplementaryOfferSubscribeRequest(offeringName, offeringId, actionType)

        var mObserver = mMySubscriptionsRepositry.subscribeToSupplementaryOffer(supplementaryOfferSubscribeRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            parseSubscribeToMySubscriptionProcessedResponseLiveData(result)
                            if(actionType.equals("3")) {
                                Analytics.logAppEvent(EventValues.MySubscriptionEvents.my_subscription, EventValues.MySubscriptionEvents.deactivation, EventValues.GenericValues.Success)
                            }
                            if(actionType.equals("1")) {
                                Analytics.logAppEvent(EventValues.MySubscriptionEvents.my_subscription, EventValues.MySubscriptionEvents.renewal, EventValues.GenericValues.Success)
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            subscribeToMySubscriptionProcessedErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                            if(actionType.equals("3")) {
                                Analytics.logAppEvent(EventValues.MySubscriptionEvents.my_subscription, EventValues.MySubscriptionEvents.deactivation, EventValues.GenericValues.Failure)
                            }
                            if(actionType.equals("1")) {
                                Analytics.logAppEvent(EventValues.MySubscriptionEvents.my_subscription, EventValues.MySubscriptionEvents.renewal, EventValues.GenericValues.Failure)
                            }
                        }
                )

        compositeDisposables?.add(disposable)
    }//subscribeToSupplementaryOffer ends

    private fun parseSubscribeToMySubscriptionProcessedResponseLiveData(response: MySubscriptionsSuccessAfterOfferSubscribeResponse) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        subscribeToMySubscriptionProcessedResponseLiveData.postValue(response)
                    } else {
                        /**the response has no data object
                         * in this case we have to handle the ui only*/
                        subscribeToMySubscriptionProcessedErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                    }
                } else {
                    /**the response has no data object
                     * in this case we have to handle the ui only*/
                    subscribeToMySubscriptionProcessedErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                }
            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                /**force log out user case
                 * when the server sends the result code as 7*/
                subscribeToMySubscriptionProcessedErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT, ""))
            } else {
                /**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*/
                if (hasValue(response.resultDesc.toString())) {
                    subscribeToMySubscriptionProcessedErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
                } else {
                    subscribeToMySubscriptionProcessedErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
                }
            }
        } else {
            /**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*/
            if (hasValue(response.resultDesc.toString())) {
                subscribeToMySubscriptionProcessedErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
            } else {
                subscribeToMySubscriptionProcessedErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
            }
        }

        //hide the dialog
        view()?.hideLoading()
    }

    private fun parseResponse(response: MySubscriptionResponse) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        //process the response here
                        mySubscriptionsResponseLiveData.postValue(response)
                    } else {
                        /**the response has no data object
                         * in this case we have to handle the ui only*/
                        mySubscriptionsErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                    }
                } else {
                    /**the response has no data object
                     * in this case we have to handle the ui only*/
                    mySubscriptionsErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE, ""))
                }
            } else if (response.resultCode == ConstantsUtility.ServerResponseCodes.FORCE_LOG_OUT) {
                /**force log out user case
                 * when the server sends the result code as 7*/
                mySubscriptionsErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT, ""))
            } else {
                /**check if has description
                 * in case of result code other than 00
                 * if has description show it in the popup
                 * else show a simple api failure popup*/
                if (hasValue(response.resultDesc.toString())) {
                    mySubscriptionsErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
                } else {
                    mySubscriptionsErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
                }
            }
        } else {
            /**check if has description
             * in case of no result code
             * if has description show it in the popup
             * else show a simple api failure popup*/
            if (hasValue(response.resultDesc.toString())) {
                mySubscriptionsErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION, response.resultDesc))
            } else {
                mySubscriptionsErrorResponseLiveData.postValue(AzerAPIErrorHelperResponse(ConstantsUtility.APIResponseErrorCodes.API_FAILURE, ""))
            }
        }

        //hide the dialog
//        view()?.hideLoading()
    }//parseResponse ends

    private fun parseMySubscriptionResponseInResponse(response: MySubscriptionResponse) {
        //check the response code
        if (hasValue(response.resultCode.toString())) {
            if (response.resultCode == ConstantsUtility.ServerResponseCodes.SUCCESS) {
                /**handle the success case
                 * when the result code from the server is 00*/
                if (response.data != null) {
                    if (hasValue(response.data.toString())) {
                        logE("mySubBG", "bgResponse:::".plus(response.toString()), fromClass, "parseMySubscriptionResponseInResponse")
                        //process the response here
                        mySubscriptionsBackgroundResponseLiveData.postValue(response)
                    }
                }
            }
        }
        //hide the dialog
        view()?.hideLoading()
    }//parseResponse ends
}//class ends