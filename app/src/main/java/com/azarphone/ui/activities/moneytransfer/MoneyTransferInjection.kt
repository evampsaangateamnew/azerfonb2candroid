package com.azarphone.ui.activities.moneytransfer

/**
 * @author Junaid Hassan on 08, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
object MoneyTransferInjection{
    fun provideMoneyTransferRepository(): MoneyTransferRepositry {
        return MoneyTransferRepositry()
    }

    fun provideMoneyTransferFactory(): MoneyTransferFactory {
        return MoneyTransferFactory(provideMoneyTransferRepository())
    }
}