package com.azarphone.ui.activities.moneytransfer

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.AzerAPIErrorHelperResponse
import com.azarphone.api.pojo.response.inappsurvey.InAppSurvey
import com.azarphone.api.pojo.response.inappsurvey.Surveys
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.moneytransfer.MoneyTransferResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.LayoutMoneyTransferActivityBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.OnClickSurveySubmit
import com.azarphone.eventhandler.UpdateListOnSuccessfullySurvey
import com.azarphone.ui.adapters.spinners.MoneyTransferSpinnerAdapter
import com.azarphone.ui.dialogs.InAppFeedbackDialog
import com.azarphone.util.*
import com.azarphone.validators.isValidMsisdn
import com.azarphone.widgets.popups.showMessageDialog
import com.es.tec.LocalSharedPrefStorage
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.layout_money_transfer_activity.*

/**
 * @author Junaid Hassan on 08, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class MoneyTransferActivity :
    BaseActivity<LayoutMoneyTransferActivityBinding, MoneyTransferFactory, MoneyTransferViewModel>() {

    private val fromClass = "MoneyTransferActivity"

    override fun getLayoutId(): Int {
        return R.layout.layout_money_transfer_activity
    }//getLayoutId ends

    override fun getViewModelClass(): Class<MoneyTransferViewModel> {
        return MoneyTransferViewModel::class.java
    }//getViewModelClass ends

    override fun getFactory(): MoneyTransferFactory {
        return MoneyTransferInjection.provideMoneyTransferFactory()
    }//getFactory ends

    override fun init() {
        subscribe()
        initUI()
        initUIEvents()
        listenersForInAppReview()
    }//init ends

    private fun listenersForInAppReview() {
        val inAppSurveyResponseErrorResponseData =
            Observer<AzerAPIErrorHelperResponse> { response ->
                if (!isNetworkAvailable(this@MoneyTransferActivity)) {
                    showMessageDialog(
                        this@MoneyTransferActivity, this@MoneyTransferActivity,
                        resources.getString(R.string.popup_note_title),
                        resources.getString(R.string.message_no_internet)
                    )
                } else {
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    /**check the error code
                     * if FAILED_WITH_DESCRIPTION then show the error detail from the server
                     * else show a general api failure popup*/
                    if (response!!.errorCode == ConstantsUtility.APIResponseErrorCodes.FAILED_WITH_DESCRIPTION) {
                        showMessageDialog(
                            this@MoneyTransferActivity, this@MoneyTransferActivity,
                            this@MoneyTransferActivity.resources.getString(R.string.popup_error_title),
                            response.errorDetail.toString()
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.FORCE_LOG_OUT) {
                        //force logout the user here
                        logOut(
                            this@MoneyTransferActivity,
                            this@MoneyTransferActivity,
                            false,
                            fromClass ?: "TopUp",
                            "initSupplementaryOffersCardAdapterEventsa34324"
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.NO_RESPONSE) {
                        //handle the ui in this case
                        //hide or show the no data found layout in this case
                        showMessageDialog(
                            this@MoneyTransferActivity, this@MoneyTransferActivity,
                            this@MoneyTransferActivity.resources.getString(R.string.popup_error_title),
                            this@MoneyTransferActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    } else if (response.errorCode == ConstantsUtility.APIResponseErrorCodes.API_FAILURE) {
                        showMessageDialog(
                            this@MoneyTransferActivity, this@MoneyTransferActivity,
                            this@MoneyTransferActivity.resources.getString(R.string.popup_error_title),
                            this@MoneyTransferActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    } else {
                        showMessageDialog(
                            this@MoneyTransferActivity, this@MoneyTransferActivity,
                            this@MoneyTransferActivity.resources.getString(R.string.popup_error_title),
                            this@MoneyTransferActivity.resources.getString(R.string.server_stopped_responding_please_try_again)
                        )
                    }
                }
            }
        mViewModel.inAppSurveyResponseErrorResponseData.observe(
            this@MoneyTransferActivity,
            inAppSurveyResponseErrorResponseData
        )
    }

    private fun subscribe() {
        val moneyTransferLiveDataResponse = object : Observer<MoneyTransferResponse> {
            override fun onChanged(t: MoneyTransferResponse?) {
//                showMessageDialog(this@MoneyTransferActivity, this@MoneyTransferActivity,
//                        resources.getString(R.string.popup_ok),
//                        t?.resultDesc!!)

                inAppFeedback(
                    this@MoneyTransferActivity,
                    resources.getString(R.string.popup_note_title),
                    t?.resultDesc!!
                )
                UserDataManager.setIsGetHomePageCalledWithSuccess(false)
                removeHomePageResponseFromLocalPreferences()
            }
        }

        mViewModel.moneyTransferLiveDataResponse.observe(
            this@MoneyTransferActivity,
            moneyTransferLiveDataResponse
        )
    }

    private fun inAppFeedback(context: Context, title: String, message: String) {

        var inAppFeedbackDialog: InAppFeedbackDialog? = null
        var currentVisit: Int =
            LocalSharedPrefStorage(context).getInt(LocalSharedPrefStorage.PREF_TRANSFER_MONEY)
        currentVisit += 1
        val inAppSurvey: InAppSurvey? = UserDataManager.getInAppSurvey()
        if ((inAppSurvey?.data) != null) {
            val surveys: Surveys? =
                inAppSurvey.data.findSurvey(ConstantsUtility.InAppSurveyConstants.TRANSFER_MONEY_PAGE)
            if (surveys != null) {
                if (surveys.surveyLimit > surveys.surveyCount) {
                    if (surveys.visitLimit <= currentVisit && surveys.visitLimit > -1) {
                        inAppFeedbackDialog =
                            InAppFeedbackDialog(context, object : OnClickSurveySubmit {
                                override fun onClickSubmit(
                                    uploadSurvey: UploadSurvey,
                                    onTransactionComplete: String,
                                    updateListOnSuccessfullySurvey: UpdateListOnSuccessfullySurvey?
                                ) {
                                    mViewModel.uploadInAppSurvey(
                                        context,
                                        uploadSurvey,
                                        onTransactionComplete
                                    )
                                    if (inAppFeedbackDialog?.isShowing == true) {
                                        inAppFeedbackDialog?.dismiss()
                                    }
                                }

                            })
                        if (surveys.questions != null) {
                            currentVisit = 0
                            inAppFeedbackDialog.showTitleWithMessageDialog(
                                surveys,
                                title,
                                message
                            )
                        } else {
                            showMessageDialog(
                                context, this@MoneyTransferActivity,
                                title,
                                message
                            )
                        }
                    } else {
                        showMessageDialog(
                            context, this@MoneyTransferActivity,
                            title,
                            message
                        )
                    }
                } else {
                    showMessageDialog(
                        context, this@MoneyTransferActivity,
                        title,
                        message
                    )
                }

            } else {
                showMessageDialog(
                    applicationContext, this@MoneyTransferActivity,
                    title,
                    message
                )
            }
        } else {
            showMessageDialog(
                applicationContext, this@MoneyTransferActivity,
                resources.getString(R.string.popup_note_title),
                message
            )
        }
        LocalSharedPrefStorage(this@MoneyTransferActivity).addInt(
            LocalSharedPrefStorage.PREF_TRANSFER_MONEY,
            currentVisit
        )
    }

    private fun initUIEvents() {
        azerActionBarBackIcon.setOnClickListener {
            onBackPressed()
        }

        transferButton.setOnClickListener {
            val msisdn = userNameET.text.toString()
                .replace(ConstantsUtility.UserConstants.USERNAME_PREFIX, "")

            if (isValidMsisdn(msisdn)) {
                showMoneyTransferConfirmationPopup(
                    getMoneyTransferLocalizedConfirmation(msisdn),
                    msisdn
                )
            } else {
                showMessageDialog(
                    applicationContext, this@MoneyTransferActivity,
                    resources.getString(R.string.invalid_number),
                    resources.getString(R.string.invalid_number)
                )
            }

        }//transferButton.setOnClickListener ends
    }//initUIEvents ends

    private fun getMoneyTransferLocalizedConfirmation(msisdn: String): String {

        return when {
            ProjectApplication.mLocaleManager.getLanguage().equals(
                ConstantsUtility.LanguageKeywords.EN,
                ignoreCase = true
            ) -> "Are you sure you want to top up ".plus(msisdn).plus("?")
            ProjectApplication.mLocaleManager.getLanguage().equals(
                ConstantsUtility.LanguageKeywords.AZ,
                ignoreCase = true
            ) -> msisdn.plus(" balansını artırmaq istədiyindən əminsən?")
            else -> "Ты уверен, что хочешь пополнить баланс ".plus(msisdn).plus("?")
        }
    }

    private fun showMoneyTransferConfirmationPopup(title: String, msisdn: String) {
        if (this@MoneyTransferActivity.isFinishing) return

        val dialogBuilder = AlertDialog.Builder(this@MoneyTransferActivity)
        val inflater =
            this@MoneyTransferActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val dialogView = inflater.inflate(R.layout.azer_vas_services_confirmation_popup, null)
        dialogBuilder.setView(dialogView)

        val alertDialog = dialogBuilder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.show()

        val popupTitle = dialogView.findViewById(R.id.popupTitle) as TextView
        popupTitle.typeface = getALSNormalFont()
        popupTitle.text = title
        val popupOkayButton = dialogView.findViewById(R.id.yesButton) as Button
        popupOkayButton.typeface = getALSBoldFont()
        val popupCancelButton = dialogView.findViewById(R.id.noButton) as Button
        popupCancelButton.typeface = getALSBoldFont()

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN) {
            //english
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "No"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ) {
            //azri
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "Xeyr"
        }

        if (ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.RU) {
            //russian
            popupOkayButton.text = getString(R.string.popup_text_update_ok)
            popupCancelButton.text = "Нет"
        }

        popupOkayButton.setOnClickListener {
            alertDialog.dismiss()
            //if we get null by amountSpinner then it send empty string to server to get the error message
            mViewModel.requestMoneyTransfer(msisdn, amountSpinner?.selectedItem?.toString() ?: "")
        }

        popupCancelButton.setOnClickListener {
            alertDialog.dismiss()
        }
    }

    private fun initUI() {
        addPrefixToUserNameField(userNameET)
        setupHeaderActionBar()
        setupInputFields()
    }

    private fun setupInputFields() {
        mobileNumberTIL.defaultHintTextColor = resources.getColorStateList(R.color.brown_grey)
        userNameET.typeface = getALSBoldFont()

        if (UserDataManager.getPredefineData()?.topup?.moneyTransfer?.selectAmount?.prepaid != null) {
            val amountList =
                UserDataManager.getPredefineData()?.topup?.moneyTransfer?.selectAmount?.prepaid
            amountSpinner.adapter = MoneyTransferSpinnerAdapter(this, amountList)
        }
    }//setupInputFields ends

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.money_transfer_title)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, MoneyTransferActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends
    }//object ends

}//class ends