package com.azarphone.ui.activities.nartv

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.azarphone.R
import com.azarphone.api.pojo.response.nartvmigrationresponse.NarTvMigrationResponse
import com.azarphone.api.pojo.response.nartvsubscriptionsresponse.NarTvSubscriptionResponse
import com.azarphone.api.pojo.response.nartvsubscriptionsresponse.PlansItem
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.ActivityNartvBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.NarTvSubscriptionsEventHandler
import com.azarphone.ui.adapters.recyclerviews.NarTvRecyclerAdapter
import com.azarphone.widgets.popups.showMessageDialog
import kotlinx.android.synthetic.main.activity_nartv.*
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*


@Suppress("DEPRECATION")
class NarTvActivity : BaseActivity<ActivityNartvBinding, NarTvFactory, NarTvViewModel>(), NarTvSubscriptionsEventHandler {


    private val listener: NarTvSubscriptionsEventHandler = this@NarTvActivity
    var selectedItem: PlansItem? = null

    override fun getLayoutId(): Int {
        return R.layout.activity_nartv
    }

    override fun getViewModelClass(): Class<NarTvViewModel> {
        return NarTvViewModel::class.java
    }

    override fun getFactory(): NarTvFactory {
        return NarTvInjectionUtils.provideNarTvFactory()
    }


    override fun init() {

        initUI()
        initUIEvents()
        subscribe()
        subscribeForMigration()

        mViewModel.requestNarTvSubscriptions()
        /* viewDataBinding.narRecycler.apply {
             val list:List<PlansItem?>?=ArrayList()
             layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
             adapter = NarTvRecyclerAdapter(list as ArrayList<PlansItem>?)
         }*/
    }

    private fun initUIEvents() {
        azerActionBarBackIcon.setOnClickListener {
            onBackPressed()
        }

        subscribeButton.setOnClickListener {
            if (selectedItem != null) {
                // Toast.makeText(baseContext,"Selected",Toast.LENGTH_LONG).show()
                mViewModel.requestNarTvMigration()
            }
        }

        nartvButton.setOnClickListener {
            val url = UserDataManager.getPredefineData()?.narTv?.narTvRedirectionLinkAndroid
            if (!url.isNullOrEmpty()) {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                startActivity(browserIntent)
            }

        }


    }

    private fun initUI() {
        setupHeaderActionBar()
    }

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.GONE//show the title
        azerActionBarTitle.text = resources.getString(R.string.nartv_title)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

    private fun subscribe() {
        val narTvSubscriptionsResponseObserver = object : Observer<NarTvSubscriptionResponse> {
            override fun onChanged(response: NarTvSubscriptionResponse?) {


                if (response?.data?.subscriptions != null && !response?.data?.subscriptions.isNullOrEmpty()) {

                    subscribeButton.visibility = View.VISIBLE
                    nar_recycler.visibility = View.VISIBLE
                    nartvButton.visibility = View.GONE
                    nartv_description.visibility = View.GONE

                    viewDataBinding.narRecycler.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@NarTvActivity, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
                    val adapter = NarTvRecyclerAdapter(response?.data?.subscriptions as ArrayList<PlansItem>?, listener, this@NarTvActivity)
                    viewDataBinding.narRecycler.adapter = adapter
                } else {
                    subscribeButton.visibility = View.GONE
                    nar_recycler.visibility = View.GONE
                    nartvButton.visibility = View.VISIBLE
                    nartv_description.visibility = View.VISIBLE
                }

            }
        }

        mViewModel.narTvSubscriptionsResponse.observe(this@NarTvActivity, narTvSubscriptionsResponseObserver)
    }

    private fun subscribeForMigration() {
        val narTvMigrationResponseObserver = object : Observer<NarTvMigrationResponse> {
            override fun onChanged(response: NarTvMigrationResponse?) {

                showMessageDialog(this@NarTvActivity, this@NarTvActivity,
                        resources.getString(R.string.popup_ok),
                        response?.resultDesc!!)

            }
        }

        mViewModel.narTvMigrationResponse.observe(this@NarTvActivity, narTvMigrationResponseObserver)
    }

    override fun onSubscriptionSelected(item: PlansItem) {
        selectedItem = item
    }


    companion object {
        fun start(context: Context) {
            val intent = Intent(context, NarTvActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }
    }

}