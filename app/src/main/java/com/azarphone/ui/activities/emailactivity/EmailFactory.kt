package com.azarphone.ui.activities.emailactivity

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class EmailFactory(val mEmailRepositry: EmailRepositry) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(EmailViewModel::class.java)) {
            return EmailViewModel(mEmailRepositry) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}