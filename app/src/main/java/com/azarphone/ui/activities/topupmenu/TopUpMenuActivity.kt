package com.azarphone.ui.activities.topupmenu

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.commit
import com.azarphone.R
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.ActivityTopupMenuLayoutBinding
import com.azarphone.ui.fragment.topup.TopupFragment
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.logE
import com.azarphone.validators.hasValue
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*

/**
 * @author Muhammad Ahmed on 14, July, 2021
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * muhammad.ahmed@evampsaanga.com
 */
class TopUpMenuActivity :
    BaseActivity<ActivityTopupMenuLayoutBinding, TopUpMenuFactory, TopUpMenuViewModel>() {

    //Class Name variable
    private val fromClass = "TopUpMenuActivity"

    //Bundle variables
    private var matchedSubScreen = ""
    private var tabToRedirect = ""
    private var offeringIdToRedirect = ""


    override fun getLayoutId(): Int {
        return R.layout.activity_topup_menu_layout
    }

    override fun getViewModelClass(): Class<TopUpMenuViewModel> {
        return TopUpMenuViewModel::class.java
    }

    override fun getFactory(): TopUpMenuFactory {
        return TopUpMenuInjection.provideTopUpMenuFactory()
    }

    override fun init() {
        getIntentData()
        setupHeaderActionBar()
        initUIEvents()
        loadTopUpFragment()
    }

    private fun loadTopUpFragment() {
        val fragment = TopupFragment()
        fragment.arguments = Bundle().apply {
            if (hasValue(matchedSubScreen)) {
                putString(
                    ConstantsUtility.DynamicLinkConstants.SUB_SCREEN_NAME_DATA,
                    matchedSubScreen
                )
            }

            if (hasValue(tabToRedirect)) {
                putString(
                    ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA,
                    tabToRedirect
                )
            }

            if (hasValue(offeringIdToRedirect)) {
                putString(
                    ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA,
                    offeringIdToRedirect
                )
            }
        }
        supportFragmentManager.commit {
            setReorderingAllowed(true)
            add(R.id.topUpFragmentContainer, fragment)
        }
    }

    private fun initUIEvents() {
        azerActionBarBackIcon.setOnClickListener {
            onBackPressed()
        }//azerActionBarBackIcon.setOnClickListener  ends
    }

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageResource(R.drawable.bubblestripcut)//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.balance_screen_title)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, TopUpMenuActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends

        fun start(
            context: Context,
            matchedSubScreen: String,
            tabToRedirect: String,
            offeringIdToRedirect: String
        ) {
            val intent = Intent(context, TopUpMenuActivity::class.java).apply {

                putExtra(
                    ConstantsUtility.DynamicLinkConstants.STEP_ONE_SCREEN_NAME_DATA,
                    matchedSubScreen
                )
                putExtra(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA, tabToRedirect)
                putExtra(
                    ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA,
                    offeringIdToRedirect
                )

                addFlags(
                    Intent.FLAG_ACTIVITY_NEW_TASK
                )
            }
            context.startActivity(intent)
        }//start ends
    }//object ends

    private fun getIntentData() {
        //Getting values from argument bundle
        intent?.extras?.let {

            if (it.containsKey(ConstantsUtility.DynamicLinkConstants.STEP_ONE_SCREEN_NAME_DATA)
                && hasValue(it.getString(ConstantsUtility.DynamicLinkConstants.STEP_ONE_SCREEN_NAME_DATA))
            ) {
                val subScreen =
                    it.getString(ConstantsUtility.DynamicLinkConstants.STEP_ONE_SCREEN_NAME_DATA)
                setMatchedSubScreenValue(subScreen!!)
            }

            if (it.containsKey(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA)
                && hasValue(it.getString(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA))
            ) {
                tabToRedirect = it.getString(ConstantsUtility.DynamicLinkConstants.TAB_NAME_DATA)!!
            }

            if (it.containsKey(ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA)
                && hasValue(it.getString(ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA))
            ) {
                offeringIdToRedirect =
                    it.getString(ConstantsUtility.DynamicLinkConstants.OFFERING_ID_DATA)!!
            }
        }

        //Logging final received values from argument bundle
        logE(
            ConstantsUtility.DynamicLinkConstants.DYNAMIC_LINK_TAG,
            "matchedSubScreen = $matchedSubScreen tabToRedirect = $tabToRedirect" +
                    "offeringIdToRedirect = $offeringIdToRedirect",
            fromClass,
            "getIntentData"
        )
    }//getArgumentsData ends

    private fun setMatchedSubScreenValue(subScreen: String) {
        when {
            subScreen.equals(
                ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_TOPUP,
                ignoreCase = true
            ) -> {
                matchedSubScreen =
                    ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_TOPUP
            }
            subScreen.equals(
                ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_LOAN,
                ignoreCase = true
            ) -> {
                matchedSubScreen =
                    ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_LOAN
            }
            subScreen.equals(
                ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_MONEY_TRANSFER,
                ignoreCase = true
            ) -> {
                matchedSubScreen =
                    ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_MONEY_TRANSFER
            }
            subScreen.equals(
                ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_MONEY_REQUEST,
                ignoreCase = true
            ) -> {
                matchedSubScreen =
                    ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_MONEY_REQUEST
            }
            subScreen.equals(
                ConstantsUtility.TopupChildMenusIdentifiers.Fast_TOPUP,
                ignoreCase = true
            ) -> {
                matchedSubScreen =
                    ConstantsUtility.TopupChildMenusIdentifiers.Fast_TOPUP
            }
            subScreen.equals(
                ConstantsUtility.TopupChildMenusIdentifiers.AUTO_PAYMENT,
                ignoreCase = true
            ) -> {
                matchedSubScreen =
                    ConstantsUtility.TopupChildMenusIdentifiers.AUTO_PAYMENT
            }
        }
    }//setMatchedSubScreenValue ends
}