package com.azarphone.ui.activities.datawithoutpack

object DataWithOutPackInjectionUtils {

    fun provideEmailRepository(): DataWithOutPackRepository {
        return DataWithOutPackRepository()
    }

    fun provideEmailFactory(): DataWithOutPackFactory {
        return DataWithOutPackFactory(provideEmailRepository())
    }

}