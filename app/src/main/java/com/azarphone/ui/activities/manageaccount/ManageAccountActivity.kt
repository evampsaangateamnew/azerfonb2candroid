package com.azarphone.ui.activities.manageaccount

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.UserAccounts
import com.azarphone.api.pojo.response.customerdata.CustomerData
import com.azarphone.bases.BaseActivity
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.ManageAccountEventsHandler
import com.azarphone.ui.activities.loginactivity.LoginActivity
import com.azarphone.ui.activities.mainactivity.MainActivity
import com.azarphone.ui.adapters.recyclerviews.ManageAccountRecyclerAdapter
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.ConstantsUtility.ManageAccount.LOGIN_USECASE_ADDACCOUNT
import com.azarphone.util.ConstantsUtility.ManageAccount.LOGIN_USECASE_TYPE_KEY
import com.azarphone.util.getUpdatedCustomerDataLocaliy
import com.azarphone.util.getUserAccountsDataFromLocalPreferences
import com.azarphone.util.setCustomerDataToLocalPreferences
import com.azarphone.widgets.popups.showMessageDialog
import kotlinx.android.synthetic.main.activity_manage_accounts.*
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*

class ManageAccountActivity : BaseActivity<com.azarphone.databinding.ActivityManageAccountsBinding, ManageAccountFactory, ManageAccountViewModel>(), ManageAccountEventsHandler {


    private val fromClass = "ManageAccountActivity"
    private val listener: ManageAccountEventsHandler = this@ManageAccountActivity

    override fun getLayoutId(): Int {
        return R.layout.activity_manage_accounts
    }

    override fun getViewModelClass(): Class<ManageAccountViewModel> {
        return ManageAccountViewModel::class.java
    }

    override fun getFactory(): ManageAccountFactory {
        return ManageAccountUtils.provideManageAccountFactory()
    }

    override fun init() {
        setupHeaderActionBar()
        initUiEvents()
        addAccountBtn.isSelected = true
        var list: ArrayList<CustomerData>? = ArrayList()
        if (getUserAccountsDataFromLocalPreferences() != null && getUserAccountsDataFromLocalPreferences().usersList != null) {
            list = getUserAccountsDataFromLocalPreferences().usersList as ArrayList<CustomerData>
        }

        for (i in 0 until list?.size!!) {
            if (UserDataManager.getCustomerData()?.msisdn.equals(list.get(i).msisdn)) {
                val currentAccount = list.get(i)
                list.removeAt(i)
                list.add(0, currentAccount)

            }
        }

        viewDataBinding.manageAccountIncludeLayout.manageAccountRecycler.apply {
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
            adapter = ManageAccountRecyclerAdapter(list, listener)
        }
    }

    private fun initUiEvents() {
        azerActionBarBackIcon.setOnClickListener {
            onBackPressed()
        }

        addAccountBtn.setOnClickListener {
            var list: ArrayList<CustomerData>? = ArrayList()


            if (getUserAccountsDataFromLocalPreferences() != null && getUserAccountsDataFromLocalPreferences().usersList != null) {
                list = getUserAccountsDataFromLocalPreferences().usersList as ArrayList<CustomerData>
            }
            if (list?.size!! < 5) {
                val intent = Intent(this, LoginActivity::class.java)
                intent.putExtra(LOGIN_USECASE_TYPE_KEY, LOGIN_USECASE_ADDACCOUNT)
                startActivity(intent)
            } else {
                showMessageDialog(this, this,
                        resources.getString(R.string.popup_error_title),
                        resources.getString(R.string.maximum_number_added_msg))
            }
        }
    }

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.manage_account_screen_title)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, ManageAccountActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends
    }

    override fun onSwitchNumberClick(customer: CustomerData) {
        val isPasswordChanged = ProjectApplication.getInstance().getLocalPreferneces().getBool(
                ProjectApplication.getInstance().getLocalPreferneces().IS_PASSWORD_CHANGE_MULTI_USER_LOGGED_OUT,
                false)
        if (!UserDataManager.getCustomerData()?.msisdn.equals(customer.msisdn) || isPasswordChanged) {
            setCustomerDataToLocalPreferences(getUpdatedCustomerDataLocaliy(customer))
            UserDataManager.saveCustomerData(getUpdatedCustomerDataLocaliy(customer))
            //save the time of the login
//            setUserLoginTime(getCurrentDateTimeForRateUs())
            UserDataManager.setIsAppResumeCalledWithSuccess(false)
            //now go to the next activity
            MainActivity.start(applicationContext, ConstantsUtility.BundleKeys.BUNDLE_FROM_SWITCH_NUMBER)
        }
    }

    override fun onDeleteNumberClick(customerData: CustomerData) {
        var isMsisdnAlreadyExists = false


        if (UserDataManager.getCustomerData()?.msisdn == customerData.msisdn) {
            isMsisdnAlreadyExists = true
        }

        if (isMsisdnAlreadyExists) {
            showMessageDialog(this, this@ManageAccountActivity,
                    resources.getString(R.string.popup_error_title),
                    resources.getString(R.string.delete_number_error))
        } else {
            var list: ArrayList<CustomerData>? = ArrayList()


            if (getUserAccountsDataFromLocalPreferences() != null && getUserAccountsDataFromLocalPreferences().usersList != null) {
                list = getUserAccountsDataFromLocalPreferences().usersList as ArrayList<CustomerData>
            }


            list?.remove(customerData)

            ProjectApplication.getInstance().getLocalPreferneces().setResponse(
                    ProjectApplication.getInstance().getLocalPreferneces().KEY_USER_ACCOUNTS_DATA
                    ,
                    UserAccounts(list)
            )

        }


    }
}