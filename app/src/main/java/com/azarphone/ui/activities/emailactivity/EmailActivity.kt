package com.azarphone.ui.activities.emailactivity

import androidx.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.azarphone.api.pojo.response.updateemailreponse.UpdateEmailResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.EmailActivityBinding
import com.azarphone.widgets.popups.showMessageDialog
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.email_activity.*
import android.widget.Toast
import com.azarphone.R
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.util.hideKeyboard
import com.azarphone.validators.hasValue
import java.util.regex.Matcher
import java.util.regex.Pattern


class EmailActivity : BaseActivity<EmailActivityBinding, EmailFactory, EmailViewModel>() {
    override fun getLayoutId(): Int {
        return R.layout.email_activity
    }


    override fun getViewModelClass(): Class<EmailViewModel> {
        return EmailViewModel::class.java
    }

    override fun getFactory(): EmailFactory {
        return EmailInjectionUtils.provideEmailFactory()
    }

    override fun init() {
        setupHeaderActionBar()
        initUi()
        initUIEvents()
        subscribe()

    }

    private fun initUi() {
        if (UserDataManager.getCustomerData() != null) {

            val userData = UserDataManager.getCustomerData()
            if (userData?.email != null && hasValue(userData.email)) {
                if (emailValidator(userData?.email!!)) {
                    val number = userData?.email!!.split("@")[0].toString().toIntOrNull()
                    val isInteger = number != null
                    if (!isInteger) {
                        msisdnET.setText(userData?.email)
                    } else {
                        msisdnET.setText("")
                    }
                }
            }
        }
    }
    private fun subscribe() {
        val updateEmailResponseObserver = object : Observer<UpdateEmailResponse> {
            override fun onChanged(response: UpdateEmailResponse?) {
                if (!this@EmailActivity.isFinishing) {
                    showMessageDialog(applicationContext, this@EmailActivity,
                            "",
                            response?.data?.message!!)
                    UserDataManager.getCustomerData()?.email = msisdnET.text.toString()
                }
            }
        }

        mViewModel.updateEmailResponse.observe(this@EmailActivity, updateEmailResponseObserver)
    }

    fun emailValidator(email: String): Boolean {
        val pattern: Pattern
        val matcher: Matcher
        val EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
        pattern = Pattern.compile(EMAIL_PATTERN)
        matcher = pattern.matcher(email)
        return matcher.matches()
    }

    private fun initUIEvents() {
        azerActionBarBackIcon.setOnClickListener {
            onBackPressed()
        }
        saveButton.setOnClickListener{

            processOnUpdateEmail()
        }

        msisdnET.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if (event!!.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    processOnUpdateEmail()
                }//if ends
                return false
            }//onKey ends
        })

        msisdnET.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    processOnUpdateEmail()
                }//if ends
                return false
            }//onEditorAction ends
        })
    }

    private fun processOnUpdateEmail() {
        hideKeyboard(this@EmailActivity)
        if(emailValidator(msisdnET.text.toString())) {
            val number = msisdnET.text!!.split("@")[0].toString().toIntOrNull()
            val isInteger = number != null
            if(!isInteger) {
                mViewModel.requestUpdateEmail(msisdnET.text.toString())
            }
            else{
                Toast.makeText(this@EmailActivity,resources.getString(R.string.add_email_incorrect),Toast.LENGTH_LONG).show()
            }
        }
        else{
            Toast.makeText(this@EmailActivity,resources.getString(R.string.add_email_incorrect),Toast.LENGTH_LONG).show()
        }
    }


    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.add_email_title)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, EmailActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends
    }

}