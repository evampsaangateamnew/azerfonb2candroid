package com.azarphone.ui.activities.newautopayment

object NewAutoPaymentInjection {


    private fun provideNewAutoPaymentRepository(): NewAutoPaymentRepository {
        return NewAutoPaymentRepository()
    }

    fun provideNewAutoPaymentFactory(): NewAutoPaymentFactory {
        return NewAutoPaymentFactory(provideNewAutoPaymentRepository())
    }
}