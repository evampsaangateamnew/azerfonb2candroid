package com.azarphone.ui.activities.moneyrequest

import androidx.lifecycle.MutableLiveData
import com.azarphone.api.pojo.request.RequestMoneyRequest
import com.azarphone.api.pojo.response.requestmoneyresponse.RequestMoneyResponse
import com.azarphone.bases.BaseViewModel
import com.es.tec.applyIOSchedulers
import io.reactivex.disposables.Disposable

/**
 * @author Junaid Hassan on 08, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class MoneyRequestViewModel(val mMoneyRequestRepositry: MoneyRequestRepositry) : BaseViewModel() {

    private lateinit var disposable: Disposable
    val moneyRequestResponse = MutableLiveData<RequestMoneyResponse>()

    init {

    }//init

    fun requestMoneyRequestAPI(friends_msisdn: String, amount: String) {
        view()?.showLoading()

        val moneyTransferRequest = RequestMoneyRequest(friends_msisdn, amount)

        var mObserver = mMoneyRequestRepositry.requestMoney(moneyTransferRequest)
        mObserver = mObserver.onErrorReturn({ throwable -> null })

        disposable = mObserver
                .compose(applyIOSchedulers())
                .subscribe(
                        { result ->
                            view()?.hideLoading()
                            view()?.isErrorErrorDialoge(result)?.let {
                                if (it != true) {
                                    moneyRequestResponse.postValue(result)
                                }
                            }
                        },
                        { error ->
                            view()?.hideLoading()
                            view()?.loadError(error)
                        }
                )

        compositeDisposables?.add(disposable)
    }
}