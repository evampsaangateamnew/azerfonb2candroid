package com.azarphone.ui.activities.newautopayment

import android.content.Context
import android.content.Intent
import android.os.Handler
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import com.azarphone.R
import com.azarphone.api.pojo.request.AddPaymentSchedulerRequest
import com.azarphone.api.pojo.response.autopaymentresponse.daily.AddSchedulePaymentResponse
import com.azarphone.api.pojo.response.topup.savedcards.SavedCardsResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.ActivityNewAutoPaymentBinding
import com.azarphone.eventhandler.PaymentSchedulerClickListener
import com.azarphone.ui.activities.autopayment.AutoPaymentActivity
import com.azarphone.ui.adapters.pagers.AutoPaymentPagerAdapter
import com.azarphone.ui.fragment.newautopayment.daily.DailyAutoPaymentFragment
import com.azarphone.ui.fragment.newautopayment.monthly.MonthlyAutoPaymentFragment
import com.azarphone.ui.fragment.newautopayment.weekly.WeeklyAutoPaymentFragment
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.widgets.popups.showMessageDialogWithOkCallBack
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_new_auto_payment.*
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.layout_activity_loan.*
import kotlinx.android.synthetic.main.layout_mysubscriptions_activity.tabLayout

class NewAutoPaymentActivity : BaseActivity<ActivityNewAutoPaymentBinding, NewAutoPaymentFactory, NewAutoPaymentViewModel>() {


    private val redirectionIndex = 0
    private lateinit var autoPaymentPagerAdapter: AutoPaymentPagerAdapter

    override fun getLayoutId(): Int {
        return R.layout.activity_new_auto_payment
    }

    override fun getViewModelClass(): Class<NewAutoPaymentViewModel> {
        return NewAutoPaymentViewModel::class.java
    }

    override fun getFactory(): NewAutoPaymentFactory {
        return NewAutoPaymentInjection.provideNewAutoPaymentFactory()
    }

    override fun init() {
        setupHeaderActionBar()
        RootValues.setSavedCardsResponse(null)
        initUIEvents()
        setUpTabs()
        subscribe()
        initializePaymentSchedulerListener()
    }

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.lbl_auto_payment) // lbl_schedule_payment
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

    private fun initializePaymentSchedulerListener() {
        val paymentSchedulerClickListener: PaymentSchedulerClickListener
        paymentSchedulerClickListener = object : PaymentSchedulerClickListener {
            override fun paymentSchedulerClickListener(amount: String, recurrence: String, billingCycleDaily: String, savedCardId: String, dateForAPI: String) {
                val addPaymentSchedulerRequest = AddPaymentSchedulerRequest(amount = amount, billingCycle = billingCycleDaily,
                        startDate = dateForAPI, recurrenceNumber = recurrence, savedCardId = savedCardId)
                mViewModel.requestPaymentScheduler(addPaymentSchedulerRequest)
            }
        }

//        RootValues.setPymentSchedlerClickListener(paymentSchedulerClickListener)
    }

    override fun onResume() {
        super.onResume()
        mViewModel.requestTopUpCards()
    }

    private fun subscribe() {

        //Get Saved Cards
        val savedCardsLiveData = object : Observer<SavedCardsResponse> {
            override fun onChanged(t: SavedCardsResponse?) {
//                if (t != null && t.data != null && t?.data?.cardDetails != null && t?.data?.cardDetails!!.isNotEmpty()) {
                    noDataFound.visibility = View.GONE
                    durationViewpager.visibility = View.VISIBLE
                    setUpViewPager(t)
                    RootValues.setSavedCardsResponse(t)
                /* } else {
                     durationViewpager.visibility = View.GONE
                     noDataFound.visibility = View.VISIBLE
                 }*/
            }
        }
        mViewModel.savedCardsResponseLiveData.observe(this, savedCardsLiveData)


        //Schedule Paymnt
        val addSchedulePaymentLiveData = object : Observer<AddSchedulePaymentResponse> {
            override fun onChanged(t: AddSchedulePaymentResponse?) {
                processAddSchedulePaymentLiveData(t)
            }
        }

        mViewModel.addPaymentSchedulerResponseLiveData.observe(this, addSchedulePaymentLiveData)
    }

    private fun processAddSchedulePaymentLiveData(response: AddSchedulePaymentResponse?) {

        var responseDescription = ""
        if (!this@NewAutoPaymentActivity.isFinishing && response != null &&
                hasValue(response.resultDesc)) {
            responseDescription = response.resultDesc
        }

        if (!this@NewAutoPaymentActivity.isFinishing && response != null) {
            showMessageDialogWithOkCallBack(this@NewAutoPaymentActivity, this@NewAutoPaymentActivity,
                    getString(R.string.app_name), responseDescription, object : OnClickListener {
                override fun onClick() {
                    //TODO: Confirm this thing
                    init()
                }

            })
        }
    }

    private fun setUpTabs() {
        tabLayout.addTab(tabLayout.newTab().setText( getString(R.string.add_auto_payment_daily)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.add_auto_payment_weekly)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.add_auto_payment_monthly)));

        for (i in 0 until tabLayout.tabCount) {
            val tab = (tabLayout.getChildAt(0) as ViewGroup).getChildAt(i)
            val p = tab.layoutParams as ViewGroup.MarginLayoutParams
            val margin = (15 * resources.displayMetrics.density)
            p.setMargins(0, 0, margin.toInt(), 0)
            tab.requestLayout()

            //set the custom view
            val tabView = tabLayout.getTabAt(i)
            if (tabView != null) {
                val tabTextView = TextView(this@NewAutoPaymentActivity)
                tabView.customView = tabTextView
                //set the text
                tabTextView.text = tabView.text
                tabTextView.textAlignment = TextView.TEXT_ALIGNMENT_CENTER
                //set the currently selected tab font
                if (redirectionIndex == i) {
                    setSelectedTabFont(applicationContext, tabTextView)
                } else {
                    setUnSelectedTabFont(applicationContext, tabTextView)
                }
            }
        }

        gotoTabByIndex(redirectionIndex)
    }

    private fun setUpViewPager(t: SavedCardsResponse?) {
        val pagerFragmentsList = ArrayList<androidx.fragment.app.Fragment>()
        pagerFragmentsList.add(DailyAutoPaymentFragment.getInstance(t))
        pagerFragmentsList.add(WeeklyAutoPaymentFragment.getInstance(t))
        pagerFragmentsList.add(MonthlyAutoPaymentFragment.getInstance(t))

        autoPaymentPagerAdapter = AutoPaymentPagerAdapter(supportFragmentManager, pagerFragmentsList)
        durationViewpager.adapter = autoPaymentPagerAdapter
        durationViewpager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(p0: TabLayout.Tab?) {

            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
            }

            override fun onTabReselected(p0: TabLayout.Tab?) {
            }

        })
    }

    private fun initUIEvents() {

        azerActionBarBackIcon.setOnClickListener {
            onBackPressed()
        }//azerActionBarBackIcon.setOnClickListener  ends

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                if (tab != null && tab.customView != null) {
                    val tabTextView = tab.customView as TextView
                    setUnSelectedTabFont(applicationContext, tabTextView)
                }
            }//onTabUnselected ends

            override fun onTabSelected(tab: TabLayout.Tab?) {
                durationViewpager.setCurrentItem(tab!!.position, false)
                if (tab.customView != null) {
                    val tabTextView = tab.customView as TextView
                    setSelectedTabFont(applicationContext, tabTextView)
                }
            }//onTabSelected ends
        })//tabLayout.addOnTabSelectedListener ends
    }//initUIEvents ends


    private fun gotoTabByIndex(index: Int) {
        if (tabLayout != null && tabLayout.tabCount > 0) {
            Handler().postDelayed({
                tabLayout.getTabAt(index)!!.select()
                tabLayout.isSmoothScrollingEnabled = true
                tabLayout.setScrollPosition(index, 0f, true)
            }, 50L)
        }
    }//gotoTabByIndex ends


    companion object {
        private val TAG = AutoPaymentActivity::class.java.simpleName
        fun start(context: Context) {
            val intent = Intent(context, NewAutoPaymentActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends
    }//object

}