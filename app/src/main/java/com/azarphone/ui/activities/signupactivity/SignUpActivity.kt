package com.azarphone.ui.activities.signupactivity

import androidx.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.navigation.Navigation.findNavController
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.OTPTimerHelperModel
import com.azarphone.api.pojo.helperpojo.UserAccounts
import com.azarphone.api.pojo.response.customerdata.CustomerData
import com.azarphone.api.pojo.response.otpgetresponse.OTPResponse
import com.azarphone.api.pojo.response.otpverifyresponse.OTPVerifyResponse
import com.azarphone.api.pojo.response.savecustomerresponse.SaveCustomerResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.SignUpActivityBinding
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.SignupActivityToolTipEvents
import com.azarphone.ui.activities.mainactivity.MainActivity
import com.azarphone.util.*
import com.azarphone.util.ConstantsUtility.ManageAccount.LOGIN_USECASE_ADDACCOUNT
import com.azarphone.util.ConstantsUtility.ManageAccount.LOGIN_USECASE_LOGIN
import com.azarphone.validators.hasValue
import com.azarphone.validators.isValidMsisdn
import com.azarphone.validators.isValidOTP
import com.azarphone.validators.isValidPasswordLength
import com.azarphone.widgets.popups.showMessageDialog
import kotlinx.android.synthetic.main.sign_up_activity.*

class SignUpActivity : BaseActivity<SignUpActivityBinding, SignUpFactory, SignUpViewModel>() {

    private val fromClass = "SignUpActivity"
    private var userNumber = ""
    private var signupOTPPin = ""
    private var password = ""
    private var confirmPassword = ""
    private var isAllowback = true
    private var signupActivityToolTipEvents: SignupActivityToolTipEvents? = null
    private var useCase_Type = LOGIN_USECASE_LOGIN

    override fun getLayoutId(): Int {
        return R.layout.sign_up_activity
    }

    override fun getViewModelClass(): Class<SignUpViewModel> {
        return SignUpViewModel::class.java
    }

    override fun getFactory(): SignUpFactory {
        return SignUpInjectionUtils.provideSignUpFactory()
    }

    override fun init() {
        subscribeSignupStepperDataObserver()
        subscribeToSignupMsisdnLiveDataObserver()
        subscribeToSignupOTPLiveData()
        subscribeToSignupOTPVerifyAPILiveData()
        subscribeToSignupPasswordLiveData()
        subscribeToSignupConfirmPasswordLiveData()
        subscribeToSaveCustomerResponseLiveData()
        subscribeToSignupTermsCondtionsCheckLiveData()
        subscribeToRequestOTPObservers()
        subscribeToSignupOTP25SecondsTimerLiveData()
        subscribeToAPILoader()
        registerListener()

        /*signupButton.isEnabled = false
        signupButton.isClickable = false*/

        if (intent.extras != null) {
            getDataFromIntent(intent.extras!!)

        }

        otpbackButton.setOnClickListener {
            if (RootValues.getSignUpEvents() != null) {
                RootValues.getSignUpEvents()!!.onGoBack()
            }

            onBackPressed()
        }//otpbackButton.setOnClickListener ends

        signupButton.setOnClickListener {
            processOnSignup()
        }//signupButton.setOnClickListener  ends
    }//init ends

    fun processOnSignup() {
        hideKeyboard(this@SignUpActivity)
        if (signupButton.tag.toString()
                        .equals(ConstantsUtility.SignupScreenSteps.ENTER_NUMBER_STEP, ignoreCase = true)) {
            //validate the msisdn
            if (isValidMsisdn(userNumber)) {

                if (useCase_Type.equals(LOGIN_USECASE_ADDACCOUNT)) {
                    var list: ArrayList<CustomerData>? = ArrayList()
                    if (getUserAccountsDataFromLocalPreferences() != null && getUserAccountsDataFromLocalPreferences().usersList != null) {
                        list = getUserAccountsDataFromLocalPreferences().usersList as ArrayList<CustomerData>
                    }
                    var msisdn = userNumber.toString()
                    //replace the prefix
                    msisdn = msisdn.replace(ConstantsUtility.UserConstants.USERNAME_PREFIX, "")

                    var isMsisdnAlreadyExists = false
                    if (list != null) {
                        for (number in list.iterator()) {
                            if (number.msisdn == msisdn) {
                                isMsisdnAlreadyExists = true
                                break
                            }
                        }
                    }

                    if (isMsisdnAlreadyExists) {
                        showMessageDialog(this, this@SignUpActivity,
                                resources.getString(R.string.popup_error_title),
                                resources.getString(R.string.number_already_exists))
                    } else {
                        mViewModel.requestOTP(userNumber)
                    }

                } else {
                    //request otp
                    mViewModel.requestOTP(userNumber)
                }
            } else {
                RootValues.getSignupActivityToolTipEvents().onInvalidMsisdnNumberTooltip(resources.getString(R.string.error_msg_invalid_number))
            }//validate msisdn ends
        } else if (signupButton.tag.toString()
                        .equals(ConstantsUtility.SignupScreenSteps.ENTER_PIN_STEP, ignoreCase = true)) {
            if (isValidOTP(signupOTPPin)) {
                /**call the verify otp
                 * and on success redirect to the set password fragment*/
                val otp = signupOTPPin
                mViewModel.requestVerifyOTP(otp, userNumber)
            } else {
                RootValues.getSignupActivityToolTipEvents().onInvalidOTPPINTooltip(resources.getString(R.string.invalid_otp_label))
            }
        } else if (signupButton.tag.toString()
                        .equals(ConstantsUtility.SignupScreenSteps.SET_PASSWORD_STEP, ignoreCase = true)) {
            //call save customer api here
            if (!hasValue(password)) {
                //password field is empty
                RootValues.getSignupActivityToolTipEvents().onInvalidPasswordTooltip(resources.getString(R.string.passwords_criteria_error))
            } else if (password.length < 6) {
                RootValues.getSignupActivityToolTipEvents().onInvalidPasswordTooltip(resources.getString(R.string.passwords_criteria_error))
            } else if (!hasValue(confirmPassword)) {
                RootValues.getSignupActivityToolTipEvents().onInvalidConfirmPasswordTooltip(resources.getString(R.string.passwords_criteria_error))
            } else if (!isValidPasswordLength(password)) {
                RootValues.getSignupActivityToolTipEvents().onInvalidPasswordTooltip(resources.getString(R.string.passwords_criteria_error))
            } else if (!isValidPasswordLength(confirmPassword)) {
                RootValues.getSignupActivityToolTipEvents().onInvalidConfirmPasswordTooltip(resources.getString(R.string.passwords_criteria_error))
            } else {
                //check if password and confirm passwords are matched
                if (password.equals(confirmPassword)) {
                    //passwords matched
                    mViewModel.requestSaveCustomer(userNumber, password, confirmPassword, signupOTPPin, "1")
                } else {
                    //show error for passwords not matched
                    RootValues.getSignupActivityToolTipEvents().onInvalidConfirmPasswordTooltip(resources.getString(R.string.passwords_not_matched))
                }
            }
        } else {
            logE("signupsteps", "else->do nothing", fromClass, "forgotPasswordButton.setOnClickListener")
        }
    }

    private fun getDataFromIntent(extras: Bundle) {
        if (extras.containsKey(ConstantsUtility.ManageAccount.LOGIN_USECASE_TYPE_KEY)) {
            useCase_Type = extras.getString(ConstantsUtility.ManageAccount.LOGIN_USECASE_TYPE_KEY)!!
        }
    }

    private fun registerListener() {
        signupActivityToolTipEvents = object : SignupActivityToolTipEvents {
            override fun onInvalidPasswordTooltip(errorDetail: String) {
                //do nothing here
            }

            override fun onInvalidConfirmPasswordTooltip(errorDetail: String) {
                //do nothing here
            }

            override fun onInvalidOTPPINTooltip(errorDetail: String) {
                //do nothing here
            }

            override fun onInvalidMsisdnNumberTooltip(errorDetail: String) {
                //do nothing here
            }
        }

        RootValues.setSignupActivityToolTipEvents(signupActivityToolTipEvents!!)

        val isTermsAndCondtionsAccepted = object : Observer<Boolean> {
            override fun onChanged(isAccepted: Boolean?) {
                if(isAccepted!!){
                    signupButton.isEnabled = true
                    signupButton.isClickable = true
                }else{
                    signupButton.isEnabled = false
                    signupButton.isClickable = false
                }
            }
        }

        mViewModel.isTermsAndCondtionsAccepted.observe(this@SignUpActivity, isTermsAndCondtionsAccepted)
    }

    /**all mutable live data observers
     * should be here*/

    private fun subscribeToSignupOTP25SecondsTimerLiveData() {
        val signupOTP25SecondsTimerLiveData = object : Observer<OTPTimerHelperModel> {
            override fun onChanged(model: OTPTimerHelperModel?) {
                if (model!!.showHideFlag!!) {
                    if (model.flag!!) {
                        //set isAllowBack to true
                        isAllowback = true
                        otpbackButton.visibility = View.VISIBLE
                        otpTimerText.visibility = View.GONE
                    } else {
                        //set isAllowBack to false
                        isAllowback = false
                        otpbackButton.visibility = View.GONE
                        otpTimerText.visibility = View.VISIBLE
                        otpTimerText.text = model.timerText
                    }
                } else {
                    otpbackButton.visibility = View.GONE
                    otpTimerText.visibility = View.GONE
                }
            }//onChanged ends
        }//val signupOTP25SecondsTimerLiveData ends

        mViewModel.signupOTP25SecondsTimerLiveData.observe(this@SignUpActivity, signupOTP25SecondsTimerLiveData)
    }//subscribeToSignupOTP25SecondsTimerLiveData ends

    private fun subscribeToRequestOTPObservers() {
        val signupOTPResponseLiveData = object : Observer<OTPResponse> {
            override fun onChanged(t: OTPResponse?) {
                if (t != null) {
                    if (t.data != null) {
                        if (hasValue(t.data.pin)) {
                            val mBundle = Bundle()
                            mBundle.putString(ConstantsUtility.BundleKeys.BUNDLE_SIGN_UP_NUMBER, userNumber)
                            //goto enter pin fragment
                            findNavController(this@SignUpActivity, R.id.my_nav_host_fragment)
                                    .navigate(R.id.action_signup_enter_number_to_enter_pin, mBundle)
                        } else {
                            showMessageDialog(applicationContext, this@SignUpActivity,
                                    resources.getString(R.string.popup_error_title),
                                    resources.getString(R.string.server_stopped_responding_please_try_again))
                        }
                    } else {
                        showMessageDialog(applicationContext, this@SignUpActivity,
                                resources.getString(R.string.popup_error_title),
                                resources.getString(R.string.server_stopped_responding_please_try_again))
                    }
                } else {
                    showMessageDialog(applicationContext, this@SignUpActivity,
                            resources.getString(R.string.popup_error_title),
                            resources.getString(R.string.server_stopped_responding_please_try_again))
                }
            }
        }

        mViewModel.signupOTPResponseLiveData.observe(this@SignUpActivity, signupOTPResponseLiveData)
    }//subscribeToRequestOTPObservers ends

    private fun subscribeToSignupTermsCondtionsCheckLiveData() {
        val signupTermsConditionsCheckLiveData = object : Observer<Boolean> {
            override fun onChanged(isChecked: Boolean?) {
                if (signupButton.tag.toString().equals(ConstantsUtility.SignupScreenSteps.SET_PASSWORD_STEP, ignoreCase = true)) {
                    if (isChecked!!) {
                        signupButton.isEnabled = true
                        signupButton.isClickable = true
                    } else {
                        signupButton.isEnabled = false
                        signupButton.isClickable = false
                    }
                }
            }
        }

        mViewModel.signupTermsConditionsCheckLiveData.observe(this@SignUpActivity, signupTermsConditionsCheckLiveData)
    }

    private fun subscribeToSaveCustomerResponseLiveData() {
        val saveCustomerResponseLiveData = object : Observer<SaveCustomerResponse> {
            override fun onChanged(customer: SaveCustomerResponse?) {
                //handle the response
                if (customer != null) {
                    if (customer.data != null) {
                        if (customer.data.customerData != null) {
                            /**show promo message here*/
                            if (customer.data.promoMessage != null && hasValue(customer.data.promoMessage)) {
                                UserDataManager.setPromoMessage(customer.data.promoMessage)
                            }

                            var list: ArrayList<CustomerData>? = ArrayList()
                            if (useCase_Type.equals(LOGIN_USECASE_ADDACCOUNT)) {


                                if (getUserAccountsDataFromLocalPreferences() != null && getUserAccountsDataFromLocalPreferences().usersList != null) {
                                    list = getUserAccountsDataFromLocalPreferences().usersList as ArrayList<CustomerData>
                                }

                            }
                            list?.add(0, customer.data.customerData!!)

                            /*  if (list != null) {
                                  for (i in 0 until list.size)
                                      logE("UserListMSISDN", list.get(i).msisdn!!, fromClass, "UserAccountAdded")
                              }*/

                            ProjectApplication.getInstance().getLocalPreferneces().setResponse(
                                    ProjectApplication.getInstance().getLocalPreferneces().KEY_USER_ACCOUNTS_DATA,
                                    UserAccounts(list)
                            )

                            //save the data in the session
                            UserDataManager.saveCustomerData(customer.data.customerData)
                            UserDataManager.savePredefineData(customer.data.predefinedData)
                            //save the response in shared preferences
                            setCustomerDataToLocalPreferences(customer.data.customerData!!)

                            //save the time of the login
                            logE("timeX", "time1:::".plus(getUserLoginTime()), fromClass, "currentDateTime")
                            setUserLoginTime(getCurrentDateTimeForRateUs())
                            logE("timeX", "time2:::".plus(getUserLoginTime()), fromClass, "currentDateTime")

                            //no go to the next activity
                            MainActivity.start(applicationContext, ConstantsUtility.BundleKeys.BUNDLE_DONT_CALL_APP_RESUME)
                        } else {
                            showMessageDialog(applicationContext, this@SignUpActivity,
                                    resources.getString(R.string.popup_error_title),
                                    resources.getString(R.string.server_stopped_responding_please_try_again))
                        }
                    } else {
                        showMessageDialog(applicationContext, this@SignUpActivity,
                                resources.getString(R.string.popup_error_title),
                                resources.getString(R.string.server_stopped_responding_please_try_again))
                    }
                } else {
                    showMessageDialog(applicationContext, this@SignUpActivity,
                            resources.getString(R.string.popup_error_title),
                            resources.getString(R.string.server_stopped_responding_please_try_again))
                }
            }
        }

        mViewModel.saveCustomerResponseLiveData.observe(this@SignUpActivity, saveCustomerResponseLiveData)

    }//subscribeToSaveCustomerResponseLiveData ends

    private fun subscribeToSignupPasswordLiveData() {
        val signupPasswordLiveData = object : Observer<String> {
            override fun onChanged(t: String?) {
                t?.let {
                    password = t
                }
            }//onChanged ends
        }//val signupOTPLiveData ends

        mViewModel.signupPasswordLiveData.observe(this@SignUpActivity, signupPasswordLiveData)
    }//subscribeToSignupPasswordLiveData ends

    private fun subscribeToSignupConfirmPasswordLiveData() {
        val signupConfirmPasswordLiveData = object : Observer<String> {
            override fun onChanged(t: String?) {
                t?.let {
                    confirmPassword = t
                }
            }//onChanged ends
        }//val signupOTPLiveData ends

        mViewModel.signupConfirmPasswordLiveData.observe(this@SignUpActivity, signupConfirmPasswordLiveData)
    }//subscribeToSignupConfirmPasswordLiveData ends

    private fun subscribeToSignupOTPVerifyAPILiveData() {
        val signupOTPVerifyResponseLiveData = object : Observer<OTPVerifyResponse> {
            override fun onChanged(t: OTPVerifyResponse?) {
                if (t != null) {
                    if (t.data != null) {
                        if (hasValue(t.data.content)) {
                            logE("tandc", "tandc:::".plus(t.data.content), fromClass, "subscribeToSignupOTPVerifyAPILiveData♥")
                            UserDataManager.setTermsAndConditionsDataString(t.data.content)
                        }
                    }
                }
                findNavController(this@SignUpActivity, R.id.my_nav_host_fragment).navigate(R.id.action_signup_enter_pin_to_update_password)
            }
        }

        mViewModel.signupOTPVerifyResponseLiveData.observe(this@SignUpActivity, signupOTPVerifyResponseLiveData)
    }//subscribeToSignupOTPVerifyAPILiveData ends

    private fun subscribeToAPILoader() {
        val isDataLoading = object : Observer<Boolean> {
            override fun onChanged(t: Boolean?) {
                t?.let {
                    if (t == true) {
                        showLoading()
                    } else {
                        hideLoading()
                    }
                }
            }
        }

        mViewModel.isDataLoading.observe(this@SignUpActivity, isDataLoading)
    }//subscribeToAPILoader ends

    private fun subscribeToSignupOTPLiveData() {
        val signupOTPLiveData = object : Observer<String> {
            override fun onChanged(t: String?) {
                t?.let {
                    signupOTPPin = t
                }
            }//onChanged ends
        }//val signupOTPLiveData ends

        mViewModel.signupOTPLiveData.observe(this@SignUpActivity, signupOTPLiveData)
    }//subscribeToSignupOTPLiveData ends

    private fun subscribeToSignupMsisdnLiveDataObserver() {
        val signupMsisdnLiveDataObserver = object : Observer<String> {
            override fun onChanged(t: String?) {
                t?.let {
                    userNumber = t
                    userNumber = userNumber.replace(ConstantsUtility.UserConstants.USERNAME_PREFIX, "")
                }
            }
        }

        mViewModel.signupMsisdnLiveDataObserver.observe(this@SignUpActivity, signupMsisdnLiveDataObserver)
    }//subscribeToSignupMsisdnLiveDataObserver ends

    private fun subscribeSignupStepperDataObserver() {
        val signupPasswordStepperDataObserver = object : Observer<String> {
            override fun onChanged(step: String?) {
                if (step.equals(ConstantsUtility.SignupScreenSteps.ENTER_NUMBER_STEP, ignoreCase = true)) {
                    signupButton.text = resources.getString(R.string.forgot_password_no_next_button_label)
                    signupButton.tag = ConstantsUtility.SignupScreenSteps.ENTER_NUMBER_STEP
                } else if (step.equals(ConstantsUtility.SignupScreenSteps.ENTER_PIN_STEP, ignoreCase = true)) {
                    signupButton.text = resources.getString(R.string.forgot_password_enter_pin_verify_button)
                    signupButton.tag = ConstantsUtility.SignupScreenSteps.ENTER_PIN_STEP
                } else if (step.equals(ConstantsUtility.SignupScreenSteps.SET_PASSWORD_STEP, ignoreCase = true)) {
                    signupButton.text = resources.getString(R.string.login_sign_up_label)
                    signupButton.tag = ConstantsUtility.SignupScreenSteps.SET_PASSWORD_STEP
                } else {
                    logE("signupsteps", "else->do nothing", fromClass, "subscribeForgotPasswordStepperDataObserver")
                }//else ends
            }//onChanged ends
        }//val forgotPasswordStepperDataObserver = object : Observer<String> ends

        mViewModel.signupPasswordStepperData.observe(this@SignUpActivity, signupPasswordStepperDataObserver)
    }//subscribeSignupStepperDataObserver ends

    /** any ui related overriden method
     * should be here*/
    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            /**if user is on the last step of the sign up
             * force him that he cant use back key to go back
             * in other words on the last step of the signup the user cant go back*/
            if (!signupButton.tag.toString().equals(ConstantsUtility.SignupScreenSteps.SET_PASSWORD_STEP, ignoreCase = true)) {
                //do nothing, continue the normal flow
                if (!isAllowback) {
                    return false
                }
            } else {
                return false
            }//if-else ends
        }
        return super.onKeyDown(keyCode, event)
    }//onKeyDown ends

    /**any campanion objects
     * should be here*/

    companion object {
        fun startSignUpActivity(context: Context, useCaseType: String) {
            val intent = Intent(context, SignUpActivity::class.java)
            intent.putExtra(ConstantsUtility.ManageAccount.LOGIN_USECASE_TYPE_KEY, useCaseType)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }
    }
}