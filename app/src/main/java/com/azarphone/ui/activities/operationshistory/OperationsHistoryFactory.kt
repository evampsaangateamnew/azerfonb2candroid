package com.azarphone.ui.activities.operationshistory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

/**
 * @author Junaid Hassan on 08, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class OperationsHistoryFactory(val mOperationsHistoryRepositry: OperationsHistoryRepositry) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(OperationsHistoryViewModel::class.java)) {
            return OperationsHistoryViewModel(mOperationsHistoryRepositry) as T
        }
        throw IllegalStateException("ViewModel cannot be casted")
    }
}