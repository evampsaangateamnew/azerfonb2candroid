package com.azarphone.ui.activities.moneytransfer

import com.azarphone.api.ApiClient
import com.azarphone.api.pojo.request.MoneyTransferRequest
import com.azarphone.api.pojo.response.inappsurvey.InAppSurvey
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.moneytransfer.MoneyTransferResponse
import io.reactivex.Observable

/**
 * @author Junaid Hassan on 08, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class MoneyTransferRepositry {
    init {

    }//init ends

    fun requestMoneyTransfer(moneyTransferRequest: MoneyTransferRequest): Observable<MoneyTransferResponse> {
        return ApiClient.newApiClientInstance.getServerAPI().requestMoneyTransfer(moneyTransferRequest)
    }

    fun saveInAppSurvey(uploadSurvey: UploadSurvey): Observable<InAppSurvey> {
        return ApiClient.newApiClientInstance.getServerAPI().saveInAppSurvey(uploadSurvey)
    }
}