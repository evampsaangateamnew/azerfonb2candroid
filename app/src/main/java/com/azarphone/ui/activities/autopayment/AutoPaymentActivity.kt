package com.azarphone.ui.activities.autopayment

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.azarphone.R
import com.azarphone.api.pojo.response.autopaymentresponse.ScheduledPaymentDataItem
import com.azarphone.api.pojo.response.autopaymentresponse.ScheduledPaymentResponse
import com.azarphone.api.pojo.response.autopaymentresponse.deletepayment.DeleteAutoPayment
import com.azarphone.api.pojo.response.fasttopupresponse.deletefasttopup.DeleteItemResponse
import com.azarphone.bases.BaseActivity
import com.azarphone.databinding.ActivityAutoPaymentBinding
import com.azarphone.ui.activities.newautopayment.AutoPaymentNewActivity
import com.azarphone.ui.adapters.recyclerviews.AutoPaymentAdapter
import com.azarphone.util.isNetworkAvailable
import com.azarphone.util.logE
import com.azarphone.validators.hasValue
import com.azarphone.widgets.popups.showMessageDialog
import kotlinx.android.synthetic.main.activity_auto_payment.*
import kotlinx.android.synthetic.main.activity_fast_topup.*
import kotlinx.android.synthetic.main.azer_custom_header_actionbar.*
import kotlinx.android.synthetic.main.layout_topup_activity.*
import java.util.*

/**
 * @author Umair Mustafa on 02, April, 2021
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * umair.mustafa@evampsaanga.com
 */
class AutoPaymentActivity : BaseActivity<ActivityAutoPaymentBinding, AutoPaymentFactory, AutoPaymentViewModel>() {

    var deletedPosition: Int = -1
    override fun getLayoutId(): Int {

        return R.layout.activity_auto_payment
    }

    override fun getViewModelClass(): Class<AutoPaymentViewModel> {
        return AutoPaymentViewModel::class.java
    }

    override fun getFactory(): AutoPaymentFactory {
        return AutoPaymentInjection.provideAutoPaymentFactory()
    }

    override fun init() {
        setupHeaderActionBar()
        initUI()
        subscribe()
        initUiEvents()
    }

    override fun onResume() {
        super.onResume()

        if (isNetworkAvailable(applicationContext)) {
            mViewModel.requestGetScheduledPayments()
        } else {
            showMessageDialog(applicationContext, this@AutoPaymentActivity,
                    "", resources.getString(R.string.message_no_internet))
        }
    }

    private fun subscribe() {
        val getAutoPaymentsResponseLiveData = Observer<ScheduledPaymentResponse> { scheduledPaymentResponse -> processAutoPaymentResponse(scheduledPaymentResponse!!) }
        mViewModel.getScheduledPaymentsResponseLiveData.observe(this@AutoPaymentActivity, getAutoPaymentsResponseLiveData)

        val deleteItemErrorLiveData = object : Observer<String> {
            override fun onChanged(t: String?) {
                logE("FastTopUpActivity", "error:::", "FastTopUpActivity", "error on delete item ")

                processDeleteItemResponse(t)
            }
        }
        mViewModel.deleteScheduledPaymentErrorLiveData.observe(this@AutoPaymentActivity, deleteItemErrorLiveData)

        val deleteItemLiveData = object : Observer<DeleteItemResponse> {
            override fun onChanged(t: DeleteItemResponse?) {
                if(!this@AutoPaymentActivity.isFinishing && deletedPosition != -1 && auto_payment_recycleview?.adapter!=null && t!=null) {
                    (auto_payment_recycleview.adapter as AutoPaymentAdapter).deleteItem(position=deletedPosition)
                    (auto_payment_recycleview.adapter as AutoPaymentAdapter).notifyDataSetChanged()
                    deletedPosition = -1
                } else {
                    deletedPosition = -1
                    (auto_payment_recycleview.adapter as AutoPaymentAdapter).notifyDataSetChanged()
                }
            }
        }
        mViewModel.deleteScheduledPaymentResponseLiveData.observe(this@AutoPaymentActivity, deleteItemLiveData)

    }

    private fun processDeleteItemResponse(t: String?) {
        auto_payment_recycleview.adapter?.notifyDataSetChanged()
    }


    private fun processAutoPaymentResponse(scheduledPaymentResponse: ScheduledPaymentResponse) {
        initializeRecyclerView(scheduledPaymentResponse.data.data as ArrayList<ScheduledPaymentDataItem?>)
    }

    private fun scheduledPaymentDeleted(deleteItemResponse: DeleteItemResponse) {

        if (!this@AutoPaymentActivity.isFinishing && deleteItemResponse != null && hasValue(deleteItemResponse.resultDesc)) {
            (auto_payment_recycleview!!.adapter as AutoPaymentAdapter).deleteItem(position = deletedPosition)
            (auto_payment_recycleview!!.adapter as AutoPaymentAdapter).notifyDataSetChanged()

            showMessageDialog(applicationContext, this@AutoPaymentActivity,
                    "", deleteItemResponse.resultDesc)
        } else {
            (auto_payment_recycleview?.adapter as AutoPaymentAdapter).notifyDataSetChanged()
        }
    }

    private fun initUI() {
    }

    private fun initUiEvents() {
        azerActionBarBackIcon.setOnClickListener { onBackPressed() }
    }

    private fun setupHeaderActionBar() {
        azerActionBarBubblesBg.setImageDrawable(resources.getDrawable(R.drawable.bubblestripcut))//set the action bar bg
        azerActionBarHamburgerIcon.visibility = View.GONE//show the hamburger icon
        azerActionBarNarLogo.visibility = View.GONE//show the nar logo in the center of action bar
        azerActionBarBackIcon.visibility = View.VISIBLE//show the back icon
        azerActionBarTitle.visibility = View.VISIBLE//show the title
        azerActionBarTitle.text = resources.getString(R.string.lbl_auto_payment)
        azerActionBarBellIcon.visibility = View.GONE//hide the bell icon
    }//setupHeaderActionBar ends

    private fun initializeRecyclerView(scheduledPaymentDataItems: ArrayList<ScheduledPaymentDataItem?>) {
        //placeholder data for mock display
        scheduledPaymentDataItems.add(ScheduledPaymentDataItem("", "", "",
                "", "", -1, "", "", "", ""))
        //Adapter creation
        val autoPaymentAdapter = AutoPaymentAdapter(this@AutoPaymentActivity, scheduledPaymentDataItems, object :
                AutoPaymentAdapter.Companion.ScheduledPaymentItemEvent {
            override fun onItemClick(position: Int) {
                if (position == scheduledPaymentDataItems.size - 1) {
//                    NewAutoPaymentActivity.start(this@AutoPaymentActivity)
                    AutoPaymentNewActivity.start(this@AutoPaymentActivity)
                }
            }

            override fun onDelete(position: Int) {
                scheduledPaymentDataItems[position]?.let {
                    deletedPosition = position
                    val deletedModel = DeleteAutoPayment(it.id)
                    mViewModel.requestDeleteScheduledPayment(deletedModel)
                }
            }

        })

        auto_payment_recycleview.layoutManager = LinearLayoutManager(this)
//        if (scheduledPaymentDataItems != null && scheduledPaymentDataItems.size > 1) {
//            val itemTouchHelper = ItemTouchHelper(enableSwipeToDeleteAndUndo(autoPaymentAdapter, scheduledPaymentDataItems)!!)
//            itemTouchHelper.attachToRecyclerView(auto_payment_recycleview)
//        }
        auto_payment_recycleview.adapter = autoPaymentAdapter

    }

    companion object {
        private val TAG = AutoPaymentActivity::class.java.simpleName
        fun start(context: Context) {
            val intent = Intent(context, AutoPaymentActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }//start ends
    }//object


    private fun enableSwipeToDeleteAndUndo(autoPaymentAdapter: AutoPaymentAdapter, scheduledPaymentDataItems: ArrayList<ScheduledPaymentDataItem?>): ItemTouchHelper.SimpleCallback? {
        return object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
                //Remove swiped item from list and notify the RecyclerView
                deletedPosition = viewHolder.adapterPosition
                scheduledPaymentDataItems[deletedPosition]?.let {

                    val deletedModel = DeleteAutoPayment(it.id)
                    mViewModel.requestDeleteScheduledPayment(deletedModel)
                }

            }
        }
    }


}