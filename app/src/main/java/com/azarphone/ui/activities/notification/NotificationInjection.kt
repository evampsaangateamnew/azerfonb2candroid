package com.azarphone.ui.activities.notification

object NotificationInjection{
    fun provideNotificationRepository():NotificationRepositry{
        return NotificationRepositry()
    }

    fun provideNotificationFactory():NotificationFactory{
        return NotificationFactory(provideNotificationRepository())
    }
}