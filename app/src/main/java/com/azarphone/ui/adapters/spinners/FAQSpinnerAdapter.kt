package com.azarphone.ui.adapters.spinners

import android.annotation.SuppressLint
import android.content.Context
import androidx.constraintlayout.widget.ConstraintLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.faqhelpers.FAQSpinnerHelperModel
import com.azarphone.eventhandler.FAQItemSelectEvents
import com.azarphone.validators.hasValue
import java.util.ArrayList

/**
 * @author Junaid Hassan on 26, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class FAQSpinnerAdapter : ArrayAdapter<FAQSpinnerHelperModel> {

    private var titlesList: ArrayList<FAQSpinnerHelperModel>? = null
    private var fAQItemSelectEvents: FAQItemSelectEvents? = null

    constructor(context: Context, titlesList: ArrayList<FAQSpinnerHelperModel>, fAQItemSelectEvents: FAQItemSelectEvents) : super(context, R.layout.layout_faq_spinner, R.id.faqTitle, titlesList) {
        this.titlesList = titlesList
        this.fAQItemSelectEvents = fAQItemSelectEvents
    }//constructor ends

    override fun getCount(): Int {
        return titlesList!!.size
    }//getCount ends

    override fun getItem(position: Int): FAQSpinnerHelperModel? {
        return super.getItem(position)
    }//getItem ends

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }//getItemId ends

    @SuppressLint("InflateParams")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View?
        val viewHolder: ViewHolder
        if (convertView == null) {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.layout_faq_spinner, null)
            viewHolder = ViewHolder(view)
            view?.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }//else ends

        /*  viewHolder.faqTitle!!.isSelected = true
          viewHolder.faqTitle!!.text = titlesList!![position].title
          viewHolder.faqTitle!!.visibility = View.GONE
          viewHolder.mainContainer!!.setBackgroundColor(context.resources.getColor(R.color.colorWhite))*/
        return view as View
    }//getView ends

    @SuppressLint("InflateParams")
    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        // This view starts when we click the spinner.
        val view: View?
        val viewHolder: ViewHolder
        if (convertView == null) {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.layout_faq_spinner, null)
            viewHolder = ViewHolder(view)
            view?.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }//else ends

        //view handling
        viewHolder.faqTitle!!.isSelected = true
        viewHolder.faqTitle!!.text = titlesList!![position].title
        viewHolder.mainContainer!!.setBackgroundColor(context.resources.getColor(R.color.colorWhite))

        viewHolder.mainContainer!!.setOnClickListener {
            if (hasValue(titlesList!![position].title!!)) {
                fAQItemSelectEvents!!.onItemSelectForSearch(titlesList!![position].title!!)
            }
        }//viewHolder.mainContainer!!.setOnClickListener ends
        return view as View
    }//getDropDownView ends

    private class ViewHolder(row: View?) {
        var faqTitle: TextView? = null
        var mainContainer: ConstraintLayout? = null

        init {
            this.faqTitle = row?.findViewById(R.id.faqTitle) as TextView
            this.mainContainer = row.findViewById(R.id.mainContainer) as ConstraintLayout
        }//init ends
    }// class ViewHolder ends

}