package com.azarphone.ui.adapters.spinners

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.azarphone.R
import com.azarphone.api.pojo.response.supplementaryoffers.response.Countries
import com.azarphone.eventhandler.RoamingSpinnerAdapterEvents
import com.azarphone.util.getImageFromAssests
import com.azarphone.validators.hasValue

/**
 * @author Junaid Hassan on 24, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class RoamingCountriesSpinnerAdapter : BaseAdapter {

    private var context: Context? = null
    private var countries: List<Countries?>? = null
    private var inflater: LayoutInflater? = null
    private var roamingSpinnerAdapterEvents: RoamingSpinnerAdapterEvents

    constructor(contxt: Context, countries: List<Countries?>, roamingSpinnerAdapterEvents: RoamingSpinnerAdapterEvents) {
        context = contxt
        this.countries = countries
        this.roamingSpinnerAdapterEvents = roamingSpinnerAdapterEvents
        inflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun getCount(): Int {
        return countries!!.size
    }

    override fun getItem(position: Int): String {
        return ""
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    @SuppressLint("InflateParams")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        val view: View?
        val viewHolder: ViewHolder
        if (convertView == null) {
            val inflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.usage_roaming_countries_spinner, null)
            viewHolder = ViewHolder(view)
            view?.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }//else ends

        //do nothing
        viewHolder.mainContainer!!.visibility = View.GONE
        return view as View
    }//getView ends

    @SuppressLint("InflateParams")
    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        // This view starts when we click the spinner.
        val view: View?
        val viewHolder: ViewHolder
        if (convertView == null) {
            val inflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.usage_roaming_countries_spinner, null)
            viewHolder = ViewHolder(view)
            view?.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }//else ends

        //view handling
        var countryName = ""
        if (countries!![position]!!.name != null && hasValue(countries!![position]!!.name)) {
            viewHolder.roamingCountryTitle!!.text = countries!![position]!!.name
            viewHolder.roamingCountryTitle!!.isSelected = true
            countryName=countries!![position]!!.name!!
        }//if (hasValue(countries!![position]!!.name)) ends

        var countryFlag = ""
        if (countries!![position]!!.flag != null && hasValue(countries!![position]!!.flag)) {
            viewHolder.countryFlag!!.setImageDrawable(getImageFromAssests(context!!, countries!![position]!!.flag!!))
            countryFlag=countries!![position]!!.flag!!
        }//if (countries!![position]!!.flag != null && hasValue(countries!![position]!!.flag)) ends

        viewHolder.mainContainer!!.background = context!!.resources.getDrawable(R.drawable.spinner_item_selector_bg)

        viewHolder.mainContainer!!.setOnClickListener {
            roamingSpinnerAdapterEvents.onCountrySelectedForRoaming(countryName,countryFlag)
        }

        return view as View
    }//getDropDownView ends

    private class ViewHolder(row: View?) {
        var roamingCountryTitle: TextView? = null
        var countryFlag: ImageView? = null
        var mainContainer: RelativeLayout? = null

        init {
            this.roamingCountryTitle = row?.findViewById(R.id.roamingCountryTitle) as TextView
            this.countryFlag = row.findViewById(R.id.countryFlag) as ImageView
            this.mainContainer = row.findViewById(R.id.mainContainer) as RelativeLayout
        }//init ends
    }// class ViewHolder ends

}