package com.azarphone.ui.adapters.recyclerviews

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.azarphone.R
import com.azarphone.api.pojo.response.fnfresponse.FnfListItem
import com.azarphone.eventhandler.FnFEditButtonEvents
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.RootValues
import com.azarphone.util.logE
import com.azarphone.validators.hasValue

class FnFListAdapter(val mContext: Context, val fnfList: List<FnfListItem?>) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

    private val fromClass = "FNFAdapter"

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fnf_list_item,
                parent, false)
        return RequestHistoryAdapterViewHolder(view)
    }//onCreateViewHolder ends

    override fun getItemCount(): Int {
        return fnfList.size
    }//getItemCount ends

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as RequestHistoryAdapterViewHolder

        viewHolder.number.text = ConstantsUtility.UserConstants.USERNAME_PREFIX + fnfList[position]?.msisdn
        viewHolder.number.isSelected = true
        viewHolder.date.text = fnfList[position]?.createdDate
        viewHolder.date.isSelected = true

        if (position == fnfList.size - 1) {
            viewHolder.separator.visibility = View.GONE
        } else {
            viewHolder.separator.visibility = View.VISIBLE
        }

        viewHolder.editImage.setOnClickListener {
            var number = ""
            if (hasValue(viewHolder.number.text.toString())) {
                number = viewHolder.number.text.toString()
                number= number.replace(ConstantsUtility.UserConstants.USERNAME_PREFIX, "")
            }
            RootValues.getFnFEditButtonEvents().onEditButtonClicked(number)
        }
    }//onBindViewHolder ends

    inner class RequestHistoryAdapterViewHolder(row: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(row) {
        var number: TextView = row.findViewById(R.id.number) as TextView
        var date: TextView = row.findViewById(R.id.date) as TextView
        var editImage: ImageView = row.findViewById(R.id.editImage) as ImageView
        var separator: View = row.findViewById(R.id.separator) as View
    }
}