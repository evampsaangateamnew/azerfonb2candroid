package com.azarphone.ui.adapters.recyclerviews

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.HomePageFreeResourcesHelperModel
import com.azarphone.ui.activities.mysubscriptions.MySubscriptionsActivity
import com.azarphone.ui.activities.supplementaryoffers.SupplementaryOffersActivity
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.logE

/**
 * @author Junaid Hassan on 09, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class DashboadMiddleRecycler(val mContext: Context, val mList: ArrayList<HomePageFreeResourcesHelperModel>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val fromClass = "DashboadMiddleRecycler"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_dashboard_middle_item, parent, false)
        return DashboadMiddleRecyclerViewHolder(view)
    }//onCreateViewHolder ends

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as DashboadMiddleRecyclerViewHolder

        viewHolder.titleTV.text = mList[position].resourcesTitleLabel

        viewHolder.valueTV.text = mList[position].remainingFormatted
        viewHolder.valueTV.isSelected = true

        viewHolder.resourceUnitName.text = mList[position].resourceUnitName

        /*set the progresses*/
        val resourceInitialUnits = mList[position].resourceInitialUnits!!.toDouble()
        val resourceRemainingUnits = mList[position].resourceRemainingUnits!!.toDouble()

        var progress = ((resourceRemainingUnits * 100.0f) / resourceInitialUnits).toInt()
        /* if (progress == 100) {
             //do nothing
         } else {
             progress = 100 - progress
         }*/
        logE("percentageMiddle", "percentage:::".plus(progress), "DashboadMiddleRecycler", "onBindViewHolder")
        when (progress) {
            0 -> viewHolder.progressBar.progress = 0
            100 -> viewHolder.progressBar.progress = 100
            else -> viewHolder.progressBar.progress = progress
        }

        if (position == mList.size - 1) {
            viewHolder.verticalView.visibility = View.GONE
        } else {
            viewHolder.verticalView.visibility = View.VISIBLE
        }

        initUIEvents(viewHolder, position)

    }//onBindViewHolder ends

    private fun initUIEvents(viewHolder: DashboadMiddleRecyclerViewHolder, position: Int) {
        viewHolder.titleAddIcon.setOnClickListener {
            logE("selectedTitleX", "titleClicked:::".plus(mList[position].resourceType), fromClass, "initUIEvents")

            val intent = Intent(mContext, SupplementaryOffersActivity::class.java)
            intent.putExtra(ConstantsUtility.PackagesConstants.DASHBOARD_INTENT_DATA, mList[position].resourceType)
            mContext.startActivity(intent)
        }//viewHolder.titleAddIcon.setOnClickListener ends

        viewHolder.subscriptionsRedirectionArea.setOnClickListener {
            logE("selectedTitleX", "titleClicked:::".plus(mList[position].resourceType), fromClass, "initUIEvents")

            val intent = Intent(mContext, MySubscriptionsActivity::class.java)
            intent.putExtra(ConstantsUtility.MySubscriptionsConstants.DASHBOARD_INTENT_DATA, mList[position].resourceType)
            mContext.startActivity(intent)
        }//viewHolder.subscriptionsRedirectionArea.setOnClickListener ends
    }//initUIEvents ends

    override fun getItemCount(): Int {
        return mList.size
    }//getItemCount ends

    private inner class DashboadMiddleRecyclerViewHolder(row: View) : RecyclerView.ViewHolder(row) {
        var titleTV: TextView = row.findViewById(R.id.titleTV) as TextView
        var verticalView: View = row.findViewById(R.id.verticalView) as View
        var titleAddIcon: ImageView = row.findViewById(R.id.titleAddIcon) as ImageView
        var valueTV: TextView = row.findViewById(R.id.valueTV) as TextView
        var progressBar: ProgressBar = row.findViewById(R.id.progressBar) as ProgressBar
        var resourceUnitName: TextView = row.findViewById(R.id.resourceUnitName) as TextView
        var subscriptionsRedirectionArea: ConstraintLayout = row.findViewById(R.id.subscriptionsRedirectionArea) as ConstraintLayout
    }//DashboadMiddleRecyclerViewHolder ends
}//class ends