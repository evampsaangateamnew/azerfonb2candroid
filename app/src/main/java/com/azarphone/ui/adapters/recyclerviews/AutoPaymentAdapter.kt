package com.azarphone.ui.adapters.recyclerviews

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.azarphone.R
import com.azarphone.api.pojo.response.autopaymentresponse.ScheduledPaymentDataItem
import com.daimajia.swipe.SwipeLayout

class AutoPaymentAdapter(val context: Context?, val savedScheduleItems: ArrayList<ScheduledPaymentDataItem?>, private val scheduledPaymentItemEvent: ScheduledPaymentItemEvent?) : RecyclerView.Adapter<AutoPaymentAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.auto_payment_item,
                parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val scheduledPaymentDataItem: ScheduledPaymentDataItem? = savedScheduleItems[position]
        holder.status.text = scheduledPaymentDataItem?.recurrentDay
        holder.status.isSelected = true
        holder.topUpNumber.text = scheduledPaymentDataItem?.msisdn
        holder.textPayment.text = scheduledPaymentDataItem?.amount

        // For Marquees
        holder.textPayment.isSelected = true
        holder.status.isSelected = true
        holder.topUpNumber.isSelected = true

        holder.swipeLayout.close()
        //set Click listener to RecyclerView Row
        holder.itemLayout.setOnClickListener { _: View? -> updateItemCheckState(position) }
        if (position == savedScheduleItems.size - 1) {
            //check if true, then show Add card option & hide radio button for Add card Option in the recyclerview.
            //Setting Visibility for views
            holder.llAmount.visibility = View.GONE
            holder.status.visibility = View.GONE
            holder.arrowRight.visibility = View.VISIBLE
            holder.deleteButton.visibility = View.GONE
            //Setting up card icon
            holder.cardIcon.setImageResource(R.drawable.ic_auto_payment)
            //Setting text to view for add new auto payment
            holder.topUpNumber.text =
                context?.getString(R.string.lbl_auto_payment_add_new_auto_payment)

        } else {
            //Setting Visibility for views
            holder.llAmount.visibility = View.VISIBLE
            holder.status.visibility = View.VISIBLE
            holder.arrowRight.visibility = View.GONE

            //Setting up card icons according to type
            if (scheduledPaymentDataItem?.cardType?.contains("v") == true) {
                holder.cardIcon.setImageResource(R.drawable.ic_visa_card)
            } else if (scheduledPaymentDataItem?.cardType?.contains("m") == true) {
                holder.cardIcon.setImageResource(R.drawable.ic_master_card)
            }
        }
        holder.deleteButton.setOnClickListener {
            scheduledPaymentItemEvent?.onDelete(position)
        }
    } //end onBindViewHolder


    private fun updateItemCheckState(position: Int) {
        scheduledPaymentItemEvent?.onItemClick(position)
    }

    override fun getItemCount(): Int {
        return savedScheduleItems.size
    }

    fun deleteItem(position: Int) {
        savedScheduleItems.removeAt(position)
        notifyDataSetChanged()
    }

    class ViewHolder( itemView: View) : RecyclerView.ViewHolder(itemView) {
        var topUpNumber: TextView = itemView.findViewById(R.id.card_text)
        var textPayment: TextView = itemView.findViewById(R.id.text_payment)
        var status: TextView = itemView.findViewById(R.id.text_status)
        var cardIcon: ImageView = itemView.findViewById(R.id.card_icon)
        var arrowRight: ImageView = itemView.findViewById(R.id.arrow_right)
        var llAmount: LinearLayout = itemView.findViewById(R.id.llAmount)
        var deleteButton :TextView = itemView.findViewById(R.id.deleteButton)
        var swipeLayout : SwipeLayout = itemView.findViewById(R.id.swipeLayout)
        var itemLayout : ConstraintLayout = itemView.findViewById(R.id.cl_main_item)


    }

    companion object {
        interface ScheduledPaymentItemEvent {
            fun onItemClick(position: Int)
            fun onDelete(position: Int)
        }
    }
}
