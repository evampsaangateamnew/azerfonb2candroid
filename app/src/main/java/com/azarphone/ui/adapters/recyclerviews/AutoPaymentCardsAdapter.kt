package com.azarphone.ui.adapters.recyclerviews

import android.content.Context
import android.graphics.Typeface
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.azarphone.R
import com.azarphone.api.pojo.response.topup.savedcards.SavedCardsItem
import com.azarphone.eventhandler.TopUpItemSelectEvents

class AutoPaymentCardsAdapter(private val dataSet: List<SavedCardsItem?>, val mContext: Context, private val topUpItemSelectEvents: TopUpItemSelectEvents) : RecyclerView.Adapter<AutoPaymentCardsAdapter.ViewHolder>() {
    private var selectedItem = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.auto_payment_cards_item,
                parent, false)
        return ViewHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val getSavedCardsItems: SavedCardsItem? = dataSet[position]

        holder.cardNumber.setText(getSavedCardsItems?.cardMaskNumber)
        //check if position is selected Item then update selected row color
        //else set properties to normal.
        if (position == selectedItem) {
            holder.cardNumber.setTextColor(mContext.getColor(R.color.colorPrimary))
            holder.radioButton.isChecked = true
            holder.cardNumber.setTypeface(holder.cardNumber.typeface, Typeface.BOLD);

        } else {
            holder.cardNumber.setTextColor(mContext.getColor(R.color.brown_grey))
            holder.radioButton.isChecked = false
            holder.cardNumber.setTypeface(holder.cardNumber.typeface, Typeface.NORMAL);
        }
        //set Click listener to radioButton
        holder.radioButton.setOnClickListener { v: View? -> updateItemCheckState(position) }
        //set Click listener to RecyclerView Row
        holder.itemView.setOnClickListener { view: View? -> updateItemCheckState(position) }

        //else show simple card according to its type
        if (getSavedCardsItems?.cardType?.contains("v") == true) {
            holder.cardIcon.setImageResource(R.drawable.ic_visa_card)
        } else if (getSavedCardsItems?.cardType?.contains("m") == true) {
            holder.cardIcon.setImageResource(R.drawable.ic_master_card)
        }
    } //end onBindViewHolder


    private fun isValidCardData(getSavedCardsItems: SavedCardsItem?): Boolean {
        return (getSavedCardsItems?.id != null && getSavedCardsItems.id.isNotEmpty() && getSavedCardsItems?.cardType!!.isNotEmpty() && getSavedCardsItems.cardMaskNumber!!.isNotEmpty())
    }

    private fun updateItemCheckState(position: Int) {
        val copyOfLastCheckedPosition = selectedItem
        selectedItem = position
        notifyItemChanged(copyOfLastCheckedPosition)
        notifyItemChanged(selectedItem)
        dataSet[position]?.paymentKey?.let { topUpItemSelectEvents.onItemSelected(position, it) }
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var cardNumber: TextView
        var cardIcon: ImageView
        var radioButton: RadioButton

        init {
            cardIcon = itemView.findViewById(R.id.card_icon)
            cardNumber = itemView.findViewById(R.id.card_text)
            radioButton = itemView.findViewById(R.id.radio_btn)
        }
    }
}
