package com.azarphone.ui.adapters.expandablelists

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import androidx.core.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.faqhelpers.FAQListHelperModel
import com.azarphone.util.logE
import com.azarphone.validators.hasValue

/**
 * @author Junaid Hassan on 26, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class FAQListAdapter : BaseExpandableListAdapter, Filterable {

    private var faqFilter: FAQFilter? = null
    private var tempFaqs: ArrayList<FAQListHelperModel>? = null
    private var faqsArrayList: ArrayList<FAQListHelperModel>? = null
    private var context: Context? = null
    private var activity: Activity? = null

    constructor(context: Context, activity: Activity, faqsArrayList: ArrayList<FAQListHelperModel>) {
        this.context = context
        this.activity = activity
        this.faqsArrayList = faqsArrayList
        tempFaqs=faqsArrayList
    }

    override fun getFilter(): Filter {
        if (faqFilter == null)
            faqFilter = FAQFilter()
        return faqFilter!!
    }

    internal inner class FAQFilter : Filter() {
        override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {
            val results = Filter.FilterResults()
            if (constraint != null) {
                logE("filX2","key:::".plus(constraint),"FAQAdapter","filter")
                if (constraint.isEmpty()) {
                    results.values = tempFaqs
                    results.count = tempFaqs!!.size
                } else {
                    val filterList = ArrayList<FAQListHelperModel>()
                    for (faq in tempFaqs!!) {
                        if(hasValue(constraint.toString())&& hasValue(faq.title)){
                            if (faq.title!!.toLowerCase().contains(constraint.toString().toLowerCase())) {
                                logE("filX2","key:::foundddd:::","FAQAdapter","filter")
                                filterList.add(faq)
                            }else{
                                logE("filX2","key:::NOT FOUND:::","FAQAdapter","filter")
                            }
                        }
                    }
                    results.count = filterList.size
                    results.values = filterList

                }
            }
            return results
        }

        override fun publishResults(constraint: CharSequence, results: Filter.FilterResults) {
            faqsArrayList = results.values as ArrayList<FAQListHelperModel>
            notifyDataSetChanged()
        }
    }

    override fun getGroupCount(): Int {
        return if (faqsArrayList == null) 0 else faqsArrayList!!.size
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return false
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        return 1
    }

    override fun getGroup(groupPosition: Int): FAQListHelperModel {
        return faqsArrayList!![groupPosition]
    }

    override fun getChild(groupPosition: Int, childPosition: Int): String {
        return faqsArrayList!![groupPosition].answer!!
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    @SuppressLint("InflateParams")
    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup): View {
        val view: View?
        val viewHolder: ViewHolder
        if (convertView == null) {
            val inflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.layout_faq_header_item, null)
            viewHolder = ViewHolder(view)
            view?.tag = viewHolder
        }//if ends
        else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }//else ends

        if (hasValue(faqsArrayList!![groupPosition].question)) {
            viewHolder.headerText!!.text = faqsArrayList!![groupPosition].question
            viewHolder.headerText!!.isSelected = true
        } else {
            viewHolder.headerText!!.text = ""
        }

        if (isExpanded) {
            viewHolder.faqListIcon!!.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.faqminus))
        } else {
            viewHolder.faqListIcon!!.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.faqplus))
        }

        return view as View
    }//getGroupView ends

    @SuppressLint("InflateParams")
    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
        val view: View?
        val viewHolder: ViewHolder

        if (convertView == null) {
            val inflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.layout_faq_child_item, null)
            viewHolder = ViewHolder(view)
            view?.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }

        if (hasValue(faqsArrayList!![groupPosition].answer)) {
            viewHolder.childText!!.text = faqsArrayList!![groupPosition].answer
        } else {
            viewHolder.childText!!.text = ""
        }
        return view as View
    }//getChildView ends

    private class ViewHolder(row: View?) {
        var headerText: TextView? = null
        var childText: TextView? = null
        var faqListIcon: ImageView? = null

        init {
            this.headerText = row?.findViewById(R.id.headerText)
            this.childText = row?.findViewById(R.id.childText)
            this.faqListIcon = row?.findViewById(R.id.faqListIcon)
        }//init ends
    }// class GroupViewHolder ends
}//class ends