package com.azarphone.ui.adapters.recyclerviews

import androidx.lifecycle.Observer
import android.content.Context
import android.graphics.Color
import android.view.View
import com.azarphone.R
import com.azarphone.api.pojo.response.nartvsubscriptionsresponse.NarTvSubscriptionResponse
import com.azarphone.api.pojo.response.nartvsubscriptionsresponse.PlansItem
import com.azarphone.bases.BaseAdapter
import com.azarphone.eventhandler.NarTvSubscriptionsEventHandler
import com.azarphone.ui.adapters.MyViewHolder
import com.azarphone.util.getDateAccordingToClientSide
import kotlinx.android.synthetic.main.nartv_item.view.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class NarTvRecyclerAdapter(var list: ArrayList<PlansItem>?, val listener: NarTvSubscriptionsEventHandler, val context: Context) : BaseAdapter() {
    override fun getObjForPosition(position: Int): Any {

        return list!!.get(position)
    }

    override fun getLayoutIdForPosition(position: Int): Int {
        return R.layout.nartv_item
    }


    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, p1: Int) {
        val obj = getObjForPosition(p1)
        holder.bind(obj)

        if (!list?.get(p1)?.status.equals("true", ignoreCase = true)) {
            holder.itemView.renew.visibility = View.GONE
            holder.itemView.renew_date.visibility = View.GONE
            holder.itemView.renew_days.visibility = View.GONE
            holder.itemView.progessbar.visibility = View.GONE
        } else {
            holder.itemView.renew.visibility = View.VISIBLE
            holder.itemView.renew_date.visibility = View.VISIBLE
            holder.itemView.renew_days.visibility = View.VISIBLE
            holder.itemView.progessbar.visibility = View.VISIBLE

            val dateFormat = SimpleDateFormat("yyyy-MM-dd")
            val date = Date()
            System.out.println(dateFormat.format(date))

            if(list?.get(p1)?.activatedPlanStatus!=null) {
                var max = getCountOfDays(list?.get(p1)?.activatedPlanStatus?.renewalDate!!, list?.get(p1)?.activatedPlanStatus?.startDate!!)
                var remaining = getCountOfDays(dateFormat.format(date), list?.get(p1)?.activatedPlanStatus?.renewalDate!!)

                max = max.replace("-", "")
                remaining = remaining.replace("-", "")
                holder.itemView.progessbar.max = max.toInt()
                holder.itemView.progessbar.progress = max.toInt() - remaining.toInt()

                holder.itemView.renew_days.text = remaining.toString()
                holder.itemView.renew_date.text = list?.get(p1)?.activatedPlanStatus?.renewalDateLabel!! + getDateAccordingToClientSide(list?.get(p1)?.activatedPlanStatus?.renewalDate!!)
            }
            }

        if (list?.get(p1)?.isSelected!!) {
            holder.itemView.main_layout.setBackgroundColor(context.resources.getColor(R.color.colorFaqView))
        } else {
            holder.itemView.main_layout.setBackgroundColor(context.resources.getColor(R.color.cardview_light_background))
        }


        /* if(UserDataManager.getCustomerData()?.msisdn.equals(list?.get(p1)?.msisdn)) {
             holder.itemView.title.visibility = View.VISIBLE
         }
         else{
             holder.itemView.ovalIV.visibility = View.INVISIBLE
         }*/
    }

    fun onItemClicked(item: PlansItem) {
//        Log.d("Clicked", item.title)

        changeBGColorOfSelectedItem(item)

        listener.onSubscriptionSelected(item)


    }

    private fun changeBGColorOfSelectedItem(item: PlansItem) {

        for (i in 0 until list?.size!!) {
            if (list?.get(i) == item) {
                list?.get(i)?.isSelected = true
            } else {
                list?.get(i)?.isSelected = false
            }
        }

        notifyDataSetChanged()
    }

    /*
        fun onDeleteClicked(item: PlansItem) {
            Log.d("Clicked",item.msisdn)
            if( UserDataManager.getCustomerData()?.msisdn!=item.msisdn){
                list?.remove(item)
                notifyDataSetChanged()
            }
            listener.onDeleteNumberClick(item)

        }*/
    fun getCountOfDays(createdDateString: String, expireDateString: String): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

        var createdConvertedDate: Date? = null
        var expireCovertedDate: Date? = null
        var todayWithZeroTime: Date? = null
        try {
            createdConvertedDate = dateFormat.parse(createdDateString)
            expireCovertedDate = dateFormat.parse(expireDateString)

            val today = Date()

            todayWithZeroTime = dateFormat.parse(dateFormat.format(today))
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        var cYear = 0
        var cMonth = 0
        var cDay = 0

        if (createdConvertedDate!!.after(todayWithZeroTime)) {
            val cCal = Calendar.getInstance()
            cCal.time = createdConvertedDate
            cYear = cCal.get(Calendar.YEAR)
            cMonth = cCal.get(Calendar.MONTH)
            cDay = cCal.get(Calendar.DAY_OF_MONTH)

        } else {
            val cCal = Calendar.getInstance()
            cCal.time = todayWithZeroTime
            cYear = cCal.get(Calendar.YEAR)
            cMonth = cCal.get(Calendar.MONTH)
            cDay = cCal.get(Calendar.DAY_OF_MONTH)
        }


        /*   val todayCal = Calendar.getInstance();
       val todayYear = todayCal.get(Calendar.YEAR);
           val today = todayCal.get(Calendar.MONTH);
           val todayDay = todayCal.get(Calendar.DAY_OF_MONTH);*/


        val eCal = Calendar.getInstance()
        eCal.time = expireCovertedDate

        val eYear = eCal.get(Calendar.YEAR)
        val eMonth = eCal.get(Calendar.MONTH)
        val eDay = eCal.get(Calendar.DAY_OF_MONTH)

        val date1 = Calendar.getInstance()
        val date2 = Calendar.getInstance()

        date1.clear()
        date1.set(cYear, cMonth, cDay)
        date2.clear()
        date2.set(eYear, eMonth, eDay)

        val diff = date2.timeInMillis - date1.timeInMillis

        val dayCount = diff.toFloat() / (24 * 60 * 60 * 1000)

        //Log.d("Days", "" + dayCount.toInt() + " Days")
        return "" + dayCount.toInt()
    }

}