package com.azarphone.ui.adapters.recyclerviews

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.azarphone.R
import com.azarphone.api.pojo.response.fasttopupresponse.FastTopUpDetail
import com.azarphone.ui.activities.fasttopup.FastTopUpActivity
import com.azarphone.ui.activities.topup.TopupActivity
import com.daimajia.swipe.SwipeLayout
import java.util.*


class FastTopUpAdapter(
    private val fastTopUpDetails: ArrayList<FastTopUpDetail>,
    val context: Context,
    private val setOnItemSelected: SetOnItemSelected
) : RecyclerView.Adapter<FastTopUpAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(
            R.layout.fast_topup_item,
            parent, false
        )
        return ViewHolder(view)
    } // onCreateViewHolder Ends


    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val fastTopUpDetail: FastTopUpDetail = fastTopUpDetails[position]
        //safe passage
        holder.swipeLayout.close()
        //set Click listener to RecyclerView Row
        holder.itemLayout.setOnClickListener { view: View? ->
            //Check if user clicked last item then Go to TopUp Screen
            if (position == fastTopUpDetails.size - 1 && context is FastTopUpActivity) {
                TopupActivity.start(context)
            }
        }
        //check if last item then set properties to Do More Payments item.
        //else continue with normal flow
        if (position == fastTopUpDetails.size - 1) {
            //Setting Visibility of views
            holder.llAmount.visibility = View.GONE
            holder.btnPay.visibility = View.GONE
            holder.arrowRight.visibility = View.VISIBLE
            //Setting up card icon
            holder.cardIcon.setImageResource(R.drawable.ic_fast_topup)
            holder.deleteBtn.visibility = View.GONE
            //Setting text to view for more payment
            holder.cardNumber.text = context.getString(R.string.more_payments)
        } else {
            //Setting Visibility of views
            holder.arrowRight.visibility = View.GONE
            holder.llAmount.visibility = View.VISIBLE
            holder.btnPay.visibility = View.VISIBLE

            //Setting text to views
            holder.cardNumber.text = fastTopUpDetail.topupNumber
            holder.textPayment.text = fastTopUpDetail.amount
            //Adding code for marquees
            holder.textPayment.isSelected = true
            holder.cardNumber.isSelected = true
            //Setting up card icons according to type
            if (fastTopUpDetail.cardType.contains("v")) {
                holder.cardIcon.setImageResource(R.drawable.ic_visa_card)
            } else if (fastTopUpDetail.cardType.contains("m")) {
                holder.cardIcon.setImageResource(R.drawable.ic_master_card)
            }
        }

        //Setting ClickListener to Button
        holder.btnPay.setOnClickListener { _ ->
            setOnItemSelected.onSelected(fastTopUpDetail)
        }

        holder.deleteBtn.setOnClickListener {
            setOnItemSelected.onDelete(fastTopUpDetail, position)
        }
    } //onBindViewHolder Ends


    override fun getItemCount(): Int {
        return fastTopUpDetails.size
    }//getItemCount Ends

    fun deleteItem(position: Int) {
        fastTopUpDetails.removeAt(position)
        notifyDataSetChanged()
    }//deleteItem Ends

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var cardNumber: TextView = itemView.findViewById(R.id.card_text)
        var textPayment: TextView = itemView.findViewById(R.id.text_payment)
        var llAmount: LinearLayout = itemView.findViewById(R.id.llAmount)
        var cardIcon: ImageView = itemView.findViewById(R.id.card_icon)
        var arrowRight: ImageView = itemView.findViewById(R.id.arrow_right)
        var btnPay: Button = itemView.findViewById(R.id.btn_pay)
        var deleteBtn: TextView = itemView.findViewById(R.id.deleteButton)
        var swipeLayout: SwipeLayout = itemView.findViewById(R.id.swipeLayout)
        var itemLayout: ConstraintLayout = itemView.findViewById(R.id.cl_main_item)

    }

    companion object {
        interface SetOnItemSelected {
            fun onSelected(fastTopUpDetail: FastTopUpDetail?)
            fun onDelete(fastTopUpDetail: FastTopUpDetail?, position: Int)
        }


    }

}
