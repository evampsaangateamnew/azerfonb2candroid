package com.azarphone.ui.adapters.recyclerviews

import android.view.View
import com.azarphone.R
import com.azarphone.api.pojo.response.customerdata.CustomerData
import com.azarphone.bases.BaseAdapter
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.MainActivityEventsHandler
import com.azarphone.ui.adapters.MyViewHolder
import kotlinx.android.synthetic.main.manage_account_item.view.*
import java.util.ArrayList

class ManageAccountPopUpRecyclerAdapter(val list: ArrayList<CustomerData>?,val listener: MainActivityEventsHandler) :BaseAdapter(){
    override fun getObjForPosition(position: Int): Any {
        return list!!.get(position)
    }

    override fun getLayoutIdForPosition(position: Int): Int {
        return R.layout.manage_account_item
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, p1: Int) {
        val obj=getObjForPosition(p1)
        holder.bind(obj)
        if(UserDataManager.getCustomerData()?.msisdn.equals(list?.get(p1)?.msisdn)) {
            holder.itemView.ovalIV.visibility = View.VISIBLE
        }
        else{
            holder.itemView.ovalIV.visibility = View.INVISIBLE
        }

        holder.itemView.nameTV.isSelected=true
        holder.itemView.number.isSelected=true
    }

    fun onItemClicked(customer: CustomerData) {
//        Log.d("Clicked",customer.msisdn)
        //save the data in the session
       // UserDataManager.saveCustomerData(customer)
        listener.onSwitchNumberClick(customer)

        for(i in 0 until list?.size!!) {
            if (UserDataManager.getCustomerData()?.msisdn.equals( customer.msisdn)) {
               // val currentAccount=list.get(i)
                list.remove(customer)
                list.add(0,customer)

            }
        }

        notifyDataSetChanged()
    }

}