package com.azarphone.ui.adapters.recyclerviews

import android.content.Context
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.azarphone.R
import com.azarphone.api.pojo.response.notification.NotificationsListItem
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.ui.activities.mainactivity.MainActivity
import com.azarphone.ui.activities.specialoffers.SpecialOffersActivity
import com.azarphone.ui.activities.supplementaryoffers.SupplementaryOffersActivity
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.RootValues
import com.azarphone.util.logE
import com.azarphone.validators.hasValue

/**
 * @author Junaid Hassan on 28, June, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class NotificationsAdapterNew(val mContext: Context, val mList: List<NotificationsListItem>) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

    private val fromClass = "NotificationsAdapterNew"

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as NotificationsRecyclerViewHolder

        if (mList[position].message != null && hasValue(mList[position].message)) {
            viewHolder.descriptionTV.text = mList[position].message
        }

        if (mList[position].datetime != null && hasValue(mList[position].datetime)) {
            viewHolder.timeTV.text = mList[position].datetime
            viewHolder.timeTV.isSelected = true
        }

        viewHolder.parentHolder.setOnClickListener {
            var actionTypeFromServer = ""
            if (mList[position].actionType != null && hasValue(mList[position].actionType)) {
                actionTypeFromServer = mList[position].actionType!!
            }

            var actionIdFromServer = ""
            if (mList[position].actionID != null && hasValue(mList[position].actionID)) {
                actionIdFromServer = mList[position].actionID!!
            }

            logE("notfIX", "id:::".plus(actionIdFromServer), fromClass, "click")
            logE("notfIX", "type:::".plus(actionTypeFromServer), fromClass, "click")

            if (actionTypeFromServer.equals(ConstantsUtility.NotificationsConstants.ACTION_TYPE_SUPPLEMENTARY_OFFERS, ignoreCase = true)) {
                logE("notfIX", "ACTION_TYPE_SUPPLEMENTARY_OFFERS", fromClass, "click")
                /**check if action id exists in the customer data
                 * special offers*/
                if (UserDataManager.getCustomerData() != null
                        && UserDataManager.getCustomerData()!!.specialOffersTariffData != null
                        && !UserDataManager.getCustomerData()!!.specialOffersTariffData!!.offersSpecialIds.isNullOrEmpty()) {
                    if (UserDataManager.getCustomerData()!!.specialOffersTariffData!!.offersSpecialIds!!.contains(actionIdFromServer)) {
                        /**goto special offers activity*/
                        SpecialOffersActivity.start(mContext, actionIdFromServer)
                        logE("notfIX", "is a special offer", fromClass, "click")
                    } else {
                        logE("notfIX", "offer id is null or empty", fromClass, "click")
                        /**goto supplementary offers activity*/
                        SupplementaryOffersActivity.start(mContext, actionIdFromServer)
                    }
                } else {
                    logE("notfIX", "nooot a special offer", fromClass, "click")
                    /**goto supplementary offers activity*/
                    SupplementaryOffersActivity.start(mContext, actionIdFromServer)
                }
            } else if (actionTypeFromServer.equals(ConstantsUtility.NotificationsConstants.ACTION_TYPE_TARIFFS_OFFERS, ignoreCase = true)) {
                logE("notfIX", "ACTION_TYPE_TARIFFS_OFFERS", fromClass, "click")
                /**goto tariff offers*/
                if (RootValues.getNotificationsCloserEvents() != null) {
                    RootValues.getNotificationsCloserEvents()!!.onCloseThisScreen()
                }

                if(hasValue(actionIdFromServer)){
                    UserDataManager.resetUserDataManager()
                    UserDataManager.setActionIdFromServer(actionIdFromServer)
                    MainActivity.start(mContext, "X1289Y")
                }
            } else {
                logE("notfIX", "INVALID TYPE", fromClass, "click")
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.notification_items_new, parent, false)
        return NotificationsRecyclerViewHolder(view)
    }

    private inner class NotificationsRecyclerViewHolder(row: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(row) {
        val ovalIV: ImageView = row.findViewById(R.id.ovalIV) as ImageView
        val descriptionTV: TextView = row.findViewById(R.id.descriptionTV) as TextView
        val timeTV: TextView = row.findViewById(R.id.timeTV) as TextView
        val parentHolder: ConstraintLayout = row.findViewById(R.id.parentHolder) as ConstraintLayout
    }//ServiceMenuRecyclerViewHolder ends
}