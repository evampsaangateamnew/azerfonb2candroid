package com.azarphone.ui.adapters.recyclerviews

import android.content.Context
import android.widget.Toast
import com.azarphone.api.pojo.helperpojo.menus.HorizontalChildMenusHelperModel
import com.azarphone.bases.BaseAdapter
import com.azarphone.R
import com.azarphone.ui.activities.exchangeservice.ExchangeServiceActivity
import com.azarphone.ui.activities.fnf.FnFActivity
import com.azarphone.ui.activities.freesms.FreeSmsActivity
import com.azarphone.ui.activities.loan.LoanActivity
import com.azarphone.ui.activities.moneyrequest.MoneyRequestActivity
import com.azarphone.ui.activities.moneytransfer.MoneyTransferActivity
import com.azarphone.ui.activities.mysubscriptions.MySubscriptionsActivity
import com.azarphone.ui.activities.nartv.NarTvActivity
import com.azarphone.ui.activities.operationshistory.OperationsHistoryActivity
import com.azarphone.ui.activities.specialoffers.SpecialOffersActivity
import com.azarphone.ui.activities.supplementaryoffers.SupplementaryOffersActivity
import com.azarphone.ui.activities.topup.TopupActivity
import com.azarphone.ui.activities.usagehistory.UsageHistoryActivity
import com.azarphone.ui.activities.vasservices.CoreServicesActivity
import com.azarphone.util.ConstantsUtility


class InnerMenuAdapter(val context: Context, val mLayoutID: Int, val mList: ArrayList<HorizontalChildMenusHelperModel>) : BaseAdapter() {

    override fun getObjForPosition(position: Int): Any {
        return mList[position]
    }

    override fun getLayoutIdForPosition(position: Int): Int {
        return mLayoutID
    }

    override fun getItemCount(): Int {
        return mList.size
    }


    fun onItemClicked(identifier: String) {
        if (identifier.equals(ConstantsUtility.HorizontalChildMenuIdentifiers.IDENTIFIER_TOPUP, ignoreCase = true)) {
            TopupActivity.start(context)
        } else if (identifier.equals(ConstantsUtility.HorizontalChildMenuIdentifiers.IDENTIFIER_LOAN, ignoreCase = true)) {
            LoanActivity.start(context)
        } else if (identifier.equals(ConstantsUtility.HorizontalChildMenuIdentifiers.IDENTIFIER_MONEY_TRANSFER, ignoreCase = true)) {
            MoneyTransferActivity.start(context)
        } else if (identifier.equals(ConstantsUtility.HorizontalChildMenuIdentifiers.IDENTIFIER_MONEY_REQUEST, ignoreCase = true)) {
            MoneyRequestActivity.start(context)
        }  else {
            Toast.makeText(context, context.resources.getString(R.string.no_data_found), Toast.LENGTH_SHORT).show()
        }
    }

}

