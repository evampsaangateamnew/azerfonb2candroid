package com.azarphone.ui.adapters.spinners

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.azarphone.R
import com.azarphone.api.pojo.response.predefinedata.PrepaidItem
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.getManatConcatedString


@Suppress("DEPRECATION")
class CreditAmountSpinnerAdapter : BaseAdapter {

    private var context: Context? = null
    private var spinnerItems: List<PrepaidItem?>? = null
    private var inflater: LayoutInflater? = null

    constructor(contxt: Context, spinnerValues: List<PrepaidItem?>?) {
        context = contxt
        spinnerItems = spinnerValues

        inflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun getCount(): Int {
        return spinnerItems!!.size
    }

    override fun getItem(position: Int): String? {
        return spinnerItems!![position]?.amount
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    @SuppressLint("InflateParams")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        val view: View?
        val viewHolder: ViewHolder
        if (convertView == null) {
            val inflater =
                context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.usage_history_period_spinner, null)
            viewHolder = ViewHolder(view)
            view?.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }//else ends

        viewHolder.periodTitle!!.visibility = View.VISIBLE
        viewHolder.periodTitle!!.isSelected = true
        viewHolder.periodTitle!!.text = spinnerItems!![position]?.amount?.let {
            getManatConcatedString(
                context!!, it,
                ConstantsUtility.PackagesConstants.AZERI_SYMBOL, 0.20f, R.dimen._10ssp
            )
        }
        viewHolder.periodTitleDropDown!!.visibility = View.GONE
        viewHolder.mainContainer!!.setBackgroundColor(context!!.resources.getColor(R.color.colorWhite))
        return view as View
    }//getView ends

    @SuppressLint("InflateParams")
    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        // This view starts when we click the spinner.
        val view: View?
        val viewHolder: ViewHolder
        if (convertView == null) {
            val inflater =
                context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.usage_history_period_spinner, null)
            viewHolder = ViewHolder(view)
            view?.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }//else ends

        //view handling
        viewHolder.periodTitleDropDown!!.visibility = View.VISIBLE
        viewHolder.periodTitleDropDown!!.isSelected = true
        viewHolder.periodTitleDropDown!!.text = spinnerItems!![position]?.amount
        viewHolder.periodTitle!!.visibility = View.GONE
        viewHolder.mainContainer!!.background =
            context!!.resources.getDrawable(R.drawable.spinner_item_selector_bg)

        return view as View
    }//getDropDownView ends

    private class ViewHolder(row: View?) {
        var periodTitle: TextView? = null
        var periodTitleDropDown: TextView? = null
        var mainContainer: ConstraintLayout? = null

        init {
            this.periodTitle = row?.findViewById(R.id.periodTitle) as TextView
            this.periodTitleDropDown = row.findViewById(R.id.periodTitleDropDown) as TextView
            this.mainContainer = row.findViewById(R.id.mainContainer) as ConstraintLayout
        }//init ends
    }// class ViewHolder ends

}
