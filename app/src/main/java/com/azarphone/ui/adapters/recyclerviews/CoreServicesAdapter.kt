package com.azarphone.ui.adapters.recyclerviews

import android.app.Activity
import android.content.Context
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import android.text.InputFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.response.coreservices.CoreServicesItem
import com.azarphone.api.pojo.response.coreservices.CoreServicesListItem
import com.azarphone.eventhandler.CoreServiceProcessEvents
import com.azarphone.eventhandler.CoreServicesCallDivertDisableEvents
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.validators.isNumeric
import com.azarphone.widgets.coreservices.CoreServicesViewItem

/**
 * @author Junaid Hassan on 02, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class CoreServicesAdapter(context: Context, activity: Activity,
                          coreServices: List<CoreServicesItem?>, isIcalledYouBack: Boolean,
                          coreServiceProcessEvents: CoreServiceProcessEvents,
                          coreServicesCallDivertDisableEvents: CoreServicesCallDivertDisableEvents) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

    private val fromClass = "CoreServicesAdapter"
    private var coreServiceProcessEvents: CoreServiceProcessEvents? = null
    private var context: Context? = null
    private var activity: Activity? = null
    private var coreServices: List<CoreServicesItem?>? = null
    private var isIcalledYouBack: Boolean? = null
    private var coreServicesCallDivertDisableEvents: CoreServicesCallDivertDisableEvents? = null

    init {
        this.coreServiceProcessEvents = coreServiceProcessEvents
        this.context = context
        this.activity = activity
        this.coreServices = coreServices
        this.isIcalledYouBack = isIcalledYouBack
        this.coreServicesCallDivertDisableEvents = coreServicesCallDivertDisableEvents
    }//init ends

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_core_services_adapter,
                parent, false)
        return CoreServicesViewHolder(view)
    }//onCreateViewHolder ends

    override fun getItemCount(): Int {
        return coreServices!!.size
    }//getItemCount ends

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as CoreServicesViewHolder

        if (coreServices != null) {

            /**populate the title sections of the core services*/
            if (coreServices!![position]!!.coreServiceCategory != null && hasValue(coreServices!![position]!!.coreServiceCategory)) {
                viewHolder.forwardNumberTitle!!.text = coreServices!![position]!!.coreServiceCategory
                viewHolder.forwardNumberTitle!!.isSelected = true

                //set the background
                if (position == 0) {
                    viewHolder.forwardNumberHeader!!.background = context!!.resources.getDrawable(R.drawable.subscriptions_header_bg)
                } else {
                    viewHolder.forwardNumberHeader!!.setBackgroundColor(context!!.resources.getColor(R.color.colorSideMenuBottomLines))
                }
            }//set core service title ends

            if (coreServices!![position]!!.coreServicesList != null && coreServices!![position]!!.coreServicesList!!.isNotEmpty()) {
                populateCoreServices(coreServices!![position]!!.coreServicesList!!, viewHolder, position)
            }//if (coreServices!![position]!!.coreServicesList != null && coreServices!![position]!!.coreServicesList!!.isNotEmpty()) ends
        }

    }//onBindViewHolder ends

    private fun populateCoreServices(coreServicesList: List<CoreServicesListItem?>, viewHolder: CoreServicesViewHolder, position: Int) {
        viewHolder.contentLayout!!.removeAllViews()
        /**loop through the core services and populate the ui*/
        for (its in coreServicesList.indices) {
            val coreServicesViewItem = CoreServicesViewItem(context!!, coreServiceProcessEvents!!, coreServicesCallDivertDisableEvents!!)

            /**check for call divert*/
            if (coreServicesList[its]!!.offeringId != null && hasValue(coreServicesList[its]!!.offeringId)) {
                if (coreServicesList[its]!!.offeringId == ConstantsUtility.CoreServicesConstants.CALL_FORWARD_OFFERING_ID) {
                    logE("coreServiceStatusX", "offeringId111:::".plus(coreServicesList[its]!!.offeringId), fromClass, "populateCoreServices")
                    prepareCallDivertCoreService(coreServicesList[its]!!, coreServicesViewItem)
                } else {
                    logE("coreServiceStatusX", "offeringId:::".plus(coreServicesList[its]!!.offeringId), fromClass, "populateCoreServices")
                    prepareVASServices(coreServicesList[its]!!, coreServicesViewItem)
                }

                //control the visibility of the view below the content
                if (coreServicesList.size == 1) {
                    coreServicesViewItem.getViewBelowContent().visibility = View.GONE
                    coreServicesViewItem.getEmptySpaceBelowDescriptionRow().visibility = View.VISIBLE
                } else {
                    if (its == coreServicesList.size - 1) {
                        coreServicesViewItem.getViewBelowContent().visibility = View.GONE
                        coreServicesViewItem.getEmptySpaceBelowDescriptionRow().visibility = View.VISIBLE
                    } else {
                        coreServicesViewItem.getViewBelowContent().visibility = View.VISIBLE
                        coreServicesViewItem.getEmptySpaceBelowDescriptionRow().visibility = View.GONE
                    }
                }
            }//if (coreServicesList[its]!!.offeringId != null && hasValue(coreServicesList[its]!!.offeringId)) ends

            viewHolder.contentLayout!!.addView(coreServicesViewItem)
        }//for ends
    }//populateCoreServices ends

    private fun prepareVASServices(vasService: CoreServicesListItem, coreServicesViewItem: CoreServicesViewItem) {
        //name
        if (vasService.name != null && hasValue(vasService.name)) {
            coreServicesViewItem.getCoreServiceNameLabel().text = vasService.name
            coreServicesViewItem.getCoreServiceNameLabel().filters = arrayOf(InputFilter.LengthFilter(100))//because forward number is hidden in case
            coreServicesViewItem.getCoreServiceNameLabel().isSelected = true
        }

        //description
        if (vasService.description != null && hasValue(vasService.description)) {
            coreServicesViewItem.getCoreServiceDescriptionLabel().text = vasService.description
        }

        //price
        if (vasService.price != null && hasValue(vasService.price)) {
            if (vasService.price == "0.0" || vasService.price == "0") {
                coreServicesViewItem.getCoreServicePriceLabel().text = getLocalizedFreeTextForVasServices()
                coreServicesViewItem.getCoreServicePriceLabel().isSelected = true
                coreServicesViewItem.getCoreServicePriceLabel().setTextColor(context!!.resources.getColor(R.color.colorPrimary))
            } else {
                //check numeric price
                if (isNumeric(vasService.price)) {
                    val price = vasService.price
                    coreServicesViewItem.getCoreServicePriceLabel().text = getManatConcatedString(
                            context!!, price, ConstantsUtility.CoreServicesConstants.AZERI_SYMBOL, 0.25f, R.dimen._10ssp)
                    coreServicesViewItem.getCoreServicePriceLabel().isSelected = true
                } else {
                    coreServicesViewItem.getCoreServicePriceLabel().text = vasService.price
                    coreServicesViewItem.getCoreServicePriceLabel().isSelected = true
                }
            }
        }

        var status = ""
        if (vasService.status != null && hasValue(vasService.status)) {
            status = vasService.status!!
        }

        //check or uncheck the description switch based on status
        coreServicesViewItem.getCoreServiceDescriptionSwitch().isChecked = status.equals(ConstantsUtility.CoreServicesConstants.STATUS_ACTIVE, ignoreCase = true)

        var offeringId = ""
        if (vasService.offeringId != null && hasValue(vasService.offeringId)) {
            offeringId = vasService.offeringId
        }

        if (status.equals(ConstantsUtility.CoreServicesConstants.STATUS_ACTIVE, ignoreCase = true) &&
                vasService.effectiveDate != null && hasValue(vasService.effectiveDate) &&
                vasService.expireDate != null && hasValue(vasService.expireDate) &&
                vasService.renewable != null && hasValue(vasService.renewable.toString())) {
            /**show the service section with the progress bar*/
            coreServicesViewItem.getCoreServiceProgressRow().visibility = View.VISIBLE
            coreServicesViewItem.getValidityLabel().visibility = View.GONE
            //calculate the progress
            val startMili = getMiliSecondsFromDateForVasServices(vasService.effectiveDate)
            var endMili = getMiliSecondsFromDateForVasServices(vasService.expireDate)
            endMili = getOneDayMinusForVasServices(endMili)
            val currentMili = getCurrentMiliSecondsForVasServices()

            var daysDiffCurrent = getDaysDifferenceBetweenMilisecondsForVasServices(startMili, currentMili)
            var daysDiffTotal = getDaysDifferenceBetweenMilisecondsForVasServices(startMili, endMili)

            if (daysDiffTotal <= 0) {
                daysDiffTotal = 1
                daysDiffCurrent = 1
            }

            coreServicesViewItem.getCoreServiceProgressBar().max = daysDiffTotal
            coreServicesViewItem.getCoreServiceProgressBar().progress = daysDiffCurrent

            var daysLeft = daysDiffTotal - daysDiffCurrent

            if (daysLeft < 0) {
                daysLeft = 0
            }
            coreServicesViewItem.getTitleRightAboveProgress().text = "$daysLeft".plus(" ").plus(getLocalizedDaysTextForVasServices(daysLeft))
            coreServicesViewItem.getTitleRightAboveProgress().isSelected = true

            if (vasService.progressTitle != null && hasValue(vasService.progressTitle)) {
                coreServicesViewItem.getTitleLeftAboveProgress().text = vasService.progressTitle
                coreServicesViewItem.getTitleLeftAboveProgress().isSelected = true
            }

            if (vasService.progressDateLabel != null && hasValue(vasService.progressDateLabel)) {
                val rightBelowLabel = vasService.progressDateLabel.plus(": ").plus(getDateAccordingToClientSide(vasService.expireDate))
                coreServicesViewItem.getTitleRightBelowProgress().text = rightBelowLabel
                coreServicesViewItem.getTitleRightBelowProgress().isSelected = true
            }
        } else {
            coreServicesViewItem.getCoreServiceProgressRow().visibility = View.GONE
            coreServicesViewItem.getValidityLabel().visibility=View.VISIBLE
            var validityLabel = ""
            if (vasService.validityLabel != null && hasValue(vasService.validityLabel)) {
                validityLabel = vasService.validityLabel
            }

            var validity = ""
            if (vasService.validity != null && hasValue(vasService.validity)) {
                validity = vasService.validity
            }

            if(hasValue(validity)){
                coreServicesViewItem.getValidityLabel().text = validityLabel.plus(": ").plus(validity)
                coreServicesViewItem.getValidityLabel().isSelected = true
            }else{
                coreServicesViewItem.getValidityLabel().visibility=View.GONE
            }

        }

        //set the offering id
        coreServicesViewItem.getOfferingIdLabel().text = offeringId

        //set the status
        coreServicesViewItem.getStatusLabel().text = status

        //show the price label
        coreServicesViewItem.getCoreServicePriceLabel().visibility = View.VISIBLE

        //hide the call divert switch
        coreServicesViewItem.getForwardNumberSwitch().visibility = View.GONE

        //hide the forward number
        coreServicesViewItem.getForwardToNumberLabel().visibility = View.GONE

    }//prepareVASServices ends

    private fun prepareCallDivertCoreService(callDivertItem: CoreServicesListItem, coreServicesViewItem: CoreServicesViewItem) {
        //name
//        if (callDivertItem.name != null && hasValue(callDivertItem.name)) {
        coreServicesViewItem.getCoreServiceNameLabel().text = getCoreServiceCallDivertSectionTitle()
        coreServicesViewItem.getCoreServiceNameLabel().isSelected = true
//        }

        //description
        if (callDivertItem.description != null && hasValue(callDivertItem.description)) {
            coreServicesViewItem.getCoreServiceDescriptionLabel().text = callDivertItem.description
        }

        //check status
        if (callDivertItem.status != null && hasValue(callDivertItem.status)) {
            if (callDivertItem.status.equals(ConstantsUtility.CoreServicesConstants.STATUS_ACTIVE, ignoreCase = true)) {
                logE("coreServiceStatusX", "status1:::".plus(callDivertItem.status), fromClass, "prepareCallDivertCoreService")
                coreServicesViewItem.getForwardNumberSwitch().isChecked = true
                coreServicesViewItem.getForwardNumberSwitch().isClickable = true
                coreServicesViewItem.getForwardNumberSwitch().isEnabled = true
            } else {
                logE("coreServiceStatusX", "status2:::".plus(callDivertItem.status), fromClass, "prepareCallDivertCoreService")
                coreServicesViewItem.getForwardNumberSwitch().isChecked = false
                coreServicesViewItem.getForwardNumberSwitch().isClickable = false
                coreServicesViewItem.getForwardNumberSwitch().isEnabled = false
            }

            //set the status
            coreServicesViewItem.getStatusLabel().text = callDivertItem.status
        } else {
            logE("coreServiceStatusX", "status0:::null", fromClass, "prepareCallDivertCoreService")
            coreServicesViewItem.getForwardNumberSwitch().isChecked = false
            coreServicesViewItem.getForwardNumberSwitch().isClickable = false
            coreServicesViewItem.getForwardNumberSwitch().isEnabled = false
        }

        //check the forwardNumber
        if (callDivertItem.forwardNumber != null && hasValue(callDivertItem.forwardNumber)) {
            logE("coreServiceStatusX", "forwardNo:::".plus(callDivertItem.forwardNumber), fromClass, "prepareCallDivertCoreService")
            coreServicesViewItem.getForwardToNumberLabel().text = callDivertItem.forwardNumber
            coreServicesViewItem.getForwardToNumberLabel().isSelected = true
        } else {
            //show not set label to the forward number
            logE("coreServiceStatusX", "forwardNo:::".plus(getForwardNumberNotSetLabel()), fromClass, "prepareCallDivertCoreService")
            coreServicesViewItem.getForwardToNumberLabel().text = getForwardNumberNotSetLabel()
            coreServicesViewItem.getForwardToNumberLabel().isSelected = true
        }

        var offeringId = ""
        if (callDivertItem.offeringId != null && hasValue(callDivertItem.offeringId)) {
            offeringId = callDivertItem.offeringId
        }
        //set the offering id
        coreServicesViewItem.getOfferingIdLabel().text = offeringId

        //show the forward number switch
        coreServicesViewItem.getForwardNumberSwitch().visibility = View.VISIBLE

        //hide the price
        coreServicesViewItem.getCoreServicePriceLabel().visibility = View.GONE

        //hide the description switch
        coreServicesViewItem.getCoreServiceDescriptionSwitch().visibility = View.GONE

        //hide the progress row
        coreServicesViewItem.getCoreServiceProgressRow().visibility = View.GONE

        //hide the view below content
        coreServicesViewItem.getViewBelowContent().visibility = View.GONE
    }//prepareCallDivertUI ends

    inner class CoreServicesViewHolder : androidx.recyclerview.widget.RecyclerView.ViewHolder {

        var forwardNumberTitle: TextView? = null
        var forwardNumberHeader: ConstraintLayout? = null
        var contentLayout: LinearLayout? = null

        constructor(row: View) : super(row) {
            contentLayout = row.findViewById(R.id.contentLayout) as LinearLayout
            forwardNumberTitle = row.findViewById(R.id.forwardNumberTitle) as TextView
            forwardNumberHeader = row.findViewById(R.id.forwardNumberHeader) as ConstraintLayout
        }//constructor
    }//view holder class ends

}//CoreServicesAdapter ends