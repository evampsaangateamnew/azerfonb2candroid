package com.azarphone.ui.adapters.recyclerviews

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.azarphone.R
import com.azarphone.api.pojo.response.predefinedata.CardType
import com.azarphone.util.OnCardTypeClicked
import com.azarphone.validators.hasValue
import java.util.*

class TopUpCardTypeDialogAdapter(val context: Context?, private val cardTypes: ArrayList<CardType?>, private val onCardTypeClicked: OnCardTypeClicked?) : RecyclerView.Adapter<TopUpCardTypeDialogAdapter.ViewHolder>() {
    private var selectedItem: Int = 0


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.rv_dialog_card_type_item,
                parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val cardType = cardTypes[position]
        if (cardType != null && hasValue(cardType.value)) {
            val card: String? = cardType.value
            holder.radioButton.isChecked = position == 0

            // Setting card Image
            if (card?.equals("master", ignoreCase = false) == true) {
                holder.ivCardType.setImageResource(R.drawable.ic_master_card)
            } else if (card?.equals("visa", ignoreCase = false) == true) {
                holder.ivCardType.setImageResource(R.drawable.ic_visa_card)
            }

            // Formatting string
            val str1 = card?.substring(0, 1)
            val str2 = card?.substring(1, card.length)
            holder.tvTitle.text = str1?.uppercase(Locale.ROOT) + str2
            holder.tvTitle.isSelected = true

            // Managing Radio button state
            holder.radioButton.isChecked = position == selectedItem

            //set Click listener to radioButton
            holder.llRoot.setOnClickListener { updateItemCheckState(position) }
        }

    } //end onBindViewHolder


    private fun updateItemCheckState(position: Int) {
        // managing  radio button state and click listener
        selectedItem = position
        notifyDataSetChanged()
        if (onCardTypeClicked != null) {
            cardTypes[position]?.let { onCardTypeClicked.onCardClicked(it.key) }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val radioButton: RadioButton = itemView.findViewById(R.id.rb_card_type)
        val llRoot: LinearLayout = itemView.findViewById(R.id.llRoot)
        val tvTitle: TextView = itemView.findViewById(R.id.tvTitle)
        val ivCardType: ImageView = itemView.findViewById(R.id.ivCardType)
    }

    override fun getItemCount(): Int {
        return cardTypes.size
    }
}
