package com.azarphone.ui.adapters.expandablelists


import android.content.Context
import androidx.core.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ImageView
import android.widget.TextView
import com.azarphone.R
import com.azarphone.api.pojo.response.usagehistoryresponse.SummaryListItem
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.getManatConcatedString
import com.azarphone.validators.hasValue

class UsageHistorySummaryAdapter(val context: Context, val summaryList: List<SummaryListItem?>) : BaseExpandableListAdapter() {
    override fun getGroup(p0: Int): SummaryListItem? {
        return summaryList[p0]
    }

    override fun isChildSelectable(p0: Int, p1: Int): Boolean {
        return false
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getGroupView(groupPostion: Int, isExpanded: Boolean, convertView: View?, p3: ViewGroup?): View {
        var convertView: View? = convertView

        val infalInflater = context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        convertView = infalInflater.inflate(R.layout.usage_history_summary_list_item_parent, null)


        convertView.findViewById<TextView>(R.id.type).text= summaryList[groupPostion]?.name
        convertView.findViewById<TextView>(R.id.type).isSelected=true
        convertView.findViewById<TextView>(R.id.usage).text=summaryList[groupPostion]?.totalUsage
        convertView.findViewById<TextView>(R.id.usage).isSelected=true
        var charge=""
        if(hasValue(summaryList[groupPostion]?.totalCharge)){
            charge=summaryList[groupPostion]?.totalCharge!!
            convertView.findViewById<TextView>(R.id.charged).text= getManatConcatedString(context,charge,ConstantsUtility.TariffConstants.AZERI_SYMBOL,0.20f,R.dimen._8ssp)
            convertView.findViewById<TextView>(R.id.charged).isSelected=true
        }

        if (isExpanded) {
            convertView.findViewById<ImageView>(R.id.icon_Plus).setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.faqminus))
        } else {
            convertView.findViewById<ImageView>(R.id.icon_Plus).setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.faqplus))
        }


        return convertView
    }

    override fun getChildrenCount(p0: Int): Int {
        if (summaryList[p0]?.records == null) {
            return 0
        } else {
            return summaryList[p0]?.records!!.size
        }

    }

    override fun getChild(parentPosition: Int, childPosititon: Int): Any {
        return summaryList[parentPosition]?.records!![childPosititon]!!
    }

    override fun getGroupId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getChildView(groupPostion: Int, childPosition: Int, p2: Boolean, convertView: View?, p4: ViewGroup?): View {
        var convertView = convertView
        val infalInflater = context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        convertView = infalInflater.inflate(R.layout.usage_history_summary_list_item_child, null)


        convertView.findViewById<TextView>(R.id.serviceType).text= summaryList[groupPostion]?.records!![childPosition]?.service_type
        convertView.findViewById<TextView>(R.id.serviceType).isSelected=true
        convertView.findViewById<TextView>(R.id.serviceUsage).text=summaryList[groupPostion]?.records!![childPosition]?.total_usage
        convertView.findViewById<TextView>(R.id.serviceUsage).isSelected=true

        if(hasValue(summaryList[groupPostion]?.records!![childPosition]?.chargeable_amount)){
            convertView.findViewById<TextView>(R.id.serviceCharged).text=getManatConcatedString(context,summaryList[groupPostion]?.records!![childPosition]?.chargeable_amount!!,ConstantsUtility.TariffConstants.AZERI_SYMBOL,0.20f,R.dimen._8ssp)
            convertView.findViewById<TextView>(R.id.serviceCharged).isSelected=true
        }


        return convertView
    }

    override fun getChildId(p0: Int, p1: Int): Long {
        return p0.toLong()
    }

    override fun getGroupCount(): Int {
        return summaryList.size
    }

}