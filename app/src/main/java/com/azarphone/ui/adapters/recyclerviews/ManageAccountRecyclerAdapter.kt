package com.azarphone.ui.adapters.recyclerviews

import android.view.View
import com.azarphone.R
import com.azarphone.api.pojo.response.customerdata.CustomerData
import com.azarphone.bases.BaseAdapter
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.ManageAccountEventsHandler
import com.azarphone.ui.adapters.MyViewHolder
import kotlinx.android.synthetic.main.manage_account_items.view.*

class ManageAccountRecyclerAdapter(var list: ArrayList<CustomerData>?,val listener: ManageAccountEventsHandler) :BaseAdapter(){
    override fun getObjForPosition(position: Int): Any {

          return list!!.get(position)
    }

    override fun getLayoutIdForPosition(position: Int): Int {
        return R.layout.manage_account_items
    }


    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, p1: Int) {
        val obj=getObjForPosition(p1)
        holder.bind(obj)
        if(UserDataManager.getCustomerData()?.msisdn.equals(list?.get(p1)?.msisdn)) {
            holder.itemView.ovalIV.visibility = View.VISIBLE
        }
        else{
            holder.itemView.ovalIV.visibility = View.INVISIBLE
        }
    }

    fun onItemClicked(customer: CustomerData) {

        listener.onSwitchNumberClick(customer)

        notifyDataSetChanged()
    }

    fun onDeleteClicked(customer: CustomerData) {
        if( UserDataManager.getCustomerData()?.msisdn!=customer.msisdn){
            list?.remove(customer)
            notifyDataSetChanged()
        }
        listener.onDeleteNumberClick(customer)

    }
}