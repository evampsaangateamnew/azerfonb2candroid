package com.azarphone.ui.adapters.expandablelists

import android.content.Context
import androidx.core.content.ContextCompat
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import com.azarphone.api.pojo.response.storelocator.StoresItem
import androidx.core.content.ContextCompat.getSystemService
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.TextView
import com.azarphone.R
import org.w3c.dom.Text


class StoreListAdapter(val context: Context, val stores: List<StoresItem>) : BaseExpandableListAdapter() {
    override fun getGroup(p0: Int): Any {
        return stores[p0]
    }

    override fun isChildSelectable(p0: Int, p1: Int): Boolean {
        return false
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getGroupView(groupPostion: Int, isExpanded: Boolean, convertView: View?, p3: ViewGroup?): View {
        var convertView: View? = convertView

        val infalInflater = context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        convertView = infalInflater.inflate(R.layout.store_title_item, null)


        convertView.findViewById<TextView>(R.id.storeTitleTV).text= stores[groupPostion].store_name

        convertView.findViewById<TextView>(R.id.addressTV).text=stores[groupPostion].address

        if (isExpanded) {
            convertView.findViewById<ImageView>(R.id.IconIV).setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.faqminus))
        } else {
            convertView.findViewById<ImageView>(R.id.IconIV).setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.faqplus))
        }


        return convertView
    }

    override fun getChildrenCount(p0: Int): Int {
        if (stores[p0].timing == null) {
            return 0
        } else {
            return stores[p0].timing!!.size
        }

    }

    override fun getChild(parentPosition: Int, childPosititon: Int): Any {
        return stores[parentPosition].timing!![childPosititon]
    }

    override fun getGroupId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getChildView(groupPostion: Int, childPosition: Int, p2: Boolean, convertView: View?, p4: ViewGroup?): View {
        var convertView = convertView
            val infalInflater = context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = infalInflater.inflate(R.layout.store_day_item, null)


        convertView.findViewById<TextView>(R.id.weekdayTV).text= stores[groupPostion].timing!![childPosition].day

        convertView.findViewById<TextView>(R.id.offOnStatus).text=stores[groupPostion].timing!![childPosition].timings

        return convertView
    }

    override fun getChildId(p0: Int, p1: Int): Long {
        return p0.toLong()
    }

    override fun getGroupCount(): Int {
        return stores.size
    }

}