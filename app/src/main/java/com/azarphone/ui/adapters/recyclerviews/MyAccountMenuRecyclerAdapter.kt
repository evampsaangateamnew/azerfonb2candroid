package com.azarphone.ui.adapters.recyclerviews

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.menus.HorizontalChildMenusHelperModel
import com.azarphone.ui.activities.mysubscriptions.MySubscriptionsActivity
import com.azarphone.ui.activities.operationshistory.OperationsHistoryActivity
import com.azarphone.ui.activities.usagehistory.UsageHistoryActivity
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.logE
import com.azarphone.validators.hasValue

/**
 * @author Junaid Hassan on 21, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class MyAccountMenuRecyclerAdapter(
    val mContext: Context,
    val mList: ArrayList<HorizontalChildMenusHelperModel>,
    val matchedSubScreen: String,
    val tabToRedirect: String,
    val offeringIdToRedirect: String
) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

    private val fromClass = MyAccountMenuRecyclerAdapter::class.simpleName.toString()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        p1: Int
    ): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.child_menus_row_item, parent, false)
        return MyAccountMenuRecyclerViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: androidx.recyclerview.widget.RecyclerView.ViewHolder,
        position: Int
    ) {
        val viewHolder = holder as MyAccountMenuRecyclerViewHolder

        viewHolder.titleTV.isSelected = true

        when {
            mList[position].identifier.equals(
                ConstantsUtility.MyAccountChildMenusIdentifiers.MYACCOUNT_USAGE_HISTORY,
                ignoreCase = true
            ) -> {
                viewHolder.menuIcon.setImageResource(R.drawable.usage_history)
                viewHolder.titleTV.text = mList[position].title
            }
            mList[position].identifier.equals(
                ConstantsUtility.MyAccountChildMenusIdentifiers.MYACCOUNT_OPERATIONS_HISTORY,
                ignoreCase = true
            ) -> {
                viewHolder.menuIcon.setImageResource(R.drawable.operations_history)
                viewHolder.titleTV.text = mList[position].title
            }
            mList[position].identifier.equals(
                ConstantsUtility.MyAccountChildMenusIdentifiers.MYACCOUNT_MY_SUBSCRIPTION,
                ignoreCase = true
            ) -> {
                viewHolder.menuIcon.setImageResource(R.drawable.my_subscriptions)
                viewHolder.titleTV.text = mList[position].title
            }
            else -> {
                viewHolder.menuIcon.setImageResource(R.drawable.ic_warn_oval)
                viewHolder.titleTV.text = mList[position].title
            }
        }

        if (position == mList.size - 1) {
            /** If last index is being populated then
            Redirecting to Dynamic Link Destination    */
            manageDynamicLinkRedirection()
        }

        initUIEvents(viewHolder, position)
    }//onBindViewHolder ends

    private fun initUIEvents(viewHolder: MyAccountMenuRecyclerViewHolder, position: Int) {
        //Applying click listener on recyclerview items
        viewHolder.centerContentCV.setOnClickListener {
            when {
                mList[position].identifier.equals(
                    ConstantsUtility.MyAccountChildMenusIdentifiers.MYACCOUNT_USAGE_HISTORY,
                    ignoreCase = true
                ) -> UsageHistoryActivity.start(mContext)
                mList[position].identifier.equals(
                    ConstantsUtility.MyAccountChildMenusIdentifiers.MYACCOUNT_OPERATIONS_HISTORY,
                    ignoreCase = true
                ) -> OperationsHistoryActivity.start(mContext)
                mList[position].identifier.equals(
                    ConstantsUtility.MyAccountChildMenusIdentifiers.MYACCOUNT_MY_SUBSCRIPTION,
                    ignoreCase = true
                ) -> MySubscriptionsActivity.start(mContext)
                else -> Toast.makeText(
                    mContext,
                    mContext.resources.getString(R.string.no_data_found),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }//viewHolder.centerContentCV.setOnClickListener
    }//initUIEvents ends

    private fun manageDynamicLinkRedirection() {
        logE(
            ConstantsUtility.DynamicLinkConstants.DYNAMIC_LINK_TAG,
            " $matchedSubScreen -- $offeringIdToRedirect,  $tabToRedirect",
            fromClass,
            "manageDynamicLinkRedirection"
        )
        if (hasValue(matchedSubScreen)) {
            when (matchedSubScreen) {
                ConstantsUtility.MyAccountChildMenusIdentifiers.MYACCOUNT_USAGE_HISTORY -> UsageHistoryActivity.start(
                    mContext
                )
                ConstantsUtility.MyAccountChildMenusIdentifiers.MYACCOUNT_OPERATIONS_HISTORY -> OperationsHistoryActivity.start(
                    mContext
                )
                ConstantsUtility.MyAccountChildMenusIdentifiers.MYACCOUNT_MY_SUBSCRIPTION -> MySubscriptionsActivity.start(
                    mContext, offeringIdToRedirect, tabToRedirect
                )
            }
        }
    }//manageDynamicLinkRedirection ends

    override fun getItemCount(): Int {
        return mList.size
    }//getItemCount ends

    private inner class MyAccountMenuRecyclerViewHolder(row: View) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(row) {
        var centerContentCV: androidx.cardview.widget.CardView =
            row.findViewById(R.id.centerContentCV) as androidx.cardview.widget.CardView
        var menuIcon: ImageView = row.findViewById(R.id.menuIcon) as ImageView
        var titleTV: TextView = row.findViewById(R.id.titleTV) as TextView
    }//ServiceMenuRecyclerViewHolder ends

}//class ends