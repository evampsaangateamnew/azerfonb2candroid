package com.azarphone.ui.adapters.pagers

import androidx.fragment.app.FragmentPagerAdapter

/**
 * @author Junaid Hassan on 04, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class InternetPagerAdapter(
    fm: androidx.fragment.app.FragmentManager,
    private val fragmentsList: List<androidx.fragment.app.Fragment>
) : FragmentPagerAdapter(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    init {
    }


    override fun getCount(): Int {
        return fragmentsList.size
    }//getCount ends

    override fun getItem(position: Int): androidx.fragment.app.Fragment {
        return when (position) {
            0 -> fragmentsList[0]
            1 -> fragmentsList[1]
            2 -> fragmentsList[2]
            3 -> fragmentsList[3]
            4 -> fragmentsList[4]
            else -> fragmentsList[1]
        }//when ends
    }//getItem ends
}