package com.azarphone.ui.adapters.expandablelists

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.TextView
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.response.requesthistory.LoanRequestItem
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.getManatConcatedString
import com.azarphone.util.logE
import com.azarphone.validators.hasValue

/**
 * @author Junaid Hassan on 01, July, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class LoanRequestHistoryAdapter(val mContext: Context, val activity: Activity, val loanRequest: List<LoanRequestItem??>) : BaseExpandableListAdapter() {

    private val fromClass = "LoanRequestHistoryAdapter"

    @SuppressLint("InflateParams")
    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup): View {
        val view: View?
        val viewHolderGroup: GroupViewHolder
        if (convertView == null) {
            val inflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.request_history_adapter_items, null)
            viewHolderGroup = GroupViewHolder(view)
            view?.tag = viewHolderGroup
        }//if ends
        else {
            view = convertView
            viewHolderGroup = view.tag as GroupViewHolder
        }//else ends

        //ui handling here
        if (hasValue(loanRequest[groupPosition]!!.dateTime)) {
            logE("adapterValue", "dateTime:::".plus(loanRequest[groupPosition]!!.dateTime), fromClass, "subscribe")
            viewHolderGroup.dateTimeValue.text = loanRequest[groupPosition]!!.dateTime
        }

        if (hasValue(loanRequest[groupPosition]!!.status)) {
            viewHolderGroup.statusValue.isSelected = true
            viewHolderGroup.statusValue.text = loanRequest[groupPosition]!!.status
        }

        if (hasValue(loanRequest[groupPosition]!!.remaining)) {
            val remainVal = loanRequest[groupPosition]!!.remaining
            viewHolderGroup.remainingValue.isSelected = true
            viewHolderGroup.remainingValue.text = getManatConcatedString(mContext, remainVal!!,
                    ConstantsUtility.TariffConstants.AZERI_SYMBOL, 0.25f, R.dimen._8ssp)
        }

        if (hasValue(loanRequest[groupPosition]!!.paid)) {
            val paidAmount = loanRequest[groupPosition]!!.paid
            viewHolderGroup.paidValue.isSelected = true
            viewHolderGroup.paidValue.text = getManatConcatedString(mContext, paidAmount!!,
                    ConstantsUtility.TariffConstants.AZERI_SYMBOL, 0.25f, R.dimen._8ssp)
        }
        return view as View
    }//getGroupView ends

    @SuppressLint("InflateParams")
    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
        val view: View?
        val viewHolderChild: ChildViewHolder

        if (convertView == null) {
            val inflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.adapter_item_childs_operation_history, null)
            viewHolderChild = ChildViewHolder(view)
            view?.tag = viewHolderChild
        } else {
            view = convertView
            viewHolderChild = view.tag as ChildViewHolder
        }

        viewHolderChild.endingBalanceTitle.text = getLocalizedTitle()
        viewHolderChild.endingBalanceTitle.isSelected = true
        //ui handling here
        if (hasValue(loanRequest[groupPosition]!!.loanID)) {
            val loanID = loanRequest[groupPosition]!!.loanID
            viewHolderChild.endingBalanceValue.isSelected = true
            viewHolderChild.endingBalanceValue.text = loanID
        }

        return view as View
    }//getChildView ends

    private fun getLocalizedTitle(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "Request number"
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Sorğunun nömrəsi"
            else -> "Номер запроса"
        }
    }

    private class GroupViewHolder(row: View) {
        var dateTimeValue: TextView = row.findViewById(R.id.dateTimeValue) as TextView
        var statusValue: TextView = row.findViewById(R.id.statusValue) as TextView
        var paidValue: TextView = row.findViewById(R.id.paidValue) as TextView
        var remainingValue: TextView = row.findViewById(R.id.remainingValue) as TextView
    }// class GroupViewHolder ends

    private class ChildViewHolder(row: View) {
        val endingBalanceValue: TextView = row.findViewById(R.id.endingBalanceValue) as TextView
        val endingBalanceTitle: TextView = row.findViewById(R.id.endingBalanceTitle) as TextView
    }// class ChildViewHolder ends

    override fun getChild(groupPosition: Int, childPosititon: Int): String {
        return this.loanRequest[groupPosition]!!.loanID!!
    }//getChild ends

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }//getChildId ends

    override fun getChildrenCount(groupPosition: Int): Int {
        return 1
    }//getChildrenCount ends

    override fun getGroup(groupPosition: Int): Any {
        return this.loanRequest[groupPosition]!!
    }//getGroup ends

    override fun getGroupCount(): Int {
        return this.loanRequest.size
    }//getGroupCount ends

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }//getGroupId ends

    override fun hasStableIds(): Boolean {
        return false
    }//hasStableIds ends

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }//isChildSelectable ends
}