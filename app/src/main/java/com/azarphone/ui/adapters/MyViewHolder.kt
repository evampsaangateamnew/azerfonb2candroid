package com.azarphone.ui.adapters

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.azarphone.BR
import com.azarphone.bases.BaseAdapter
import java.util.*

class MyViewHolder(val viewDataBinding: ViewDataBinding) : androidx.recyclerview.widget.RecyclerView.ViewHolder(viewDataBinding.root){

    fun bind(obj: Any){
        viewDataBinding.setVariable(BR.obj,obj)
        viewDataBinding.executePendingBindings()
    }

    fun bind(adapter: BaseAdapter) {
        viewDataBinding.setVariable(BR.adapter, adapter)
        viewDataBinding.executePendingBindings()
    }

}