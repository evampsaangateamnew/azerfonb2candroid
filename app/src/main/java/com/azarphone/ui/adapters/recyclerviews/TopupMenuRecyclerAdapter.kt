package com.azarphone.ui.adapters.recyclerviews

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.menus.HorizontalChildMenusHelperModel
import com.azarphone.ui.activities.autopayment.AutoPaymentActivity
import com.azarphone.ui.activities.fasttopup.FastTopUpActivity
import com.azarphone.ui.activities.loan.LoanActivity
import com.azarphone.ui.activities.moneyrequest.MoneyRequestActivity
import com.azarphone.ui.activities.moneytransfer.MoneyTransferActivity
import com.azarphone.ui.activities.topup.TopupActivity
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.logE
import com.azarphone.validators.hasValue

/**
 * @author Junaid Hassan on 21, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class TopupMenuRecyclerAdapter(
    val mContext: Context,
    val mList: ArrayList<HorizontalChildMenusHelperModel>,
    val matchedSubScreen: String,
    val tabToRedirect: String,
    val offeringIdToRedirect: String
) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

    private val fromClass = TopupMenuRecyclerAdapter::class.simpleName.toString()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        p1: Int
    ): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.child_menus_row_item, parent, false)
        return TopupMenuRecyclerViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: androidx.recyclerview.widget.RecyclerView.ViewHolder,
        position: Int
    ) {
        val viewHolder = holder as TopupMenuRecyclerViewHolder

        viewHolder.titleTV.isSelected = true

        when {
            mList[position].identifier.equals(
                ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_TOPUP,
                ignoreCase = true
            ) -> {
                viewHolder.menuIcon.setImageResource(R.drawable.top_up)
                viewHolder.titleTV.text = mList[position].title
            }
            mList[position].identifier.equals(
                ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_LOAN,
                ignoreCase = true
            ) -> {
                viewHolder.menuIcon.setImageResource(R.drawable.loan)
                viewHolder.titleTV.text = mList[position].title
            }
            mList[position].identifier.equals(ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_MONEY_TRANSFER, ignoreCase = true) -> {
                viewHolder.menuIcon.setImageResource(R.drawable.money_transfer)
                viewHolder.titleTV.text = mList[position].title
            }
            mList[position].identifier.equals(
                ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_MONEY_REQUEST,
                ignoreCase = true
            ) -> {
                viewHolder.menuIcon.setImageResource(R.drawable.money_request)
                viewHolder.titleTV.text = mList[position].title
            }
            mList[position].identifier.equals(
                ConstantsUtility.TopupChildMenusIdentifiers.Fast_TOPUP,
                ignoreCase = true
            ) -> {
                viewHolder.menuIcon.setImageResource(R.drawable.ic_fast_topup)
                viewHolder.titleTV.text = mList[position].title
            }
            mList[position].identifier.equals(
                ConstantsUtility.TopupChildMenusIdentifiers.AUTO_PAYMENT,
                ignoreCase = true
            ) -> {
                viewHolder.menuIcon.setImageResource(R.drawable.ic_auto_payment)
                viewHolder.titleTV.text = mList[position].title
            }
            else -> {
                viewHolder.menuIcon.setImageResource(R.drawable.ic_warn_oval)
                viewHolder.titleTV.text = mList[position].title
            }
        }

        if (position == mList.size - 1) {
            /** If last index is being populated then
            Redirecting to Dynamic Link Destination    */
            manageDynamicLinkRedirection()
        }

        initUIEvents(viewHolder, position)
    }//onBindViewHolder ends

    private fun initUIEvents(viewHolder: TopupMenuRecyclerViewHolder, position: Int) {
        //Applying click listener on recyclerview items
        viewHolder.centerContentCV.setOnClickListener {
            when {
                mList[position].identifier.equals(
                    ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_TOPUP,
                    ignoreCase = true
                ) -> TopupActivity.start(mContext)
                mList[position].identifier.equals(
                    ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_LOAN,
                    ignoreCase = true
                ) -> LoanActivity.start(mContext)
                mList[position].identifier.equals(
                    ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_MONEY_TRANSFER,
                    ignoreCase = true
                ) -> MoneyTransferActivity.start(mContext)
                mList[position].identifier.equals(
                    ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_MONEY_REQUEST,
                    ignoreCase = true
                ) -> MoneyRequestActivity.start(mContext)
                mList[position].identifier.equals(
                    ConstantsUtility.TopupChildMenusIdentifiers.Fast_TOPUP,
                    ignoreCase = true
                ) -> FastTopUpActivity.start(mContext)
                mList[position].identifier.equals(
                    ConstantsUtility.TopupChildMenusIdentifiers.AUTO_PAYMENT,
                    ignoreCase = true
                ) -> AutoPaymentActivity.start(mContext)
                else -> Toast.makeText(
                    mContext,
                    mContext.resources.getString(R.string.no_data_found),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }//viewHolder.centerContentCV.setOnClickListener
    }//initUIEvents ends

    private fun manageDynamicLinkRedirection() {
        logE(
            ConstantsUtility.DynamicLinkConstants.DYNAMIC_LINK_TAG,
            " $matchedSubScreen",
            fromClass,
            "manageDynamicLinkRedirection"
        )
        if (hasValue(matchedSubScreen)) {
            when (matchedSubScreen) {
                ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_MONEY_TRANSFER -> MoneyTransferActivity.start(
                    mContext
                )
                ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_TOPUP -> TopupActivity.start(
                    mContext
                )
                ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_LOAN -> LoanActivity.start(
                    mContext
                )
                ConstantsUtility.TopupChildMenusIdentifiers.TOPUP_MONEY_REQUEST -> MoneyRequestActivity.start(
                    mContext
                )
                ConstantsUtility.TopupChildMenusIdentifiers.Fast_TOPUP -> FastTopUpActivity.start(
                    mContext
                )
                ConstantsUtility.TopupChildMenusIdentifiers.AUTO_PAYMENT -> AutoPaymentActivity.start(
                    mContext
                )
            }
        }
    }//manageDynamicLinkRedirection ends

    override fun getItemCount(): Int {
        return mList.size
    }//getItemCount ends

    private inner class TopupMenuRecyclerViewHolder(row: View) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(row) {
        var centerContentCV: androidx.cardview.widget.CardView =
            row.findViewById(R.id.centerContentCV) as androidx.cardview.widget.CardView
        var menuIcon: ImageView = row.findViewById(R.id.menuIcon) as ImageView
        var titleTV: TextView = row.findViewById(R.id.titleTV) as TextView
    }//ServiceMenuRecyclerViewHolder ends

}//class ends