package com.azarphone.ui.adapters.pagers

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.azarphone.ui.fragment.loan.LoanFragment
import com.azarphone.ui.fragment.loan.PaymentFragment
import com.azarphone.ui.fragment.loan.RequestFragment

/**
 * @author Junaid Hassan on 05, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class LoanPagerAdapter(fm: androidx.fragment.app.FragmentManager) : FragmentStatePagerAdapter(fm) {
    private val fragmentsList = listOf(LoanFragment(), RequestFragment(), PaymentFragment())

    override fun getCount(): Int {
        return fragmentsList.size
    }//getCount ends

    override fun getItem(position: Int): androidx.fragment.app.Fragment {
        return when (position) {
            0 -> fragmentsList[0]
            1 -> fragmentsList[1]
            2 -> fragmentsList[2]
            else -> fragmentsList[0]
        }//when ends
    }//getItem ends
}