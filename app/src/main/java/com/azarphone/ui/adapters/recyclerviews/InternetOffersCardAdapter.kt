package com.azarphone.ui.adapters.recyclerviews

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewStub
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import com.azarphone.R
import com.azarphone.api.pojo.response.inappsurvey.InAppSurvey
import com.azarphone.api.pojo.response.inappsurvey.Surveys
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.internetpackresponse.Offer
import com.azarphone.api.pojo.response.supplementaryoffers.helper.SupplementryOffersDetailsAndDescription
import com.azarphone.api.pojo.response.supplementaryoffers.helper.SupplementryOffersHeaders
import com.azarphone.api.pojo.response.supplementaryoffers.response.*
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.OffersSearchResultListener
import com.azarphone.eventhandler.OnClickSurveySubmit
import com.azarphone.eventhandler.UpdateListOnSuccessfullySurvey
import com.azarphone.ui.activities.supplementaryoffers.SupplementaryOffersViewModel
import com.azarphone.ui.activities.topup.TopupActivity
import com.azarphone.ui.dialogs.InAppFeedbackDialog
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.validators.isNumeric
import com.azarphone.widgets.supplementaryoffers.*
import com.es.tec.LocalSharedPrefStorage
import net.cachapa.expandablelayout.ExpandableLayout
import java.util.*


/**
 * @author Junaid Hassan on 22, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class InternetOffersCardAdapter(
    context: Context,
    supplementaryOffersList: ArrayList<Offer?>?,
    isRoaming: Boolean,
    selectedCountryName: String,
    selectedCountryFlag: String,
    isSpecialOffers: Boolean,
    offersSearchResultListener: OffersSearchResultListener,
    pageTitle: String,
    private val viewModel: SupplementaryOffersViewModel
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>(),
    Filterable {

    private var isHeaderLayoutExpanded: Boolean = false
    private var isDetailsLayoutExpanded: Boolean = false

    private var isDetailsRoundingView = false
    private var isDetailsTextWithTitleView = false
    private var isDetailsTextWithoutTitleView = false
    private var isDetailsTextWithPointsView = false
    private var isRoamingView = false

    private lateinit var detailsView: View
    private val fromClass = "SupplementaryOffersCardAdapter"
    private var context: Context? = null
    private val HANDLER_TIME = 150L
    private var pageTitle = ""
    private var supplementaryOffersList: ArrayList<Offer?>? = null
    private var offerArrayListTemp: ArrayList<Offer?>? = null
    private var isRoaming: Boolean = false
    private var selectedCountryName = ""
    private var selectedCountryFlag = ""
    private var supplementaryFilter: SupplementaryFilter? = null
    private var isSpecialOffers = false
    private var offersSearchResultListener: OffersSearchResultListener? = null

    init {
        this.pageTitle = pageTitle
        this.offersSearchResultListener = offersSearchResultListener
        this.isSpecialOffers = isSpecialOffers
        this.selectedCountryFlag = selectedCountryFlag
        this.selectedCountryName = selectedCountryName
        this.isRoaming = isRoaming
        this.context = context
        this.supplementaryOffersList = supplementaryOffersList
        offerArrayListTemp = supplementaryOffersList
    }//init ends

    override fun getFilter(): Filter {
        if (supplementaryFilter == null) {
            supplementaryFilter = SupplementaryFilter()
        }
        return supplementaryFilter!!
    }//getFilter ends

    override fun onCreateViewHolder(
        parent: ViewGroup,
        p1: Int
    ): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_supplementary_offers_card_adapter, parent, false)
        return SupplementaryOffersCardAdapterViewHolder(view)
    }//onCreateViewHolder ends

    override fun getItemCount(): Int {
        return supplementaryOffersList!!.size
    }//getItemCount ends

    override fun onBindViewHolder(
        holder: androidx.recyclerview.widget.RecyclerView.ViewHolder,
        position: Int
    ) {
        var rateStars = 0

        val viewHolder = holder as SupplementaryOffersCardAdapterViewHolder

        /**button texts*/
        viewHolder.topUpButton!!.text = getTopupLabel()
        viewHolder.subscribeButton!!.text = getSubscribeButtonLabel()
        viewHolder.renewButton!!.text = getRenewButtonLabel()
        viewHolder.subscribedButton!!.text = getSubscribedButtonLabel()

        /**buttons marquee*/
        viewHolder.subscribeButton!!.isSelected = true
        viewHolder.topUpButton!!.isSelected = true
        viewHolder.renewButton!!.isSelected = true
        viewHolder.subscribedButton!!.isSelected = true

        viewHolder.detailsLayoutTitleLabel!!.text = getLocalizedDetailsTabTitle()
        if (supplementaryOffersList != null && supplementaryOffersList!!.isNotEmpty()) {
            /**create the package card header here*/
            if (supplementaryOffersList!![position]!!.header != null) {
                preparePackageCardHeader(supplementaryOffersList!![position]!!.header!!, viewHolder)
            } else {
                viewHolder.packageNameValidityHolderLayout!!.visibility = View.GONE
            }//if (supplementaryOffersList!![position]!!.header != null) ends

            /**create details card here*/
            if (supplementaryOffersList!![position]!!.details != null) {
                Handler().postDelayed({
                    viewHolder.detailsViewStub!!.layoutResource =
                        R.layout.layout_packages_details_content_layout

                    if (viewHolder.detailsViewStub!!.parent != null) {
                        detailsView = viewHolder.detailsViewStub!!.inflate()
                    }
                    if (position < supplementaryOffersList?.size!!)
                        setupDetailsSection(
                            detailsView,
                            supplementaryOffersList!![position]?.details!!
                        )
                }, HANDLER_TIME)
            } else {
                viewHolder.detailsLayout!!.visibility = View.GONE
            }
        }//if (supplementaryOffersList != null && !supplementaryOffersList!!.isEmpty()) ends

        /**set the expandable layouts behavior*/
        initExpandableLayoutsBehavior(position, viewHolder)

        initUIEvents(viewHolder, position)

        /**set the card height*/
        if (isRoaming) {
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) {
                val params = viewHolder.mainContainer!!.layoutParams
                params.height = context!!.resources.getDimensionPixelSize(R.dimen._400sdp)
                viewHolder.mainContainer!!.layoutParams = params
            } else {
                val params = viewHolder.mainContainer!!.layoutParams
                params.height = context!!.resources.getDimensionPixelSize(R.dimen._420sdp)
                viewHolder.mainContainer!!.layoutParams = params
            }
        } else {
            /**check if isSpecialOffers*/
            if (isSpecialOffers) {
                val params = viewHolder.mainContainer!!.layoutParams
                params.height = context!!.resources.getDimensionPixelSize(R.dimen._425sdp)
                viewHolder.mainContainer!!.layoutParams = params
            } else {
                if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) {
                    val params = viewHolder.mainContainer!!.layoutParams
                    params.height = context!!.resources.getDimensionPixelSize(R.dimen._360sdp)
                    viewHolder.mainContainer!!.layoutParams = params
                } else {
                    val params = viewHolder.mainContainer!!.layoutParams
                    params.height = context!!.resources.getDimensionPixelSize(R.dimen._380sdp)
                    viewHolder.mainContainer!!.layoutParams = params
                }
            }
        }


        /** setting stars of rating*/
        val surveyData = UserDataManager.getInAppSurvey()?.data
        val findSurvey =
            surveyData?.findSurvey(ConstantsUtility.InAppSurveyConstants.BUNDLE_SUBSCRIBED_PAGE)
        findSurvey?.let {
            surveyData.userSurveys?.let { userSurveyData ->
                for (i in userSurveyData.indices) {
                    if (userSurveyData[i].offeringId!! == supplementaryOffersList!![position]!!.header!!.offeringId!!) {
                        userSurveyData[i].answer?.let {
                            userSurveyData[i].surveyId?.let { userSurveyId ->
                                val survey =
                                    surveyData.findSurveyById(userSurveyId.toIntOrNull() ?: 0)
                                survey?.let { requiredSurvey ->
                                    val answerId = userSurveyData[i].answerId
                                    val questionId = userSurveyData[i].questionId
                                    if (isNumeric(answerId)) {
                                        if (isNumeric(questionId)) {
                                            rateStars = setStars(
                                                answerId!!.toInt(),
                                                questionId!!.toInt(),
                                                requiredSurvey,
                                                viewHolder
                                            )
                                        }
                                    }
                                }
                            }
                        } ?: run {
                            rateStars = setStars(0, 0, null, viewHolder)
                        }
                    }
                }
            }
        } ?: run {
            viewHolder.ratingStarsLayout?.visibility = View.GONE
        }

        viewHolder.ratingStarsLayout?.setOnClickListener {
            val tariffId = supplementaryOffersList!![position]!!.header!!.offeringId!!
            context?.let {
                inAppFeedback(it, tariffId, rateStars, position)
            }

        }


    }//onBindViewHolder ends

    private fun inAppFeedback(context: Context, tariffId: String, rating: Int, position: Int) {

        var inAppFeedbackDialog: InAppFeedbackDialog? = null
        var currentVisit: Int =
            LocalSharedPrefStorage(context).getInt(LocalSharedPrefStorage.PREF_BUNDLE_SUBSCRIBED_PAGE)
        currentVisit += 1
        val inAppSurvey: InAppSurvey? = UserDataManager.getInAppSurvey()
        if ((inAppSurvey?.data) != null) {
            val surveys: Surveys? =
                inAppSurvey.data.findSurvey(ConstantsUtility.InAppSurveyConstants.BUNDLE_SUBSCRIBED_PAGE)
            if (surveys != null) {
                inAppFeedbackDialog = InAppFeedbackDialog(context, object : OnClickSurveySubmit {
                    override fun onClickSubmit(
                        uploadSurvey: UploadSurvey,
                        onTransactionComplete: String,
                        updateListOnSuccessfullySurvey: UpdateListOnSuccessfullySurvey?
                    ) {
                        viewModel.uploadInAppSurvey(
                            context,
                            uploadSurvey,
                            onTransactionComplete,
                            updateListOnSuccessfullySurvey
                        )
                        if (inAppFeedbackDialog?.isShowing == true) {
                            inAppFeedbackDialog?.dismiss()
                        }
                    }

                })
                if (surveys.questions != null) {
                    currentVisit = 0
                    inAppFeedbackDialog.setUpdateListOnSuccessfullySurveyListener(object :
                        UpdateListOnSuccessfullySurvey {
                        override fun updateListOnSuccessfullySurvey() {
                            updateList(position)
                        }
                    })
                    // 1 for tariff and 2 for supplementary
                    inAppFeedbackDialog.showTariffDialog(surveys, "2", tariffId, rating)
                }
            }
        }
        LocalSharedPrefStorage(context).addInt(
            LocalSharedPrefStorage.PREF_BUNDLE_SUBSCRIBED_PAGE,
            currentVisit
        )
    }

    private fun updateList(position: Int) {
        notifyItemChanged(position)
    }

    private fun setStars(
        answerId: Int,
        questionId: Int,
        survey: Surveys?,
        viewHolder: SupplementaryOffersCardAdapterViewHolder
    ): Int {
        survey?.let { currentSurvey ->
            val question = currentSurvey.findQuestion(questionId)
            question?.let { currentQuestion ->
                val answers = currentQuestion.answers
                answers?.let { currentAnswer ->
                    if (currentAnswer.isNotEmpty() && currentAnswer[0].id == answerId) {
                        viewHolder.starOne?.isChecked = true
                        viewHolder.starTwo?.isChecked = false
                        viewHolder.starThree?.isChecked = false
                        viewHolder.starFour?.isChecked = false
                        viewHolder.starFive?.isChecked = false
                        return 1
                    } else if (currentAnswer.size > 1 && currentAnswer[1].id == answerId) {
                        viewHolder.starOne?.isChecked = true
                        viewHolder.starTwo?.isChecked = true
                        viewHolder.starThree?.isChecked = false
                        viewHolder.starFour?.isChecked = false
                        viewHolder.starFive?.isChecked = false
                        return 2
                    } else if (currentAnswer.size > 2 && currentAnswer[2].id == answerId) {
                        viewHolder.starOne?.isChecked = true
                        viewHolder.starTwo?.isChecked = true
                        viewHolder.starThree?.isChecked = true
                        viewHolder.starFour?.isChecked = false
                        viewHolder.starFive?.isChecked = false
                        return 3
                    } else if (currentAnswer.size > 3 && currentAnswer[3].id == answerId) {
                        viewHolder.starOne?.isChecked = true
                        viewHolder.starTwo?.isChecked = true
                        viewHolder.starThree?.isChecked = true
                        viewHolder.starFour?.isChecked = true
                        viewHolder.starFive?.isChecked = false
                        return 4
                    } else if (currentAnswer.size > 4 && currentAnswer[4].id == answerId) {
                        viewHolder.starOne?.isChecked = true
                        viewHolder.starTwo?.isChecked = true
                        viewHolder.starThree?.isChecked = true
                        viewHolder.starFour?.isChecked = true
                        viewHolder.starFive?.isChecked = true
                        return 5
                    }

                } ?: run {
                    viewHolder.starOne?.isChecked = false
                    viewHolder.starTwo?.isChecked = false
                    viewHolder.starThree?.isChecked = false
                    viewHolder.starFour?.isChecked = false
                    viewHolder.starFive?.isChecked = false
                    return 0
                }
            } ?: run {
                viewHolder.starOne?.isChecked = false
                viewHolder.starTwo?.isChecked = false
                viewHolder.starThree?.isChecked = false
                viewHolder.starFour?.isChecked = false
                viewHolder.starFive?.isChecked = false
                return 0
            }
        } ?: run {
            viewHolder.starOne?.isChecked = false
            viewHolder.starTwo?.isChecked = false
            viewHolder.starThree?.isChecked = false
            viewHolder.starFour?.isChecked = false
            viewHolder.starFive?.isChecked = false
            return 0
        }
        return 0
    }

    private fun initExpandableLayoutsBehavior(
        position: Int,
        viewHolder: SupplementaryOffersCardAdapterViewHolder
    ) {
        if (isDetailsLayoutExpanded || isHeaderLayoutExpanded) {
            logE("tarX123", "anyone true", fromClass, "expandedPosition")
            if (isHeaderLayoutExpanded) {
                logE("tarX123", "isHeaderLayoutExpanded", fromClass, "isHeaderLayoutExpanded")
                collapseDetailsLayout(viewHolder)
                expandBonusLayout(viewHolder)
            } else if (isDetailsLayoutExpanded) {
                logE("tarX123", "isDetailsLayoutExpanded", fromClass, "isDetailsLayoutExpanded")
                collapseBonusLayout(viewHolder)
                expandDetailsLayout(viewHolder)
            }
        } else {
            logE("tarX123", "allllll false", fromClass, "expandedPosition NOT")
            if (supplementaryOffersList!![position]!!.header != null) {
                expandBonusLayout(viewHolder)
            } else {
                expandDetailsLayout(viewHolder)
            }//if (supplementaryOffersList!![position]!!.header != null) ends
        }

    }//initExpandableLayoutsBehavior ends

    private fun setupDetailsSection(
        detailsView: View,
        details: SupplementryOffersDetailsAndDescription
    ) {
        val detailsRoundingHeaderLayout: LinearLayout =
            detailsView.findViewById(R.id.detailsRoundingHeaderLayout) as LinearLayout
        val viewBelowDetailsRoundingContentLayout: View =
            detailsView.findViewById(R.id.viewBelowDetailsRoundingContentLayout) as View
        val detailsTextWithTitleText: TextView =
            detailsView.findViewById(R.id.detailsTextWithTitleText) as TextView
        detailsTextWithTitleText.typeface = getALSNormalFont()
        val viewBelowDetailsTextWithTitleHeaderLabel: View =
            detailsView.findViewById(R.id.viewBelowDetailsTextWithTitleHeaderLabel) as View
        val viewBelowDetailsTextWithPointsContentLayout: View =
            detailsView.findViewById(R.id.viewBelowDetailsTextWithPointsContentLayout) as View
        val detailsRoamingHeaderLabel: TextView =
            detailsView.findViewById(R.id.detailsRoamingHeaderLabel) as TextView
        detailsRoamingHeaderLabel.typeface = getALSBoldFont()
        val detailsTextWithPointsContentLayout: LinearLayout =
            detailsView.findViewById(R.id.detailsTextWithPointsContentLayout) as LinearLayout
        val detailPricesHeaderLayout: LinearLayout =
            detailsView.findViewById(R.id.detailPricesHeaderLayout) as LinearLayout
        val detailsTextWithTitleHeaderLabel: TextView =
            detailsView.findViewById(R.id.detailsTextWithTitleHeaderLabel) as TextView
        detailsTextWithTitleHeaderLabel.typeface = getALSBoldFont()
        val detailRoamingContentLayout: LinearLayout =
            detailsView.findViewById(R.id.detailRoamingContentLayout) as LinearLayout
        val detailsTextWithoutTitleLabel: TextView =
            detailsView.findViewById(R.id.detailsTextWithoutTitleLabel) as TextView
        detailsTextWithoutTitleLabel.typeface = getALSNormalFont()
        val viewBelowDetailsTextWithoutTitleLabel: View =
            detailsView.findViewById(R.id.viewBelowDetailsTextWithoutTitleLabel) as View
        val detailsRoamingHeaderIcon: ImageView =
            detailsView.findViewById(R.id.detailsRoamingHeaderIcon) as ImageView
        val detailsTextWithPointsHeaderLabel =
            detailsView.findViewById(R.id.detailsTextWithPointsHeaderLabel) as TextView
        detailsTextWithPointsHeaderLabel.typeface = getALSBoldFont()
        val detailsTextWithPointsHeaderIcon =
            detailsView.findViewById(R.id.detailsTextWithPointsHeaderIcon) as ImageView
        val detailsTextWithPointsHeaderLayout =
            detailsView.findViewById(R.id.detailsTextWithPointsHeaderLayout) as LinearLayout

        /**set the details prices section*/
        if (!details.price.isNullOrEmpty()) {
            setupDetailsPriceSection(details.price, detailsView)
        } else {
            detailPricesHeaderLayout.visibility = View.GONE
        }

        /** set the details rounding section*/
        if (details.rounding != null) {
            isDetailsRoundingView = true
            setupDetailsRoundingSection(details.rounding, detailsView)
        } else {
            detailsRoundingHeaderLayout.visibility = View.GONE
            isDetailsRoundingView = false
        }

        /**set the text with title layout*/
        if (details.textWithTitle != null) {
            isDetailsTextWithTitleView = true
            setupTextWithTitleSection(details.textWithTitle, detailsView)
        } else {
            detailsTextWithTitleHeaderLabel.visibility = View.GONE
            detailsTextWithTitleText.visibility = View.GONE
            isDetailsTextWithTitleView = false
        }

        /**set the text with points layout*/
        if (details.textWithPoints != null) {
            //juni12891226nosi
            isDetailsTextWithPointsView = true
            /**set the title*/
            if (details.textWithPoints.title != null && hasValue(details.textWithPoints.title)) {
                detailsTextWithPointsHeaderLabel.text = details.textWithPoints.title
                detailsTextWithPointsHeaderLabel.isSelected = true
            }

            /**set the title icon*/
            if (details.textWithPoints.titleIcon != null && hasValue(details.textWithPoints.titleIcon)) {
                detailsTextWithPointsHeaderIcon.setImageResource(getCardAdapterMappedImage(details.textWithPoints.titleIcon))
            }

            /**set the points*/
            if (details.textWithPoints.pointsList != null && !details.textWithPoints.pointsList.isEmpty()) {
                setupTextWithPointsSection(details.textWithPoints.pointsList, detailsView)
            } else {
                detailsTextWithPointsContentLayout.visibility = View.GONE
                isDetailsTextWithPointsView = false
            }

            /**title and icon not there*/
            if (!hasValue(details.textWithPoints.title) &&
                !hasValue(details.textWithPoints.titleIcon)
            ) {
                detailsTextWithPointsHeaderLayout.visibility = View.GONE
            }

            /**nothing is there*/
            if (!hasValue(details.textWithPoints.title) &&
                !hasValue(details.textWithPoints.titleIcon) &&
                details.textWithPoints.pointsList.isNullOrEmpty()
            ) {
                isDetailsTextWithPointsView = false
                detailsTextWithPointsContentLayout.visibility = View.GONE
            }
        } else {
            detailsTextWithPointsContentLayout.visibility = View.GONE
            isDetailsTextWithPointsView = false
        }

        /**set the text without title*/
        if (details.textWithOutTitle != null) {
            isDetailsTextWithoutTitleView = true
            if (details.textWithOutTitle.description != null && hasValue(details.textWithOutTitle.description)) {
                detailsTextWithoutTitleLabel.text = details.textWithOutTitle.description
            } else {
                detailsTextWithoutTitleLabel.visibility = View.GONE
                isDetailsTextWithoutTitleView = false
            }
        } else {
            detailsTextWithoutTitleLabel.visibility = View.GONE
            isDetailsTextWithoutTitleView = false
        }

        /**set the roaming layout*/
        if (details.roamingDetails != null) {
            isRoamingView = true
            logE("roamX", "roamingDetails is not null", fromClass, "setupDetailsSection")
            if (details.roamingDetails.descriptionAbove != null && hasValue(details.roamingDetails.descriptionAbove)) {
                detailsRoamingHeaderLabel.text = details.roamingDetails.descriptionAbove
            } else {
                detailsRoamingHeaderLabel.visibility = View.GONE
            }

            if (details.roamingDetails.roamingIcon != null && hasValue(details.roamingDetails.roamingIcon)) {
                detailsRoamingHeaderIcon.setImageResource(getCardAdapterMappedImage(details.roamingDetails.roamingIcon))
            }

            //set the countries
            if (details.roamingDetails.roamingDetailsCountriesList != null && !details.roamingDetails.roamingDetailsCountriesList.isEmpty()) {
                setupRoamingSection(details.roamingDetails.roamingDetailsCountriesList, detailsView)
            } else {
                detailRoamingContentLayout.visibility = View.GONE
            }
        } else {
            isRoamingView = false
            logE("roamX", "roamingDetails is null", fromClass, "setupDetailsSection")
            detailsRoamingHeaderLabel.visibility = View.GONE
            detailRoamingContentLayout.visibility = View.GONE
        }

        if (isDetailsRoundingView) {
            viewBelowDetailsRoundingContentLayout.visibility = View.VISIBLE
            if (!isDetailsTextWithTitleView) {
                viewBelowDetailsRoundingContentLayout.visibility = View.GONE
                if (!isDetailsTextWithoutTitleView) {
                    viewBelowDetailsRoundingContentLayout.visibility = View.GONE
                    if (!isDetailsTextWithPointsView) {
                        viewBelowDetailsRoundingContentLayout.visibility = View.GONE
                        if (!isRoamingView) {
                            viewBelowDetailsRoundingContentLayout.visibility = View.GONE
                        } else {
                            viewBelowDetailsRoundingContentLayout.visibility = View.VISIBLE
                        }//if (!isRoamingView) ends
                    } else {
                        viewBelowDetailsRoundingContentLayout.visibility = View.VISIBLE
                    }//if (!isDetailsTextWithPointsView) ends
                } else {
                    viewBelowDetailsRoundingContentLayout.visibility = View.VISIBLE
                }//if (!isDetailsTextWithoutTitleView) ends
            } else {
                viewBelowDetailsRoundingContentLayout.visibility = View.VISIBLE
            }//if (!isDetailsTextWithTitleView) ends
        }//if (isDetailsRoundingView) ends

        if (isDetailsTextWithTitleView) {
            viewBelowDetailsTextWithTitleHeaderLabel.visibility = View.VISIBLE
            if (!isDetailsTextWithoutTitleView) {
                viewBelowDetailsTextWithTitleHeaderLabel.visibility = View.GONE
                if (!isDetailsTextWithPointsView) {
                    viewBelowDetailsTextWithTitleHeaderLabel.visibility = View.GONE
                    if (!isRoamingView) {
                        viewBelowDetailsTextWithTitleHeaderLabel.visibility = View.GONE
                    } else {
                        viewBelowDetailsTextWithTitleHeaderLabel.visibility = View.VISIBLE
                    }//if (!isRoamingView) ends
                } else {
                    viewBelowDetailsTextWithTitleHeaderLabel.visibility = View.VISIBLE
                }// if (!isDetailsTextWithPointsView) ends
            } else {
                viewBelowDetailsTextWithTitleHeaderLabel.visibility = View.VISIBLE
            }//if (!isDetailsTextWithoutTitleView) ends
        }//if (isDetailsTextWithTitleView)  ends

        if (isDetailsTextWithoutTitleView) {
            viewBelowDetailsTextWithoutTitleLabel.visibility = View.VISIBLE
            if (!isDetailsTextWithPointsView) {
                viewBelowDetailsTextWithoutTitleLabel.visibility = View.GONE
                if (!isRoamingView) {
                    viewBelowDetailsTextWithoutTitleLabel.visibility = View.GONE
                } else {
                    viewBelowDetailsTextWithoutTitleLabel.visibility = View.VISIBLE
                }//if (!isRoamingView)  ends
            } else {
                viewBelowDetailsTextWithoutTitleLabel.visibility = View.VISIBLE
            }
        }//if (isDetailsTextWithoutTitleView) ends

        if (isDetailsTextWithPointsView) {
            viewBelowDetailsTextWithPointsContentLayout.visibility = View.VISIBLE
            if (!isRoamingView) {
                viewBelowDetailsTextWithPointsContentLayout.visibility = View.GONE
            } else {
                viewBelowDetailsTextWithPointsContentLayout.visibility = View.VISIBLE
            }//if (!isRoamingView) ends
        }//if (isDetailsTextWithPointsView) ends
    }//setupDetailsSection ends

    private fun setupDetailsPriceSection(price: List<PriceItem?>, detailsView: View) {
        val detailPricesHeaderLayout: LinearLayout =
            detailsView.findViewById(R.id.detailPricesHeaderLayout) as LinearLayout

        //iterate over the list and add the view items
        detailPricesHeaderLayout.removeAllViews()
        for (its in price.indices) {
            val packagesDetailsPriceViewItem = PackagesDetailsPriceViewItem(context!!)

            /* */
            /**hide the very last bottom view*//*
            if (its == price.size - 1) {
                packagesDetailsPriceViewItem.getViewBelowDetailsContentLayoutLayout().visibility = View.GONE
            } else {
                packagesDetailsPriceViewItem.getViewBelowDetailsContentLayoutLayout().visibility = View.VISIBLE
            }*/

            if (price[its]!!.iconName != null && hasValue(price[its]!!.iconName)) {
                packagesDetailsPriceViewItem.getDetailsPriceIcon()
                    .setImageResource(getCardAdapterMappedImage(price[its]!!.iconName!!))
            }//if (price[its]!!.iconName != null && hasValue(price[its]!!.iconName)) ends

            if (price[its]!!.title != null && hasValue(price[its]!!.title)) {
                packagesDetailsPriceViewItem.getDetailsHeaderLabel().text = price[its]!!.title
                packagesDetailsPriceViewItem.getDetailsHeaderLabel().isSelected = true
            }//if (price[its]!!.title != null && hasValue(price[its]!!.title)) ends

            if (price[its]!!.value != null && hasValue(price[its]!!.value)) {
                if (price[its]!!.offersCurrency != null && hasValue(price[its]!!.offersCurrency)) {
                    if (price[its]!!.offersCurrency.equals(
                            ConstantsUtility.TariffConstants.PRICE_OFFERS_CURRENCY,
                            ignoreCase = true
                        )
                    ) {
                        val value = price[its]!!.value
                        packagesDetailsPriceViewItem.getDetailsHeaderValue().text =
                            getManatConcatedString(
                                context!!, value!!, ConstantsUtility.TariffConstants.AZERI_SYMBOL,
                                0.25f, R.dimen._10ssp
                            )
                        packagesDetailsPriceViewItem.getDetailsHeaderValue().isSelected = true
                    } else {
                        if (isNumeric(price[its]!!.value)) {
                            val value = price[its]!!.value
                            packagesDetailsPriceViewItem.getDetailsHeaderValue().text =
                                getManatConcatedString(
                                    context!!,
                                    value!!,
                                    ConstantsUtility.TariffConstants.AZERI_SYMBOL,
                                    0.25f,
                                    R.dimen._10ssp
                                )
                            packagesDetailsPriceViewItem.getDetailsHeaderValue().isSelected = true
                        } else {
                            packagesDetailsPriceViewItem.getDetailsHeaderValue().text =
                                price[its]!!.value
                            packagesDetailsPriceViewItem.getDetailsHeaderValue().isSelected = true
                        }//isNumeric ends
                    }
                } else {
                    if (isNumeric(price[its]!!.value)) {
                        val value = price[its]!!.value
                        packagesDetailsPriceViewItem.getDetailsHeaderValue().text =
                            getManatConcatedString(
                                context!!, value!!, ConstantsUtility.TariffConstants.AZERI_SYMBOL,
                                0.25f, R.dimen._10ssp
                            )
                        packagesDetailsPriceViewItem.getDetailsHeaderValue().isSelected = true
                    } else {
                        packagesDetailsPriceViewItem.getDetailsHeaderValue().text =
                            price[its]!!.value
                        packagesDetailsPriceViewItem.getDetailsHeaderValue().isSelected = true
                    }//isNumeric ends
                }
            }

            if (!hasValue(price[its]!!.value) && !hasValue(price[its]!!.title)) {
                packagesDetailsPriceViewItem.getDetailsPriceViewItemLayout().visibility = View.GONE
            }

            /**populate the content*/
            if (price[its]!!.attributeList != null && price[its]!!.attributeList!!.isNotEmpty()) {
                packagesDetailsPriceViewItem.getDetailsContentLayout().removeAllViews()
                //iterate over the attributes and populate the details price attributes view items
                for (iterations in price[its]!!.attributeList!!.indices) {
                    val packagesDetailsPriceAttributesViewItem =
                        PackagesDetailsPriceAttributesViewItem(context!!)

                    if (price[its]!!.attributeList!![iterations]!!.title != null && hasValue(price[its]!!.attributeList!![iterations]!!.title)) {
                        packagesDetailsPriceAttributesViewItem.getDetailPriceAttributeViewItemLabel().text =
                            price[its]!!.attributeList!![iterations]!!.title
                        packagesDetailsPriceAttributesViewItem.getDetailPriceAttributeViewItemLabel().isSelected =
                            true
                    }

                    if (price[its]!!.attributeList!![iterations]!!.value != null && hasValue(price[its]!!.attributeList!![iterations]!!.value)) {
                        if (price[its]!!.attributeList!![iterations]!!.unit != null && hasValue(
                                price[its]!!.attributeList!![iterations]!!.unit
                            )
                        ) {
                            if (price[its]!!.attributeList!![iterations]!!.unit!!.equals(
                                    ConstantsUtility.TariffConstants.ATTRIBUTE_LIST_UNIT,
                                    ignoreCase = true
                                )
                            ) {
                                val value = price[its]!!.attributeList!![iterations]!!.value
                                packagesDetailsPriceAttributesViewItem.getDetailPriceAttributeItemValue().text =
                                    getManatConcatedString(
                                        context!!,
                                        value!!,
                                        ConstantsUtility.TariffConstants.AZERI_SYMBOL,
                                        0.25f,
                                        R.dimen._10ssp
                                    )
                                packagesDetailsPriceAttributesViewItem.getDetailPriceAttributeItemValue().isSelected =
                                    true
                            } else {
                                if (isNumeric(price[its]!!.attributeList!![iterations]!!.value)) {
                                    val value = price[its]!!.attributeList!![iterations]!!.value
                                    packagesDetailsPriceAttributesViewItem.getDetailPriceAttributeItemValue().text =
                                        getManatConcatedString(
                                            context!!,
                                            value!!,
                                            ConstantsUtility.TariffConstants.AZERI_SYMBOL,
                                            0.25f,
                                            R.dimen._10ssp
                                        )
                                    packagesDetailsPriceAttributesViewItem.getDetailPriceAttributeItemValue().isSelected =
                                        true
                                } else {
                                    packagesDetailsPriceAttributesViewItem.getDetailPriceAttributeItemValue().text =
                                        price[its]!!.attributeList!![iterations]!!.value
                                    packagesDetailsPriceAttributesViewItem.getDetailPriceAttributeItemValue().isSelected =
                                        true
                                }//isNumeric ends
                            }
                        } else {
                            if (isNumeric(price[its]!!.attributeList!![iterations]!!.value)) {
                                val value = price[its]!!.attributeList!![iterations]!!.value
                                packagesDetailsPriceAttributesViewItem.getDetailPriceAttributeItemValue().text =
                                    getManatConcatedString(
                                        context!!,
                                        value!!,
                                        ConstantsUtility.TariffConstants.AZERI_SYMBOL,
                                        0.25f,
                                        R.dimen._10ssp
                                    )
                                packagesDetailsPriceAttributesViewItem.getDetailPriceAttributeItemValue().isSelected =
                                    true
                            } else {
                                packagesDetailsPriceAttributesViewItem.getDetailPriceAttributeItemValue().text =
                                    price[its]!!.attributeList!![iterations]!!.value
                                packagesDetailsPriceAttributesViewItem.getDetailPriceAttributeItemValue().isSelected =
                                    true
                            }//isNumeric ends
                        }

                    }

                    if (!hasValue(price[its]!!.attributeList!![iterations]!!.value) && !hasValue(
                            price[its]!!.attributeList!![iterations]!!.title
                        )
                    ) {
                        packagesDetailsPriceAttributesViewItem.getDetailPriceAttributeViewItemLayout().visibility =
                            View.GONE
                    }

                    packagesDetailsPriceViewItem.getDetailsContentLayout()
                        .addView(packagesDetailsPriceAttributesViewItem)
                }//for ends
            } else {
                packagesDetailsPriceViewItem.getDetailsContentLayout().visibility = View.GONE
            }//if (price[its]!!.attributeList != null && !price[its]!!.attributeList!!.isEmpty()) ends

            detailPricesHeaderLayout.addView(packagesDetailsPriceViewItem)
        }//for ends
    }//setupDetailsPriceSection ends

    private fun setupRoamingSection(
        roamingDetailsCountriesList: List<RoamingDetailsCountriesListItem?>,
        detailsView: View
    ) {
        val detailRoamingContentLayout: LinearLayout =
            detailsView.findViewById(R.id.detailRoamingContentLayout) as LinearLayout

        detailRoamingContentLayout.removeAllViews()
        for (its in roamingDetailsCountriesList.indices) {
            val packagesRoamingViewItem = PackagesRoamingViewItem(context!!)

            if (roamingDetailsCountriesList[its]!!.countryName != null && hasValue(
                    roamingDetailsCountriesList[its]!!.countryName
                )
            ) {
                packagesRoamingViewItem.getDetailsRoamingCountryLabel().text =
                    roamingDetailsCountriesList[its]!!.countryName
                packagesRoamingViewItem.getDetailsRoamingCountryLabel().isSelected = true
            }

            if (roamingDetailsCountriesList[its]!!.flag != null && hasValue(
                    roamingDetailsCountriesList[its]!!.flag
                )
            ) {
                packagesRoamingViewItem.getDetailsRoamingCountryFlag().setImageDrawable(
                    getImageFromAssests(
                        context!!,
                        roamingDetailsCountriesList[its]!!.flag!!
                    )
                )
            }

            /**set the operators*/
            if (roamingDetailsCountriesList[its]!!.operatorList != null && !roamingDetailsCountriesList[its]!!.operatorList!!.isEmpty()) {
                packagesRoamingViewItem.getroamingOperatorsLayout().removeAllViews()

                for (operators in roamingDetailsCountriesList[its]!!.operatorList!!.indices) {
                    val packagesRoamingOperatorsViewItem =
                        PackagesRoamingOperatorsViewItem(context!!)

                    if (roamingDetailsCountriesList[its]!!.operatorList!![operators] != null &&
                        hasValue(roamingDetailsCountriesList[its]!!.operatorList!![operators])
                    ) {
                        packagesRoamingOperatorsViewItem.getRoamingOperatorViewItemValue().text =
                            roamingDetailsCountriesList[its]!!.operatorList!![operators].toString()
                        packagesRoamingOperatorsViewItem.getRoamingOperatorViewItemValue().isSelected =
                            true
                    }

                    packagesRoamingViewItem.getroamingOperatorsLayout()
                        .addView(packagesRoamingOperatorsViewItem)
                }//for operators end
            }

            detailRoamingContentLayout.addView(packagesRoamingViewItem)
        }//for ends

    }//setupRoamingSection ends

    private fun setupTextWithPointsSection(pointsList: List<String?>, detailsView: View) {
        val detailsTextWithPointsContentLayout: LinearLayout =
            detailsView.findViewById(R.id.detailsTextWithPointsContentLayout) as LinearLayout

        detailsTextWithPointsContentLayout.removeAllViews()
        for (its in pointsList.indices) {
            val packagesTextWithPointsViewItem = PackagesTextWithPointsViewItem(context!!)

            if (pointsList[its] != null && hasValue(pointsList[its].toString())) {
                packagesTextWithPointsViewItem.getTextWithPointsViewItemValue().text =
                    pointsList[its].toString()
            } else {
                packagesTextWithPointsViewItem.getTextWithPointsViewItemValue().visibility =
                    View.GONE
            }//if (pointsList[its] != null && hasValue(pointsList[its].toString())) ends

            if (!hasValue(pointsList[its].toString())) {
                packagesTextWithPointsViewItem.getTextWithPointsViewItemLayout().visibility =
                    View.GONE
            }

            detailsTextWithPointsContentLayout.addView(packagesTextWithPointsViewItem)
        }//for ends
    }//setupTextWithPointsSection ends

    private fun setupTextWithTitleSection(textWithTitle: TextWithTitle, detailsView: View) {
        val detailsTextWithTitleHeaderLabel: TextView =
            detailsView.findViewById(R.id.detailsTextWithTitleHeaderLabel) as TextView
        val detailsTextWithTitleText: TextView =
            detailsView.findViewById(R.id.detailsTextWithTitleText) as TextView
        val viewBelowDetailsTextWithTitleHeaderLabel: View =
            detailsView.findViewById(R.id.viewBelowDetailsTextWithTitleHeaderLabel) as View

        if (textWithTitle.title != null && hasValue(textWithTitle.title)) {
            detailsTextWithTitleHeaderLabel.text = textWithTitle.title
            detailsTextWithTitleHeaderLabel.isSelected = true
        } else {
            detailsTextWithTitleHeaderLabel.visibility = View.GONE
        }

        if (textWithTitle.text != null && hasValue(textWithTitle.text)) {
            detailsTextWithTitleText.text = textWithTitle.text
        } else {
            detailsTextWithTitleText.visibility = View.GONE
        }

        if (!hasValue(textWithTitle.title) && !hasValue(textWithTitle.text)) {
            isDetailsTextWithTitleView = false
            detailsTextWithTitleText.visibility = View.GONE
            detailsTextWithTitleHeaderLabel.visibility = View.GONE
        }
    }//setupTextWithTitleSection ends

    private fun setupDetailsRoundingSection(rounding: Rounding, detailsView: View) {
        val detailsRoundingHeaderIcon: ImageView =
            detailsView.findViewById(R.id.detailsRoundingHeaderIcon) as ImageView
        val detailsRoundingHeaderLabel: TextView =
            detailsView.findViewById(R.id.detailsRoundingHeaderLabel) as TextView
        detailsRoundingHeaderLabel.typeface = getALSBoldFont()
        val detailsRoundingHeaderValue: TextView =
            detailsView.findViewById(R.id.detailsRoundingHeaderValue) as TextView
        detailsRoundingHeaderValue.typeface = getALSNormalFont()
        val detailsRoundingContentLayout: LinearLayout =
            detailsView.findViewById(R.id.detailsRoundingContentLayout) as LinearLayout
        val viewBelowDetailsRoundingContentLayout: View =
            detailsView.findViewById(R.id.viewBelowDetailsRoundingContentLayout) as View

        //rounding title here
        if (rounding.title != null && hasValue(rounding.title)) {
            detailsRoundingHeaderLabel.text = rounding.title
            detailsRoundingHeaderLabel.isSelected = true
        }//if (rounding.title != null && hasValue(rounding.title)) ends

        //rounding titleValueRight here
        if (rounding.value != null && hasValue(rounding.value)) {
            detailsRoundingHeaderValue.text = rounding.value
            detailsRoundingHeaderValue.isSelected = true
        }//if (rounding.value != null && hasValue(rounding.value)) ends

        //rounding icon here
        if (rounding.iconName != null && hasValue(rounding.iconName)) {
            detailsRoundingHeaderIcon.setImageResource(getCardAdapterMappedImage(rounding.iconName))
        }//if (rounding.iconName != null && hasValue(rounding.iconName)) ends

        /**set the sms attributes*/
        if (rounding.attributeList != null && !rounding.attributeList.isEmpty()) {
            setDetailsRoundingAttributes(rounding.attributeList, detailsView)
        } else {
            detailsRoundingContentLayout.visibility = View.GONE
            isDetailsRoundingView = false
        }//if (rounding.attributeList != null && !rounding.attributeList.isEmpty()) ends
    }//setupDetailsRoundingSection ends

    private fun setDetailsRoundingAttributes(
        attributeList: List<AttributeListItem?>,
        detailsView: View
    ) {
        val detailsRoundingContentLayout: LinearLayout =
            detailsView.findViewById(R.id.detailsRoundingContentLayout) as LinearLayout
        //iterate over the list and add view items
        //remove the view
        detailsRoundingContentLayout.removeAllViews()
        for (its in attributeList.indices) {
            val packagesRoundingViewItem = PackagesRoundingViewItem(context!!)

            if (attributeList[its]!!.title != null && hasValue(attributeList[its]!!.title)) {
                packagesRoundingViewItem.getRoundingViewItemLabel().text =
                    attributeList[its]!!.title
                packagesRoundingViewItem.getRoundingViewItemLabel().isSelected = true
            }

            if (attributeList[its]!!.value != null && hasValue(attributeList[its]!!.value)) {
                if (attributeList[its]!!.unit != null && hasValue(attributeList[its]!!.unit)) {
                    if (attributeList[its]!!.unit.equals(
                            ConstantsUtility.TariffConstants.ATTRIBUTE_LIST_UNIT,
                            ignoreCase = true
                        )
                    ) {
                        //show the manat with the value
                        val value = attributeList[its]!!.value
                        packagesRoundingViewItem.getRoundingViewItemValue().text =
                            getManatConcatedString(
                                context!!, value!!, ConstantsUtility.TariffConstants.AZERI_SYMBOL,
                                0.25f, R.dimen._10ssp
                            )
                        packagesRoundingViewItem.getRoundingViewItemValue().isSelected = true
                    } else {
                        //dont show the manat with the value
                        val value = attributeList[its]!!.value
                        packagesRoundingViewItem.getRoundingViewItemValue().text = value!!
                        packagesRoundingViewItem.getRoundingViewItemValue().isSelected = true
                    }
                } else {
                    packagesRoundingViewItem.getRoundingViewItemValue().text =
                        attributeList[its]!!.value
                    packagesRoundingViewItem.getRoundingViewItemValue().isSelected = true
                }
            }//if (attributeList[its]!!.value != null && hasValue(attributeList[its]!!.value))  ends

            /*  if (!hasValue(attributeList[its]!!.title) && !hasValue(attributeList[its]!!.value)) {
                  packagesRoundingViewItem.getRoundingViewItemLayout().visibility = View.GONE
              }*/

            detailsRoundingContentLayout.addView(packagesRoundingViewItem)
        }//for ends
    }//setDetailsRoundingAttributes ends

    private fun preparePackageCardHeader(
        header: SupplementryOffersHeaders,
        viewHolder: SupplementaryOffersCardAdapterViewHolder
    ) {
        /**check roaming*/
        if (isRoaming) {
            logE(
                "countrySent",
                "isRoamingTrue:::".plus(selectedCountryName),
                fromClass,
                "preparePackageCardHeader"
            )
            viewHolder.packageRoamingHolderLayout!!.visibility = View.VISIBLE
            if (hasValue(selectedCountryName)) {
                viewHolder.roamingCountryName!!.text = selectedCountryName
                viewHolder.roamingCountryName!!.isSelected = true
            }
            if (hasValue(selectedCountryFlag)) {
                viewHolder.roamingCountryFlag!!.setImageDrawable(
                    getImageFromAssests(
                        context!!,
                        selectedCountryFlag
                    )
                )
            }
        } else {
            logE("countrySent", "isRoamingFalse:::", fromClass, "preparePackageCardHeader")
            viewHolder.packageRoamingHolderLayout!!.visibility = View.GONE
        }

        /**check for the buttons*/
        if (header.isTopUp != null && hasValue(header.isTopUp) && header.isTopUp == "1") {
            //show topup button button
            viewHolder.subscribeButton!!.visibility = View.GONE
            viewHolder.topUpButton!!.visibility = View.VISIBLE
            viewHolder.renewSubscribedButtonHolder!!.visibility = View.GONE
            viewHolder.subscribedButton!!.visibility = View.GONE
            viewHolder.renewButton!!.visibility = View.GONE
        } else if (header.isAlreadySusbcribed != null && hasValue(header.isAlreadySusbcribed) && header.isAlreadySusbcribed.equals(
                "true",
                ignoreCase = true
            )
        ) {
            //check btnRenew =1, show renew button else show subscribed button
            if (header.btnRenew != null && hasValue(header.btnRenew) && header.btnRenew == "1") {
                //show renew button
                viewHolder.subscribeButton!!.visibility = View.GONE
                viewHolder.topUpButton!!.visibility = View.GONE
                viewHolder.renewSubscribedButtonHolder!!.visibility = View.VISIBLE
                viewHolder.ratingStarsLayout!!.visibility = View.VISIBLE
            } else {
                //show subscribed button
                viewHolder.subscribeButton!!.visibility = View.GONE
                viewHolder.topUpButton!!.visibility = View.GONE
                viewHolder.renewSubscribedButtonHolder!!.visibility = View.VISIBLE
                viewHolder.subscribedButton!!.visibility = View.VISIBLE
                viewHolder.renewButton!!.visibility = View.GONE
                viewHolder.ratingStarsLayout!!.visibility = View.VISIBLE
            }
        } else {
            //show subscribe button
            viewHolder.renewSubscribedButtonHolder!!.visibility = View.GONE
            viewHolder.subscribeButton!!.visibility = View.VISIBLE
            viewHolder.topUpButton!!.visibility = View.GONE
            viewHolder.ratingStarsLayout!!.visibility = View.GONE
        }

        //offer name
        if (header.offerName != null && hasValue(header.offerName)) {
            viewHolder.packageNameLabel!!.text = header.offerName
            viewHolder.packageNameLabel!!.isSelected = true
        }//if (header.offerName != null && hasValue(header.offerName)) ends

        if (isSpecialOffers) {
            if (header.price != null && hasValue(header.price)) {
                if (isNumeric(header.price)) {
                    val price = header.price
                    viewHolder.packageNameValue!!.text = getManatConcatedString(
                        context!!,
                        price,
                        ConstantsUtility.PackagesConstants.AZERI_SYMBOL,
                        0.20f,
                        R.dimen._10ssp
                    )
                    viewHolder.packageNameValue!!.isSelected = true
                } else {
                    viewHolder.packageNameValue!!.text = header.price
                    viewHolder.packageNameValue!!.isSelected = true
                }
            } else {
                viewHolder.packageNameValue!!.visibility = View.GONE
            }
        } else {
            //offer price
            if (header.price != null && hasValue(header.price) && header.price == "0" || header.price != null && hasValue(
                    header.price
                ) && header.price == "0.00"
            ) {
                viewHolder.packageNameValue!!.visibility = View.GONE
            } else {
                if (header.price != null && hasValue(header.price)) {
                    if (isNumeric(header.price)) {
                        if (header.price.toDouble() == 0.0) {
                            viewHolder.packageNameValue!!.visibility = View.GONE
                        } else {
                            val price = header.price
                            viewHolder.packageNameValue!!.text = getManatConcatedString(
                                context!!,
                                price,
                                ConstantsUtility.PackagesConstants.AZERI_SYMBOL,
                                0.20f,
                                R.dimen._10ssp
                            )
                            viewHolder.packageNameValue!!.isSelected = true
                        }
                    } else {
                        viewHolder.packageNameValue!!.text = header.price
                        viewHolder.packageNameValue!!.isSelected = true
                    }
                }
            }//if (header.price != null && hasValue(header.price))  ends
        }

        //offer validity
        if (header.validityTitle != null && hasValue(header.validityTitle)) {
            viewHolder.packageValidityLabel!!.text = header.validityTitle
            viewHolder.packageValidityLabel!!.isSelected = true
        }//if (header.validityTitle != null && hasValue(header.validityTitle)) ends

        //offer validity value
        if (header.validityValue != null && hasValue(header.validityValue)) {
            viewHolder.packageValidityValue!!.text = header.validityValue
            viewHolder.packageValidityValue!!.isSelected = true
        }//if (header.validityValue != null && hasValue(header.validityValue)) ends

        if (header.attributeList != null && header.attributeList.isNotEmpty()) {
            setupHeaderAttributes(header.attributeList, viewHolder)
        } else {
            viewHolder.bonusesContentLayout!!.visibility = View.GONE
        } //if(header.attributeList!=null&&!header.attributeList.isEmpty()) ends
    }//preparePackageCardHeader ends

    private fun setupHeaderAttributes(
        attributeList: List<AttributeListItem?>,
        viewHolder: SupplementaryOffersCardAdapterViewHolder
    ) {
        //iterate over it
        //remove all existing menu views
        viewHolder.bonusesContentLayout!!.removeAllViews()

        for (its in attributeList.indices) {
            val bonusTariffViewItem = PackagesBonusViewItem(context!!)

            if (its == attributeList.size - 1) {
                bonusTariffViewItem.getViewBelowBonusViewItemLayout().visibility = View.GONE
            } else {
                bonusTariffViewItem.getViewBelowBonusViewItemLayout().visibility = View.VISIBLE
            }

            //icon
            if (attributeList[its]!!.iconMap != null && hasValue(attributeList[its]!!.iconMap)) {
                bonusTariffViewItem.getBonusViewItemIcon()
                    .setImageResource(getCardAdapterMappedImage(attributeList[its]!!.iconMap!!))
            }//if (attributeList[its]!!.iconMap != null && hasValue(attributeList[its]!!.iconMap)) ends

            //set the left label
            if (attributeList[its]!!.title != null && hasValue(attributeList[its]!!.title)) {
                bonusTariffViewItem.getBonusViewItemLabel().text = attributeList[its]!!.title
                bonusTariffViewItem.getBonusViewItemLabel().typeface = getALSBoldFont()
                bonusTariffViewItem.getBonusViewItemLabel().isSelected = true
            }//if (attributeList[its]!!.title!=null&&hasValue(attributeList[its]!!.title)) ends

            //set the right value
            var value = ""
            if (attributeList[its]!!.value != null && hasValue(attributeList[its]!!.value)) {
                value = attributeList[its]!!.value!!
            }

            var valueUnit = ""
            if (attributeList[its]!!.unit != null && hasValue(attributeList[its]!!.unit)) {
                valueUnit = attributeList[its]!!.unit!!
            }

            bonusTariffViewItem.getBonusViewItemValue().text = value.plus(" ").plus(valueUnit)
            bonusTariffViewItem.getBonusViewItemValue().typeface = getALSBoldFont()
            bonusTariffViewItem.getBonusViewItemValue().isSelected = true

            /*if (!hasValue(attributeList[its]!!.value) && hasValue(attributeList[its]!!.title)) {
                bonusTariffViewItem.getBonusViewItemLayout().visibility = View.GONE
                bonusTariffViewItem.getViewBelowBonusViewItemLayout().visibility = View.GONE
            }*/

            viewHolder.bonusesContentLayout!!.addView(bonusTariffViewItem)
        }//for ends
    }//setupHeaderAttributes ends

    @SuppressLint("ClickableViewAccessibility")
    private fun initUIEvents(viewHolder: SupplementaryOffersCardAdapterViewHolder, position: Int) {
        viewHolder.headerHolder!!.setOnClickListener {
            if (viewHolder.bonusesExpandableLayout!!.isExpanded) {
                collapseBonusLayout(viewHolder)
                isHeaderLayoutExpanded = false
                expandDetailsLayout(viewHolder)
                isDetailsLayoutExpanded = true
            } else {
                expandBonusLayout(viewHolder)
                isHeaderLayoutExpanded = true
                collapseDetailsLayout(viewHolder)
                isDetailsLayoutExpanded = false
            }

            notifyDataSetChanged()
        }//viewHolder.bonusHeaderLayout!!.setOnClickListener ends

        viewHolder.detailsHeaderLayout!!.setOnClickListener {
            if (viewHolder.detailsExpandableLayout!!.isExpanded) {
                expandBonusLayout(viewHolder)
                isHeaderLayoutExpanded = true
                collapseDetailsLayout(viewHolder)
                isDetailsLayoutExpanded = false
            } else {
                collapseBonusLayout(viewHolder)
                isHeaderLayoutExpanded = false
                expandDetailsLayout(viewHolder)
                isDetailsLayoutExpanded = true
            }

            notifyDataSetChanged()
        }//viewHolder.detailsHeaderLayout!!.setOnClickListener  ends

        /* viewHolder.layoutInsideScrollView!!.setOnTouchListener { v, event ->
             // Disallow the touch request for parent scroll on touch of child view
             v.parent.requestDisallowInterceptTouchEvent(true)
             false
         }//viewHolder.layoutInsideScrollView!!.setOnTouchListener ends*/

        viewHolder.subscribeButton!!.setOnClickListener {
            /**get the offer name and offer id from the model*/
            var offeringId = ""
            var offerName = ""
            if (supplementaryOffersList != null && supplementaryOffersList!!.isNotEmpty()) {
                if (supplementaryOffersList!![position]!!.header != null) {
                    //offer name
                    if (supplementaryOffersList!![position]!!.header!!.offerName != null &&
                        hasValue(supplementaryOffersList!![position]!!.header!!.offerName)
                    ) {
                        offerName = supplementaryOffersList!![position]!!.header!!.offerName!!
                    }

                    //offering id
                    if (supplementaryOffersList!![position]!!.header!!.offeringId != null &&
                        hasValue(supplementaryOffersList!![position]!!.header!!.offeringId)
                    ) {
                        offeringId = supplementaryOffersList!![position]!!.header!!.offeringId!!
                    }
                }
            }

            when {
                isRoaming -> RootValues.getSupplementaryOffersRoamingCardAdapterEvents()
                    .onSupplementaryOfferSubscribeButtonClick(
                        getSupplementaryOfferSubscribeConfirmationMessage(),
                        offeringId,
                        offerName,
                        "1"
                    )
                isSpecialOffers -> RootValues.getSupplementarySpecialOffersCardAdapterEvents()
                    .onSupplementaryOfferSubscribeButtonClick(
                        getSupplementaryOfferSubscribeConfirmationMessage(),
                        offeringId,
                        offerName,
                        "1"
                    )
                else -> RootValues.getInternetOffersCardAdapterEvents()
                    .onSupplementaryOfferSubscribeButtonClick(
                        getSupplementaryOfferSubscribeConfirmationMessage(),
                        offeringId,
                        offerName,
                        "1"
                    )
            }
        }//viewHolder.subscribeButton!!.setOnClickListener ends

        viewHolder.renewButton!!.setOnClickListener {
            /**get the offer name and offer id from the model*/
            var offeringId = ""
            var offerName = ""
            if (supplementaryOffersList != null && supplementaryOffersList!!.isNotEmpty()) {
                if (supplementaryOffersList!![position]!!.header != null) {
                    //offer name
                    if (supplementaryOffersList!![position]!!.header!!.offerName != null &&
                        hasValue(supplementaryOffersList!![position]!!.header!!.offerName)
                    ) {
                        offerName = supplementaryOffersList!![position]!!.header!!.offerName!!
                    }

                    //offering id
                    if (supplementaryOffersList!![position]!!.header!!.offeringId != null &&
                        hasValue(supplementaryOffersList!![position]!!.header!!.offeringId)
                    ) {
                        offeringId = supplementaryOffersList!![position]!!.header!!.offeringId!!
                    }
                }
            }

            when {
                isRoaming -> RootValues.getSupplementaryOffersRoamingCardAdapterEvents()
                    .onSupplementaryOfferSubscribeButtonClick(
                        getSupplementaryOfferSubscribeConfirmationMessage(),
                        offeringId,
                        offerName,
                        "1"
                    )
                isSpecialOffers -> RootValues.getInternetOffersCardAdapterEvents()
                    .onSupplementaryOfferSubscribeButtonClick(
                        getSupplementaryOfferSubscribeConfirmationMessage(),
                        offeringId,
                        offerName,
                        "1"
                    )
                else -> RootValues.getInternetOffersCardAdapterEvents()
                    .onSupplementaryOfferSubscribeButtonClick(
                        getSupplementaryOfferRenewConfirmationMessage(),
                        offeringId,
                        offerName,
                        "1"
                    )
            }
        }//viewHolder.renewButton.setOnClickListener ends

        viewHolder.topUpButton!!.setOnClickListener {
            val intent = Intent(context, TopupActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context!!.startActivity(intent)
        }//viewHolder.topUpButton!!.setOnClickListener ends
    }//initUIEvents ends

    private fun expandBonusLayout(viewHolder: SupplementaryOffersCardAdapterViewHolder) {
        viewHolder.bonusesExpandableLayout!!.expand()
    }//expandBonusLayout ends

    private fun collapseBonusLayout(viewHolder: SupplementaryOffersCardAdapterViewHolder) {
        viewHolder.bonusesExpandableLayout!!.collapse()
    }//collapseBonusLayout ends

    private fun expandDetailsLayout(viewHolder: SupplementaryOffersCardAdapterViewHolder) {
        viewHolder.detailsExpandableLayout!!.expand()
        viewHolder.detailsLayoutExpandIcon!!.setImageResource(R.drawable.faqminus)
        viewHolder.detailsLayoutTitleLabel!!.setTextColor(context!!.resources.getColor(R.color.colorPrimary))
    }//expandDetailsLayout ends

    private fun collapseDetailsLayout(viewHolder: SupplementaryOffersCardAdapterViewHolder) {
        viewHolder.detailsExpandableLayout!!.collapse()
        viewHolder.detailsLayoutExpandIcon!!.setImageResource(R.drawable.faqplus)
        viewHolder.detailsLayoutTitleLabel!!.setTextColor(context!!.resources.getColor(R.color.purplish_brown))
    }//collapseDetailsLayout ends

    inner class SupplementaryOffersCardAdapterViewHolder :
        androidx.recyclerview.widget.RecyclerView.ViewHolder {
        var mainContainer: RelativeLayout? = null
        var packageNameLabel: TextView? = null
        var roamingCountryName: TextView? = null
        var roamingCountryFlag: ImageView? = null
        var packageRoamingHolderLayout: ConstraintLayout? = null
        var packageNameValue: TextView? = null
        var packageValidityLabel: TextView? = null
        var packageValidityValue: TextView? = null
        var packageNameValidityHolderLayout: ConstraintLayout? = null
        var bonusesExpandableLayout: ExpandableLayout? = null
        var bonusesContentLayout: LinearLayout? = null
        var layoutInsideScrollView: RelativeLayout? = null
        var detailsLayout: LinearLayout? = null
        var detailsLayoutTitleLabel: TextView? = null
        var detailsLayoutExpandIcon: ImageView? = null
        var detailsHeaderLayout: RelativeLayout? = null
        var detailsViewStub: ViewStub? = null
        var detailsExpandableLayout: ExpandableLayout? = null
        var subscribeButton: Button? = null
        var topUpButton: Button? = null
        var subscribedButton: Button? = null
        var renewButton: Button? = null
        var renewSubscribedButtonHolder: LinearLayout? = null
        var headerHolder: ConstraintLayout? = null

        var ratingStarsLayout: ConstraintLayout? = null
        var starOne: CheckBox? = null
        var starTwo: CheckBox? = null
        var starThree: CheckBox? = null
        var starFour: CheckBox? = null
        var starFive: CheckBox? = null

        constructor(row: View) : super(row) {
            headerHolder = row.findViewById(R.id.headerHolder) as ConstraintLayout
            renewSubscribedButtonHolder =
                row.findViewById(R.id.renewSubscribedButtonHolder) as LinearLayout
            renewButton = row.findViewById(R.id.renewButton) as Button
            subscribedButton = row.findViewById(R.id.subscribedButton) as Button
            topUpButton = row.findViewById(R.id.topUpButton) as Button
            subscribeButton = row.findViewById(R.id.subscribeButton) as Button
            mainContainer = row.findViewById(R.id.mainContainer) as RelativeLayout
            roamingCountryName = row.findViewById(R.id.roamingCountryName) as TextView
            roamingCountryFlag = row.findViewById(R.id.roamingCountryFlag) as ImageView
            packageRoamingHolderLayout =
                row.findViewById(R.id.packageRoamingHolderLayout) as ConstraintLayout
            detailsViewStub = row.findViewById(R.id.detailsViewStub) as ViewStub
            detailsLayoutTitleLabel = row.findViewById(R.id.detailsLayoutTitleLabel) as TextView
            packageNameLabel = row.findViewById(R.id.packageNameLabel) as TextView
            packageNameValue = row.findViewById(R.id.packageNameValue) as TextView
            packageValidityLabel = row.findViewById(R.id.packageValidityLabel) as TextView
            packageValidityValue = row.findViewById(R.id.packageValidityValue) as TextView
            packageNameValidityHolderLayout =
                row.findViewById(R.id.packageNameValidityHolderLayout) as ConstraintLayout
            bonusesExpandableLayout =
                row.findViewById(R.id.bonusesExpandableLayout) as ExpandableLayout
            bonusesContentLayout = row.findViewById(R.id.bonusesContentLayout) as LinearLayout
            layoutInsideScrollView = row.findViewById(R.id.layoutInsideScrollView) as RelativeLayout
            detailsLayout = row.findViewById(R.id.detailsLayout) as LinearLayout
            detailsLayoutExpandIcon = row.findViewById(R.id.detailsLayoutExpandIcon) as ImageView
            detailsHeaderLayout = row.findViewById(R.id.detailsHeaderLayout) as RelativeLayout
            detailsExpandableLayout =
                row.findViewById(R.id.detailsExpandableLayout) as ExpandableLayout

            ratingStarsLayout = row.findViewById(R.id.ratingStarsLayout)
            starOne = row.findViewById(R.id.star1)
            starTwo = row.findViewById(R.id.star2)
            starThree = row.findViewById(R.id.star3)
            starFour = row.findViewById(R.id.star4)
            starFive = row.findViewById(R.id.star5)

        }//constructor ends
    }//SupplementaryOffersCardAdapterViewHolder ends

    private inner class SupplementaryFilter : Filter() {

        override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {
            val results = Filter.FilterResults()
            if (constraint == null) {
                //do nothing
            } else if (constraint.toString().trim { it <= ' ' }.isEmpty()) {
                offersSearchResultListener!!.onSearchResult(true)
                results.values = offerArrayListTemp
                results.count = offerArrayListTemp!!.size
            } else {
                val filteredResult = ArrayList<Offer>()
                /**check if search is applied for the filter or search from the input field*/
                if (constraint.toString().startsWith("fromPopup")) {
                    val constraintKey = constraint.toString().replace("fromPopup", "")
                    val arrayOfConstraintKeys = constraintKey.split("juni1289")

                    logE(
                        "keyConstraint",
                        "constraintKey111:::".plus(constraint.toString()),
                        fromClass,
                        "appliedFilterSearch"
                    )

                    for (its in arrayOfConstraintKeys.indices) {
                        val keyConstraint = arrayOfConstraintKeys[its]
                        for (offer in offerArrayListTemp!!) {
                            if (offer != null && offer.header != null && offer.header!!.appOfferFilter != null
                                && hasValue(offer.header!!.appOfferFilter)
                            ) {
                                if (hasValue(keyConstraint)) {
                                    logE(
                                        "keyConstraint",
                                        "keyConstraint:::".plus(keyConstraint),
                                        fromClass,
                                        "appliedFilterSearch"
                                    )
                                    if (offer.header!!.appOfferFilter!!.equals(
                                            keyConstraint,
                                            ignoreCase = true
                                        )
                                    ) {
                                        //found the result
                                        logE(
                                            "keyConstraint",
                                            "foundResult:::",
                                            fromClass,
                                            "appliedFilterSearch"
                                        )
                                        filteredResult.add(offer)
                                    }
                                }
                            }
                        }
                    }
                } else {
                    for (offer in offerArrayListTemp!!) {
                        if (offer!!.header != null &&
                            offer.header!!.offerName!!.toLowerCase()
                                .contains(constraint.toString().toLowerCase())
                        ) {
                            filteredResult.add(offer)
                        }
                    }
                }

                if (filteredResult.size > 0) {
                    offersSearchResultListener!!.onSearchResult(true)
                    results.values = filteredResult
                    results.count = filteredResult.size
                } else {
                    offersSearchResultListener!!.onSearchResult(false)
                }
            }
            return results
        }

        override fun publishResults(constraint: CharSequence, results: Filter.FilterResults) {
            if (results.count > 0) {
                offersSearchResultListener!!.onSearchResult(true)
                supplementaryOffersList = results.values as ArrayList<Offer?>
                notifyDataSetChanged()
            } else {
                offersSearchResultListener!!.onSearchResult(false)
            }
        }
    }

}//class ends