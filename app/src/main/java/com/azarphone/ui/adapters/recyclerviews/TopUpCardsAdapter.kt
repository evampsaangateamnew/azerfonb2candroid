package com.azarphone.ui.adapters.recyclerviews

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.azarphone.R
import com.azarphone.api.pojo.response.topup.savedcards.SavedCardsItem
import com.azarphone.eventhandler.TopUpItemSelectEvents
import com.daimajia.swipe.SwipeLayout

class TopUpCardsAdapter(private val dataSet: ArrayList<SavedCardsItem>, val mContext: Context, private val topUpItemSelectEvents: TopUpItemSelectEvents) : RecyclerView.Adapter<TopUpCardsAdapter.ViewHolder>() {
    private var selectedItem = 0
    var onDelete: ((SavedCardsItem, Int) -> Unit) ? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.topup_cards_item,
                parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val getSavedCardsItems: SavedCardsItem? = dataSet[position]
        holder.cardNumber.setText(getSavedCardsItems?.cardMaskNumber)
        //check if position is selected Item then update selected row color
        //else set properties to normal.
        if (position == selectedItem) {
            holder.cardNumber.setTextColor(mContext!!.resources.getColor(R.color.colorPrimary))
            holder.radioButton.isChecked = true
        } else {
            holder.cardNumber.setTextColor(mContext!!.resources.getColor(R.color.brown_grey))
            holder.radioButton.isChecked = false
        }
        //set Click listener to radioButton
        holder.radioButton.setOnClickListener { v: View? -> updateItemCheckState(position) }
        //set Click listener to RecyclerView Row

        holder.swipeLayout.close()
        //set Click listener to RecyclerView Row
        holder.itemLayout.setOnClickListener  { view: View? -> updateItemCheckState(position) }
        if (position == (dataSet?.size?.minus(1))) {
            //check if true, then show Add card option & hide radio button for Add card Option in the recyclerview.
            holder.cardIcon.setImageResource(R.drawable.ic_add_card)
            holder.cardNumber.setTextColor(mContext!!.resources.getColor(R.color.brown_grey))
            holder.radioButton.visibility = View.GONE
            holder.deleteButton.visibility = View.GONE

            if (dataSet.size > 1) {
                holder.cardNumber.text = mContext.getString(R.string.another_card)
            } else {
                holder.cardNumber.text = mContext.getString(R.string.add_card)
            }

        } else {
            holder.radioButton.visibility = View.VISIBLE
            //else show simple card according to its type
            if (getSavedCardsItems?.cardType?.contains("v") == true) {
                holder.cardIcon.setImageResource(R.drawable.ic_visa_card)
            } else if (getSavedCardsItems?.cardType?.contains("m") == true) {
                holder.cardIcon.setImageResource(R.drawable.ic_master_card)
            }
        }
        holder.deleteButton.setOnClickListener {
            onDelete?.invoke(dataSet[position],position)
        }
    } //end onBindViewHolder


    private fun updateItemCheckState(position: Int) {

        if (dataSet.size > 1 && position == selectedItem) {
            selectedItem = -1
            notifyDataSetChanged()
            dataSet[position]?.paymentKey?.let { topUpItemSelectEvents.onItemSelected(selectedItem, "") }
        } else {
            val copyOfLastCheckedPosition = selectedItem
            selectedItem = position
            notifyItemChanged(copyOfLastCheckedPosition)
            notifyItemChanged(selectedItem)
            dataSet[position]?.paymentKey?.let { topUpItemSelectEvents.onItemSelected(position, it) }
        }

    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    fun deleteItem(position: Int) {
        dataSet.removeAt(position)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var cardNumber: TextView = itemView.findViewById(R.id.card_text)
        var cardIcon: ImageView = itemView.findViewById(R.id.card_icon)
        var radioButton: RadioButton = itemView.findViewById(R.id.radio_btn)
        var deleteButton:TextView = itemView.findViewById(R.id.deleteButton)
        var swipeLayout: SwipeLayout = itemView.findViewById(R.id.swipeLayout)
        var itemLayout: ConstraintLayout = itemView.findViewById(R.id.cl_main_item)

    }
}
