package com.azarphone.ui.adapters.spinners

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.azarphone.R
import com.azarphone.api.pojo.response.predefinedata.CardType
import com.azarphone.eventhandler.TopUpItemSelectEvents
import kotlinx.android.synthetic.main.layout_cardtype_spinner.view.*

class CardTypeSpinnerAdapter(val context: Context,
                             private val topUpHelperModels: ArrayList<CardType>,
                             private val topUpItemSelectEvents: TopUpItemSelectEvents) : BaseAdapter() {

    override fun getCount(): Int {
        return topUpHelperModels.size
    }

    override fun getItem(position: Int): Any {
        return topUpHelperModels[position]
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_cardtype_spinner, null)
        view.topUpTitle.text = topUpHelperModels[position].value
        view.setOnClickListener {
            topUpHelperModels[position].key.toString().let { it1 -> topUpItemSelectEvents.onItemSelected(position, it1) }
        }
        return view
    }

}
