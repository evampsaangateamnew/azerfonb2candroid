package com.azarphone.ui.adapters.recyclerviews

import android.content.Context
import android.content.Intent
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewStub
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.response.inappsurvey.InAppSurvey
import com.azarphone.api.pojo.response.inappsurvey.Surveys
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.mysubscriptions.helper.HeaderMain
import com.azarphone.api.pojo.response.mysubscriptions.helper.Subscriptions
import com.azarphone.api.pojo.response.mysubscriptions.helper.SubscriptionsDetailsMain
import com.azarphone.api.pojo.response.mysubscriptions.response.*
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.OnClickSurveySubmit
import com.azarphone.eventhandler.UpdateListOnSuccessfullySurvey
import com.azarphone.ui.activities.mysubscriptions.MySubscriptionsViewModel
import com.azarphone.ui.activities.topup.TopupActivity
import com.azarphone.ui.dialogs.InAppFeedbackDialog
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.validators.isNumeric
import com.azarphone.widgets.mysubscriptions.*
import com.es.tec.LocalSharedPrefStorage
import net.cachapa.expandablelayout.ExpandableLayout
import java.text.ParseException


/**
 * @author Junaid Hassan on 27, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class MySubscriptionsCardAdapter(
    context: Context,
    mysubscriptions: List<Subscriptions?>?,
    isRoaming: Boolean,
    selectedCountryName: String,
    selectedCountryFlag: String,
    isSpecialOffers: Boolean,
    private val viewModel:
    MySubscriptionsViewModel
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

    private var isHeaderLayoutExpanded: Boolean = false
    private var isDetailsLayoutExpanded: Boolean = false

    private var isDetailsRoundingView = false
    private var isDetailsTextWithTitleView = false
    private var isDetailsTextWithoutTitleView = false
    private var isDetailsTextWithPointsView = false
    private var isRoamingView = false

    private var mysubscriptions: List<Subscriptions?>? = null
    private lateinit var detailsView: View
    private val fromClass = "MySubscriptionsCardAdapter"
    private var context: Context? = null
    private val HANDLER_TIME = 100L
    private var isRoaming: Boolean = false
    private var selectedCountryName = ""
    private var selectedCountryFlag = ""
    private var isSpecialOffers = false

    init {
        this.mysubscriptions = mysubscriptions
        this.context = context
        this.isRoaming = isRoaming
        this.selectedCountryFlag = selectedCountryFlag
        this.selectedCountryName = selectedCountryName
        this.isSpecialOffers = isSpecialOffers
    }//init ends

    override fun onCreateViewHolder(
        parent: ViewGroup,
        p1: Int
    ): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_mysubscriptions_card_adapter, parent, false)
        return MySubscriptionsCardAdapterViewHolder(view)
    }//onCreateViewHolder ends

    override fun getItemCount(): Int {
        return mysubscriptions?.size ?: 0
    }//getItemCount ends

    override fun onBindViewHolder(
        holder: androidx.recyclerview.widget.RecyclerView.ViewHolder,
        position: Int
    ) {
        val viewHolder = holder as MySubscriptionsCardAdapterViewHolder

        var rateStars = 0

        /**button texts*/
        viewHolder.topUpButton!!.text = getTopupLabel()
        viewHolder.deactivateButton!!.text = getDeactivateLabel()
        viewHolder.renewButton!!.text = getRenewButtonLabel()
        viewHolder.subscribedButton!!.text = getSubscribedButtonLabel()

        /**buttons marquee*/
        viewHolder.deactivateButton!!.isSelected = true
        viewHolder.renewButton!!.isSelected = true
        viewHolder.subscribedButton!!.isSelected = true
        viewHolder.topUpButton!!.isSelected = true

        viewHolder.detailsLayoutTitleLabel!!.text = getLocalizedDetailsTabTitle()

        /** set the header section*/
        if (mysubscriptions != null) {
            if (mysubscriptions!![position]!!.header != null) {
                Handler().postDelayed({
                    prepareHeaderSection(
                        mysubscriptions!![position]!!.header,
                        mysubscriptions!![position]!!,
                        viewHolder
                    )
                }, HANDLER_TIME)
            }//if (mysubscriptions!![position]!!.header != null) ends

            /**create details section here*/
            if (mysubscriptions!![position]!!.details != null) {
                Handler().postDelayed({
                    viewHolder.detailsViewStub!!.layoutResource =
                        R.layout.layout_subscriptions_details_content_layout

                    if (viewHolder.detailsViewStub!!.parent != null) {
                        detailsView = viewHolder.detailsViewStub!!.inflate()
                    }
                    setupDetailsSection(detailsView, mysubscriptions!![position]!!.details!!)
                }, HANDLER_TIME)
            } else {
                viewHolder.detailsLayout!!.visibility = View.GONE
            }//if (mysubscriptions!![position]!!.details != null) ends

            /**set the expandable layouts behavior*/
            initExpandableLayoutsBehavior(position, viewHolder)

            /**register UI listeners*/
            initUIListeners(viewHolder, position)
        }//if(mysubscriptions!=null) ends

        /**set the card height*/
        if (android.os.Build.VERSION.SDK_INT == android.os.Build.VERSION_CODES.M) {
            val params = viewHolder.mainContainer!!.layoutParams
            params.height = context!!.resources.getDimensionPixelSize(R.dimen._360sdp)
            viewHolder.mainContainer!!.layoutParams = params
        } else {
            val params = viewHolder.mainContainer!!.layoutParams
            params.height = context!!.resources.getDimensionPixelSize(R.dimen._380sdp)
            viewHolder.mainContainer!!.layoutParams = params
        }

        /** setting stars of rating*/
        val surveyData = UserDataManager.getInAppSurvey()?.data
        val findSurvey =
            surveyData?.findSurvey(ConstantsUtility.InAppSurveyConstants.BUNDLE_SUBSCRIBED_PAGE)
        findSurvey?.let {
            surveyData.userSurveys?.let { userSurveyData ->
                for (i in userSurveyData.indices) {
                    if (userSurveyData[i].offeringId!! == mysubscriptions?.get(position)?.header?.offeringId ?: 0) {
                        userSurveyData[i].answer?.let {
                            userSurveyData[i].surveyId?.let { userSurveyId ->
                                val survey =
                                    surveyData.findSurveyById(userSurveyId.toIntOrNull() ?: 0)
                                survey?.let { requiredSurvey ->
                                    val answerId = userSurveyData[i].answerId
                                    val questionId = userSurveyData[i].questionId
                                    if (isNumeric(answerId)) {
                                        if (isNumeric(questionId)) {
                                            rateStars = setStars(
                                                answerId!!.toInt(),
                                                questionId!!.toInt(),
                                                requiredSurvey,
                                                viewHolder
                                            )
                                        }
                                    }
                                }
                            }
                        } ?: run {
                            rateStars = setStars(0, 0, null, viewHolder)
                        }
                    }
                }
            }
        }?:run{
            viewHolder.ratingStarsLayout?.visibility = View.GONE
        }

        viewHolder.ratingStarsLayout?.setOnClickListener {
            val tariffId = mysubscriptions?.get(position)?.header?.offeringId ?: "0"
            context?.let {
                inAppFeedback(it, tariffId, rateStars, position)
            }

        }


    }//onBindViewHolder ends

    private fun inAppFeedback(context: Context, tariffId: String, rating: Int, position: Int) {

        var inAppFeedbackDialog: InAppFeedbackDialog? = null
        var currentVisit: Int =
            LocalSharedPrefStorage(context).getInt(LocalSharedPrefStorage.PREF_BUNDLE_SUBSCRIBED_PAGE)
        currentVisit += 1
        val inAppSurvey: InAppSurvey? = UserDataManager.getInAppSurvey()
        if ((inAppSurvey?.data) != null) {
            val surveys: Surveys? =
                inAppSurvey.data.findSurvey(ConstantsUtility.InAppSurveyConstants.BUNDLE_SUBSCRIBED_PAGE)
            if (surveys != null) {
                inAppFeedbackDialog = InAppFeedbackDialog(context, object : OnClickSurveySubmit {
                    override fun onClickSubmit(
                        uploadSurvey: UploadSurvey,
                        onTransactionComplete: String,
                        updateListOnSuccessfullySurvey: UpdateListOnSuccessfullySurvey?
                    ) {
                        viewModel.uploadInAppSurvey(
                            context,
                            uploadSurvey,
                            onTransactionComplete,
                            updateListOnSuccessfullySurvey
                        )
                        if (inAppFeedbackDialog?.isShowing == true) {
                            inAppFeedbackDialog?.dismiss()
                        }
                    }

                })
                if (surveys.questions != null) {
                    currentVisit = 0
                    inAppFeedbackDialog.setUpdateListOnSuccessfullySurveyListener(object :
                        UpdateListOnSuccessfullySurvey {
                        override fun updateListOnSuccessfullySurvey() {
                            updateList(position)
                        }
                    })
                    // 1 for tariff and 2 for supplementary
                    inAppFeedbackDialog.showTariffDialog(surveys, "2", tariffId, rating)
                }
            }
        }
        LocalSharedPrefStorage(context).addInt(
            LocalSharedPrefStorage.PREF_BUNDLE_SUBSCRIBED_PAGE,
            currentVisit
        )
    }

    private fun updateList(position: Int) {
        notifyItemChanged(position)
    }

    private fun setStars(
        answerId: Int,
        questionId: Int,
        survey: Surveys?,
        viewHolder: MySubscriptionsCardAdapterViewHolder
    ): Int {
        survey?.let { currentSurvey ->
            val question = currentSurvey.findQuestion(questionId)
            question?.let { currentQuestion ->
                val answers = currentQuestion.answers
                answers?.let { currentAnswer ->
                    if (currentAnswer.isNotEmpty() && currentAnswer[0].id == answerId) {
                        viewHolder.starOne?.isChecked = true
                        viewHolder.starTwo?.isChecked = false
                        viewHolder.starThree?.isChecked = false
                        viewHolder.starFour?.isChecked = false
                        viewHolder.starFive?.isChecked = false
                        return 1
                    } else if (currentAnswer.size > 1 && currentAnswer[1].id == answerId) {
                        viewHolder.starOne?.isChecked = true
                        viewHolder.starTwo?.isChecked = true
                        viewHolder.starThree?.isChecked = false
                        viewHolder.starFour?.isChecked = false
                        viewHolder.starFive?.isChecked = false
                        return 2
                    } else if (currentAnswer.size > 2 && currentAnswer[2].id == answerId) {
                        viewHolder.starOne?.isChecked = true
                        viewHolder.starTwo?.isChecked = true
                        viewHolder.starThree?.isChecked = true
                        viewHolder.starFour?.isChecked = false
                        viewHolder.starFive?.isChecked = false
                        return 3
                    } else if (currentAnswer.size > 3 && currentAnswer[3].id == answerId) {
                        viewHolder.starOne?.isChecked = true
                        viewHolder.starTwo?.isChecked = true
                        viewHolder.starThree?.isChecked = true
                        viewHolder.starFour?.isChecked = true
                        viewHolder.starFive?.isChecked = false
                        return 4
                    } else if (currentAnswer.size > 4 && currentAnswer[4].id == answerId) {
                        viewHolder.starOne?.isChecked = true
                        viewHolder.starTwo?.isChecked = true
                        viewHolder.starThree?.isChecked = true
                        viewHolder.starFour?.isChecked = true
                        viewHolder.starFive?.isChecked = true
                        return 5
                    }

                } ?: run {
                    viewHolder.starOne?.isChecked = false
                    viewHolder.starTwo?.isChecked = false
                    viewHolder.starThree?.isChecked = false
                    viewHolder.starFour?.isChecked = false
                    viewHolder.starFive?.isChecked = false
                    return 0
                }
            } ?: run {
                viewHolder.starOne?.isChecked = false
                viewHolder.starTwo?.isChecked = false
                viewHolder.starThree?.isChecked = false
                viewHolder.starFour?.isChecked = false
                viewHolder.starFive?.isChecked = false
                return 0
            }
        } ?: run {
            viewHolder.starOne?.isChecked = false
            viewHolder.starTwo?.isChecked = false
            viewHolder.starThree?.isChecked = false
            viewHolder.starFour?.isChecked = false
            viewHolder.starFive?.isChecked = false
            return 0
        }
        return 0
    }

    private fun initExpandableLayoutsBehavior(
        position: Int,
        viewHolder: MySubscriptionsCardAdapterViewHolder
    ) {
        if (isDetailsLayoutExpanded || isHeaderLayoutExpanded) {
            logE("tarX123", "anyone true", fromClass, "expandedPosition")
            if (isHeaderLayoutExpanded) {
                logE("tarX123", "isHeaderLayoutExpanded", fromClass, "isHeaderLayoutExpanded")
                collapseDetailsLayout(viewHolder)
                expandUsageLayout(viewHolder)
            } else if (isDetailsLayoutExpanded) {
                logE("tarX123", "isDetailsLayoutExpanded", fromClass, "isDetailsLayoutExpanded")
                collapseUsageLayout(viewHolder)
                expandDetailsLayout(viewHolder)
            }
        } else {
            logE("tarX123", "allllll false", fromClass, "expandedPosition NOT")
            if (mysubscriptions!![position]!!.header != null) {
                /**expand the usages section*/
                expandUsageLayout(viewHolder)
                collapseDetailsLayout(viewHolder)
            } else {
                collapseUsageLayout(viewHolder)
                expandDetailsLayout(viewHolder)
            }
        }

    }//initExpandableLayoutsBehavior ends

    private fun expandUsageLayout(viewHolder: MySubscriptionsCardAdapterViewHolder) {
        viewHolder.usagesExpandableLayout!!.expand()
    }//expandUsageLayout ends

    private fun collapseUsageLayout(viewHolder: MySubscriptionsCardAdapterViewHolder) {
        viewHolder.usagesExpandableLayout!!.collapse()
    }//collapseBonusLayout ends

    private fun expandDetailsLayout(viewHolder: MySubscriptionsCardAdapterViewHolder) {
        viewHolder.detailsExpandableLayout!!.expand()
        viewHolder.detailsLayoutExpandIcon!!.setImageResource(R.drawable.faqminus)
        viewHolder.detailsLayoutTitleLabel!!.setTextColor(context!!.resources.getColor(R.color.colorPrimary))
    }//expandDetailsLayout ends

    private fun collapseDetailsLayout(viewHolder: MySubscriptionsCardAdapterViewHolder) {
        viewHolder.detailsExpandableLayout!!.collapse()
        viewHolder.detailsLayoutExpandIcon!!.setImageResource(R.drawable.faqplus)
        viewHolder.detailsLayoutTitleLabel!!.setTextColor(context!!.resources.getColor(R.color.purplish_brown))
    }//collapseDetailsLayout ends

    private fun setupDetailsSection(detailsView: View, details: SubscriptionsDetailsMain) {
        val detailsRoundingHeaderLayout: LinearLayout =
            detailsView.findViewById(R.id.detailsRoundingHeaderLayout) as LinearLayout
        val viewBelowDetailsRoundingContentLayout: View =
            detailsView.findViewById(R.id.viewBelowDetailsRoundingContentLayout) as View
        val detailsTextWithTitleText: TextView =
            detailsView.findViewById(R.id.detailsTextWithTitleText) as TextView
        val viewBelowDetailsTextWithTitleHeaderLabel: View =
            detailsView.findViewById(R.id.viewBelowDetailsTextWithTitleHeaderLabel) as View
        val viewBelowDetailsTextWithPointsContentLayout: View =
            detailsView.findViewById(R.id.viewBelowDetailsTextWithPointsContentLayout) as View
        val detailsRoamingHeaderLabel: TextView =
            detailsView.findViewById(R.id.detailsRoamingHeaderLabel) as TextView
        val detailsTextWithPointsContentLayout: LinearLayout =
            detailsView.findViewById(R.id.detailsTextWithPointsContentLayout) as LinearLayout
        val detailPricesHeaderLayout: LinearLayout =
            detailsView.findViewById(R.id.detailPricesHeaderLayout) as LinearLayout
        val detailsTextWithTitleHeaderLabel: TextView =
            detailsView.findViewById(R.id.detailsTextWithTitleHeaderLabel) as TextView
        val detailRoamingContentLayout: LinearLayout =
            detailsView.findViewById(R.id.detailRoamingContentLayout) as LinearLayout
        val detailsTextWithoutTitleLabel: TextView =
            detailsView.findViewById(R.id.detailsTextWithoutTitleLabel) as TextView
        val viewBelowDetailsTextWithoutTitleLabel: View =
            detailsView.findViewById(R.id.viewBelowDetailsTextWithoutTitleLabel) as View
        val detailsTextWithPointsHeaderLabel =
            detailsView.findViewById(R.id.detailsTextWithPointsHeaderLabel) as TextView
        val detailsTextWithPointsHeaderIcon =
            detailsView.findViewById(R.id.detailsTextWithPointsHeaderIcon) as ImageView
        val detailsTextWithPointsHeaderLayout =
            detailsView.findViewById(R.id.detailsTextWithPointsHeaderLayout) as LinearLayout
        val detailsRoamingHeaderIcon: ImageView =
            detailsView.findViewById(R.id.detailsRoamingHeaderIcon) as ImageView

        /**set the details prices section*/
        if (details.price != null) {
            setupDetailsPriceSection(details.price, detailsView)
        } else {
            detailPricesHeaderLayout.visibility = View.GONE
        }

        /** set the details rounding section*/
        if (details.rounding != null) {
            isDetailsRoundingView = true
            setupDetailsRoundingSection(details.rounding, detailsView)//see here juna12891226
        } else {
            isDetailsRoundingView = false
            detailsRoundingHeaderLayout.visibility = View.GONE
        }

        /**set the text with title layout*/
        if (details.textWithTitle != null) {
            isDetailsTextWithTitleView = true
            setupTextWithTitleSection(details.textWithTitle, detailsView)
        } else {
            detailsTextWithTitleHeaderLabel.visibility = View.GONE
            detailsTextWithTitleText.visibility = View.GONE
            isDetailsTextWithTitleView = false
        }

        /**set the text with points layout*/
        if (details.textWithPoints != null) {
            //juni12891226nosi
            isDetailsTextWithPointsView = true
            /**set the title*/
            if (details.textWithPoints.title != null && hasValue(details.textWithPoints.title)) {
                detailsTextWithPointsHeaderLabel.text = details.textWithPoints.title
                detailsTextWithPointsHeaderLabel.isSelected = true
            }

            /**set the title icon*/
            if (details.textWithPoints.titleIcon != null && hasValue(details.textWithPoints.titleIcon)) {
                detailsTextWithPointsHeaderIcon.setImageResource(getCardAdapterMappedImage(details.textWithPoints.titleIcon))
            }

            /**set the points*/
            if (details.textWithPoints.pointsList != null && details.textWithPoints.pointsList.isNotEmpty()) {
                setupTextWithPointsSection(details.textWithPoints.pointsList, detailsView)
            } else {
                isDetailsTextWithPointsView = false
                detailsTextWithPointsContentLayout.visibility = View.GONE
            }

            /**title and icon not there*/
            if (!hasValue(details.textWithPoints.title) &&
                !hasValue(details.textWithPoints.titleIcon)
            ) {
                detailsTextWithPointsHeaderLayout.visibility = View.GONE
            }

            /**nothing is there*/
            if (!hasValue(details.textWithPoints.title) &&
                !hasValue(details.textWithPoints.titleIcon) &&
                details.textWithPoints.pointsList.isNullOrEmpty()
            ) {
                isDetailsTextWithPointsView = false
                detailsTextWithPointsContentLayout.visibility = View.GONE
            }
        } else {
            detailsTextWithPointsContentLayout.visibility = View.GONE
            isDetailsTextWithPointsView = false
        }

        /**set the text without title*/
        if (details.textWithOutTitle != null) {
            isDetailsTextWithoutTitleView = true
            if (details.textWithOutTitle.description != null && hasValue(details.textWithOutTitle.description)) {
                detailsTextWithoutTitleLabel.text = details.textWithOutTitle.description
            } else {
                detailsTextWithoutTitleLabel.visibility = View.GONE
                isDetailsTextWithoutTitleView = false
            }
        } else {
            detailsTextWithoutTitleLabel.visibility = View.GONE
            isDetailsTextWithoutTitleView = false
        }

        /**set the roaming layout*/
        if (details.roamingDetails != null) {
            isRoamingView = true
            logE("roamX", "roamingDetails is not null", fromClass, "setupDetailsSection")
            if (details.roamingDetails.descriptionAbove != null && hasValue(details.roamingDetails.descriptionAbove)) {
                detailsRoamingHeaderLabel.text = details.roamingDetails.descriptionAbove
            } else {
                detailsRoamingHeaderLabel.visibility = View.GONE
            }

            if (details.roamingDetails.roamingIcon != null && hasValue(details.roamingDetails.roamingIcon)) {
                detailsRoamingHeaderIcon.setImageResource(getCardAdapterMappedImage(details.roamingDetails.roamingIcon))
            }

            //set the countries
            if (details.roamingDetails.roamingDetailsCountriesList != null && !details.roamingDetails.roamingDetailsCountriesList.isEmpty()) {
                setupRoamingSection(details.roamingDetails.roamingDetailsCountriesList, detailsView)
            } else {
                detailRoamingContentLayout.visibility = View.GONE
            }
        } else {
            logE("roamX", "roamingDetails is null", fromClass, "setupDetailsSection")
            detailsRoamingHeaderLabel.visibility = View.GONE
            detailRoamingContentLayout.visibility = View.GONE
            isRoamingView = false
        }

        if (isDetailsRoundingView) {
            viewBelowDetailsRoundingContentLayout.visibility = View.VISIBLE
            if (!isDetailsTextWithTitleView) {
                viewBelowDetailsRoundingContentLayout.visibility = View.GONE
                if (!isDetailsTextWithoutTitleView) {
                    viewBelowDetailsRoundingContentLayout.visibility = View.GONE
                    if (!isDetailsTextWithPointsView) {
                        viewBelowDetailsRoundingContentLayout.visibility = View.GONE
                        if (!isRoamingView) {
                            viewBelowDetailsRoundingContentLayout.visibility = View.GONE
                        } else {
                            viewBelowDetailsRoundingContentLayout.visibility = View.VISIBLE
                        }//if (!isRoamingView) ends
                    } else {
                        viewBelowDetailsRoundingContentLayout.visibility = View.VISIBLE
                    }//if (!isDetailsTextWithPointsView) ends
                } else {
                    viewBelowDetailsRoundingContentLayout.visibility = View.VISIBLE
                }//if (!isDetailsTextWithoutTitleView) ends
            } else {
                viewBelowDetailsRoundingContentLayout.visibility = View.VISIBLE
            }//if (!isDetailsTextWithTitleView) ends
        }//if (isDetailsRoundingView) ends

        if (isDetailsTextWithTitleView) {
            viewBelowDetailsTextWithTitleHeaderLabel.visibility = View.VISIBLE
            if (!isDetailsTextWithoutTitleView) {
                viewBelowDetailsTextWithTitleHeaderLabel.visibility = View.GONE
                if (!isDetailsTextWithPointsView) {
                    viewBelowDetailsTextWithTitleHeaderLabel.visibility = View.GONE
                    if (!isRoamingView) {
                        viewBelowDetailsTextWithTitleHeaderLabel.visibility = View.GONE
                    } else {
                        viewBelowDetailsTextWithTitleHeaderLabel.visibility = View.VISIBLE
                    }//if (!isRoamingView) ends
                } else {
                    viewBelowDetailsTextWithTitleHeaderLabel.visibility = View.VISIBLE
                }// if (!isDetailsTextWithPointsView) ends
            } else {
                viewBelowDetailsTextWithTitleHeaderLabel.visibility = View.VISIBLE
            }//if (!isDetailsTextWithoutTitleView) ends
        }//if (isDetailsTextWithTitleView)  ends

        if (isDetailsTextWithoutTitleView) {
            viewBelowDetailsTextWithoutTitleLabel.visibility = View.VISIBLE
            if (!isDetailsTextWithPointsView) {
                viewBelowDetailsTextWithoutTitleLabel.visibility = View.GONE
                if (!isRoamingView) {
                    viewBelowDetailsTextWithoutTitleLabel.visibility = View.GONE
                } else {
                    viewBelowDetailsTextWithoutTitleLabel.visibility = View.VISIBLE
                }//if (!isRoamingView)  ends
            } else {
                viewBelowDetailsTextWithoutTitleLabel.visibility = View.VISIBLE
            }
        }//if (isDetailsTextWithoutTitleView) ends

        if (isDetailsTextWithPointsView) {
            viewBelowDetailsTextWithPointsContentLayout.visibility = View.VISIBLE
            if (!isRoamingView) {
                viewBelowDetailsTextWithPointsContentLayout.visibility = View.GONE
            } else {
                viewBelowDetailsTextWithPointsContentLayout.visibility = View.VISIBLE
            }//if (!isRoamingView) ends
        }//if (isDetailsTextWithPointsView) ends
    }//setupDetailsSection ends

    private fun setupDetailsPriceSection(price: List<PriceItem?>, detailsView: View) {
        val detailPricesHeaderLayout: LinearLayout =
            detailsView.findViewById(R.id.detailPricesHeaderLayout) as LinearLayout

        //iterate over the list and add the view items
        detailPricesHeaderLayout.removeAllViews()
        for (its in price.indices) {
            val subscriptionsDetailsPriceViewItem = SubscriptionsDetailsPriceViewItem(context!!)

            if (price[its]!!.iconName != null && hasValue(price[its]!!.iconName)) {
                subscriptionsDetailsPriceViewItem.getDetailsPriceIcon()
                    .setImageResource(getCardAdapterMappedImage(price[its]!!.iconName!!))
            }//if (price[its]!!.iconName != null && hasValue(price[its]!!.iconName)) ends

            if (price[its]!!.title != null && hasValue(price[its]!!.title)) {
                subscriptionsDetailsPriceViewItem.getDetailsHeaderLabel().text = price[its]!!.title
                subscriptionsDetailsPriceViewItem.getDetailsHeaderLabel().isSelected = true
            }//if (price[its]!!.title != null && hasValue(price[its]!!.title)) ends

            if (price[its]!!.value != null && hasValue(price[its]!!.value)) {
                if (price[its]!!.offersCurrency != null && hasValue(price[its]!!.offersCurrency)) {
                    if (price[its]!!.offersCurrency.equals(
                            ConstantsUtility.TariffConstants.PRICE_OFFERS_CURRENCY,
                            ignoreCase = true
                        )
                    ) {
                        val value = price[its]!!.value
                        subscriptionsDetailsPriceViewItem.getDetailsHeaderValue().text =
                            getManatConcatedString(
                                context!!, value!!, ConstantsUtility.TariffConstants.AZERI_SYMBOL,
                                0.25f, R.dimen._10ssp
                            )
                        subscriptionsDetailsPriceViewItem.getDetailsHeaderValue().isSelected = true
                    } else {
                        subscriptionsDetailsPriceViewItem.getDetailsHeaderValue().text =
                            price[its]!!.value
                        subscriptionsDetailsPriceViewItem.getDetailsHeaderValue().isSelected = true
                    }
                } else {
                    subscriptionsDetailsPriceViewItem.getDetailsHeaderValue().text =
                        price[its]!!.value
                    subscriptionsDetailsPriceViewItem.getDetailsHeaderValue().isSelected = true
                }
            }

            if (!hasValue(price[its]!!.value) && !hasValue(price[its]!!.title)) {
                subscriptionsDetailsPriceViewItem.getDetailsPriceViewItemLayout().visibility =
                    View.GONE
            }

            /**populate the content*/
            if (price[its]!!.attributeList != null && !price[its]!!.attributeList!!.isEmpty()) {
                subscriptionsDetailsPriceViewItem.getDetailsContentLayout().removeAllViews()
                //iterate over the attributes and populate the details price attributes view items
                for (iterations in price[its]!!.attributeList!!.indices) {
                    val subscriptionsDetailsPriceAttributesViewItem =
                        SubscriptionsDetailsPriceAttributesViewItem(context!!)

                    if (price[its]!!.attributeList!![iterations]!!.title != null && hasValue(price[its]!!.attributeList!![iterations]!!.title)) {
                        subscriptionsDetailsPriceAttributesViewItem.getDetailPriceAttributeViewItemLabel().text =
                            price[its]!!.attributeList!![iterations]!!.title
                        subscriptionsDetailsPriceAttributesViewItem.getDetailPriceAttributeViewItemLabel().isSelected =
                            true
                    }

                    if (price[its]!!.attributeList!![iterations]!!.value != null && hasValue(price[its]!!.attributeList!![iterations]!!.value)) {
                        if (price[its]!!.attributeList!![iterations]!!.unit != null && hasValue(
                                price[its]!!.attributeList!![iterations]!!.unit
                            )
                        ) {
                            if (price[its]!!.attributeList!![iterations]!!.unit!!.equals(
                                    ConstantsUtility.TariffConstants.ATTRIBUTE_LIST_UNIT,
                                    ignoreCase = true
                                )
                            ) {
                                val value = price[its]!!.attributeList!![iterations]!!.value
                                subscriptionsDetailsPriceAttributesViewItem.getDetailPriceAttributeItemValue().text =
                                    getManatConcatedString(
                                        context!!,
                                        value!!,
                                        ConstantsUtility.TariffConstants.AZERI_SYMBOL,
                                        0.25f,
                                        R.dimen._10ssp
                                    )
                                subscriptionsDetailsPriceAttributesViewItem.getDetailPriceAttributeItemValue().isSelected =
                                    true
                            } else {
                                subscriptionsDetailsPriceAttributesViewItem.getDetailPriceAttributeItemValue().text =
                                    price[its]!!.attributeList!![iterations]!!.value
                                subscriptionsDetailsPriceAttributesViewItem.getDetailPriceAttributeItemValue().isSelected =
                                    true
                            }
                        } else {
                            subscriptionsDetailsPriceAttributesViewItem.getDetailPriceAttributeItemValue().text =
                                price[its]!!.attributeList!![iterations]!!.value
                            subscriptionsDetailsPriceAttributesViewItem.getDetailPriceAttributeItemValue().isSelected =
                                true
                        }

                    }

                    if (!hasValue(price[its]!!.attributeList!![iterations]!!.value) && !hasValue(
                            price[its]!!.attributeList!![iterations]!!.title
                        )
                    ) {
                        subscriptionsDetailsPriceAttributesViewItem.getDetailPriceAttributeViewItemLayout().visibility =
                            View.GONE
                    }

                    subscriptionsDetailsPriceViewItem.getDetailsContentLayout()
                        .addView(subscriptionsDetailsPriceAttributesViewItem)
                }//for ends
            } else {
                subscriptionsDetailsPriceViewItem.getDetailsContentLayout().visibility = View.GONE
                subscriptionsDetailsPriceViewItem.getViewBelowPriceDetailsContentLayoutLayout().visibility =
                    View.GONE
            }//if (price[its]!!.attributeList != null && !price[its]!!.attributeList!!.isEmpty()) ends

            detailPricesHeaderLayout.addView(subscriptionsDetailsPriceViewItem)
        }//for ends
    }//setupDetailsPriceSection ends

    private fun setupRoamingSection(
        roamingDetailsCountriesList: List<RoamingDetailsCountriesListItem?>,
        detailsView: View
    ) {
        val detailRoamingContentLayout: LinearLayout =
            detailsView.findViewById(R.id.detailRoamingContentLayout) as LinearLayout

        detailRoamingContentLayout.removeAllViews()
        for (its in roamingDetailsCountriesList.indices) {
            val subscriptionsRoamingViewItem = SubscriptionsRoamingViewItem(context!!)

            if (roamingDetailsCountriesList[its]!!.countryName != null && hasValue(
                    roamingDetailsCountriesList[its]!!.countryName
                )
            ) {
                subscriptionsRoamingViewItem.getDetailsRoamingCountryLabel().text =
                    roamingDetailsCountriesList[its]!!.countryName
                subscriptionsRoamingViewItem.getDetailsRoamingCountryLabel().isSelected = true
            }

            if (roamingDetailsCountriesList[its]!!.flag != null && hasValue(
                    roamingDetailsCountriesList[its]!!.flag
                )
            ) {
                subscriptionsRoamingViewItem.getDetailsRoamingCountryFlag().setImageDrawable(
                    getImageFromAssests(
                        context!!,
                        roamingDetailsCountriesList[its]!!.flag!!
                    )
                )
            }

            /**set the operators*/
            if (roamingDetailsCountriesList[its]!!.operatorList != null && !roamingDetailsCountriesList[its]!!.operatorList!!.isEmpty()) {
                subscriptionsRoamingViewItem.getroamingOperatorsLayout().removeAllViews()

                for (operators in roamingDetailsCountriesList[its]!!.operatorList!!.indices) {
                    val subscriptionsRoamingOperatorsViewItem =
                        SubscriptionsRoamingOperatorsViewItem(context!!)

                    if (roamingDetailsCountriesList[its]!!.operatorList!![operators] != null &&
                        hasValue(roamingDetailsCountriesList[its]!!.operatorList!![operators])
                    ) {
                        subscriptionsRoamingOperatorsViewItem.getRoamingOperatorViewItemValue().text =
                            roamingDetailsCountriesList[its]!!.operatorList!![operators].toString()
                        subscriptionsRoamingOperatorsViewItem.getRoamingOperatorViewItemValue().isSelected =
                            true
                    }

                    subscriptionsRoamingViewItem.getroamingOperatorsLayout()
                        .addView(subscriptionsRoamingOperatorsViewItem)
                }//for operators end
            }

            detailRoamingContentLayout.addView(subscriptionsRoamingViewItem)
        }//for ends

    }//setupRoamingSection ends

    private fun setupTextWithPointsSection(pointsList: List<String?>, detailsView: View) {
        val detailsTextWithPointsContentLayout: LinearLayout =
            detailsView.findViewById(R.id.detailsTextWithPointsContentLayout) as LinearLayout

        detailsTextWithPointsContentLayout.removeAllViews()
        for (its in pointsList.indices) {
            val subscriptionsTextWithPointsViewItem = SubscriptionsTextWithPointsViewItem(context!!)

            if (pointsList[its] != null && hasValue(pointsList[its].toString())) {
                subscriptionsTextWithPointsViewItem.getTextWithPointsViewItemValue().text =
                    pointsList[its].toString()
            } else {
                subscriptionsTextWithPointsViewItem.getTextWithPointsViewItemValue().visibility =
                    View.GONE
            }//if (pointsList[its] != null && hasValue(pointsList[its].toString())) ends

            if (!hasValue(pointsList[its].toString())) {
                subscriptionsTextWithPointsViewItem.getTextWithPointsViewItemLayout().visibility =
                    View.GONE
            }

            detailsTextWithPointsContentLayout.addView(subscriptionsTextWithPointsViewItem)
        }//for ends
    }//setupTextWithPointsSection ends

    private fun setupTextWithTitleSection(textWithTitle: TextWithTitle, detailsView: View) {
        val detailsTextWithTitleHeaderLabel: TextView =
            detailsView.findViewById(R.id.detailsTextWithTitleHeaderLabel) as TextView
        val detailsTextWithTitleText: TextView =
            detailsView.findViewById(R.id.detailsTextWithTitleText) as TextView

        if (textWithTitle.title != null && hasValue(textWithTitle.title)) {
            detailsTextWithTitleHeaderLabel.text = textWithTitle.title
            detailsTextWithTitleHeaderLabel.isSelected = true
        } else {
            detailsTextWithTitleHeaderLabel.visibility = View.GONE
        }

        if (textWithTitle.text != null && hasValue(textWithTitle.text)) {
            detailsTextWithTitleText.text = textWithTitle.text
        } else {
            detailsTextWithTitleText.visibility = View.GONE
        }

        if (!hasValue(textWithTitle.title) && !hasValue(textWithTitle.text)) {
            isDetailsTextWithTitleView = false
            detailsTextWithTitleText.visibility = View.GONE
            detailsTextWithTitleHeaderLabel.visibility = View.GONE
        }
    }//setupTextWithTitleSection ends

    private fun setupDetailsRoundingSection(rounding: Rounding, detailsView: View) {
        val detailsRoundingHeaderIcon: ImageView =
            detailsView.findViewById(R.id.detailsRoundingHeaderIcon) as ImageView
        val detailsRoundingHeaderLabel: TextView =
            detailsView.findViewById(R.id.detailsRoundingHeaderLabel) as TextView
        val detailsRoundingHeaderValue: TextView =
            detailsView.findViewById(R.id.detailsRoundingHeaderValue) as TextView
        val detailsRoundingContentLayout: LinearLayout =
            detailsView.findViewById(R.id.detailsRoundingContentLayout) as LinearLayout

        //rounding title here
        if (rounding.title != null && hasValue(rounding.title)) {
            detailsRoundingHeaderLabel.text = rounding.title
            detailsRoundingHeaderLabel.isSelected = true
        }//if (rounding.title != null && hasValue(rounding.title)) ends

        //rounding titleValueRight here
        if (rounding.value != null && hasValue(rounding.value)) {
            detailsRoundingHeaderValue.text = rounding.value
            detailsRoundingHeaderValue.isSelected = true
        }//if (rounding.value != null && hasValue(rounding.value)) ends

        //rounding icon here
        if (rounding.iconName != null && hasValue(rounding.iconName)) {
            detailsRoundingHeaderIcon.setImageResource(getCardAdapterMappedImage(rounding.iconName))
        }//if (rounding.iconName != null && hasValue(rounding.iconName)) ends

        /**set the sms attributes*/
        if (rounding.attributeList != null && !rounding.attributeList.isEmpty()) {
            setDetailsRoundingAttributes(rounding.attributeList, detailsView)
        } else {
            isDetailsRoundingView = false
            detailsRoundingContentLayout.visibility = View.GONE
        }//if (rounding.attributeList != null && !rounding.attributeList.isEmpty()) ends
    }//setupDetailsRoundingSection ends

    private fun setDetailsRoundingAttributes(
        attributeList: List<AttributeListItem?>,
        detailsView: View
    ) {
        val detailsRoundingContentLayout: LinearLayout =
            detailsView.findViewById(R.id.detailsRoundingContentLayout) as LinearLayout
        //iterate over the list and add view items
        //remove the view
        detailsRoundingContentLayout.removeAllViews()
        for (its in attributeList.indices) {
            val subscriptionsRoundingViewItem = SubscriptionsRoundingViewItem(context!!)

            if (attributeList[its]!!.title != null && hasValue(attributeList[its]!!.title)) {
                subscriptionsRoundingViewItem.getRoundingViewItemLabel().text =
                    attributeList[its]!!.title
                subscriptionsRoundingViewItem.getRoundingViewItemLabel().isSelected = true
            }

            if (attributeList[its]!!.value != null && hasValue(attributeList[its]!!.value)) {
                if (attributeList[its]!!.unit != null && hasValue(attributeList[its]!!.unit)) {
                    if (attributeList[its]!!.unit.equals(
                            ConstantsUtility.TariffConstants.ATTRIBUTE_LIST_UNIT,
                            ignoreCase = true
                        )
                    ) {
                        //show the manat with the value
                        val value = attributeList[its]!!.value
                        subscriptionsRoundingViewItem.getRoundingViewItemValue().text =
                            getManatConcatedString(
                                context!!, value!!, ConstantsUtility.TariffConstants.AZERI_SYMBOL,
                                0.25f, R.dimen._10ssp
                            )
                        subscriptionsRoundingViewItem.getRoundingViewItemValue().isSelected = true
                    } else {
                        //dont show the manat with the value
                        val value = attributeList[its]!!.value
                        subscriptionsRoundingViewItem.getRoundingViewItemValue().text = value!!
                        subscriptionsRoundingViewItem.getRoundingViewItemValue().isSelected = true
                    }
                } else {
                    subscriptionsRoundingViewItem.getRoundingViewItemValue().text =
                        attributeList[its]!!.value
                    subscriptionsRoundingViewItem.getRoundingViewItemValue().isSelected = true
                }
            }//if (attributeList[its]!!.value != null && hasValue(attributeList[its]!!.value))  ends

            if (!hasValue(attributeList[its]!!.value) && !hasValue(attributeList[its]!!.title)) {
                subscriptionsRoundingViewItem.getRoundingViewItemLayout().visibility = View.GONE
            }

            detailsRoundingContentLayout.addView(subscriptionsRoundingViewItem)
        }//for ends
    }//setDetailsRoundingAttributes ends

    private fun initUIListeners(viewHolder: MySubscriptionsCardAdapterViewHolder, position: Int) {

        viewHolder.headerHolder!!.setOnClickListener {
            if (viewHolder.usagesExpandableLayout!!.isExpanded) {
                collapseUsageLayout(viewHolder)
                isHeaderLayoutExpanded = false
                expandDetailsLayout(viewHolder)
                isDetailsLayoutExpanded = true
            } else {
                expandUsageLayout(viewHolder)
                isHeaderLayoutExpanded = true
                collapseDetailsLayout(viewHolder)
                isDetailsLayoutExpanded = false
            }

            notifyDataSetChanged()
        }//viewHolder.usagesLayout!!.setOnClickListener ends

        viewHolder.detailsHeaderLayout!!.setOnClickListener {
            if (viewHolder.detailsExpandableLayout!!.isExpanded) {
                expandUsageLayout(viewHolder)
                isHeaderLayoutExpanded = true
                collapseDetailsLayout(viewHolder)
                isDetailsLayoutExpanded = false
            } else {
                collapseUsageLayout(viewHolder)
                isHeaderLayoutExpanded = false
                expandDetailsLayout(viewHolder)
                isDetailsLayoutExpanded = true
            }

            notifyDataSetChanged()
        }//viewHolder.detailsLayout!!.setOnClickListener  ends

        viewHolder.deactivateButton!!.setOnClickListener {
            var offeringName = ""
            var offeringId = ""
            val actionType = "3"

            if (mysubscriptions != null &&
                mysubscriptions!![position] != null &&
                mysubscriptions!![position]!!.header != null &&
                mysubscriptions!![position]!!.header!!.offerName != null &&
                hasValue(mysubscriptions!![position]!!.header!!.offerName)
            ) {
                offeringName = mysubscriptions!![position]!!.header!!.offerName!!
            }

            if (mysubscriptions != null &&
                mysubscriptions!![position] != null &&
                mysubscriptions!![position]!!.header != null &&
                mysubscriptions!![position]!!.header!!.offeringId != null &&
                hasValue(mysubscriptions!![position]!!.header!!.offeringId)
            ) {
                offeringId = mysubscriptions!![position]!!.header!!.offeringId!!
            }

            RootValues.getMySubscriptionEvents()
                .onMySubscriptionOfferProcessed(offeringName, offeringId, actionType)
        }//viewHolder.deactivateButton!!.setOnClickListener ends

        viewHolder.renewButton!!.setOnClickListener {
            var offeringName = ""
            var offeringId = ""
            val actionType = "1"

            if (mysubscriptions != null &&
                mysubscriptions!![position] != null &&
                mysubscriptions!![position]!!.header != null &&
                mysubscriptions!![position]!!.header!!.offerName != null &&
                hasValue(mysubscriptions!![position]!!.header!!.offerName)
            ) {
                offeringName = mysubscriptions!![position]!!.header!!.offerName!!
            }

            if (mysubscriptions != null &&
                mysubscriptions!![position] != null &&
                mysubscriptions!![position]!!.header != null &&
                mysubscriptions!![position]!!.header!!.offeringId != null &&
                hasValue(mysubscriptions!![position]!!.header!!.offeringId)
            ) {
                offeringId = mysubscriptions!![position]!!.header!!.offeringId!!
            }

            RootValues.getMySubscriptionEvents()
                .onMySubscriptionOfferProcessed(offeringName, offeringId, actionType)
        }//viewHolder.renewButton!!.setOnClickListener  ends

        viewHolder.topUpButton!!.setOnClickListener {
            val intent = Intent(context, TopupActivity::class.java)
            context!!.startActivity(intent)
        }
    }

    private fun prepareHeaderSection(
        header: HeaderMain?,
        subscriptions: Subscriptions,
        viewHolder: MySubscriptionsCardAdapterViewHolder
    ) {
        /**set the title and the price of the subscription*/
        if (header!!.offerName != null && hasValue(header.offerName)) {
            viewHolder.subscriptionsNameLabel!!.text = header.offerName
            viewHolder.subscriptionsNameLabel!!.isSelected = true
        }//if (header!!.offerName != null && hasValue(header!!.offerName)) ends

        if (header.price != null && hasValue(header.price)) {
            val price = header.price
            if (isNumeric(price)) {
                //price is numeric, append the manat symbol
                viewHolder.subscriptionsNameValue!!.text = getManatConcatedString(
                    context!!, price, ConstantsUtility.MySubscriptionsConstants.AZERI_SYMBOL,
                    0.25f, R.dimen._10ssp
                )
                viewHolder.subscriptionsNameValue!!.isSelected = true
            } else {
                //price is not numeric, don't append the manat symbol
                viewHolder.subscriptionsNameValue!!.text = header.price
                viewHolder.subscriptionsNameValue!!.isSelected = true
            }//if (isNumeric(price)) ends
        }//if (header.price != null && hasValue(header.price)) ends

        /**set the usages layout*/
        if (header.usage != null && header.usage.isNotEmpty()) {
            logE("usageX", "notnull", fromClass, "prepareHeaderSection")
            prepareUsages(header.usage, subscriptions, viewHolder)
        } else {
            logE("usageX", "null", fromClass, "prepareHeaderSection")
            viewHolder.usagesContentLayout!!.visibility = View.GONE
        }//if (header.usage != null && header.usage.isNotEmpty()) ends

        if (header.isTopUp != null && hasValue(header.isTopUp) && header.isTopUp == "1") {
            viewHolder.topUpButton!!.visibility = View.VISIBLE
            viewHolder.subscribedButton!!.visibility = View.GONE
            viewHolder.renewSubscribedButtonHolder!!.visibility = View.GONE
        } else {
            viewHolder.topUpButton!!.visibility = View.GONE
            if (header.btnRenew != null && hasValue(header.btnRenew) &&
                header.btnDeactivate != null && hasValue(header.btnDeactivate)
            ) {
                viewHolder.buttonsHolder!!.visibility = View.VISIBLE
                if (header.btnRenew.equals("1", ignoreCase = true) &&
                    header.btnDeactivate.equals("1", ignoreCase = true)
                ) {
                    viewHolder.renewButton!!.visibility = View.VISIBLE
                    viewHolder.deactivateButton!!.visibility = View.VISIBLE
                } else if (header.btnRenew.equals("1", ignoreCase = true)
                    && header.btnDeactivate.equals("0", ignoreCase = true)
                ) {
                    viewHolder.renewButton!!.visibility = View.VISIBLE
                    viewHolder.deactivateButton!!.visibility = View.GONE
                } else if (header.btnRenew.equals("0", ignoreCase = true)
                    && header.btnDeactivate.equals("1", ignoreCase = true)
                ) {
                    viewHolder.renewButton!!.visibility = View.GONE
                    viewHolder.deactivateButton!!.visibility = View.VISIBLE
                } else {
                    viewHolder.buttonsHolder!!.visibility = View.GONE
                }
            } else {
                viewHolder.buttonsHolder!!.visibility = View.GONE
            }
        }

    }//prepareHeaderSection ends

    private fun prepareUsages(
        usage: List<UsageItem?>,
        subscriptions: Subscriptions,
        viewHolder: MySubscriptionsCardAdapterViewHolder
    ) {
        //iterate over the list
        //remove all view from content layout
        viewHolder.usagesContentLayout!!.removeAllViews()

        for (its in usage.indices) {
            val mySubscriptionsUsageViewItem = MySubscriptionsUsageViewItem(context!!)

            //total usage
            if (usage[its]!!.totalUsage != null && hasValue(usage[its]!!.totalUsage)) {
                if (usage[its]!!.totalUsage == "0") {
                    mySubscriptionsUsageViewItem.getUsageProgressLayout().visibility = View.GONE
//                        mySubscriptionsUsageViewItem.getViewBetweenUsageRenewalProgressLayout().visibility = View.GONE
                } else {
                    mySubscriptionsUsageViewItem.getUsageProgressLayout().visibility = View.VISIBLE
                }
            } else {
                mySubscriptionsUsageViewItem.getUsageProgressLayout().visibility = View.GONE
//                    mySubscriptionsUsageViewItem.getViewBetweenUsageRenewalProgressLayout().visibility = View.GONE
            }//if (usage[its]!!.totalUsage != null && hasValue(usage[its]!!.totalUsage)) ends

            //remaining title
            if (usage[its]!!.remainingTitle != null && hasValue(usage[its]!!.remainingTitle)) {
                mySubscriptionsUsageViewItem.getTitleLeftAboveUsageProgress().text =
                    usage[its]!!.remainingTitle
                mySubscriptionsUsageViewItem.getTitleLeftAboveUsageProgress().isSelected = true
            }

            //renew title
            if (usage[its]!!.renewalTitle != null && hasValue(usage[its]!!.renewalTitle)) {
                if (subscriptions.header!!.btnRenew != null && hasValue(subscriptions.header.btnRenew)) {
                    if (subscriptions.header.btnRenew == "1") {
                        mySubscriptionsUsageViewItem.getTitleLeftAboveRenewalProgress().text =
                            usage[its]!!.renewalTitle
                        mySubscriptionsUsageViewItem.getTitleLeftAboveRenewalProgress().isSelected =
                            true
                    } else {
                        mySubscriptionsUsageViewItem.getTitleLeftAboveRenewalProgress().text =
                            getLocalizedExpirationLabel()
                        mySubscriptionsUsageViewItem.getTitleLeftAboveRenewalProgress().isSelected =
                            true
                    }
                }
            }

            var remainingUsage = ""
            if (usage[its]!!.remainingUsage != null && hasValue(usage[its]!!.remainingUsage)) {
                remainingUsage = usage[its]!!.remainingUsage!!
            }

            var unit = ""
            if (usage[its]!!.unit != null && hasValue(usage[its]!!.unit)) {
                unit = usage[its]!!.unit!!
            }

            var totalUsage = ""
            if (usage[its]!!.totalUsage != null && hasValue(usage[its]!!.totalUsage)) {
                totalUsage = usage[its]!!.totalUsage!!
            }

            if (hasValue(remainingUsage) && hasValue(totalUsage)) {
                val remainTitleVal =
                    remainingUsage.plus(" ").plus(unit).plus(" / ").plus(totalUsage)
                mySubscriptionsUsageViewItem.getTitleRightAboveUsageProgress().text = remainTitleVal
                mySubscriptionsUsageViewItem.getTitleRightAboveUsageProgress().isSelected = true
            }

            if (usage[its]!!.remainingUsage != null && hasValue(usage[its]!!.remainingUsage) &&
                usage[its]!!.totalUsage != null && hasValue(usage[its]!!.totalUsage)
            ) {
                if (usage[its]!!.remainingUsage!!.equals(
                        ConstantsUtility.MySubscriptionsConstants.IS_UNLIMITED,
                        ignoreCase = true
                    ) ||
                    usage[its]!!.totalUsage!!.equals(
                        ConstantsUtility.MySubscriptionsConstants.IS_UNLIMITED,
                        ignoreCase = true
                    )
                ) {

                    mySubscriptionsUsageViewItem.getTitleRightAboveUsageProgress().text =
                        getLocalizedFreeTitle()

                    mySubscriptionsUsageViewItem.getUsageProgressBar().max = 100
                    mySubscriptionsUsageViewItem.getUsageProgressBar().progress = 100

                    mySubscriptionsUsageViewItem.getUsageProgressBar().progressDrawable =
                        ContextCompat.getDrawable(context!!, R.drawable.progress_bar_horizontal_bg)

                } else {
                    var remainingUsageProgress = 0.0
                    var totalUsageProgress = 0.0
                    if (isNumeric(usage[its]!!.totalUsage)) {
                        totalUsageProgress =
                            getDoubleFromStringForMySubscription(usage[its]!!.totalUsage!!)
                    }
                    if (isNumeric(usage[its]!!.remainingUsage)) {
                        remainingUsageProgress =
                            getDoubleFromStringForMySubscription(usage[its]!!.remainingUsage!!)
                    }
                    mySubscriptionsUsageViewItem.getUsageProgressBar().max =
                        totalUsageProgress.toInt()
                    mySubscriptionsUsageViewItem.getUsageProgressBar().progress =
                        (totalUsageProgress - remainingUsageProgress).toInt()
                }
            } else {
                mySubscriptionsUsageViewItem.getUsageProgressLayout().visibility = View.GONE
//                    mySubscriptionsUsageViewItem.getViewBetweenUsageRenewalProgressLayout().visibility = View.GONE
            }

            if (usage[its]!!.activationDate != null && hasValue(usage[its]!!.activationDate) &&
                usage[its]!!.renewalDate != null && hasValue(usage[its]!!.renewalDate)
            ) {
                var isDaily = "false"
                if (subscriptions.header != null && subscriptions.header.isDaily != null && hasValue(
                        subscriptions.header.isDaily
                    )
                ) {
                    isDaily = subscriptions.header.isDaily
                }

                if (isDaily.equals("false", ignoreCase = true)) {
                    /**calculate the days difference*/
                    val startMili =
                        getMiliSecondsFromDateForMySubscription(usage[its]!!.activationDate!!)
                    var endMili =
                        getMiliSecondsFromDateForMySubscription(usage[its]!!.renewalDate!!)
                    endMili = getOneDayMinusForMySubscription(endMili)
                    val currentMili = getCurrentMiliSecondsForMySubscription()

                    val daysDiffCurrent =
                        getDaysDifferenceBetweenMilisecondsForMySubscription(startMili, currentMili)
                    val daysDiffTotal =
                        getDaysDifferenceBetweenMilisecondsForMySubscription(startMili, endMili)

                    var days = daysDiffTotal - daysDiffCurrent
                    if (days <= -1) {
                        days = 0
                    }

                    if (days > 0) {
                        mySubscriptionsUsageViewItem.getRenewalProgressBar().max = daysDiffTotal
                        mySubscriptionsUsageViewItem.getRenewalProgressBar().progress =
                            daysDiffCurrent
                    } else {
                        mySubscriptionsUsageViewItem.getRenewalProgressBar().max = 100
                        mySubscriptionsUsageViewItem.getRenewalProgressBar().progress = 100
                    }

                    mySubscriptionsUsageViewItem.getTitleRightAboveRenewalProgress().text =
                        context!!.resources.getQuantityString(
                            R.plurals.my_subscriptions_remaining_days,
                            days,
                            days
                        )
                    mySubscriptionsUsageViewItem.getTitleRightAboveRenewalProgress().isSelected =
                        true
                    try {

                        if (subscriptions.header != null) {
                            if (subscriptions.header.btnRenew != null && hasValue(subscriptions.header.btnRenew) &&
                                subscriptions.header.btnRenew == "1"
                            ) {
                                mySubscriptionsUsageViewItem.getTitleRightBelowRenewalProgress().text =
                                    getLocalizedRenewLabel(getDateAccordingToClientSide(usage[its]!!.renewalDate!!))
                                mySubscriptionsUsageViewItem.getTitleRightBelowRenewalProgress().isSelected =
                                    true
                            } else {
                                mySubscriptionsUsageViewItem.getTitleRightBelowRenewalProgress().text =
                                    getLocalizedValidtityLabel(getDateAccordingToClientSide(usage[its]!!.renewalDate!!))
                                mySubscriptionsUsageViewItem.getTitleRightBelowRenewalProgress().isSelected =
                                    true
                            }
                        }

                        mySubscriptionsUsageViewItem.getTitleLeftBelowRenewalProgress().text =
                            getLocalizedActivatedLabel(getDateAccordingToClientSide(usage[its]!!.activationDate!!))
                        mySubscriptionsUsageViewItem.getTitleLeftBelowRenewalProgress().isSelected =
                            true

                    } catch (e: ParseException) {
                        logE(
                            "mysubsX123",
                            "error111:::".plus(e.toString()),
                            fromClass,
                            "prepareUsages"
                        )
                    }

                } else {
                    /**calculate the hours difference*/
                    val startMili =
                        getMiliSecondsFromDateForMySubscription(usage[its]!!.activationDate!!)
                    var endMili =
                        getMiliSecondsFromDateForMySubscription(usage[its]!!.renewalDate!!)
                    endMili = getOneHourMinusForMySubscription(endMili)
                    val currentMili = getCurrentMiliSecondsForMySubscription()

                    val hoursDiffCurrent = getHoursDifferenceBetweenMilisecondsForMySubscription(
                        startMili,
                        currentMili
                    )
                    val hoursDiffTotal =
                        getHoursDifferenceBetweenMilisecondsForMySubscription(startMili, endMili)

                    var hours = hoursDiffTotal - hoursDiffCurrent
                    if (hours < 0) {
                        hours = 0
                    }

                    if (hours > 0) {
                        mySubscriptionsUsageViewItem.getRenewalProgressBar().max = hoursDiffTotal
                        mySubscriptionsUsageViewItem.getRenewalProgressBar().progress =
                            hoursDiffCurrent
                    } else {
                        mySubscriptionsUsageViewItem.getRenewalProgressBar().max = 100
                        mySubscriptionsUsageViewItem.getRenewalProgressBar().progress = 100
                    }

                    mySubscriptionsUsageViewItem.getTitleRightAboveRenewalProgress().text =
                        "".plus(hours).plus(" ")
                            .plus(context!!.resources.getString(R.string.my_subscriptions_hour))
                    mySubscriptionsUsageViewItem.getTitleRightAboveRenewalProgress().isSelected =
                        true
                    try {

                        if (subscriptions.header != null) {
                            if (subscriptions.header.btnRenew != null && hasValue(subscriptions.header.btnRenew) &&
                                subscriptions.header.btnRenew == "1"
                            ) {
                                mySubscriptionsUsageViewItem.getTitleRightBelowRenewalProgress().text =
                                    getLocalizedRenewLabel(getDateAccordingToClientSide(usage[its]!!.renewalDate!!))
                                mySubscriptionsUsageViewItem.getTitleRightBelowRenewalProgress().isSelected =
                                    true
                            } else {
                                mySubscriptionsUsageViewItem.getTitleRightBelowRenewalProgress().text =
                                    getLocalizedValidtityLabel(getDateAccordingToClientSide(usage[its]!!.renewalDate!!))
                                mySubscriptionsUsageViewItem.getTitleRightBelowRenewalProgress().isSelected =
                                    true
                            }
                        }

                        mySubscriptionsUsageViewItem.getTitleLeftBelowRenewalProgress().text =
                            getLocalizedActivatedLabel(getDateAccordingToClientSide(usage[its]!!.activationDate!!))
                        mySubscriptionsUsageViewItem.getTitleLeftBelowRenewalProgress().isSelected =
                            true

                    } catch (e: ParseException) {
                        logE(
                            "mysubsX123",
                            "error222:::".plus(e.toString()),
                            fromClass,
                            "prepareUsages"
                        )
                    }

                }
            } else {

                mySubscriptionsUsageViewItem.getTitleRightAboveRenewalProgress().text =
                    context!!.resources.getQuantityString(
                        R.plurals.my_subscriptions_remaining_days,
                        0,
                        0
                    )
                mySubscriptionsUsageViewItem.getTitleRightAboveRenewalProgress().isSelected = true

                mySubscriptionsUsageViewItem.getRenewalProgressBar().max = 100
                mySubscriptionsUsageViewItem.getRenewalProgressBar().progress = 100

                mySubscriptionsUsageViewItem.getTitleRightBelowRenewalProgress()
                    .setText(R.string.my_subscriptions_lbl_expired)
                mySubscriptionsUsageViewItem.getTitleRightBelowRenewalProgress().isSelected = true

                // To hide of null
                if (usage[its]!!.activationDate == null || usage[its]!!.renewalDate == null) {
//                        mySubscriptionsUsageViewItem.getViewBetweenUsageRenewalProgressLayout().visibility = View.GONE
                    mySubscriptionsUsageViewItem.getRenewalProgressLayout().visibility = View.GONE
                }
            }

            //icon here
            if (usage[its]!!.iconName != null && hasValue(usage[its]!!.iconName)) {
                mySubscriptionsUsageViewItem.getTitleImageIcon()
                    .setImageResource(getCardAdapterMappedImage(usage[its]!!.iconName!!))
            }//if (usage[its]!!.iconName != null && hasValue(usage[its]!!.iconName)) ends

            if (mySubscriptionsUsageViewItem.getRenewalProgressLayout().visibility == View.GONE &&
                mySubscriptionsUsageViewItem.getUsageProgressLayout().visibility == View.GONE
            ) {
                mySubscriptionsUsageViewItem.getViewBelowBottom().visibility = View.GONE
            } else {
                mySubscriptionsUsageViewItem.getViewBelowBottom().visibility = View.VISIBLE
            }

            if (its == usage.size - 1) {
                mySubscriptionsUsageViewItem.getViewBelowBottom().visibility = View.GONE
            }

            viewHolder.usagesContentLayout!!.addView(mySubscriptionsUsageViewItem)
        }//for ends
    }//prepareUsages ends

    private fun getLocalizedValidtityLabel(date: String): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "End date".plus(
                ": "
            ).plus(date)
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Bitmə tarixi".plus(
                ": "
            ).plus(date)
            else -> "Дата окончания".plus(": ").plus(date)
        }
    }

    private fun getLocalizedFreeTitle(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "Free"
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Pulsuz"
            else -> "Бесплатно"
        }
    }

    private fun getLocalizedRenewLabel(date: String): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "Renew Date".plus(
                ": "
            ).plus(date)
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Yenilənmə tarixi".plus(
                ": "
            ).plus(date)
            else -> "Дата обновления".plus(": ").plus(date)
        }
    }

    private fun getLocalizedActivatedLabel(date: String): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "Activated".plus(
                ": "
            ).plus(date)
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Aktiv edilib".plus(
                ": "
            ).plus(date)
            else -> "Активирован".plus(": ").plus(date)
        }
    }

    private fun getLocalizedExpirationLabel(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "To Expiration"
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Bitməsi üçün"
            else -> "До истечения"
        }
    }

    inner class MySubscriptionsCardAdapterViewHolder :
        androidx.recyclerview.widget.RecyclerView.ViewHolder {
        var subscriptionsNameHolderLayout: ConstraintLayout? = null
        var subscriptionsNameLabel: TextView? = null
        var subscriptionsNameValue: TextView? = null
        var usagesExpandableLayout: ExpandableLayout? = null
        var usagesContentLayout: LinearLayout? = null
        var detailsLayout: LinearLayout? = null
        var detailsLayoutTitleLabel: TextView? = null
        var detailsLayoutExpandIcon: ImageView? = null
        var detailsExpandableLayout: ExpandableLayout? = null
        var detailsViewStub: ViewStub? = null
        var subscribedButton: Button? = null
        var renewSubscribedButtonHolder: LinearLayout? = null
        var renewButton: Button? = null
        var deactivateButton: Button? = null
        var buttonsHolder: RelativeLayout? = null
        var headerHolder: ConstraintLayout? = null
        var topUpButton: Button? = null
        var mainContainer: RelativeLayout? = null
        var detailsHeaderLayout: RelativeLayout? = null
        var ratingStarsLayout: ConstraintLayout? = null
        var starOne: CheckBox? = null
        var starTwo: CheckBox? = null
        var starThree: CheckBox? = null
        var starFour: CheckBox? = null
        var starFive: CheckBox? = null

        constructor(row: View) : super(row) {
            mainContainer = row.findViewById(R.id.mainContainer) as RelativeLayout
            detailsHeaderLayout = row.findViewById(R.id.detailsHeaderLayout) as RelativeLayout
            topUpButton = row.findViewById(R.id.topUpButton) as Button
            headerHolder = row.findViewById(R.id.headerHolder) as ConstraintLayout
            buttonsHolder = row.findViewById(R.id.buttonsHolder) as RelativeLayout
            renewSubscribedButtonHolder =
                row.findViewById(R.id.renewSubscribedButtonHolder) as LinearLayout
            subscribedButton = row.findViewById(R.id.subscribedButton) as Button
            renewButton = row.findViewById(R.id.renewButton) as Button
            deactivateButton = row.findViewById(R.id.deactivateButton) as Button
            detailsViewStub = row.findViewById(R.id.detailsViewStub) as ViewStub
            detailsExpandableLayout =
                row.findViewById(R.id.detailsExpandableLayout) as ExpandableLayout
            detailsLayoutExpandIcon = row.findViewById(R.id.detailsLayoutExpandIcon) as ImageView
            detailsLayoutTitleLabel = row.findViewById(R.id.detailsLayoutTitleLabel) as TextView
            detailsLayout = row.findViewById(R.id.detailsLayout) as LinearLayout
            subscriptionsNameHolderLayout =
                row.findViewById(R.id.subscriptionsNameHolderLayout) as ConstraintLayout
            subscriptionsNameLabel = row.findViewById(R.id.subscriptionsNameLabel) as TextView
            subscriptionsNameValue = row.findViewById(R.id.subscriptionsNameValue) as TextView
            usagesContentLayout = row.findViewById(R.id.usagesContentLayout) as LinearLayout
            usagesExpandableLayout =
                row.findViewById(R.id.usagesExpandableLayout) as ExpandableLayout

            ratingStarsLayout = row.findViewById(R.id.ratingStarsLayout)
            starOne = row.findViewById(R.id.star1)
            starTwo = row.findViewById(R.id.star2)
            starThree = row.findViewById(R.id.star3)
            starFour = row.findViewById(R.id.star4)
            starFive = row.findViewById(R.id.star5)
        }//constructor ends
    }//MySubscriptionsCardAdapterViewHolder ends
}//class ends