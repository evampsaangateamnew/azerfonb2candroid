package com.azarphone.ui.adapters.recyclerviews

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.azarphone.R
import com.azarphone.api.pojo.response.supplementaryoffers.helper.OfferFilter
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.util.logE
import com.azarphone.validators.hasValue
import com.azarphone.widgets.checkbox.AzerCheckBox
import java.util.*

/**
 * @author Junaid Hassan on 30, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class SupplementaryOffersFilterAdapter(val mContext: Context, val offerFilters: ArrayList<OfferFilter?>) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_supplementary_offers_filter_adapter,
                parent, false)
        return SupplementaryOffersFilterAdapterViewHolder(view)
    }//onCreateViewHolder ends

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as SupplementaryOffersFilterAdapterViewHolder

        if (offerFilters[position]!!.key != null && hasValue(offerFilters[position]!!.key)) {
            var isSelected = false
            if (UserDataManager.getSupplementaryFilterKeysList().isNotEmpty()) {
                val valueFromOffersList = offerFilters[position]!!.key
                //iterate over the list of values
                for (its in UserDataManager.getSupplementaryFilterKeysList().indices) {
                    val valueFromSelectedFilterKeys = UserDataManager.getSupplementaryFilterKeysList()[its]
                    if (valueFromOffersList.equals(valueFromSelectedFilterKeys, ignoreCase = true)) {
                        isSelected = true
                        break
                    }//if (valueFromOffersList.equals(valueFromSelectedFilterKeys, ignoreCase = true)) ends
                }//for ends
            }//if (UserDataManager.getSupplementaryFilterKeysList().isNotEmpty()) ends

            if (isSelected) {
                /**filter was used before, so check the checkboxes*/
                viewHolder.filterCheck.setChecked(true, false)
            } else {
                viewHolder.filterCheck.setChecked(false, false)
            }//if (isSelected) ends

            viewHolder.filterText.text = offerFilters[position]!!.value
            viewHolder.filterText.isSelected = true
        }// if (offerFilters[position]!!.key != null && hasValue(offerFilters[position]!!.key)) ends

        viewHolder.filterCheck.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                if (hasValue(offerFilters[position]!!.key)) {
                    UserDataManager.getSupplementaryFilterKeysList().add(offerFilters[position]!!.key!!)

//                    for (its in UserDataManager.getSupplementaryFilterKeysList().indices) {
//                        logE("valueFilter", "added:::".plus(UserDataManager.getSupplementaryFilterKeysList()[its]), "SupplementaryOffersFilterAdapter", "added")
//                    }
                }//if (hasValue(offerFilters[position]!!.key)) ends
            } else {
                if (hasValue(offerFilters[position]!!.key)) {
                    val valueFromOffersList = offerFilters[position]!!.key
                    //iterate over the list of values
                    for (its in UserDataManager.getSupplementaryFilterKeysList().indices) {
                        val valueFromSelectedFilterKeys = UserDataManager.getSupplementaryFilterKeysList()[its]
                        if (valueFromOffersList.equals(valueFromSelectedFilterKeys, ignoreCase = true)) {
                            UserDataManager.getSupplementaryFilterKeysList().removeAt(its)
                            break
                        }
                    }

//                    for (its in UserDataManager.getSupplementaryFilterKeysList().indices) {
//                        logE("valueFilter", "removed:::".plus(UserDataManager.getSupplementaryFilterKeysList()[its]), "SupplementaryOffersFilterAdapter", "added")
//                    }//for ends
                }//if (hasValue(offerFilters[position]!!.key)) ends
            }//if (isChecked) ends
        }//click listener ends
    }//onBindViewHolder ends

    override fun getItemCount(): Int {
        return offerFilters.size
    }//getItemCount ends

    private inner class SupplementaryOffersFilterAdapterViewHolder(row: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(row) {
        var filterCheck: AzerCheckBox = row.findViewById(R.id.filterCheck) as AzerCheckBox
        var filterText: TextView = row.findViewById(R.id.filterText) as TextView
    }//SupplementaryOffersFilterAdapterViewHolder ends
}//class ends