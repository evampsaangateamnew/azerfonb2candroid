package com.azarphone.ui.adapters.pagers

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.azarphone.R
import com.azarphone.ui.fragment.storelocator.StoreListFragment
import com.google.android.gms.maps.MapFragment

public class StoreLocatorPagerAdapter(val context: Context,val fragmentManager: androidx.fragment.app.FragmentManager): FragmentPagerAdapter(fragmentManager){

    override fun getItem(p0: Int): androidx.fragment.app.Fragment {
        when(p0){
            0 ->{
                return com.azarphone.ui.fragment.storelocator.MapFragment()
            }
            1 ->{
               return StoreListFragment()
            }
        }
        return com.azarphone.ui.fragment.storelocator.MapFragment()
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        // Generate title based on item position
        when (position) {
            0 -> return context.getString(R.string.title_activity_maps)
            1 -> return context.getString(R.string.storelist)
            else -> return context.getString(R.string.title_activity_maps)
        }
    }


}