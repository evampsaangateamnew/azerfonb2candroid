package com.azarphone.ui.adapters.pagers

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

/**
 * @author Junaid Hassan on 22, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class SupplementaryOffersPagerAdapter(fm: androidx.fragment.app.FragmentManager, val fragmentsList: ArrayList<androidx.fragment.app.Fragment>) : FragmentStatePagerAdapter(fm) {

    override fun getCount(): Int {
        return fragmentsList.size
    }//getCount ends

    override fun getItem(position: Int): androidx.fragment.app.Fragment {
        return fragmentsList[position]
    }//getItem ends

    override fun getItemPosition(obj: Any): Int {
        return POSITION_NONE
    }
}