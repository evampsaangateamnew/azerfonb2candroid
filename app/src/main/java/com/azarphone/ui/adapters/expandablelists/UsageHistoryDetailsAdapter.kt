package com.azarphone.ui.adapters.expandablelists

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.TextView
import com.azarphone.R
import com.azarphone.api.pojo.response.usagedetailsresponse.RecordsItem
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.getManatConcatedString
import com.azarphone.validators.hasValue

class UsageHistoryDetailsAdapter(val context: Context, recordsList: List<RecordsItem?>) : BaseExpandableListAdapter() {

    private var recordsList: ArrayList<RecordsItem?>? = null

    init {
        this.recordsList = recordsList as ArrayList<RecordsItem?>
    }

    fun updateList(arrayList: ArrayList<RecordsItem?>) {
        recordsList = ArrayList()
        this.recordsList!!.addAll(arrayList)
        notifyDataSetChanged()
    }

    override fun getGroup(p0: Int): RecordsItem? {
        return recordsList!![p0]
    }

    override fun isChildSelectable(p0: Int, p1: Int): Boolean {
        return false
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    @SuppressLint("InflateParams")
    override fun getGroupView(groupPostion: Int, isExpanded: Boolean, convertView: View?, p3: ViewGroup?): View {
        var convertView: View? = convertView

        val infalInflater = context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        convertView = infalInflater.inflate(R.layout.usage_history_details_list_item_parent, null)


        convertView.findViewById<TextView>(R.id.datetime).text = recordsList!![groupPostion]?.startDateTime
        convertView.findViewById<TextView>(R.id.service).text = recordsList!![groupPostion]?.service
        convertView.findViewById<TextView>(R.id.service).isSelected = true
        convertView.findViewById<TextView>(R.id.number).text = recordsList!![groupPostion]?.number
        convertView.findViewById<TextView>(R.id.number).isSelected = true
        convertView.findViewById<TextView>(R.id.usage).text = recordsList!![groupPostion]?.usage
        convertView.findViewById<TextView>(R.id.usage).isSelected = true

        /*   if (isExpanded) {
               convertView.findViewById<ImageView>(R.id.icon_Plus).setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.faqminus))
           } else {
               convertView.findViewById<ImageView>(R.id.icon_Plus).setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.faqplus))
           }*/


        return convertView
    }

    override fun getChildrenCount(p0: Int): Int {
        /*   if (recordsList[p0]?.records == null) {
               return 0
           } else {
               return recordsList[p0]?.records!!.size
           }*/
        return 1

    }

    override fun getChild(parentPosition: Int, childPosititon: Int): Any {
        return recordsList!![parentPosition]!!
    }

    override fun getGroupId(p0: Int): Long {
        return p0.toLong()
    }

    @SuppressLint("InflateParams")
    override fun getChildView(groupPostion: Int, childPosition: Int, p2: Boolean, convertView: View?, p4: ViewGroup?): View {
        var convertView = convertView
        val infalInflater = context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        convertView = infalInflater.inflate(R.layout.usage_history_details_list_item_child, null)


        if (hasValue(recordsList!![groupPostion]?.chargedAmount)) {
            convertView.findViewById<TextView>(R.id.charge_value).text = getManatConcatedString(context, recordsList!![groupPostion]?.chargedAmount!!, ConstantsUtility.TariffConstants.AZERI_SYMBOL, 0.20f, R.dimen._8ssp)
        }
        convertView.findViewById<TextView>(R.id.charge_value).isSelected = true
        convertView.findViewById<TextView>(R.id.peak_value).text = recordsList!![groupPostion]?.period
        convertView.findViewById<TextView>(R.id.peak_value).isSelected = true
        convertView.findViewById<TextView>(R.id.zone_value).text = recordsList!![groupPostion]?.zone
        convertView.findViewById<TextView>(R.id.zone_value).isSelected = true
        convertView.findViewById<TextView>(R.id.destination_value).text = recordsList!![groupPostion]?.destination
        convertView.findViewById<TextView>(R.id.destination_value).isSelected = true

//        convertView.findViewById<TextView>(R.id.charge).text = recordsList!![groupPostion]?.destination
        convertView.findViewById<TextView>(R.id.charge).isSelected = true
//        convertView.findViewById<TextView>(R.id.peak).text = recordsList!![groupPostion]?.destination
        convertView.findViewById<TextView>(R.id.peak).isSelected = true
//        convertView.findViewById<TextView>(R.id.zone).text = recordsList!![groupPostion]?.destination
        convertView.findViewById<TextView>(R.id.zone).isSelected = true
//        convertView.findViewById<TextView>(R.id.destination).text = recordsList!![groupPostion]?.destination
        convertView.findViewById<TextView>(R.id.destination).isSelected = true
        return convertView
    }

    override fun getChildId(p0: Int, p1: Int): Long {
        return p0.toLong()
    }

    override fun getGroupCount(): Int {
        return recordsList!!.size
    }

}