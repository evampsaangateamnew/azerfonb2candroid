package com.azarphone.ui.adapters.recyclerviews

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.azarphone.R
import com.azarphone.api.pojo.helperpojo.menus.HorizontalChildMenusHelperModel
import com.azarphone.ui.activities.datawithoutpack.DataWithOutPackActivity
import com.azarphone.ui.activities.exchangeservice.ExchangeServiceActivity
import com.azarphone.ui.activities.fnf.FnFActivity
import com.azarphone.ui.activities.freesms.FreeSmsActivity
import com.azarphone.ui.activities.nartv.NarTvActivity
import com.azarphone.ui.activities.specialoffers.SpecialOffersActivity
import com.azarphone.ui.activities.supplementaryoffers.SupplementaryOffersActivity
import com.azarphone.ui.activities.topupmenu.TopUpMenuActivity
import com.azarphone.ui.activities.vasservices.CoreServicesActivity
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.logE
import com.azarphone.validators.hasValue

/**
 * @author Junaid Hassan on 21, May, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class ServiceMenuRecyclerAdapter(
    val mContext: Context,
    val mList: ArrayList<HorizontalChildMenusHelperModel>,
    val stepOneScreen: String,
    val matchedSubScreen: String,
    val tabToRedirect: String,
    val offeringIdToRedirect: String,
) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

    //Class constant
    private val fromClass = ServiceMenuRecyclerAdapter::class.simpleName.toString()
    override fun onCreateViewHolder(
        parent: ViewGroup,
        p1: Int
    ): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.child_menus_row_item, parent, false)
        return ServiceMenuRecyclerViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: androidx.recyclerview.widget.RecyclerView.ViewHolder,
        position: Int
    ) {
        val viewHolder = holder as ServiceMenuRecyclerViewHolder

        viewHolder.titleTV.isSelected = true

        when {
            /**special offers*/
            mList[position].identifier.equals(
                ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_SPECIAL_OFFERS,
                ignoreCase = true
            ) -> {
                /**show the special offers menu*/
                viewHolder.centerContentCVForSpecialOffers.visibility = View.VISIBLE
                viewHolder.centerContentCV.visibility = View.GONE

                viewHolder.specialOffersMenuIcon.setImageResource(R.drawable.special_offers)
                viewHolder.specialOffersMenuTitle.text = mList[position].title
            }

            /**exchange service*/
            mList[position].identifier.equals(
                ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_EXCHANGE_SERVICES,
                ignoreCase = true
            ) -> {
                viewHolder.menuIcon.setImageResource(R.drawable.exchange_service)
                viewHolder.titleTV.text = mList[position].title
            }

            /**supplementary offers*/
            mList[position].identifier.equals(
                ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_SUPPLEMENTORY_OFFERS,
                ignoreCase = true
            ) -> {
                viewHolder.menuIcon.setImageResource(R.drawable.supplementary_offers)
                viewHolder.titleTV.text = mList[position].title
            }

            /**free sms*/
            mList[position].identifier.equals(
                ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_FREE_SMS,
                ignoreCase = true
            ) -> {
                viewHolder.menuIcon.setImageResource(R.drawable.free_sms)
                viewHolder.titleTV.text = mList[position].title
            }

            /**fnf*/
            mList[position].identifier.equals(
                ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_FNF,
                ignoreCase = true
            ) -> {
                viewHolder.menuIcon.setImageResource(R.drawable.fnf)
                viewHolder.titleTV.text = mList[position].title
            }

            /**vas services*/
            mList[position].identifier.equals(
                ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_VAS_SERVICES,
                ignoreCase = true
            ) -> {
                viewHolder.menuIcon.setImageResource(R.drawable.vas_services)
                viewHolder.titleTV.text = mList[position].title
            }

            /**nar tv*/
            mList[position].identifier.equals(
                ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_NAR_TV,
                ignoreCase = true
            ) -> {
                viewHolder.menuIcon.setImageResource(R.drawable.nar_tv)
                viewHolder.titleTV.text = mList[position].title
            }
            /**nar topUp*/
            mList[position].identifier.equals(
                ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_TOPUP,
                ignoreCase = true
            ) -> {
                viewHolder.menuIcon.setImageResource(R.drawable.service_top_up)
                viewHolder.titleTV.text = mList[position].title
            }
            /**nar data without pack*/
            mList[position].identifier.equals(
                ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_DATA_WITH_OUT_PACK,
                ignoreCase = true
            ) -> {
                viewHolder.menuIcon.setImageResource(R.drawable.service_interent)
                viewHolder.titleTV.text = mList[position].title
            }
            else -> {

                /** some unknown menu*/
                viewHolder.menuIcon.setImageResource(R.drawable.ic_warn_oval)
                viewHolder.titleTV.text = mList[position].title
            }
        }

        if (position == mList.size - 1) {
            /** If last index is being populated then
            Redirecting to Dynamic Link Destination    */
            manageDynamicLinkRedirection()
        }

        initUIEvents(viewHolder, position)
    }//onBindViewHolder ends

    private fun initUIEvents(viewHolder: ServiceMenuRecyclerViewHolder, position: Int) {
        //Applying click listener on recyclerview items
        viewHolder.centerContentCV.setOnClickListener {
            when {
                mList[position].identifier.equals(
                    ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_FREE_SMS,
                    ignoreCase = true
                ) -> {
                    FreeSmsActivity.start(mContext)
                }
                mList[position].identifier.equals(
                    ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_FNF,
                    ignoreCase = true
                ) -> FnFActivity.start(mContext)
                mList[position].identifier.equals(
                    ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_SUPPLEMENTORY_OFFERS,
                    ignoreCase = true
                ) -> SupplementaryOffersActivity.start(mContext, "")
                mList[position].identifier.equals(
                    ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_VAS_SERVICES,
                    ignoreCase = true
                ) -> CoreServicesActivity.start(mContext)
                mList[position].identifier.equals(
                    ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_EXCHANGE_SERVICES,
                    ignoreCase = true
                ) -> ExchangeServiceActivity.start(mContext)
                mList[position].identifier.equals(
                    ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_NAR_TV,
                    ignoreCase = true
                ) -> NarTvActivity.start(mContext)
                mList[position].identifier.equals(
                    ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_TOPUP,
                    ignoreCase = true
                ) -> TopUpMenuActivity.start(mContext)
                mList[position].identifier.equals(
                    ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_DATA_WITH_OUT_PACK,
                    ignoreCase = true
                ) -> DataWithOutPackActivity.start(mContext)
                else -> Toast.makeText(
                    mContext,
                    mContext.resources.getString(R.string.no_data_found),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }//viewHolder.centerContentCV.setOnClickListener

        viewHolder.centerContentCVForSpecialOffers.setOnClickListener {
            SpecialOffersActivity.start(
                mContext,
                ""
            )
        }
    }//initUIEvents ends

    private fun manageDynamicLinkRedirection() {
        if (hasValue(matchedSubScreen)) {
            when (matchedSubScreen) {
                ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_FREE_SMS -> FreeSmsActivity.start(
                    mContext
                )
                ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_FNF -> FnFActivity.start(
                    mContext
                )
                ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_SUPPLEMENTORY_OFFERS -> SupplementaryOffersActivity.start(
                    mContext,
                    tabToRedirect,
                    offeringIdToRedirect
                )
                ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_VAS_SERVICES -> CoreServicesActivity.start(
                    mContext
                )
                ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_EXCHANGE_SERVICES -> ExchangeServiceActivity.start(
                    mContext,
                    tabToRedirect
                )
                ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_NAR_TV -> NarTvActivity.start(
                    mContext
                )
                ConstantsUtility.ServicesChildMenusIdentifiers.SERVICES_TOPUP -> TopUpMenuActivity.start(
                    mContext, stepOneScreen, tabToRedirect,
                    offeringIdToRedirect
                )
            }
            logE(
                ConstantsUtility.DynamicLinkConstants.DYNAMIC_LINK_TAG,
                " $matchedSubScreen",
                fromClass,
                "manageDynamicLinkRedirection"
            )
        }
    }//manageDynamicLinkRedirection ends

    override fun getItemCount(): Int {
        return mList.size
    }//getItemCount ends

    private inner class ServiceMenuRecyclerViewHolder(row: View) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(row) {
        val centerContentCV: androidx.cardview.widget.CardView =
            row.findViewById(R.id.centerContentCV) as androidx.cardview.widget.CardView
        val menuIcon: ImageView = row.findViewById(R.id.menuIcon) as ImageView
        val titleTV: TextView = row.findViewById(R.id.titleTV) as TextView
        val centerContentCVForSpecialOffers: androidx.cardview.widget.CardView =
            row.findViewById(R.id.centerContentCVForSpecialOffers) as androidx.cardview.widget.CardView
        val specialOffersMenuTitle: TextView =
            row.findViewById(R.id.specialOffersMenuTitle) as TextView
        val specialOffersMenuIcon: ImageView =
            row.findViewById(R.id.specialOffersMenuIcon) as ImageView
    }//ServiceMenuRecyclerViewHolder ends

}//class ends