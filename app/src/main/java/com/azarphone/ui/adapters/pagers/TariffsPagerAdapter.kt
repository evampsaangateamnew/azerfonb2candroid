package com.azarphone.ui.adapters.pagers

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter
import java.util.ArrayList

/**
 * @author Junaid Hassan on 10, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class TariffsPagerAdapter(fm: androidx.fragment.app.FragmentManager, val fragmentsList: ArrayList<androidx.fragment.app.Fragment>) : FragmentStatePagerAdapter(fm) {

    var fragmentsListNew=ArrayList<androidx.fragment.app.Fragment>()

    init {
        fragmentsListNew.clear()
        fragmentsListNew=fragmentsList
    }

    override fun getCount(): Int {
        return fragmentsList.size
    }//getCount ends

    override fun getItem(position: Int): androidx.fragment.app.Fragment {
        return fragmentsList[position]
    }//getItem ends

    override fun getItemPosition(obj: Any): Int {
        return POSITION_NONE
    }
}