package com.azarphone.ui.adapters.expandablelists

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.TextView
import com.azarphone.ProjectApplication
import com.azarphone.R
import com.azarphone.api.pojo.response.operationshistory.RecordsItem
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.getALSNormalFont
import com.azarphone.util.getManatConcatedString
import com.azarphone.validators.hasValue

/**
 * @author Junaid Hassan on 11, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class OperationHistoryAdapter(val context: Context, val activity: Activity, val records: List<RecordsItem?>) : BaseExpandableListAdapter() {

    private val fromClass = "OperationHistoryAdapter"

    @SuppressLint("InflateParams")
    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup): View {
        val view: View?
        val viewHolderGroup: GroupViewHolder
        if (convertView == null) {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.adapter_item_operation_history, null)
            viewHolderGroup = GroupViewHolder(view)
            view?.tag = viewHolderGroup
        }//if ends
        else {
            view = convertView
            viewHolderGroup = view.tag as GroupViewHolder
        }//else ends

        //ui handling here

        if (hasValue(records[groupPosition]!!.date)) {
            viewHolderGroup.dateTimeValue.text = records[groupPosition]!!.date
            viewHolderGroup.dateTimeValue.typeface= getALSNormalFont()
        }

        if (hasValue(records[groupPosition]!!.transactionType)) {
            viewHolderGroup.typeValue.text = records[groupPosition]!!.transactionType
            viewHolderGroup.typeValue.typeface= getALSNormalFont()
            viewHolderGroup.typeValue.isSelected = true
        }

        if (hasValue(records[groupPosition]!!.description)) {
            viewHolderGroup.descriptionValue.isSelected = true
            viewHolderGroup.descriptionValue.typeface= getALSNormalFont()
            viewHolderGroup.descriptionValue.text = records[groupPosition]!!.description
        }

        if (hasValue(records[groupPosition]!!.amount)) {
            val amount = records[groupPosition]!!.amount
            viewHolderGroup.amountValue.isSelected = true
            viewHolderGroup.amountValue.typeface= getALSNormalFont()
            viewHolderGroup.amountValue.text = getManatConcatedString(context, amount!!, ConstantsUtility.TariffConstants.AZERI_SYMBOL, 0.25f, R.dimen._8ssp)
        }
        return view as View
    }//getGroupView ends

    @SuppressLint("InflateParams")
    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
        val view: View?
        val viewHolderChild: ChildViewHolder

        if (convertView == null) {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.adapter_item_childs_operation_history, null)
            viewHolderChild = ChildViewHolder(view)
            view?.tag = viewHolderChild
        } else {
            view = convertView
            viewHolderChild = view.tag as ChildViewHolder
        }

        viewHolderChild.endingBalanceTitle.text = getLocalizedTitle()
        viewHolderChild.endingBalanceTitle.typeface= getALSNormalFont()
        viewHolderChild.endingBalanceTitle.isSelected = true

        //ui handling here
        if (hasValue(records[groupPosition]!!.endingBalance)) {
            val endingBalance = records[groupPosition]!!.endingBalance
            viewHolderChild.endingBalanceValue.isSelected = true
            viewHolderChild.endingBalanceValue.typeface= getALSNormalFont()
            viewHolderChild.endingBalanceValue.text = getManatConcatedString(context, endingBalance!!, ConstantsUtility.TariffConstants.AZERI_SYMBOL, 0.20f, R.dimen._10ssp)
        }

        return view as View
    }//getChildView ends

    private fun getLocalizedTitle(): String {
        return when {
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.EN -> "Ending balance"
            ProjectApplication.mLocaleManager.getLanguage() == ConstantsUtility.LanguageKeywords.AZ -> "Son balans"
            else -> "Конечный баланс"
        }
    }

    private class GroupViewHolder(row: View) {
        val dateTimeValue: TextView = row.findViewById(R.id.dateTimeValue) as TextView
        val typeValue: TextView = row.findViewById(R.id.typeValue) as TextView
        val descriptionValue: TextView = row.findViewById(R.id.descriptionValue) as TextView
        val amountValue: TextView = row.findViewById(R.id.amountValue) as TextView
    }// class GroupViewHolder ends

    private class ChildViewHolder(row: View) {
        val endingBalanceValue: TextView = row.findViewById(R.id.endingBalanceValue) as TextView
        val endingBalanceTitle: TextView = row.findViewById(R.id.endingBalanceTitle) as TextView
    }// class ChildViewHolder ends

    override fun getChild(groupPosition: Int, childPosititon: Int): String {
        return this.records[groupPosition]!!.transactionType!!
    }//getChild ends

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }//getChildId ends

    override fun getChildrenCount(groupPosition: Int): Int {
        return 1
    }//getChildrenCount ends

    override fun getGroup(groupPosition: Int): Any {
        return this.records[groupPosition]!!
    }//getGroup ends

    override fun getGroupCount(): Int {
        return this.records.size
    }//getGroupCount ends

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }//getGroupId ends

    override fun hasStableIds(): Boolean {
        return false
    }//hasStableIds ends

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }//isChildSelectable ends
}//class ends