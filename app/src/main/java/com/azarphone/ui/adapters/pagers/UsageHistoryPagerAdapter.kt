package com.azarphone.ui.adapters.pagers

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.azarphone.ui.fragment.usagehistory.UsageHistoryDetailsFragment
import com.azarphone.ui.fragment.usagehistory.UsageHistorySummaryFragment

/**
 * @author Junaid Hassan on 04, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class UsageHistoryPagerAdapter(fm: androidx.fragment.app.FragmentManager, msisdn: String, val isCorporateCustomer: Boolean) : FragmentStatePagerAdapter(fm) {
    private var fragmentsList = listOf<androidx.fragment.app.Fragment>()

    init {
        fragmentsList = if (isCorporateCustomer) {
            listOf(UsageHistorySummaryFragment())
        } else {
            listOf(UsageHistorySummaryFragment(), UsageHistoryDetailsFragment())
        }
    }


    override fun getCount(): Int {
        return fragmentsList.size
    }//getCount ends

    override fun getItem(position: Int): androidx.fragment.app.Fragment {
        return when (position) {
            0 -> fragmentsList[0]
            1 -> fragmentsList[1]
            else -> fragmentsList[0]
        }//when ends
    }//getItem ends
}