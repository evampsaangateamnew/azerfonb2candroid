package com.azarphone.ui.adapters.recyclerviews

import android.content.Context
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewStub
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import com.azarphone.R
import com.azarphone.api.pojo.response.inappsurvey.InAppSurvey
import com.azarphone.api.pojo.response.inappsurvey.Surveys
import com.azarphone.api.pojo.response.inappsurvey.UploadSurvey
import com.azarphone.api.pojo.response.tariffsresponse.*
import com.azarphone.datamanagers.UserDataManager
import com.azarphone.eventhandler.OnClickSurveySubmit
import com.azarphone.eventhandler.TariffsSearchResultListener
import com.azarphone.eventhandler.UpdateListOnSuccessfullySurvey
import com.azarphone.ui.activities.mainactivity.MainViewModel
import com.azarphone.ui.dialogs.InAppFeedbackDialog
import com.azarphone.util.*
import com.azarphone.validators.hasValue
import com.azarphone.validators.isNumeric
import com.azarphone.widgets.tariffs.*
import com.es.tec.LocalSharedPrefStorage
import net.cachapa.expandablelayout.ExpandableLayout


/**
 * @author Junaid Hassan on 10, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
@Suppress("DEPRECATION")
class TariffsCardAdapter(
    context: Context,
    tariffCardItems: List<ItemsItem?>?,
    tariffsSearchResultListener: TariffsSearchResultListener,
    private val viewModel: MainViewModel
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>(),
    Filterable {

    private var isBonusLayoutExpanded: Boolean = false
    private var isPackagePricesLayoutExpanded: Boolean = false
    private var isDetailsLayoutExpanded: Boolean = false

    private var isCallView = false
    private var isSMSView = false
    private var isInternetView = false

    private var isCallPayGView = false
    private var isInternetPayGView = false
    private var isSMSPayGView = false

    private var isDetailsRoundingView = false
    private var isDetailsTextWithTitleView = false
    private var isDetailsTextWithoutTitleView = false
    private var isDetailsTextWithPointsView = false
    private var isRoamingView = false

    private val fromClass = "TariffsCardAdapter"
    private var context: Context? = null
    private var tariffCardItems: ArrayList<ItemsItem?>? = null
    private var tariffCardItemsTemp: ArrayList<ItemsItem?>? = null
    private lateinit var packagePriceView: View
    private lateinit var detailsView: View
    private val HANDLER_TIME = 150L
    private val expansionHandlerTime = 20L
    private var tariffsFilter: TariffsFilter? = null
    private var tariffsSearchResultListener: TariffsSearchResultListener? = null

    init {
        this.context = context
        this.tariffsSearchResultListener = tariffsSearchResultListener
        this.tariffCardItems = tariffCardItems as ArrayList<ItemsItem?>
        tariffCardItemsTemp = tariffCardItems
    }

    override fun getFilter(): Filter {
        if (tariffsFilter == null) {
            tariffsFilter = TariffsFilter()
        }
        return tariffsFilter!!
    }//getFilter ends

    /*override fun getItemId(position: Int): Long {
        return if (tariffCardItems!![position] != null) {
            tariffCardItems!![position]!!.hashCode().toLong()
        } else {
            position.hashCode().toLong()
        }
    }//getItemId ends

    override fun getItemViewType(position: Int): Int {
        return position
    }//getItemViewType ends*/

    private inner class TariffsFilter : Filter() {

        override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {
            val results = Filter.FilterResults()
            if (constraint == null) {
                results.values = tariffCardItemsTemp
                results.count = tariffCardItemsTemp!!.size
            } else if (constraint.toString().trim { it <= ' ' }.isEmpty()) {
                logE(
                    "conXY",
                    "searchKey:::".plus(constraint.toString()),
                    fromClass,
                    "showingConstraint"
                )
                tariffsSearchResultListener!!.onSearchResult(true)
                results.values = tariffCardItemsTemp
                results.count = tariffCardItemsTemp!!.size
            } else {
                logE(
                    "conXY",
                    "searchKey:::".plus(constraint.toString()),
                    fromClass,
                    "showingConstraint"
                )
                val filteredResult = ArrayList<ItemsItem>()
                for (offer in tariffCardItemsTemp!!) {
                    if (offer!!.header != null &&
                        offer.header!!.name!!.toLowerCase()
                            .contains(constraint.toString().toLowerCase())
                    ) {
                        filteredResult.add(offer)
                    }
                }

                if (filteredResult.size > 0) {
                    tariffsSearchResultListener!!.onSearchResult(true)
                    results.values = filteredResult
                    results.count = filteredResult.size
                } else {
                    tariffsSearchResultListener!!.onSearchResult(false)
                }
            }
            return results
        }

        override fun publishResults(constraint: CharSequence, results: Filter.FilterResults) {
            if (results.count > 0) {
                tariffsSearchResultListener!!.onSearchResult(true)
                tariffCardItems = results.values as ArrayList<ItemsItem?>
                notifyDataSetChanged()
            } else {
                tariffsSearchResultListener!!.onSearchResult(false)
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_tariff_card_items, parent, false)
        return TariffsCardAdapterViewHolder(view)
    }//onCreateViewHolder ends

    override fun getItemCount(): Int {
        return tariffCardItems!!.size
    }//getItemCount ends

    override fun onBindViewHolder(
        holder: androidx.recyclerview.widget.RecyclerView.ViewHolder,
        position: Int
    ) {
        val viewHolder = holder as TariffsCardAdapterViewHolder

        /**buttons text*/
        viewHolder.subscribeButton!!.text = getSubscribeButtonLabel()
        viewHolder.subscribeDisabledButton!!.text = getSubscribeButtonLabel()
        viewHolder.renewButton!!.text = getRenewButtonLabel()
        viewHolder.subscribedButton!!.text = getSubscribedButtonLabel()

        /**buttons marquee*/
        viewHolder.subscribeButton!!.isSelected = true
        viewHolder.renewButton!!.isSelected = true
        viewHolder.subscribedButton!!.isSelected = true
        viewHolder.subscribeDisabledButton!!.isSelected = true

        var rateStars = 0

        if (tariffCardItems != null) {
            if (tariffCardItems!![position] != null) {

                /**set the buttons*/
                if (tariffCardItems!![position]!!.subscribable != null && hasValue(tariffCardItems!![position]!!.subscribable)) {
                    when {
                        tariffCardItems!![position]!!.subscribable == ConstantsUtility.TariffConstants.CAN_NOT_SUBSCRIBE -> {
                            logE(
                                "subscribableTX",
                                "subscribable:::".plus(ConstantsUtility.TariffConstants.CAN_NOT_SUBSCRIBE),
                                fromClass,
                                "subscribable"
                            )
                            viewHolder.subscribeDisabledButton!!.visibility = View.VISIBLE
                            viewHolder.subscribeButton!!.visibility = View.GONE
                            viewHolder.renewButton!!.visibility = View.GONE
                            viewHolder.subscribedButton!!.visibility = View.GONE
                            viewHolder.ratingStarsLayout?.visibility = View.GONE
                        }
                        tariffCardItems!![position]!!.subscribable == ConstantsUtility.TariffConstants.ALREADY_SUBSCRIBED -> {
                            logE(
                                "subscribableTX",
                                "subscribable:::".plus(ConstantsUtility.TariffConstants.ALREADY_SUBSCRIBED),
                                fromClass,
                                "subscribable"
                            )
                            viewHolder.subscribeDisabledButton!!.visibility = View.GONE
                            viewHolder.subscribeButton!!.visibility = View.GONE
                            viewHolder.renewButton!!.visibility = View.GONE
                            viewHolder.subscribedButton!!.visibility = View.VISIBLE
                            viewHolder.ratingStarsLayout?.visibility = View.VISIBLE
                        }
                        tariffCardItems!![position]!!.subscribable == ConstantsUtility.TariffConstants.CAN_SUBSCRIBE -> {
                            logE(
                                "subscribableTX",
                                "subscribable:::".plus(ConstantsUtility.TariffConstants.CAN_SUBSCRIBE),
                                fromClass,
                                "subscribable"
                            )
                            viewHolder.subscribeDisabledButton!!.visibility = View.GONE
                            viewHolder.subscribeButton!!.visibility = View.VISIBLE
                            viewHolder.renewButton!!.visibility = View.GONE
                            viewHolder.subscribedButton!!.visibility = View.GONE
                            viewHolder.ratingStarsLayout?.visibility = View.GONE
                        }
                        tariffCardItems!![position]!!.subscribable == ConstantsUtility.TariffConstants.RENEWABLE -> {
                            logE(
                                "subscribableTX",
                                "subscribable:::".plus(ConstantsUtility.TariffConstants.RENEWABLE),
                                fromClass,
                                "subscribable"
                            )
                            viewHolder.subscribeDisabledButton!!.visibility = View.GONE
                            viewHolder.subscribeButton!!.visibility = View.GONE
                            viewHolder.renewButton!!.visibility = View.VISIBLE
                            viewHolder.subscribedButton!!.visibility = View.GONE
                            viewHolder.ratingStarsLayout?.visibility = View.VISIBLE
                        }
                    }
                }

                /**set the header of the tariff card*/
                if (tariffCardItems!![position]!!.header != null) {
                    if (tariffCardItems!![position]!!.header!!.bonusLabel != null && hasValue(
                            tariffCardItems!![position]!!.header!!.bonusLabel
                        )
                    ) {
                        viewHolder.bonusLayoutTitleLabel!!.text =
                            tariffCardItems!![position]!!.header!!.bonusLabel
                    }
                    setTariffHeader(tariffCardItems!![position]!!.header!!, viewHolder)
                    if (tariffCardItems!![position]!!.header!!.attributes != null && !tariffCardItems!![position]!!.header!!.attributes!!.isEmpty()) {
                        setTariffAttributes(
                            tariffCardItems!![position]!!.header!!.attributes!!,
                            viewHolder
                        )
                    } else {
                        viewHolder.bonusesParentLayout!!.visibility = View.GONE
                        viewHolder.bonusesContentLayout!!.visibility = View.GONE
                    }
                }

                /**set the package price section of the tariff card*/
                if (tariffCardItems!![position]!!.packagePrice != null) {
                    Handler().postDelayed({
                        viewHolder.packagePriceViewStub!!.layoutResource =
                            R.layout.layout_tariffs_package_price

                        if (viewHolder.packagePriceViewStub!!.parent != null) {
                            packagePriceView = viewHolder.packagePriceViewStub!!.inflate()
                        }


                        //set the parent layout title
                        val packagePriceItem = tariffCardItems!![position]!!.packagePrice!!
                        if (packagePriceItem.packagePriceLabel != null && hasValue(packagePriceItem.packagePriceLabel)) {
                            viewHolder.packagesLayoutTitleLabel!!.text =
                                packagePriceItem.packagePriceLabel
                        }//if (packagePrice.packagePriceLabel != null && hasValue(packagePrice.packagePriceLabel)) ends
                        setupPackagePriceSection(packagePriceView, packagePriceItem)

                    }, HANDLER_TIME)
                }

                /**set the details section of the tariff card*/
                if (tariffCardItems!![position]!!.details != null) {
                    Handler().postDelayed({
                        viewHolder.detailsViewStub!!.layoutResource =
                            R.layout.layout_tariffs_details_content_layout

                        if (viewHolder.detailsViewStub!!.parent != null) {
                            detailsView = viewHolder.detailsViewStub!!.inflate()

                        }

                        val detailsItem = tariffCardItems!![position]!!.details!!
                        //set the header text title
                        if (detailsItem.detailLabel != null && hasValue(detailsItem.detailLabel)) {
                            viewHolder.detailsLayoutTitleLabel!!.text = detailsItem.detailLabel
                        }

                        setupDetailsSection(detailsView, detailsItem)
                    }, HANDLER_TIME)
                }

                /** setting stars of rating*/
                val surveyData = UserDataManager.getInAppSurvey()?.data
                val findSurvey =
                    surveyData?.findSurvey(ConstantsUtility.InAppSurveyConstants.TARIFF_SUBSCRIBED_PAGE)
                findSurvey?.let {
                    surveyData.userSurveys?.let { userSurveyData ->
                        for (i in userSurveyData.indices) {
                            if (userSurveyData[i].offeringId!! == tariffCardItems!![position]!!.header!!.offeringId!!) {
                                userSurveyData[i].answer?.let {
                                    userSurveyData[i].surveyId?.let { userSurveyId ->
                                        val survey =
                                            surveyData.findSurveyById(
                                                userSurveyId.toIntOrNull() ?: 0
                                            )
                                        survey?.let { requiredSurvey ->
                                            val answerId = userSurveyData[i].answerId
                                            val questionId = userSurveyData[i].questionId
                                            if (isNumeric(answerId)) {
                                                if (isNumeric(questionId)) {
                                                    rateStars = setStars(
                                                        answerId!!.toInt(),
                                                        questionId!!.toInt(),
                                                        requiredSurvey,
                                                        viewHolder
                                                    )
                                                }
                                            }
                                        }
                                    }
                                } ?: run {
                                    rateStars = setStars(0, 0, null, viewHolder)
                                }
                            }
                        }
                    }
                } ?: run {
                    viewHolder.ratingStarsLayout?.visibility = View.GONE
                }
                viewHolder.ratingStarsLayout?.setOnClickListener {
                    val tariffId = tariffCardItems!![position]!!.header!!.offeringId!!
                    context?.let {
                        inAppFeedback(it, tariffId, rateStars, position)
                    }

                }

            }//if (tariffCardItems[position] != null)  ends
        }//if (tariffCardItems != null)  ends

        /**set the expandable layouts behavior*/
        initExpandableLayoutsBehavior(position, viewHolder)

        //ui events
        initUIEvents(viewHolder, position)
    }//onBindViewHolder ends

    private fun inAppFeedback(context: Context, tariffId: String, rating: Int, position: Int) {

        var inAppFeedbackDialog: InAppFeedbackDialog? = null
        var currentVisit: Int =
            LocalSharedPrefStorage(context).getInt(LocalSharedPrefStorage.PREF_TARIFF_SUBSCRIBED_PAGE)
        currentVisit += 1
        val inAppSurvey: InAppSurvey? = UserDataManager.getInAppSurvey()
        if ((inAppSurvey?.data) != null) {
            val surveys: Surveys? =
                inAppSurvey.data.findSurvey(ConstantsUtility.InAppSurveyConstants.TARIFF_SUBSCRIBED_PAGE)
            if (surveys != null) {
                inAppFeedbackDialog = InAppFeedbackDialog(context, object : OnClickSurveySubmit {
                    override fun onClickSubmit(
                        uploadSurvey: UploadSurvey,
                        onTransactionComplete: String,
                        updateListOnSuccessfullySurvey: UpdateListOnSuccessfullySurvey?
                    ) {
                        viewModel.uploadInAppSurvey(
                            context,
                            uploadSurvey,
                            onTransactionComplete,
                            updateListOnSuccessfullySurvey
                        )
                        if (inAppFeedbackDialog?.isShowing == true) {
                            inAppFeedbackDialog?.dismiss()
                        }
                    }

                })
                if (surveys.questions != null) {
                    currentVisit = 0
                    inAppFeedbackDialog.setUpdateListOnSuccessfullySurveyListener(object :
                        UpdateListOnSuccessfullySurvey {
                        override fun updateListOnSuccessfullySurvey() {
                            updateList(position)
                        }
                    })
                    // 1 for tariff and 2 for supplementary
                    inAppFeedbackDialog.showTariffDialog(surveys, "1", tariffId, rating)
                }
            }
        }
        LocalSharedPrefStorage(context).addInt(
            LocalSharedPrefStorage.PREF_TARIFF_SUBSCRIBED_PAGE,
            currentVisit
        )
    }

    private fun updateList(position: Int) {
        notifyItemChanged(position)
    }

    private fun setStars(
        answerId: Int,
        questionId: Int,
        survey: Surveys?,
        viewHolder: TariffsCardAdapterViewHolder
    ): Int {
        survey?.let { currentSurvey ->
            val question = currentSurvey.findQuestion(questionId)
            question?.let { currentQuestion ->
                val answers = currentQuestion.answers
                answers?.let { currentAnswer ->
                    if (currentAnswer.isNotEmpty() && currentAnswer[0].id == answerId) {
                        viewHolder.starOne?.isChecked = true
                        viewHolder.starTwo?.isChecked = false
                        viewHolder.starThree?.isChecked = false
                        viewHolder.starFour?.isChecked = false
                        viewHolder.starFive?.isChecked = false
                        return 1
                    } else if (currentAnswer.size > 1 && currentAnswer[1].id == answerId) {
                        viewHolder.starOne?.isChecked = true
                        viewHolder.starTwo?.isChecked = true
                        viewHolder.starThree?.isChecked = false
                        viewHolder.starFour?.isChecked = false
                        viewHolder.starFive?.isChecked = false
                        return 2
                    } else if (currentAnswer.size > 2 && currentAnswer[2].id == answerId) {
                        viewHolder.starOne?.isChecked = true
                        viewHolder.starTwo?.isChecked = true
                        viewHolder.starThree?.isChecked = true
                        viewHolder.starFour?.isChecked = false
                        viewHolder.starFive?.isChecked = false
                        return 3
                    } else if (currentAnswer.size > 3 && currentAnswer[3].id == answerId) {
                        viewHolder.starOne?.isChecked = true
                        viewHolder.starTwo?.isChecked = true
                        viewHolder.starThree?.isChecked = true
                        viewHolder.starFour?.isChecked = true
                        viewHolder.starFive?.isChecked = false
                        return 4
                    } else if (currentAnswer.size > 4 && currentAnswer[4].id == answerId) {
                        viewHolder.starOne?.isChecked = true
                        viewHolder.starTwo?.isChecked = true
                        viewHolder.starThree?.isChecked = true
                        viewHolder.starFour?.isChecked = true
                        viewHolder.starFive?.isChecked = true
                        return 5
                    }

                } ?: run {
                    viewHolder.starOne?.isChecked = false
                    viewHolder.starTwo?.isChecked = false
                    viewHolder.starThree?.isChecked = false
                    viewHolder.starFour?.isChecked = false
                    viewHolder.starFive?.isChecked = false
                    return 0
                }
            } ?: run {
                viewHolder.starOne?.isChecked = false
                viewHolder.starTwo?.isChecked = false
                viewHolder.starThree?.isChecked = false
                viewHolder.starFour?.isChecked = false
                viewHolder.starFive?.isChecked = false
                return 0
            }
        } ?: run {
            viewHolder.starOne?.isChecked = false
            viewHolder.starTwo?.isChecked = false
            viewHolder.starThree?.isChecked = false
            viewHolder.starFour?.isChecked = false
            viewHolder.starFive?.isChecked = false
            return 0
        }
        return 0
    }


    private fun initExpandableLayoutsBehavior(
        position: Int,
        viewHolder: TariffsCardAdapterViewHolder
    ) {
        if (isDetailsLayoutExpanded || isPackagePricesLayoutExpanded || isBonusLayoutExpanded) {
            logE("tarX123", "anyone true", fromClass, "expandedPosition")
            if (isBonusLayoutExpanded) {
                logE("tarX123", "isBonusLayoutExpanded", fromClass, "isBonusLayoutExpanded")
                collapsePackagesPricesLayout(viewHolder)
                collapseDetailsLayout(viewHolder)
                expandBonusLayout(viewHolder)
            } else if (isPackagePricesLayoutExpanded) {
                logE(
                    "tarX123",
                    "isPackagePricesLayoutExpanded",
                    fromClass,
                    "isPackagePricesLayoutExpanded"
                )
                collapseBonusLayout(viewHolder)
                collapseDetailsLayout(viewHolder)
                expandPackagesPricesLayout(viewHolder)
            } else if (isDetailsLayoutExpanded) {
                logE("tarX123", "isDetailsLayoutExpanded", fromClass, "isDetailsLayoutExpanded")
                collapseBonusLayout(viewHolder)
                collapsePackagesPricesLayout(viewHolder)
                expandDetailsLayout(viewHolder)
            }
        } else {
            logE("tarX123", "allllll false", fromClass, "expandedPosition NOT")
            if (tariffCardItems!![position]!!.header != null) {
                if (tariffCardItems!![position]!!.header!!.attributes != null && !tariffCardItems!![position]!!.header!!.attributes!!.isEmpty()) {
                    expandBonusLayout(viewHolder)
                } else {
                    expandPackagesPricesLayout(viewHolder)
                }
            }
        }

    }//initExpandableLayoutsBehavior ends

    private fun setupDetailsSection(detailsView: View, details: Details) {
        val detailsRoundingHeaderLayout: LinearLayout =
            detailsView.findViewById(R.id.detailsRoundingHeaderLayout) as LinearLayout
        val viewBelowDetailsRoundingContentLayout: View =
            detailsView.findViewById(R.id.viewBelowDetailsRoundingContentLayout) as View
        val detailsTextWithTitleText: TextView =
            detailsView.findViewById(R.id.detailsTextWithTitleText) as TextView
        val viewBelowDetailsTextWithTitleHeaderLabel: View =
            detailsView.findViewById(R.id.viewBelowDetailsTextWithTitleHeaderLabel) as View
        val viewBelowDetailsTextWithPointsContentLayout: View =
            detailsView.findViewById(R.id.viewBelowDetailsTextWithPointsContentLayout) as View
        val detailsRoamingHeaderLabel: TextView =
            detailsView.findViewById(R.id.detailsRoamingHeaderLabel) as TextView
        val detailsTextWithPointsContentLayout: LinearLayout =
            detailsView.findViewById(R.id.detailsTextWithPointsContentLayout) as LinearLayout
        val detailsTextWithTitleHeaderLabel: TextView =
            detailsView.findViewById(R.id.detailsTextWithTitleHeaderLabel) as TextView
        val detailRoamingContentLayout: LinearLayout =
            detailsView.findViewById(R.id.detailRoamingContentLayout) as LinearLayout
        val detailsTextWithoutTitleLabel: TextView =
            detailsView.findViewById(R.id.detailsTextWithoutTitleLabel) as TextView
        val viewBelowDetailsTextWithoutTitleLabel: View =
            detailsView.findViewById(R.id.viewBelowDetailsTextWithoutTitleLabel) as View
        val detailPricesHeaderLayout: LinearLayout =
            detailsView.findViewById(R.id.detailPricesHeaderLayout) as LinearLayout
        val detailsRoamingHeaderIcon: ImageView =
            detailsView.findViewById(R.id.detailsRoamingHeaderIcon) as ImageView
        val detailsTextWithPointsHeaderLabel =
            detailsView.findViewById(R.id.detailsTextWithPointsHeaderLabel) as TextView
        val detailsTextWithPointsHeaderIcon =
            detailsView.findViewById(R.id.detailsTextWithPointsHeaderIcon) as ImageView
        val detailsTextWithPointsHeaderLayout =
            detailsView.findViewById(R.id.detailsTextWithPointsHeaderLayout) as LinearLayout
        val textWithTitleHeaderLayout =
            detailsView.findViewById(R.id.textWithTitleHeaderLayout) as LinearLayout
        val textWithTitleHeaderIcon =
            detailsView.findViewById(R.id.textWithTitleHeaderIcon) as ImageView


        /**set the details prices section*/
        if (details.price != null && details.price.isNotEmpty()) {
            setupDetailsPriceSection(details.price, detailsView)
        } else {
            detailPricesHeaderLayout.visibility = View.GONE
        }

        /** set the details rounding section*/
        if (details.rounding != null) {
            isDetailsRoundingView = true
            setupDetailsRoundingSection(details.rounding, detailsView)
        } else {
            detailsRoundingHeaderLayout.visibility = View.GONE
            isDetailsRoundingView = false
        }

        /**set the text with title layout*/
        if (details.textWithTitle != null) {
            isDetailsTextWithTitleView = true
            if (details.textWithTitle.titleIcon != null && hasValue(details.textWithTitle.titleIcon)) {
                textWithTitleHeaderIcon.setImageResource(getCardAdapterMappedImage(details.textWithTitle.titleIcon))
            } else {
                /* Log.e("asd234","asdasdasdasdadasdad")*/
            }
            setupTextWithTitleSection(details.textWithTitle, detailsView)
        } else {
            textWithTitleHeaderLayout.visibility = View.GONE
            detailsTextWithTitleHeaderLabel.visibility = View.GONE
            detailsTextWithTitleText.visibility = View.GONE
            isDetailsTextWithTitleView = false
        }

        /**set the text with points layout*/
        if (details.textWithPoints != null) {
            //juni12891226nosi
            isDetailsTextWithPointsView = true
            /**set the title*/
            if (details.textWithPoints.title != null && hasValue(details.textWithPoints.title)) {
                detailsTextWithPointsHeaderLabel.text = details.textWithPoints.title
                detailsTextWithPointsHeaderLabel.isSelected = true
            }

            /**set the title icon*/
            if (details.textWithPoints.titleIcon != null && hasValue(details.textWithPoints.titleIcon)) {
                detailsTextWithPointsHeaderIcon.setImageResource(getCardAdapterMappedImage(details.textWithPoints.titleIcon))
            }

            /**set the points*/
            if (details.textWithPoints.pointsList != null && details.textWithPoints.pointsList.isNotEmpty()) {
                setupTextWithPointsSection(details.textWithPoints.pointsList, detailsView)
            } else {
                isDetailsTextWithPointsView = false
                detailsTextWithPointsContentLayout.visibility = View.GONE
                detailsTextWithPointsHeaderLayout.visibility = View.GONE
                detailsTextWithPointsHeaderIcon.visibility = View.GONE
                detailsTextWithPointsHeaderLabel.visibility = View.GONE
            }

            /**title and icon not there*/
            if (!hasValue(details.textWithPoints.title) &&
                !hasValue(details.textWithPoints.titleIcon)
            ) {
                detailsTextWithPointsHeaderLayout.visibility = View.GONE
            }

            /**nothing is there*/
            if (!hasValue(details.textWithPoints.title) &&
                !hasValue(details.textWithPoints.titleIcon) &&
                details.textWithPoints.pointsList.isNullOrEmpty()
            ) {
                isDetailsTextWithPointsView = false
                detailsTextWithPointsContentLayout.visibility = View.GONE
            }
        } else {
            isDetailsTextWithPointsView = false
            detailsTextWithPointsContentLayout.visibility = View.GONE
            detailsTextWithPointsHeaderLayout.visibility = View.GONE
        }

        /**set the text without title*/
        if (details.textWithOutTitle != null) {
            isDetailsTextWithoutTitleView = true
            if (details.textWithOutTitle.description != null && hasValue(details.textWithOutTitle.description)) {
                detailsTextWithoutTitleLabel.text = details.textWithOutTitle.description
            } else {
                detailsTextWithoutTitleLabel.visibility = View.GONE
                isDetailsTextWithoutTitleView = false
            }
        } else {
            detailsTextWithoutTitleLabel.visibility = View.GONE
            isDetailsTextWithoutTitleView = false
        }

        /**set the roaming layout*/
        if (details.roamingDetails != null) {
            isRoamingView = true
            logE("roamX", "roamingDetails is not null", fromClass, "setupDetailsSection")
            if (details.roamingDetails.descriptionAbove != null && hasValue(details.roamingDetails.descriptionAbove)) {
                detailsRoamingHeaderLabel.text = details.roamingDetails.descriptionAbove
            } else {
                detailsRoamingHeaderLabel.visibility = View.GONE
            }

            if (details.roamingDetails.roamingIcon != null && hasValue(details.roamingDetails.roamingIcon)) {
                detailsRoamingHeaderIcon.setImageResource(getCardAdapterMappedImage(details.roamingDetails.roamingIcon))
            }

            //set the countries
            if (details.roamingDetails.roamingDetailsCountriesList != null && !details.roamingDetails.roamingDetailsCountriesList.isEmpty()) {
                setupRoamingSection(details.roamingDetails.roamingDetailsCountriesList, detailsView)
            } else {
                detailRoamingContentLayout.visibility = View.GONE
            }
        } else {
            logE("roamX", "roamingDetails is null", fromClass, "setupDetailsSection")
            detailsRoamingHeaderLabel.visibility = View.GONE
            detailRoamingContentLayout.visibility = View.GONE
            isRoamingView = false
        }

        if (isDetailsRoundingView) {
            viewBelowDetailsRoundingContentLayout.visibility = View.VISIBLE
            if (!isDetailsTextWithTitleView) {
                viewBelowDetailsRoundingContentLayout.visibility = View.GONE
                if (!isDetailsTextWithoutTitleView) {
                    viewBelowDetailsRoundingContentLayout.visibility = View.GONE
                    if (!isDetailsTextWithPointsView) {
                        viewBelowDetailsRoundingContentLayout.visibility = View.GONE
                        if (!isRoamingView) {
                            viewBelowDetailsRoundingContentLayout.visibility = View.GONE
                        } else {
                            viewBelowDetailsRoundingContentLayout.visibility = View.VISIBLE
                        }//if (!isRoamingView) ends
                    } else {
                        viewBelowDetailsRoundingContentLayout.visibility = View.VISIBLE
                    }//if (!isDetailsTextWithPointsView) ends
                } else {
                    viewBelowDetailsRoundingContentLayout.visibility = View.VISIBLE
                }//if (!isDetailsTextWithoutTitleView) ends
            } else {
                viewBelowDetailsRoundingContentLayout.visibility = View.VISIBLE
            }//if (!isDetailsTextWithTitleView) ends
        }//if (isDetailsRoundingView) ends

        if (isDetailsTextWithTitleView) {
            viewBelowDetailsTextWithTitleHeaderLabel.visibility = View.VISIBLE
            if (!isDetailsTextWithoutTitleView) {
                viewBelowDetailsTextWithTitleHeaderLabel.visibility = View.GONE
                if (!isDetailsTextWithPointsView) {
                    viewBelowDetailsTextWithTitleHeaderLabel.visibility = View.GONE
                    if (!isRoamingView) {
                        viewBelowDetailsTextWithTitleHeaderLabel.visibility = View.GONE
                    } else {
                        viewBelowDetailsTextWithTitleHeaderLabel.visibility = View.VISIBLE
                    }//if (!isRoamingView) ends
                } else {
                    viewBelowDetailsTextWithTitleHeaderLabel.visibility = View.VISIBLE
                }// if (!isDetailsTextWithPointsView) ends
            } else {
                viewBelowDetailsTextWithTitleHeaderLabel.visibility = View.VISIBLE
            }//if (!isDetailsTextWithoutTitleView) ends
        }//if (isDetailsTextWithTitleView)  ends

        if (isDetailsTextWithoutTitleView) {
            viewBelowDetailsTextWithoutTitleLabel.visibility = View.VISIBLE
            if (!isDetailsTextWithPointsView) {
                viewBelowDetailsTextWithoutTitleLabel.visibility = View.GONE
                if (!isRoamingView) {
                    viewBelowDetailsTextWithoutTitleLabel.visibility = View.GONE
                } else {
                    viewBelowDetailsTextWithoutTitleLabel.visibility = View.VISIBLE
                }//if (!isRoamingView)  ends
            } else {
                viewBelowDetailsTextWithoutTitleLabel.visibility = View.VISIBLE
            }
        }//if (isDetailsTextWithoutTitleView) ends

        if (isDetailsTextWithPointsView) {
            viewBelowDetailsTextWithPointsContentLayout.visibility = View.VISIBLE
            if (!isRoamingView) {
                viewBelowDetailsTextWithPointsContentLayout.visibility = View.GONE
            } else {
                viewBelowDetailsTextWithPointsContentLayout.visibility = View.VISIBLE
            }//if (!isRoamingView) ends
        }//if (isDetailsTextWithPointsView) ends
    }//setupDetailsSection ends

    private fun setupRoamingSection(
        roamingDetailsCountriesList: List<RoamingDetailsCountriesListItem?>,
        detailsView: View
    ) {
        val detailRoamingContentLayout: LinearLayout =
            detailsView.findViewById(R.id.detailRoamingContentLayout) as LinearLayout

        detailRoamingContentLayout.removeAllViews()
        for (its in roamingDetailsCountriesList.indices) {
            val tariffsRoamingViewItem = TariffsRoamingViewItem(context!!)

            if (roamingDetailsCountriesList[its]!!.countryName != null && hasValue(
                    roamingDetailsCountriesList[its]!!.countryName
                )
            ) {
                tariffsRoamingViewItem.getDetailsRoamingCountryLabel().text =
                    roamingDetailsCountriesList[its]!!.countryName
                tariffsRoamingViewItem.getDetailsRoamingCountryLabel().isSelected = true
            }

            if (roamingDetailsCountriesList[its]!!.flag != null && hasValue(
                    roamingDetailsCountriesList[its]!!.flag
                )
            ) {
                tariffsRoamingViewItem.getDetailsRoamingCountryFlag().setImageDrawable(
                    getImageFromAssests(
                        context!!,
                        roamingDetailsCountriesList[its]!!.flag!!
                    )
                )
            }

            /**set the operators*/
            if (roamingDetailsCountriesList[its]!!.operatorList != null && !roamingDetailsCountriesList[its]!!.operatorList!!.isEmpty()) {
                tariffsRoamingViewItem.getroamingOperatorsLayout().removeAllViews()

                for (operators in roamingDetailsCountriesList[its]!!.operatorList!!.indices) {
                    val tariffsRoamingOperatorsViewItem = TariffsRoamingOperatorsViewItem(context!!)

                    if (roamingDetailsCountriesList[its]!!.operatorList!![operators] != null &&
                        hasValue(roamingDetailsCountriesList[its]!!.operatorList!![operators])
                    ) {
                        tariffsRoamingOperatorsViewItem.getRoamingOperatorViewItemValue().text =
                            roamingDetailsCountriesList[its]!!.operatorList!![operators].toString()
                        tariffsRoamingOperatorsViewItem.getRoamingOperatorViewItemValue().isSelected =
                            true
                    }

                    tariffsRoamingViewItem.getroamingOperatorsLayout()
                        .addView(tariffsRoamingOperatorsViewItem)
                }//for operators end
            }

            detailRoamingContentLayout.addView(tariffsRoamingViewItem)
        }//for ends

    }//setupRoamingSection ends

    private fun setupTextWithPointsSection(pointsList: List<String?>, detailsView: View) {
        val detailsTextWithPointsContentLayout: LinearLayout =
            detailsView.findViewById(R.id.detailsTextWithPointsContentLayout) as LinearLayout

        detailsTextWithPointsContentLayout.removeAllViews()
        for (its in pointsList.indices) {
            val tariffsTextWithPointsViewItem = TariffsTextWithPointsViewItem(context!!)

            if (pointsList[its] != null && hasValue(pointsList[its].toString())) {
                tariffsTextWithPointsViewItem.getTextWithPointsViewItemValue().text =
                    pointsList[its].toString()
            } else {
                tariffsTextWithPointsViewItem.getTextWithPointsViewItemValue().visibility =
                    View.GONE
            }//if (pointsList[its] != null && hasValue(pointsList[its].toString())) ends

            if (!hasValue(pointsList[its].toString())) {
                tariffsTextWithPointsViewItem.getTextWithPointsViewItemLayout().visibility =
                    View.GONE
            }

            detailsTextWithPointsContentLayout.addView(tariffsTextWithPointsViewItem)
        }//for ends
    }//setupTextWithPointsSection ends

    private fun setupTextWithTitleSection(textWithTitle: TextWithTitle, detailsView: View) {
        val detailsTextWithTitleHeaderLabel: TextView =
            detailsView.findViewById(R.id.detailsTextWithTitleHeaderLabel) as TextView
        val detailsTextWithTitleText: TextView =
            detailsView.findViewById(R.id.detailsTextWithTitleText) as TextView
        val viewBelowDetailsTextWithTitleHeaderLabel: View =
            detailsView.findViewById(R.id.viewBelowDetailsTextWithTitleHeaderLabel) as View
        val textWithTitleHeaderLayout =
            detailsView.findViewById(R.id.textWithTitleHeaderLayout) as LinearLayout

        if (textWithTitle.title != null && hasValue(textWithTitle.title)) {
            detailsTextWithTitleHeaderLabel.text = textWithTitle.title
            detailsTextWithTitleHeaderLabel.isSelected = true
        }

        if (textWithTitle.text != null && hasValue(textWithTitle.text)) {
            detailsTextWithTitleText.text = textWithTitle.text
        } else {
            detailsTextWithTitleText.visibility = View.GONE
        }

        if (!hasValue(textWithTitle.title) && !hasValue(textWithTitle.text)) {
            isDetailsTextWithTitleView = false
            detailsTextWithTitleText.visibility = View.GONE
            detailsTextWithTitleHeaderLabel.visibility = View.GONE
            textWithTitleHeaderLayout.visibility = View.GONE
        }
    }//setupTextWithTitleSection ends

    private fun setupDetailsRoundingSection(rounding: Rounding, detailsView: View) {
        val detailsRoundingHeaderIcon: ImageView =
            detailsView.findViewById(R.id.detailsRoundingHeaderIcon) as ImageView
        val detailsRoundingHeaderLabel: TextView =
            detailsView.findViewById(R.id.detailsRoundingHeaderLabel) as TextView
        val detailsRoundingHeaderValue: TextView =
            detailsView.findViewById(R.id.detailsRoundingHeaderValue) as TextView
        val detailsRoundingContentLayout: LinearLayout =
            detailsView.findViewById(R.id.detailsRoundingContentLayout) as LinearLayout
        val viewBelowDetailsRoundingContentLayout: View =
            detailsView.findViewById(R.id.viewBelowDetailsRoundingContentLayout) as View

        //rounding title here
        if (rounding.title != null && hasValue(rounding.title)) {
            detailsRoundingHeaderLabel.text = rounding.title
            detailsRoundingHeaderLabel.isSelected = true
        }//if (rounding.title != null && hasValue(rounding.title)) ends

        //rounding titleValueRight here
        if (rounding.value != null && hasValue(rounding.value)) {
            detailsRoundingHeaderValue.text = rounding.value
            detailsRoundingHeaderValue.isSelected = true
        }//if (rounding.value != null && hasValue(rounding.value)) ends

        //rounding icon here
        if (rounding.iconName != null && hasValue(rounding.iconName)) {
            detailsRoundingHeaderIcon.setImageResource(getCardAdapterMappedImage(rounding.iconName))
        }//if (rounding.iconName != null && hasValue(rounding.iconName)) ends

        /**set the sms attributes*/
        if (rounding.attributeList != null && rounding.attributeList.isNotEmpty()) {
            setDetailsRoundingAttributes(rounding.attributeList, detailsView)
        } else {
            detailsRoundingContentLayout.visibility = View.GONE
            isDetailsRoundingView = false
        }//if (rounding.attributeList != null && !rounding.attributeList.isEmpty()) ends
    }//setupDetailsRoundingSection ends

    private fun setDetailsRoundingAttributes(
        attributeList: List<AttributeListItem?>,
        detailsView: View
    ) {
        val detailsRoundingContentLayout: LinearLayout =
            detailsView.findViewById(R.id.detailsRoundingContentLayout) as LinearLayout
        //iterate over the list and add view items
        //remove the view
        detailsRoundingContentLayout.removeAllViews()
        for (its in attributeList.indices) {
            val tariffsRoundingViewItem = TariffsRoundingViewItem(context!!)

            if (attributeList[its]!!.title != null && hasValue(attributeList[its]!!.title)) {
                tariffsRoundingViewItem.getRoundingViewItemLabel().text = attributeList[its]!!.title
                tariffsRoundingViewItem.getRoundingViewItemLabel().isSelected = true
            }

            if (attributeList[its]!!.value != null && hasValue(attributeList[its]!!.value)) {
                if (attributeList[its]!!.unit != null && hasValue(attributeList[its]!!.unit)) {
                    if (attributeList[its]!!.unit.equals(
                            ConstantsUtility.TariffConstants.ATTRIBUTE_LIST_UNIT,
                            ignoreCase = true
                        )
                    ) {
                        //show the manat with the value
                        val value = attributeList[its]!!.value
                        tariffsRoundingViewItem.getRoundingViewItemValue().text =
                            getManatConcatedString(
                                context!!, value!!, ConstantsUtility.TariffConstants.AZERI_SYMBOL,
                                0.25f, R.dimen._10ssp
                            )
                        tariffsRoundingViewItem.getRoundingViewItemValue().isSelected = true
                    } else {
                        //dont show the manat with the value
                        val value = attributeList[its]!!.value
                        tariffsRoundingViewItem.getRoundingViewItemValue().text = value!!
                        tariffsRoundingViewItem.getRoundingViewItemValue().isSelected = true
                    }
                } else {
                    tariffsRoundingViewItem.getRoundingViewItemValue().text =
                        attributeList[its]!!.value
                    tariffsRoundingViewItem.getRoundingViewItemValue().isSelected = true
                }
            }//if (attributeList[its]!!.value != null && hasValue(attributeList[its]!!.value))  ends

            if (!hasValue(attributeList[its]!!.title) && !hasValue(attributeList[its]!!.value)) {
                tariffsRoundingViewItem.getRoundingViewItemLayout().visibility = View.GONE
            }

            detailsRoundingContentLayout.addView(tariffsRoundingViewItem)
        }//for ends
    }//setDetailsRoundingAttributes ends

    private fun setupDetailsPriceSection(price: List<PriceItem?>, detailsView: View) {
        val detailPricesHeaderLayout: LinearLayout =
            detailsView.findViewById(R.id.detailPricesHeaderLayout) as LinearLayout

        //iterate over the list and add the view items
        detailPricesHeaderLayout.removeAllViews()
        for (its in price.indices) {
            val tariffsDetailsPriceViewItem = TariffsDetailsPriceViewItem(context!!)

            if (price[its]!!.iconName != null && hasValue(price[its]!!.iconName)) {
                tariffsDetailsPriceViewItem.getDetailsPriceIcon()
                    .setImageResource(getCardAdapterMappedImage(price[its]!!.iconName!!))
            }//if (price[its]!!.iconName != null && hasValue(price[its]!!.iconName)) ends

            if (price[its]!!.title != null && hasValue(price[its]!!.title)) {
                tariffsDetailsPriceViewItem.getDetailsHeaderLabel().text = price[its]!!.title
                tariffsDetailsPriceViewItem.getDetailsHeaderLabel().isSelected = true
            }//if (price[its]!!.title != null && hasValue(price[its]!!.title)) ends

            if (price[its]!!.value != null && hasValue(price[its]!!.value)) {
                if (price[its]!!.offersCurrency != null && hasValue(price[its]!!.offersCurrency)) {
                    if (price[its]!!.offersCurrency.equals(
                            ConstantsUtility.TariffConstants.PRICE_OFFERS_CURRENCY,
                            ignoreCase = true
                        )
                    ) {
                        val value = price[its]!!.value
                        tariffsDetailsPriceViewItem.getDetailsHeaderValue().text =
                            getManatConcatedString(
                                context!!, value!!, ConstantsUtility.TariffConstants.AZERI_SYMBOL,
                                0.25f, R.dimen._10ssp
                            )
                        tariffsDetailsPriceViewItem.getDetailsHeaderValue().isSelected = true
                    } else {
                        tariffsDetailsPriceViewItem.getDetailsHeaderValue().text =
                            price[its]!!.value
                        tariffsDetailsPriceViewItem.getDetailsHeaderValue().isSelected = true
                    }
                } else {
                    tariffsDetailsPriceViewItem.getDetailsHeaderValue().text = price[its]!!.value
                    tariffsDetailsPriceViewItem.getDetailsHeaderValue().isSelected = true
                }
            }

            if (!hasValue(price[its]!!.value) && !hasValue(price[its]!!.title)) {
                tariffsDetailsPriceViewItem.getDetailsPriceViewItemLayout().visibility = View.GONE
            }

            /**populate the content*/
            if (price[its]!!.attributeList != null && !price[its]!!.attributeList!!.isEmpty()) {
                tariffsDetailsPriceViewItem.getDetailsContentLayout().removeAllViews()
                //iterate over the attributes and populate the details price attributes view items
                for (iterations in price[its]!!.attributeList!!.indices) {
                    val tariffsDetailsPriceAttributesViewItem =
                        TariffsDetailsPriceAttributesViewItem(context!!)

                    if (price[its]!!.attributeList!![iterations]!!.title != null && hasValue(price[its]!!.attributeList!![iterations]!!.title)) {
                        tariffsDetailsPriceAttributesViewItem.getDetailPriceAttributeViewItemLabel().text =
                            price[its]!!.attributeList!![iterations]!!.title
                        tariffsDetailsPriceAttributesViewItem.getDetailPriceAttributeViewItemLabel().isSelected =
                            true
                    }

                    if (price[its]!!.attributeList!![iterations]!!.value != null && hasValue(price[its]!!.attributeList!![iterations]!!.value)) {
                        if (price[its]!!.attributeList!![iterations]!!.unit != null && hasValue(
                                price[its]!!.attributeList!![iterations]!!.unit
                            )
                        ) {
                            if (price[its]!!.attributeList!![iterations]!!.unit!!.equals(
                                    ConstantsUtility.TariffConstants.ATTRIBUTE_LIST_UNIT,
                                    ignoreCase = true
                                )
                            ) {
                                val value = price[its]!!.attributeList!![iterations]!!.value
                                tariffsDetailsPriceAttributesViewItem.getDetailPriceAttributeItemValue().text =
                                    getManatConcatedString(
                                        context!!,
                                        value!!,
                                        ConstantsUtility.TariffConstants.AZERI_SYMBOL,
                                        0.25f,
                                        R.dimen._10ssp
                                    )
                                tariffsDetailsPriceAttributesViewItem.getDetailPriceAttributeItemValue().isSelected =
                                    true
                            } else {
                                tariffsDetailsPriceAttributesViewItem.getDetailPriceAttributeItemValue().text =
                                    price[its]!!.attributeList!![iterations]!!.value
                                tariffsDetailsPriceAttributesViewItem.getDetailPriceAttributeItemValue().isSelected =
                                    true
                            }
                        } else {
                            if (isNumeric(price[its]!!.attributeList!![iterations]!!.value)) {
                                val value = price[its]!!.attributeList!![iterations]!!.value
                                tariffsDetailsPriceAttributesViewItem.getDetailPriceAttributeItemValue().text =
                                    getManatConcatedString(
                                        context!!,
                                        value!!,
                                        ConstantsUtility.TariffConstants.AZERI_SYMBOL,
                                        0.25f,
                                        R.dimen._10ssp
                                    )
                                tariffsDetailsPriceAttributesViewItem.getDetailPriceAttributeItemValue().isSelected =
                                    true
                            } else {
                                tariffsDetailsPriceAttributesViewItem.getDetailPriceAttributeItemValue().text =
                                    price[its]!!.attributeList!![iterations]!!.value
                                tariffsDetailsPriceAttributesViewItem.getDetailPriceAttributeItemValue().isSelected =
                                    true
                            }
                        }
                    }

                    if (!hasValue(price[its]!!.attributeList!![iterations]!!.value) && !hasValue(
                            price[its]!!.attributeList!![iterations]!!.title
                        )
                    ) {
                        tariffsDetailsPriceAttributesViewItem.getDetailPriceAttributeViewItemLayout().visibility =
                            View.GONE
                    }

                    tariffsDetailsPriceViewItem.getDetailsContentLayout()
                        .addView(tariffsDetailsPriceAttributesViewItem)
                }//for ends
            } else {
                tariffsDetailsPriceViewItem.getDetailsContentLayout().visibility = View.GONE
                tariffsDetailsPriceViewItem.getViewBelowPriceDetailsContentLayoutLayout().visibility =
                    View.GONE
            }//if (price[its]!!.attributeList != null && !price[its]!!.attributeList!!.isEmpty()) ends

            detailPricesHeaderLayout.addView(tariffsDetailsPriceViewItem)
        }//for ends
    }//setupDetailsPriceSection ends

    private fun setupPackagePriceSection(packagePriceView: View, packagePrice: PackagePrice) {
        val callPricesHeaderLayout: LinearLayout =
            packagePriceView.findViewById(R.id.callPricesHeaderLayout) as LinearLayout
        val viewBelowCallsContentLayout: View =
            packagePriceView.findViewById(R.id.viewBelowCallsContentLayout) as View
        val smsPricesHeaderLayout: ConstraintLayout =
            packagePriceView.findViewById(R.id.smsPricesHeaderLayout) as ConstraintLayout
        val viewBelowSmsContentLayout: View =
            packagePriceView.findViewById(R.id.viewBelowSmsContentLayout) as View
        val internetPricesHeaderLayout: ConstraintLayout =
            packagePriceView.findViewById(R.id.internetPricesHeaderLayout) as ConstraintLayout
        val viewBelowInternetContentLayout: View =
            packagePriceView.findViewById(R.id.viewBelowInternetContentLayout) as View
        val callpaygPricesHeaderLayout: LinearLayout =
            packagePriceView.findViewById(R.id.callpaygPricesHeaderLayout) as LinearLayout
        val viewBelowCallpaygContentLayout: View =
            packagePriceView.findViewById(R.id.viewBelowCallpaygContentLayout) as View
        val smspaygPricesHeaderLayout: ConstraintLayout =
            packagePriceView.findViewById(R.id.smspaygPricesHeaderLayout) as ConstraintLayout
        val viewBelowSmspaygContentLayout: View =
            packagePriceView.findViewById(R.id.viewBelowSmspaygContentLayout) as View
        val internetpaygPricesHeaderLayout: ConstraintLayout =
            packagePriceView.findViewById(R.id.internetpaygPricesHeaderLayout) as ConstraintLayout
        val internetpaygContentLayout: LinearLayout =
            packagePriceView.findViewById(R.id.internetpaygContentLayout) as LinearLayout
        val smspaygContentLayout: LinearLayout =
            packagePriceView.findViewById(R.id.smspaygContentLayout) as LinearLayout
        val callpaygContentLayout: LinearLayout =
            packagePriceView.findViewById(R.id.callpaygContentLayout) as LinearLayout
        val internetContentLayout: LinearLayout =
            packagePriceView.findViewById(R.id.internetContentLayout) as LinearLayout
        val smsContentLayout: LinearLayout =
            packagePriceView.findViewById(R.id.smsContentLayout) as LinearLayout
        val callsContentLayout: LinearLayout =
            packagePriceView.findViewById(R.id.callsContentLayout) as LinearLayout

        /**set the package price call section*/
        if (packagePrice.call != null) {
            isCallView = true
            setupCallSection(packagePriceView, packagePrice.call)
        } else {
            isCallView = false
            callPricesHeaderLayout.visibility = View.GONE
            callsContentLayout.visibility = View.GONE
        }

        /**set the package price sms section*/
        if (packagePrice.sms != null) {
            isSMSView = true
            setupSMSSection(packagePriceView, packagePrice.sms)
        } else {
            isSMSView = false
            smsPricesHeaderLayout.visibility = View.GONE
            smsContentLayout.visibility = View.GONE
        }

        /**set the package price internet section*/
        if (packagePrice.internet != null) {
            isInternetView = true
            setupInternetSection(packagePriceView, packagePrice.internet)
        } else {
            isInternetView = false
            internetPricesHeaderLayout.visibility = View.GONE
            internetContentLayout.visibility = View.GONE
        }

        /**set the package price callpayg section*/
        if (packagePrice.callPayg != null) {
            isCallPayGView = true
            setupCallPayGSection(packagePriceView, packagePrice.callPayg)//see here juna12891226
        } else {
            isCallPayGView = false
            callpaygPricesHeaderLayout.visibility = View.GONE
            callpaygContentLayout.visibility = View.GONE
        }

        /**set the package price smspayg section*/
        if (packagePrice.smsPayg != null) {
            isSMSPayGView = true
            setupSmspaygSection(packagePriceView, packagePrice.smsPayg)
        } else {
            isSMSPayGView = false
            smspaygPricesHeaderLayout.visibility = View.GONE
            smspaygContentLayout.visibility = View.GONE
        }

        /**set the package price internetPayg section*/
        if (packagePrice.internetPayg != null) {
            isInternetPayGView = true
            setupInternetPayGSection(packagePriceView, packagePrice.internetPayg)
        } else {
            isInternetPayGView = false
            internetpaygPricesHeaderLayout.visibility = View.GONE
            internetpaygContentLayout.visibility = View.GONE
        }

        if (isCallView) {
            viewBelowCallsContentLayout.visibility = View.VISIBLE
            if (!isSMSView) {
                viewBelowCallsContentLayout.visibility = View.GONE
                if (!isInternetView) {
                    viewBelowCallsContentLayout.visibility = View.GONE
                    if (!isCallPayGView) {
                        viewBelowCallsContentLayout.visibility = View.GONE
                        if (!isSMSPayGView) {
                            viewBelowCallsContentLayout.visibility = View.GONE
                            if (!isInternetPayGView) {
                                viewBelowCallsContentLayout.visibility = View.GONE
                            }
                        }
                    }
                }
            }
        }

        if (isSMSView) {
            viewBelowSmsContentLayout.visibility = View.VISIBLE
            if (!isInternetView) {
                viewBelowSmsContentLayout.visibility = View.GONE
                if (!isCallPayGView) {
                    viewBelowSmsContentLayout.visibility = View.GONE
                    if (!isSMSPayGView) {
                        viewBelowSmsContentLayout.visibility = View.GONE
                        if (!isInternetPayGView) {
                            viewBelowSmsContentLayout.visibility = View.GONE
                        }
                    }
                }
            }
        }

        if (isInternetView) {
            viewBelowInternetContentLayout.visibility = View.VISIBLE
            if (!isCallPayGView) {
                viewBelowInternetContentLayout.visibility = View.GONE
                if (!isSMSPayGView) {
                    viewBelowInternetContentLayout.visibility = View.GONE
                    if (!isInternetPayGView) {
                        viewBelowInternetContentLayout.visibility = View.GONE
                    }
                }
            }
        }

        if (isCallPayGView) {
            viewBelowCallpaygContentLayout.visibility = View.VISIBLE
            if (!isSMSPayGView) {
                viewBelowCallpaygContentLayout.visibility = View.GONE
                if (!isInternetPayGView) {
                    viewBelowCallpaygContentLayout.visibility = View.GONE
                }
            }
        }

        if (isSMSPayGView) {
            viewBelowSmspaygContentLayout.visibility = View.VISIBLE
            if (!isInternetPayGView) {
                viewBelowSmspaygContentLayout.visibility = View.GONE
            }
        }

    }//setupPackagePriceSection ends

    private fun setupSmspaygSection(packagePriceView: View, smsPayg: SmsPayg) {
        val smspaygPricesIcon: ImageView =
            packagePriceView.findViewById(R.id.smspaygPricesIcon) as ImageView
        val smspaygHeaderLabel: TextView =
            packagePriceView.findViewById(R.id.smspaygHeaderLabel) as TextView
        val smspaygHeaderValue: TextView =
            packagePriceView.findViewById(R.id.smspaygHeaderValue) as TextView
        val smspaygContentLayout: LinearLayout =
            packagePriceView.findViewById(R.id.smspaygContentLayout) as LinearLayout
        val viewBelowSmspaygContentLayout: View =
            packagePriceView.findViewById(R.id.viewBelowSmspaygContentLayout) as View
        val smspaygPricesHeaderLayout: ConstraintLayout =
            packagePriceView.findViewById(R.id.smspaygPricesHeaderLayout) as ConstraintLayout

        //callPayg title here
        if (smsPayg.title != null && hasValue(smsPayg.title)) {
            smspaygHeaderLabel.text = smsPayg.title
            smspaygHeaderLabel.isSelected = true
        }//if (smsPayg.title != null && hasValue(smsPayg.title)) ends

        //callPayg titleValueRight here
        if (smsPayg.titleValue != null && hasValue(smsPayg.titleValue)) {
            smspaygHeaderValue.text = smsPayg.titleValue
            smspaygHeaderValue.isSelected = true
        }//if (smsPayg.titleValue != null && hasValue(smsPayg.titleValue)) ends

        //internet icon here
        if (smsPayg.iconName != null && hasValue(smsPayg.iconName)) {
            smspaygPricesIcon.setImageResource(getCardAdapterMappedImage(smsPayg.iconName))
        }//if (smsPayg.iconName != null && hasValue(smsPayg.iconName)) ends

        if (!hasValue(smsPayg.title) && !hasValue(smsPayg.titleValue)) {
            smspaygPricesHeaderLayout.visibility = View.GONE
            isSMSPayGView = false
            smspaygContentLayout.visibility = View.GONE
            return
        }

        /**set the sms attributes*/
        if (smsPayg.attributes != null && smsPayg.attributes.isNotEmpty()) {
            setSmsPayGAttributes(smsPayg.attributes, packagePriceView)
        } else {
            smspaygPricesHeaderLayout.visibility = View.GONE
            isSMSPayGView = false
            smspaygContentLayout.visibility = View.GONE
        } //if (smsPayg.attributes != null && !smsPayg.attributes.isEmpty()) ends
    }//setupSmspaygSection ends

    private fun setupInternetPayGSection(packagePriceView: View, internetPayg: InternetPayg) {
        val internetpaygPricesIcon: ImageView =
            packagePriceView.findViewById(R.id.internetpaygPricesIcon) as ImageView
        val internetpaygHeaderLabel: TextView =
            packagePriceView.findViewById(R.id.internetpaygHeaderLabel) as TextView
        val internetpaygHeaderValue: TextView =
            packagePriceView.findViewById(R.id.internetpaygHeaderValue) as TextView
        val internetpaygContentLayout: LinearLayout =
            packagePriceView.findViewById(R.id.internetpaygContentLayout) as LinearLayout
        val internetpaygPricesHeaderLayout: ConstraintLayout =
            packagePriceView.findViewById(R.id.internetpaygPricesHeaderLayout) as ConstraintLayout

        //internet title here
        if (internetPayg.title != null && hasValue(internetPayg.title)) {
            internetpaygHeaderLabel.text = internetPayg.title
            internetpaygHeaderLabel.isSelected = true
        }//if (internetPayg.title != null && hasValue(internetPayg.title))  ends

        //internet titleValue here
        if (internetPayg.titleValue != null && hasValue(internetPayg.titleValue)) {
            internetpaygHeaderValue.text = internetPayg.titleValue
            internetpaygHeaderValue.isSelected = true
        }//if (internetPayg.titleValue != null && hasValue(internetPayg.titleValue))  ends

        //internet icon here
        if (internetPayg.iconName != null && hasValue(internetPayg.iconName)) {
            internetpaygPricesIcon.setImageResource(getCardAdapterMappedImage(internetPayg.iconName))
        }//if (internetPayg.iconName != null && hasValue(internetPayg.iconName)) ends

        if (!hasValue(internetPayg.title) && !hasValue(internetPayg.titleValue)) {
            internetpaygPricesHeaderLayout.visibility = View.GONE
            internetpaygContentLayout.visibility = View.GONE
            return
        }

        /**set the internetPayg attributes*/
        if (!internetPayg.attributes.isNullOrEmpty()) {
            setInternetPayGAttributes(packagePriceView, internetPayg.attributes)
        } else {
            internetpaygPricesHeaderLayout.visibility = View.GONE
            internetpaygContentLayout.visibility = View.GONE
        }
    }//setupInternetPayGSection ends

    private fun setInternetPayGAttributes(
        packagePriceView: View,
        attributes: List<AttributesItem?>
    ) {
        val internetpaygContentLayout: LinearLayout =
            packagePriceView.findViewById(R.id.internetpaygContentLayout) as LinearLayout
        //iterate over it
        internetpaygContentLayout.removeAllViews()
        for (its in attributes.indices) {
            val tariffsInternetPayGViewItem = TariffsInternetPayGViewItem(context!!)

            if (attributes[its]!!.title != null && hasValue(attributes[its]!!.title)) {
                logE(
                    "adapteras1226",
                    "label1::::".plus(attributes[its]!!.title),
                    fromClass,
                    "setInternetPayGAttributes"
                )
                tariffsInternetPayGViewItem.getInternetViewItemLabel().text =
                    attributes[its]!!.title!!.trim()
                tariffsInternetPayGViewItem.getInternetViewItemLabel().isSelected = true
            }

            if (attributes[its]!!.value != null && hasValue(attributes[its]!!.value)) {
                val value = attributes[its]!!.value!!.trim()
                tariffsInternetPayGViewItem.getInternetViewItemValue().text =
                    getManatConcatedString(
                        context!!, value, ConstantsUtility.TariffConstants.AZERI_SYMBOL, 0.25f,
                        R.dimen._10ssp
                    )
                tariffsInternetPayGViewItem.getInternetViewItemValue().isSelected = true
            }

            if (!hasValue(attributes[its]!!.title) && !hasValue(attributes[its]!!.value)) {
                tariffsInternetPayGViewItem.getInternetViewItemLayout().visibility = View.GONE
            }

            internetpaygContentLayout.addView(tariffsInternetPayGViewItem)
        }
    }//setInternetPayGAttributes ends

    private fun setSmsPayGAttributes(attributes: List<AttributesItem?>, packagePriceView: View) {
        val smspaygContentLayout: LinearLayout =
            packagePriceView.findViewById(R.id.smspaygContentLayout) as LinearLayout
        //iterate over the list
        //remove all view from the content layout
        smspaygContentLayout.removeAllViews()
        for (its in attributes.indices) {
            val tariffsSMSPayGViewItem = TariffsSMSPayGViewItem(context!!)

            if (attributes[its]!!.title != null && hasValue(attributes[its]!!.title)) {
                tariffsSMSPayGViewItem.getSmspaygViewItemLabel().text = attributes[its]!!.title
                tariffsSMSPayGViewItem.getSmspaygViewItemLabel().isSelected = true
            }//if(attributes[its]!!.title!=null&& hasValue(attributes[its]!!.title)) ends

            if (attributes[its]!!.valueRight != null && hasValue(attributes[its]!!.valueRight)) {
                val value = attributes[its]!!.valueRight
                tariffsSMSPayGViewItem.getSmspaygViewItemValue().text = getManatConcatedString(
                    context!!,
                    value!!,
                    ConstantsUtility.TariffConstants.AZERI_SYMBOL,
                    0.25f,
                    R.dimen._10ssp
                )
                tariffsSMSPayGViewItem.getSmspaygViewItemValue().isSelected = true
            }

            if (!hasValue(attributes[its]!!.title) && !hasValue(attributes[its]!!.valueRight)) {
                tariffsSMSPayGViewItem.getSmspaygViewItemLayout().visibility = View.GONE
            }

            smspaygContentLayout.addView(tariffsSMSPayGViewItem)
        }//for ends
    }//setSmsPayGAttributes ends

    private fun setupCallPayGSection(packagePriceView: View, callPayg: CallPayg) {
        val callpaygPricesIcon: ImageView =
            packagePriceView.findViewById(R.id.callpaygPricesIcon) as ImageView
        val callpaygPricesLabel: TextView =
            packagePriceView.findViewById(R.id.callpaygPricesLabel) as TextView
        val callpaygPricesValueLeft: TextView =
            packagePriceView.findViewById(R.id.callpaygPricesValueLeft) as TextView
        val callpaygPricesValueRight: TextView =
            packagePriceView.findViewById(R.id.callpaygPricesValueRight) as TextView
        val callpaygContentLayout: LinearLayout =
            packagePriceView.findViewById(R.id.callpaygContentLayout) as LinearLayout
        val viewBelowCallpaygContentLayout: View =
            packagePriceView.findViewById(R.id.viewBelowCallpaygContentLayout) as View
        val callpaygPricesHeaderLayout: LinearLayout =
            packagePriceView.findViewById(R.id.callpaygPricesHeaderLayout) as LinearLayout
        val callPayGVerticalViewBar: View =
            packagePriceView.findViewById(R.id.callPayGVerticalViewBar) as View

        //callPayg title here
        if (callPayg.title != null && hasValue(callPayg.title)) {
            callpaygPricesLabel.text = callPayg.title
            callpaygPricesLabel.isSelected = true
        }//if (callPayg.title != null && hasValue(callPayg.title))  ends

        //callPayg titleValueRight here
        if (callPayg.titleValueRight != null && hasValue(callPayg.titleValueRight)) {
            callpaygPricesValueRight.text = callPayg.titleValueRight
            callpaygPricesValueRight.isSelected = true
        }//if (callPayg.titleValueRight != null && hasValue(callPayg.titleValueRight))  ends

        var priceTemplate = ""
        if (callPayg.priceTemplate != null &&
            hasValue(callPayg.priceTemplate) &&
            callPayg.priceTemplate.equals(
                ConstantsUtility.TariffConstants.PRICE_TEMPELATE_DOUBLE_TITLE,
                ignoreCase = true
            )
        ) {

            priceTemplate = callPayg.priceTemplate
            //call titleValueLeft here
            if (callPayg.titleValueLeft != null && hasValue(callPayg.titleValueLeft)) {
                callpaygPricesValueLeft.text = callPayg.titleValueLeft
                callpaygPricesValueLeft.isSelected = true
            } else {
                callpaygPricesValueLeft.visibility = View.INVISIBLE
                callPayGVerticalViewBar.visibility = View.INVISIBLE
            } //if (call.titleValueLeft != null && hasValue(call.titleValueLeft)) ends
        } else {
            callpaygPricesValueLeft.visibility = View.INVISIBLE
            callPayGVerticalViewBar.visibility = View.INVISIBLE
        }

        //internet icon here
        if (callPayg.iconName != null && hasValue(callPayg.iconName)) {
            callpaygPricesIcon.setImageResource(getCardAdapterMappedImage(callPayg.iconName))
        }//if (callPayg.iconName != null && hasValue(callPayg.iconName)) ends

        if (!hasValue(callPayg.title) && !hasValue(callPayg.titleValueRight)) {
            callpaygPricesHeaderLayout.visibility = View.GONE
            isCallPayGView = false
            callpaygContentLayout.visibility = View.GONE
            return
        }

        /**set the callpayg attributes*/
        if (callPayg.attributes != null && callPayg.attributes.isNotEmpty()) {
            setCallPayGAttributes(callPayg.attributes, packagePriceView, priceTemplate)
            logE("callPagX1", "notempty nor null", fromClass, "setCallPayGAttributesCheck")
        } else {
            logE("callPagX1", "empty or null", fromClass, "setCallPayGAttributesCheck")
            callpaygPricesHeaderLayout.visibility = View.GONE
            isCallPayGView = false
            callpaygContentLayout.visibility = View.GONE
        } //if (callPayg.attributes != null && !callPayg.attributes.isEmpty()) ends
    }//setupCallPayGSection ends

    private fun setCallPayGAttributes(
        attributes: List<AttributesItem?>,
        packagePriceView: View,
        priceTemplate: String
    ) {
        val callpaygContentLayout: LinearLayout =
            packagePriceView.findViewById(R.id.callpaygContentLayout) as LinearLayout
        //iterate over the list
        //remove all view from the content layout
        callpaygContentLayout.removeAllViews()
        for (its in attributes.indices) {
            val tariffsCallPayGViewItem = TariffsCallPayGViewItem(context!!)

            if (attributes[its]!!.title != null && hasValue(attributes[its]!!.title)) {
                tariffsCallPayGViewItem.getCallpaygViewItemLabel().text = attributes[its]!!.title
                tariffsCallPayGViewItem.getCallpaygViewItemLabel().isSelected = true
            }//if(attributes[its]!!.title!=null&& hasValue(attributes[its]!!.title)) ends

            if (attributes[its]!!.valueRight != null && hasValue(attributes[its]!!.valueRight)) {
                val valueRight = attributes[its]!!.valueRight
                if (isNumeric(valueRight)) {
                    tariffsCallPayGViewItem.getCallpaygViewItemValueRight().text =
                        getManatConcatedString(
                            context!!,
                            valueRight!!,
                            ConstantsUtility.TariffConstants.AZERI_SYMBOL,
                            0.25f,
                            R.dimen._10ssp
                        )
                    tariffsCallPayGViewItem.getCallpaygViewItemValueRight().isSelected = true
                } else {
                    tariffsCallPayGViewItem.getCallpaygViewItemValueRight().text = valueRight
                    tariffsCallPayGViewItem.getCallpaygViewItemValueRight().isSelected = true
                }
            }

            if (priceTemplate.equals(
                    ConstantsUtility.TariffConstants.PRICE_TEMPELATE_DOUBLE_TITLE,
                    ignoreCase = true
                ) ||
                priceTemplate.equals(
                    ConstantsUtility.TariffConstants.PRICE_TEMPELATE_DOUBLE_VALUE,
                    ignoreCase = true
                )
            ) {
                if (attributes[its]!!.valueLeft != null && hasValue(attributes[its]!!.valueLeft)) {
                    if (isNumeric(attributes[its]!!.valueLeft)) {
                        val value = attributes[its]!!.valueLeft
                        tariffsCallPayGViewItem.getCallpaygViewItemValueLeft().text =
                            getManatConcatedString(
                                context!!,
                                value!!,
                                ConstantsUtility.TariffConstants.AZERI_SYMBOL,
                                0.25f,
                                R.dimen._10ssp
                            )
                        tariffsCallPayGViewItem.getCallpaygViewItemValueLeft().isSelected = true
                    } else {
                        tariffsCallPayGViewItem.getCallpaygViewItemValueLeft().text =
                            attributes[its]!!.valueLeft
                        tariffsCallPayGViewItem.getCallpaygViewItemValueLeft().isSelected = true
                    }
                } else {
                    tariffsCallPayGViewItem.getColValueLeft().visibility = View.INVISIBLE
                }
            } else {
                tariffsCallPayGViewItem.getColValueLeft().visibility = View.INVISIBLE
            }

            if (!hasValue(attributes[its]!!.title) && !hasValue(attributes[its]!!.valueRight) && !hasValue(
                    attributes[its]!!.valueLeft
                )
            ) {
                tariffsCallPayGViewItem.getCallpaygViewItemLayout().visibility = View.GONE
            }

            callpaygContentLayout.addView(tariffsCallPayGViewItem)
        }//for ends
    }//setCallPayGAttributes ends

    private fun setupInternetSection(packagePriceView: View, internet: Internet) {
        val internetPricesIcon: ImageView =
            packagePriceView.findViewById(R.id.internetPricesIcon) as ImageView
        val internetHeaderLabel: TextView =
            packagePriceView.findViewById(R.id.internetHeaderLabel) as TextView
        val internetHeaderValue: TextView =
            packagePriceView.findViewById(R.id.internetHeaderValue) as TextView
        val internetContentLayout: LinearLayout =
            packagePriceView.findViewById(R.id.internetContentLayout) as LinearLayout
        val viewBelowInternetContentLayout: View =
            packagePriceView.findViewById(R.id.viewBelowInternetContentLayout) as View
        val internetPricesHeaderLayout: ConstraintLayout =
            packagePriceView.findViewById(R.id.internetPricesHeaderLayout) as ConstraintLayout

        if (internet.title != null && hasValue(internet.title)) {
            internetHeaderLabel.text = internet.title
            internetHeaderLabel.isSelected = true
        }

        if (internet.titleValue != null && hasValue(internet.titleValue)) {
            internetHeaderValue.text = internet.titleValue
            internetHeaderValue.isSelected = true
        }//if (internet.titleValue != null && hasValue(internet.titleValue))  ends

        if (internet.iconName != null && hasValue(internet.iconName)) {
            internetPricesIcon.setImageResource(getCardAdapterMappedImage(internet.iconName))
        }//if (internet.iconName != null && hasValue(internet.iconName)) ends

        if (!hasValue(internet.title) && !hasValue(internet.titleValue)) {
            internetPricesHeaderLayout.visibility = View.GONE
            isInternetView = false
            internetContentLayout.visibility = View.GONE
            return
        }

        /**set the internet attributes*/
        if (!internet.attributes.isNullOrEmpty()) {
            setupInternetAttributes(packagePriceView, internet.attributes)
        } else {
            internetPricesHeaderLayout.visibility = View.GONE
            isInternetView = false
            internetContentLayout.visibility = View.GONE
        }
    }//setupInternetSection ends

    private fun setupInternetAttributes(packagePriceView: View, attributes: List<AttributesItem?>) {
        val internetContentLayout: LinearLayout =
            packagePriceView.findViewById(R.id.internetContentLayout) as LinearLayout
        //iterate over it
        internetContentLayout.removeAllViews()
        for (its in attributes.indices) {
            val tariffsInternetViewItem = TariffsInternetViewItem(context!!)

            if (attributes[its]!!.title != null && hasValue(attributes[its]!!.title)) {
                logE(
                    "adapteras1289",
                    "label1::::".plus(attributes[its]!!.title),
                    fromClass,
                    "setupInternetAttributes"
                )
                tariffsInternetViewItem.getInternetViewItemLabel().text =
                    attributes[its]!!.title!!.trim()
                tariffsInternetViewItem.getInternetViewItemLabel().isSelected = true
            }

            if (attributes[its]!!.value != null && hasValue(attributes[its]!!.value)) {
                val value = attributes[its]!!.value!!.trim()
                tariffsInternetViewItem.getInternetViewItemValue().text = getManatConcatedString(
                    context!!, value, ConstantsUtility.TariffConstants.AZERI_SYMBOL, 0.25f,
                    R.dimen._10ssp
                )
                tariffsInternetViewItem.getInternetViewItemValue().isSelected = true
            }

            if (!hasValue(attributes[its]!!.title) && !hasValue(attributes[its]!!.value)) {
                tariffsInternetViewItem.getInternetViewItemLayout().visibility = View.GONE
            }

            internetContentLayout.addView(tariffsInternetViewItem)
        }
    }//setupInternetAttributes ends

    private fun setupSMSSection(packagePriceView: View, sms: Sms) {
        val smsPricesIcon: ImageView =
            packagePriceView.findViewById(R.id.smsPricesIcon) as ImageView
        val smsHeaderLabel: TextView =
            packagePriceView.findViewById(R.id.smsHeaderLabel) as TextView
        val smsHeaderValue: TextView =
            packagePriceView.findViewById(R.id.smsHeaderValue) as TextView
        val smsPricesHeaderLayout: ConstraintLayout =
            packagePriceView.findViewById(R.id.smsPricesHeaderLayout) as ConstraintLayout
        val smsContentLayout: LinearLayout =
            packagePriceView.findViewById(R.id.smsContentLayout) as LinearLayout
        val viewBelowSmsContentLayout: View =
            packagePriceView.findViewById(R.id.viewBelowSmsContentLayout) as View

        if (sms.title != null && hasValue(sms.title)) {
            smsHeaderLabel.text = sms.title
            smsHeaderLabel.isSelected = true
        }

        if (sms.titleValue != null && hasValue(sms.titleValue)) {
            smsHeaderValue.text = sms.titleValue
            smsHeaderValue.isSelected = true
        }

        if (sms.iconName != null && hasValue(sms.iconName)) {
            smsPricesIcon.setImageResource(getCardAdapterMappedImage(sms.iconName))
        }

        if (!hasValue(sms.title) && !hasValue(sms.titleValue)) {
            smsPricesHeaderLayout.visibility = View.GONE
            isSMSView = false
            smsContentLayout.visibility = View.GONE
            return
        }

        /**set the sms attributes*/
        if (sms.attributes != null && sms.attributes.isNotEmpty()) {
            setSmsAttributes(sms.attributes, packagePriceView)
        } else {
            smsPricesHeaderLayout.visibility = View.GONE
            isSMSView = false
            smsContentLayout.visibility = View.GONE
        }
    }//setupCallSection ends

    private fun setSmsAttributes(attributes: List<AttributesItem?>, packagePriceView: View) {
        val smsContentLayout: LinearLayout =
            packagePriceView.findViewById(R.id.smsContentLayout) as LinearLayout
        //iterate over it
        smsContentLayout.removeAllViews()
        for (its in attributes.indices) {
            val tariffsSMSViewItem = TariffsSMSViewItem(context!!)

            if (attributes[its]!!.title != null && hasValue(attributes[its]!!.title)) {
                logE(
                    "adapteras1289",
                    "label1::::".plus(attributes[its]!!.title),
                    fromClass,
                    "setSmsAttributes"
                )
                tariffsSMSViewItem.getSMSViewItemLabel().text = attributes[its]!!.title!!.trim()
                tariffsSMSViewItem.getSMSViewItemLabel().isSelected = true
            }

            if (attributes[its]!!.value != null && hasValue(attributes[its]!!.value)) {
                val value = attributes[its]!!.value!!.trim()
                tariffsSMSViewItem.getSMSViewItemValue().text = getManatConcatedString(
                    context!!, value, ConstantsUtility.TariffConstants.AZERI_SYMBOL, 0.25f,
                    R.dimen._10ssp
                )
                tariffsSMSViewItem.getSMSViewItemValue().isSelected = true
            }

            if (!hasValue(attributes[its]!!.title) && !hasValue(attributes[its]!!.value)) {
                tariffsSMSViewItem.getsmsViewItemLayout().visibility = View.GONE
            }

            smsContentLayout.addView(tariffsSMSViewItem)
        }//for ends
    }

    private fun setupCallSection(packagePriceView: View, call: Call) {
        val callPricesIcon: ImageView =
            packagePriceView.findViewById(R.id.callPricesIcon) as ImageView
        val callHeaderLabel: TextView =
            packagePriceView.findViewById(R.id.callHeaderLabel) as TextView
        val callHeaderValueRight: TextView =
            packagePriceView.findViewById(R.id.callHeaderValueRight) as TextView
        val callHeaderValueLeft: TextView =
            packagePriceView.findViewById(R.id.callHeaderValueLeft) as TextView
        val callsContentLayout: LinearLayout =
            packagePriceView.findViewById(R.id.callsContentLayout) as LinearLayout
        val viewBelowCallsContentLayout: View =
            packagePriceView.findViewById(R.id.viewBelowCallsContentLayout) as View
        val callPricesHeaderLayout: LinearLayout =
            packagePriceView.findViewById(R.id.callPricesHeaderLayout) as LinearLayout
        val callVerticalViewBar: View =
            packagePriceView.findViewById(R.id.callVerticalViewBar) as View

        //call title here
        if (call.title != null && hasValue(call.title)) {
            callHeaderLabel.text = call.title
            callHeaderLabel.isSelected = true
        }//if (call.title != null && hasValue(call.title)) ends

        //call titleValueRight here
        if (call.titleValueRight != null && hasValue(call.titleValueRight)) {
            callHeaderValueRight.text = call.titleValueRight
            callHeaderValueRight.isSelected = true
        }//if (call.titleValueRight != null && hasValue(call.titleValueRight)) ends

        var priceTemplate = ""
        if (call.priceTemplate != null &&
            hasValue(call.priceTemplate) &&
            call.priceTemplate.equals(
                ConstantsUtility.TariffConstants.PRICE_TEMPELATE_DOUBLE_TITLE,
                ignoreCase = true
            )
        ) {
            priceTemplate = call.priceTemplate
            //call titleValueLeft here
            if (call.titleValueLeft != null && hasValue(call.titleValueLeft)) {
                callHeaderValueLeft.text = call.titleValueLeft
                callHeaderValueLeft.isSelected = true
            } else {
                callHeaderValueLeft.visibility = View.INVISIBLE
                callVerticalViewBar.visibility = View.INVISIBLE
            } //if (call.titleValueLeft != null && hasValue(call.titleValueLeft)) ends
        } else {
            callHeaderValueLeft.visibility = View.INVISIBLE
            callVerticalViewBar.visibility = View.INVISIBLE
        }

        //call imageicon here
        if (call.iconName != null && hasValue(call.iconName)) {
            callPricesIcon.setImageResource(getCardAdapterMappedImage(call.iconName))
        }//if (call.iconName != null && hasValue(call.iconName)) ends

        if (!hasValue(call.title) &&
            !hasValue(call.titleValueRight) &&
            !hasValue(call.titleValueLeft)
        ) {
            callPricesHeaderLayout.visibility = View.GONE
            isCallView = false
            callsContentLayout.visibility = View.GONE
            return
        }

        /**set the call attributes*/
        if (call.attributes != null && call.attributes.isNotEmpty()) {
            setCallAttributes(call.attributes, packagePriceView, priceTemplate)
        } else {
            callPricesHeaderLayout.visibility = View.GONE
            isCallView = false
            callsContentLayout.visibility = View.GONE
        } //if (call.attributes != null && !call.attributes.isEmpty())
    }//setupCallSection ends

    private fun setCallAttributes(
        attributes: List<AttributesItem?>,
        packagePriceView: View,
        priceTemplate: String
    ) {
        val callsContentLayout: LinearLayout =
            packagePriceView.findViewById(R.id.callsContentLayout) as LinearLayout
        //iterate over it
        callsContentLayout.removeAllViews()
        for (its in attributes.indices) {
            val tariffCallViewItem = TariffsCallViewItem(context!!)

            if (attributes[its]!!.title != null && hasValue(attributes[its]!!.title)) {
                tariffCallViewItem.getCallViewItemLabel().text = attributes[its]!!.title!!.trim()
                tariffCallViewItem.getCallViewItemLabel().isSelected = true
            }

            if (priceTemplate.equals(
                    ConstantsUtility.TariffConstants.PRICE_TEMPELATE_DOUBLE_TITLE,
                    ignoreCase = true
                ) ||
                priceTemplate.equals(
                    ConstantsUtility.TariffConstants.PRICE_TEMPELATE_DOUBLE_VALUE,
                    ignoreCase = true
                )
            ) {
                if (attributes[its]!!.valueLeft != null && hasValue(attributes[its]!!.valueLeft)) {
                    val value = attributes[its]!!.valueLeft!!.trim()
                    if (isNumeric(value)) {
                        tariffCallViewItem.getCallViewItemValueLeft().text = getManatConcatedString(
                            context!!,
                            value!!,
                            ConstantsUtility.TariffConstants.AZERI_SYMBOL,
                            0.25f,
                            R.dimen._10ssp
                        )
                        tariffCallViewItem.getCallViewItemValueLeft().isSelected = true
                    } else {
                        tariffCallViewItem.getCallViewItemValueLeft().text = value
                        tariffCallViewItem.getCallViewItemValueLeft().isSelected = true
                    }
                } else {
                    tariffCallViewItem.getColValueLeft().visibility = View.INVISIBLE
                }
            } else {
                tariffCallViewItem.getColValueLeft().visibility = View.INVISIBLE
            }

            if (attributes[its]!!.valueRight != null && hasValue(attributes[its]!!.valueRight)) {
                val value = attributes[its]!!.valueRight!!.trim()
                if (isNumeric(value)) {
                    tariffCallViewItem.getCallViewItemValueRight().text = getManatConcatedString(
                        context!!, value!!, ConstantsUtility.TariffConstants.AZERI_SYMBOL, 0.25f,
                        R.dimen._10ssp
                    )
                    tariffCallViewItem.getCallViewItemValueRight().isSelected = true
                } else {
                    tariffCallViewItem.getCallViewItemValueRight().text = value
                    tariffCallViewItem.getCallViewItemValueRight().isSelected = true
                }
            }

            if (!hasValue(attributes[its]!!.title) &&
                !hasValue(attributes[its]!!.valueRight) &&
                !hasValue(attributes[its]!!.valueLeft)
            ) {
                tariffCallViewItem.getCallViewItemLayout().visibility = View.GONE
            }

            callsContentLayout.addView(tariffCallViewItem)
        }//for ends
    }//setCallAttributes ends

    private fun setTariffAttributes(
        attributes: List<AttributesItem?>,
        viewHolder: TariffsCardAdapterViewHolder
    ) {
        //iterate over it
        //remove all existing menu views
        viewHolder.bonusesContentLayout!!.removeAllViews()
        for (its in attributes.indices) {
            val bonusTariffViewItem = TariffsBonusViewItem(context!!)

            /**hide the bottom view below the
             * very last item of the attributes*/
            if (its == attributes.size - 1) {
                bonusTariffViewItem.getBonusBottomView().visibility = View.GONE
            } else {
                bonusTariffViewItem.getBonusBottomView().visibility = View.VISIBLE
            }

            //imageicon here
            if (attributes[its]!!.iconName != null && hasValue(attributes[its]!!.iconName)) {
                bonusTariffViewItem.getBonusViewItemIcon()
                    .setImageResource(getCardAdapterMappedImage(attributes[its]!!.iconName!!))
            }

            //set the left label
            if (hasValue(attributes[its]!!.title)) {
                bonusTariffViewItem.getBonusViewItemLabel().text = attributes[its]!!.title
                bonusTariffViewItem.getBonusViewItemLabel().isSelected = true
            }

            //set the right label with metrics
            var value = ""
            if (hasValue(attributes[its]!!.value)) {
                value = attributes[its]!!.value!!
            }

            var metrics = ""
            if (hasValue(attributes[its]!!.metrics)) {
                metrics = attributes[its]!!.metrics!!
            }

            var pricing = ""
            if (isRedValue(metrics)) {
                /**metrics are free
                 * change the text color
                 * don't append the value
                 * just show the metrics instead*/
                pricing = metrics
                bonusTariffViewItem.getBonusViewItemValue().text = pricing
                bonusTariffViewItem.getBonusViewItemValue()
                    .setTextColor(context!!.resources.getColor(R.color.colorTariffsRedValue))
                bonusTariffViewItem.getBonusViewItemValue().isSelected = true
            } else {
                pricing = value.plus(" ").plus(metrics)
                bonusTariffViewItem.getBonusViewItemValue().text = pricing
                bonusTariffViewItem.getBonusViewItemValue().isSelected = true
            }

            //add the view
            viewHolder.bonusesContentLayout!!.addView(bonusTariffViewItem)
        }//setTariffAttributes for ends
    }//setTariffAttributes ends

    private fun setTariffHeader(header: Header, viewHolder: TariffsCardAdapterViewHolder) {
        //set the tariff name here
        if (hasValue(header.name)) {
            viewHolder.tariffNameLabel!!.text = header.name
            viewHolder.tariffNameLabel!!.isSelected = true
        }// if (hasValue(header.name)) ends

        //set the priceLabel & priceValue here
        var pricelabel = ""
        if (hasValue(header.priceLabel)) {
            pricelabel = header.priceLabel!!
        }// if (hasValue(header.priceLabel)) ends

        var priceValue = ""
        if (hasValue(header.priceValue)) {
            priceValue = header.priceValue!!
        }// if (hasValue(header.priceValue)) ends

        //set the pricing
        var pricing = ""

        if (hasValue(pricelabel) && hasValue(priceValue)) {
            pricing = pricelabel.plus(": ").plus(priceValue)
        } else if (hasValue(pricelabel) && !hasValue(priceValue)) {
            pricing = pricelabel
        } else if (!hasValue(pricelabel) && hasValue(priceValue)) {
            pricing = priceValue
        }

        if (hasValue(priceValue)) {
            viewHolder.tariffPriceLabel!!.text = getManatConcatedString(
                context!!, pricing,
                ConstantsUtility.TariffConstants.AZERI_SYMBOL, 0.25f, R.dimen._10ssp
            )
            viewHolder.tariffPriceLabel!!.isSelected = true
        } else {
            viewHolder.tariffPriceLabel!!.text = pricing
            viewHolder.tariffPriceLabel!!.isSelected = true
        }

        //set the tariffDescription here
        if (hasValue(header.tariffDescription)) {
            viewHolder.tariffDescriptionLabel!!.text = header.tariffDescription
        }// if (hasValue(header.tariffDescription)) ends
    }//setTariffHeader ends

    private fun initUIEvents(viewHolder: TariffsCardAdapterViewHolder, position: Int) {
        viewHolder.bonusHeaderLayout!!.setOnClickListener {
            if (viewHolder.bonusesExpandableLayout!!.isExpanded) {
                //do nothing
            } else {
                expandBonusLayout(viewHolder)
                isBonusLayoutExpanded = true
                collapseDetailsLayout(viewHolder)
                isDetailsLayoutExpanded = false
                collapsePackagesPricesLayout(viewHolder)
                isPackagePricesLayoutExpanded = false
            }
            Handler().postDelayed({ notifyDataSetChanged() }, expansionHandlerTime)
        }//viewHolder.bonusHeaderLayout!!.setOnClickListener ends

        viewHolder.packagesHeaderLayout!!.setOnClickListener {
            if (viewHolder.packagesExpandableLayout!!.isExpanded) {
                if (tariffCardItems!![position]!!.header != null) {
                    if (tariffCardItems!![position]!!.header!!.attributes != null && !tariffCardItems!![position]!!.header!!.attributes!!.isEmpty()) {
                        expandBonusLayout(viewHolder)
                        isBonusLayoutExpanded = true
                        collapseDetailsLayout(viewHolder)
                        isDetailsLayoutExpanded = false
                        collapsePackagesPricesLayout(viewHolder)
                        isPackagePricesLayoutExpanded = false
                    } else {
                        collapseBonusLayout(viewHolder)
                        isBonusLayoutExpanded = false
                        expandDetailsLayout(viewHolder)
                        isDetailsLayoutExpanded = true
                        collapsePackagesPricesLayout(viewHolder)
                        isPackagePricesLayoutExpanded = false
                    }
                }
            } else {
                collapseBonusLayout(viewHolder)
                isBonusLayoutExpanded = false
                collapseDetailsLayout(viewHolder)
                isDetailsLayoutExpanded = false
                expandPackagesPricesLayout(viewHolder)
                isPackagePricesLayoutExpanded = true
            }
            Handler().postDelayed({ notifyDataSetChanged() }, expansionHandlerTime)
        }//viewHolder.packagesHeaderLayout!!.setOnClickListener ends

        viewHolder.detailsHeaderLayout!!.setOnClickListener {
            if (viewHolder.detailsExpandableLayout!!.isExpanded) {
                if (tariffCardItems!![position]!!.header != null) {
                    if (tariffCardItems!![position]!!.header!!.attributes != null && !tariffCardItems!![position]!!.header!!.attributes!!.isEmpty()) {
                        expandBonusLayout(viewHolder)
                        isBonusLayoutExpanded = true
                        collapseDetailsLayout(viewHolder)
                        isDetailsLayoutExpanded = false
                        collapsePackagesPricesLayout(viewHolder)
                        isPackagePricesLayoutExpanded = false
                    } else {
                        collapseBonusLayout(viewHolder)
                        isBonusLayoutExpanded = false
                        collapseDetailsLayout(viewHolder)
                        isDetailsLayoutExpanded = false
                        expandPackagesPricesLayout(viewHolder)
                        isPackagePricesLayoutExpanded = true
                    }
                }
            } else {
                collapseBonusLayout(viewHolder)
                isBonusLayoutExpanded = false
                expandDetailsLayout(viewHolder)
                isDetailsLayoutExpanded = true
                collapsePackagesPricesLayout(viewHolder)
                isPackagePricesLayoutExpanded = false
            }
            Handler().postDelayed({ notifyDataSetChanged() }, expansionHandlerTime)
        }//viewHolder.detailsHeaderLayout!!.setOnClickListener ends

        viewHolder.subscribeButton!!.setOnClickListener {
            var tariffName = ""
            var offeringId = ""

            if (tariffCardItems != null && !tariffCardItems.isNullOrEmpty()) {
                if (tariffCardItems!![position]!!.header != null && hasValue(tariffCardItems!![position]!!.header!!.offeringId)) {
                    offeringId = tariffCardItems!![position]!!.header!!.offeringId!!
                }

                if (tariffCardItems!![position]!!.header != null && hasValue(tariffCardItems!![position]!!.header!!.name)) {
                    tariffName = tariffCardItems!![position]!!.header!!.name!!
                }
            }//if (tariffCardItems != null && !tariffCardItems.isNullOrEmpty()) ends

            RootValues.getTariffsSubscriptionsEvents().onTariffToBeSubscribe(
                getTariffMigrationMessageWithPricing(offeringId),
                tariffName,
                offeringId,
                "1"
            )
        }//viewHolder.subscribeButton!!.setOnClickListener ends

        viewHolder.renewButton!!.setOnClickListener {
            var tariffName = ""
            var offeringId = ""
            var actionType = ""

            if (tariffCardItems != null &&
                tariffCardItems!![position] != null &&
                tariffCardItems!![position]!!.subscribable != null &&
                hasValue(tariffCardItems!![position]!!.subscribable)
            ) {
                actionType = tariffCardItems!![position]!!.subscribable!!
            }

            if (tariffCardItems != null && !tariffCardItems.isNullOrEmpty()) {
                if (tariffCardItems!![position]!!.header != null && hasValue(tariffCardItems!![position]!!.header!!.offeringId)) {
                    offeringId = tariffCardItems!![position]!!.header!!.offeringId!!
                }

                if (tariffCardItems!![position]!!.header != null && hasValue(tariffCardItems!![position]!!.header!!.name)) {
                    tariffName = tariffCardItems!![position]!!.header!!.name!!
                }
            }//if (tariffCardItems != null && !tariffCardItems.isNullOrEmpty()) ends

            RootValues.getTariffsSubscriptionsEvents().onTariffToBeSubscribe(
                getTariffMigrationMessageWithPricing(offeringId),
                tariffName,
                offeringId,
                actionType
            )
        }//viewHolder.renewButton.setOnClickListener ends
    }//initUIEvents ends

    private fun expandBonusLayout(viewHolder: TariffsCardAdapterViewHolder) {
        viewHolder.bonusesExpandableLayout!!.expand()
        viewHolder.bonusLayoutExpandIcon!!.setImageResource(R.drawable.faqminus)
        viewHolder.bonusLayoutTitleLabel!!.setTextColor(context!!.resources.getColor(R.color.colorPrimary))
    }//expandBonusLayout ends

    private fun collapseBonusLayout(viewHolder: TariffsCardAdapterViewHolder) {
        viewHolder.bonusesExpandableLayout!!.collapse()
        viewHolder.bonusLayoutExpandIcon!!.setImageResource(R.drawable.faqplus)
        viewHolder.bonusLayoutTitleLabel!!.setTextColor(context!!.resources.getColor(R.color.purplish_brown))
    }//collapseBonusLayout ends

    private fun expandDetailsLayout(viewHolder: TariffsCardAdapterViewHolder) {
        viewHolder.detailsExpandableLayout!!.expand()
        viewHolder.detailsLayoutExpandIcon!!.setImageResource(R.drawable.faqminus)
        viewHolder.detailsLayoutTitleLabel!!.setTextColor(context!!.resources.getColor(R.color.colorPrimary))
    }//expandDetailsLayout ends

    private fun collapseDetailsLayout(viewHolder: TariffsCardAdapterViewHolder) {
        viewHolder.detailsExpandableLayout!!.collapse()
        viewHolder.detailsLayoutExpandIcon!!.setImageResource(R.drawable.faqplus)
        viewHolder.detailsLayoutTitleLabel!!.setTextColor(context!!.resources.getColor(R.color.purplish_brown))
    }//collapseDetailsLayout ends

    private fun expandPackagesPricesLayout(viewHolder: TariffsCardAdapterViewHolder) {
        viewHolder.packagesExpandableLayout!!.expand()
        viewHolder.packagesLayoutExpandIcon!!.setImageResource(R.drawable.faqminus)
        viewHolder.packagesLayoutTitleLabel!!.setTextColor(context!!.resources.getColor(R.color.colorPrimary))
    }//expandDetailsLayout ends

    private fun collapsePackagesPricesLayout(viewHolder: TariffsCardAdapterViewHolder) {
        viewHolder.packagesExpandableLayout!!.collapse()
        viewHolder.packagesLayoutExpandIcon!!.setImageResource(R.drawable.faqplus)
        viewHolder.packagesLayoutTitleLabel!!.setTextColor(context!!.resources.getColor(R.color.purplish_brown))
    }//collapseDetailsLayout ends

    inner class TariffsCardAdapterViewHolder :
        androidx.recyclerview.widget.RecyclerView.ViewHolder {
        var detailsLayoutExpandIcon: ImageView? = null
        var bonusLayoutTitleLabel: TextView? = null
        var detailsLayoutTitleLabel: TextView? = null
        var detailsViewStub: ViewStub? = null
        var packagePriceViewStub: ViewStub? = null
        var tariffNameLabel: TextView? = null
        var tariffDescriptionLabel: TextView? = null
        var tariffPriceLabel: TextView? = null
        var packagesLayoutTitleLabel: TextView? = null
        var packagesLayoutExpandIcon: ImageView? = null
        var bonusLayoutExpandIcon: ImageView? = null
        var bonusesContentLayout: LinearLayout? = null
        var bonusHeaderLayout: RelativeLayout? = null
        var bonusesExpandableLayout: ExpandableLayout? = null
        var packagesExpandableLayout: ExpandableLayout? = null
        var bonusesParentLayout: LinearLayout? = null
        var packagesHeaderLayout: RelativeLayout? = null
        var detailsHeaderLayout: RelativeLayout? = null
        var detailsExpandableLayout: ExpandableLayout? = null
        var subscribeButton: Button? = null
        var renewButton: Button? = null
        var subscribedButton: Button? = null
        var subscribeDisabledButton: Button? = null

        var ratingStarsLayout: ConstraintLayout? = null
        var starOne: CheckBox? = null
        var starTwo: CheckBox? = null
        var starThree: CheckBox? = null
        var starFour: CheckBox? = null
        var starFive: CheckBox? = null

        constructor(row: View) : super(row) {
            subscribeDisabledButton = row.findViewById(R.id.subscribeDisabledButton) as Button
            subscribeButton = row.findViewById(R.id.subscribeButton) as Button
            subscribedButton = row.findViewById(R.id.subscribedButton) as Button
            renewButton = row.findViewById(R.id.renewButton) as Button
            detailsLayoutExpandIcon = row.findViewById(R.id.detailsLayoutExpandIcon) as ImageView
            bonusLayoutTitleLabel = row.findViewById(R.id.bonusLayoutTitleLabel) as TextView
            detailsExpandableLayout =
                row.findViewById(R.id.detailsExpandableLayout) as ExpandableLayout
            detailsHeaderLayout = row.findViewById(R.id.detailsHeaderLayout) as RelativeLayout
            packagesHeaderLayout = row.findViewById(R.id.packagesHeaderLayout) as RelativeLayout
            bonusesParentLayout = row.findViewById(R.id.bonusesLayout) as LinearLayout
            packagesExpandableLayout =
                row.findViewById(R.id.packagesExpandableLayout) as ExpandableLayout
            bonusesExpandableLayout =
                row.findViewById(R.id.bonusesExpandableLayout) as ExpandableLayout
            bonusHeaderLayout = row.findViewById(R.id.bonusHeaderLayout) as RelativeLayout
            bonusesContentLayout = row.findViewById(R.id.bonusesContentLayout) as LinearLayout
            bonusLayoutExpandIcon = row.findViewById(R.id.bonusLayoutExpandIcon) as ImageView
            packagesLayoutExpandIcon = row.findViewById(R.id.packagesLayoutExpandIcon) as ImageView
            packagesLayoutTitleLabel = row.findViewById(R.id.packagesLayoutTitleLabel) as TextView
            detailsLayoutTitleLabel = row.findViewById(R.id.detailsLayoutTitleLabel) as TextView
            detailsViewStub = row.findViewById(R.id.detailsViewStub) as ViewStub
            packagePriceViewStub = row.findViewById(R.id.packagePriceViewStub) as ViewStub
            tariffNameLabel = row.findViewById(R.id.tariffNameLabel) as TextView
            tariffDescriptionLabel = row.findViewById(R.id.tariffDescriptionLabel) as TextView
            tariffPriceLabel = row.findViewById(R.id.tariffPriceLabel) as TextView

            ratingStarsLayout = row.findViewById(R.id.ratingStarsLayout)
            starOne = row.findViewById(R.id.star1)
            starTwo = row.findViewById(R.id.star2)
            starThree = row.findViewById(R.id.star3)
            starFour = row.findViewById(R.id.star4)
            starFive = row.findViewById(R.id.star5)
        }
    }//TariffsCardAdapterViewHolder ends
}//class ends