package com.azarphone.ui.adapters.recyclerviews

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.azarphone.R
import com.azarphone.api.pojo.response.paymenthistory.PaymentHistoryItem
import com.azarphone.util.ConstantsUtility
import com.azarphone.util.getManatConcatedString
import com.azarphone.util.logE
import com.azarphone.validators.hasValue

/**
 * @author Junaid Hassan on 11, April, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */
class PaymentHistoryAdapter(val mContext: Context, val paymentHistory: List<PaymentHistoryItem?>) : androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder>() {

    private val fromClass = "PaymentHistoryAdapter"

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.payment_history_adapter_items,
                parent, false)
        return PaymentHistoryAdapterViewHolder(view)
    }//onCreateViewHolder ends

    override fun getItemCount(): Int {
        return paymentHistory.size
    }//getItemCount ends

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as PaymentHistoryAdapterViewHolder

        if (hasValue(paymentHistory[position]!!.dateTime)) {
            logE("adapterValue", "dateTime:::".plus(paymentHistory[position]!!.dateTime), fromClass, "subscribe")
            viewHolder.dateTimeValue.text = paymentHistory[position]!!.dateTime
        } else {
            logE("adapterValue", "no adapterValue:::", fromClass, "subscribe")
        }

        if (hasValue(paymentHistory[position]!!.loanID)) {
            viewHolder.loanIdValue.isSelected = true
            viewHolder.loanIdValue.text = paymentHistory[position]!!.loanID
        }

        if (hasValue(paymentHistory[position]!!.amount)) {
            val amount = paymentHistory[position]!!.amount
            viewHolder.amountValue.isSelected = true
            viewHolder.amountValue.text = getManatConcatedString(mContext, amount!!,
                    ConstantsUtility.TariffConstants.AZERI_SYMBOL, 0.25f, R.dimen._8ssp)
        }
    }//onBindViewHolder ends

    inner class PaymentHistoryAdapterViewHolder(row: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(row) {
        var dateTimeValue: TextView= row.findViewById(R.id.dateTimeValue) as TextView
        var loanIdValue: TextView= row.findViewById(R.id.loanIdValue) as TextView
        var amountValue: TextView= row.findViewById(R.id.amountValue) as TextView
    }//PaymentHistoryAdapterViewHolder ends

}//class ends