package com.azarphone.validators

/**
@author Ehtisham haq
The file Contains the util method for TextFields and String
 **/


/**
 * Method to check if string has value
 *  @param givenString
 * @return Boolean
 **/

inline fun hasValue(givenString: String?): Boolean {
    return try {
        givenString != null && !givenString.equals("null", ignoreCase = true) && !givenString.equals("", ignoreCase = true)
    } catch (e: Exception) {
        false
    }
}


/**
 *  Method to check if we have Numeric Value
 *  @param s
 *  @return Boolean
 **/
inline fun isNumeric(s: String?): Boolean {
    return s?.trim { it <= ' ' }?.matches("[-+]?\\d*\\.?\\d+".toRegex()) ?: false
}//fun is numeric