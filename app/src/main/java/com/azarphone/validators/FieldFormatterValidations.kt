package com.azarphone.validators

import com.azarphone.util.ConstantsUtility

/**
 * @author Junaid Hassan on 11, March, 2019
 * Evamp & Saanga Pvt Ltd. Islamabad Pakistan,
 * junaid.hassan@evampsaanga.com
 */

inline fun isValidMsisdn(msisdn: String): Boolean {
    var validMsisdn = false
    if (hasValue(msisdn) && isNumeric(msisdn) && msisdn.length == ConstantsUtility.UserConstants.USER_NUMBER_LENGTH) {
        validMsisdn = true
    }
    return validMsisdn
}//isValidMsisdn ends

inline fun isValidOTP(pin: String): Boolean {
    var isValidPin = false
    var _pin: String = ""
    if (hasValue(pin)) {
        _pin = pin.trim({ it <= ' ' })
    }
    if (hasValue(_pin) && isNumeric(_pin) && pin.length == ConstantsUtility.UserConstants.USER_OTP_LENGTH) {
        isValidPin = true
    }
    return isValidPin
}//isValidOTP ends

inline fun isValidPassword(password: String): Boolean {
    var validPassword = false

    if (hasValue(password) && isValidPasswordLength(password)) {
        validPassword = true
    }

    return validPassword
}//isValidPassword ends

inline fun isValidPasswordLength(password: String): Boolean {
    var isValidLength = false

    isValidLength = password.length in 6..15

    return isValidLength
}//isValidPasswordLength ends