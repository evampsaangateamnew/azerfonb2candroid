﻿#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring

JNICALL
Java_com_azarphone_ui_activities_splashactivity_SplashActivity_getSecureKeyValues(
        JNIEnv *env,
        jobject /* this */) {
    std::string key = "$7*93)(@$#$><D:}[.,)(><2@§das";
    return env->NewStringUTF(key.c_str());
}

extern "C" JNIEXPORT jstring

JNICALL
Java_com_azarphone_ui_activities_splashactivity_SplashActivity_getKeysPublicServer(
        JNIEnv *env,
        jobject /* this */) {

    // A Separator is used for separation for the keys
    std::string separator = ":::::::"; //7 chars

    // Server Public Keys
    std::string sslPiningKey = "fr9aQi0of/5z/Lm4lDp7VNpB1DYr18slEq6KtqdTsJs=" +
                               separator +
                               "zE25Erem2tgZDGoOEqK9nrJxER5HhgZN8QYzMfLWQGY=" ;

    return env->NewStringUTF(sslPiningKey.c_str());

}


extern "C" JNIEXPORT jint

JNICALL
Java_com_azarphone_ui_activities_splashactivity_SplashActivity_AddValues(JNIEnv *env, jobject /* this */,
                                                                         jint firstValue, jint secondValue) {
    return (firstValue + secondValue);
}

//"fr9aQi0of/5z/Lm4lDp7VNpB1DYr18slEq6KtqdTsJs="//older one used in 2019

//"zE25Erem2tgZDGoOEqK9nrJxER5HhgZN8QYzMfLWQGY="//new one provided in 2020