package com.es.tec


/**
 *@author Ehtisham haq
 * NoConnectionException class if there is no network to avoid app crash if network is not avalible
 */
class NoConectionException(msg: String) : RuntimeException(msg)
