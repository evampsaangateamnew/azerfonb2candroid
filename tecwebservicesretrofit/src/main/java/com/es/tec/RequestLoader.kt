package com.es.tec

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater

/**
 * @author Ehtisham haq
 *
 * @Descrption: Class to provide Generic Loader for Network Requests
 * @param context Context
 */

class RequestLoader(var context: Context){

    lateinit var alertDialog: AlertDialog
    lateinit var  myLoaderDialogeBuilder:AlertDialog.Builder


  /**
   * Primary constructor to initialze the Dialoge Box
   */

  init {
      myLoaderDialogeBuilder=AlertDialog.Builder(context)

      val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
      val dialogeView=inflater.inflate(R.layout.loader,null)
      myLoaderDialogeBuilder.setView(dialogeView)
      myLoaderDialogeBuilder.setCancelable(false)
      alertDialog=myLoaderDialogeBuilder.create()
  }

    /**
     *  Method to Show the dialoge
     */

    fun showLoading(){
        alertDialog.show()

    }


    /**
     *  Method to Hide the dialoge
     */
    fun hideLoading(){
        alertDialog.hide()
    }

}