package com.es.tec

import android.content.Context
import android.content.SharedPreferences
import com.squareup.moshi.Moshi

/**
 * @author Junaid Hassan on 06, November, 2019
 * Senior Android Engineer. Islamabad Pakistan
 */
class LocalSharedPrefStorage(val context: Context) {
    val PREFERENCE_NAME = "azer.fone.local.shared.preferences.name"
    val KEY_CUSTOMER_DATA = "key.customer.data.preference"
    val KEY_USER_ACCOUNTS_DATA = "key.user.accounts.data.preference"
    val PREF_APP_RATER_SHOWN = "key.app.rater.shown"
    val MAY_BE_LATER_TIME = "key.may.be.later.time"
    val USER_LOGIN_TIME = "key.user.login.time"
    val USER_MSISDN = "key.user.msisdn.number"
    val FINGER_PRINT_SHOW = "key.pic.finger.print.show"
    val PIC_PROFILE_PICTURE_PATH = "key.pic.profile.picture.path"
    val IS_PASSWORD_CHANGE_MULTI_USER_LOGGED_OUT = "is.password.change.set.called"
    val PREF_DISCLAIMER_DIALOG_OPERATIONAL_HISTORY_NEEDTO_SHOW_KEY =
        "key.disclaimer.dialog.operational.history.show"
    val PREF_DISCLAIMER_DIALOG_USAGE_HISTORY_DETAILS_NEEDTO_SHOW_KEY =
        "key.disclaimer.dialog.usage.history.show"

    companion object {
        const val PREF_HOME_PAGE = "key.home.page"
        const val PREF_TOP_UP_PAGE = "key.top.up.page"
        const val PREF_BUNDLE_PAGE = "key.bundle.page"
        const val PREF_BUNDLE_SUBSCRIBED_PAGE = "key.bundle.subscribed.page"
        const val PREF_FNF_PAGE = "key.fnf.page"
        const val PREF_LOAN_PAGE = "key.loan.page"
        const val PREF_TRANSFER_MONEY = "key.transfer.money.page"
        const val PREF_TARIFF_PAGE = "key.tariff.page"
        const val PREF_TARIFF_SUBSCRIBED_PAGE = "key.tariff.subscribed.page"
    }


    var mSharedPreferences: SharedPreferences
    var mEditor: SharedPreferences.Editor

    init {
        mSharedPreferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)
        mEditor = mSharedPreferences.edit()
        mEditor.apply()
    }


    /**
     * @param key
     * @param timeToExpire
     * @param dataObject
     *
     *  @description:
     *  The Method Sets The Value in Local Storage
     **/
    /*fun <T> setResponse(key: String, timeToExpire: Long, dataObject: T) {

        val currentdata = LocalResponseObject(timeToExpire, 1, dataObject as Object)
        val moshi = Moshi.Builder().build()
        val jsonAdapter = moshi.adapter<LocalResponseObject>(LocalResponseObject::class.java!!)
        val currentResponseString = jsonAdapter.toJson(currentdata)
        mEditor.putString(key, currentResponseString)
        mEditor.apply()
    }*/

    //juni1289
    fun setResponse(key: String, dataObject: Any) {
        val moshi = Moshi.Builder().build()
        val jsonAdapter = moshi.adapter<Any>(dataObject::class.java)
        val currentResponseString = jsonAdapter.toJson(dataObject)
        mEditor.putString(key, currentResponseString)
            .apply()
    }//setResponse ends


    /**
     * @param key
     * @description:
     *  The Method return the response and Flag
     *  If Response is outdated the flag is 2
     *  If Response if not expired then flag is 1
     **/

    /*fun <T> getResponse(key: String): Pair<Int, T> {
        val moshi = Moshi.Builder().build()
        val jsonAdapter = moshi.adapter<LocalResponseObject>(LocalResponseObject::class.java!!)
        val localResponseObject = jsonAdapter.fromJson(mSharedPreferences.getString(key, ""))

        var flag = 1

        val currenttTimeInMilli = System.currentTimeMillis()

        val diff = localResponseObject!!.dateToExpire - currenttTimeInMilli
        if (diff > 0) {
            flag = 1
        } else {
            flag = 2
        }

        return Pair(flag, localResponseObject!!.data as T)
    }*/

    //juni1289
    fun getResponse(key: String, clazz: Class<*>): Any {
        val moshi = Moshi.Builder().build()
        val jsonAdapter = moshi.adapter<Any>(clazz)
        val localResponseObject = jsonAdapter.fromJson(mSharedPreferences.getString(key, "{}"))
        return localResponseObject!!
    }//getResponse ends

    fun addString(key: String, value: String) {
        mEditor.putString(key, value)
            .apply()
    } // add

    fun getString(key: String, def: String): String? {
        return mSharedPreferences.getString(key, def)
    }

    fun addBool(key: String, value: Boolean) {
        mEditor.putBoolean(key, value)
            .apply()
    } // add

    fun getBool(key: String, default: Boolean): Boolean {
        return mSharedPreferences.getBoolean(key, default)
    }

    fun addInt(key: String, value: Int) {
        mEditor.putInt(key, value)
            .apply()
    } // add

    fun getInt(key: String): Int {
        return mSharedPreferences.getInt(key, 0)
    }

    fun deleteAllPreferences() {
        mEditor.clear().commit()
    }

    fun deleteHomePageResponse(key: String) {
        mEditor.remove(key).commit()
    }

}