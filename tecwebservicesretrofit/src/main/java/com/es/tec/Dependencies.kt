package com.es.tec

import android.content.Context
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.security.MessageDigest
import java.security.cert.Certificate
import java.security.cert.CertificateException
import java.util.*
import java.util.concurrent.TimeUnit
import javax.net.ssl.*

/**
 * Created by Ehitshamshami on 3/8/2018.
 * The Base class for setting up dependencies of network client
 */

abstract class Dependencies {

    /**
     * Method to return Retrofit Builder
     * @param classService of Type Class<S>
     */

    protected fun <S> provideRestApi(classService: Class<S>): S {

        var httpClient: OkHttpClient

        httpClient = provideHttpClientManual(provideHttpLoggingInterceptor())

        val builder = Retrofit.Builder()


                .baseUrl(setBaseUrl())
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(httpClient)
        return builder.build().create(classService)
    }

    /**
     * This generate OkHTTP Client and return it
     */
    protected fun provideHttpClientManual(interceptor: HttpLoggingInterceptor): OkHttpClient {
        try {

            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                @Throws(CertificateException::class)
                override fun checkClientTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
                }

                @Throws(CertificateException::class)
                override fun checkServerTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
                }

                override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
                    return arrayOf()
                }
            })

            // Install the all-trusting trust manager
            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, java.security.SecureRandom())
            // Create an ssl socket factory with our all-trusting manager
            val sslSocketFactory = sslContext.socketFactory

            val builder = OkHttpClient.Builder()
//            builder.addNetworkInterceptor(StethoInterceptor())
            builder.sslSocketFactory(sslSocketFactory)
            builder.hostnameVerifier(object : HostnameVerifier {
                override fun verify(hostname: String, session: SSLSession): Boolean {
                    try {
                        var certs = session.peerCertificates
                        return checkIfCertificateMatch(certs)
                    } catch (e: Exception) {
                    }
                    return false

                }
            })

            builder.addInterceptor(interceptor)
            builder.addInterceptor { chain ->
                val request = chain.request()
                val builder = request.newBuilder()
                val headers = setHeaders()
                try {
                    if (headers != null && headers.size > 0) {
                        synchronized(this) {
                            for ((key, value) in headers) {
                                builder.addHeader(key, value)
//                                Log.e(key, value)
                            }
                        }
                    }
                } catch (e: Exception) {
                    var a = 0
                }
                chain.proceed(builder.build())
            }

            val timeout = setTimeOut()
            builder.connectTimeout(timeout.toLong(), TimeUnit.SECONDS)
            builder.readTimeout(timeout.toLong(), TimeUnit.SECONDS)
            builder.writeTimeout(timeout.toLong(), TimeUnit.SECONDS)

            return builder.build()
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

    }

    /**
     * This function match public key with local manualy and return true if mathced
     */
    fun checkIfCertificateMatch(serverCertsList: Array<Certificate>): Boolean {
        var isMatch = false

        try {
            if (!enableSSL()) {
                isMatch = true
                return isMatch
            }

            if (serverCertsList != null) {
                for (i in serverCertsList.indices) {
                    if (serverCertsList[i].publicKey != null) {
                        val md = MessageDigest.getInstance("SHA-256")
                        md.update(serverCertsList[i].publicKey.encoded)
                        val digest = md.digest()
                        val certformatch = String(android.util.Base64.encode(digest, android.util.Base64.DEFAULT))
                        val serverPublicKey = certformatch.trim()
                        val localPublicKeys = setSslCertificate()
                        if (localPublicKeys != null) {
                            for (j in 0 until localPublicKeys.size) {
//                                Log.d("okhttp", "serverPublicKey:::".plus(serverPublicKey).plus(" localPublicKey:::".plus(localPublicKeys[j])))
                                if (localPublicKeys[j].equals(serverPublicKey, true)) {
//                                    Log.d("okhttp", "matchedWith:::".plus(serverPublicKey).plus(" matchedLocalPublicKey:::".plus(localPublicKeys[j])))
                                    isMatch = true
                                    break
                                }
                            }
                        }
                        if (isMatch) {
                            break
                        }
                    }
                }
            }
        } catch (e: Exception) {
        }

        return isMatch
    }

    protected fun <S> provideRestApi(classService: Class<S>, timeout: Int): S {
        var httpClient: OkHttpClient

        httpClient = provideHttpClientManual(provideHttpLoggingInterceptor())

        val builder = Retrofit.Builder()
                .baseUrl(setBaseUrl())
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(httpClient)
        return builder.build().create(classService)
    }


    /**
     * Method to Provide OK Http client.
     * @param interceptor of Type HttpLogginInterceptor
     * @return okBuilder of type OkHttpClient
     */

    protected fun provideOkHttpClientDefault(interceptor: HttpLoggingInterceptor): OkHttpClient {
        val okBuilder = OkHttpClient.Builder()
        okBuilder.addInterceptor(interceptor)
        okBuilder.addInterceptor { chain ->
            if (isNetworkConnected(setContext())) {
                val request = chain.request()
                val builder = request.newBuilder()
                val headers = setHeaders()
                if (headers != null && headers.size > 0) {
                    for ((key, value) in headers) {
                        builder.addHeader(key, value)
//                        Log.e(key, value)
                    }
                }
                chain.proceed(builder.build())

            } else {
                throw NoConectionException("Network Connection Failed")
            }
        }

        val timeout = setTimeOut()
        okBuilder.connectTimeout(timeout.toLong(), TimeUnit.SECONDS)
        okBuilder.readTimeout(timeout.toLong(), TimeUnit.SECONDS)
        okBuilder.writeTimeout(timeout.toLong(), TimeUnit.SECONDS)

        return okBuilder.build()
    }


    /**
     * Method to Provide OK Http client, with api specfic timeout
     * @param interceptor of Type HttpLogginInterceptor
     * @return okBuilder of type OkHttpClient
     */

    /*   protected fun provideOkHttpClientDefault(interceptor: HttpLoggingInterceptor,timeout:Int): OkHttpClient {
           val okBuilder = OkHttpClient.Builder()
           okBuilder.addInterceptor(interceptor)
           okBuilder.addInterceptor { chain ->
               val request = chain.request()
               val builder = request.newBuilder()
               val headers = setHeaders()
               if (headers != null && headers.size > 0) {
                   for ((key, value) in headers) {
                       builder.addHeader(key, value)
                       Log.e(key, value)
                   }
               }
               chain.proceed(builder.build())
           }

           okBuilder.connectTimeout(timeout.toLong(), TimeUnit.SECONDS)
           okBuilder.readTimeout(timeout.toLong(), TimeUnit.SECONDS)
           okBuilder.writeTimeout(timeout.toLong(), TimeUnit.SECONDS)

           if(setSslCertificate().length>0 && enableSSL()){

               okBuilder.sslSocketFactory(getSllSocketContext(setSslCertificate()).socketFactory, getSystemDefaultTrustManager())
           }

           return okBuilder.build()
       }*/


    /**
     * Method to Provide Loggin Interceptor
     * @return interceptor of type HttpLogginInterceptor
     */

    protected fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = if (BuildConfig.DEBUG && setLogs()) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        return interceptor
    }


    /**
     *  Method to set logs
     */
    open fun setLogs() = true

    open fun enableSSL() = false

    open fun setSslCertificate(): ArrayList<String>? = null


    /**
     * Method to set Connection timeout
     * @return 30 of type Int
     */

    abstract fun setTimeOut(): Int

    abstract fun setBaseUrl(): String
    abstract fun setHeaders(): HashMap<String, String>
    abstract fun setContext(): Context


}