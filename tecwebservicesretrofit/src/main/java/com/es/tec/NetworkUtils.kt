package com.es.tec

import android.content.Context
import android.net.ConnectivityManager


/**
 * @author Ehtisham haq
 * Inline Network Utils Method
 */



/**
 * @Description
 * Inline Network Utils Method to check Network Connectivity
 * @param context of Type Context
 * @return Network state
 *
 */
inline fun isNetworkConnected(context: Context):Boolean{
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    val wifiNetwork = connectivityManager.activeNetworkInfo
    if (wifiNetwork != null && wifiNetwork.isConnected) {
        return true
    }

    return false
}



/**
 * @Description
 * Inline Network Utils Method to check If wifi is conncected
 * @param context of Type Context
 * @return True if Wifi Connected
 */
inline fun isWifiConnection(context: Context):Boolean{
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    val wifiNetwork = connectivityManager.activeNetworkInfo
    if (wifiNetwork != null && wifiNetwork.isConnected && wifiNetwork.isConnected) {
        return true
    }

    return false
}



/**
 * @Descrption
 * Inline Network Utils Method to check If mobile network is conncected
 * @param context of Type Context
 * @return True if Mobile Data
 */
inline fun isMobileConnection(context: Context):Boolean{
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    val isMobileConnection = connectivityManager.activeNetworkInfo
    if (isMobileConnection != null && isMobileConnection.isConnected && isMobileConnection.isConnected) {
        return true
    }
    return false
}


/**
 * @Description
 * Inline Network Utils Method to get Network Type
 * @return NetworkType
 */
inline fun getNetworkConnectionType(context: Context):Int{
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val mNetwork = connectivityManager.activeNetworkInfo
    return mNetwork.type
}



