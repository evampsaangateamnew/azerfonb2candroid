# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}


-keep public class com.android.installreferrer.** { *; }


#AndroidX
-dontwarn com.google.android.material.**
-keep class com.google.android.material.** { *; }

-dontwarn androidx.**
-keep class androidx.** { *; }
-keep interface androidx.* { *; }

# Uncomment this to preserve the line number information for
# debugging stack traces.
-keepattributes SourceFile,LineNumberTable

-keep public class * extends java.lang.Exception

# If you keep the line number information, uncomment this to
# hide the original source file name.
-renamesourcefileattribute SourceFile

#-optimizations   code/simplification/arithmetic,!code/simplification/cast,!field/*,! class/merging/*,!method/inlining/*
#-optimizationpasses 5
#-allowaccessmodification

-ignorewarnings

-keep class android.view.** { *; }
-keep class android.app.** { *; }
-keep class android.widget.** { *; }
-keep class android.support.** { *; }
-keep class android.support.v4.app.** { *; }
-keep class android.support.v4.content.** { *; }
-keep interface android.support.v4.app.** { *; }
-keep interface android.support.v4.content.** { *; }
-keep class android.support.v7.widget.** { *; }
-keep class * extends android.content.ContentProvider
-keep public class * extends android.content.ContextWrapper {public *;}

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.support.v7.widget.AppCompatTextView
-keep public class * extends android.support.v7.widget.AppCompatButton
-keep public class * extends com.google.firebase.iid.FirebaseInstanceIdServic
-keep public class * extends com.google.firebase.messaging.FirebaseMessagingService
-keep public class * extends android.os.Parcelable

-keep class com.google.** { *; }
#-keep class com.firebase.** { *; }
-keep class org.apache.** { *; }
-keepnames class com.fasterxml.jackson.** { *; }
-keepnames class javax.servlet.** { *; }
-keepnames class org.ietf.jgss.** { *; }
-dontwarn org.apache.**
-dontwarn org.w3c.dom.**

#-dontwarn com.mixpanel.**
#-keep class com.mixpanel.** { *; }

#-keep class com.facebook.** { *; }
-keepattributes Signature
-keep class com.excelliance.** { *; }


-dontwarn org.jetbrains.annotations.**

-dontwarn android.text.StaticLayout

-keepclassmembers class * {
    @com.squareup.moshi.FromJson <methods>;
    @com.squareup.moshi.ToJson <methods>;
}

-keep class android.support.** { *; }
-keep interface android.support.** { *; }
# Android Arch
-keep class android.arch.** { *; }



 #### -- Apache Commons --
 -dontwarn org.apache.commons.logging.**
#-keep class com.facebook.react.bridge.queue.NativeRunnable { *; }
#-keep class com.facebook.react.cxxbridge.ModuleRegistryHolder { *; }


-keepattributes Signature, InnerClasses, EnclosingMethod
# Retain service method parameters when optimizing.
-keepclassmembers,allowshrinking,allowobfuscation interface * {
    @retrofit2.http.* <methods>;
}
# Ignore annotation used for build tooling.
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
# Ignore JSR 305 annotations for embedding nullability information.
-dontwarn javax.annotation.**

-keepattributes Signature
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions


# Java
-keep class java.** { *; }
-dontwarn java.**


-dontwarn com.loopj.android.http.**
-keep class com.loopj.android.http.**{*;}
-dontwarn cz.msebera.**
-keep class cz.msebera.**{*;}

### For All liberary Module ###
#-keep class com.jazz.jazzworld.liberary.** { *; }

### For Jazz App Model Classes
#-keep class com.jazz.jazzworld.data.** { *; }
#-keep class com.jazz.jazzworld.data.model.** { *; }
#-keep class com.jazz.jazzworld.network.genericapis.balancemodel.** { *; }
#-keep class com.jazz.jazzworld.network.genericapis.** { *; }
#-keep class com.jazz.jazzworld.network.genericapis.downloadpostpaidbill.** { *; }
#-keep class com.jazz.jazzworld.network.genericapis.logout.** { *; }
#-keep class com.jazz.jazzworld.usecase.buySim.request.** { *; }
#-keep class com.jazz.jazzworld.usecase.buySim.response.** { *; }
#-keep class com.jazz.jazzworld.network.genericapis.modelfavourite.** { *; }
#-keep class com.jazz.jazzworld.network.genericapis.offerdetails.** { *; }
#-keep class com.jazz.jazzworld.network.genericapis.subscribemodel.** { *; }
#-keep class com.jazz.jazzworld.usecase.dashboard.models.** { *; }
#-keep class com.jazz.jazzworld.appmodels.model.** { *; }
#-keep class com.jazz.jazzworld.usecase.login.model.** { *; }
#-keep class com.jazz.jazzworld.usecase.login.termsconditions.models.** { *; }
#-keep class com.jazz.jazzworld.usecase.login.verifynumber.model.** { *; }
#-keep class com.jazz.jazzworld.usecase.login.verifypin.model.** { *; }
#-keep class com.jazz.jazzworld.usecase.login.welcome.tutorial.model.response.** { *; }
#-keep class com.jazz.jazzworld.firebase.** { *; }
#-keep class com.jazz.jazzworld.appmodels.response.** { *; }
#-keep class com.jazz.jazzworld.usecase.moreServices.fragments.appsFragment.response.** { *; }
#-keep class com.jazz.jazzworld.usecase.moreServices.fragments.servicesFragment.request.** { *; }
#-keep class com.jazz.jazzworld.usecase.moreServices.fragments.servicesFragment.response.** { *; }
#-keep class com.jazz.jazzworld.usecase.myAccount.Request.** { *; }
#-keep class com.jazz.jazzworld.usecase.myAccount.Response.** { *; }
#-keep class com.jazz.jazzworld.appmodels.model.** { *; }
#-keep class com.jazz.jazzworld.usecase.offers.modeloffers.request.** { *; }
#-keep class com.jazz.jazzworld.usecase.offers.modeloffers.response.** { *; }
#-keep class com.jazz.jazzworld.usecase.recharge.request.** { *; }
#-keep class com.jazz.jazzworld.usecase.recharge.response.** { *; }
#-keep class com.jazz.jazzworld.usecase.recharge.scratchCard.request.** { *; }
#-keep class com.jazz.jazzworld.usecase.recharge.scratchCard.response.** { *; }
#-keep class com.jazz.jazzworld.usecase.recharge.jazzCash.request.** { *; }
#-keep class com.jazz.jazzworld.usecase.recharge.jazzCash.response.** { *; }
#-keep class com.jazz.jazzworld.usecase.settings.settingsContent.request.** { *; }
#-keep class com.jazz.jazzworld.usecase.settings.settingsContent.response.** { *; }
#-keep class com.jazz.jazzworld.usecase.subscribedOffers.model.** { *; }
#-keep class com.jazz.jazzworld.appmodels.viewHistory.model.** { *; }
#-keep class com.jazz.jazzworld.usecase.whatsNew.** { *; }
#-keep class com.jazz.jazzworld.usecase.vasDetails.vasoverviewitem.** { *; }
#-keep class com.jazz.jazzworld.usecase.moreServices.fragments.servicesFragment.vasoffervisibilitymodel.** { *; }


#-Phase 2 Started ######
#-keep class com.jazz.jazzworld.appmodels.** { *; }
#-keep class com.jazz.jazzworld.usecase.switchnumber.modelclass.** { *; }
#-keep class com.jazz.jazzworld.usecase.login.verifypin.model.response.** { *; }
#-keep class com.jazz.jazzworld.usecase.switchnumber.addnumber.** { *; }
#-keep class com.jazz.jazzworld.usecase.recommendedoffers.request.** { *; }
#-keep class com.jazz.jazzworld.usecase.recommendedoffers.response.** { *; }
#-keep class com.jazz.jazzworld.usecase.dailyreward.request.** { *; }
#-keep class com.jazz.jazzworld.usecase.dailyreward.response.** { *; }
#-keep class com.jazz.jazzworld.network.genericapis.recommendedoffersmodel.** { *; }
#-keep class com.jazz.jazzworld.appmodels.submitcomplaint.request.** { *; }
#-keep class com.jazz.jazzworld.appmodels.submitcomplaint.response.** { *; }
#-keep class com.jazz.jazzworld.data.** { *; }



# Retrofit
#-keep class com.google.gson.** { *; }
#-keep public class com.google.gson.** {public private protected *;}
#-keep class com.google.inject.** { *; }
#-keep class org.apache.http.** { *; }
#-keep class org.apache.james.mime4j.** { *; }
#-keep class javax.inject.** { *; }
#-keep class javax.xml.stream.** { *; }
#-keep class retrofit.** { *; }
#-keep class com.google.appengine.** { *; }
#-keepattributes *Annotation*
#-keepattributes Signature
#-dontwarn com.squareup.okhttp.*
#-dontwarn rx.**
#-dontwarn javax.xml.stream.**
#-dontwarn com.google.appengine.**
#-dontwarn java.nio.file.**
#-dontwarn org.codehaus.**
#
#-dontwarn retrofit2.**
#-dontwarn org.codehaus.mojo.**
#-keep class retrofit2.** { *; }
#-keepattributes Exceptions
#-keepattributes RuntimeVisibleAnnotations
#-keepattributes RuntimeInvisibleAnnotations
#-keepattributes RuntimeVisibleParameterAnnotations
#-keepattributes RuntimeInvisibleParameterAnnotations
#
#-keepattributes EnclosingMethod
#-keepclasseswithmembers class * {
#    @retrofit2.http.* <methods>;
#}
#-keepclasseswithmembers interface * {
#    @retrofit2.* <methods>;
#}

### Android Architecture Components
# Ref: https://issuetracker.google.com/issues/62113696
# LifecycleObserver's empty constructor is considered to be unused by proguard
#-keepclassmembers class * implements android.arch.lifecycle.LifecycleObserver {
#    <init>(...);
#}

#-keep class * implements android.arch.lifecycle.LifecycleObserver {
#    <init>(...);
#}

# ViewModel's empty constructor is considered to be unused by proguard
#-keepclassmembers class * extends android.arch.lifecycle.ViewModel {
#    <init>(...);
#}

# keep Lifecycle State and Event enums values
#-keepclassmembers class android.arch.lifecycle.Lifecycle$State { *; }
#-keepclassmembers class android.arch.lifecycle.Lifecycle$Event { *; }

# keep methods annotated with @OnLifecycleEvent even if they seem to be unused
# (Mostly for LiveData.LifecycleBoundObserver.onStateChange(), but who knows)
#-keepclassmembers class * {
#    @android.arch.lifecycle.OnLifecycleEvent *;
#}



### Kotlin Coroutine
# https://github.com/Kotlin/kotlinx.coroutines/blob/master/README.md
# ServiceLoader support
#-keepnames class kotlinx.coroutines.internal.MainDispatcherFactory {}
#-keepnames class kotlinx.coroutines.CoroutineExceptionHandler {}
#-keepnames class kotlinx.coroutines.android.AndroidExceptionPreHandler {}
#-keepnames class kotlinx.coroutines.android.AndroidDispatcherFactory {}
# Most of volatile fields are updated with AFU and should not be mangled
#-keepclassmembernames class kotlinx.** {
#    volatile <fields>;
#}
# https://github.com/Kotlin/kotlinx.atomicfu/issues/57
#-dontwarn kotlinx.atomicfu.**

### Kotlin
#https://stackoverflow.com/questions/33547643/how-to-use-kotlin-with-proguard
#https://medium.com/@AthorNZ/kotlin-metadata-jackson-and-proguard-f64f51e5ed32
#-keep class kotlin.Metadata { *; }
#-keepclassmembers class kotlin.Metadata {
#    public <methods>;
#}
#
#-keep class kotlin.** { *; }
#-keep class kotlin.Metadata { *; }
#-dontwarn kotlin.**
#-keepclassmembers class **$WhenMappings {
#    <fields>;
#}
#-keepclassmembers class kotlin.Metadata {
#    public <methods>;
#}
#-assumenosideeffects class kotlin.jvm.internal.Intrinsics {
#    static void checkParameterIsNotNull(java.lang.Object, java.lang.String);
#}

# Moshi
#-keep class kotlin.reflect.jvm.internal.impl.builtins.BuiltInsLoaderImpl


### Google Play Billing
#-keep class com.android.vending.billing.**

### Adjust SDK, android.installreferrer
#-keep public class com.adjust.sdk.** { *; }
#-keep class com.google.android.gms.common.ConnectionResult {
#    int SUCCESS;
#}
#-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient {
#    com.google.android.gms.ads.identifier.AdvertisingIdClient$Info getAdvertisingIdInfo(android.content.Context);
#}
#-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient$Info {
#    java.lang.String getId();
#    boolean isLimitAdTrackingEnabled();
#}


### RxJava, RxAndroid (https://gist.github.com/kosiara/487868792fbd3214f9c9)
#-keep class rx.schedulers.Schedulers {
#    public static <methods>;
#}
#-keep class rx.schedulers.ImmediateScheduler {
#    public <methods>;
#}
#-keep class rx.schedulers.TestScheduler {
#    public <methods>;
#}
#-keep class rx.schedulers.Schedulers {
#    public static ** test();
#}
#-keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
#    long producerIndex;
#    long consumerIndex;
#}
#-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
#    long producerNode;
#    long consumerNode;
#}
#-dontwarn sun.misc.Unsafe



## Android Advertiser ID
#-keep class com.google.android.gms.common.GooglePlayServicesUtil {*;}
#-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient {*;}
#-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient$Info {*;}


### Retrofit 2
# Platform calls Class.forName on types which do not exist on Android to determine platform.
#-dontnote retrofit2.Platform
# Platform used when running on RoboVM on iOS. Will not be used at runtime.
#-dontnote retrofit2.Platform$IOS$MainThreadExecutor
# Platform used when running on Java 8 VMs. Will not be used at runtime.
#-dontwarn retrofit2.Platform$Java8
# Retain generic type information for use by reflection by converters and adapters.


#-dontwarn retrofit2.adapter.rxjava.CompletableHelper$** # https://github.com/square/retrofit/issues/2034
#To use Single instead of Observable in Retrofit interface
#-keepnames class rx.Single
#Retrofit does reflection on generic parameters. InnerClasses is required to use Signature and
# EnclosingMethod is required to use InnerClasses.

# Guarded by a NoClassDefFoundError try/catch and only used when on the classpath.
#-dontwarn kotlin.Unit
# Top-level functions that can only be used by Kotlin.
#-dontwarn retrofit2.-KotlinExtensions


### OkHttp3
#-keep class okhttp3.** { *; }
#-dontwarn okhttp3.**
#-dontwarn okio.**
#-dontwarn javax.annotation.**
# A resource is loaded with a relative path so the package of this class must be preserved.
#-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase
#-keep class okhttp3.Headers { *; }


# The Maps API uses serialization.
#-keepclassmembers class * implements java.io.Serializable {
#    static final long serialVersionUID;
#    static final java.io.ObjectStreamField[] serialPersistentFields;
#    private void writeObject(java.io.ObjectOutputStream);
#    private void readObject(java.io.ObjectInputStream);
#    java.lang.Object writeReplace();
#    java.lang.Object readResolve();
#}
